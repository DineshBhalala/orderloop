import React, { useState } from "react";
import { ReactComponent as DropDownIcon } from "../../../../Assets/chevron-down.svg";
import { ReactComponent as TrashIcon } from "../../../../Assets/trash.svg";
import { ReactComponent as EditIcon } from "../../../../Assets/edit.svg";
import AddButton from "./AddButton";

export default function KOTCard(props) {
    const [showCustomization, setShowCustomization] = useState(false);

    const handleClickDropdown = () => {
        setShowCustomization(!showCustomization);
    };

    return (
        <>
            <div className="border border-neutral-300 rounded-md p-4 shadow-smallDropDownShadow mb-1 relative">
                <div className="flex flex-row justify-between max-w-[240px]">
                    <div className="flex flex-row">
                        <span className="paragraphMediumMedium text-black mr-1">{props.number}.</span>
                        <span className="paragraphMediumMedium text-black">{props.title}</span>
                    </div>
                </div>

                <div className="mt-4 flex flex-row pl-[37px] mb-2">
                    <div className="flex flex-row mr-3 items-center">
                        <span className="paragraphSmallRegular">Customize</span>
                        <div className="cursor-pointer" onClick={handleClickDropdown}>
                            <DropDownIcon height={19} width={19} className={`${showCustomization && "rotate-180"}`} />
                        </div>
                    </div>
                    <div className={`flex paragraphSmallUnderline text-primary-500 ${!props.isAddNotes && "hidden"}`}>Add notes</div>
                </div>

                <div className={`${!showCustomization && "hidden"}`}>
                    <div className="mb-2">
                        <div className="paragraphSmallMedium pb-1 pl-[37px]">With</div>

                        <div className="flex flex-row justify-between pb-1 pl-[37px]">
                            <span className="text-neutral-500 paragraphSmallItalic">Extra cheese</span>
                            <div className="flex flex-row items-center">
                                <div className="flex flex-row items-center pr-4">
                                    <TrashIcon stroke="#EF4444" height={16} width={16} />
                                    <span className="paragraphSmallRegular text-destructive-500 ml-1">Remove</span>
                                </div>
                                <span className="paragraphSmallRegular">₹20.00/-</span>
                            </div>
                        </div>

                        <div className="flex flex-row justify-between pl-[37px]">
                            <span className="text-neutral-500 paragraphSmallItalic">Extra veggies</span>
                            <div className="flex flex-row items-center">
                                <div className="flex flex-row items-center pr-4">
                                    <TrashIcon stroke="#EF4444" height={16} width={16} />
                                    <span className="paragraphSmallRegular text-destructive-500 ml-1">Remove</span>
                                </div>
                                <span className="paragraphSmallRegular">₹20.00/-</span>
                            </div>
                        </div>
                    </div>

                    <div className="flex flex-row justify-between mb-1 pl-[37px]">
                        <span className="paragraphSmallMedium">Note</span>
                        <div className="flex flex-row items-center">
                            <EditIcon height={16} width={16} stroke="#EF4444" />
                            <span className="paragraphSmallRegular text-destructive-500 ml-1">Edit</span>
                        </div>
                    </div>

                    <div className="paragraphSmallRegular text-black ml-[37px]">Keep the pizza loaded with ample amount of the veggies and herbs.</div>
                </div>

                <div className="flex flex-col absolute right-4 top-4">
                    <span className="paragraphMediumMedium text-end">{props.price}</span>
                    {!props.isPreparing ? (
                        <div className="mt-3">
                            <AddButton />
                        </div>
                    ) : (
                        <div className="mt-[18px]">
                            <span className="px-1 py-0.5 border-primary-500 rounded bg-primary-50 text-primary-500 border">Preparing</span>
                        </div>
                    )}
                </div>
            </div>
        </>
    );
}
