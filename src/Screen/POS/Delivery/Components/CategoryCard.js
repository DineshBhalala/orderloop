import React from "react";

export default function CategoryCard(props) {
    return (
        <>
            <div className="border border-neutral-300 rounded-md px-3 py-2.5 min-w-[196px] flex flex-row">
                <img src={props.image} alt="" className="w-[72px] h-[72px] rounded-md mr-2" />
                <div className="flex flex-col py-0.5">
                    <h6 className="headingH6MediumDesktop mb-5"> {props.title}</h6>
                    <span className="paragraphSmallItalic text-neutral-500">{props.dishes} dishes</span>
                </div>
            </div>
        </>
    );
}
