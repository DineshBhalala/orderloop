import React, { useState } from "react";
import { ReactComponent as VegIcon } from "../../../../Assets/vegetable-icon.svg";
import AddButton from "./AddButton";

export default function DishCard(props) {
    const [dishItems, setDishItems] = useState(0);

    const handleValueChange = (value) => {
        setDishItems(value);
    };

    return (
        <>
            <div className="relative">
                <div
                    className={`h-[160px] border ${dishItems > 0 ? "border-primary-500 bg-primary-50" : "border-neutral-300"} rounded-md p-4 ${
                        props.image ? "w-[411px]" : "w-[195px]"
                    } flex flex-row items-start cursor-pointer`}
                    onClick={props.onClick}
                >
                    {props.image && <img alt="" src={props.image} className="mr-4 w-[128px] h-[128px] rounded" />}
                    <div className="w-full">
                        <div className="flex flex-row items-start justify-between mb-2">
                            <span className="paragraphMediumMedium text-black">{props.dishName}</span>
                            <VegIcon />
                        </div>
                        <div className="">
                            <span className="paragraphSmallRegular">{props.price}</span>
                        </div>
                    </div>
                </div>
                <div className="flex absolute bottom-4 right-4">
                    <AddButton handleValueChange={handleValueChange} />
                </div>
            </div>
        </>
    );
}
