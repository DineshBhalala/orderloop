import React from "react";
import { ReactComponent as LeftArrowIcon } from "../../../../../Assets/chevron-down.svg";
import { ReactComponent as CloseIcon } from "../../../../../Assets/close.svg";
import { ReactComponent as VegIcon } from "../../../../../Assets/vegetable-icon.svg";
import { RadioButton } from "../../../../../Components/FormControl/FormControls";
import { LargePrimaryButton } from "../../../../../Components/Buttons/Button";

export default function CustomizeOrderPizzaSizePopUp(props) {
    const pizzaInformation = [
        { name: "Personal giant size (22.5 cm)", price: "₹0.00/-" },
        { name: "Regular size (Serves 1, 17.7 cm)", price: "₹10.00/-" },
        { name: "Medium size (Serves 2, 24.5 cm)", price: "₹100.00/-" },
        { name: "Large size (Serves 4, 33 cm)", price: "₹120.00/-" },
        { name: "The giant size (Serves 8, 45.7 cm)", price: "₹150.00/-" },
        { name: "The monster size (Serves 12, 61 cm)", price: "₹200.00/-" },
    ];

    return (
        <>
            <div className="fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 flex justify-center items-center overflow-auto md:bg-white md:relative px-4">
                <div className="max-w-[613px] w-full rounded-xl md:rounded-none bg-shades-50 px-6 py-6 md:px-0 md:py-4 m-auto md:max-w-full">
                    <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer" onClick={props.handleClickClose}>
                        <LeftArrowIcon className="rotate-90" />
                        <span className="paragraphMediumMedium pl-1">Customize as per customer's taste</span>
                    </div>

                    <div className="flex flex-row justify-between items-center mb-6 md:hidden">
                        <div className="flex flex-col">
                            <span className="paragraphLargeMedium">Customize as per customer's taste</span>
                            <div className="flex flex-row items-center">
                                <span className="paragraphMediumItalic text-neutral-500">Double Cheese Margherita</span>
                                <div className="h-1 w-1 rounded-full bg-neutral-500 mx-3" />
                                <span className="paragraphMediumItalic text-neutral-500">₹559.00 - ₹1,559.00</span>
                            </div>
                        </div>

                        <div className="md:hidden cursor-pointer" onClick={props.handleClickClose}>
                            <CloseIcon />
                        </div>
                    </div>

                    <div className="">
                        <span className="paragraphLargeMedium mr-2">Select pizza base size</span>
                        <span className="paragraphMediumItalic text-neutral-500">(Select any 1)</span>
                    </div>

                    <div className="mb-14">
                        {pizzaInformation.map((el, index) => (
                            <div className="flex flex-row justify-between items-center mt-4" key={index}>
                                <div className="flex flex-row items-center">
                                    <RadioButton label={el.name} paddingL="6" optionId={index} labelStyle="paragraphMediumRegular" />
                                    <div className="ml-2">
                                        <VegIcon />
                                    </div>
                                </div>
                                <span className="paragraphSmallRegular">{el.price}</span>
                            </div>
                        ))}
                    </div>

                    <div className="flex flex-row items-center justify-between">
                        <span className="paragraphLargeMedium">Step 1/2</span>
                        <div className="min-w-[118px] cursor-pointer" onClick={props.handleClickProceed}>
                            <LargePrimaryButton name="Continue" />
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
