import React from "react";
import { ReactComponent as LeftArrowIcon } from "../../../../../Assets/chevron-down.svg";
import { ReactComponent as CloseIcon } from "../../../../../Assets/close.svg";
import { ReactComponent as VegIcon } from "../../../../../Assets/vegetable-icon.svg";
import { CheckBox } from "../../../../../Components/FormControl/FormControls";
import { LargePrimaryButton } from "../../../../../Components/Buttons/Button";

export default function CustomizeOrderToppingPpoUp(props) {
    const toppingVeg = [
        {
            name: "Jalapenos",
            price: "₹10.00/-",
        },
        { name: "Pineapples", price: "₹20.00/-" },
        { name: "Fresh tomatoes", price: "₹20.00/-" },
        { name: "Onion", price: "₹10.00/-" },
        { name: "Capsicums", price: "₹20.00/-" },
    ];
    return (
        <>
            <div className="fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 flex justify-center items-center overflow-auto md:bg-white md:relative px-4">
                <div className="max-w-[613px] w-full rounded-xl md:rounded-none bg-shades-50 px-6 py-6 md:px-0 md:py-4 m-auto md:max-w-full">
                    <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer" onClick={props.handleClickClose}>
                        <LeftArrowIcon className="rotate-90" />
                        <span className="paragraphMediumMedium pl-1">Customize as per customer's taste</span>
                    </div>

                    <div className="flex flex-row justify-between items-center mb-4 md:hidden">
                        <div className="flex flex-col">
                            <span className="paragraphLargeMedium">Customize as per customer's taste</span>
                            <div className="flex flex-row items-center">
                                <span className="paragraphMediumItalic text-neutral-500">Double Cheese Margherita</span>
                                <div className="h-1 w-1 rounded-full bg-neutral-500 mx-3" />
                                <span className="paragraphMediumItalic text-neutral-500">₹559.00</span>
                            </div>
                        </div>

                        <div className="md:hidden cursor-pointer" onClick={props.handleClickClose}>
                            <CloseIcon />
                        </div>
                    </div>

                    <div className="border border-neutral-300 rounded-lg shadow-smallDropDownShadow flex flex-row items-center justify-between py-[14px] px-4 mb-4 h-[52px]">
                        <div className="flex flex-row items-center">
                            <span className="paragraphMediumRegular mr-2">Personal giant size (22.5 cm)</span>
                            <VegIcon />
                        </div>
                        <span className="text-primary-500 font-normal text-sm leading-4 mt-0.5 border-b border-primary-500 cursor-pointer">Change</span>
                    </div>

                    <div className="">
                        <span className="paragraphLargeMedium mr-2">Toppings-veg (giant slices)</span>
                        <span className="paragraphMediumItalic text-neutral-500">(0/5)</span>
                    </div>

                    <div className="mb-9">
                        {toppingVeg.map((el, index) => (
                            <div className="pt-4 flex flex-row justify-between" key={index}>
                                <div className="flex flex-row items-center">
                                    <CheckBox label={el.name} optionId={el.name + index} paddingL="6" labelStyle="paragraphMediumRegular" />
                                    <VegIcon className="ml-2" />
                                </div>
                                <span className="paragraphSmallRegular ">{el.price}</span>
                            </div>
                        ))}
                    </div>

                    <div className="flex flex-row items-center justify-between">
                        <span className="paragraphLargeMedium">Step 2/2</span>
                        <div className="">
                            <LargePrimaryButton name="Add dish" />
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
