import React from "react";
import { ReactComponent as TrashIcon } from "../../../../../Assets/trash.svg";
import { LargeDestructiveButton } from "../../../../../Components/Buttons/Button";

export default function RestartOrderPopup(props) {
    return (
        <>
            <div className="fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 md:px-4 flex">
                <div className="max-w-[475px] w-full rounded-xl bg-shades-50 px-8 py-6 m-auto md:w-full md:px-4 md:py-4">
                    <div className="pb-3 flex flex-row items-center">
                        <div className="p-2 bg-destructive-200 rounded-md">
                            <TrashIcon stroke="#EF4444" />
                        </div>
                        <h2 className="paragraphLargeMedium ml-2">Restart the order?</h2>
                    </div>
                    <p className="paragraphMediumItalic text-neutral-500 pb-3">
                        Are you sure to restart the order of
                        <span className="text-destructive-500 italic"> Sarthak Kanchan</span> with <span className="text-destructive-500 italic">0</span> dishes?
                    </p>

                    <div className="flex flex-row justify-between">
                        <button
                            onClick={() => props.handleClickClose()}
                            className="h-12 w-1/2 mr-2.5 md:mr-[7.5px] paragraphMediumMedium rounded-md text-neutral-500 bg-shades-50 border border-neutral-300"
                        >
                            Cancel
                        </button>
                        <div className="w-1/2 md:ml-[7.5px] ml-2.5 cursor-pointer" onClick={props.handleClickClose}>
                            <LargeDestructiveButton name="Restart" />
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
