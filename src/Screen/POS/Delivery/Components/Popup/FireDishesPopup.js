import React from "react";
import { ReactComponent as LeftArrowIcon } from "../../../../../Assets/chevron-down.svg";
import { ReactComponent as CloseIcon } from "../../../../../Assets/close.svg";
import { ReactComponent as VegIcon } from "../../../../../Assets/vegetable-icon.svg";
import { CheckBox } from "../../../../../Components/FormControl/FormControls";
import { LargePrimaryButton } from "../../../../../Components/Buttons/Button";

export default function FireDishesPopup(props) {
    const dish = [
        {
            name: "Double Cheese Margherita",
            qty: "02",
        },
        { name: "Farmhouse Extraveganza Veggie", qty: "01" },
        { name: "Indi Tandoori Paneers", qty: "01" },
    ];
    return (
        <>
            <div className="fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 flex justify-center items-center overflow-auto md:bg-white md:relative px-4">
                <div className="max-w-[629px] w-full rounded-xl md:rounded-none bg-shades-50 px-6 py-6 md:px-0 md:py-4 m-auto md:max-w-full">
                    <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer" onClick={props.handleClickClose}>
                        <LeftArrowIcon className="rotate-90" />
                        <span className="paragraphMediumMedium pl-1">Fire Dishes</span>
                    </div>

                    <div className="flex flex-row justify-between items-center mb-6 md:hidden">
                        <div className="flex flex-col">
                            <span className="paragraphLargeMedium">Fire Dishes</span>
                            <span className="paragraphMediumItalic text-neutral-500">Select dishes you would like to start preparaing</span>
                        </div>

                        <div className="md:hidden cursor-pointer" onClick={props.handleClickClose}>
                            <CloseIcon />
                        </div>
                    </div>

                    <div className="">
                        <span className="paragraphLargeMedium mr-2">Toppings-veg (giant slices)</span>
                        <span className="paragraphMediumItalic text-neutral-500">(0/5)</span>
                    </div>

                    <div className="mb-12">
                        {dish.map((el, index) => (
                            <div className="pt-4 flex flex-row justify-between" key={index}>
                                <div className="flex flex-row items-center">
                                    <CheckBox label={el.name} optionId={el.name + index} paddingL="4" labelStyle="paragraphMediumRegular" />
                                    <VegIcon className="ml-2" />
                                </div>
                                <span className="paragraphSmallRegular ">{el.qty}</span>
                            </div>
                        ))}
                    </div>

                    <div className="flex flex-row justify-between">
                        <CheckBox label="Select/deselect all" optionId="selectDeselectAll" paddingL="4" labelStyle="paragraphLargeMedium" />
                        <div className="max-w-[151px] w-full">
                            <LargePrimaryButton name="KOT selected" />
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
