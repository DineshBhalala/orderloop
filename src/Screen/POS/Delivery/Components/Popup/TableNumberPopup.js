import React from "react";
import { ReactComponent as CloseIcon } from "../../../../../Assets/close.svg";
import { ReactComponent as LeftArrowIcon } from "../../../../../Assets/chevron-down.svg";
import DropDown from "../../../../../Components/DropDown/DropDown";
import { LargePrimaryButton } from "../../../../../Components/Buttons/Button";

export default function TableNumberPopup(props) {
    return (
        <>
            <div className="fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 flex justify-center items-center overflow-auto md:bg-white md:relative px-4">
                <div className="max-w-[475px] w-full rounded-xl md:rounded-none bg-shades-50 px-8 py-6 md:px-0 md:py-4 m-auto md:max-w-full">
                    <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer" onClick={props.handleClickClose}>
                        <LeftArrowIcon className="rotate-90" />
                        <span className="paragraphMediumMedium pl-1">Fire Dishes</span>
                    </div>

                    <div className="flex flex-row justify-between items-center mb-6 md:hidden">
                        <div className="flex flex-col">
                            <span className="paragraphLargeMedium">Table number</span>
                            <span className="paragraphMediumItalic text-neutral-500">Select table for the customer</span>
                        </div>

                        <div className="md:hidden cursor-pointer" onClick={props.handleClickClose}>
                            <CloseIcon />
                        </div>
                    </div>

                    <div className="mb-12">
                        <DropDown label="Table number" dropdownLabel="Select table" buttonTextColor="neutral-300" shadow="shadow-shadowXsmall" />
                    </div>

                    <div className="flex flex-row justify-between items-center">
                        <span className="paragraphLargeMedium">Step 2/2</span>
                        <div className="max-w-[118px]">
                            <LargePrimaryButton name="Continue" />
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
