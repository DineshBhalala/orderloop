import React from "react";

export default function OrderDetailCard(props) {
    return (
        <>
            <div className="w-full rounded h-[56px] border border-neutral-300 relative justify-center text-center mx-auto pt-1">
                {props.icon ?? <div className="paragraphMediumRegular text-secondary-800 h-6">-</div>}
                <div className={`paragraphMediumMedium ${props.truncate && "truncate w-[106px]"} mx-auto ${props.textColor}`}>{props.title}</div>
                <div className="absolute top-1 right-1">{props.statusIcon}</div>
            </div>
        </>
    );
}
