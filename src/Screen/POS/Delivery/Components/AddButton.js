import React, { useEffect, useState } from "react";
import { ReactComponent as TrashIcon } from "../../../../Assets/trash.svg";

export default function AddButton(props) {
    const [buttonValue, setButtonValue] = useState(0);

    const changeValue = (value) => {
        value === "add" ? setButtonValue(buttonValue + 1) : setButtonValue(buttonValue - 1);
    };

    const handleClick = () => {
        buttonValue === 0 && setButtonValue(1);
    };

    useEffect(() => {
        props.handleValueChange && props.handleValueChange(buttonValue);
    }, [buttonValue]);

    return (
        <>
            <button className={`paragraphSmallRegular p-[1px] h-[30px] ${buttonValue === 0 ? "min-w-[80px]" : "min-w-[104px]"} w-full rounded-md bg-primary-500 text-center cursor-pointer`} onClick={handleClick}>
                <div className="flex flex-row items-center">
                    <span
                        className={`font-normal cursor-pointer text-2xl leading-5 ${buttonValue === 1 ? "px-[7px] py-1.5" : "px-[9.5px] py-1"} text-[#6C5DD3] bg-white rounded-md ${buttonValue === 0 && "hidden"}`}
                        onClick={() => changeValue("remove")}
                    >
                        {buttonValue === 1 ? <TrashIcon width={16} height={16} /> : "-"}
                    </span>
                    <span className="text-shades-50 mx-auto paragraphMediumRegular">{buttonValue !== 0 ? buttonValue : "Add"}</span>
                    <span className={`font-normal text-2xl leading-5 px-[7px] py-1 text-[#6C5DD3] bg-white rounded-md ${buttonValue === 0 && "hidden"} cursor-pointer`} onClick={() => changeValue("add")}>
                        +
                    </span>
                </div>
            </button>
        </>
    );
}
