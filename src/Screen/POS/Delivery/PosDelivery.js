import React, { useState } from "react";

import { ReactComponent as SearchIcon } from "../../../Assets/search.svg";
import { ReactComponent as UserIcon } from "../../../Assets/user.svg";
import { ReactComponent as ReimbursementIcon } from "../../../Assets/reimbursement.svg";
import { ReactComponent as DininIcon } from "../../../Assets/dine-in.svg";
import { ReactComponent as SuccessIcon } from "../../../Assets/success-tick.svg";
import { ReactComponent as OfferIcon } from "../../../Assets/offers.svg";
import { ReactComponent as HoldIcon } from "../../../Assets/hold.svg";
import { ReactComponent as DeliveryIcon } from "../../../Assets/riders.svg";
import { ReactComponent as TakeAwayIcon } from "../../../Assets/orders.svg";
import { ReactComponent as LocationIcon } from "../../../Assets/location.svg";

import { DefaultInputField } from "../../../Components/InputField/InputField";
import { Tab } from "../../../Components/Tabs/Tabs";
import { LargePrimaryButton, LargeTertiaryButton } from "../../../Components/Buttons/Button";

import OrderDetailCard from "./Components/OrderDetailCard";
import DishCard from "./Components/DishCard";
import pizza1 from "../../../Assets/mediaLibrary/pizza1.png";
import pizza2 from "../../../Assets/mediaLibrary/pizza2.png";
import pizza3 from "../../../Assets/mediaLibrary/pizza2.png";
import pizza4 from "../../../Assets/mediaLibrary/pizza3.png";
import pizza5 from "../../../Assets/mediaLibrary/pizza4.png";
import pizza6 from "../../../Assets/mediaLibrary/pizza5.png";
import pizza7 from "../../../Assets/mediaLibrary/pizza6.png";
import pizza8 from "../../../Assets/mediaLibrary/pizza7.png";
import CategoryCard from "./Components/CategoryCard";
import CustomerInformationPopup from "./Components/Popup/CustomerInformationPopup";
import TableNumberPopup from "./Components/Popup/TableNumberPopup";
import RestartOrderPopup from "./Components/Popup/RestartOrderPopup";
import CustomizeOrderPizzaSizePopUp from "./Components/Popup/CustomizeOrderPizzaSizePopup";
import CustomizeOrderToppingPpoUp from "./Components/Popup/CustomizeOrderToppingOrderPpoUp";
import { useLocation } from "react-router-dom";
import FireDishesPopup from "./Components/Popup/FireDishesPopup";
import KOTCard from "./Components/KOTCard";

export default function PosDelivery(props) {
    const dishes = [
        { dishName: "Double Cheese Margherita ", price: "₹559.00/-", image: pizza1 },
        { dishName: "Deluxe Veggie", price: "₹549.00/-" },
        { dishName: "Farmhouse", price: "₹459.00/-" },
        { dishName: "Peppy Paneer", price: "₹459.00/-" },
        { dishName: "Veg Extravaganza", price: "₹549.00/-" },
        { dishName: "Mexican Green Wave", price: "₹459.00/-" },
        { dishName: "Cheese n Corn", price: "₹379.00/-" },
        { dishName: "Peppy Paneer", price: "₹459.00/-" },
        { dishName: "Paneer Makhani", price: "₹549.00/-" },
        { dishName: "Farmhouse Extraveganza Vaeggie", price: "₹559.00/-", image: pizza2 },
        { dishName: "Peppy Paneer", price: "₹459.00/-" },
        { dishName: "Veg Extravaganza", price: "₹549.00/-" },
        { dishName: "Mexican Green Wave", price: "₹459.00/-" },
        { dishName: "Cheese n Corn", price: "₹379.00/-" },
    ];

    const categories = [
        { title: "All", dishes: "102", image: pizza1 },
        { title: "Salad", dishes: "10", image: pizza2 },
        { title: "Pizza", dishes: "32", image: pizza3 },
        { title: "Pastries", dishes: "10", image: pizza4 },
        { title: "Burger", dishes: "02", image: pizza5 },
        { title: "Juices", dishes: "10", image: pizza6 },
        { title: "Shakes", dishes: "07", image: pizza7 },
        { title: "Burger", dishes: "02", image: pizza5 },
        { title: "Juices", dishes: "10", image: pizza6 },
        { title: "Shakes", dishes: "07", image: pizza7 },
        { title: "ice cream", dishes: "15", image: pizza8 },
    ];

    const location = useLocation();

    const orderType = location.state?.pageName;

    const [popupState, setPopupState] = useState({
        showCustomerInformationPopup: false,
        showTableNumberPopup: false,
        showRestartOrderPopup: false,
        showCustomizeDishPopup: false,
        showCustomizationToppingPopup: false,
        showFireDishPopup: false,
    });

    const handleClick = (key) => {
        setPopupState((prevState) => ({
            ...prevState,
            [key]: !prevState[key],
        }));
    };

    const openSecondPopup = (popup1, popup2) => {
        setPopupState((prevState) => ({
            ...prevState,
            [popup2]: !prevState[popup2],
            [popup1]: false,
        }));
    };

    return (
        <>
            <div className="bg-[#fafafa]">
                <div className="lg:px-4 lg:pb-[23px] w-full max-w-[1336px] mx-auto bg-white">
                    <div className="flex flex-row items-start">
                        <div className="w-full pl-6 pt-4">
                            <div className="flex flex-row justify-between items-start pr-7">
                                <div className="flex flex-col">
                                    <span className="paragraphLargeSemiBold mb-4">Dishes</span>
                                    <span className="paragraphMediumItalic text-neutral-500">Pizza</span>
                                </div>

                                <div className="w-full max-w-[375px]">
                                    <DefaultInputField placeholder="Search category or dish" placeholderIcon={<SearchIcon stroke="#D3D2D8" />} />
                                </div>
                            </div>

                            <div className="mb-2">
                                <div className="-ml-2.5 h-[564px] overflow-auto scrollbarStyle mr-2.5">
                                    {dishes.map((el, index) => (
                                        <div className="inline-block align-top mx-2.5 mt-4 cursor-pointer" key={index}>
                                            <DishCard dishName={el.dishName} price={el.price} image={el.image} onClick={() => handleClick("showCustomizeDishPopup")} />
                                        </div>
                                    ))}
                                </div>
                            </div>

                            <div className="sticky z-1 -ml-6 bottom-0 pt-4 pl-6 bg-white border-t border-neutral-300">
                                <div className="paragraphLargeSemiBold">Categories</div>

                                <div className="w-[877px] flex flex-col overflow-auto [&::-webkit-scrollbar]:hidden -ml-6 pl-[14px] pr-4">
                                    <div className="flex flex-row mt-4">
                                        {categories.map((el, index) => (
                                            <div className="even:hidden mx-2.5" key={index}>
                                                <CategoryCard title={el.title} dishes={el.dishes} image={el.image} />
                                            </div>
                                        ))}
                                    </div>
                                    <div className="flex flex-row mt-4">
                                        {categories.map((el, index) => (
                                            <div className="mx-2.5 odd:hidden" key={index}>
                                                <CategoryCard title={el.title} dishes={el.dishes} image={el.image} />
                                            </div>
                                        ))}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="pl-3 pr-5 pt-3 max-w-[443px] w-full border-l border-neutral-300">
                            <div className="paragraphMediumMedium text-end text-destructive-500 mb-3 cursor-pointer" onClick={() => handleClick("showRestartOrderPopup")}>
                                Restart Order
                            </div>

                            <div className="flex flex-row mb-4 pb-4 border-b border-neutral-300">
                                {orderType === "takeaway" ? (
                                    <div className="mr-1 w-full">
                                        <OrderDetailCard
                                            icon={<UserIcon className="mx-auto" stroke="#3D8C82" />}
                                            statusIcon={<SuccessIcon width={16} height={16} />}
                                            title="Mr. Sarthak Kanchan"
                                            textColor="text-tertiary-800"
                                        />
                                    </div>
                                ) : orderType === "delivery" ? (
                                    <div className="mr-1 w-full">
                                        <OrderDetailCard
                                            truncate={true}
                                            icon={<UserIcon className="mx-auto" stroke="#3D8C82" />}
                                            statusIcon={<SuccessIcon width={16} height={16} />}
                                            title="Mr. Sarthak Kanchan"
                                            textColor="text-tertiary-800"
                                        />
                                    </div>
                                ) : (
                                    <div className="mr-1 w-full cursor-pointer" onClick={() => handleClick("showCustomerInformationPopup")}>
                                        <OrderDetailCard
                                            icon={<UserIcon className="mx-auto" stroke="#CB8400" />}
                                            statusIcon={<ReimbursementIcon width={16} height={16} />}
                                            title="Customer info."
                                            textColor="text-secondary-800"
                                        />
                                    </div>
                                )}

                                <div className="mx-1 w-full">
                                    {orderType === "takeAway" ? (
                                        <OrderDetailCard
                                            icon={<TakeAwayIcon className="mx-auto" stroke="#3D8C82" />}
                                            statusIcon={<SuccessIcon width={16} height={16} />}
                                            title="Takeaway"
                                            textColor="text-tertiary-800"
                                        />
                                    ) : orderType === "delivery" ? (
                                        <OrderDetailCard
                                            icon={<DeliveryIcon className="mx-auto" stroke="#3D8C82" />}
                                            statusIcon={<SuccessIcon width={16} height={16} />}
                                            title="Delivery"
                                            textColor="text-tertiary-800"
                                        />
                                    ) : (
                                        <OrderDetailCard
                                            icon={<DininIcon className="mx-auto" stroke="#3D8C82" />}
                                            statusIcon={<SuccessIcon width={16} height={16} />}
                                            title="Dine-in"
                                            textColor="text-tertiary-800"
                                        />
                                    )}
                                </div>

                                {orderType === "delivery" ? (
                                    <div className="ml-1 w-full">
                                        <OrderDetailCard
                                            statusIcon={<SuccessIcon stroke="#CB8400" width={16} height={16} />}
                                            icon={<LocationIcon className="mx-auto" stroke="#3D8C82" />}
                                            title="Location"
                                            textColor="text-tertiary-800"
                                        />
                                    </div>
                                ) : (
                                    orderType !== "takeAway" && (
                                        <div className="ml-1 w-full">
                                            <OrderDetailCard statusIcon={<ReimbursementIcon stroke="#CB8400" width={16} height={16} />} title="Table number" textColor="text-secondary-800" />
                                        </div>
                                    )
                                )}
                            </div>

                            <div className="flex flex-row justify-between items-center mb-2">
                                <div className="flex flex-row items-center">
                                    <span className="paragraphMediumMedium text-black mr-2">Customer order</span>

                                    <span className="paragraphSmallItalic text-neutral-500">
                                        (<span className="paragraphSmallSemiBold text-primary-500">0</span> dish, <span className="paragraphSmallSemiBold text-primary-500">0</span> Quantity)
                                    </span>
                                </div>

                                <span className="paragraphMediumMedium text-neutral-300">Clear order</span>
                            </div>

                            <div className="mb-2">
                                <DefaultInputField placeholder="Enter dish name" shadow="shadow-smallDropDownShadow" />
                            </div>

                            <div className="flex flex-row items-center justify-between pb-4 mb-4 border-b border-neutral-300">
                                <span className="paragraphMediumSemiBold">KOT 1</span>
                                <span className="paragraphMediumRegular text-primary-500 cursor-pointer">Show</span>
                            </div>

                            <div className="mb-2">
                                <span className="paragraphMediumSemiBold">KOT 2</span>
                            </div>

                            <div className="overflow-auto scrollbarStyle h-[292px] -mr-3.5 pr-2">
                                <KOTCard isAddNotes number={1} title="Double Cheese Margherita" price="₹1,118.00/-" />
                                <KOTCard isPreparing number={2} title="Farmhouse Extraveganza Veggie" price="₹529.00/-" />
                                <KOTCard isPreparing number={3} title="Indi Tandoori Paner" price="₹549.00/-" />
                            </div>

                            <div className="flex flex-row mb-2 pt-2 border-t border-neutral-300">
                                <div className="mr-1.5 w-full">
                                    <Tab shadow="shadow-smallDropDownShadow" label="Apply offer" IconDefault={<OfferIcon />} />
                                </div>

                                <div className="w-full ml-1.5 cursor-pointer" onClick={props.handelHoldOrder}>
                                    <Tab shadow="shadow-smallDropDownShadow" label="Hold order" IconDefault={<HoldIcon />} />
                                </div>
                            </div>

                            <div className="p-4 border rounded-md shadow-smallDropDownShadow border-neutral-300 mb-2">
                                <div className="flex flex-row justify-between mb-4">
                                    <span className="paragraphMediumRegular">Tax</span>
                                    <span className="paragraphMediumRegular text-neutral-500">₹200.00/-</span>
                                </div>

                                <div className="flex flex-row justify-between mb-4">
                                    <span className="paragraphMediumRegular">Subtotal</span>
                                    <span className="paragraphMediumRegular text-neutral-500">₹2,125.00/-</span>
                                </div>

                                <div className="flex flex-row justify-between">
                                    <span className="paragraphMediumSemiBold">Total</span>
                                    <span className="paragraphMediumSemiBold">₹2,325.00/-</span>
                                </div>
                            </div>

                            <div className="flex flex-row mb-2">
                                <div className="mr-1 w-1/3">
                                    <LargeTertiaryButton name="Save" />
                                </div>

                                <div className="mx-1 w-1/3">
                                    <LargeTertiaryButton name="Save & print" />
                                </div>

                                <div className="ml-1 w-1/3">
                                    <LargeTertiaryButton nameMargin="mx-0.2" name="Save & e-bill" />
                                </div>
                            </div>

                            <div className="flex flex-row mb-4">
                                <div className="mr-1 w-1/3 cursor-pointer" onClick={() => handleClick("showFireDishPopup")}>
                                    <LargePrimaryButton name="KOT" isDefault={false} />
                                </div>

                                <div className="mx-1 w-1/3">
                                    <LargePrimaryButton name="KOT & print" />
                                </div>

                                <div className="ml-1 w-1/3">
                                    <LargePrimaryButton padding="px-2.5" name="Add payment" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className={`${!popupState.showCustomerInformationPopup && "hidden"}`}>
                <CustomerInformationPopup
                    handleClickClose={() => handleClick("showCustomerInformationPopup")}
                    handleClickProceed={() => openSecondPopup("showCustomerInformationPopup", "showTableNumberPopup")}
                />
            </div>
            <div className={`${!popupState.showTableNumberPopup && "hidden"}`}>
                <TableNumberPopup handleClickClose={() => openSecondPopup("showTableNumberPopup", "showCustomerInformationPopup")} />
            </div>
            <div className={`${!popupState.showRestartOrderPopup && "hidden"}`}>
                <RestartOrderPopup handleClickClose={() => handleClick("showRestartOrderPopup")} />
            </div>
            <div className={`${!popupState.showCustomizeDishPopup && "hidden"}`}>
                <CustomizeOrderPizzaSizePopUp
                    handleClickClose={() => handleClick("showCustomizeDishPopup")}
                    handleClickProceed={() => openSecondPopup("showCustomizeDishPopup", "showCustomizationToppingPopup")}
                />
            </div>
            <div className={`${!popupState.showCustomizationToppingPopup && "hidden"}`}>
                <CustomizeOrderToppingPpoUp handleClickClose={() => openSecondPopup("showCustomizationToppingPopup", "showCustomizeDishPopup")} />
            </div>
            <div className={`${!popupState.showFireDishPopup && "hidden"}`}>
                <FireDishesPopup handleClickClose={() => handleClick("showFireDishPopup")} />
            </div>
        </>
    );
}
