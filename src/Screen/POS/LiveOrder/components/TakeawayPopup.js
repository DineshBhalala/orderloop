import React from "react";
import { ReactComponent as LeftArrowIcon } from "../../../../Assets/chevron-down.svg";
import { ReactComponent as CloseIcon } from "../../../../Assets/close.svg";
import { DefaultInputField } from "../../../../Components/InputField/InputField";
import { LargePrimaryButton } from "../../../../Components/Buttons/Button";
import { useNavigate } from "react-router-dom";

export default function TakeawayPopup(props) {
    const navigate = useNavigate();

    const handleClickProceed = () => {
        navigate("/pos-delivery", { state: { pageName: "takeAway" } });
    };
    return (
        <>
            <div className="fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 flex justify-center items-center overflow-auto md:bg-white md:absolute px-4 md:pt-10">
                <div className="max-w-[475px] w-full rounded-xl md:rounded-none bg-shades-50 px-8 py-6 md:px-0 md:py-4 m-auto md:max-w-full">
                    <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer" onClick={props.handleClickClose}>
                        <LeftArrowIcon className="rotate-90" />
                        <span className="paragraphMediumMedium pl-1">Back to table management</span>
                    </div>

                    <div className="flex flex-row justify-between items-center mb-6 md:hidden">
                        <div className="flex flex-col">
                            <span className="paragraphLargeMedium">Takeaway #BBQA</span>
                            <span className="paragraphMediumItalic text-neutral-500">Enter basic details of the customer </span>
                        </div>

                        <div className="md:hidden cursor-pointer" onClick={props.handleClickClose}>
                            <CloseIcon />
                        </div>
                    </div>

                    <div className="mb-4">
                        <DefaultInputField label="Customer name" placeholder="Enter customer name" />
                    </div>

                    <div className="mb-12">
                        <DefaultInputField label="Mobile number" placeholder="Enter mobile number" />
                    </div>

                    <div className="cursor-pointer" onClick={handleClickProceed}>
                        <LargePrimaryButton name="Proceed" />
                    </div>
                </div>
            </div>
        </>
    );
}
