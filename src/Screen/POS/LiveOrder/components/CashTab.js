import React from "react";
import { DefaultInputField } from "../../../../Components/InputField/InputField";
import { LargePrimaryButton } from "../../../../Components/Buttons/Button";

export default function CashTab() {
    return (
        <>
            <div className="mb-[216px] md:mb-52">
                <DefaultInputField placeholder="Enter cash given by customer in rupees" label="Cash amount" shadow="shadow-shadowXsmall" />
            </div>
            <div className="md:-mx-4 md:bg-white md:fixed md:w-full md:bottom-0">
                <div className="md:p-4 md:border-y md:border-neutral-300">
                    <div className="flex flex-row justify-between mb-4">
                        <span className="paragraphMediumSemiBold">Total bill amount</span>
                        <span className="paragraphMediumSemiBold">₹5,325.00/-</span>
                    </div>
                    <div className="flex flex-row justify-between mb-4 border-b pb-4 border-neutral-300 md:border-none md:mb-0">
                        <span className="paragraphMediumSemiBold">Total cash given</span>
                        <span className="paragraphMediumSemiBold">
                            <span className="mr-2">-</span>₹2,000.00/-
                        </span>
                    </div>
                    <div className="flex flex-row justify-between mb-12 md:mb-0">
                        <span className="paragraphMediumSemiBold">Return cash to customer</span>
                        <span className="paragraphMediumSemiBold">
                            <span className="mr-2">-</span>₹5,325.00/-
                        </span>
                    </div>
                </div>
                <div className="md:px-4 md:pt-2 md:pb-6 shadow-dropShadow">
                    <div className="w-full">
                        <LargePrimaryButton name="Add cash" />
                    </div>
                </div>
            </div>
        </>
    );
}
