import React from "react";
import DropDown from "../../../../Components/DropDown/DropDown";
import { DefaultInputField } from "../../../../Components/InputField/InputField";
import { LargePrimaryButton } from "../../../../Components/Buttons/Button";

export default function UPITab() {
    return (
        <>
            <div className="md:pb-40">
                <div className="mb-2">
                    <DropDown label="Select merchant" menuItems={["Paytm"]} shadow="shadow-shadowXsmall" />
                </div>
                <div className="mb-2">
                    <DefaultInputField label="Transaction number" placeholder="Enter transaction number" shadow="shadow-shadowXsmall" />
                </div>
                <div className="mb-[56px]">
                    <DefaultInputField label="Enter amount" placeholder="Enter amount" shadow="shadow-shadowXsmall" />
                </div>
            </div>
            <div className="md:-mx-4 md:bg-white md:fixed md:w-full md:bottom-0">
                <div className="md:p-4 md:border-y md:border-neutral-300">
                    <div className="flex flex-row justify-between mb-4">
                        <span className="paragraphMediumSemiBold">Total bill amount</span>
                        <span className="paragraphMediumSemiBold">₹5,325.00/-</span>
                    </div>
                    <div className="flex flex-row justify-between mb-4 border-b pb-4 border-neutral-300 md:border-none md:mb-0">
                        <span className="paragraphMediumSemiBold">Total UPI amount</span>
                        <span className="paragraphMediumSemiBold">
                            <span className="mr-2">-</span>₹3,325.00/-
                        </span>
                    </div>
                    <div className="flex flex-row justify-between mb-12 md:mb-0">
                        <span className="paragraphMediumSemiBold">UPI status</span>
                        <span className="text-success-600 text-[10px] border font-medium w-[41px] h-6 px-2 pt-1.5 text-center border-success-600 rounded bg-success-50 ml-2 leading-none">Paid</span>
                    </div>
                </div>
                <div className="md:px-4 md:pt-2 md:pb-6 shadow-dropShadow">
                    <div className="w-full">
                        <LargePrimaryButton name="Add UPI payment" />
                    </div>
                </div>
            </div>
        </>
    );
}
