import React, { useEffect, useState } from "react";
import { ReactComponent as CloseIcon } from "../../../../Assets/close.svg";
import { ReactComponent as LeftArrowIcon } from "../../../../Assets/chevron-down.svg";
import CashTab from "./CashTab";
import { MultipleTab } from "../../../../Components/Tabs/Tabs";
import UPITab from "./UPITab";
import { DropDownTabs } from "../../../../Components/DropDown/DropDownTabs";

export default function AddPaymentPopup(props) {
    const billCard = (title, amount, titleColor, isNegative) => {
        return (
            <div className="border border-neutral-300 rounded-lg px-4 py-[11px] flex flex-col">
                <span className={`paragraphSmallRegular text-${titleColor} pb-1`}>{title}</span>
                <span className="paragraphMediumSemiBold">
                    {isNegative && <span className="mr-1">-</span>} {amount}
                </span>
            </div>
        );
    };

    const billPaymentCard = [
        { title: "Total bill", amount: "₹5,325.00/-", titleColor: "primary-500" },
        { title: "Customer paid", amount: "₹2,000.00/-", titleColor: "tertiary-800" },
        { title: "Total remaining", amount: "₹5,325.00/-", titleColor: "destructive-500" },
        { title: "Return customer", amount: "₹3,325.00/-", titleColor: "secondary-800", isNegative: true },
    ];

    const paymentType = [
        { label: "Cash", badgeText: "₹2,000" },
        { label: "Credit card" },
        { label: "Debit card" },
        { label: "Wallet" },
        { label: "UPI", badgeText: "₹3,325" },
        { label: "Cryptocurrency" },
        { label: "Gift card" },
    ];

    const [activeTab, setActiveTab] = useState(0);

    const handleClickPaymentTab = (index) => {
        setActiveTab(index);
    };

    const [renderComponent, setRenderComponent] = useState(<CashTab />);

    useEffect(() => {
        switch (activeTab) {
            case 0:
                setRenderComponent(<CashTab />);
                break;
            case 4:
                setRenderComponent(<UPITab />);
                break;
            default:
                break;
        }
    }, [activeTab]);

    return (
        <>
            <div className="fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 flex justify-center items-center overflow-auto md:bg-white md:relative p-4">
                <div className="max-w-[830px] w-full rounded-xl md:rounded-none bg-shades-50 px-8 py-6 md:px-0 md:py-4 m-auto md:max-w-full">
                    <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer" onClick={props.handleClickClose}>
                        <LeftArrowIcon className="rotate-90" />
                        <span className="paragraphMediumMedium pl-1">Fire Dishes</span>
                    </div>

                    <div className="flex flex-row justify-between items-center mb-6 md:hidden">
                        <div className="flex flex-col">
                            <span className="paragraphLargeMedium">Add payment</span>
                            <span className="paragraphMediumItalic text-neutral-500">Select and complete payment</span>
                        </div>

                        <div className="md:hidden cursor-pointer" onClick={props.handleClickClose}>
                            <CloseIcon />
                        </div>
                    </div>

                    <div className=" w-full pb-4 border-b border-neutral-300">
                        <div className="flex flex-row justify-between md:block md:-mx-1 mobile:mx-0">
                            {billPaymentCard.map((el, index) => (
                                <div className={`${index !== 0 && "ml-4 md:ml-0 md:mt-2"} w-1/4 md:w-1/2 md:inline-block md:px-1 mobile:w-full mobile:px-0`} key={index}>
                                    {billCard(el.title, el.amount, el.titleColor, el.isNegative)}
                                </div>
                            ))}
                        </div>
                    </div>

                    <div className="flex flex-row items-start md:block md:mt-4">
                        <div className="flex flex-col md:hidden">
                            {paymentType.map((el, index) => (
                                <div className="mt-4 min-w-[173px]" key={index}>
                                    <MultipleTab index={index} label={el.label} badgeText={el.badgeText} isActive={activeTab === index} onClick={handleClickPaymentTab} />
                                </div>
                            ))}
                        </div>
                        <div className="hidden md:block w-full">
                            <DropDownTabs
                                menuItems={[
                                    { item: "Cash", badgeText: "₹2,000", onClick: () => handleClickPaymentTab(0) },
                                    { item: "Credit card" },
                                    { item: "Debit card" },
                                    { item: "Wallet" },
                                    { item: "UPI", badgeText: "₹3,325", onClick: () => handleClickPaymentTab(4) },
                                    { item: "Cryptocurrency" },
                                    { item: "Gift card" }
                                ]} />
                        </div>
                        <div className="w-full max-w-[399px] pt-4 border-l border-neutral-300 ml-6 pl-6 md:max-w-full md:ml-0 md:pl-0 md:border-none">{renderComponent}</div>
                    </div>
                </div>
            </div>
        </>
    );
}
