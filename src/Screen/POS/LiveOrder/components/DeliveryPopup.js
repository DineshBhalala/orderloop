import React, { useState } from "react";
import { ReactComponent as CloseIcon } from "../../../../Assets/close.svg";
import { ReactComponent as LeftArrowIcon } from "../../../../Assets/chevron-down.svg";
import { ReactComponent as CalenderIcon } from "../../../../Assets/calendar.svg";
import { ReactComponent as TimerIcon } from "../../../../Assets/timer.svg";
import { DefaultInputField } from "../../../../Components/InputField/InputField";
import DropDown from "../../../../Components/DropDown/DropDown";
import address from "../../../../Assets/address.png";
import { LargePrimaryButton } from "../../../../Components/Buttons/Button";
import { useNavigate } from "react-router-dom";

export default function DeliveryPopup(props) {
    const [showSelectionSection, setShowSelectionSection] = useState(false);

    const handleClickDeliveryAddress = () => {
        setShowSelectionSection(!showSelectionSection);
    };

    const navigate = useNavigate();

    const handleClickProceed = () => {
        navigate("/pos-delivery", { state: { pageName: "delivery" } });
    };

    return (
        <>
            <div className="fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 flex justify-center items-center overflow-auto md:bg-white md:relative px-4">
                <div className="max-w-[830px] w-full rounded-xl md:rounded-none bg-shades-50 px-8 py-6 md:px-0 md:py-4 m-auto md:max-w-full">
                    <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer" onClick={props.handleClickClose}>
                        <LeftArrowIcon className="rotate-90" />
                        <span className="paragraphMediumMedium pl-1">Back to table management</span>
                    </div>

                    <div className="flex flex-row justify-between items-center mb-6 md:hidden">
                        <div className="flex flex-col">
                            <span className="paragraphLargeMedium">Delivery #BBQA</span>
                            <span className="paragraphMediumItalic text-neutral-500">Enter basic details for order delivery</span>
                        </div>

                        <div className="md:hidden cursor-pointer" onClick={props.handleClickClose}>
                            <CloseIcon />
                        </div>
                    </div>

                    <div className="flex flex-row items-center mb-4">
                        <div className="w-1/2 mr-2">
                            <DefaultInputField label="Customer name" placeholder="Enter customer name" shadow="shadow-shadowXsmall" inputStyle="sm:px-3 mobile:px-2" />
                        </div>
                        <div className="w-1/2 ml-2">
                            <DefaultInputField label="Mobile number" placeholder="Enter mobile number" shadow="shadow-shadowXsmall" inputStyle="sm:px-3 mobile:px-2" />
                        </div>
                    </div>

                    <div className="flex flex-row items-center mb-4">
                        <div className="w-1/2 mr-2">
                            <DropDown
                                shadow="shadow-shadowXsmall"
                                label="Select date"
                                placeholder="Enter customer name"
                                menuIcon={<CalenderIcon height={24} />}
                                dropdownLabel="Select date"
                                buttonTextColor="neutral-300"
                                buttonStyle="sm:px-3 mobile:px-2"
                            />
                        </div>
                        <div className="w-1/2 ml-2">
                            <DropDown
                                shadow="shadow-shadowXsmall"
                                label="Select time"
                                placeholder="Select time"
                                menuIcon={<TimerIcon height={24} />}
                                dropdownLabel="Select date"
                                buttonTextColor="neutral-300"
                                buttonStyle="sm:px-3 mobile:px-2"
                            />
                        </div>
                    </div>

                    <div className="flex flex-row items-center mb-4">
                        <div className="w-1/2 mr-2">
                            <DefaultInputField shadow="shadow-shadowXsmall" label="House name" placeholder="Enter house name" inputStyle="sm:px-3 mobile:px-2" />
                        </div>
                        <div className="w-1/2 ml-2">
                            <DefaultInputField shadow="shadow-shadowXsmall" label="Landmark" placeholder="Enter landmark" inputStyle="sm:px-3 mobile:px-2" />
                        </div>
                    </div>

                    <div className={`mb-4 ${showSelectionSection && "hidden"} cursor-pointer`} onClick={handleClickDeliveryAddress}>
                        <DefaultInputField shadow="shadow-shadowXsmall" label="Delivery address" placeholder="Enter locality" />
                    </div>

                    {showSelectionSection && (
                        <>
                            <div className="mb-1">
                                <DefaultInputField shadow="shadow-shadowXsmall" label="Select section" placeholder="Select section" />
                            </div>
                            <div className="mb-4">
                                <DefaultInputField shadow="shadow-shadowXsmall" placeholder="Select address" />
                            </div>
                        </>
                    )}

                    <div className="mb-12 md:mb-6">
                        <img src={address} alt="" className="border border-neutral-300 rounded-lg shadow-smallDropDownShadow" />
                    </div>

                    <div className="cursor-pointer" onClick={handleClickProceed}>
                        <LargePrimaryButton name="Proceed" />
                    </div>
                </div>
            </div>
        </>
    );
}
