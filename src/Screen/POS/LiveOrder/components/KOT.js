import React, { useState } from "react";
import { ReactComponent as TimerIcon } from "../../../../Assets/timer.svg";
import { ReactComponent as VegIcon } from "../../../../Assets/vegetable-icon.svg";
import { ReactComponent as NonVegIcon } from "../../../../Assets/non-veg.svg";
import { ReactComponent as DineInIcons } from "../../../../Assets/dine-in.svg";
import { ReactComponent as DropDownIcon } from "../../../../Assets/chevron-down.svg";
import { ReactComponent as PrintIcon } from "../../../../Assets/print.svg";
import { LargeDestructiveButton, LargePrimaryButton, LargeTertiaryButton } from "../../../../Components/Buttons/Button";

export default function KOT(props) {
    const [showWithItems, setShowWithItems] = useState(false);

    const handleClickWithDropDown = () => {
        setShowWithItems(!showWithItems);
    };

    return (
        <>
            <div className="w-full border border-neutral-300 rounded-xl p-4">
                <div className="flex flex-row justify-between items-center mb-4 pb-4 border-b border-neutral-300">
                    <div className="flex flex-row">
                        {props.orderingMode === "Dine-In order" ? (
                            <>
                                <div className="p-2 bg-primary-500 rounded-lg">
                                    <DineInIcons height={32} width={32} stroke="#FAFAFA" />
                                </div>
                            </>
                        ) : (
                            props.orderIcon
                        )}
                        <div className="ml-2">
                            <div className="paragraphMediumSemiBold">{props.orderingMode}</div>
                            <div className="paragraphMediumItalic text-neutral-500">Order {props.orderLabel}</div>
                        </div>
                    </div>
                    <div className="flex flex-col items-end">
                        <div className="paragraphMediumRegular">09 Aug 2022, 11:27 AM</div>
                        <div className="paragraphMediumItalic text-neutral-500">Swiggy</div>
                    </div>
                </div>

                <div className="border-b border-neutral-300 pb-4 mb-4 paragraphMediumSemiBold flex flex-row justify-between">
                    <div className="flex flex-row">
                        <span>
                            {props.dishPacked == null ? "Dishes served" : "Dish packed"} {props.tableServed}
                        </span>
                    </div>
                    <span>
                        <span className="paragraphMediumMedium text-primary-500">{props.dishPacked ?? props.dishServed}</span> out of 10
                    </span>
                </div>

                <div className={`mb-4 pb-4 border-b border-neutral-300 ${!props.isUpdated && "hidden"} text-center`}>
                    <span className="paragraphOverlineLarge text-tertiary-800">ORDERED DISHES UPDATED!</span>
                </div>

                <div className="flex flex-row justify-between mb-4">
                    <span className="text-base leading-4 border-b border-neutral-900">Ordered dishes</span>
                    <span className="text-base leading-4 border-b border-neutral-900">Quantity</span>
                </div>

                <div className="border-b border-neutral-300 mb-4">
                    {props.items.map((el, index) => (
                        <div className="mb-4" key={index}>
                            <div className="flex flex-row justify-between mb-1">
                                <div className="flex flex-row max-w-[199px]">
                                    <span className={`flex ${el.isPrepared ? "paragraphMediumStrikethrough" : "paragraphMediumSemiBold"} ${el.updatedQuantity && "text-tertiary-800"}`}>
                                        {el.displayName}
                                    </span>
                                    <div className="min-w-[15.6px] min-h-[15.6px] pt-1.5 ml-2">{el.foodType === "veg" ? <VegIcon /> : <NonVegIcon />}</div>
                                </div>
                                <div className="min-w-[70px] text-right">
                                    <span
                                        className={`${el.isDeleted ? "paragraphMediumStrikethrough" : "paragraphMediumSemiBold"} ${
                                            el.updatedQuantity && "paragraphXSmallStrikethrough text-tertiary-800"
                                        }`}
                                    >
                                        {("0" + el.quantity).slice(-2)}
                                    </span>
                                    {el.updatedQuantity && <span className="paragraphMediumSemiBold text-tertiary-800 ml-1">{("0" + el.updatedQuantity).slice(-2)}</span>}
                                </div>
                            </div>
                            <div className="pl-[21px] ml-2 border-l border-neutral-300">
                                {el.customization && (
                                    <div className="">
                                        <div className="flex flex-row items-center">
                                            <span className={`${el.isPrepared ? "paragraphSmallStrikethrough mr-1" : "paragraphSmallMedium"} titleSap relative`}>With</span>
                                            <div className={`${!el.isPrepared && "hidden"} cursor-pointer`} onClick={handleClickWithDropDown}>
                                                <DropDownIcon height={20} className={`${showWithItems && "rotate-180"}`} />
                                            </div>
                                        </div>
                                        <div className={`flex flex-col paragraphSmallItalic text-neutral-500 mb-1 ${!showWithItems && el.isPrepared && "hidden"}`}>
                                            {el.customization.map((el, index) => (
                                                <div className="mt-1" key={index}>
                                                    {el.displayName}
                                                </div>
                                            ))}
                                        </div>
                                    </div>
                                )}
                                {el.addons && (
                                    <div className="w-full max-w-[199px]">
                                        <span className="paragraphSmallMedium titleSap relative">Note</span>
                                        <p className="mt-1 flex flex-col paragraphSmallItalic text-neutral-500">{el.addons}</p>
                                    </div>
                                )}
                            </div>
                        </div>
                    ))}
                </div>

                <div className="flex flex-row justify-between paragraphMediumSemiBold mb-4 pb-4 border-b border-neutral-300">
                    <div className="">
                        <span>Total bill amount</span>
                        {props.isBillPaid ? (
                            <span className="text-success-600 text-[10px] border font-medium w-[37px] px-2 py-1 text-center border-success-600 rounded bg-success-50 ml-2 leading-none">Paid</span>
                        ) : (
                            <span className="text-destructive-600 text-[10px] border font-medium w-[37px] px-2 py-1 text-center border-destructive-600 rounded bg-destructive-100 ml-2 leading-none">
                                Unpaid
                            </span>
                        )}
                    </div>

                    <span>{props.totalBillAmount}</span>
                </div>

                <div className="flex flex-row justify-between pb-4 mb-4 border-b border-neutral-300">
                    <span className="paragraphMediumSemiBold">
                        Mr. Sarthak's<span className="paragraphMediumRegular text-primary-500"> 56th order</span>
                    </span>
                    <span className="paragraphMediumUnderline text-primary-500">Customer details</span>
                </div>

                {props.isBillPaid ? (
                    <div className="mb-4 pb-4 border-b border-neutral-300">
                        <div className="flex flex-row mb-2">
                            <div className="w-1/2 mr-[7px]">
                                <LargeDestructiveButton name="Cancel order" />
                            </div>
                            <div className="w-1/2 ml-[7px]">
                                <LargePrimaryButton name="Accept order" />
                            </div>
                        </div>
                        <div className="w-full">
                            <LargeTertiaryButton name="Rider assignment" />
                        </div>
                    </div>
                ) : (
                    <div className="flex flex-row items-center pb-4 mb-4 border-b border-neutral-300">
                        <div className="p-[11px] border border-neutral-300 rounded-md">
                            <PrintIcon height={24} />
                        </div>
                        <span className="paragraphMediumRegular mx-3 px-[16px] py-3 border border-neutral-300 rounded-md min-w-[81px]">+ KOT</span>
                        <div className="max-w-[223px] w-full" onClick={props.handleClickAddPayment}>
                            <LargePrimaryButton name="Add payment" />
                        </div>
                    </div>
                )}
                <div className="flex flex-row justify-between paragraphMediumSemiBold border-neutral-300 text-tertiary-800">
                    <span>Time elapsed</span>
                    <div className="flex flex-row items-center">
                        <TimerIcon height={24} width={24} stroke="#3D8C82" />
                        <span className="ml-2">
                            {props.timeElapsed[0].mins} mins {props.timeElapsed[1].seconds} secs
                        </span>
                    </div>
                </div>
            </div>
        </>
    );
}
