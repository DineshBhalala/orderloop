import React, { useState, useEffect, useRef } from "react";
import { useNavigate } from "react-router-dom";
import { ReactComponent as LiveOrderIcon } from "../../../Assets/live-order.svg";
import { ReactComponent as PastOrderIcon } from "../../../Assets/past-order.svg";
import { ReactComponent as FailedOrderIcon } from "../../../Assets/failed-order.svg";
import { ReactComponent as AddIcon } from "../../../Assets/add.svg";
import { ReactComponent as SwiggyIcon } from "../../../Assets/swiggy.svg";
import { ReactComponent as ZomatoIcon } from "../../../Assets/zomato.svg";
import { ReactComponent as DineIn } from "../../../Assets/dine-in.svg";
import AddPaymentPopup from "./Components/AddPaymentPopup";
import KOT from "./Components/KOT";
import PaginationWithNumber from "../../../Components/Pagination/PaginationWithNumber";
import DeliveryPopup from "./Components/DeliveryPopup";
import TakeAwayPopup from "./Components/TakeawayPopup";
import OrdersCard from "../../Orders/OrdersCard";
import { TabBig } from "../../../Components/Tabs/Tabs";
import { LargePrimaryButton } from "../../../Components/Buttons/Button";
import { DropDownTabs } from "../../../Components/DropDown/DropDownTabs";
import { ModeAndCustomerName } from "../../Orders/ModeAndCustomerName";
import { OrderStatus } from "../../Orders/OrderStatus";
import CustomerPastOrderDetailsPopup from "../../Orders/CustomerPastOrderDetailsPopup";
import CustomerFailedOrderDetailsPopup from "../../Orders/CustomerFailedOrderDetailsPopup";
import OrdersDetailsPopup from "../../Orders/OrdersDetailsPopup";
import { ReactComponent as OrderIcon } from "../../../Assets/orders.svg";
import { ReactComponent as SuccessTick } from "../../../Assets/success-tick.svg";
import { ReactComponent as Reimbursement } from "../../../Assets/reimbursement.svg";
import { ReactComponent as Cancel } from "../../../Assets/cancel.svg";
import { ReactComponent as IconClose } from "../../../Assets/close.svg";
import { ReactComponent as Card } from "../../../Assets/card.svg";
import { ReactComponent as UpiIcon } from "../../../Assets/UPI.svg";
import { ReactComponent as Cash } from "../../../Assets/cash.svg";
import { ReactComponent as Riders } from "../../../Assets/riders.svg";
import CalenderField from "../../../Components/Calender/CalenderField";
import { ReactComponent as FilterIcon } from "../../../Assets/filter.svg";
import { ReactComponent as Export } from "../../../Assets/export.svg";
import CustomerPastOrderDetailsDropDown from "../../Orders/CustomerPastOrderDetailsDropDown";
import CustomerFailedOrderDetailsDropDown from "../../Orders/CustomerFailedOrderDetailsDropDown";
import { OrdersIcon, OrdersIcons, OrdersStatusMobileIcons } from "../../Orders/PastOrderIcons";

export default function PosOrders() {
    const [windowWidth, setWindowWidth] = useState(window.innerWidth);

    useEffect(() => {
        const handleResize = () => {
            setWindowWidth(window.innerWidth);
        };

        window.addEventListener("resize", handleResize);

        return () => {
            window.removeEventListener("resize", handleResize);
        };
    }, []);

    // const orderStatus = [
    //     { label: "Live orders", iconDefault: <LiveOrderIcon />, iconClick: <LiveOrderIcon stroke="#6C5DD3" /> },
    //     { label: "Past orders", iconDefault: <PastOrderIcon />, iconClick: <PastOrderIcon stroke="#6C5DD3" /> },
    //     { label: "Failed orders", iconDefault: <FailedOrderIcon />, iconClick: <FailedOrderIcon stroke="#6C5DD3" /> },
    // ];

    const orderTypes = [
        { label: "Dine-in", iconMobile: "dineInIcon" },
        { label: "Takeaway", iconMobile: "takeAwayIcon" },
        { label: "Delivery", iconMobile: "deliveryIcon" },
    ];

    // const [activeOrderTab, setActiveOrderTab] = useState(0);

    // const handleClickOrderTab = (index) => {
    //     setActiveOrderTab(index);
    // };

    const orders = [
        {
            icon: <SwiggyIcon height={48} width={48} />,
            orderingMode: "Delivery order",
            orderLabel: "#BBQA",
            isBillPaid: true,
            dishPacked: 0,
            items: [
                {
                    displayName: "Double Cheese Margherita Pizza",
                    note: "Keep the pizza loaded with ample amount of cheese",
                    foodType: "veg",
                    quantity: 2,
                    addons: "Keep the pizza loaded with ample amount of cheese",
                    customization: [
                        {
                            displayName: "Extra cheese",
                        },
                        {
                            displayName: "Extra veggies",
                        },
                    ],
                },
                {
                    displayName: "Indi Tandoori Paneer",
                    foodType: "veg",
                    quantity: 1,
                    isPrepared: true,
                },
                {
                    displayName: "Farmhouse Extraveganza Veggie",
                    foodType: "veg",
                    quantity: 3,
                    customization: [
                        {
                            displayName: "Extra cheese",
                        },
                        {
                            displayName: "Extra veggies",
                        },
                    ],
                },
                {
                    displayName: "Mexican Green Wave",
                    foodType: "veg",
                    quantity: 2,
                },
                {
                    displayName: "Italian Pesto Pasta",
                    foodType: "veg",
                    quantity: 2,
                },
            ],
            timeElapsed: [
                {
                    mins: 15,
                },
                {
                    seconds: 16,
                },
            ],
        },
        {
            orderingMode: "Dine-In order",
            orderLabel: "#BBQA",
            isBillPaid: false,
            dishServed: 3,
            tableServed: "(T1)",
            items: [
                {
                    displayName: "Double Cheese Margherita Pizza",
                    foodType: "veg",
                    quantity: 2,
                    customization: [
                        {
                            displayName: "Extra cheese",
                        },
                        {
                            displayName: "Extra veggies",
                        },
                    ],
                },
                {
                    displayName: "Farmhouse",
                    foodType: "veg",
                    quantity: 1,
                    isPrepared: true,
                    customization: [
                        {
                            displayName: "Extra cheese",
                        },
                        {
                            displayName: "Extra veggies",
                        },
                    ],
                },
                {
                    displayName: "Indi Tandoori Paneer",
                    foodType: "veg",
                    quantity: 2,
                    customization: [
                        {
                            displayName: "Extra cheese",
                        },
                        {
                            displayName: "Extra veggies",
                        },
                    ],
                },
            ],
            timeElapsed: [
                {
                    mins: 10,
                },
                {
                    seconds: 10,
                },
            ],
        },
        {
            orderingMode: "Dine-In order",
            orderLabel: "#BBQA",
            isBillPaid: true,
            dishServed: 3,
            tableServed: "(T1)",
            items: [
                {
                    displayName: "Double Cheese Margherita Pizza",
                    foodType: "veg",
                    quantity: 2,
                    addons: "Keep the pizza loaded with ample amount of cheese",
                    customization: [
                        {
                            displayName: "Extra cheese",
                        },
                        {
                            displayName: "Extra veggies",
                        },
                    ],
                },
            ],
            timeElapsed: [
                {
                    mins: 5,
                },
                {
                    seconds: 10,
                },
            ],
        },
        {
            orderingMode: "Dine-In order",
            orderLabel: "#BBQA",
            isBillPaid: true,
            dishPacked: 7,
            isUpdated: true,
            items: [
                {
                    displayName: "Double Cheese Margherita Pizza",
                    foodType: "veg",
                    quantity: 2,
                },
                {
                    displayName: "Farmhouse",
                    foodType: "veg",
                    quantity: 1,
                    updatedQuantity: 3,
                },
                {
                    displayName: "Indi Tandoori Paneer",
                    foodType: "veg",
                    quantity: 1,
                },
            ],
            timeElapsed: [
                {
                    mins: 3,
                },
                {
                    seconds: 10,
                },
            ],
        },
        {
            icon: <ZomatoIcon />,
            orderingMode: "Delivery order",
            orderLabel: "#BBQA",
            isBillPaid: true,
            isUpdated: true,
            items: [
                {
                    displayName: "Double Cheese Margherita Pizza",
                    foodType: "veg",
                    quantity: 2,
                },
                {
                    displayName: "Farmhouse",
                    foodType: "veg",
                    quantity: 1,
                    updatedQuantity: 3,
                },
                {
                    displayName: "Indi Tandoori Paneer",
                    foodType: "veg",
                    quantity: 1,
                    updatedQuantity: 3,
                },
            ],
            timeElapsed: [
                {
                    mins: 3,
                },
                {
                    seconds: 10,
                },
            ],
        },
        {
            orderingMode: "Dine-In order",
            orderLabel: "#BBQA",
            isUpdated: true,
            isBillPaid: true,
            items: [
                {
                    displayName: "Double Cheese Margherita Pizza",
                    foodType: "veg",
                    quantity: 2,
                },
                {
                    displayName: "Farmhouse",
                    foodType: "veg",
                    quantity: 1,
                    updatedQuantity: 3,
                },
                {
                    displayName: "Indi Tandoori Paneer",
                    foodType: "veg",
                    quantity: 1,
                    updatedQuantity: 3,
                },
            ],
            timeElapsed: [
                {
                    mins: 3,
                },
                {
                    seconds: 10,
                },
            ],
        },
        {
            icon: <SwiggyIcon height={48} width={48} />,
            orderingMode: "Delivery order",
            orderLabel: "#BBQA",
            isBillPaid: true,
            isUpdated: true,
            items: [
                {
                    displayName: "Double Cheese Margherita Pizza",
                    foodType: "veg",
                    quantity: 2,
                },
                {
                    displayName: "Farmhouse",
                    foodType: "veg",
                    quantity: 1,
                    updatedQuantity: 3,
                },
                {
                    displayName: "Indi Tandoori Paneer",
                    foodType: "veg",
                    quantity: 1,
                    updatedQuantity: 3,
                },
            ],
            timeElapsed: [
                {
                    mins: 3,
                },
                {
                    seconds: 10,
                },
            ],
        },
        {
            orderingMode: "Dine-In order",
            orderLabel: "#BBQA",
            isUpdated: true,
            isBillPaid: true,
            items: [
                {
                    displayName: "Double Cheese Margherita Pizza",
                    foodType: "veg",
                    quantity: 2,
                },
                {
                    displayName: "Farmhouse",
                    foodType: "veg",
                    quantity: 1,
                    updatedQuantity: 3,
                },
                {
                    displayName: "Indi Tandoori Paneer",
                    foodType: "veg",
                    quantity: 1,
                },
            ],
            timeElapsed: [
                {
                    mins: 3,
                },
                {
                    seconds: 10,
                },
            ],
        },
    ];

    const [showAddPaymentPopup, setShowAddPaymentPopup] = useState(false);

    const handleClickAddPayment = () => {
        setShowAddPaymentPopup(!showAddPaymentPopup);
    };

    const [kotView, setKotView] = useState(true);

    const handleClickView = (value) => {
        setKotView(value);
    };

    const liveOrdersDetail = [
        {
            modeCustomerName: <ModeAndCustomerName modeBg="bg-primary-500" modeIcon={<DineIn stroke="#ffffff" />} customerName="Sarthak Kanchan" />,
            orderAge: "10 mins ago",
            orderAmount: "₹559.00/-",
            orderStatus: <OrderStatus orderStatusStyle="text-primary-500 bg-primary-50 border-primary-500" OrderStatusLabel="Pending" />,
        },
        {
            modeCustomerName: <ModeAndCustomerName modeBg="bg-primary-500" modeIcon={<DineIn stroke="#ffffff" />} customerName="Arjun patel" />,
            orderAge: "15 mins ago",
            orderAmount: "₹759.00/-",
            orderStatus: <OrderStatus orderStatusStyle="text-primary-500 bg-primary-50 border-primary-500" OrderStatusLabel="Pending" />,
        },
        {
            modeCustomerName: <ModeAndCustomerName modeBg="bg-secondary-700" modeIcon={<DineIn stroke="#ffffff" />} customerName="Bhrugen Joshi" />,
            orderAge: "2 hrs 15 mins",
            orderAmount: "₹1,789.00/-",
            orderStatus: <OrderStatus orderStatusStyle="text-primary-500 bg-primary-50 border-primary-500" OrderStatusLabel="Pending" />,
        },
        {
            modeCustomerName: <ModeAndCustomerName modeBg="bg-secondary-700" modeIcon={<DineIn stroke="#ffffff" />} customerName="Bhargav Joshi" />,
            orderAge: "4 hrs 20 mins",
            orderAmount: "₹123.00/-",
            orderStatus: <OrderStatus orderStatusStyle="text-secondary-800 bg-secondary-50 border-secondary-800" OrderStatusLabel="Preparing" />,
        },
        {
            modeCustomerName: <ModeAndCustomerName modeBg="bg-tertiary-800" modeIcon={<DineIn stroke="#ffffff" />} customerName="Ananya Panday" />,
            orderAge: "5 hrs 15 mins",
            orderAmount: "₹5,238.00/-",
            orderStatus: <OrderStatus orderStatusStyle="text-secondary-800 bg-secondary-50 border-secondary-800" OrderStatusLabel="Preparing" />,
        },
        {
            modeCustomerName: <ModeAndCustomerName modeBg="bg-tertiary-800" modeIcon={<DineIn stroke="#ffffff" />} customerName="Kanneganti Brahmanandam" />,
            orderAge: "5 hrs 34 mins",
            orderAmount: "₹3,559.00/-",
            orderStatus: <OrderStatus orderStatusStyle="text-tertiary-800 bg-tertiary-50 border-tertiary-800" OrderStatusLabel="Prepared" />,
        },
        {
            modeCustomerName: <ModeAndCustomerName modeBg="bg-tertiary-800" modeIcon={<DineIn stroke="#ffffff" />} customerName="Amrendra Bahubali" />,
            orderAge: "6 hrs 18 mins",
            orderAmount: "₹1,559.00/-",
            orderStatus: <OrderStatus orderStatusStyle="text-primary-500 bg-primary-50 border-primary-500" OrderStatusLabel="Pending" />,
        },
        {
            modeCustomerName: <ModeAndCustomerName modeBg="bg-tertiary-800" modeIcon={<DineIn stroke="#ffffff" />} customerName="Riddhi Shah" />,
            orderAge: "1 day",
            orderAmount: "₹749.00/-",
            orderStatus: <OrderStatus orderStatusStyle="text-tertiary-800 bg-tertiary-50 border-tertiary-800" OrderStatusLabel="Prepared" />,
        },
        {
            modeCustomerName: <ModeAndCustomerName modeBg="bg-tertiary-800" modeIcon={<DineIn stroke="#ffffff" />} customerName="Shreya Khanna" />,
            orderAge: "6 hrs 18 mins",
            orderAmount: "₹1,559.00/-",
            orderStatus: <OrderStatus orderStatusStyle="text-tertiary-800 bg-tertiary-50 border-tertiary-800" OrderStatusLabel="Prepared" />,
        },
    ];

    const orderDetails = [
        {
            icon: <SwiggyIcon height={40} width={40} />,
            orderingMode: "Order number",
            orderLabel: "#BBQR",
            items: [
                {
                    displayName: "Double Cheese Margherita Pizza",
                    note: "Keep the pizza loaded with ample amount of cheese",
                    foodType: "veg",
                    quantity: 2,
                    addons: "Keep the pizza loaded with ample amount of cheese",
                    customization: [
                        {
                            displayName: "Extra cheese",
                        },
                        {
                            displayName: "Extra veggies",
                        },
                    ],
                },
                {
                    displayName: "Farmhouse Extraveganza Veggie",
                    foodType: "veg",
                    quantity: 3,
                    customization: [
                        {
                            displayName: "Extra cheese",
                        },
                        {
                            displayName: "Extra veggies",
                        },
                    ],
                },
                {
                    displayName: "Mexican Green Wave",
                    foodType: "veg",
                    quantity: 2,
                },
                {
                    displayName: "Italian Pesto Pasta",
                    foodType: "veg",
                    quantity: 2,
                },
            ],
            timeElapsed: [
                {
                    mins: 15,
                },
                {
                    seconds: 16,
                },
            ],
        },
    ];

    const pastOrdersDetails = [
        {
            customerName: "Riddhi Shah",
            orderDate: "18 Nov 2022",
            orderType: <OrdersIcon icon={<DineIn />} label="Dine-In" />,
            ordersStatusMobile: <OrdersStatusMobileIcons iconSuccess={<SuccessTick />} />,
            ordersStatus: <OrdersIcon icon={<SuccessTick />} label="Delivered" />,
            itemsOrdered: "12",
            orderAmount: "₹5,325.00/-",
            paymentMode: <OrdersIcon icon={<Cash />} label="Cash" />,
        },
        {
            customerName: "Amrendra Bahubali",
            orderDate: "13 Nov 2022",
            orderType: <OrdersIcon icon={<DineIn />} label="Dine-In" />,
            ordersStatusMobile: <OrdersStatusMobileIcons iconSuccess={<Cancel />} iconClose={<Cancel />} />,
            ordersStatus: <OrdersIcons iconSuccess={<Cancel />} labelSuccess="Refund failed" iconClose={<Cancel />} labelClose="Cancelled" />,
            itemsOrdered: "05",
            orderAmount: "₹235.00/-",
            paymentMode: <OrdersIcon icon={<Card />} label="Debit card" />,
        },
        {
            customerName: "Jay Sharma",
            orderDate: "05 Nov 2022",
            orderType: <OrdersIcon icon={<OrderIcon />} label="Takeaway" />,
            ordersStatusMobile: <OrdersStatusMobileIcons iconSuccess={<SuccessTick />} />,
            ordersStatus: <OrdersIcon icon={<SuccessTick />} label="Delivered" />,
            itemsOrdered: "03",
            orderAmount: "₹120.00/-",
            paymentMode: <OrdersIcon icon={<Cash />} label="Cash" />,
        },
        {
            customerName: "Rashi Agrawal",
            orderDate: "04 Nov 2022",
            orderType: <OrdersIcon icon={<Riders />} label="Delivery" />,
            ordersStatusMobile: <OrdersStatusMobileIcons iconSuccess={<SuccessTick />} />,
            ordersStatus: <OrdersIcon icon={<SuccessTick />} label="Delivered" />,
            itemsOrdered: " 01",
            orderAmount: "₹99.00/-",
            paymentMode: <OrdersIcon icon={<Card />} label="Credit card" />,
        },
        {
            customerName: "Shraddha Kapoor",
            orderDate: "04 Nov 2022",
            orderType: <OrdersIcon icon={<DineIn />} label="Dine-In" />,
            ordersStatusMobile: <OrdersStatusMobileIcons iconClose={<Cancel />} />,
            ordersStatus: <OrdersIcon icon={<Cancel />} label="Cancelled" />,
            itemsOrdered: "17",
            orderAmount: "₹2,559.00/-",
            paymentMode: <OrdersIcon icon={<UpiIcon />} label="UPI" />,
        },
        {
            customerName: "Shreya Khanna",
            orderDate: "01 Nov 2022",
            orderType: <OrdersIcon icon={<Riders />} label="Delivery" />,
            ordersStatusMobile: <OrdersStatusMobileIcons iconSuccess={<SuccessTick />} />,
            ordersStatus: <OrdersIcon icon={<SuccessTick />} label="Delivered" />,
            itemsOrdered: "09",
            orderAmount: "₹1,759.00/-",
            paymentMode: <OrdersIcon icon={<UpiIcon />} label="UPI" />,
        },
    ];

    const failedOrdersDetails = [
        {
            customerName: "Riddhi Shah",
            orderDate: "18 Nov 2022",
            orderType: <OrdersIcon icon={<Riders />} label="Dine-In" />,
            ordersStatusMobile: <OrdersStatusMobileIcons iconClose={<Cancel />} iconSuccess={<SuccessTick />} />,
            ordersStatus: <OrdersIcons iconClose={<Cancel />} labelClose="Failed order" iconSuccess={<SuccessTick />} labelSuccess="Refund success" />,
            itemsOrdered: "12",
            orderAmount: "₹5,325.00/-",
            paymentMode: <OrdersIcon icon={<UpiIcon />} label="UPI" />,
        },
        {
            customerName: "Amrendra Bahubali",
            orderDate: "13 Nov 2022",
            orderType: <OrdersIcon icon={<Riders />} label="Dine-In" />,
            ordersStatusMobile: <OrdersStatusMobileIcons iconClose={<Cancel />} iconSuccess={<SuccessTick />} />,
            ordersStatus: <OrdersIcons iconClose={<Cancel />} labelClose="Failed order" iconSuccess={<SuccessTick />} labelSuccess="Refund success" />,
            itemsOrdered: "05",
            orderAmount: "₹235.00/-",
            paymentMode: <OrdersIcon icon={<UpiIcon />} label="UPI" />,
        },
        {
            customerName: "Jay Sharma",
            orderDate: "05 Nov 2022",
            orderType: <OrdersIcon icon={<OrderIcon />} label="Takeaway" />,
            ordersStatusMobile: <OrdersStatusMobileIcons iconClose={<Cancel />} iconSuccess={<Reimbursement />} />,
            ordersStatus: <OrdersIcons iconClose={<Cancel />} labelClose="Failed order" iconSuccess={<Reimbursement />} labelSuccess="Refund initiated" />,
            itemsOrdered: "03",
            orderAmount: "₹120.00/-",
            paymentMode: <OrdersIcon icon={<UpiIcon />} label="UPI" />,
        },
        {
            customerName: "Rashi Agrawal",
            orderDate: "04 Nov 2022",
            orderType: <OrdersIcon icon={<Riders />} label="Delivery" />,
            ordersStatusMobile: <OrdersStatusMobileIcons iconClose={<Cancel />} iconSuccess={<SuccessTick />} />,
            ordersStatus: <OrdersIcons iconClose={<Cancel />} labelClose="Failed order" iconSuccess={<SuccessTick />} labelSuccess="Refund success" />,
            itemsOrdered: " 01",
            orderAmount: "₹99.00/-",
            paymentMode: <OrdersIcon icon={<UpiIcon />} label="UPI" />,
        },
        {
            customerName: "Shraddha Kapoor",
            orderDate: "04 Nov 2022",
            orderType: <OrdersIcon icon={<Riders />} label="Dine-In" />,
            ordersStatusMobile: <OrdersStatusMobileIcons iconClose={<Cancel />} iconSuccess={<Reimbursement />} />,
            ordersStatus: <OrdersIcons iconClose={<Cancel />} labelClose="Failed order" iconSuccess={<Reimbursement />} labelSuccess="Refund initiated" />,
            itemsOrdered: "17",
            orderAmount: "₹2,559.00/-",
            paymentMode: <OrdersIcon icon={<UpiIcon />} label="UPI" />,
        },
        {
            customerName: "Shreya Khanna",
            orderDate: "01 Nov 2022",
            orderType: <OrdersIcon icon={<Riders />} label="Delivery" />,
            ordersStatusMobile: <OrdersStatusMobileIcons iconClose={<Cancel />} iconSuccess={<SuccessTick />} />,
            ordersStatus: <OrdersIcons iconClose={<Cancel />} labelClose="Failed order" iconSuccess={<SuccessTick />} labelSuccess="Refund success" />,
            itemsOrdered: "09",
            orderAmount: "₹1,759.00/-",
            paymentMode: <OrdersIcon icon={<UpiIcon />} label="UPI" />,
        },
    ];

    const [showDeliveryPopup, setShowDeliveryPopup] = useState(false);

    const handleClickAddDelivery = () => {
        setShowDeliveryPopup(!showDeliveryPopup);
    };

    const [showTakeAwayPopup, setShowTakeAwayPopup] = useState(false);

    const handleClickTakeAway = () => {
        setShowTakeAwayPopup(!showTakeAwayPopup);
    };

    const [showCustomerPastOrderDetails, setShowCustomerPastOrderDetails] = useState(false);

    const handleClickCustomerPastOrderDetails = () => {
        setShowCustomerPastOrderDetails(!showCustomerPastOrderDetails);
    };

    const [showCustomerFailedOrderDetails, setShowCustomerFailedOrderDetails] = useState(false);

    const handleClickCustomerFailedOrderDetails = () => {
        setShowCustomerFailedOrderDetails(!showCustomerFailedOrderDetails);
    };

    const [showOrdersDetails, setShowOrdersDetails] = useState(false);
    const handleClickOrdersDetails = () => {
        setShowOrdersDetails(!showOrdersDetails);
    };

    const [isActiveLiveOrders, setLiveOrders] = useState(true);
    const [isActivePastOrder, setPastOrder] = useState(false);
    const [isAcitveFailedOrder, setFailedOrder] = useState(false);

    const handleClickLiveOrders = () => {
        setFailedOrder(false);
        setPastOrder(false);
        setLiveOrders(true);
    };
    const handleClickPastOrder = () => {
        setLiveOrders(false);
        setFailedOrder(false);
        setPastOrder(true);
    };
    const handleClickFailedOrder = () => {
        setPastOrder(false);
        setLiveOrders(false);
        setFailedOrder(true);
    };

    const navigate = useNavigate();

    const handleClickDineIn = () => {
        navigate("/pos-delivery");
    };

    const orderTypeMenu = useRef(null);
    const handleClickOrderType = () => {
        orderTypeMenu.current.classList.toggle("menuOrderType");
    };
    return (
        <>
            <div className="bg-[#fafafa]">
                <div
                    className={`${
                        (showCustomerPastOrderDetails || showCustomerFailedOrderDetails || showOrdersDetails || showAddPaymentPopup || showDeliveryPopup || showTakeAwayPopup) && "md:hidden"
                    }`}
                >
                    <div className="px-8 pb-[70px] pageContentSmall:px-4 lg:pb-[23px] pt-4 w-full max-w-[1336px] mx-auto bg-white">
                        <div className="flex flex-row justify-between mb-4 pb-4 border-b border-neutral-300">
                            {/* <div className="flex flex-row items-center">
                                    {orderStatus.map((el, index) => (
                                        <div className="mr-4 xl:mr-2 cursor-pointer" onClick={() => handleClickOrderTab(index)} key={index}>
                                            <TabBig IconDefault={el.iconDefault} label={el.label} IconClick={el.iconClick} isActive={index === activeOrderTab} />
                                        </div>
                                    ))}
                                </div> */}
                            <div className="flex flex-row lg:hidden">
                                <div className="w-[148px] md:w-24 cursor-pointer" onClick={handleClickLiveOrders}>
                                    <TabBig label="Live orders" IconDefault={<LiveOrderIcon />} IconClick={<LiveOrderIcon stroke="#6C5DD3" />} isActive={isActiveLiveOrders} />
                                </div>
                                <div className="ml-4 xl:ml-2 w-[151px] md:w-24 cursor-pointer" onClick={handleClickPastOrder}>
                                    <TabBig label="Past orders" IconDefault={<PastOrderIcon />} IconClick={<PastOrderIcon stroke="#6C5DD3" />} isActive={isActivePastOrder} />
                                </div>
                                <div className="ml-4 xl:ml-2 w-[163px] md:w-24 cursor-pointer" onClick={handleClickFailedOrder}>
                                    <TabBig label="Failed orders" IconDefault={<FailedOrderIcon />} IconClick={<FailedOrderIcon stroke="#6C5DD3" />} isActive={isAcitveFailedOrder} />
                                </div>
                            </div>

                            <div className="hidden lg:block w-full max-w-[213px] md:max-w-full">
                                <DropDownTabs
                                    menuItems={[
                                        { item: "Live orders", onClick: () => handleClickLiveOrders() },
                                        { item: "Past orders", onClick: () => handleClickPastOrder() },
                                        { item: "Failed orders", onClick: () => handleClickFailedOrder() },
                                    ]}
                                />
                            </div>
                            <div className="md:hidden">
                                <div className={isActiveLiveOrders === true ? "block" : "hidden"}>
                                    <div className="flex flex-row">
                                        {orderTypes.map((el, index) => (
                                            <div
                                                className="ml-4 xl:ml-2 cursor-pointer"
                                                key={index}
                                                onClick={index === 2 ? handleClickAddDelivery : index === 1 ? handleClickTakeAway : index === 0 && handleClickDineIn}
                                            >
                                                <LargePrimaryButton name={el.label} leftIconDefault={<AddIcon stroke="#ffffff" />} leftIconClick={<AddIcon stroke="#C4BEED" />} isDefault={false} />
                                            </div>
                                        ))}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className={isActiveLiveOrders === true ? "hidden" : "block"}>
                            <div className="flex md:block justify-between mb-6 lg:mb-4 md:mb-0">
                                <div className="md:mb-4">
                                    <CalenderField label="25 Sept 2022 - 09 Oct 2022" buttonStyle="md:w-full" />
                                </div>

                                <div className="flex justify-between">
                                    <div className="w-[120px] mx-4 lg:mx-2 md:ml-0 md:w-full mobile:mr-1">
                                        <LargePrimaryButton leftIconDefault={<FilterIcon fill="#ffffff" />} leftIconClick={<FilterIcon fill="#C4BEED" />} name="Filters" />
                                    </div>

                                    <div className="w-[161px] md:ml-2 md:w-full mobile:ml-1">
                                        <LargePrimaryButton leftIconDefault={<Export stroke="#ffffff" />} leftIconClick={<Export stroke="#C4BEED" />} name="Export data" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className={isActiveLiveOrders === true ? "block" : "hidden"}>
                            <div className="flex flex-row justify-between md:block">
                                <div className="flex flex-row md:block">
                                    <div className="min-w-[331px] lg:min-w-[250px] mr-4 lg:mr-2 w-full md:min-w-full md:mr-0 md:mb-2">
                                        <DropDownTabs
                                            menuItems={[
                                                { item: "All", badgeText: "P - 6" },
                                                { item: "Dine-in", badgeText: "P - 6" },
                                                { item: "Takeaway", badgeText: "P - 6" },
                                                { item: "Delivery", badgeText: "P - 6" },
                                            ]}
                                            fixedLabel={windowWidth > 1023 ? "Ordering mode " : "O.M."}
                                            menuButtonStyle="border-neutral-300"
                                            badgeTextStyle="bg-neutral-200"
                                            dropDownIconFill="#131126"
                                            boxStyle="border-neutral-300"
                                            textColor="text-neutral-900"
                                        />
                                    </div>
                                    <div className="min-w-[278px] lg:min-w-[213px] w-full">
                                        <DropDownTabs
                                            fixedLabel="Filter"
                                            menuItems={[{ item: "All" }, { item: "Pending" }, { item: "Preparing" }, { item: "Prepared" }, { item: "Dispatched" }]}
                                            boxStyle="border-neutral-300"
                                            textColor="text-neutral-900"
                                            menuButtonStyle="border-neutral-300"
                                            dropDownIconFill="#131126"
                                        />
                                    </div>
                                </div>

                                <div className="min-w-[180px] md:hidden">
                                    <DropDownTabs
                                        menuItems={[
                                            { item: "KOT view", onClick: () => handleClickView(true) },
                                            { item: "List view", onClick: () => handleClickView(false) },
                                        ]}
                                        boxStyle="border-neutral-300"
                                        textColor="text-neutral-900"
                                        dropDownIconFill="#131126"
                                        menuButtonStyle="border-neutral-300"
                                    />
                                </div>
                            </div>
                            <div className="md:hidden">
                                {kotView ? (
                                    <div className={`md:contents -mx-2.5 pageContent:-mx-1 pageContent:pt-2 lg:mx-0`}>
                                        {orders.map((el, index) => (
                                            <div
                                                className={`w-[410px] pageContent:w-[32.6%] xl:w-[49.1%] mx-2.5 pageContent:mx-1 lg:mx-0 pt-4 pageContent:pt-2 inline-block lg:block align-top lg:w-full`}
                                                key={index}
                                            >
                                                <KOT
                                                    KOT={index + 1}
                                                    orderIcon={el.icon}
                                                    isUpdated={el.isUpdated}
                                                    orderLabel={el.orderLabel}
                                                    orderingMode={el.orderingMode}
                                                    timeElapsed={el.timeElapsed}
                                                    items={el.items}
                                                    isBillPaid={el.isBillPaid}
                                                    dishPacked={el.dishPacked}
                                                    dishServed={el.dishServed}
                                                    tableServed={el.tableServed}
                                                    handleClickAddPayment={handleClickAddPayment}
                                                />
                                            </div>
                                        ))}
                                    </div>
                                ) : (
                                    <div className="flex flex-row justify-between mt-6 lg:mt-4">
                                        <div className="w-full pr-5 xl:pr-0">
                                            <div className="w-full border border-neutral-300 rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden">
                                                <table className="w-full break-words">
                                                    <thead>
                                                        <tr className="bg-neutral-50 uppercase paragraphOverlineSmall text-neutral-700">
                                                            <th className="text-left h-11 pl-6 shadow-innerShadow min-w-[273px] 2xl:min-w-[220px]">Mode &amp; customer Name</th>
                                                            <th className="text-left h-11 pl-6 shadow-innerShadow min-w-[144px] 2xl:min-w-[130px]">order age</th>
                                                            <th className="text-left h-11 pl-6 shadow-innerShadow min-w-[163px] 2xl:min-w-[155px]">order amount</th>
                                                            <th className="text-left h-11 pl-6 shadow-innerShadow min-w-[153px] 2xl:min-w-[145px]">order status</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {liveOrdersDetail.map((row, index) => (
                                                            <tr
                                                                key={index}
                                                                className={`even:bg-neutral-50 ${
                                                                    index !== 0 && "border-t"
                                                                } border-neutral-300 paragraphSmallRegular cursor-default hover:bg-primary-100 hover:border-primary-500 hover:border-y hover:last:border-b-0`}
                                                                onClick={handleClickOrdersDetails}
                                                            >
                                                                <td className="h-[70px] pl-6">{row.modeCustomerName}</td>
                                                                <td className="h-[70px] pl-6">{row.orderAge}</td>
                                                                <td className="h-[70px] pl-6">{row.orderAmount}</td>
                                                                <td className="h-[70px] pl-6">{row.orderStatus}</td>
                                                            </tr>
                                                        ))}
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div className="my-4">
                                                <PaginationWithNumber />
                                            </div>
                                        </div>
                                        <div className="min-w-[518px] w-full 2xl:min-w-min 2xl:max-w-[450px] xl:hidden">
                                            {orderDetails.map((el, index) => (
                                                <div key={index}>
                                                    <OrdersCard
                                                        ordersIcon={el.icon}
                                                        isUpdated={el.isUpdated}
                                                        orderLabel={el.orderLabel}
                                                        orderingMode={el.orderingMode}
                                                        timeElapsed={el.timeElapsed}
                                                        items={el.items}
                                                    />
                                                </div>
                                            ))}
                                        </div>
                                    </div>
                                )}
                            </div>
                            <div className="hidden md:block md:mt-4">
                                {liveOrdersDetail.map((row, index) => (
                                    <div
                                        key={index}
                                        className="w-full px-4 mobile:px-3 py-[14px] mobile:py-2.5 mb-2 border border-neutral-300 rounded-md cursor-pointer"
                                        onClick={handleClickOrdersDetails}
                                    >
                                        <div className="flex flex-row items-center justify-between">
                                            <div className="paragraphSmallRegular">{row.modeCustomerName}</div>
                                            <div>{row.orderStatus}</div>
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div>
                        <div className={isActivePastOrder === true ? "block" : "hidden"}>
                            <div className="mb-4">
                                <div className="w-full border border-neutral-300 rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden">
                                    <table className="w-full break-words">
                                        <thead>
                                            <tr className="bg-neutral-50 uppercase paragraphOverlineSmall text-neutral-700">
                                                <th className="text-left h-11 pl-6 min-w-[234px] lg:min-w-[195px] shadow-innerShadow">customer name</th>
                                                <th className="text-left h-11 pl-6 min-w-[174px] shadow-innerShadow">order date</th>
                                                <th className="text-left h-11 pl-6 min-w-[166px] shadow-innerShadow">Order type</th>
                                                <th className="text-left h-11 pl-6 min-w-[167px] shadow-innerShadow">order status</th>
                                                <th className="text-left h-11 pl-6 min-w-[163px] shadow-innerShadow">items ordered</th>
                                                <th className="text-left h-11 pl-6 min-w-[192px] shadow-innerShadow">order amount</th>
                                                <th className="text-left h-11 pl-6 min-w-[176px] shadow-innerShadow">payment mode</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {pastOrdersDetails.map((row, index) => (
                                                <tr key={index} className={`even:bg-neutral-50 ${index !== 0 && "border-t"} border-neutral-300 paragraphSmallRegular`}>
                                                    <td className="py-2 pl-6 h-[70px] cursor-pointer" onClick={handleClickCustomerPastOrderDetails}>
                                                        {row.customerName}
                                                    </td>
                                                    <td className="py-2 pl-6">{row.orderDate}</td>
                                                    <td className="py-2 pl-6">{row.orderType}</td>
                                                    <td className="py-2 pl-6">{row.ordersStatus}</td>
                                                    <td className="py-2 pl-6">{row.itemsOrdered} </td>
                                                    <td className="py-2 pl-6">{row.orderAmount} </td>
                                                    <td className="py-2 pl-6">{row.paymentMode} </td>
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                </div>
                                <div className="hidden md:block mt-3">
                                    {pastOrdersDetails.map((el, index) => {
                                        return (
                                            <div className="mb-1 pt-1" key={index}>
                                                <CustomerPastOrderDetailsDropDown
                                                    customerName={el.customerName}
                                                    ordersStatusMobile={el.ordersStatusMobile}
                                                    orderDate={el.orderDate}
                                                    orderType={el.orderType}
                                                    ordersStatus={el.ordersStatus}
                                                    itemsOrdered={el.itemsOrdered}
                                                    orderAmount={el.orderAmount}
                                                    paymentMode={el.paymentMode}
                                                    handleClickCustomerPastOrderDetails={handleClickCustomerPastOrderDetails}
                                                />
                                            </div>
                                        );
                                    })}
                                </div>
                            </div>
                            <div className="md:hidden">
                                <PaginationWithNumber />
                            </div>
                        </div>

                        <div className={isAcitveFailedOrder === true ? "block" : "hidden"}>
                            <div className="mb-4">
                                <div className="w-full border border-neutral-300 rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden">
                                    <table className="w-full break-words">
                                        <thead>
                                            <tr className="bg-neutral-50 uppercase paragraphOverlineSmall text-neutral-700">
                                                <th className="text-left h-11 pl-6 min-w-[234px] lg:min-w-[195px] shadow-innerShadow">customer name</th>
                                                <th className="text-left h-11 pl-6 min-w-[174px] shadow-innerShadow">order date</th>
                                                <th className="text-left h-11 pl-6 min-w-[166px] shadow-innerShadow">Order type</th>
                                                <th className="text-left h-11 pl-6 min-w-[191px] shadow-innerShadow">order status</th>
                                                <th className="text-left h-11 pl-6 min-w-[163px] shadow-innerShadow">items ordered</th>
                                                <th className="text-left h-11 pl-6 min-w-[162px] shadow-innerShadow">order amount</th>
                                                <th className="text-left h-11 pl-6 min-w-[176px] shadow-innerShadow">payment mode</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {failedOrdersDetails.map((row, index) => (
                                                <tr key={index} className={`even:bg-neutral-50 ${index !== 0 && "border-t"} border-neutral-300 paragraphSmallRegular`}>
                                                    <td className="py-2 pl-6 h-[70px] cursor-pointer" onClick={handleClickCustomerFailedOrderDetails}>
                                                        {row.customerName}
                                                    </td>
                                                    <td className="py-2 pl-6">{row.orderDate}</td>
                                                    <td className="py-2 pl-6">{row.orderType}</td>
                                                    <td className="py-2 pl-6">{row.ordersStatus}</td>
                                                    <td className="py-2 pl-6">{row.itemsOrdered} </td>
                                                    <td className="py-2 pl-6">{row.orderAmount} </td>
                                                    <td className="py-2 pl-6">{row.paymentMode} </td>
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                </div>
                                <div className="hidden md:block mt-3">
                                    {failedOrdersDetails.map((el, index) => {
                                        return (
                                            <div className="mb-1 pt-1" key={index}>
                                                <CustomerFailedOrderDetailsDropDown
                                                    customerName={el.customerName}
                                                    ordersStatusMobile={el.ordersStatusMobile}
                                                    orderDate={el.orderDate}
                                                    orderType={el.orderType}
                                                    ordersStatus={el.ordersStatus}
                                                    itemsOrdered={el.itemsOrdered}
                                                    orderAmount={el.orderAmount}
                                                    paymentMode={el.paymentMode}
                                                    handleClickCustomerFailedOrderDetails={handleClickCustomerFailedOrderDetails}
                                                />
                                            </div>
                                        );
                                    })}
                                </div>
                            </div>
                            <div className="md:hidden">
                                <PaginationWithNumber />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className={`${!showCustomerPastOrderDetails && "hidden"} p-4`}>
                <CustomerPastOrderDetailsPopup handleClickCustomerPastOrderDetails={handleClickCustomerPastOrderDetails} />
            </div>
            <div className={`${!showCustomerFailedOrderDetails && "hidden"} p-4`}>
                <CustomerFailedOrderDetailsPopup handleClickCustomerFailedOrderDetails={handleClickCustomerFailedOrderDetails} />
            </div>
            <div className="hidden xl:block">
                <div className={`${!showOrdersDetails && "hidden"}`}>
                    <OrdersDetailsPopup handleClickOrdersDetails={handleClickOrdersDetails} />
                </div>
            </div>
            <div className={`${!showAddPaymentPopup && "hidden"}`}>
                <AddPaymentPopup handleClickClose={handleClickAddPayment} />
            </div>
            <div className={`${!showDeliveryPopup && "hidden"}`}>
                <DeliveryPopup handleClickClose={handleClickAddDelivery} />
            </div>
            <div className={`${!showTakeAwayPopup && "hidden"}`}>
                <TakeAwayPopup handleClickClose={handleClickTakeAway} />
            </div>
            <div className="hidden md:block" ref={orderTypeMenu}>
                <div className={isActiveLiveOrders === true ? "block" : "hidden"}>
                    <div className="fixed bg-black bg-opacity-70 inset-0 z-[6] menuOrderItems">
                        <div className="flex flex-col justify-end items-end h-full">
                            <div className="orderTypeMobile flex flex-col items-end mr-4 mb-[98px]">
                                {orderTypes.map((el, index) => (
                                    <div className="mb-4" key={index} onClick={index === 2 ? handleClickAddDelivery : index === 1 ? handleClickTakeAway : index === 0 && handleClickDineIn}>
                                        <div>
                                            <LargePrimaryButton
                                                buttonStyle={el.iconMobile}
                                                name={el.label}
                                                leftIconDefault={<AddIcon stroke="#ffffff" />}
                                                leftIconClick={<AddIcon stroke="#C4BEED" />}
                                                isDefault={false}
                                            />
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>
                    <div className="fixed bottom-[42px] right-4 w-14 h-14 rounded-full bg-primary-500 flex shadow-shadowXlarge z-[7] cursor-pointer" onClick={handleClickOrderType}>
                        <span className="m-auto addIcon">
                            <AddIcon width={35} height={35} />
                        </span>
                        <span className="m-auto closeIcon">
                            <IconClose stroke="#fff" width={35} height={35} />
                        </span>
                    </div>
                </div>
            </div>
        </>
    );
}
