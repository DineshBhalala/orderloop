import React, { useEffect, useState, useRef } from "react";
import { useNavigate } from "react-router-dom";
import { LargePrimaryButton } from "../../../Components/Buttons/Button";
import DropDown from "../../../Components/DropDown/DropDown";
import Table from "../../TableManagement/Components/Table";
import ColorDescription from "../../../Components/ColorDescription/ColorDescription";
import { ReactComponent as Redirect } from "../../../Assets/redirect.svg";
import { ReactComponent as AddIcon } from "../../../Assets/add.svg";
import { ReactComponent as IconClose } from "../../../Assets/close.svg";
import TakeAwayPopup from "../LiveOrder/Components/TakeawayPopup";
import DeliveryPopup from "../LiveOrder/Components/DeliveryPopup";
import ReserveTablePopup from "./Components/ReserveTablePopup";
import ReservationsWaitlist from "./Components/ReservationsWaitlist";
import { ReactComponent as LeftArrowIcon } from "../../../Assets/chevron-down.svg";
import { ReactComponent as CloseIcon } from "../../../Assets/close.svg";

export default function PosTableView() {
    const tableLocation = ["All", "Ground floor", "First floor", "Balcony", "Garden", "Banquet Hall"];
    const acTable = [
        { capacity: 6, price: "₹2,325.00/-", time: "15 mins 16 secs" },
        { capacity: 6, price: "₹1,343.00/-", time: "05 mins 16 secs" },
        { capacity: 6 },
        { capacity: 6 },
        { capacity: 6 },
        { capacity: 6 },
        { capacity: 6 },
        { capacity: 6 },
        { capacity: 6 },
        { capacity: 6 },
    ];

    const [windowWidth, setWindowWidth] = useState(window.innerWidth);

    useEffect(() => {
        const handleResize = () => {
            setWindowWidth(window.innerWidth);
        };

        window.addEventListener("resize", handleResize);

        return () => {
            window.removeEventListener("resize", handleResize);
        };
    }, []);


    const [showReservationDetails, setShowReservationDetails] = useState(false);

    const handleClickReservation = () => {
        setShowReservationDetails(!showReservationDetails);
    };

    const [showTakeAwayPopup, setShowTakeAwayPopup] = useState(false);
    const handleClickTakeAway = () => {
        setShowTakeAwayPopup(!showTakeAwayPopup);
    };
    const [showDeliveryPopup, setShowDeliveryPopup] = useState(false);
    const handleClickAddDelivery = () => {
        setShowDeliveryPopup(!showDeliveryPopup);
    };

    const [showReservationDetailsPopup, setShowReservationDetailsPopup] = useState(false);
    const handleClickReservationDetails = () => {
        setShowReservationDetailsPopup(!showReservationDetailsPopup);
    };

    const navigate = useNavigate();
    const handleClickTableManagement = () => {
        navigate("/table-management");
    };
    const handleClickBillPayments = () => {
        navigate("/bill-payments");
    };

    const orderTypeMenu = useRef(null);
    const handleClickOrderType = () => {
        orderTypeMenu.current.classList.toggle("menuOrderType");
    };

    return (
        <>
            <div className="bg-[#fafafa]">
                <div className={`${(showDeliveryPopup || showTakeAwayPopup || showReservationDetailsPopup || showReservationDetails) && "md:hidden"}`}>
                    <div className="px-8 pb-6 lg:px-4 lg:pb-[23px] pt-4 w-full max-w-[1336px] mx-auto bg-white">
                        <div className="mb-2">
                            <div className="flex flex-row justify-between pb-4 border-b border-neutral-300 md:border-none md:pb-0">
                                <div className="flex flex-row md:hidden">
                                    <div className="mr-4 lg:hidden" onClick={handleClickTableManagement}>
                                        <LargePrimaryButton name="Table management" leftIconDefault={<Redirect fill="#FFFFFF" />} leftIconClick={<Redirect fill="#C4BEED" />} />
                                    </div>
                                    <div onClick={handleClickBillPayments}>
                                        <LargePrimaryButton name="Bill payments" leftIconDefault={<Redirect fill="#FFFFFF" />} leftIconClick={<Redirect fill="#C4BEED" />} />
                                    </div>
                                </div>
                                <div className="flex flex-row md:hidden">
                                    <div onClick={handleClickTakeAway}>
                                        <LargePrimaryButton name="Takeaway" leftIconDefault={<AddIcon />} leftIconClick={<AddIcon stroke="#C4BEED" />} />
                                    </div>
                                    <div className="ml-4 lg:ml-2" onClick={handleClickAddDelivery}>
                                        <LargePrimaryButton name="Delivery" leftIconDefault={<AddIcon />} leftIconClick={<AddIcon stroke="#C4BEED" />} />
                                    </div>
                                </div>
                            </div>

                            <div className="flex flex-row">
                                <div className="mt-4 md:mt-0">
                                    <div className="flex flex-row-reverse justify-between mb-6 md:block">
                                        <div className="flex md:border-b md:border-neutral-300 md:pb-4 md:mb-4">
                                            <div onClick={handleClickReservationDetails} className="md:w-1/2">
                                                <LargePrimaryButton
                                                    name={windowWidth > 1023 ? "Reserve table" : "Reserve / Waitlist"}
                                                    leftIconDefault={<AddIcon />}
                                                    leftIconClick={<AddIcon stroke="#C4BEED" />}
                                                    isDefault={false}
                                                    hideName="sm:text-sm mobile:text-xs"
                                                />
                                            </div>
                                            <div className="ml-4 lg:hidden">
                                                <LargePrimaryButton name="Waitlist" leftIconDefault={<AddIcon />} leftIconClick={<AddIcon stroke="#C4BEED" />} />
                                            </div>
                                            <div className="ml-4 lg:ml-2 md:w-1/2" onClick={handleClickReservation}>
                                                <LargePrimaryButton name="Reservations / waitlist" hideName="sm:text-sm mobile:text-xs" />
                                            </div>
                                        </div>
                                        <div className="w-full max-w-[278px] mr-2 md:mr-0 md:max-w-full">
                                            <DropDown menuItems={tableLocation} />
                                        </div>
                                    </div>
                                    <div>
                                        <div className="mb-4 paragraphLargeMedium">Ground Floor</div>
                                        <div className="paragraphLargeItalic text-neutral-500">AC</div>

                                        <div className="-mx-2.5 sm:text-center">
                                            {acTable.map((el, index) => (
                                                <Table
                                                    key={index}
                                                    tableAllocation={index < 2 && "seated"}
                                                    tableName={"T" + (index + 1)}
                                                    maxCapacity={el.capacity}
                                                    time={el.time}
                                                    price={el.price}
                                                />
                                            ))}
                                        </div>

                                        <div className="mt-2 paragraphLargeItalic text-neutral-500">Non-AC</div>

                                        <div className="-mx-2.5 sm:text-center">
                                            {[...Array(4)].map((_, index) => (
                                                <Table key={index} tableAllocation={index === 3 && "disabled"} tableName={"T1" + (index + 1)} maxCapacity={6} />
                                            ))}
                                        </div>

                                        <div className="mb-4 mt-2 paragraphLargeMedium">First Floor</div>
                                        <div className="paragraphLargeItalic text-neutral-500">AC</div>

                                        <div className="-mx-2.5 sm:text-center">
                                            {acTable.map((el, index) => (
                                                <Table
                                                    key={index}
                                                    tableAllocation={index < 2 && "seated"}
                                                    tableName={"T" + (index + 1)}
                                                    maxCapacity={el.capacity}
                                                    time={el.time}
                                                    price={el.price}
                                                />
                                            ))}
                                        </div>
                                    </div>
                                </div>
                                <div className={`xl:hidden max-w-[412px] pageContent:min-w-max w-full pl-8 pt-6 border-l border-neutral-300 ml-5 ${!showReservationDetails && "hidden"}`}>
                                    <div className="w-full h-screen">
                                        <ReservationsWaitlist />
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className="sticky bottom-6 z-40 md:hidden">
                            <div className="border border-neutral-300 shadow-shadowMedium px-4 py-3 rounded-lg flex-row flex bg-white w-max">
                                <ColorDescription border="border-neutral-300" label={windowWidth < 1023 ? "Empty | 25" : "Empty seats | 25"} marginR="mr-10" />
                                <ColorDescription border="border-primary-300" bgColor="bg-primary-50" label={windowWidth < 1023 ? "Empty | 2" : "Empty seats | 2"} marginR="mr-10" />
                                <ColorDescription border="border-secondary-500" bgColor="bg-secondary-50" label={windowWidth < 1023 ? "Reserved | 1" : "Reserved seats | 1"} marginR="mr-10" />
                                <ColorDescription border="border-neutral-300" bgColor="bg-neutral-200" label={windowWidth < 1023 ? "Disabled | 2" : "Disabled seats | 2"} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="hidden xl:block">
                <div className={`fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 flex justify-center items-center overflow-auto md:bg-white md:absolute p-4 md:pt-28 ${!showReservationDetails && "hidden"}`}>
                    <div className="max-w-[443px] w-full rounded-xl md:rounded-none bg-shades-50 px-8 pt-6 pb-2 md:px-0 md:pt-4 m-auto md:max-w-full relative">
                        <div onClick={() => setShowReservationDetails()} className="md:hidden cursor-pointer absolute top-[37px] right-[33px]">
                            <CloseIcon />
                        </div>
                        <div onClick={() => setShowReservationDetails()} className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer">
                            <LeftArrowIcon className="rotate-90" />
                            <span className="paragraphMediumMedium pl-1">Back to table view</span>
                        </div>
                        <ReservationsWaitlist />
                    </div>
                </div>
            </div>
            <div className={`${!showTakeAwayPopup && "hidden"}`}>
                <TakeAwayPopup handleClickClose={handleClickTakeAway} />
            </div>
            <div className={`${!showDeliveryPopup && "hidden"}`}>
                <DeliveryPopup handleClickClose={handleClickAddDelivery} />
            </div>
            <div className={`${!showReservationDetailsPopup && "hidden"}`}>
                <ReserveTablePopup handleClickClose={handleClickReservationDetails} />
            </div>

            <div className="hidden md:block" ref={orderTypeMenu}>
                <div className="fixed bg-black bg-opacity-70 inset-0 z-[6] menuOrderItems">
                    <div className="flex flex-col justify-end items-end h-full">
                        <div className="orderTypeMobile flex flex-col items-end mr-4 mb-[98px]">
                            <div className="mb-4" onClick={handleClickTakeAway}>
                                <LargePrimaryButton buttonStyle="takeAwayIcon" name="Takeaway" leftIconDefault={<AddIcon />} leftIconClick={<AddIcon stroke="#C4BEED" />} />
                            </div>
                            <div className="mb-4" onClick={handleClickAddDelivery}>
                                <LargePrimaryButton buttonStyle="Delivery" name="Delivery" leftIconDefault={<AddIcon />} leftIconClick={<AddIcon stroke="#C4BEED" />} />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="fixed bottom-[42px] right-4 w-14 h-14 rounded-full bg-primary-500 flex shadow-shadowXlarge z-[7] cursor-pointer" onClick={handleClickOrderType}>
                    <span className="m-auto addIcon"><AddIcon width={35} height={35} /></span>
                    <span className="m-auto closeIcon"><IconClose stroke="#fff" width={35} height={35} /></span>
                </div>
            </div>
        </>
    );
}
