import React, { useState } from "react";
import { DefaultInputField } from "../../../../Components/InputField/InputField";
import { Tab } from "../../../../Components/Tabs/Tabs";
import { ReactComponent as SearchIcon } from "../../../../Assets/search.svg";
import { ReactComponent as CalenderIcon } from "../../../../Assets/calendar.svg";
import { ReactComponent as DropdownIcon } from "../../../../Assets/chevron-down.svg";
import ReservationCard from "./ReservationCard";


const ReservationsWaitlist = () => {

    const [activeReservationTav, setActiveReservationTav] = useState("All");

    const handleClickReservationTab = (item) => {
        setActiveReservationTav(item);
    };

    const reservationList = [
        { time: "7:30 PM", reservationType: "Reservation", reservationStatus: "Not confirmed", name: "Mr. Arjun Patel", mobileNumber: "(+91) 9876543231", guestNumber: 12 },
        { time: "7:45 PM", reservationType: "Waitlist", reservationStatus: "Confirmed", name: "Ms. Neha Shah", mobileNumber: "(+91) 9876543231", guestNumber: 12 },
        { time: "8:30 PM", reservationType: "Reservation", reservationStatus: "Cancelled", name: "Mr. John Kumar Doe", mobileNumber: "(+91) 9876543231", guestNumber: 4 },
    ];

    const [showReservationList, setShowReservationList] = useState(false);

    const handleClickShowReservationDropDown = () => {
        setShowReservationList(!showReservationList);
    };

    return (
        <>
            <div className="mb-6 xl:pr-[54px] md:pr-0">
                <DefaultInputField placeholder="Search reservation / waitlist" placeholderIcon={<SearchIcon stroke="#D3D2D8" />} />
            </div>

            <div className="mb-6 flex flex-row items-center">
                <div className="mr-4 mobile:mr-2" onClick={() => handleClickReservationTab("All")}>
                    <Tab label="All" isActive={activeReservationTav === "All"} tabStyle="mobile:px-3" />
                </div>

                <div className="mr-4 mobile:mr-2" onClick={() => handleClickReservationTab("waitlist")}>
                    <Tab label="Waitlist" isActive={activeReservationTav === "waitlist"} tabStyle="mobile:px-3" />
                </div>

                <div onClick={() => handleClickReservationTab("reservations")}>
                    <Tab label="Reservations" isActive={activeReservationTav === "reservations"} tabStyle="mobile:px-3" />
                </div>
            </div>
            <div className="h-screen xl:h-auto overflow-auto scrollbarStyle -mx-1 px-1">
                <div className="border-b border-neutral-300 pb-6 mb-6">
                    <div className="flex flex-row justify-between items-center">
                        <div className="flex flex-row items-center">
                            <CalenderIcon />
                            <span className="paragraphLargeRegular ml-4">12th Nov 2022</span>
                        </div>

                        <div className="cursor-pointer" onClick={handleClickShowReservationDropDown}>
                            <DropdownIcon className={`${showReservationList && "rotate-180"}`} />
                        </div>
                    </div>

                    <div className={`${!showReservationList && "hidden"}`}>
                        {reservationList.map((el, index) => (
                            <div className="mt-4" key={index}>
                                <ReservationCard
                                    time={el.time}
                                    reservationType={el.reservationType}
                                    reservationStatus={el.reservationStatus}
                                    name={el.name}
                                    mobileNumber={el.mobileNumber}
                                    guestNumber={el.guestNumber}
                                />
                            </div>
                        ))}
                    </div>
                </div>

                <div className="flex flex-row justify-between items-center border-b border-neutral-300 pb-6 mb-6">
                    <div className="flex flex-row items-center">
                        <CalenderIcon />
                        <span className="paragraphLargeRegular ml-4">14th Nov 2022</span>
                    </div>

                    <div className="">
                        <DropdownIcon className={`${showReservationList && "rotate-180"}`} />
                    </div>
                </div>

                <div className="flex flex-row justify-between items-center border-b border-neutral-300 pb-6 mb-6">
                    <div className="flex flex-row items-center">
                        <CalenderIcon />
                        <span className="paragraphLargeRegular ml-4">15th Nov 2022</span>
                    </div>

                    <div className="">
                        <DropdownIcon className={`${showReservationList && "rotate-180"}`} />
                    </div>
                </div>

                <div className="flex flex-row justify-between items-center border-b border-neutral-300 pb-6 mb-6">
                    <div className="flex flex-row items-center">
                        <CalenderIcon />
                        <span className="paragraphLargeRegular ml-4">16th Nov 2022</span>
                    </div>

                    <div className="">
                        <DropdownIcon className={`${showReservationList && "rotate-180"}`} />
                    </div>
                </div>
            </div>
        </>
    );
};

export default ReservationsWaitlist;
