import React, { useEffect, useState } from "react";
import { ReactComponent as UserIcon } from "../../../../Assets/user.svg";
import { ReactComponent as CallIcon } from "../../../../Assets/call.svg";
import { ReactComponent as CustomersIcon } from "../../../../Assets/customers.svg";
import ReservationStatusDropDown from "./ReservationStatusDropDown";

function ReservationCard(props) {
    const [color, setColor] = useState({ bgColor: "", borderColor: "" });

    const handleChangeReservationStatus = (item) => {
        switch (item) {
            case "Not confirmed":
                setColor({ bgColor: "white", borderColor: "#D3D2D8" });
                break;

            case "Confirmed":
                setColor({ bgColor: "success-50", borderColor: "#22C55E" });
                break;

            case "Cancelled":
                setColor({ bgColor: "destructive-50", borderColor: "#EF4444" });
                break;

            case "Late":
                setColor({ bgColor: "secondary-50", borderColor: "#FFCE73" });
                break;

            case "Seated":
                setColor({ bgColor: "primary-50", borderColor: "#6C5DD3" });
                break;

            default:
                break;
        }
    };

    useEffect(() => {
        handleChangeReservationStatus(props.reservationStatus);
    }, []);

    return (
        <>
            <div className={`border rounded-lg flex flex-row px-4 md:px-3 mobile:px-2 py-2 ${"bg-" + color.bgColor}`} style={{ borderColor: color.borderColor }}>
                <div className="pr-3 mobile:pr-1 mobile:mr-2 border-r border-neutral-300 mr-4 py-2 max-w-[146px] w-full">
                    <div className="flex flex-col mb-3">
                        <span className="paragraphLargeRegular text-black pb-1">{props.time}</span>
                        <span className="paragraphSmallItalic text-neutral-500">{props.reservationType}</span>
                    </div>
                    <div className="flex flex-row items-center">
                        <ReservationStatusDropDown reservationStatus={props.reservationStatus} handleChangeReservationStatus={handleChangeReservationStatus} />
                    </div>
                </div>
                <div className="py-2 w-full">
                    <div className="flex flex-row items-center mb-2">
                        <UserIcon />
                        <span className="paragraphSmallRegular ml-2">{props.name}</span>
                    </div>
                    <div className="flex flex-row items-center mb-2">
                        <CallIcon />
                        <span className="paragraphSmallRegular ml-2">{props.mobileNumber}</span>
                    </div>
                    <div className="flex flex-row justify-between items-end">
                        <div className="flex flex-row items-center">
                            <CustomersIcon />
                            <span className="paragraphSmallRegular ml-2">{props.guestNumber} Guests</span>
                        </div>
                        <div className="flex flex-row mb-1.5">
                            <div className="w-1 h-1 bg-neutral-900 rounded-full" />
                            <div className="w-1 h-1 bg-neutral-900 rounded-full mx-0.5" />
                            <div className="w-1 h-1 bg-neutral-900 rounded-full" />
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default ReservationCard;
