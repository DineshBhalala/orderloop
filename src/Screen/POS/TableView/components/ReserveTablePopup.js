import React from "react";
import { ReactComponent as CloseIcon } from "../../../../Assets/close.svg";
import { ReactComponent as LeftArrowIcon } from "../../../../Assets/chevron-down.svg";
import { ReactComponent as CalenderIcon } from "../../../../Assets/calendar.svg";
import { ReactComponent as TimerIcon } from "../../../../Assets/timer.svg";
import { DefaultInputField, InputArea } from "../../../../Components/InputField/InputField";
import DropDown from "../../../../Components/DropDown/DropDown";
import { LargePrimaryButton } from "../../../../Components/Buttons/Button";

export default function ReserveTablePopup(props) {
    const reservationStatus = ["Not confirmed", "Confirmed", "Cancelled", "Late", "Seated"];
    return (
        <>
            <div className="fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 flex justify-center items-center overflow-auto md:bg-white md:relative px-4">
                <div className="max-w-[830px] w-full rounded-xl md:rounded-none bg-shades-50 px-8 py-6 md:px-0 md:py-4 m-auto md:max-w-full">
                    <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer" onClick={props.handleClickClose}>
                        <LeftArrowIcon className="rotate-90" />
                        <span className="paragraphMediumMedium pl-1">Fire Dishes</span>
                    </div>

                    <div className="flex flex-row justify-between items-center mb-6 md:hidden">
                        <div className="flex flex-col">
                            <span className="paragraphLargeMedium">Reservation #BBQA</span>
                            <span className="paragraphMediumItalic text-neutral-500">Reservation details and status</span>
                        </div>

                        <div className="md:hidden cursor-pointer" onClick={props.handleClickClose}>
                            <CloseIcon />
                        </div>
                    </div>

                    <div className="flex flex-row items-center mb-4">
                        <div className="mr-2 w-1/2">
                            <DefaultInputField label="Customer name" placeholder="Enter customer name" />
                        </div>
                        <div className="ml-2 w-1/2">
                            <DefaultInputField label="Mobile number" placeholder="Enter mobile number" />
                        </div>
                    </div>

                    <div className="flex flex-row items-center mb-4">
                        <div className="w-1/2 mr-2">
                            <DropDown
                                shadow="shadow-shadowXsmall"
                                label="Select date"
                                placeholder="Enter customer name"
                                menuIcon={<CalenderIcon height={24} />}
                                dropdownLabel="Select date"
                                buttonTextColor="neutral-300"
                            />
                        </div>
                        <div className="w-1/2 ml-2">
                            <DropDown
                                shadow="shadow-shadowXsmall"
                                label="Select time"
                                placeholder="Select time"
                                menuIcon={<TimerIcon height={24} />}
                                dropdownLabel="Select date"
                                buttonTextColor="neutral-300"
                            />
                        </div>
                    </div>

                    <div className="flex flex-row items-center mb-4">
                        <div className="w-1/2 mr-2">
                            <DefaultInputField label="Number of guests" placeholder="Enter number of guests" shadow="shadow-shadowXsmall" />
                        </div>
                        <div className="w-1/2 ml-2">
                            <DropDown menuItems={["Casual"]} label="Select occasion" />
                        </div>
                    </div>

                    <div className="flex flex-row items-center mb-4">
                        <div className="w-1/2 mr-2">
                            <DropDown label="Select section" dropdownLabel="Select section" buttonTextColor="neutral-300" />
                        </div>
                        <div className="w-1/2 ml-2">
                            <DropDown dropdownLabel="Select table" buttonTextColor="neutral-300" label="Select table" type="posReservation" />
                        </div>
                    </div>

                    <div className="mb-12">
                        <InputArea label="Special requests" labelMarginB="mb-1" />
                    </div>

                    <div className="flex flex-row items-end">
                        <div className="w-1/2 mr-2">
                            <DropDown label="Reservation status" placeholder="" menuItems={reservationStatus} />
                        </div>
                        <div className="w-1/2 ml-2">
                            <LargePrimaryButton name="Resend payment link" />
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
