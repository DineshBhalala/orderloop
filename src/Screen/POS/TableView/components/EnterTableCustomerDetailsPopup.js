import React from "react";
import { ReactComponent as LeftArrowIcon } from "../../../../Assets/chevron-down.svg";
import { ReactComponent as CloseIcon } from "../../../../Assets/close.svg";
import { ReactComponent as WarningIcon } from "../../../../Assets/warning.svg";
import { DefaultInputField } from "../../../../Components/InputField/InputField";
import { LargePrimaryButton } from "../../../../Components/Buttons/Button";
import { WarningAlert } from "../../../../Components/Alerts/Alerts";

export default function EnterTableCustomerDetailsPopup(props) {
    return (
        <>
            <div className="fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 flex justify-center items-center overflow-auto md:bg-white md:relative px-4">
                <div className="max-w-[475px] w-full rounded-xl md:rounded-none bg-shades-50 px-8 py-6 md:px-0 md:py-4 m-auto md:max-w-full">
                    <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer" onClick={props.handleClickClose}>
                        <LeftArrowIcon className="rotate-90" />
                        <span className="paragraphMediumMedium pl-1">Back to table management</span>
                    </div>

                    <div className="flex flex-row justify-between items-center mb-6 md:hidden">
                        <div className="flex flex-col">
                            <span className="paragraphLargeMedium">{props.tableName}</span>
                            <span className="paragraphMediumItalic text-neutral-500">Maximum Capacity - 6</span>
                        </div>

                        <div className="md:hidden cursor-pointer" onClick={props.handleClickClose}>
                            <CloseIcon />
                        </div>
                    </div>

                    <div className="mb-4">
                        <DefaultInputField label="Mobile number" placeholder="Enter mobile number" shadow="shadow-shadowXsmall" />
                    </div>

                    <div className="mb-4">
                        <DefaultInputField label="Customer name" placeholder="Enter customer name" shadow="shadow-shadowXsmall" />
                    </div>

                    <div className="mb-4">
                        <DefaultInputField label="Number of guests" placeholder="Enter number of guests" shadow="shadow-shadowXsmall" />
                    </div>

                    <div className="mb-12">
                        <WarningAlert
                            iconStyle="-mt-0.5"
                            alertHeader="Number of guests beyond table capacity."
                            headerStyle="paragraphSmallRegular"
                            action1="Merge tables"
                            action2="Choose another table"
                            action3="Ignore"
                            icon={<WarningIcon stroke="#CB8400" />}
                            action1Style="mb-0.5 border-b-[1.5px] border-secondary-800 paragraphSmallRegular"
                            action2Style="mb-0.5 border-b-[1.5px] border-secondary-800 paragraphSmallRegular"
                            action3Style="mb-0.5 border-b-[1.5px] border-secondary-800 paragraphSmallRegular"
                            shadow="shadow-smallDropDownShadow"
                        />
                    </div>

                    <div className="mb-4">
                        <LargePrimaryButton name="Proceed" />
                    </div>

                    <div className="paragraphSmallMedium text-primary-500 text-center cursor-pointer">Skip for now</div>
                </div>
            </div>
        </>
    );
}
