import React, { useState } from "react";

import { ReactComponent as HoldIcon } from "../../Assets/hold.svg";
import { ReactComponent as DininIcon } from "../../Assets/dine-in.svg";
import { ReactComponent as TakeawayIcon } from "../../Assets/orders.svg";
import { ReactComponent as DeliveryIcon } from "../../Assets/riders.svg";

export default function HoldOrder() {
    const holdOrderDetails = [
        { mode: "dine-in", customerName: "Sarthak Kanchan" },
        { mode: "takeaway", customerName: "Bhrugen Joshi" },
        { mode: "delivery", customerName: "Ananya Panday" },
    ];

    const [showHoldOrder, setShowHoldOrder] = useState(false);

    const handleClickHold = () => {
        setShowHoldOrder(!showHoldOrder);
    };

    return (
        <>
            <div className="relative">
                <div
                    className={`border cursor-pointer ${showHoldOrder ? "border-primary-500 bg-primary-100" : "border-neutral-300 bg-neutral-50"} flex flex-row px-1.5 py-2 rounded-md max-w-[70px] cursor-pointer`}
                    onClick={handleClickHold}
                >
                    <HoldIcon stroke={`${showHoldOrder ? "#6C5DD3" : "#706E7E"}`} height={24} width={24} />
                    <span className={`ml-1 paragraphXSmallMedium rounded-3xl ${showHoldOrder ? "bg-primary-500 text-neutral-50" : "text-neutral-700 bg-neutral-200"} px-2.5 py-0.5`}>3</span>
                </div>

                <div className={`pt-3 pb-6 px-[22px] border border-neutral-300 absolute right-0 top-12 bg-white rounded-lg min-w-[391px] shadow-shadowLarge ${!showHoldOrder && "hidden"}`}>
                    <span className="paragraphOverlineSmall">HOLD ORDERS</span>
                    {holdOrderDetails.map((el, index) => (
                        <div className="flex flex-row items-center w-full px-4 py-3 border border-neutral-300 rounded-md justify-between mt-2" key={index}>
                            <div className="flex flex-row">
                                <div className={`p-2 ${el.mode === "takeaway" ? "bg-secondary-700" : el.mode === "dine-in" ? "bg-primary-500" : "bg-tertiary-800"} rounded-lg w-10 h-10 mr-1`}>
                                    {el.mode === "takeaway" ? (
                                        <TakeawayIcon stroke="#FFFFFF" height={24} width={24} />
                                    ) : el.mode === "dine-in" ? (
                                        <DininIcon stroke="#FFFFFF" height={24} width={24} />
                                    ) : (
                                        <DeliveryIcon stroke="#FFFFFF" height={24} width={24} />
                                    )}
                                </div>
                                <div className="">
                                    <div className="paragraphOverlineSmall text-neutral-700 mb-1 -mt-[3px]">MODE & CUSTOMER NAME:</div>
                                    <div className="paragraphSmallRegular">{el.customerName}</div>
                                </div>
                            </div>
                            <span className="paragraphSmallMedium text-neutral-500 px-2 py-1 border border-neutral-500 bg-primary-50 rounded">Hold</span>
                        </div>
                    ))}
                </div>
            </div>
        </>
    );
}
