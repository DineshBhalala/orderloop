import React from "react";
import { ReactComponent as CloseIcon } from "../../../Assets/close.svg";
import { ReactComponent as LeftArrowIcon } from "../../../Assets/chevron-down.svg";

export default function CustomerOrderDetailsPopup(props) {
  return (
    <div className={`fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 flex md:relative px-10 md:px-0 overflow-auto`}>
      <div className="max-w-[652px] w-full rounded-xl bg-shades-50 py-6 m-auto max-h-[969px] md:max-w-full md:rounded-none md:py-4 md:max-h-full">
        <div className="flex flex-row items-center justify-between mb-6 px-8 md:hidden">
          <div>
            <h3 className="paragraphLargeMedium">Customer order details</h3>
            <div className="flex flex-row items-center">
              <p className="paragraphMediumItalic text-neutral-500">Customer order details</p>
            </div>
          </div>
          <span className="cursor-pointer" onClick={props.handleClickClose}>
            <CloseIcon />
          </span>
        </div>

        <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 px-8 md:px-4 cursor-pointer" onClick={() => props.handleClickClose()}>
          <LeftArrowIcon className="rotate-90" />
          <span className="ml-1">Back to cashback</span>
        </div>
      </div>
    </div>
  );
}
