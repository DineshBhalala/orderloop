import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { DefaultBreadcrumbs } from "../../Components/Breadcrumbs/Breadcrumbs";
import { ReactComponent as BillPaymentsIcon } from "../../Assets/bill-payments.svg";
import { ReactComponent as RedirectIcon } from "../../Assets/redirect.svg";
import { ReactComponent as TableManagementIcon } from "../../Assets/table-management.svg";
import { ReactComponent as FilterIcon } from "../../Assets/filter.svg";
import { ReactComponent as ExportIcon } from "../../Assets/export.svg";
import { ReactComponent as CashIcon } from "../../Assets/cash.svg";
import { ReactComponent as UpiIcon } from "../../Assets/UPI.svg";
import { ReactComponent as CardIcon } from "../../Assets/card.svg";
import { LargePrimaryButton } from "../../Components/Buttons/Button";
import CalenderField from "../../Components/Calender/CalenderField";
import PaginationWithNumber from "../../Components/Pagination/PaginationWithNumber";
import CustomerOrderDetails from "../../Components/CustomerOrderDetails/CustomerOrderDetailsPopup";
import ListViewBillPayment from "../../Components/ListView/ListViewBillPayment";

export default function BillPayments() {

    const tableDetails = [
        { code: "7333", customerName: "Riddhi Shah", orderAmount: "₹5,325.00/-", orderDate: "18 Nov 2022", paymentMode: "Cash", paymentStatus: "Paid" },
        { code: "7333", customerName: "Riddhi Shah", orderAmount: "₹5,325.00/-", orderDate: "18 Nov 2022", paymentMode: "Debit card", paymentStatus: "Paid" },
        { code: "7333", customerName: "Riddhi Shah", orderAmount: "₹5,325.00/-", orderDate: "18 Nov 2022", paymentMode: "UPI", paymentStatus: "Unpaid" },
        { code: "7333", customerName: "Riddhi Shah", orderAmount: "₹5,325.00/-", orderDate: "18 Nov 2022", paymentMode: "Cash", paymentStatus: "Paid" },
        { code: "7333", customerName: "Riddhi Shah", orderAmount: "₹5,325.00/-", orderDate: "18 Nov 2022", paymentMode: "Debit card", paymentStatus: "Paid" },
        { code: "7333", customerName: "Riddhi Shah", orderAmount: "₹5,325.00/-", orderDate: "18 Nov 2022", paymentMode: "UPI", paymentStatus: "Unpaid" },
        { code: "7333", customerName: "Riddhi Shah", orderAmount: "₹5,325.00/-", orderDate: "18 Nov 2022", paymentMode: "UPI", paymentStatus: "Unpaid" },
    ];

    const [showCustomerDetailsPopup, setShowCustomerDetailsPopup] = useState(false);

    const inverseVisibilityPopup = () => {
        setShowCustomerDetailsPopup(!showCustomerDetailsPopup);
    };

    const navigate = useNavigate();
    const handleClickLiveOrders = () => {
        navigate("/pos-orders");
    };
    const handleClickTableManagement = () => {
        navigate("/table-management");
    };
    return (
        <>
            <div className="bg-[#fafafa]">
                <div className={`px-8 pb-[70px] lg:px-4 lg:pb-[23px] pt-4 w-full max-w-[1336px] mx-auto bg-white ${showCustomerDetailsPopup && "md:hidden"}`}>
                    <div className="mb-4 md:hidden">
                        <DefaultBreadcrumbs icon={<BillPaymentsIcon className="h-5 w-5" />} mainTab="Bill Payments" />
                    </div>

                    <div className="flex flex-row items-center mb-4 pb-4 border-b border-neutral-300">
                        <div className="mr-4 md:w-1/2" onClick={handleClickLiveOrders}>
                            <LargePrimaryButton name="Live orders" leftIconDefault={<RedirectIcon fill="#FFFFFF" />} leftIconClick={<RedirectIcon fill="#C4BEED" />} />
                        </div>
                        <div className="md:w-1/2" onClick={handleClickTableManagement}>
                            <LargePrimaryButton
                                containerStyle="md:max-w-[166px] mobile:max-w-[144px]"
                                name="Table management"
                                leftIconDefault={<TableManagementIcon stroke="#FFFFFF" />}
                                leftIconClick={<TableManagementIcon stroke="#C4BEED" />}
                            />
                        </div>
                    </div>

                    <div className="flex flex-row items-center justify-between mb-6 md:block md:w-full">
                        <CalenderField label="25 Sept 2022 - 09 Oct 2022" buttonStyle="md:w-full" />
                        <div className="flex flex-row md:mt-4">
                            <div className="mx-4 md:mx-0 md:mr-2 md:w-1/2">
                                <LargePrimaryButton name="Filters" leftIconDefault={<FilterIcon fill="#FFFFFF" />} leftIconClick={<FilterIcon fill="#C4BEED" />} />
                            </div>
                            <div className="min-w-[161px] md:ml-2 md:w-1/2">
                                <LargePrimaryButton name="Export data" leftIconDefault={<ExportIcon stroke="#FFFFFF" />} leftIconClick={<ExportIcon stroke="#C4BEED" />} />
                            </div>
                        </div>
                    </div>

                    <div className="w-full rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden border-neutral-300 border">
                        <table className="w-full break-words tableMediaLibrary">
                            <thead>
                                <tr className="shadow-innerShadow bg-neutral-50 text-left justify-center h-11">
                                    <th className="px-6 min-w-[147px] lg:min-w-[112px] paragraphOverlineSmall text-neutral-700">CODE</th>
                                    <th className="px-6 min-w-[279px] lg:min-w-[213px] paragraphOverlineSmall text-neutral-700">CUSTOMER NAME</th>
                                    <th className="px-6 min-w-[203px] lg:min-w-[160px] paragraphOverlineSmall text-neutral-700">ORDER AMOUNT</th>
                                    <th className="px-6 min-w-[213px] lg:min-w-[164px] paragraphOverlineSmall text-neutral-700">ORDER DATE</th>
                                    <th className="px-6 min-w-[214px] paragraphOverlineSmall text-neutral-700">PAYMENT MODE</th>
                                    <th className="px-6 min-w-[214px] paragraphOverlineSmall text-neutral-700">PAYMENT STATUS</th>
                                </tr>
                            </thead>
                            <tbody>
                                {tableDetails.map((el, index) => {
                                    return (
                                        <tr className={`paragraphSmallRegular ${index !== 0 && "border-t"} border-neutral-300 justify-center h-[70px]`} key={index}>
                                            <td className="px-6 cursor-pointer" onClick={inverseVisibilityPopup}>
                                                {el.code}
                                            </td>
                                            <td className="px-6">{el.customerName}</td>
                                            <td className="px-6">{el.orderAmount}</td>
                                            <td className="px-6">{el.orderDate}</td>
                                            <td className="px-6">
                                                <div className="flex flex-row items-center">
                                                    {el.paymentMode === "Cash" ? <CashIcon /> : el.paymentMode === "UPI" ? <UpiIcon /> : <CardIcon />}
                                                    <span className="paragraphSmallRegular ml-1">{el.paymentMode}</span>
                                                </div>
                                            </td>
                                            <td className="px-6">
                                                {el.paymentStatus === "Paid" ? (
                                                    <span className="text-tertiary-800 paragraphSmallMedium border px-2 py-1 border-tertiary-800 rounded bg-tertiary-100">Paid</span>
                                                ) : (
                                                    <span className="text-destructive-600 paragraphSmallMedium border px-2 py-1 border-destructive-600 rounded bg-destructive-100">Unpaid</span>
                                                )}
                                            </td>
                                        </tr>
                                    );
                                })}
                            </tbody>
                        </table>
                    </div>

                    <div className="hidden md:block">
                        {tableDetails.map((el, index) => (
                            <div className="mt-2" key={index}>
                                <ListViewBillPayment
                                    handleClickViewDetails={inverseVisibilityPopup}
                                    customerName={el.customerName}
                                    code={el.code}
                                    paymentStatus={el.paymentStatus}
                                    orderAmount={el.orderAmount}
                                    orderDate={el.orderDate}
                                    paymentMode={el.paymentMode}
                                />
                            </div>
                        ))}
                    </div>

                    <div className="mt-4 md:hidden">
                        <PaginationWithNumber />
                    </div>
                </div>

                <div className={`${!showCustomerDetailsPopup && "hidden"}`}>
                    <CustomerOrderDetails pageName="billPayment" handleClickClose={inverseVisibilityPopup} headerTabletScreen="Back to bill payments" />
                </div>
            </div>
        </>
    );
}
