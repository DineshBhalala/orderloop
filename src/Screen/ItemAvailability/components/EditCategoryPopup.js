import React, { useState } from "react";
import { ReactComponent as Close } from "../../../Assets/close.svg";
import { ReactComponent as CalenderIcon } from "../../../Assets/calendar.svg";
import ToggleSwitch from "../../../Components/ToggleSwitch/ToggleSwitch";
import { LargePrimaryButton } from "../../../Components/Buttons/Button";
import { RadioButton } from "../../../Components/FormControl/FormControls";

export default function EditCategoryPopup(props) {
  const [isShowCalender, SetIsShowCalender] = useState(false);

  const handleOptionChange = (value) => {
    value === "radioBox04" ? SetIsShowCalender(true) : SetIsShowCalender(false);
  };

  return (
    <>
      <div className="fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 md:px-4 flex">
        <div className="max-w-[475px] w-full rounded-xl bg-shades-50 px-8 py-6 m-auto md:w-full md:px-4 md:py-4">
          <div className="flex flex-row justify-between items-center mb-6 md:mb-[11px]">
            <div>
              <span className="paragraphLargeMedium">Edit category availability</span>
              <div className="flex flex-row items-center">
                <span className="paragraphMediumItalic text-neutral-500">Toggle on availability for following modes</span>
              </div>
            </div>
            <div onClick={() => props.handleClickClose()} className="md:hidden cursor-pointer">
              <Close />
            </div>
          </div>

          <div className={`${props.editType === "left" && "hidden"}`}>
            <div className="pb-6 mb-6 border-b border-neutral-300 md:mb-4 md:pb-4">
              <div className="mb-4">
                <span className="paragraphSmallSemiBold text-black">
                  Auto turn-on dish after
                  <span className="paragraphXSmallItalic text-neutral-500 ml-1">(Out of stock will be displayed)</span>
                </span>
              </div>
              <div className="">
                <RadioButton marginB="2" label="2 hours" optionId="radioBox01" value="2 hours" chacked={true} handleOptionChange={handleOptionChange} />
                <RadioButton marginB="2" label="4 hours" optionId="radioBox02" value="4 hours" handleOptionChange={handleOptionChange} />
                <RadioButton marginB="2" label="Next business days" optionId="radioBox03" value="Next business days" handleOptionChange={handleOptionChange} />
                <RadioButton marginB="2" label="Schedule time to reopen" optionId="radioBox04" value="Schedule time to reopen" handleOptionChange={handleOptionChange} />
                <div className={`${!isShowCalender && "hidden"} max-w-[256px] w-full mb-2`}>
                  <button className="border px-3 py-[11px] mobile:px-0 flex flex-row items-center rounded-md border-neutral-300 w-full">
                    <CalenderIcon className="mx-1 mobile:ml-1.5" />
                    <span className="paragraphMediumRegular mx-1 mobile:mx-0 mobile:text-sm">25 Sept 2022 - 17:45 PM</span>
                  </button>
                </div>
                <RadioButton label="Manually turn it on" optionId="radioBox05" value="Manually turn it on" handleOptionChange={handleOptionChange} />
              </div>
            </div>

            {/* <RadioButton
                  marginB="2"
                  label="2 hours"
                  optionId="radioBox01"
                />
                <RadioButton
                  marginB="2"
                  label="4 hours"
                  optionId="radioBox02"
                />
                <RadioButton
                  marginB="2"
                  label="Next business days"
                  optionId="radioBox03"
                />
                <RadioButton
                  marginB="2"
                  label="Schedule time to reopen"
                  optionId="radioBox04"
                />
                <RadioButton
                  marginB="2"
                  label="Manually turn it on"
                  optionId="radioBox05"
                />
                here is 5 radio buttons which are imported from other component which is this one
                const RadioButton = (props) => {
    return (
        <>
            <div className={`radioButton flex flex-row items-center relative ml-${props.marginL} mt-${props.marginT} mr-${props.marginR} mb-${props.marginB}`}>
                <input type="radio" id={props.optionId} name='radioGroup' />
                <label htmlFor={props.optionId} className='radio-label'><span className={`paragraphSmallRegular pl-${props.paddingL}`}>{props.label}</span></label>
            </div>
        </>
    )
}
                 i want to show hello world as "Schedule time to reopen" is selected */}

            <div className="mb-6 pb-6 border-b border-neutral-300 flex flex-row justify-between md:mb-4 md:pb-4">
              <span className="text-black paragraphSmallSemiBold">Make this dish invisible too</span>
              <ToggleSwitch />
            </div>
          </div>

          <div className="mb-20 md:mb-4">
            <div className="paragraphSmallSemiBold text-black mb-4">
              <span>Status</span>
              <span className="ml-4">Ordering mode</span>
            </div>
            <div className="paragraphSmallRegular flex flex-row items-center mb-4 md:mb-[14px]">
              <ToggleSwitch />
              <span className="ml-[21px]">Dine-in</span>
            </div>
            <div className="paragraphSmallRegular flex flex-row items-center mb-4 md:mb-[14px]">
              <ToggleSwitch />
              <span className="ml-[21px]">Takeaway</span>
            </div>
            <div className="paragraphSmallRegular flex flex-row items-center">
              <ToggleSwitch />
              <span className="ml-[21px]">Delivery</span>
            </div>
          </div>

          <div className="md:flex md:flex-row md:justify-between">
            <div className="md:block hidden w-full mr-[15px]">
              <button onClick={() => props.handleClickClose()} className="h-12 w-full paragraphMediumMedium text-neutral-500 border border-neutral-300 rounded-md lg:max-w-[289px] cursor-pointer">
                Cancel
              </button>
            </div>
            <LargePrimaryButton disabled={true} name="Save" />
          </div>
        </div>
      </div>
    </>
  );
}

// import React, { useState } from "react";
// import { RadioButton } from "../../../Components/FormControl/FormControls";
// // import RadioButton from "./RadioButton";

// const App = () => {

//   const handleOptionChange = (value) => {
//     console.log(value)
//   };

//   return (
//     <div>
//       <div className="fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 md:px-4 flex">
//         <div className="max-w-[475px] w-full rounded-xl bg-shades-50 px-8 py-6 m-auto md:w-full md:px-4 md:py-4">
//           <RadioButton
//             marginB="2"
//             label="2 hours"
//             optionId="radioBox01"
//             value="2 hours"
//             handleOptionChange={handleOptionChange}
//           />
//           <RadioButton
//             marginB="2"
//             label="4 hours"
//             optionId="radioBox02"
//             value="4 hours"
//             handleOptionChange={handleOptionChange}
//           />
//           <RadioButton
//             marginB="2"
//             label="Next business days"
//             optionId="radioBox03"
//             value="Next business days"
//             handleOptionChange={handleOptionChange}
//           />
//           <RadioButton
//             marginB="2"
//             label="Schedule time to reopen"
//             optionId="radioBox04"
//             value="Schedule time to reopen"
//             handleOptionChange={handleOptionChange}
//           />
//           <div className=""></div>
//           <RadioButton
//             marginB="2"
//             label="Manually turn it on"
//             optionId="radioBox05"
//             value="Manually turn it on"
//             handleOptionChange={handleOptionChange}
//           />
//         </div>
//       </div>
//     </div>
//   );
// };

// export default App;
