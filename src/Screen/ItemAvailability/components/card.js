import React, { useState } from "react";
import { ReactComponent as VegIcon } from "../../../Assets/vegetable-icon.svg";
import { ReactComponent as NonVegIcon } from "../../../Assets/non-veg.svg";
import OptionButtonSwitch from "../../../Components/ToggleSwitch/OptionButtonSwitch";
import { ReactComponent as LeftArrow } from "../../../Assets/chevron-down.svg";
import { ReactComponent as EditIcon } from "../../../Assets/edit.svg";
import { CheckBoxWithoutLabels } from "../../../Components/FormControl/FormControls";

export const Categories = (props) => {
    const handleClickCard = () => {
        props.handleClickCategory && props.handleClickCategory(props.title);
    };

    const [showSubcategories, setShowSubcategories] = useState(false);

    const handleClickArrow = () => {
        setShowSubcategories(!showSubcategories);
    };

    return (
        <>
            <div className="flex flex-row items-center relative">
                {props.isEdit && (
                    <div className="absolute top-10 -left-6 cursor-pointer" onClick={props.handleCLickEdit}>
                        <EditIcon />
                    </div>
                )}

                <div className={`mr-1 ${!props.isFromSelection && "hidden"}`}>
                    <CheckBoxWithoutLabels />
                </div>

                <div
                    className={`flex flex-row cursor-pointer ${props.page === "menuManagement" ? "items-start" : "items-center"} ${
                        props.page !== "menuManagement" ? "px-4" : props.isFromSelection ? "px-3" : "pl-[11px] pr-4"
                    } border md:py-3 ${props.isActive ? "border-primary-500 bg-primary-50" : "border-neutral-300"} w-full rounded-md ${props.minWidth ?? "md:min-w-[343px]"} mobile:min-w-full`}
                    onClick={handleClickCard}
                >
                    <div onClick={props.changeAvailability} className={`mr-2 ${props.page === "menuManagement" && "hidden"} cursor-pointer`}>
                        <OptionButtonSwitch />
                    </div>

                    <img
                        src={props.img}
                        alt=""
                        className={`${props.imageSize ?? "h-[78px] w-[78px] md:h-12 md:w-12"} ${props.imageMargin ?? "mt-2.5 mb-[14px]"} ${
                            props.page === "menuManagement" ? "mr-1" : "mx-2"
                        } rounded-md md:mt-0 md:mb-0 mobile:hidden`}
                    />

                    <div className={`w-full mt-[13px] md:mt-0 ${props.isFromSelection ? "ml-1" : "ml-2"}`}>
                        <div className={`flex flex-row justify-between items-center ${props.titleStyle ? "paragraphSmallMedium mb-1" : "headingH6MediumDesktop md:paragraphMediumMedium mb-0.5"}`}>
                            <span className="md:truncate md:max-w-[123px]">{props.page === "menuManagement" && props.title.length > 30 ? props.title.substring(0, 30) + "..." : props.title}</span>
                            <div className="ml-2">
                                {props.type === "veg" ? (
                                    <VegIcon className="h-[14px] w-[14px]" />
                                ) : props.type === "nonVeg" ? (
                                    <NonVegIcon className="h-[14px] w-[14px]" />
                                ) : (
                                    <div className="flex flex-row">
                                        <VegIcon className="mx-1 h-[14px] w-[14px]" />
                                        <NonVegIcon className="h-[14px] w-[14px]" />
                                    </div>
                                )}
                            </div>
                        </div>

                        <div className={`${props.isFromSelection ? "paragraphXSmallItalic" : "paragraphSmallItalic"} text-neutral-500`}>
                            <span>{props.dishes}</span>
                        </div>

                        {props.subCategoryDetails && (
                            <div className="flex flex-row justify-between mt-1.5 items-center">
                                <span className={`${props.isFromSelection ? "paragraphXSmallItalic" : "paragraphSmallItalic"} text-neutral-500`}>
                                    Sub-categories: {props.subCategoryDetails.length}
                                </span>
                                <LeftArrow className={`${showSubcategories && "rotate-180"} cursor-pointer ${props.dropdownIconSize}`} onClick={handleClickArrow} />
                            </div>
                        )}
                    </div>
                </div>
            </div>

            <div className={`${props.dropDownCategoryMarginL} w-full md:ml-0 ${!showSubcategories && "hidden"}`}>
                {props.subCategoryDetails &&
                    props.subCategoryDetails.map((el, index) => (
                        <div className="mt-2" key={index}>
                            <Categories
                                minWidth={props.minWidth}
                                key={el.title}
                                img={el.img}
                                type={el.type}
                                title={el.title}
                                imageSize={props.imageSize}
                                dishes={el.dishes}
                                isFromSelection={props.isFromSelection}
                                page={props.page}
                                isActive={el.title === props.activeCategory}
                                handleClickCategory={props.handleClickCategory}
                                isEdit={props.isEdit}
                                handleCLickEdit={props.handleCLickEdit}
                                titleStyle={props.titleStyle}
                                dishNumberTextStyle={props.dishNumberTextStyle}
                                boxPadding={props.boxPadding}
                                dropdownIconSize={props.dropdownIconSize}
                            />
                        </div>
                    ))}
            </div>
        </>
    );
};

export const Dishes = (props) => {
    const handleClickCard = () => {
        props.handleClickDish && props.handleClickDish(props.title);
    };

    return (
        <>
            <div className="flex flex-row items-center relative">
                {props.isEdit && (
                    <div className="absolute top-10 -left-6 cursor-pointer" onClick={props.handleCLickEdit}>
                        <EditIcon />
                    </div>
                )}

                <div className={`mr-1 ${!props.isFromSelection && "hidden"}`}>
                    <CheckBoxWithoutLabels />
                </div>

                <div
                    className={`${props.page !== "menuManagement" ? "px-4" : props.isFromSelection ? "px-3" : "pl-[11px] pr-4"} ${props.boxPadding ?? "py-3"} ${
                        props.boxHeight ?? "h-[104px] md:h-full md:max-h-[96px]"
                    } ${props.isActive ? "border-primary-500 bg-primary-50" : "border-neutral-300"} border rounded-lg flex flex-row w-full ${
                        props.page === "menuManagement" && "lg:px-3"
                    } cursor-pointer`}
                    onClick={handleClickCard}
                >
                    <div className={`m-auto ${props.page === "menuManagement" && "hidden"} cursor-pointer`} onClick={() => props.changeAvailability(props.position)}>
                        <OptionButtonSwitch position={props.position} />
                    </div>

                    {props.image && (
                        <img
                            src={props.image}
                            alt=""
                            className={`${props.imageSize ?? "h-[78px] w-[78px]"} rounded ${props.isFromSelection ? "mr-2" : "mr-4"} ${props.page === "menuManagement" && "lg:mr-2"}`}
                        />
                    )}

                    <div className={`w-full ${props.page === "menuManagement" ? "ml-0" : "ml-4"}`}>
                        <div className="flex flex-row items-start justify-between">
                            <span className={`${props.titleStyle ?? "headingH6MediumDesktop"} md:paragraphMediumMedium pr-4`}>
                                {props.page === "menuManagement" && props.title.length > 27 ? props.title.substring(0, 27) + "..." : props.title}
                            </span>

                            <span className="mt-0.5">
                                <VegIcon />
                            </span>
                        </div>

                        <div>
                            <span className={`${props.isFromSelection ? "paragraphXSmallItalic" : "paragraphSmallItalic"} text-neutral-500`}>Variants:</span>
                            <span className={`${props.isFromSelection ? "paragraphXSmallItalic" : "paragraphSmallItalic"} text-neutral-500`}> {props.variants}</span>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};
