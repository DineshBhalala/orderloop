import React, { useState } from "react";
import { DefaultBreadcrumbs } from "../../Components/Breadcrumbs/Breadcrumbs";

import { ReactComponent as ItemAvailabilityIcon } from "../../Assets/item-availability.svg";
import { ReactComponent as SearchIcon } from "../../Assets/search.svg";
import { ReactComponent as FilterIcon } from "../../Assets/filter.svg";
import { ReactComponent as SelectIcon } from "../../Assets/select.svg";
import { ReactComponent as LeftArrow } from "../../Assets/chevron-down.svg";

import { Tab } from "../../Components/Tabs/Tabs";
import { LargePrimaryButton } from "../../Components/Buttons/Button";
import { DefaultInputField } from "../../Components/InputField/InputField";
import { Categories, Dishes } from "./Components/card";
import { DropDownTabs } from "../../Components/DropDown/DropDownTabs";

import pizza1 from "../../Assets/mediaLibrary/pizza1.png";
import pizza2 from "../../Assets/mediaLibrary/pizza2.png";
import pizza3 from "../../Assets/mediaLibrary/pizza3.png";
import pizza4 from "../../Assets/mediaLibrary/pizza4.png";
import pizza5 from "../../Assets/mediaLibrary/pizza5.png";
import pizza6 from "../../Assets/mediaLibrary/pizza6.png";
import pizza7 from "../../Assets/mediaLibrary/pizza7.png";
import pizza8 from "../../Assets/mediaLibrary/pizza8.png";
import EditCategoryPopup from "./Components/EditCategoryPopup";

export default function ItemAvailability() {
    const [showItemAvailability, setShowItemAvailability] = useState(false);

    const handleClickAddonTab = () => {
        setShowItemAvailability(false);
    };

    const handleClickItemAvailabilityTab = () => {
        setShowItemAvailability(true);
    };

    const categoriesDetails = [
        {
            title: "Sides",
            dishes: "21 dishes",
            img: pizza1,
            type: "veg-nonVeg",
            subCategoryDetails: [
                {
                    title: "Garlic Bread",
                    dishes: "09 dishes",
                    img: pizza2,
                    type: "nonVeg",
                },
                {
                    title: "French Fries Text Extended",
                    dishes: "12 dishes",
                    img: pizza3,
                    type: "veg",
                },
            ],
        },

        {
            title: "Pizza",
            dishes: "32 dishes",
            img: pizza4,
            type: "veg-nonVeg",
        },
        {
            title: "Burger",
            dishes: "03 dishes",
            img: pizza5,
            type: "veg-nonVeg",
        },
        {
            title: "Shakes",
            dishes: "07 dishes",
            img: pizza6,
            type: "veg-nonVeg",
        },
        {
            title: "Salad",
            dishes: "10 dishes",
            img: pizza7,
            type: "veg-nonVeg",
        },
        {
            title: "Pastries",
            dishes: "10 dishes",
            img: pizza8,
            type: "veg-nonVeg",
        },
    ];

    const dishesGarlicBreadDetails = [
        {
            title: "Garlic Breadsticks",
            variants: "No",
            position: "left",
        },
        {
            title: "Classic Stuffed Garlic Bread",
            variants: "Yes (2)",
            position: "middle",
        },
        {
            title: "Paneer Tikka Stuffed Garlic Bread",
            variants: "Yes (2)",
            position: "right",
        },
    ];

    const dishesFriesDetails = [
        {
            title: "Crinkle Fries",
            variants: "No",
        },
        {
            title: "Piri Piri French Fries",
            variants: "Yes (4)",
        },
    ];

    const [showEditCategory, setShowEditCategory] = useState(false);

    const [editType, setEditType] = useState("left");

    const changeAvailability = (position) => {
        setShowEditCategory(!showEditCategory);
        setEditType(position);
    };

    const emptyFunction = () => {};

    const handleClickCategory = (categoryName) => {
        setActiveCategory(categoryName);
    };

    const [activeCategory, setActiveCategory] = useState(categoriesDetails[0].title);

    const [activeDish, setActiveDish] = useState(dishesGarlicBreadDetails[0].title);

    const handleClickDish = (dishName) => {
        setActiveDish(dishName);
    };

    return (
        <>
            <div className="bg-[#fafafa]">
                <div className="px-8 pb-[70px] lg:px-4 lg:pb-0 pt-4 w-full max-w-[1336px] mx-auto bg-white">
                    <div className="mb-4 md:hidden">
                        <DefaultBreadcrumbs mainTab="Dish availability" icon={<ItemAvailabilityIcon className="h-5 w-5" />} />
                    </div>

                    <div className="flex flex-row mb-4 pb-4 border-b border-neutral-300 md:hidden">
                        <div onClick={handleClickItemAvailabilityTab} className="mr-2 max-w-[217px] w-full cursor-pointer">
                            <Tab label="Dish availability" badgeText="Off - 6" isActive={showItemAvailability} />
                        </div>

                        <div className="ml-2 w-full max-w-[247px] cursor-pointer" onClick={handleClickAddonTab}>
                            <Tab badgeText="Off - 10" label="Add-on availability" isActive={!showItemAvailability} />
                        </div>
                    </div>

                    <div className="hidden md:block pb-4 border-b mb-4">
                        <DropDownTabs
                            menuItems={[
                                { item: "Dish availability", badgeText: "Off - 10" },
                                { item: "Add-on availability", badgeText: "Off - 10" },
                            ]}
                        />
                    </div>

                    <div className="flex flex-row justify-between mb-6 md:block md:mb-4">
                        <div className="flex flex-row md:mb-3">
                            <div className="min-w-[375px] lg:min-w-[298px] w-full mr-4 md:mr-2 md:min-w-0">
                                <DefaultInputField placeholder="Search media" placeholderIcon={<SearchIcon stroke="#D3D2D8" />} />
                            </div>

                            <div className="max-w-[156px] w-full md:max-w-full md:w-[64px] ">
                                <LargePrimaryButton name="Bulk select" leftIconDefault={<SelectIcon stroke="#FFFFFF" />} leftIconClick={<SelectIcon stroke="#C4BEED" />} hideName="md:hidden" />
                            </div>
                        </div>
                        <div className="mx-4 md:mx-0">
                            <LargePrimaryButton name="Filters" leftIconDefault={<FilterIcon fill="#FFFFFF" />} leftIconClick={<FilterIcon fill="#C4BEED" />} />
                        </div>
                    </div>

                    <div className="flex flex-row lg:block">
                        <div className="pr-8 mr-8 border-r border-neutral-300 lg:border-0 lg:pb-2 lg:pr-0 lg:mr-0 md:overflow-hidden md:h-full">
                            <div className="lg:h-[388px] lg:overflow-auto lg:pr-3 scrollbarStyle">
                                <h3 className="paragraphLargeMedium text-black">Categories</h3>
                                <div className="lg:max-w-[396px] md:max-w-full">
                                    {categoriesDetails.map((el, index) => {
                                        return (
                                            <div className="mt-4 w-full max-w-[396px] md:max-w-full md:mt-2" key={index}>
                                                <Categories
                                                    dropDownCategoryMarginL="ml-4"
                                                    handleClickCategory={handleClickCategory}
                                                    isActive={activeCategory === el.title}
                                                    img={el.img}
                                                    title={el.title}
                                                    dishes={el.dishes}
                                                    type={el.type}
                                                    minWidth="min-w-[396px]"
                                                    subCategoryDetails={el.subCategoryDetails}
                                                    changeAvailability={changeAvailability}
                                                    activeCategory={activeCategory}
                                                />
                                            </div>
                                        );
                                    })}
                                </div>
                            </div>
                        </div>

                        <div className="w-full md:relative md:h-full md:pr-0 lg:bg-white lg:border-t lg:pt-4 lg:border-neutral-300">
                            <div className="lg:w-full lg:pr-2 md:border-none lg:h-[336px] lg:overflow-auto scrollbarStyle">
                                <div className="max-w-[507px] w-full md:max-w-full">
                                    <span className="paragraphLargeMedium text-black">Dishes</span>

                                    <div className="hidden md:flex paragraphMediumMedium flex-row mb-4">
                                        <LeftArrow className="rotate-90" />
                                        <span className="ml-1">Back to cashback</span>
                                    </div>

                                    <div className="mt-4">
                                        <span className="paragraphLargeItalic text-neutral-500">Garlic Bread</span>
                                    </div>

                                    <div className="mb-4">
                                        {dishesGarlicBreadDetails.map((el, index) => {
                                            return (
                                                <div className="mt-4 h-[104px] md:h-full" key={index}>
                                                    <Dishes
                                                        title={el.title}
                                                        variants={el.variants}
                                                        changeAvailability={changeAvailability}
                                                        position={el.position}
                                                        handleClickDish={handleClickDish}
                                                        isActive={activeDish === el.title}
                                                    />
                                                </div>
                                            );
                                        })}
                                    </div>

                                    <div className="">
                                        <span className="paragraphLargeItalic text-neutral-500">French Fries Text Extended</span>
                                    </div>

                                    <div className="">
                                        {dishesFriesDetails.map((el, index) => {
                                            return (
                                                <div className="mt-4 h-[104px] md:h-full" key={index}>
                                                    <Dishes title={el.title} variants={el.variants} changeAvailability={changeAvailability} handleClickDish={handleClickDish} />
                                                </div>
                                            );
                                        })}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className={`${!showEditCategory && "hidden"}`}>
                        <EditCategoryPopup handleClickClose={changeAvailability} editType={editType} />
                    </div>
                </div>
            </div>
        </>
    );
}
