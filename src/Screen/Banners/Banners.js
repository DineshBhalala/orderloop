import React, { useState } from "react";
import { DefaultBreadcrumbs } from "../../Components/Breadcrumbs/Breadcrumbs";
import { ReactComponent as BannerIcon } from "../../Assets/banners.svg";
import { ReactComponent as RedirectIcon } from "../../Assets/redirect.svg";
import { ReactComponent as AddIcon } from "../../Assets/add.svg";
import { ReactComponent as ReArrangeIcon } from "../../Assets/re-arrange.svg";
import { ReactComponent as SelectIcon } from "../../Assets/select.svg";
import { ReactComponent as CalenderIcon } from "../../Assets/calendar.svg";
import { ReactComponent as ScheduleIcon } from "../../Assets/schedule.svg";
import { Tab } from "../../Components/Tabs/Tabs";
import { LargePrimaryButton } from "../../Components/Buttons/Button";
import pizza1 from "../../Assets/mediaLibrary/pizza1.png";
import pizza2 from "../../Assets/mediaLibrary/pizza2.png";
import pizza3 from "../../Assets/mediaLibrary/pizza3.png";
import pizza4 from "../../Assets/mediaLibrary/pizza4.png";
import pizza5 from "../../Assets/mediaLibrary/pizza5.png";
import PaginationWithNumber from "../../Components/Pagination/PaginationWithNumber";
import { LinkOffer } from "../../Components/LinkOffer/LinkOffer";
import AddBannerPopUp from "./Components/AddBannerPopUp";
import BannerCard from "./Components/BannerCard";
import { DropDownTabs } from "../../Components/DropDown/DropDownTabs";

export default function Banners() {
    const [isBannerCatalogueActive, setIsBannerCatalogueActive] = useState(true);

    const handleClickBannerCatalogue = () => {
        setIsBannerCatalogueActive(true);
    };

    const handleClickStoreBanners = () => {
        setIsBannerCatalogueActive(false);
    };

    const [tabs, setTabs] = useState([
        { label: "Primary banners", isActive: false },
        { label: "Secondary banners", isActive: true },
        { label: "Tertiary banners", isActive: false },
    ]);

    function handleTabClick(clickedTabLabel) {
        setTabs(
            tabs.map((tab) => ({
                ...tab,
                isActive: tab.label === clickedTabLabel ? true : false,
            }))
        );
    }

    const storeBannerDetails = [
        {
            bannerIcon: pizza1,
            banner: "Home delivery 30",
            validFrom: "28 Nov 2022,12:00 PM",
            validTill: "30 Nov 2022,05:00 PM",
            bannerScreen: "Home",
            availableModes: "Fine-dine,QSR,Takeaway",
            availableTimeSlots: "12:00 PM - 03:00 PM,05:00 PM - 11:30 PM",
        },
        {
            bannerIcon: pizza2,
            banner: "Free delivery",
            validFrom: "28 Nov 2022,12:00 PM",
            validTill: "30 Nov 2022,05:00 PM",
            bannerScreen: "Offers",
            availableModes: "Fine-dine",
            availableTimeSlots: "12:00 PM - 03:00 PM,05:00 PM - 11:30 PM",
        },
    ];

    const bannerCatalogsDetails = [
        {
            bannerIcon: pizza1,
            banner: "Home delivery 30",
            validFrom: "28 Nov 2022,12:00 PM",
            validTill: "30 Nov 2022,05:00 PM",
            bannerScreen: "Home",
            availableModes: "Fine-dine,QSR,Takeaway",
            availableTimeSlots: "12:00 PM - 03:00 PM,05:00 PM - 11:30 PM",
            linkOutletNumber: 4,
        },
        {
            bannerIcon: pizza2,
            banner: "Free delivery",
            validFrom: "28 Nov 2022,12:00 PM",
            validTill: "30 Nov 2022,05:00 PM",
            bannerScreen: "Offers",
            availableModes: "Fine-dine",
            availableTimeSlots: "12:00 PM - 03:00 PM,05:00 PM - 11:30 PM",
            linkOutletNumber: 4,
        },
        {
            bannerIcon: pizza3,
            banner: "Fatso30",
            validFrom: "28 Nov 2022,12:00 PM",
            validTill: "30 Nov 2022,05:00 PM",
            bannerScreen: "Cart",
            availableModes: "QSR",
            availableTimeSlots: "12:00 PM - 03:00 PM,05:00 PM - 11:30 PM",
            linkOutletNumber: 4,
        },
        {
            bannerIcon: pizza4,
            banner: "Offer 20 percent",
            validFrom: "28 Nov 2022,12:00 PM",
            validTill: "30 Nov 2022,05:00 PM",
            bannerScreen: "Profile",
            availableModes: "Takeaway,Fine-dine",
            availableTimeSlots: "12:00 PM - 03:00 PM,05:00 PM - 11:30 PM",
            linkOutletNumber: 4,
        },
        {
            bannerIcon: pizza5,
            banner: "BOGO today only",
            validFrom: "28 Nov 2022,12:00 PM",
            validTill: "30 Nov 2022,05:00 PM",
            bannerScreen: "Home",
            availableModes: "Takeaway",
            availableTimeSlots: "12:00 PM - 03:00 PM,05:00 PM - 11:30 PM",
            linkOutletNumber: 4,
        },
    ];

    const [isShowAddBannerPage, setIsShowAddBannerPage] = useState(false);

    const bannerPageShowHideController = () => {
        setIsShowAddBannerPage(!isShowAddBannerPage);
    };

    return (
        <>
            <div className="bg-[#fafafa]">
                <div className={`px-8 pb-[70px] lg:px-4 lg:pb-0 pt-4 w-full max-w-[1336px] mx-auto bg-white ${isShowAddBannerPage && "md:hidden"}`}>
                    <div className="mb-4 md:hidden">
                        <DefaultBreadcrumbs mainTab="Banners" icon={<BannerIcon className="h-5 w-5" />} />
                    </div>

                    <div className="flex flex-row justify-between pb-4 mb-4 border-b border-neutral-300 md:block">
                        <div className="flex flex-row md:mb-4">
                            <div className="mr-2 w-1/2 min-w-[167px] md:min-w-0 lg:mr-0 mobile:mr-1 cursor-pointer" onClick={handleClickBannerCatalogue}>
                                <Tab label="Banner catalogue" isActive={isBannerCatalogueActive} bannerTabStyle="md:text-[15px] mobile:text-sm" />
                            </div>
                            <div className="w-1/2 ml-2 mobile:ml-1 cursor-pointer" onClick={handleClickStoreBanners}>
                                <Tab label="Store banners" isActive={!isBannerCatalogueActive} bannerTabStyle="md:text-[15px] mobile:text-sm" />
                            </div>
                        </div>

                        <div className="flex flex-row">
                            <div className="md:w-1/2 mr-2 lg:mr-0 mobile:mr-1">
                                <LargePrimaryButton name="Media library" leftIconDefault={<RedirectIcon fill="#FFFFFF" />} leftIconClick={<RedirectIcon fill="#C4BEED" />} />
                            </div>
                            <div className={`ml-2 ${!isBannerCatalogueActive && "hidden lg:block"} md:w-1/2 mobile:ml-1 cursor-pointer`} onClick={bannerPageShowHideController}>
                                <LargePrimaryButton isDefault={false} name="Add banner" leftIconDefault={<AddIcon stroke="#FFFFFF" />} leftIconClick={<AddIcon stroke="#C4BEED" />} />
                            </div>
                        </div>
                    </div>

                    <div className="flex flex-row justify-between mb-6">
                        <div className="flex flex-row md:hidden">
                            {tabs.map((tab, index) => (
                                <div className="mr-4 lg:mr-2 cursor-pointer" key={index} onClick={() => handleTabClick(tab.label)}>
                                    <Tab label={tab.label} isActive={tab.isActive} />
                                </div>
                            ))}
                        </div>
                        <div className="md:block hidden w-full">
                            <DropDownTabs menuItems={[{ item: "Primary banners" }, { item: "Secondary banners" }, { item: "Tertiary banners" }]} />
                        </div>
                        <div className="flex flex-row">
                            <div className={`mr-4 lg:mr-2 ${isBannerCatalogueActive && "hidden"} md:hidden w-full`}>
                                <LargePrimaryButton name="Re-arrange" leftIconDefault={<ReArrangeIcon stroke="#ffffff" />} leftIconClick={<ReArrangeIcon stroke="#C4BEED" />} hideName="lg:hidden" />
                            </div>
                            <div className="md:min-w-[64px] md:ml-2 min-w-fit">
                                <LargePrimaryButton name="Bulk select" leftIconDefault={<SelectIcon stroke="#ffffff" />} leftIconClick={<SelectIcon stroke="#C4BEED" />} hideName="lg:hidden" />
                            </div>
                        </div>
                    </div>
                    <div className="w-full border border-neutral-300 rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden">
                        <table className="w-full break-words">
                            <thead>
                                <tr className="bg-neutral-50 uppercase paragraphOverlineSmall text-neutral-700">
                                    <th className="text-left py-3 pl-6 min-w-[250px] shadow-innerShadow">BANNER</th>
                                    <th className="text-left py-3 pl-6 min-w-[151px] shadow-innerShadow">VALID FROM</th>
                                    <th className="text-left py-3 pl-6 min-w-[152px] shadow-innerShadow">VALID TILL</th>
                                    <th className="text-left py-3 pl-6 min-w-[156px] shadow-innerShadow">BANNER SCREEN</th>
                                    <th className="text-left py-3 pl-6 min-w-[177px] shadow-innerShadow">AVAILABLE MODES</th>
                                    <th className="text-left py-3 pl-6 min-w-[215px] shadow-innerShadow">AVAILABLE TIME SLOTS</th>
                                    <th className="text-left py-3 pl-6 min-w-[169px] shadow-innerShadow">{isBannerCatalogueActive ? "LINK OUTLET" : "UNLINK OUTLET"}</th>
                                </tr>
                            </thead>
                            <tbody>
                                {(isBannerCatalogueActive ? bannerCatalogsDetails : storeBannerDetails).map((row, index) => {
                                    const [validFromDate, validFromTime] = row.validFrom.split(",");
                                    const [validTillDate, validTillTime] = row.validTill.split(",");
                                    const [availableTimeSlot1, availableTimeSlot2] = row.availableTimeSlots.split(",");
                                    const availableModes = row.availableModes.split(",");

                                    return (
                                        <tr key={index} className={`even:bg-neutral-50 ${index !== 0 && "border-t"} border-neutral-300 paragraphSmallRegular justify-center h-[70px]`}>
                                            <td className="pl-6 h-[70px] cursor-pointer">
                                                <div className="flex flex-row items-center">
                                                    <img src={row.bannerIcon} alt="" className="w-20 h-[45px]" />
                                                    <span className="ml-3">{row.banner}</span>
                                                </div>
                                            </td>
                                            <td className="pl-6">
                                                <div className="">
                                                    <div className="flex flex-row items-center mb-1">
                                                        <CalenderIcon className="w-6 h-6" />
                                                        <span>{validFromDate}</span>
                                                    </div>
                                                    <div className="flex flex-row items-center">
                                                        <ScheduleIcon className="w-6 h-6" />
                                                        <span>{validFromTime}</span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td className="pl-6">
                                                <div className="">
                                                    <div className="flex flex-row items-center mb-1">
                                                        <CalenderIcon className="w-6 h-6" />
                                                        <span>{validTillDate}</span>
                                                    </div>
                                                    <div className="flex flex-row items-center">
                                                        <ScheduleIcon className="w-6 h-6" />
                                                        <span>{validTillTime}</span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td className="pl-6">{row.bannerScreen}</td>
                                            <td className="pl-6">
                                                {availableModes.map((el, index) => (
                                                    <span className="inline-block px-2 py-[3px] border border-primary-500 rounded mr-1 mb-1 bg-primary-50 text-primary-500" key={index}>
                                                        {el}
                                                    </span>
                                                ))}
                                            </td>
                                            <td className="pl-6">
                                                <div className="">
                                                    <div className="flex flex-row items-center mb-1">
                                                        <ScheduleIcon className="w-6 h-6" />
                                                        <span>{availableTimeSlot1}</span>
                                                    </div>
                                                    <div className="flex flex-row items-center">
                                                        <ScheduleIcon className="w-6 h-6" />
                                                        <span>{availableTimeSlot2}</span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td className="pl-6">
                                                {isBannerCatalogueActive ? (
                                                    <LinkOffer linkOfferNumber={row.linkOutletNumber} />
                                                ) : (
                                                    <LinkOffer linkOfferNumber={row.linkOutletNumber} linkLabel="Unlink" />
                                                )}
                                            </td>
                                        </tr>
                                    );
                                })}
                            </tbody>
                        </table>
                    </div>

                    <div className="hidden md:block">
                        {(isBannerCatalogueActive ? bannerCatalogsDetails : storeBannerDetails).map((row, index) => (
                            <div className="mt-2" key={index}>
                                <BannerCard label={row.banner} imgIcon={row.bannerIcon} availableModes={row.availableModes} />
                            </div>
                        ))}
                    </div>

                    <div className="mt-4 md:hidden">
                        <PaginationWithNumber />
                    </div>
                </div>
                <div className={`${!isShowAddBannerPage && "hidden"}`}>
                    <AddBannerPopUp handleClickClose={bannerPageShowHideController} />
                </div>
            </div>
        </>
    );
}
