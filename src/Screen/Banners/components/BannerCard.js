import React, { useState } from "react";
import { ReactComponent as DropDownIcon } from "../../../Assets/chevron-down.svg";
import { ReactComponent as CalenderIcon } from "../../../Assets/calendar.svg";
import { ReactComponent as ScheduleIcon } from "../../../Assets/schedule.svg";
import { ReactComponent as LinkIcon } from "../../../Assets/link.svg";

export default function BannerCard(props) {
  const [showDetails, setShowDetails] = useState(false);

  const handleClickCard = () => {
    setShowDetails(!showDetails);
  };

  return (
    <>
      <div className="border border-neutral-300 px-4 py-3 rounded-md">
        <div
          className="flex flex-row items-center justify-between cursor-pointer"
          onClick={handleClickCard}
        >
          <div className="flex flex-row items-center">
            <img src={props.imgIcon} alt="" className="h-[45px] w-20 mr-3" />
            <span className="paragraphSmallRegular">{props.label}</span>
          </div>
          <div className="">
            <DropDownIcon className={`${showDetails && "rotate-180"}`} />
          </div>
        </div>

        <div className={`${!showDetails && "hidden"}`}>
          <div className="flex flex-row items-center mt-2.5">
            <span className="paragraphOverlineSmall mobile:text-[11px]">
              VALID FROM:
            </span>
            <div className="flex flex-row items-center mx-1">
              <CalenderIcon height={20} width={20} />
              <span className="ml-1 paragraphSmallRegular mobile:text-[11px]">
                28 Nov 2022
              </span>
            </div>
            <div className="flex flex-row items-center">
              <ScheduleIcon height={20} width={20} />
              <span className="ml-1 paragraphSmallRegular mobile:text-[11px]">
                12:00 PM
              </span>
            </div>
          </div>

          <div className="flex flex-row items-center mt-2.5">
            <span className="paragraphOverlineSmall mobile:text-[11px]">
              VALID TILL:
            </span>
            <div className="flex flex-row items-center mx-1">
              <ScheduleIcon height={20} width={20} />
              <span className="ml-1 paragraphSmallRegular mobile:text-[11px]">
                12:00 PM
              </span>
            </div>
            <div className="flex flex-row items-center">
              <CalenderIcon height={20} width={20} />
              <span className="ml-1 paragraphSmallRegular mobile:text-[11px]">
                28 Nov 2022
              </span>
            </div>
          </div>

          <div className="flex flex-row items-center mt-2.5">
            <span className="paragraphOverlineSmall mobile:text-[11px]">
              BANNER SCREEN:
            </span>
            <div className="flex flex-row items-center mx-1">
              <span className="paragraphSmallRegular mobile:text-[11px]">
                Home
              </span>
            </div>
          </div>

          <div className="flex flex-row mt-2.5 mobile:block">
            <span className="paragraphOverlineSmall mobile:text-[11px]">
              AVAILABLE MODES:
            </span>
            <div className="flex flex-col ml-1 mobile:ml-0 mobile:block">
              {props.availableModes.split(",").map((el, index) => (
                <span
                  className="inline-block paragraphSmallRegular mobile:text-[11px] px-2 py-[3px] border border-primary-500 rounded mb-2.5 bg-primary-50 text-primary-500 mobile:mr-1 mobile:mt-1"
                  key={index}
                >
                  {el}
                </span>
              ))}
            </div>
          </div>

          <div className="flex flex- mobile:block">
            <span className="paragraphOverlineSmall mobile:text-[11px]">
              AVAILABLE TIME SLOTS:
            </span>

            <div className="">
              <div className="flex flex-row ml-1">
                <ScheduleIcon height={20} width={20} />

                <div className="flex flex-col ml-1 mb-1">
                  <span className="paragraphSmallRegular mobile:text-[11px]">
                    12:00 PM
                  </span>
                  <span className="paragraphSmallRegular mobile:text-[11px] mt-1">
                    03:00 PM
                  </span>
                </div>
              </div>

              <div className="flex flex-row ml-1">
                <ScheduleIcon height={20} width={20} />

                <div className="flex flex-col ml-1">
                  <span className="paragraphSmallRegular mobile:text-[11px]">
                    12:00 PM
                  </span>
                  <span className="paragraphSmallRegular mobile:text-[11px] mt-1">
                    03:00 PM
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div className="flex flex-row items-center mt-2.5">
            <span className="paragraphOverlineSmall mobile:text-[11px]">
              LINK OUTLET:
            </span>
            <div className="flex flex-row items-center mx-1">
              <LinkIcon stroke="#6C5DD3" />
              <span className="text-primary-500 ml-1 paragraphSmallRegular mobile:text-[11px]">
                4
              </span>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
