import React, { useState } from "react";
import { ReactComponent as CloseIcon } from "../../../Assets/close.svg";
import { ReactComponent as CalenderIcon } from "../../../Assets/calendar.svg";
import { ReactComponent as LeftArrowIcon } from "../../../Assets/chevron-down.svg";
import { DefaultInputField } from "../../../Components/InputField/InputField";
import { LargeDestructiveButton, LargePrimaryButton, LargeTertiaryButton } from "../../../Components/Buttons/Button";
import UploadImage from "../../../Components/UploadImage/UploadImage";
import pizza1 from "../../../Assets/mediaLibrary/pizza1.png";
import Tag from "../../../Components/Tag/Tag";
import BannerTimeSlot from "../../../Components/TimeSlot/TimeSlot";
import DropDown from "../../../Components/DropDown/DropDown";

export default function AddBannerPopUp(props) {
    const [showHideUploadPopup, setShowHideUploadPopup] = useState(false);

    const uploadImageShowHideController = () => {
        setShowHideUploadPopup(!showHideUploadPopup);
    };

    const [showSelectContent, setShowSelectContent] = useState(false);

    const selectType = <DefaultInputField label="Select type" placeholder="Banners" placeholderTextColor="neutral-900" />;

    const handleClickSelectImageContent = () => {
        setShowSelectContent(!showSelectContent);
    };

    const [Tags, setTags] = useState(["Fine-dine", "Takeaway", "QSR"]);

    const menuItemBannerTiming = ["Full-time", "Specific time for all days", "Specific time for each day"];

    const removeTag = (index) => {
        setTags((prevTags) => prevTags.filter((_, i) => i !== index));
    };

    return (
        <>
            <div className={`fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 flex md:relative px-10 md:px-0 overflow-auto ${showHideUploadPopup && "md:hidden"}`}>
                <div className="max-w-[475px] w-full rounded-xl bg-shades-50 py-6 m-auto max-h-[969px] md:max-w-full md:rounded-none md:py-4 md:max-h-full">
                    <div className="flex flex-row items-center justify-between mb-6 px-8 md:hidden">
                        <div>
                            <h3 className="paragraphLargeMedium">Add banner</h3>
                            <div className="flex flex-row items-center">
                                <p className="paragraphMediumItalic text-neutral-500">Create a new banner to add in the catalogue</p>
                            </div>
                        </div>
                        <span className="cursor-pointer" onClick={props.handleClickClose}>
                            <CloseIcon />
                        </span>
                    </div>

                    <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 px-8 md:px-4 cursor-pointer" onClick={() => props.handleClickClose()}>
                        <LeftArrowIcon className="rotate-90" />
                        <span className="ml-1">Back to banners</span>
                    </div>
                    <div className="pl-6 pr-[14px] md:pl-4 md:pr-0">
                        <div className="h-[690px] overflow-auto scrollbarStyle pr-[12px] md:overflow-hidden md:h-full md:pb-16 md:pr-4 pl-2">
                            <div className="mb-[13px] relative">
                                <DropDown label="Banner type" dropdownLabel="Primary banners" />
                            </div>

                            <div className="mb-4">
                                <div className="mb-2">
                                    <span className="paragraphSmallMedium">Select image</span>
                                    <span className="paragraphSmallItalic text-neutral-500 ml-1">(1080px x 1080px recommended)</span>
                                </div>

                                <div className={`flex flex-row ${showSelectContent && "hidden"} paragraphMediumRegular mobile:text-sm`}>
                                    <button className="justify-center h-12 border-neutral-300 rounded-md border w-1/2 mr-[8.5px] mobile:mr-1 cursor-pointer" onClick={uploadImageShowHideController}>
                                        Upload an image
                                    </button>
                                    <button className="justify-center h-12 border-neutral-300 rounded-md border w-1/2 ml-[8.5px] mobile:ml-1 cursor-pointer" onClick={handleClickSelectImageContent}>
                                        Select from library
                                    </button>
                                </div>

                                <div className={`${!showSelectContent ? "hidden" : "flex"} flex-row justify-between items-center`}>
                                    <img alt="" src={pizza1} className="min-w-[197px] h-[109px] rounded-md md:mr-[17px] md:min-w-[163px] mobile:min-w-0" />
                                    <div className="max-w-[197px] w-full md:max-w-full md: ">
                                        <div className="mb-1.5">
                                            <LargeTertiaryButton name="Replace image" />
                                        </div>
                                        <LargeTertiaryButton name="Upload another" />
                                    </div>
                                </div>
                            </div>

                            <div className="">
                                <div className="mb-0.5">
                                    <span className="paragraphSmallMedium">ગુજરાતી: Select image</span>
                                    <span className="paragraphSmallItalic text-neutral-500 ml-1">(1080px x 1080px recommended)</span>
                                </div>

                                <div className="flex flex-row mb-[13px] paragraphMediumRegular mobile:text-sm">
                                    <button className="justify-center py-3 border-neutral-300 rounded-md border w-1/2 mr-[8.5px] mobile:mr-1">Upload an image</button>
                                    <button className="justify-center py-3 border-neutral-300 rounded-md border w-1/2 ml-[8.5px] mobile:ml-1">Select from library</button>
                                </div>
                            </div>

                            <div className={`${!showSelectContent ? "hidden" : "flex"} flex-row justify-between items-center`}>
                                <img alt="" src={pizza1} className="min-w-[197px] h-[109px] rounded-md md:min-w-[163px] mobile:min-w-0" />
                                <div className="max-w-[197px] w-full md:max-w-full md:pl-[17px] mobile:min-w-max">
                                    <div className="mb-1.5">
                                        <LargeTertiaryButton name="Replace image" />
                                    </div>
                                    <LargeTertiaryButton name="Upload another" />
                                </div>
                            </div>

                            <div className="my-[13px]">
                                <DefaultInputField boxHeight="11" label="Banner title" placeholder="Enter banner title" />
                            </div>

                            <div className="mb-[13px] relative">
                                <DropDown label="Banner screen" dropdownLabel="Select target screen" buttonTextColor="neutral-300" />
                            </div>

                            <div className="mb-[13px] relative">
                                <DropDown label="Banner mode" dropdownLabel="Select banner mode" buttonTextColor="neutral-300" />
                            </div>
                            <div className="mb-[13px] relative">
                                <DropDown label="Banner mode" dropdownLabel="Select banner mode" buttonTextColor="neutral-300" />
                                <div className="flex flex-row">
                                    {Tags.map((el, index) => (
                                        <div className="mr-2" key={index}>
                                            <Tag tag={el} onClose={() => removeTag(index)} />
                                        </div>
                                    ))}
                                </div>
                            </div>

                            <div className="mb-[13px] relative">
                                <DropDown label="Banner validity" dropdownLabel="Always" />
                            </div>

                            <div className="flex flex-row">
                                <div className="w-1/2 mr-[19px] md:mr-[15px]">
                                    <span className="paragraphSmallMedium mb-1">Start date</span>
                                    <button className="border px-3 py-[11px] mobile:px-0 flex flex-row items-center rounded-md border-neutral-300 w-full">
                                        <CalenderIcon className="mx-1 mobile:ml-1.5" />
                                        <span className="paragraphMediumRegular mx-1 mobile:mx-0 mobile:text-sm">28 Nov 2022</span>
                                    </button>
                                </div>
                                <div className="w-1/2">
                                    <span className="paragraphSmallMedium mb-1">End date</span>
                                    <button className="border px-3 py-[11px] mobile:px-0 flex flex-row items-center rounded-md border-neutral-300 w-full">
                                        <CalenderIcon className="mx-1 mobile:ml-1.5" />
                                        <span className="paragraphMediumRegular mx-1 mobile:mx-0 mobile:text-sm">28 Nov 2022</span>
                                    </button>
                                </div>
                            </div>

                            <div className="relative mt-4">
                                <DropDown label="Banner timings" dropdownLabel="Full-time" menuItems={menuItemBannerTiming} />
                            </div>

                            <div className="mt-4">
                                <BannerTimeSlot dayLabel="Sundays" />
                            </div>

                            <div className="mt-4">
                                <BannerTimeSlot dayLabel="Mondays" />
                            </div>
                        </div>
                    </div>

                    <div className="flex flex-row mt-12 px-8 md:bottom-0 md:fixed md:w-full md:pt-2 md:shadow-dropShadow md:bg-white md:pb-1 md:px-4">
                        <div className="w-1/2 mr-[9.5px]">
                            <LargeDestructiveButton name="Discard" />
                        </div>
                        <div className="w-1/2 ml-[9.5px]">
                            <LargePrimaryButton name="Add banner" disabled={true} />
                        </div>
                    </div>
                </div>
            </div>
            <div className={`${!showHideUploadPopup && "hidden"}`}>
                <UploadImage isShowBackArrow={true} showHideUploadImagePage={uploadImageShowHideController} selectType={selectType} page="banner" backToWhichPageName="banners" />
            </div>
        </>
    );
}
