import React from "react";
import Logo from "../../Assets/orderloop-logo.svg";
import { LargePrimaryButton } from "../../Components/Buttons/Button";
import LogoMobile from "../../Assets/orderloop-logo-mobile.svg";
import { ReactComponent as DownArrow } from "../../Assets/chevron-down.svg";

export default function Login() {
  return (
    <>
      <div className="max-w-[1440px] w-full mx-auto bg-white min-h-[1024px] md:min-h-0 flex flex-row justify-between h-screen">
        <div className="h-full max-w-[825px] w-full bg-[#695AD3] lg:hidden px-[84px] pt-10 relative before:absolute before:bg-[#695AD3] before:w-screen before:right-full before:top-0 before:bottom-0"></div>
        <div className="max-w-[615px] w-full lg:bg-[#695AD3] lg:px-[76px] relative lg:max-w-full md:px-0 md:h-full">
          <div className="h-full bg-white px-4">
            <div className="mb-[184px] pt-10 md:pt-4 md:mb-[50px]">
              <img src={Logo} alt="Logo" className="w-[200px] mx-auto md:hidden" />
              <img src={LogoMobile} alt="Logo" className="hidden md:block" />
            </div>
            <div className="max-w-[375px] w-full mx-auto md:max-w-full">
              <div className="mb-10 md:mb-[25px]">
                <h1 className="font-bold headingH1BoldDesktop md:text-primary-500 text-primary-500 md:headingH3BoldDesktop mb-2">Successful reset!</h1>
                <p className="font-normal text-sm leading-5 ">Your password has been successfully reset. Click below to sign in with your newly reset password.</p>
              </div>

              <div className="mb-6">
                <LargePrimaryButton name="Continue" />
              </div>

              <div className="text-center">
                <a href="/" className=" items-center justify-center flex">
                  <div className="">
                    <DownArrow fill="#6C5DD3" className="mr-0.5 w-full h-full" />
                  </div>
                  <span className="text-primary-500 text-base font-medium leading-6">Back to sign in</span>
                </a>
              </div>
            </div>

            <div className="text-center md:fixed md:bottom-0 md:pb-2 md:pt-2.5 md:left-0 md:right-0 md:bg-white hidden md:block">
              <span className="text-sm font-medium leading-5">New to OrderLoop?</span>
              <a href="/">
                <span className="text-primary-500 text-sm font-medium ml-0.5">Contact us</span>
              </a>
            </div>
            <div className="text-center absolute bottom-10 left-0 right-0 text-neutral-500 leading-5 font-normal text-xs md:hidden">&copy; 2022 Orderloop. All rights reserved.</div>
          </div>
        </div>
      </div>
    </>
  );
}
