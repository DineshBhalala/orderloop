import React from "react";
import Logo from "../../Assets/orderloop-logo.svg";
import { DefaultInputField } from "../../Components/InputField/InputField";
import { ReactComponent as EmailIcon } from "../../Assets/email-icon.svg";
import { ReactComponent as Password } from "../../Assets/password.svg";
import { LargePrimaryButton } from "../../Components/Buttons/Button";
import { ReactComponent as VisibleIcon } from "../../Assets/visible-icon.svg";
import LogoMobile from "../../Assets/orderloop-logo-mobile.svg";
import LoginSlider from "./LoginSlider";
import { CheckBox } from "../../Components/FormControl/FormControls";
import { useNavigate } from "react-router-dom";

export default function Login() {
    const navigate = useNavigate();

    const handleClickSignIn = () => {
        navigate("/select-restaurant");
    };

    return (
        <>
            <div className="max-w-[1440px] w-full mx-auto bg-white min-h-[1024px] md:min-h-0 flex flex-row justify-between h-screen">
                <div className="h-full max-w-[825px] w-full bg-[#695AD3] xl:hidden px-4 pt-10 relative before:absolute before:bg-[#695AD3] before:w-screen before:right-full before:top-0 before:bottom-0">
                    <div className="w-full h-full bg-[#4B39C9] rounded-t-[325px] overflow-hidden max-w-[657px] mx-auto">
                        <div className="text-center">
                            <LoginSlider />
                        </div>
                    </div>
                </div>

                <div className="max-w-[615px] w-full xl:bg-[#695AD3] xl:px-[76px] relative xl:max-w-full md:px-0 md:h-full">
                    <div className="h-full bg-white px-4">
                        <div className="mb-[184px] pt-10 md:pt-4 md:mb-[50px]">
                            <img src={Logo} alt="Logo" className="w-[200px] mx-auto md:hidden" />
                            <img src={LogoMobile} alt="Logo" className="hidden md:block" />
                        </div>

                        <div className="max-w-[375px] w-full mx-auto md:max-w-full">
                            <div className="mb-10 md:mb-[25px]">
                                <h1 className="text-primary-500 headingH1BoldDesktop md:text-primary-500 md:headingH3BoldDesktop mb-2">Hello there!</h1>
                                <p className="paragraphSmallRegular">Sign in and start your wonderful day with the Orderloop.</p>
                            </div>

                            <div className="mb-6">
                                <DefaultInputField label="Email" placeholder="Enter email address" placeholderIcon={<EmailIcon />} />
                            </div>

                            <div className="mb-10 md:mb-6">
                                <DefaultInputField label="Password" placeholder="Enter password" placeholderIcon={<Password />} addonIcon={<VisibleIcon />} />
                            </div>

                            <div className="flex flex-row justify-between mb-[26px]">
                                <CheckBox label="Remember Me" optionId="checkBox01" />
                                <a href="/">
                                    <span className="paragraphSmallMedium text-primary-500">Forgot password?</span>
                                </a>
                            </div>

                            <div className="mb-6" onClick={handleClickSignIn}>
                                <LargePrimaryButton name="Sign In" />
                            </div>

                            <div className="text-center md:fixed md:bottom-0 md:pb-2 md:pt-2.5 md:left-0 md:right-0 md:bg-white">
                                <span className="paragraphSmallMedium">New to OrderLoop?</span>
                                <a href="/">
                                    <span className="text-primary-500 paragraphSmallMedium ml-0.5">Contact us</span>
                                </a>
                            </div>
                        </div>

                        <div className="text-center absolute bottom-10 left-0 right-0 text-neutral-500 paragraphXSmallRegular md:hidden">&copy; 2022 Orderloop. All rights reserved.</div>
                    </div>
                </div>
            </div>
        </>
    );
}
