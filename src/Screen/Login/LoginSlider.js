import React from 'react'
import Slider from "react-slick";

const LoginSlider = () => {
    var settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        className: 'loginSlide',
    };
    return (
        <>
            <Slider {...settings}>
                <div>
                    <div className='w-full max-w-[631px] mx-auto mt-[35px]'><img src='LoginSliders/login-slide01.png' alt='Login Slider' /></div>
                    <div className='displaySmallMedium pt-[19px] px-4 pb-6 text-white'><div>Digitize your</div> restaurant's operations</div>
                    <p className='paragraphLargeRegular text-white px-36'>Manage everything from handling restaurant services to settle billing digitally now with Orderloop</p>
                </div>
                <div>
                    <div className='w-full max-w-[631px] mx-auto mt-[35px]'><img src='LoginSliders/login-slide02.png' alt='Login Slider' /></div>
                    <div className='displaySmallMedium pt-[19px] px-4 pb-6 text-white'>Trusted by <div>1000+ restaurants</div></div>
                    <p className='paragraphLargeRegular text-white px-36'>{`Orderloop is actively working with 1000+ restaurants(and growing) across the country to make their brand grow.`}</p>
                </div>
                <div>
                    <div className='w-full max-w-[631px] mx-auto mt-[35px]'><img src='LoginSliders/login-slide03.png' alt='Login Slider' /></div>
                    <div className='displaySmallMedium pt-[19px] px-4 pb-6 text-white'><div>Discover your</div> brand's potential</div>
                    <p className='paragraphLargeRegular text-white px-36'>With Orderloop, you can create a steady flow of customers who are loyal towards your brand.</p>
                </div>
            </Slider>
        </>
    )
}

export default LoginSlider