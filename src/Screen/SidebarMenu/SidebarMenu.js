import React, { useEffect, useState, useRef } from "react";
import { Outlet, Link, useLocation } from "react-router-dom";
import { ReactComponent as OrderLoopLogo } from "../../Assets/orderloop-logo.svg";
import { ReactComponent as OrderLoopLogoMobile } from "../../Assets/orderloop-logo-mobile.svg";
import { ReactComponent as Dashboard } from "../../Assets/dashboard.svg";
import { ReactComponent as KdsIcon } from "../../Assets/kitchen-display-system.svg";
import { ReactComponent as AbandonedCart } from "../../Assets/abandoned-cart.svg";
import { ReactComponent as Menu } from "../../Assets/menu.svg";
import { ReactComponent as MediaLibrary } from "../../Assets/media-library.svg";
import { ReactComponent as Availability } from "../../Assets/availability.svg";
import { ReactComponent as OrderRatings } from "../../Assets/order-ratings.svg";
import { ReactComponent as Customers } from "../../Assets/customers.svg";
import { ReactComponent as Notifications } from "../../Assets/notifications.svg";
import { ReactComponent as Offers } from "../../Assets/offers.svg";
import { ReactComponent as Banners } from "../../Assets/banners.svg";
import { ReactComponent as Credit } from "../../Assets/credits.svg";
import { ReactComponent as Settings } from "../../Assets/settings.svg";
import { ReactComponent as Reports } from "../../Assets/reports.svg";
import { ReactComponent as Riders } from "../../Assets/riders.svg";
import { ReactComponent as BillPayments } from "../../Assets/bill-payments.svg";
import { ReactComponent as TableManagement } from "../../Assets/table-management.svg";
import { ReactComponent as Cashback } from "../../Assets/cashback.svg";
import { ReactComponent as HelpDocumentation } from "../../Assets/help-documentation.svg";
import { ReactComponent as Hamburger } from "../../Assets/hamburger.svg";
import { ReactComponent as POSIcon } from "../../Assets/pos.svg";
import { ReactComponent as LiveOrderIcon } from "../../Assets/orders.svg";
import { ReactComponent as TableViewIcon } from "../../Assets/tableView.svg";
import { ReactComponent as DeliveryIcon } from "../../Assets/order.svg";
import ToggleSwitch from "../../Components/ToggleSwitch/ToggleSwitch";
import DropDownOutlet from "../../Components/DropDown/DropDownOutlet";
import DropDownAvatar from "../../Components/DropDown/DropDownAvatar";
import AvatarsIcon from "../../Components/Avatar/AvatarsIcon";
import AvatarImage from "../../Assets/avatar-image.png";
import HoldOrder from "../POS/HoldOrder";

const SidebarMenu = (props) => {
    const [windowWidth, setWindowWidth] = useState(window.innerWidth);

    const handleResize = () => {
        setWindowWidth(window.innerWidth);
    };

    window.onresize = handleResize;

    const sideMenu = useRef(null);

    const handleClick = () => {
        sideMenu.current.classList.toggle("menuExpand");
    };

    const hoverClass = "m-auto hidden group-hover:block activeMenuIcon";

    const defaultClass = "m-auto group-hover:hidden MenuIcon";

    const url = useLocation().pathname;

    const secondSlashIndex = url.indexOf("/", url.indexOf("/") + 1);

    const updatedUrl = secondSlashIndex !== -1 ? url.substring(0, secondSlashIndex) : url;

    const links = [
        {
            name: "Dashboard",
            icon: <Dashboard className={defaultClass} />,
            iconHover: <Dashboard className={hoverClass} stroke="#FAFAFA" />,
            to: "dashboard",
        },
        {
            name: "POS",
            icon: <POSIcon className={defaultClass} />,
            iconHover: <POSIcon className={hoverClass} stroke="#FAFAFA" />,
            to: "pos",
            subLinks: [
                {
                    name: "Orders",
                    icon: <LiveOrderIcon className={defaultClass} />,
                    iconHover: <LiveOrderIcon className={hoverClass} stroke="#FAFAFA" />,
                    to: "pos/orders",
                },
                {
                    name: "Table View",
                    icon: <TableViewIcon className={defaultClass} />,
                    iconHover: <TableViewIcon className={hoverClass} stroke="#FAFAFA" />,
                    to: "pos/table-view",
                },
                {
                    name: "Delivery",
                    icon: <DeliveryIcon className={defaultClass} />,
                    iconHover: <DeliveryIcon className={hoverClass} stroke="#FAFAFA" />,
                    to: "pos/delivery",
                },
            ],
        },
        {
            name: "Abandoned carts",
            icon: <AbandonedCart className={defaultClass} />,
            iconHover: <AbandonedCart className={hoverClass} stroke="#FAFAFA" />,
            to: "abandoned-carts",
        },
        {
            name: "KDS",
            icon: <KdsIcon className={defaultClass} />,
            iconHover: <KdsIcon className={hoverClass} stroke="#FAFAFA" />,
            to: "kitchen-display-system",
        },
        {
            name: "Menu management",
            icon: <Menu className={defaultClass} />,
            iconHover: <Menu className={hoverClass} stroke="#FAFAFA" />,
            to: "menu",
        },
        {
            name: "Media library",
            icon: <MediaLibrary className={defaultClass} />,
            iconHover: <MediaLibrary className={hoverClass} stroke="#FAFAFA" />,
            to: "media-library",
        },
        {
            name: "Item availability",
            icon: <Availability className={defaultClass} />,
            iconHover: <Availability className={hoverClass} stroke="#FAFAFA" />,
            to: "item-availability",
        },
        {
            name: "Order ratings",
            icon: <OrderRatings className={defaultClass} />,
            iconHover: <OrderRatings className={hoverClass} stroke="#FAFAFA" />,
            to: "order-ratings",
        },
        {
            name: "Customers",
            icon: <Customers className={defaultClass} />,
            iconHover: <Customers className={hoverClass} stroke="#FAFAFA" />,
            to: "customer-information",
        },
        {
            name: "Notifications",
            icon: <Notifications className={defaultClass} />,
            iconHover: <Notifications className={hoverClass} stroke="#FAFAFA" />,
            to: "notifications",
        },
        {
            name: "Offers",
            icon: <Offers className={defaultClass} />,
            iconHover: <Offers className={hoverClass} stroke="#FAFAFA" />,
            to: "offers",
        },
        {
            name: "Banners",
            icon: <Banners className={defaultClass} />,
            iconHover: <Banners className={hoverClass} stroke="#FAFAFA" />,
            to: "banners",
        },
        {
            name: "Credits",
            icon: <Credit className={defaultClass} />,
            iconHover: <Credit className={hoverClass} stroke="#FAFAFA" />,
            to: "credits",
        },
        {
            name: "Settings",
            icon: <Settings className={defaultClass} />,
            iconHover: <Settings className={hoverClass} stroke="#FAFAFA" />,
            to: "settings",
        },
        {
            name: "Reports",
            icon: <Reports className={defaultClass} />,
            iconHover: <Reports className={hoverClass} stroke="#FAFAFA" />,
            to: "reports",
        },
        {
            name: "Riders",
            icon: <Riders className={defaultClass} />,
            iconHover: <Riders className={hoverClass} stroke="#FAFAFA" />,
            to: "riders",
        },
        {
            name: "Bill payments",
            icon: <BillPayments className={defaultClass} />,
            iconHover: <BillPayments className={hoverClass} stroke="#FAFAFA" />,
            to: "bill-payments",
        },
        {
            name: "Table management",
            icon: <TableManagement className={defaultClass} />,
            iconHover: <TableManagement className={hoverClass} stroke="#FAFAFA" />,
            to: "table-management",
        },
        {
            name: "Loyalty cashback",
            icon: <Cashback className={defaultClass} />,
            iconHover: <Cashback className={hoverClass} stroke="#FAFAFA" />,
            to: "loyalty-cashback",
        },
        {
            name: "Help documentation",
            icon: <HelpDocumentation className={defaultClass} />,
            iconHover: <HelpDocumentation className={hoverClass} stroke="#FAFAFA" />,
            to: "help-documentation",
        },
    ];

    const [pageLabel, setPageLabel] = useState("Dashboard");

    useEffect(() => {
        const matchedLink = links.find((link) => link.to === updatedUrl.slice(1));
        const newLabel = matchedLink ? matchedLink.name : "Dashboard";
        setPageLabel(newLabel);
    }, [url]);

    const [isHeaderSticky, setHeaderSticky] = useState(false);

    useEffect(() => {
        window.addEventListener("scroll", () => {
            setHeaderSticky(window.scrollY > 0);
        });
    }, []);

    const [showSubLinks, setShowSubLinks] = useState(false);

    const handleToggleSubLinks = () => {
        setShowSubLinks(!showSubLinks);
    };

    const handleClickMobile = () => {
        windowWidth < 768 && sideMenu.current.classList.toggle("menuExpand");
        setShowSubLinks(false);
    };

    return (
        <>
            <div className="flex flex-row justify-between items-start h-full bg-[#FAFAFA] pageContent:bg-transparent">
                <div
                    ref={sideMenu}
                    className={`menuLeft bg-neutral-50 w-full max-w-[104px] xl:max-w-[72px] border-r border-neutral-300 fixed left-0 top-0 bottom-0 z-20 md:fixed md:z-[999] md:hidden h-screen 3xl:h-full ${
                        props.enableFullScreenMode ? "hidden" : "inline-table"
                    }`}
                >
                    <div className="mx-[18px] xl:mx-2 pt-[23px] h-20 border-b border-neutral-300 sticky top-0 md:absolute md:left-0 md:right-0 bg-neutral-50 menuBig">
                        <OrderLoopLogo className="mx-auto orderLoopLogo" />
                        <OrderLoopLogoMobile className="mx-auto orderLoopLogoSmall" />
                    </div>

                    <div className="mx-[18px] xl:mx-2 py-4 menuBig">
                        <div className="h-full max-h-screen pr-[5px] scrollbarStyle overflow-auto pb-[166px] md:pb-[150px] md:pt-20">
                            {links.map((link, index) => {
                                return (
                                    <div key={index}>
                                        <Link key={index} to={link.to} className={`${(updatedUrl === "/" + link.to || (updatedUrl === "/" && index === 0)) && "activeMenu"} xl:w-11 block`}>
                                            <div
                                                className="flex items-center h-14 xl:h-11 mb-2 rounded-xl xl:rounded-[10px] hover:bg-primary-500 group activeMenuBg cursor-pointer"
                                                onClick={link.subLinks ? handleToggleSubLinks : handleClickMobile}
                                            >
                                                <div className="w-14">
                                                    {link.icon}
                                                    {link.iconHover}
                                                </div>

                                                <span className="menuText w-[196px] text-base font-normal leading-6 text-neutral-900 group-hover:text-white">{link.name}</span>
                                            </div>
                                        </Link>

                                        {link.subLinks && showSubLinks && (
                                            <div className="border-b border-neutral-300 mb-2 subMenuLinks">
                                                {link.subLinks.map((subLink, subIndex) => (
                                                    <Link
                                                        key={subIndex}
                                                        to={subLink.to}
                                                        className={`${(url.substring(1) === subLink.to || (url === "/pos" && subLink.to === "pos/orders")) && "activeMenu"} block`}
                                                    >
                                                        <div className="subMenuLink flex items-center h-10 w-10 xl:h-9 xl:w-0 mx-auto mb-2 rounded-full cursor-pointer hover:bg-primary-500 group activeMenuBg">
                                                            <div className="w-10">
                                                                {subLink.icon}
                                                                {subLink.iconHover}
                                                            </div>

                                                            <span className="menuText w-[196px] text-base font-normal leading-6 text-neutral-900 group-hover:text-white">{subLink.name}</span>
                                                        </div>
                                                    </Link>
                                                ))}
                                            </div>
                                        )}
                                    </div>
                                );
                            })}
                        </div>
                    </div>

                    <div className="flex items-center justify-center mx-[18px] xl:mx-2 py-4 switchBig bg-neutral-50 border-t border-neutral-300 sticky bottom-0 md:absolute md:left-0 md:right-0">
                        <span className="paragraphMediumRegular menuText">Dock navigation</span>

                        <span>
                            <ToggleSwitch />
                        </span>
                    </div>
                </div>

                <div className={`w-full h-screen md:pt-[108px] ${!props.enableFullScreenMode && "pl-[104px] pt-20"} xl:pl-[72px] md:pl-0 contentMain`}>
                    <div
                        className={`flex ${
                            props.enableFullScreenMode ? "hidden" : "flex"
                        } items-center justify-between bg-neutral-50 w-[-webkit-fill-available] h-20 fixed top-0 border-b border-neutral-300 px-[35px] lg:px-4 z-10 md:hidden menuTop`}
                        style={{ width: "-moz-available" }}
                    >
                        <div className="flex items-center relative">
                            <div className="mr-[19px] cursor-pointer" onClick={handleClick}>
                                <Hamburger />
                            </div>

                            <div>
                                <DropDownOutlet label={pageLabel} />
                            </div>
                        </div>

                        <div>
                            <div className="flex items-center flex-row">
                                <div className={`border-r border-neutral-300 mr-4 pr-4 ${!props.isHoldOrder && "hidden"}`}>
                                    <HoldOrder />
                                </div>

                                <div className="flex items-center h-10 border-r border-neutral-300">
                                    <span className="mr-7 cursor-pointer">
                                        <Settings stroke="#706E7E" width="20px" height="20px" />
                                    </span>

                                    <span className="mr-7 cursor-pointer">
                                        <Notifications stroke="#706E7E" width="18px" height="18px" />
                                    </span>
                                </div>

                                <div>
                                    <DropDownAvatar label="Shyam T." widthIcon="16" heightIcon="16" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div
                        className={`hidden md:flex items-center justify-between bg-primary-500 pl-5 pr-4 fixed top-0 left-0 right-0 z-10 transitionEffect ${
                            isHeaderSticky ? "py-2.5 shadow-shadowWhite" : "pt-[60px] pb-4"
                        }`}
                    >
                        <div onClick={handleClick} className="cursor-pointer">
                            <Hamburger stroke="#FFFFFF" width="28" height="28" />
                            <span className="hidden menuOverlay"></span>
                        </div>

                        <span className="paragraphLargeSemiBold text-white mobile:paragraphMediumSemiBold mobile:text-white">{pageLabel}</span>

                        <span>
                            <AvatarsIcon widthOuter="8" heightOuter="8" imageIcon={AvatarImage} colorIcon="[#22C55E]" widthIcon="3" heightIcon="3" />
                        </span>
                    </div>
                    <Outlet />
                </div>
            </div>
        </>
    );
};
export default SidebarMenu;
