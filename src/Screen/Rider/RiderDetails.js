import React, { useState } from "react";
import { LargePrimaryButton } from "../../Components/Buttons/Button";
import riderImage from "../../Assets/rider-image.png";
import DashBoardCard from "../../Components/DashBoardCard/DashBoardCard";
import { ReactComponent as RiderIcon } from "../../Assets/riders.svg";
import { ReactComponent as Calender } from "../../Assets/calendar.svg";
import { ReactComponent as Filter } from "../../Assets/filter.svg";
import { ReactComponent as Export } from "../../Assets/export.svg";
import { ReactComponent as OrderIcon } from "../../Assets/order.svg";
import { ReactComponent as DistanceIcon } from "../../Assets/distance.svg";
import { ReactComponent as CashIcon } from "../../Assets/cash.svg";
import { ReactComponent as OrderRatingIcon } from "../../Assets/order-ratings.svg";
import { ReactComponent as TimerIcon } from "../../Assets/timer.svg";
import { ReactComponent as LeftArrowIcon } from "../../Assets/chevron-down.svg";
import PaginationNumber from "../../Components/Pagination/PaginationWithNumber";
import OrderDetails from "./OrderDetails";
import { DefaultBreadcrumbs } from "../../Components/Breadcrumbs/Breadcrumbs";
import ListViewRiderDetail from "../../Components/ListView/ListViewRiderDetail";
import ListViewBillDetails from "../../Components/ListView/ListViewBillDetails";
import SliderDashboard from "react-slick";
import { useNavigate } from "react-router-dom";

export default function RiderDetails() {
    const orderDetails = [
        {
            orderId: "#BBQR",
            billName: "Riddhi Shah",
            orderData: "18 Nov 2022",
            orderAmount: "₹1,000.00/-",
            deliveryDistance: "05 Kms",
            deliveryCost: "₹50.00/-",
            deliveryTime: "15 mins",
            riderRating: Number(5.0),
        },
        {
            orderId: "#BBQR",
            billName: "Riddhi Shah",
            orderData: "18 Nov 2022",
            orderAmount: "₹1,000.00/-",
            deliveryDistance: "05 Kms",
            deliveryCost: "₹50.00/-",
            deliveryTime: "15 mins",
            riderRating: Number(3.5),
        },
        {
            orderId: "#BBQR",
            billName: "Riddhi Shah",
            orderData: "18 Nov 2022",
            orderAmount: "₹1,000.00/-",
            deliveryDistance: "05 Kms",
            deliveryCost: "₹50.00/-",
            deliveryTime: "15 mins",
            riderRating: Number(3),
        },
        {
            orderId: "#BBQR",
            billName: "Riddhi Shah",
            orderData: "18 Nov 2022",
            orderAmount: "₹1,000.00/-",
            deliveryDistance: "05 Kms",
            deliveryCost: "₹50.00/-",
            deliveryTime: "15 mins",
            riderRating: Number(1),
        },
        {
            orderId: "#BBQR",
            billName: "Riddhi Shah",
            orderData: "18 Nov 2022",
            orderAmount: "₹1,000.00/-",
            deliveryDistance: "05 Kms",
            deliveryCost: "₹50.00/-",
            deliveryTime: "15 mins",
            riderRating: Number(5),
        },
    ];

    const [showTotalSalesCard, setShowTotalSalesCard] = useState(false);

    const handleClickTotalSalesCard = () => {
        setShowTotalSalesCard(!showTotalSalesCard);
    };
    var settingsDashboardSlider = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        className: "dashboardSlide",
    };

    const navigate = useNavigate();

    const handleClickBack = () => {
        navigate("/riders");
    };

    return (
        <>
            <div className="bg-[#fafafa]">
                <div className={`px-8 pb-[70px] lg:px-4 lg:pb-[23px] pt-4 w-full max-w-[1336px] bg-white mx-auto md ${showTotalSalesCard && "md:hidden"}`}>
                    <div className={``}>
                        <div className="md:hidden">
                            <DefaultBreadcrumbs mainTab="Riders" tab2="Rider details" icon={<RiderIcon className="h-5 w-5" />} />
                        </div>

                        <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer" onClick={handleClickBack}>
                            <LeftArrowIcon className="rotate-90" />
                            <span className="ml-1">Back to riders</span>
                        </div>

                        <div className="mt-4 items-center flex flex-row border-b pb-4 border-neutral-300 md:hidden xl:items-start">
                            <img src={riderImage} alt="" className="h-[120px] w-[120px]" />
                            <div className="w-full pr-[9px] py-1.5 pl-6 xl:py-0">
                                <div className="flex flex-row justify-between">
                                    <div className="flex flex-row xl:flex-col">
                                        <div className="mr-[103px]">
                                            <div className="mt-1.5 pb-1.5">
                                                <span className="paragraphMediumSemiBold text-neutral-400">Name:</span>
                                                <span className="ml-1 paragraphMediumRegular">Amrendra Bahubali</span>
                                            </div>
                                            <div className="mt-1.5 pb-1.5">
                                                <span className="paragraphMediumSemiBold text-neutral-400">Orders delivered:</span>
                                                <span className="ml-1 paragraphMediumRegular">1,235</span>
                                            </div>
                                        </div>

                                        <div>
                                            <div className="mt-1.5 pb-1.5">
                                                <span className="paragraphMediumSemiBold text-neutral-400">Mobile number:</span>
                                                <span className="ml-1 paragraphMediumRegular">(+91) 8816681288</span>
                                            </div>
                                            <div className="mt-1.5 pb-1.5">
                                                <span className="paragraphMediumSemiBold text-neutral-400">Last order:</span>
                                                <span className="ml-1 paragraphMediumRegular">18 Nov 2022</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="flex flex-row xl:flex-col">
                                        <div className="mr-[103px] xl:mr-0">
                                            <div className="mt-1.5 pb-1.5">
                                                <span className="paragraphMediumSemiBold text-neutral-400">Delivery radius:</span>
                                                <span className="ml-1 paragraphMediumRegular">15 kms</span>
                                            </div>
                                            <div className="mt-1.5 pb-1.5">
                                                <span className="paragraphMediumSemiBold text-neutral-400">Join date:</span>
                                                <span className="ml-1 paragraphMediumRegular">13 Feb 2021</span>
                                            </div>
                                        </div>

                                        <div>
                                            <div className="mt-1.5 pb-1.5">
                                                <span className="paragraphMediumSemiBold text-neutral-400">Delivery rate:</span>
                                                <span className="ml-1 paragraphMediumRegular">₹75.00/-</span>
                                            </div>
                                            <div className="mt-1.5 pb-1.5">
                                                <span className="paragraphMediumSemiBold text-neutral-400">Rider avg. rating:</span>
                                                <span className="ml-1 paragraphMediumRegular">4.0</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="mt-1.5 pb-1.5 flex flex-row">
                                    <span className="paragraphMediumSemiBold text-neutral-400 min-w-[112px]">Rider address:</span>
                                    <span className="ml-1 paragraphMediumRegular">House name - ABC, XYZ street, Kalawad Road, Rajkot, Gujarat, India - 360005</span>
                                </div>
                            </div>
                        </div>

                        <div className="border-b pb-4 mb-4 hidden md:block">
                            <ListViewRiderDetail riderName="Mahendra Bahubali" orderRatting={3.0} />
                        </div>

                        <div className="flex flex-row justify-between mb-2 pt-4 lg:mb-3 md:hidden">
                            <div className="flex flex-row">
                                <button className="border px-[11px] h-12 flex flex-row items-center rounded-md border-neutral-300 md:w-full">
                                    <Calender className="mx-1" />
                                    <span className="paragraphMediumRegular mx-1">25 Sept 2022 - 09 Oct 2022</span>
                                </button>
                                <div className="w-[120px] mx-4">
                                    <LargePrimaryButton leftIconDefault={<Filter fill="#ffffff" />} leftIconClick={<Filter fill="#C4BEED" />} name="Filters" />
                                </div>
                            </div>
                            <div className="">
                                <div className="w-[161px]">
                                    <LargePrimaryButton leftIconDefault={<Export stroke="#ffffff" />} leftIconClick={<Export stroke="#C4BEED" />} name="Export data" />
                                </div>
                            </div>
                        </div>

                        <div className="-mx-[11px] lg:-mx-0 mb-4 lg:mb-3 md:hidden">
                            <div className="w-[303px] inline-block mx-2.5 lg:w-1/2 lg:mx-0 lg:pr-1 lg:my-1 mt-2 pb-2 lg:mt-1 lg:pb-1">
                                <DashBoardCard
                                    title="Total orders delivered"
                                    amount={750}
                                    reimbursement="Number of orders delivered by the rider this week"
                                    isReimbursementIcon={false}
                                    icon={<OrderIcon stroke="#ffffff" />}
                                />
                            </div>
                            <div className="w-[303px] inline-block mx-2.5 lg:w-1/2 lg:mx-0 lg:pl-1 lg:my-1 mt-2 pb-2 lg:mt-1 lg:pb-1">
                                <DashBoardCard
                                    title="Total distance travelled"
                                    amount="245 kms"
                                    reimbursement="Total distance travelled by the rider this week"
                                    isReimbursementIcon={false}
                                    icon={<DistanceIcon stroke="#ffffff" />}
                                />
                            </div>
                            <div className="w-[303px] inline-block mx-2.5 lg:w-1/2 lg:mx-0 lg:pr-1 lg:my-1 mt-2 pb-2 lg:mt-1 lg:pb-1">
                                <DashBoardCard
                                    title="Total amount earned"
                                    amount="₹36,000/-"
                                    reimbursement="Total amount earned by the rider this week"
                                    isReimbursementIcon={false}
                                    icon={<CashIcon stroke="#ffffff" />}
                                />
                            </div>
                            <div className="w-[303px] inline-block mx-2.5 lg:w-1/2 lg:mx-0 lg:pl-1 lg:my-1 mt-2 pb-2 lg:mt-1 lg:pb-1">
                                <DashBoardCard
                                    title="Average rider rating"
                                    amount={4.0}
                                    reimbursement="Rider rating received was good by 1.5k customers this week"
                                    isReimbursementIcon={false}
                                    reimbursementFocusContent="1.5k"
                                    focusReimbursementContentColor="tertiary-800"
                                    icon={<RiderIcon stroke="#ffffff" />}
                                />
                            </div>
                        </div>

                        <div className="hidden md:block -mx-4">
                            <div>
                                <SliderDashboard {...settingsDashboardSlider}>
                                    <DashBoardCard
                                        title="Total orders delivered"
                                        amount={750}
                                        reimbursement="Number of orders delivered by the rider this week"
                                        isReimbursementIcon={false}
                                        icon={<OrderIcon stroke="#ffffff" />}
                                    />
                                    <DashBoardCard
                                        title="Total distance travelled"
                                        amount="245 kms"
                                        reimbursement="Total distance travelled by the rider this week"
                                        isReimbursementIcon={false}
                                        icon={<DistanceIcon stroke="#ffffff" />}
                                    />
                                    <DashBoardCard
                                        title="Total amount earned"
                                        amount="₹36,000/-"
                                        reimbursement="Total amount earned by the rider this week"
                                        isReimbursementIcon={false}
                                        icon={<CashIcon stroke="#ffffff" />}
                                    />
                                    <DashBoardCard
                                        title="Average rider rating"
                                        amount={4.0}
                                        reimbursement="Rider rating received was good by 1.5k customers this week"
                                        isReimbursementIcon={false}
                                        reimbursementFocusContent="1.5k"
                                        focusReimbursementContentColor="tertiary-800"
                                        icon={<RiderIcon stroke="#ffffff" />}
                                    />
                                </SliderDashboard>
                            </div>
                        </div>
                        <div className="w-full border border-neutral-300 rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden">
                            <table className="w-full break-words">
                                <thead>
                                    <tr className="bg-neutral-50 text-left paragraphOverlineSmall text-neutral-700 shadow-innerShadow h-11 justify-center">
                                        <th className="px-6 py-3 min-w-[130px]">ORDER ID</th>
                                        <th className="px-6 py-3 min-w-[194px]">BILL NAME</th>
                                        <th className="px-6 py-3 min-w-[148px]">ORDER DATE</th>
                                        <th className="px-6 py-3 min-w-[159px]">ORDER AMOUNT</th>
                                        <th className="px-6 py-3 min-w-[188px]">DELIVERY DISTANCE</th>
                                        <th className="px-6 py-3 min-w-[157px]">DELIVERY COST</th>
                                        <th className="px-6 py-3 min-w-[161px]">DELIVERY TIME</th>
                                        <th className="px-6 py-3 min-w-[149px]">RIDER RATING</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {orderDetails.map((el, index) => {
                                        return (
                                            <tr className={`paragraphSmallRegular ${index !== 0 && "border-t"} odd:bg-neutral-50 h-[70px] justify-center border-neutral-300`} key={index}>
                                                <td className="px-6">{el.orderId}</td>
                                                <td className="px-6 cursor-pointer" onClick={handleClickTotalSalesCard}>
                                                    {el.billName}
                                                </td>
                                                <td className="px-6">{el.orderData}</td>
                                                <td className="px-6">{el.orderAmount}</td>
                                                <td className="px-6">{el.deliveryDistance}</td>
                                                <td className="px-6">{el.deliveryCost}</td>
                                                <td className="px-6">
                                                    <div className="flex flex-row items-center">
                                                        <TimerIcon className="h-5 w-5 mr-1.5" />
                                                        {el.deliveryTime}
                                                    </div>
                                                </td>
                                                <td className="px-6">
                                                    <div className="flex flex-row">
                                                        <OrderRatingIcon
                                                            className="h-5 w-5 mr-1.5"
                                                            fill={el.riderRating > 3 ? "#EBF6F5" : el.riderRating === 3 ? "#FFF5E3" : "#FEF2F2"}
                                                            stroke={el.riderRating > 3 ? "#3D8C82" : el.riderRating === 3 ? "#FFA704" : "#EF4444"}
                                                        />
                                                        {el.riderRating.toFixed(1)}
                                                    </div>
                                                </td>
                                            </tr>
                                        );
                                    })}
                                </tbody>
                            </table>
                        </div>

                        <div className="hidden md:block mb-12 mt-4">
                            {orderDetails.map((el, index) => {
                                return (
                                    <div className="mt-1 pb-1" key={index}>
                                        <ListViewBillDetails
                                            orderId={el.orderId}
                                            billName={el.billName}
                                            orderData={el.orderData}
                                            orderAmount={el.orderAmount}
                                            deliveryDistance={el.deliveryDistance}
                                            deliveryCost={el.deliveryCost}
                                            deliveryTime={el.deliveryTime}
                                            riderRating={el.riderRating}
                                            handleClickTotalSalesCard={handleClickTotalSalesCard}
                                        />
                                    </div>
                                );
                            })}
                        </div>

                        <div className="pt-4 md:hidden">
                            <PaginationNumber />
                        </div>
                    </div>
                </div>
                <div className={`${!showTotalSalesCard && "hidden"}`}>
                    <OrderDetails handleClickTotalSalesCard={handleClickTotalSalesCard} />
                </div>

                <div
                    className={`${
                        showTotalSalesCard && "hidden"
                    } md:px-4 md:pt-2 md:pb-1 md:bg-white md:fixed md:bottom-0 md:left-0 md:right-0 md:shadow-dropShadow max-w-[464px] w-full  md:max-w-full`}
                >
                    <div className="hidden md:flex flex-row px-4 py-3 border border-neutral-300 justify-between items-center w-full rounded-md">
                        <div className="flex flex-row items-center">
                            <div className="h-6 w-6 rounded border-[1.5px] bg-tertiary-100 border-tertiary-800 mr-1" />
                            <span className="paragraphMediumRegular text-[#000000]">Good</span>
                        </div>
                        <div className="flex flex-row items-center">
                            <div className="h-6 w-6 rounded border-[1.5px] bg-secondary-100 border-secondary-700 mr-1" />
                            <span className="paragraphMediumRegular text-[#000000]">Average</span>
                        </div>
                        <div className="flex flex-row items-center">
                            <div className="h-6 w-6 rounded border-[1.5px] bg-destructive-50 border-destructive-500 mr-1" />
                            <span className="paragraphMediumRegular text-[#000000]">Bad</span>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
