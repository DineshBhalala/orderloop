import React, { useState } from "react";
import { ReactComponent as Close } from "../..//Assets/close.svg";
import { ReactComponent as SuccessTickIcon } from "../..//Assets/success-tick.svg";
import { ReactComponent as RiderIcon } from "../..//Assets/riders.svg";
import { ReactComponent as CardIcon } from "../..//Assets/card.svg";
import { ReactComponent as OrderRatingIcon } from "../..//Assets//order-ratings.svg";
import { ReactComponent as LeftArrow } from "../../Assets/chevron-down.svg";

export default function OrderDetails(props) {
    const [isShoRiderFeedBack, setIsShoRiderFeedBack] = useState(false);
    const [ShowHideText, setShowHideText] = useState("Show");
    const handleClickShowHide = () => {
        setIsShoRiderFeedBack(!isShoRiderFeedBack);
        setShowHideText(isShoRiderFeedBack ? "Show" : "Hide");
    };

    return (
        <>
            <div className="fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 flex justify-center items-center overflow-auto md:bg-white md:relative">
                <div className="max-w-[652px] w-full rounded-xl md:rounded-none bg-shades-50 px-8 py-6 m-auto md:max-w-full md:px-4 md:py-4">
                    <div className="hidden md:flex paragraphMediumMedium flex-row items-center mb-4 cursor-pointer" onClick={() => props.handleClickTotalSalesCard()}>
                        <LeftArrow className="rotate-90" />
                        <span className="ml-1">Back to rider details</span>
                    </div>

                    <div className="flex flex-row justify-between items-center mb-2 pb-4 border-b border-neutral-300 md:items-start mobile:block">
                        <div>
                            <span className="paragraphMediumMedium">Customer order details</span>
                            <div className="flex flex-row items-center mt-1">
                                <span className="paragraphMediumItalic text-neutral-500">Order #SAWQ</span>
                                <div className="w-1 h-1 rounded-full mx-3 bg-neutral-500 md:hidden" />
                                <div className="flex flex-row items-center md:hidden">
                                    <SuccessTickIcon />
                                    <span className="paragraphSmallRegular ml-1.5">Delivered</span>
                                </div>
                            </div>
                        </div>
                        <div className="md:hidden cursor-pointer" onClick={() => props.handleClickTotalSalesCard()}>
                            <Close />
                        </div>
                        <div className="flex-row items-center hidden md:flex">
                            <SuccessTickIcon />
                            <span className="paragraphSmallRegular ml-1">Delivered</span>
                        </div>
                    </div>
                    <div className="flex flex-row justify-between pb-4 border-b border-neutral-300 mb-4 md:block">
                        <div>
                            <div className="mb-1.5 pt-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Order bill name:</span>
                                <span className="paragraphMediumRegular ml-2">Mahendra Bahubali</span>
                            </div>
                            <div className="mb-1.5 pt-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Order date:</span>
                                <span className="paragraphMediumRegular ml-2">13 Nov 2022</span>
                            </div>
                            <div className="mb-1.5 pt-1.5 flex flex-row items-center">
                                <span className="paragraphMediumMedium text-neutral-500">Order type:</span>
                                <span className="paragraphMediumRegular ml-2 items-center flex flex-row">
                                    <RiderIcon className="mr-1" />
                                    Delivery
                                </span>
                            </div>
                            <div className="mb-1.5 pt-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Dishes ordered:</span>
                                <span className="paragraphMediumRegular ml-2">12</span>
                            </div>
                            <div className="pt-1.5 mb-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Delivery distance:</span>
                                <span className="paragraphMediumRegular ml-2">15 kms</span>
                            </div>
                        </div>
                        <div>
                            <div className="mb-1.5 pt-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Customer's order:</span>
                                <span className="paragraphMediumRegular ml-2">713</span>
                            </div>
                            <div className="mb-1.5 pt-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Order time:</span>
                                <span className="paragraphMediumRegular ml-2">17:59 PM</span>
                            </div>
                            <div className="mb-1.5 pt-1.5 flex flex-row items-center">
                                <span className="paragraphMediumMedium text-neutral-500">Payment method:</span>
                                <span className="paragraphMediumRegular ml-2 items-center flex flex-row">
                                    <CardIcon className="mr-1" />
                                    Debit card
                                </span>
                            </div>
                            <div className="mb-1.5 pt-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Delivery time:</span>
                                <span className="paragraphMediumRegular ml-2">1 hr 45 mins</span>
                            </div>
                            <div className="pt-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Delivery cost:</span>
                                <span className="paragraphMediumRegular ml-2">₹750.00/-</span>
                            </div>
                        </div>
                    </div>
                    <div className="flex flex-row justify-between items-center mb-4 pb-4 border-b">
                        <h3 className="paragraphMediumMedium">Order summary</h3>
                        <span className="paragraphMediumRegular text-primary-500">Show</span>
                    </div>
                    <div className="flex flex-row justify-between items-center mb-4 pb-4 border-b">
                        <div className="flex flex-row items-center">
                            <h3 className="paragraphMediumMedium">Total bill amount</h3>
                            <span className="text-success-600 leading-3 text-[10px] border font-medium px-2 py-1 border-success-600 rounded bg-success-50 h-5 ml-2">Paid</span>
                        </div>
                        <span className="paragraphMediumMedium">₹5,325.00/-</span>
                    </div>
                    <div className="flex flex-row justify-between items-center mb-4 pb-4 border-b">
                        <h3 className="paragraphMediumMedium">Order feedback</h3>
                        <span className="paragraphMediumRegular text-primary-500">Show</span>
                    </div>
                    <div className="border-b border-neutral-300 pb-4 mb-4">
                        <div className="flex flex-row justify-between items-center">
                            <h3 className="paragraphMediumMedium">Rider feedback</h3>
                            <span className="paragraphMediumRegular text-primary-500 cursor-pointer" onClick={handleClickShowHide}>
                                {ShowHideText}
                            </span>
                        </div>
                        <div className={`mt-4 ${!isShoRiderFeedBack && "hidden"}`}>
                            <div>
                                <h3 className="paragraphMediumItalic mb-2 text-neutral-500">Rating given on</h3>
                                <span className="font-normal text-base leading-6">13 Nov 2022</span>
                            </div>

                            <div className="mt-3">
                                <h3 className="paragraphMediumItalic mb-2 text-neutral-500">Rider rating</h3>
                                <div className="flex flex-row items-center">
                                    <OrderRatingIcon fill="#FEF2F2" stroke="#EF4444" />
                                    <div className="w-1 h-1 mx-3 bg-neutral-500" />
                                    <span className="font-normal text-base leading-6">1.0 star</span>
                                </div>
                            </div>

                            <div className="mt-3">
                                <h3 className="paragraphMediumItalic mb-2 text-neutral-500">Rider review</h3>
                                <p className="font-normal text-base leading-6">
                                    Rider was very rude and came very late. Even arriving very late and being rude, he pointed out me for not writing address properly. Didn't like the overall delivery
                                    of the order. Disappointed!
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="flex flex-row justify-between items-center">
                        <h3 className="paragraphMediumMedium">Order history</h3>
                        <span className="paragraphMediumRegular text-primary-500">Show</span>
                    </div>
                </div>
            </div>
        </>
    );
}
