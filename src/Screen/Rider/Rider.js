import React, { useState } from "react";
import { LargePrimaryButton } from "../../Components/Buttons/Button";
import { ReactComponent as RiderIcon } from "../../Assets/riders.svg";
import { ReactComponent as Filter } from "../../Assets/filter.svg";
import { ReactComponent as Export } from "../../Assets/export.svg";
import { ReactComponent as AddIcon } from "../../Assets/add.svg";
import { ReactComponent as OrderIcon } from "../../Assets/order.svg";
import { ReactComponent as CashIcon } from "../../Assets/cash.svg";
import { ReactComponent as OrderRatingIcon } from "../../Assets/order-ratings.svg";
import { ReactComponent as SelectIcon } from "../../Assets/select.svg";
import DashBoardCard from "../../Components/DashBoardCard/DashBoardCard";
import PaginationNumber from "../../Components/Pagination/PaginationWithNumber";
import { useNavigate } from "react-router-dom";
import AddRider from "./AddRider";
import { DefaultBreadcrumbs } from "../../Components/Breadcrumbs/Breadcrumbs";
import ListViewRider from "../../Components/ListView/ListViewRider";
import CalenderField from "../../Components/Calender/CalenderField";
import SliderDashboard from "react-slick";
import { ReactComponent as LeftArrowIcon } from "../../Assets/chevron-down.svg";

export default function Rider() {
    const navigate = useNavigate();

    const orderDetails = [
        {
            riderName: "Riddhi Shah",
            mobile: "(+91) 8866886688",
            joinDate: "18 Nov 2022",
            deliveryRadius: "05",
            rate: "₹100.00/-",
            riderRating: Number(5.0),
        },
        {
            riderName: "Riddhi Shah",
            mobile: "(+91) 8866886688",
            joinDate: "18 Nov 2022",
            deliveryRadius: "05",
            rate: "₹100.00/-",
            riderRating: Number(5.0),
        },
        {
            riderName: "Riddhi Shah",
            mobile: "(+91) 8866886688",
            joinDate: "18 Nov 2022",
            deliveryRadius: "05",
            rate: "₹100.00/-",
            riderRating: Number(5.0),
        },
        {
            riderName: "Riddhi Shah",
            mobile: "(+91) 8866886688",
            joinDate: "18 Nov 2022",
            deliveryRadius: "05",
            rate: "₹100.00/-",
            riderRating: Number(5.0),
        },
    ];

    const [showAddRiderPage, setShowAddRiderPage] = useState(false);

    const handleClickAddRider = () => {
        setShowAddRiderPage(!showAddRiderPage);
    };

    const handleClickName = () => {
        navigate("/rider-details");
    };

    const riderCardDetails = [
        {
            title: "Total riders",
            amount: "04",
            reimbursement: "Number of registered riders active this week",
            focusReimbursementContentColor: "tertiary-800",
            reimbursementFocusContent: "active",
            icon: <RiderIcon height={24} width={24} stroke="#ffffff" />,
        },
        {
            title: "Total expenditure",
            amount: "₹1,50,000.00/-",
            reimbursement: "Total money spent on delivering orders with 04 riders this week",
            focusReimbursementContentColor: "tertiary-800",
            reimbursementFocusContent: "04",
            icon: <CashIcon height={24} width={24} stroke="#ffffff" />,
        },
        {
            title: "Total orders delivered",
            amount: "750",
            reimbursement: "Number of orders delivered by 04 riders this week",
            focusReimbursementContentColor: "tertiary-800",
            reimbursementFocusContent: "04",
            icon: <OrderIcon height={24} width={24} stroke="#ffffff" />,
        },
        {
            title: "Average rider rating",
            startFillColor: "#FFF5E3",
            startStrokeColor: "#FFA704",
            amount: "3.0",
            ratingCard: true,
            reimbursement: "Rider rating received was good by 1.5k customers this week",
            isReimbursementIcon: false,
            reimbursementFocusContent: "1.5k",
            focusReimbursementContentColor: "tertiary-800",
            icon: <RiderIcon height={24} width={24} stroke="#ffffff" />,
        },
    ];

    var settingsDashboardSlider = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        className: "dashboardSlide",
    };

    return (
        <>
            <div className="bg-[#fafafa]">
                <div className="px-8 pb-[70px] lg:px-4 lg:pb-[23px] pt-4 w-full max-w-[1336px] mx-auto bg-white">
                    <div className={`${showAddRiderPage && "md:hidden"}`}>
                        
                        <div className="mb-4 md:hidden">
                            <DefaultBreadcrumbs mainTab="Riders" icon={<RiderIcon className="h-5 w-5" />} />
                        </div>

                        <div className="flex flex-row justify-between border-b border-neutral-300 pb-4 lg:mb-[7px] md:mb-4 md:block">
                            <div className="flex flex-row md:justify-between">
                                <CalenderField label="25 Sept 2022 - 09 Oct 2022" buttonStyle="mobile:pr-0 mobile:pl-1 md:w-full" labelStyle="mobile:text-sm mobile:mx-0" />
                                <div className="max-w-[120px] w-full ml-4 lg:ml-2 md:max-w-[56px] mobile:md:max-w-[45px]">
                                    <LargePrimaryButton name="Filters" hideName="md:hidden" leftIconDefault={<Filter fill="#ffffff" />} leftIconClick={<Filter fill="#C4BEED" />} />
                                </div>
                            </div>
                            <div className="flex flex-row md:justify-between md:mt-4">
                                <div className="mx-4 md:w-1/2 md:ml-0 md:mr-2 mobile:mr-1">
                                    <LargePrimaryButton name="Export data" leftIconDefault={<Export stroke="#FFFFFF" />} leftIconClick={<Export stroke="#C4BEED" />} />
                                </div>
                                <div className="md:w-1/2 md:ml-2 mobile:ml-1 cursor-pointer" onClick={handleClickAddRider}>
                                    <LargePrimaryButton
                                        isDefault={false}
                                        name="Add rider"
                                        leftIconDefault={<AddIcon stroke="#FFFFFF" />}
                                        leftIconClick={<AddIcon stroke="#C4BEED" />}
                                        hideName="lg:hidden md:block"
                                    />
                                </div>
                            </div>
                        </div>

                        <div className="-mx-[11px] mb-4 lg:-mx-[1px] md:hidden">
                            {riderCardDetails.map((el, index) => (
                                <div key={index} className="max-w-[303px] w-full inline-block mx-2.5 mt-4 lg:max-w-full lg:w-1/2 lg:mx-0 even:lg:pl-1 odd:lg:pr-1 lg:mt-[9px]">
                                    <DashBoardCard {...el} />
                                </div>
                            ))}
                        </div>

                        <div className="hidden md:block -mx-4">
                            <SliderDashboard {...settingsDashboardSlider}>
                                {riderCardDetails.map((el, index) => {
                                    return (
                                        <div key={index}>
                                            <div className="w-[303px] mobile:w-[288px]">
                                                <DashBoardCard {...el} />
                                            </div>
                                        </div>
                                    );
                                })}
                            </SliderDashboard>
                        </div>

                        <div className="flex flex-row justify-between">
                            <div className="max-w-[156px] w-full mb-4 md:hidden">
                                <LargePrimaryButton name="Bulk select" leftIconDefault={<SelectIcon stroke="#ffffff" />} leftIconClick={<SelectIcon stroke="#C4BEED" />} />
                            </div>
                            <div className="md:px-4 md:pt-2 md:pb-1 md:bg-white md:fixed md:bottom-0 md:left-0 md:right-0 md:shadow-dropShadow max-w-[464px] w-full  md:max-w-full">
                                <div className="flex flex-row px-4 py-3 border border-neutral-300 justify-between items-center w-full rounded-md">
                                    <span className="md:hidden">Rating guide</span>
                                    <div className="flex flex-row items-center">
                                        <div className="h-6 w-6 rounded border-[1.5px] bg-tertiary-100 border-tertiary-800 mr-1" />
                                        <span className="paragraphMediumRegular text-[#000000]">Good</span>
                                    </div>
                                    <div className="flex flex-row items-center">
                                        <div className="h-6 w-6 rounded border-[1.5px] bg-secondary-100 border-secondary-700 mr-1" />
                                        <span className="paragraphMediumRegular text-[#000000]">Average</span>
                                    </div>
                                    <div className="flex flex-row items-center">
                                        <div className="h-6 w-6 rounded border-[1.5px] bg-destructive-50 border-destructive-500 mr-1" />
                                        <span className="paragraphMediumRegular text-[#000000]">Bad</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="w-full border border-neutral-300 rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden">
                            <table className="w-full break-words">
                                <thead>
                                    <tr className="bg-neutral-50 text-left paragraphOverlineSmall text-neutral-700 shadow-innerShadow justify-center h-11">
                                        <th className="px-6 py-3 min-w-[191px]">RIDER NAME</th>
                                        <th className="px-6 py-3 min-w-[195px]">MOBILE NUMBER</th>
                                        <th className="px-6 py-3 min-w-[162px]">JOIN DATE</th>
                                        <th className="px-6 py-3 min-w-[214px]">DELIVERY RADIUS</th>
                                        <th className="px-6 py-3 min-w-[195px]">DELIVERY RATE</th>
                                        <th className="px-6 py-3 min-w-[195px]">AVG. RIDER RATING</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    {orderDetails.map((el, index) => {
                                        return (
                                            <tr className={`paragraphSmallRegular ${index !== 0 && "border-t"} odd:bg-neutral-50 border-neutral-300 h-[70px] justify-center`} key={index}>
                                                <td className="px-6 cursor-pointer" onClick={handleClickName}>
                                                    {el.riderName}
                                                </td>
                                                <td className="px-6">{el.mobile}</td>
                                                <td className="px-6">{el.joinDate}</td>
                                                <td className="px-6">{el.deliveryRadius}</td>
                                                <td className="px-6">{el.rate}</td>
                                                <td className="px-6">
                                                    <div className="flex flex-row">
                                                        <OrderRatingIcon
                                                            className="h-5 w-5 mr-1.5"
                                                            fill={el.riderRating > 3 ? "#EBF6F5" : el.riderRating === 3 ? "#FFF5E3" : "#FEF2F2"}
                                                            stroke={el.riderRating > 3 ? "#3D8C82" : el.riderRating === 3 ? "#FFA704" : "#EF4444"}
                                                        />
                                                        {el.riderRating.toFixed(1)}
                                                    </div>
                                                </td>
                                            </tr>
                                        );
                                    })}
                                </tbody>
                            </table>
                        </div>

                        <div className="hidden md:block mb-12 mt-3">
                            {orderDetails.map((el, index) => {
                                return (
                                    <div className="mb-1 pt-1" key={index}>
                                        <ListViewRider riderName={el.riderName} riderRating={el.riderRating} handleClickViewDetail={handleClickName} />
                                    </div>
                                );
                            })}
                        </div>

                        <div className="pt-4 md:hidden">
                            <PaginationNumber />
                        </div>
                    </div>

                    <div className={`${!showAddRiderPage && "hidden"}`}>
                        <AddRider handleClickAddRider={handleClickAddRider} />
                    </div>
                </div>
            </div>
        </>
    );
}
