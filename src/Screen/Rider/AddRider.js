import React from "react";
import { ReactComponent as Close } from "../..//Assets/close.svg";
import { DefaultInputField, InputArea } from "../../Components/InputField/InputField";
import DropDown from "../../Components/DropDown/DropDown";
import { LargePrimaryButton } from "../../Components/Buttons/Button";
import { ReactComponent as LeftArrow } from "../../Assets/chevron-down.svg";

export default function AddRider(props) {
    const addRiderCardInputField = [
        { label: "First name", placeholder: "Enter first name" },
        {
            label: "Middle name",
            placeholder: "Enter middle name",
            subtitle: "(Optional)",
        },
        { label: "Last name", placeholder: "Enter last name" },
        { label: "Mobile number", placeholder: "Enter mobile number" },
        { label: "Delivery radius", placeholder: "Enter radius in kms" },
        { label: "Delivery rate", placeholder: "Enter price in rupees" },
        { label: "Rider age", placeholder: "Enter rider's age" },
        { label: "Fuel allowance", placeholder: "Enter fuel allowance in rupees" },
    ];

    const OutLetName = ["Domino's Pizza - Kalawad Road Outlet", "Domino's Pizza - Gondal Road Outlet", "Domino's Pizza - 150 Ft. Ring Road Outlet"];

    return (
        <>
            <div className="fixed bg-black bg-opacity-50 inset-0 z-50 md:z-[9] flex overflow-auto px-[33px] md:bg-white md:relative md:px-0">
                <div className="max-w-[830px] min-h-[888px] rounded-xl bg-shades-50 px-8 py-6 m-auto w-full md:max-w-full md:px-0 md:py-0 md:min-h-full">
                    <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer" onClick={() => props.handleClickAddRider()}>
                        <LeftArrow className="rotate-90" />
                        <span className="ml-1">Back to riders</span>
                    </div>

                    <div className="flex flex-row justify-between items-center border-neutral-300">
                        <div>
                            <span className="paragraphLargeMedium">Add rider</span>
                            <div className="flex flex-row items-center">
                                <span className="paragraphMediumItalic text-neutral-500">Register a new rider for your outlet</span>
                            </div>
                        </div>
                        <div onClick={props.handleClickAddRider} className="md:hidden cursor-pointer">
                            <Close />
                        </div>
                    </div>

                    <div className="mt-6 flex flex-row items-center md:block">
                        <p className="paragraphMediumMedium">Upload profile picture</p>
                        <span className="paragraphMediumItalic text-neutral-500 ml-1 md:ml-0">(1080px x 1080px recommended)</span>
                    </div>

                    <div className="mt-4 mb-2 md:flex md:flex-row">
                        <button className="px-[34.5px] py-[3px] rounded-md min-w-[197px] h-[48px] border-neutral-300 border paragraphMediumRegular mr-[17px] md:min-w-0 md:w-1/2 md:px-0 mobile:text-sm">
                            Upload an image
                        </button>
                        <button className="px-[34.5px] py-[3px] rounded-md min-w-[197px] h-[48px] border-neutral-300 border paragraphMediumRegular md:min-w-0 md:px-0 md:w-1/2 mobile:text-sm">
                            Select from library
                        </button>
                    </div>

                    <div className="">
                        {addRiderCardInputField.map((el, index) => {
                            return (
                                <div key={index} className="align-top inline-block w-1/2 even:pl-2 odd:pr-2 my-2">
                                    <DefaultInputField label={el.label} placeholder={el.placeholder} />
                                </div>
                            );
                        })}
                    </div>

                    <div className="relative mt-2 pb-2">
                        <DropDown label="Select rider outlets" subTitle="(Selected - 0)" dropdownLabel="Select rider's outlets" buttonTextColor="neutral-300" menuItems={OutLetName} type="addRider" />
                    </div>

                    <div className="mt-2 mb-12">
                        <InputArea label="Rider address" placeholder="Select rider's outlets" />
                    </div>
                    <div className="w-full">
                        <LargePrimaryButton name="Add rider" />
                    </div>
                </div>
            </div>
        </>
    );
}
