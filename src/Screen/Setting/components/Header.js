import React from "react";
import ToggleSwitch from "../../../Components/ToggleSwitch/ToggleSwitch";
import { DefaultInputField } from "../../../Components/InputField/InputField";
import DropDown from "../../../Components/DropDown/DropDown";

function Header(props) {
  const {
    hasInputField,
    hasDropDown,
    placeholder,
    showStatus,
    title,
    headerBottomLine,
    hasSwitch,
    labelMarginB,
    label,
    labelStyle,
    shadow,
    placeholderTextColor,
    menuItems,
    buttonTextColor,
    dropdownLabel,
    marginBetween,
    page,
  } = props;

  return (
    <div className={`max-w-[636px] ${page !== "menuManagement" && "lg:max-w-[462px]"} md:max-w-full`}>
      <div className="flex flex-row items-center justify-between mb-1">
        <h1 className={`paragraphLargeMedium ${page !== "menuManagement" && "md:paragraphMediumMedium"}`}>{title}</h1>
        {hasSwitch && <ToggleSwitch showStatus={showStatus} />}
      </div>
      <span className={`paragraphMediumItalic text-neutral-500 ${page !== "menuManagement" && "md:paragraphSmallItalic"}`}>{headerBottomLine}</span>
      {(hasInputField || hasDropDown) && (
        <div className={`${marginBetween ?? "mt-4"} max-w-[312px] w-full relative md:max-w-full`}>
          {hasInputField && (
            <DefaultInputField
              boxHeight="[52px]"
              labelMarginB={labelMarginB}
              label={label}
              labelStyle={labelStyle}
              placeholder={placeholder}
              placeholderTextColor={placeholderTextColor}
              shadow={shadow}
            />
          )}
          {hasDropDown && (
            <DropDown
              height="h-[52px]"
              label={label}
              buttonTextColor={buttonTextColor}
              dropdownLabel={dropdownLabel}
              menuItems={menuItems}
              labelMarginB={labelMarginB}
              labelStyle={labelStyle}
              shadow={shadow}
            />
          )}
        </div>
      )}
    </div>
  );
}

export default Header;
