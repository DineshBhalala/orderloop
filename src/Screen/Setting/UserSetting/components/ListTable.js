import React from "react";
import { ReactComponent as EditIcon } from "../../../../Assets/edit.svg";
import { LinkOffer } from "../../../../Components/LinkOffer/LinkOffer";
import { ListViewUserSettingPresetList, ListViewUserSettingUserList } from "../../../../Components/ListView/ListViewUserSetting";
import ToggleSwitch from "../../../../Components/ToggleSwitch/ToggleSwitch";

export const UserList = (props) => {
  return (
    <>
      <div className="w-full rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden border-neutral-300 border">
        <table className="w-full break-words tableMediaLibrary">
          <thead>
            <tr className="paragraphOverlineSmall text-neutral-700 shadow-innerShadow bg-neutral-50 text-left justify-center h-11">
              <th className="px-6 min-w-[254px] lg:min-w-[215px]">USER NAME</th>
              <th className="px-6 min-w-[230px] lg:min-w-[165px]">USER ROLE</th>
              <th className="px-6 min-w-[271px] lg:min-w-[219px]">MOBILE NUMBER</th>
              <th className="px-6 min-w-[284px]">EMAIL ID</th>
              <th className="px-6 min-w-[231px]">OUTLET PERMISSION</th>
            </tr>
          </thead>
          <tbody>
            {props.userList.map((el, index) => {
              return (
                <tr className={`paragraphSmallRegular ${index !== 0 && "border-t"} border-neutral-300 justify-center h-[70px]`} key={index}>
                  <td className="px-6">
                    <div className="flex flex-row items-center">
                      <div className={`mr-4 ${!props.isEditList && "hidden"}`}>
                        <EditIcon />
                      </div>
                      <span>{el.userName}</span>
                    </div>
                  </td>
                  <td className="px-6">{el.userRole}</td>
                  <td className="px-6">{el.mobileNumber}</td>
                  <td className="px-6">{el.emailId}</td>
                  <td className="px-6">
                    <LinkOffer linkOfferNumber={el.outletPermission} />
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
      <div className="md:block hidden">
        {props.userList.map((el, index) => (
          <div className="mt-2" key={index}>
            <ListViewUserSettingUserList userRole={el.userRole} mobileNumber={el.mobileNumber} emailId={el.emailId} outletPermission={el.outletPermission} userName={el.userName} />
          </div>
        ))}
      </div>
    </>
  );
};

export const PresentList = (props) => {
  return (
    <>
      <div className="w-full rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden border-neutral-300 border">
        <table className="w-full break-words tableMediaLibrary">
          <thead>
            <tr className="paragraphOverlineSmall text-neutral-700 shadow-innerShadow bg-neutral-50 text-left justify-center h-11">
              <th className="px-6 min-w-[254px] lg:min-w-[215px]">PRESET NAME</th>
              <th className="px-6 min-w-[227px] lg:min-w-[189px]">LINKED USERS</th>
              <th className="px-6 min-w-[268px] lg:min-w-[207px]">CREATED BY</th>
              <th className="px-6 min-w-[278px]">CREATOR NUMBER</th>
              <th className="px-6 min-w-[243px]">CREATED ON</th>
            </tr>
          </thead>
          <tbody>
            {props.presentList.map((el, index) => {
              return (
                <tr className={`paragraphSmallRegular ${index !== 0 && "border-t"} border-neutral-300 justify-center h-[70px]`} key={index}>
                  <td className="px-6 cursor-pointer" onClick={props.isEditList ? props.handleClickEditPresetList : null}>
                    <div className="flex flex-row items-center">
                      <div className={`mr-4 ${!props.isEditList && "hidden"}`}>
                        <EditIcon />
                      </div>
                      <span>{el.presetName}</span>
                    </div>
                  </td>
                  <td className="px-6">{el.linkedUsers}</td>
                  <td className="px-6">{el.createdBy}</td>
                  <td className="px-6">{el.creatorNumber}</td>
                  <td className="px-6">{el.createdOn}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
      <div className="md:block hidden">
        {props.presentList.map((el, index) => (
          <div className="mt-2" key={index}>
            <ListViewUserSettingPresetList linkedUsers={el.linkedUsers} createdBy={el.createdBy} creatorNumber={el.creatorNumber} createdOn={el.createdOn} presetName={el.presetName} />
          </div>
        ))}
      </div>
    </>
  );
};

export const OutletPermissionTable = (props) => {
  return (
    <>
      <div className="w-full rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden border-neutral-300 border mb-6">
        <table className="w-full break-words tableMediaLibrary">
          <thead>
            <tr className="paragraphOverlineSmall text-neutral-700 shadow-innerShadow bg-neutral-50 text-left justify-center h-11">
              <th className="px-6 min-w-[102px] lg:min-w-[90px]">STATUS</th>
              <th className="px-6 min-w-[342px] lg:min-w-[302px]">OUTLET NAME</th>
              <th className="px-6 min-w-[102px] lg:min-w-[135px]">STATE</th>
              <th className="px-6 min-w-[85px] lg:min-w-[151px]">CITY</th>
              <th className="px-6 min-w-[407px]">ADDRESS</th>
            </tr>
          </thead>
          <tbody>
            {props.outletTableDetails.map((el, index) => {
              return (
                <tr className={`paragraphSmallRegular ${index !== 0 && "border-t"} justify-center h-[70px]`} key={index}>
                  <td className="px-6 border-t border-[#E2E8F0]">
                    <ToggleSwitch />
                  </td>
                  <td className="px-6 border-t border-neutral-300">{el.outletName}</td>
                  <td className="px-6">{el.state}</td>
                  <td className="px-6">{el.city}</td>
                  <td className="px-6">{el.address}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
};
