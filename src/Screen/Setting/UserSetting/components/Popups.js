import React, { useState } from "react";
import { ReactComponent as LeftArrow } from "../../../../Assets/chevron-down.svg";
import { ReactComponent as VisibleIcon } from "../../../../Assets/visible-icon.svg";
import { ReactComponent as CloseIcon } from "../../../../Assets/close.svg";
import { DefaultInputField } from "../../../../Components/InputField/InputField";
import DropDown from "../../../../Components/DropDown/DropDown";
import { LargeDestructiveButton, LargePrimaryButton } from "../../../../Components/Buttons/Button";
import PhoneInput from "react-phone-number-input";
import "../../../../index";
import "react-phone-number-input/style.css";
import { CheckBoxWithoutLabels } from "../../../../Components/FormControl/FormControls";
import { ListViewOutletPermission } from "../../../../Components/ListView/ListViewUserSetting";
import { OutletPermissionTable } from "./ListTable";

export const AddUserPopup = (props) => {
    const userRole = ["Owner", "Store Manager", "Captain"];

    const [value, setValue] = useState();

    const [isShowAddUserPopup, setIsShowAddUserPopup] = useState(true);

    const handleClickProceed = () => {
        setIsShowAddUserPopup(!isShowAddUserPopup);
    };

    return (
        <>
            <div className={`fixed bg-black bg-opacity-50 inset-0 z-50 flex ${!isShowAddUserPopup && "hidden"} md:px-0 md:absolute md:z-[9]`}>
                <div className={`max-w-[830px] w-full rounded-xl bg-shades-50 px-8 py-6 m-auto md:w-full md:px-4 md:py-4 lg:max-w-[439px] md:max-w-full md:rounded-none`}>
                    <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer" onClick={props.handleClickClose}>
                        <LeftArrow className="rotate-90" />
                        <span className="pl-1">Back to user list</span>
                    </div>

                    <div className="flex flex-row justify-between items-center border-neutral-300 mb-6 lg:mb-4">
                        <div>
                            <span className="paragraphLargeMedium">Add user</span>
                            <div className="flex flex-row items-center">
                                <span className="paragraphMediumItalic text-neutral-500">Create a new user profile to add them to the application</span>
                            </div>
                        </div>
                        <div onClick={props.handleClickClose} className="md:hidden cursor-pointer">
                            <CloseIcon />
                        </div>
                    </div>

                    <div className="flex flex-row items-center mb-4 lg:block md:mb-2">
                        <div className="w-1/2 mr-2 lg:w-full lg:mr-0 lg:mb-2 md:mb-4">
                            <DefaultInputField placeholder="Enter first name" label="User name" shadow="shadow-shadowXsmall" />
                        </div>
                        <div className="w-1/2 ml-2 relative lg:w-full lg:ml-0">
                            <DropDown label="User role" dropdownLabel="Enter middle name" buttonTextColor="neutral-300" menuItems={userRole} type="addUser" />
                        </div>
                    </div>

                    <div className="flex flex-row items-center mb-4 lg:block md:mb-2">
                        <div className="w-1/2 mr-2 lg:w-full lg:mr-0 lg:mb-2 md:mb-4">
                            <DefaultInputField placeholder="Enter email ID" label="Email ID" shadow="shadow-shadowXsmall" />
                        </div>
                        <div className="w-1/2 ml-2 relative lg:w-full lg:ml-0 removeSpinButton">
                            <DefaultInputField label="Mobile number" placeholder="Enter mobile number" paddingLeft="pl-[112px]" inputType="number" shadow="shadow-shadowXsmall" />
                            <div className="phoneinputbox absolute bottom-3 left-4">
                                <PhoneInput international countryCallingCodeEditable={false} defaultCountry="IN" value={value} onChange={setValue} />
                            </div>
                        </div>
                    </div>

                    <div className="flex flex-row items-center mb-12 lg:block">
                        <div className="w-1/2 mr-2 lg:w-full lg:mr-0 lg:mb-2 md:mb-4">
                            <DefaultInputField placeholder="Enter password" label="Password" inputType="password" addonIcon={<VisibleIcon />} shadow="shadow-shadowXsmall" />
                        </div>
                        <div className="w-1/2 ml-2 relative lg:w-full lg:ml-0">
                            <DefaultInputField label="Confirm password" placeholder="Re-type password" inputType="password" addonIcon={<VisibleIcon />} shadow="shadow-shadowXsmall" />
                        </div>
                    </div>

                    <div className="flex flex-row justify-between items-center md:fixed md:bottom-0 md:left-0 md:right-0 md:mb-0 md:shadow-dropShadow md:bg-white md:px-4 md:pt-2 md:pb-1">
                        <span className="paragraphLargeMedium">Step 1/3</span>
                        <div className="min-w-[164px] cursor-pointer" onClick={handleClickProceed}>
                            <LargePrimaryButton name="Proceed" isDefault={false} />
                        </div>
                    </div>
                </div>
            </div>
            <div className={`${isShowAddUserPopup && "hidden"}`}>
                <SelectAccessPermission handleClickClose={handleClickProceed} type="addUser" header="add user" />
            </div>
        </>
    );
};

export const CreatePreset = (props) => {
    const accessModuleList = [
        "Dashboard",
        "Customers",
        "Notification",
        "Offers",
        "Banners",
        "Outlet",
        "Live orders",
        "Past orders",
        "Menu",
        "Users",
        "Restaurant",
        "Order rating",
        "Riders",
        "Item availability",
        "Customer reward points",
        "Outlet mode",
        "Infrastructure credit",
        "Service credit",
        "Reports",
        "Table management",
        "Bill payment",
        "Media library",
    ];

    const handleChangeReadCheckBox = (object, isChecked) => {
    };

    const handleChangeWriteCheckBox = (object, isChecked) => {
    };

    const handleChangeDeleteCheckBox = (object, isChecked) => {
    };

    return (
        <>
            <div className="fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 flex overflow-auto md:px-0 md:absolute md:overflow-visible">
                <div className="max-w-[688px] w-full rounded-xl bg-shades-50 px-8 py-6 m-auto md:w-full md:px-4 md:py-4 md:max-w-full md:rounded-none">
                    <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer" onClick={props.handleClickClose}>
                        <LeftArrow className="rotate-90" />
                        <span className="pl-1">Back to preset list</span>
                    </div>

                    <div className="flex flex-row justify-between items-center border-neutral-300 mb-6">
                        <div>
                            <span className="paragraphLargeMedium">Create preset</span>
                            <div className="flex flex-row items-center">
                                <span className="paragraphMediumItalic text-neutral-500">Select a set of permissions you would like to add to the new preset</span>
                            </div>
                        </div>
                        <div onClick={props.handleClickClose} className="md:hidden cursor-pointer">
                            <CloseIcon />
                        </div>
                    </div>

                    <div className="mb-4">
                        <DefaultInputField placeholder="Enter preset name" shadow="shadow-shadowXsmall" />
                    </div>

                    <div className="flex flex-row paragraphMediumSemiBold">
                        <span className="min-w-[270px] md:min-w-[160px]">Access module</span>
                        <div className="flex flex-row justify-between w-full">
                            <span>Read</span>
                            <span>Write</span>
                            <span className="">Delete</span>
                        </div>
                    </div>

                    <div className="mb-12">
                        {accessModuleList.map((el, index) => {
                            return (
                                <div className="flex flex-row paragraphSmallRegular mt-2 items-center" key={index}>
                                    <span className="min-w-[270px] md:min-w-[160px]">{el}</span>
                                    <div className="flex flex-row justify-between w-full pl-2 pr-[14px]">
                                        <CheckBoxWithoutLabels isChecked={handleChangeReadCheckBox} object={el} />
                                        <CheckBoxWithoutLabels isChecked={handleChangeWriteCheckBox} object={el} />
                                        <CheckBoxWithoutLabels isChecked={handleChangeDeleteCheckBox} object={el} />
                                    </div>
                                </div>
                            );
                        })}
                    </div>

                    <div className="flex justify-end md:justify-center md:fixed md:bottom-0 md:left-0 md:right-0 md:mb-0 md:shadow-dropShadow md:bg-white md:px-4 md:pt-2 md:pb-1">
                        <div className="max-w-[153px] md:max-w-full md:w-full">
                            <LargePrimaryButton name="Save as preset" />
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export const SelectAccessPermission = (props) => {
    const accessModuleList = [
        "Dashboard",
        "Customers",
        "Notification",
        "Offers",
        "Banners",
        "Outlet",
        "Live orders",
        "Past orders",
        "Menu",
        "Users",
        "Restaurant",
        "Order rating",
        "Riders",
        "Item availability",
        "Customer reward points",
        "Outlet mode",
        "Infrastructure credit",
        "Service credit",
        "Reports",
        "Table management",
        "Bill payment",
        "Media library",
    ];

    const handleChangeReadCheckBox = (object, isChecked) => {
    };

    const handleChangeWriteCheckBox = (object, isChecked) => {
    };

    const handleChangeDeleteCheckBox = (object, isChecked) => {
    };

    const [isShowOutletPopup, setIsShowOutletPopup] = useState(false);

    const handleClickProceed = () => {
        setIsShowOutletPopup(!isShowOutletPopup);
    };

    return (
        <>
            <div className={`fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 flex overflow-auto ${isShowOutletPopup && "hidden"} md:px-0 md:absolute md:z-[9] md:overflow-visible`}>
                <div className="max-w-[688px] w-full rounded-xl bg-shades-50 px-8 py-6 m-auto md:w-full md:px-4 md:py-4 md:max-w-full md:rounded-none">
                    <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer" onClick={props.handleClickClose}>
                        <LeftArrow className="rotate-90" />
                        <span className="pl-1">Back to {props.header}</span>
                    </div>

                    <div className="flex flex-row justify-between items-center border-neutral-300 mb-6 md:mb-4">
                        <div>
                            <span className="paragraphLargeMedium">Select access permission</span>
                            <div className="flex flex-row items-center">
                                <span className="paragraphMediumItalic text-neutral-500">Select a set of permissions you would like to provide to the created user</span>
                            </div>
                        </div>
                        <div onClick={props.handleClickClose} className="md:hidden cursor-pointer">
                            <CloseIcon />
                        </div>
                    </div>

                    <div className="mb-4">
                        <DefaultInputField
                            placeholder="Delivery Staff"
                            Addon="Change"
                            shadow="shadow-shadowXsmall"
                            placeholderTextColor="neutral-900"
                            addonStyle="text-primary-500 font-normal text-sm leading-4 mt-0.5 border-b border-primary-500"
                        />
                    </div>

                    <div className="flex flex-row paragraphMediumSemiBold">
                        <span className="min-w-[270px]  md:min-w-[160px]">Access module</span>
                        <div className="flex flex-row justify-between w-full">
                            <span>Read</span>
                            <span>Write</span>
                            <span className="">Delete</span>
                        </div>
                    </div>

                    <div className="mb-12">
                        {accessModuleList.map((el, index) => {
                            return (
                                <div className="flex flex-row paragraphSmallRegular mt-2 items-center" key={index}>
                                    <span className="min-w-[270px]  md:min-w-[160px]">{el}</span>
                                    <div className="flex flex-row justify-between w-full pl-2 pr-[14px]">
                                        <CheckBoxWithoutLabels isChecked={handleChangeReadCheckBox} object={el} />
                                        <CheckBoxWithoutLabels isChecked={handleChangeWriteCheckBox} object={el} />
                                        <CheckBoxWithoutLabels isChecked={handleChangeDeleteCheckBox} object={el} />
                                    </div>
                                </div>
                            );
                        })}
                    </div>

                    {props.type === "addUser" ? (
                        <div className="flex flex-row justify-between items-center md:fixed md:bottom-0 md:left-0 md:right-0 md:mb-0 md:shadow-dropShadow md:bg-white md:px-4 md:pt-2 md:pb-1">
                            <span className="paragraphLargeMedium">Step 2/3</span>
                            {/* <div className="flex flex-row">
    <div className="mr-4">
      <LargePrimaryButton name="Overwrite changes" />
    </div>
    <div className="" onClick={handleClickProceed}>
    <LargePrimaryButton name="Proceed" isDefault={false} />
    </div>
  </div> */}
                            <div className="md:min-w-[235px] mobile:hidden" onClick={handleClickProceed}>
                                <LargePrimaryButton name="Save as preset & proceed" isDefault={false} />
                            </div>
                            <div className="mobile:block hidden" onClick={handleClickProceed}>
                                <LargePrimaryButton name="Save & proceed" isDefault={false} />
                            </div>
                        </div>
                    ) : (
                        <div className="flex justify-end md:fixed md:bottom-0 md:left-0 md:right-0 md:mb-0 md:shadow-dropShadow md:bg-white md:px-4 md:pt-2 md:pb-1 md:justify-center mobile:px-2">
                            <div className="flex flex-row md:w-full ">
                                <div className="mr-4 min-w-[168px] md:w-1/2 md:min-w-0 mobile:mr-2">
                                    <LargeDestructiveButton name="Discard changes" />
                                </div>
                                <div className="md:w-1/2">
                                    <LargePrimaryButton name="Save changes" />
                                </div>
                            </div>
                        </div>
                    )}
                </div>
            </div>
            <div className={`${!isShowOutletPopup && "hidden"}`}>
                <OutletPermission handleClickClose={handleClickProceed} />
            </div>
        </>
    );
};

export const OutletPermission = (props) => {
    const outletTableDetails = [
        {
            outletName: "Domino’s Pizza - Univerisity Road Outlet",
            state: "Gujarat",
            city: "Rajkot",
            address: "Shyamal Infinity, beneath Radio Mirchi, kalawad road",
        },
        {
            outletName: "Domino's Pizza - Master Outlet(All)",
            state: "Gujarat",
            city: "Rajkot",
            address: "Shyamal Infinity, beneath Radio Mirchi, kalawad road",
        },
        {
            outletName: "Domino's Pizza - Master Outlet(All)",
            state: "Gujarat",
            city: "Rajkot",
            address: "Shyamal Infinity, beneath Radio Mirchi, kalawad road",
        },
        {
            outletName: "Domino's Pizza - Master Outlet(All)",
            state: "Gujarat",
            city: "Rajkot",
            address: "Shyamal Infinity, beneath Radio Mirchi, kalawad road",
        },
        {
            outletName: "Domino's Pizza - Master Outlet(All)",
            state: "Gujarat",
            city: "Rajkot",
            address: "Shyamal Infinity, beneath Radio Mirchi, kalawad road",
        },
        {
            outletName: "Domino's Pizza - Master Outlet(All)",
            state: "Gujarat",
            city: "Rajkot",
            address: "Shyamal Infinity, beneath Radio Mirchi, kalawad road",
        },
        {
            outletName: "Domino's Pizza - Master Outlet(All)",
            state: "Gujarat",
            city: "Rajkot",
            address: "Shyamal Infinity, beneath Radio Mirchi, kalawad road",
        },
    ];
    return (
        <>
            <div className="fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 flex overflow-auto md:px-0 md:absolute md:overflow-visible px-12">
                <div className="max-w-[1102px] w-full rounded-xl bg-shades-50 px-8 py-6 m-auto md:w-full md:px-4 md:py-4 lg:max-w-[710px] md:max-w-full md:rounded-none">
                    <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer" onClick={props.handleClickClose}>
                        <LeftArrow className="rotate-90" />
                        <span className="pl-1">Back to select access permission</span>
                    </div>

                    <div className="flex justify-between items-center mb-6 md:mb-4">
                        <div>
                            <h3 className="paragraphLargeMedium">Outlet permission</h3>
                            <div className="flex flex-row items-center">
                                <p className="paragraphMediumItalic text-neutral-500">Link the user with the desired outlets</p>
                                <div className="w-1 h-1 rounded-full bg-neutral-500 mx-3 md:hidden" />
                                <span className="paragraphMediumItalic text-neutral-500 md:hidden">Outlet selected - 5</span>
                            </div>
                        </div>
                        <span className="cursor-pointer md:hidden" onClick={props.handleClickClose}>
                            <CloseIcon />
                        </span>
                    </div>

                    <OutletPermissionTable outletTableDetails={outletTableDetails} />

                    <div className="hidden md:block pb-14">
                        {outletTableDetails.map((el, index) => (
                            <div className="mt-2" key={index}>
                                <ListViewOutletPermission outletName={el.outletName} state={el.state} city={el.city} address={el.address} />
                            </div>
                        ))}
                    </div>

                    <div className="flex flex-row justify-between items-center md:fixed md:bottom-0 md:left-0 md:right-0 md:mb-0 md:shadow-dropShadow md:bg-white md:px-4 md:pt-2 md:pb-1 md:justify-between">
                        <span className="paragraphLargeMedium">Step 3/3</span>
                        <div className="min-w-[164px] cursor-pointer" onClick={props.handleClickClose}>
                            <LargePrimaryButton name="Add user" isDefault={false} />
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};
