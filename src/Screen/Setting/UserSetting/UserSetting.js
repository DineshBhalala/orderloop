import React, { useState } from "react";
import { LargePrimaryButton } from "../../../Components/Buttons/Button";
import { PresentList, UserList } from "./Components/ListTable";
import PaginationWithNumber from "../../../Components/Pagination/PaginationWithNumber";
import { ReactComponent as SelectIcon } from "../../../Assets/select.svg";
import { ReactComponent as EditIcon } from "../../../Assets/edit.svg";
import { ReactComponent as AddIcon } from "../../../Assets/add.svg";
import { ReactComponent as FilterIcon } from "../../../Assets/filter.svg";
import { Tab } from "../../../Components/Tabs/Tabs";
import { AddUserPopup, CreatePreset, SelectAccessPermission } from "./Components/Popups";
import { DropDownTabs } from "../../../Components/DropDown/DropDownTabs";

export default function UserSetting() {
    const userList = [
        {
            userName: "Amrendra Bahubali",
            userRole: "Captain",
            mobileNumber: "(+91) 8816681288",
            emailId: "ab2017@orderloop.in",
            outletPermission: "4",
        },
        {
            userName: "Amrendra Bahubali",
            userRole: "Captain",
            mobileNumber: "(+91) 8816681288",
            emailId: "ab2017@orderloop.in",
            outletPermission: "4",
        },
        {
            userName: "Amrendra Bahubali",
            userRole: "Captain",
            mobileNumber: "(+91) 8816681288",
            emailId: "ab2017@orderloop.in",
            outletPermission: "4",
        },
        {
            userName: "Amrendra Bahubali",
            userRole: "Captain",
            mobileNumber: "(+91) 8816681288",
            emailId: "ab2017@orderloop.in",
            outletPermission: "4",
        },
        {
            userName: "Amrendra Bahubali",
            userRole: "Captain",
            mobileNumber: "(+91) 8816681288",
            emailId: "ab2017@orderloop.in",
            outletPermission: "4",
        },
        {
            userName: "Amrendra Bahubali",
            userRole: "Captain",
            mobileNumber: "(+91) 8816681288",
            emailId: "ab2017@orderloop.in",
            outletPermission: "4",
        },
        {
            userName: "Amrendra Bahubali",
            userRole: "Captain",
            mobileNumber: "(+91) 8816681288",
            emailId: "ab2017@orderloop.in",
            outletPermission: "4",
        },
        {
            userName: "Amrendra Bahubali",
            userRole: "Captain",
            mobileNumber: "(+91) 8816681288",
            emailId: "ab2017@orderloop.in",
            outletPermission: "4",
        },
        {
            userName: "Amrendra Bahubali",
            userRole: "Captain",
            mobileNumber: "(+91) 8816681288",
            emailId: "ab2017@orderloop.in",
            outletPermission: "4",
        },
        {
            userName: "Amrendra Bahubali",
            userRole: "Captain",
            mobileNumber: "(+91) 8816681288",
            emailId: "ab2017@orderloop.in",
            outletPermission: "4",
        },
        {
            userName: "Amrendra Bahubali",
            userRole: "Captain",
            mobileNumber: "(+91) 8816681288",
            emailId: "ab2017@orderloop.in",
            outletPermission: "4",
        },
    ];

    const presentList = [
        { presetName: "Owners", linkedUsers: 2, createdBy: "Sarthak Kanchan", creatorNumber: "(+91) 8866886688", createdOn: "18 Nov 2022" },
        { presetName: "Store Manager", linkedUsers: 2, createdBy: "Sarthak Kanchan", creatorNumber: "(+91) 8866886688", createdOn: "18 Nov 2022" },
        { presetName: "Inventory Manager", linkedUsers: 2, createdBy: "Sarthak Kanchan", creatorNumber: "(+91) 8866886688", createdOn: "18 Nov 2022" },
        { presetName: "Captain", linkedUsers: 2, createdBy: "Sarthak Kanchan", creatorNumber: "(+91) 8866886688", createdOn: "18 Nov 2022" },
        { presetName: "Stewards", linkedUsers: 2, createdBy: "Sarthak Kanchan", creatorNumber: "(+91) 8866886688", createdOn: "18 Nov 2022" },
        { presetName: "Kitchen Staff", linkedUsers: 2, createdBy: "Sarthak Kanchan", creatorNumber: "(+91) 8866886688", createdOn: "18 Nov 2022" },
        { presetName: "Delivery Staff", linkedUsers: 2, createdBy: "Sarthak Kanchan", creatorNumber: "(+91) 8866886688", createdOn: "18 Nov 2022" },
        { presetName: "Accountants", linkedUsers: 2, createdBy: "Sarthak Kanchan", creatorNumber: "(+91) 8866886688", createdOn: "18 Nov 2022" },
    ];

    const [isShowUserList, setIsShowUserList] = useState(true);
    const [isShowAddUserPopup, setIsShowAddUserPopup] = useState(false);
    const [isShowAddPresetPopup, setIsShowAddPresetPopup] = useState(false);
    const [isShowAccessPermissionPopup, setIsShowAccessPermissionPopup] = useState(false);
    const [isEditTable, setIsEditTable] = useState(false);
    const [isEditRestoreToDefault, setIsEditRestoreToDefault] = useState(false);

    const handleClickAdd = () => {
        isShowUserList ? setIsShowAddUserPopup(!isShowAddUserPopup) : setIsShowAddPresetPopup(!isShowAddPresetPopup);
    };

    const handleClickEditPresetList = () => {
        setIsShowAccessPermissionPopup(!isShowAccessPermissionPopup);
    };

    const handleClickEdit = () => {
        setIsEditTable(!isEditTable);
        setIsEditRestoreToDefault(!isEditRestoreToDefault);
    };

    const handleClickPresetTab = () => {
        setIsShowUserList(false);
        setIsEditRestoreToDefault(false);
        isShowUserList && setIsEditTable(false);
    };

    const handleClickUserTab = () => {
        setIsEditRestoreToDefault(false);
        setIsShowUserList(true);
        !isShowUserList && setIsEditTable(false);
    };

    return (
        <>
            <div className="flex flex-row justify-between mb-4 md:block">
                <div className="flex flex-row items-center md:mb-4">
                    <div className="flex flex-row items-center mr-4 lg:mr-2 md:hidden">
                        <div className="max-w-[105px] mr-4 lg:mr-2 cursor-pointer" onClick={handleClickUserTab}>
                            <Tab label="Users list" isActive={isShowUserList && true} />
                        </div>

                        <div className="max-w-[118px] cursor-pointer" onClick={handleClickPresetTab}>
                            <Tab label="Presets list" isActive={!isShowUserList && true} />
                        </div>
                    </div>

                    <div className="md:block hidden w-full mr-2">
                        <DropDownTabs
                            menuItems={[
                                { item: "Users list", onClick: handleClickUserTab },
                                { item: "Presets list", onClick: handleClickPresetTab },
                            ]}
                        />
                    </div>

                    <div className="md:hidden">
                        <LargePrimaryButton name="Bulk select" leftIconDefault={<SelectIcon stroke="#FFFFFF" />} leftIconClick={<SelectIcon stroke="#C4BEED" />} />
                    </div>

                    <div className="hidden md:block min-w-[64px]">
                        <LargePrimaryButton leftIconDefault={<SelectIcon stroke="#FFFFFF" />} leftIconClick={<SelectIcon stroke="#C4BEED" />} />
                    </div>
                </div>

                <div className="flex flex-row md:hidden">
                    <div className={`${!isShowUserList && "hidden"}`}>
                        <LargePrimaryButton name="Filter" leftIconClick={<FilterIcon fill="#C4BEED" />} leftIconDefault={<FilterIcon fill="#FFFFFF" />} />
                    </div>

                    <div className={`ml-4 ${!isShowUserList && "hidden"} lg:hidden cursor-pointer`} onClick={handleClickEdit}>
                        <LargePrimaryButton name="Edit user" isClicked={isEditRestoreToDefault} leftIconDefault={<EditIcon stroke="#FFFFFF" />} leftIconClick={<EditIcon stroke="#C4BEED" />} />
                    </div>

                    <div className={`ml-4 ${!isShowUserList ? "hidden" : "lg:block hidden"} lg:ml-2 cursor-pointer`} onClick={handleClickEdit}>
                        <LargePrimaryButton isClicked={isEditRestoreToDefault} leftIconDefault={<EditIcon stroke="#FFFFFF" />} leftIconClick={<EditIcon stroke="#C4BEED" />} />
                    </div>

                    <div className={`ml-4 ${isShowUserList && "hidden"} lg:ml-2 cursor-pointer`} onClick={handleClickEdit}>
                        <LargePrimaryButton name="Edit preset" isClicked={isEditRestoreToDefault} leftIconDefault={<EditIcon stroke="#FFFFFF" />} leftIconClick={<EditIcon stroke="#C4BEED" />} />
                    </div>

                    <div className="ml-4 lg:hidden cursor-pointer" onClick={handleClickAdd}>
                        <LargePrimaryButton
                            name={isShowUserList ? "Add user" : "Add preset"}
                            isDefault={false}
                            leftIconDefault={<AddIcon stroke="#FFFFFF" />}
                            leftIconClick={<AddIcon stroke="#c4beed" />}
                        />
                    </div>

                    <div className="ml-2 lg:block hidden cursor-pointer" onClick={handleClickAdd}>
                        <LargePrimaryButton isDefault={false} leftIconDefault={<AddIcon stroke="#FFFFFF" />} leftIconClick={<AddIcon stroke="#c4beed" />} />
                    </div>
                </div>

                <div className={`flex-row ${!isShowUserList ? "hidden" : "md:flex hidden"}`}>
                    <div className="w-full mr-4 mobile:mr-2">
                        <LargePrimaryButton leftIconClick={<FilterIcon fill="#C4BEED" />} leftIconDefault={<FilterIcon fill="#FFFFFF" />} />
                    </div>

                    <div className="w-full mr-2 mobile:mr-1 cursor-pointer" onClick={handleClickEdit}>
                        <LargePrimaryButton isClicked={isEditRestoreToDefault} leftIconDefault={<EditIcon stroke="#FFFFFF" />} leftIconClick={<EditIcon stroke="#C4BEED" />} />
                    </div>

                    <div className="w-full ml-2 mobile:ml-1 cursor-pointer" onClick={handleClickAdd}>
                        <LargePrimaryButton isDefault={false} leftIconDefault={<AddIcon stroke="#FFFFFF" />} leftIconClick={<AddIcon stroke="#c4beed" />} />
                    </div>
                </div>

                <div className={`flex-row ${isShowUserList ? "hidden" : "md:flex hidden"}`}>
                    <div className="w-full mr-2 mobile:mr-1 cursor-pointer" onClick={handleClickEdit}>
                        <LargePrimaryButton name="Edit preset" isClicked={isEditRestoreToDefault} leftIconDefault={<EditIcon stroke="#FFFFFF" />} leftIconClick={<EditIcon stroke="#C4BEED" />} />
                    </div>

                    <div className="w-full ml-2 mobile:ml-1 cursor-pointer" onClick={handleClickAdd}>
                        <LargePrimaryButton name="Add preset" isDefault={false} leftIconDefault={<AddIcon stroke="#FFFFFF" />} leftIconClick={<AddIcon stroke="#c4beed" />} />
                    </div>
                </div>
            </div>

            <div className={`${isShowAddUserPopup && "md:hidden"}`}>
                {isShowUserList ? (
                    <UserList userList={userList} isEditList={isEditTable} />
                ) : (
                    <PresentList presentList={presentList} isEditList={isEditTable} handleClickEditPresetList={handleClickEditPresetList} />
                )}
            </div>

            <div className="md:hidden mt-6 pb-6">
                <PaginationWithNumber />
            </div>

            <div className={`${!isShowAddUserPopup && "hidden"}`}>
                <AddUserPopup handleClickClose={handleClickAdd} />
            </div>

            <div className={`${!isShowAddPresetPopup && "hidden"}`}>
                <CreatePreset handleClickClose={handleClickAdd} />
            </div>

            <div className={`${!isShowAccessPermissionPopup && "hidden"}`}>
                <SelectAccessPermission header="preset list" handleClickClose={handleClickEditPresetList} />
            </div>
        </>
    );
}
