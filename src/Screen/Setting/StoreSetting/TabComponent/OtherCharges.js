import React from "react";
import DropDown from "../../../../Components/DropDown/DropDown";
import { DefaultInputField } from "../../../../Components/InputField/InputField";
import Header from "../../Components/Header";

export default function OtherCharges() {
    const itemMenu = ["Yes", "No"];
    return (
        <>
            <div className="max-w-[636px] w-full md:max-w-full md:pb-20">
                <div className="pb-6 border-b border-neutral-300 mb-6">
                    <Header title="Keep same price for all ordering modes" hasSwitch />
                </div>
                <div className="mb-4">
                    <Header title="Delivery mode" headerBottomLine="These are the outlet-wise charges for the customers displayed in the cart section." />
                </div>
                <div className="flex flex-row lg:block">
                    <div className="max-w-[312px] w-full relative mb-4 mr-1.5 lg:mb-2 md:max-w-full">
                        <DropDown
                            height="h-[52px]"
                            label="(Enable packaging charge)"
                            labelMarginB="mb-2"
                            labelStyle="paragraphMediumItalic text-neutral-500"
                            shadow="shadow-smallDropDownShadow"
                            menuItems={itemMenu}
                        />
                    </div>
                    <div className="max-w-[312px] w-full relative mb-4 ml-1.5 lg:ml-0 md:max-w-full">
                        <DefaultInputField
                            boxHeight="[52px]"
                            labelMarginB="mb-2"
                            placeholder="Enter packaging charges in rupees"
                            label="(Packaging charge)"
                            labelStyle="paragraphMediumItalic text-neutral-500"
                            shadow="shadow-smallDropDownShadow"
                        />
                    </div>
                </div>
                <div className="flex flex-row mb-6 pb-6 border-b border-neutral-300 lg:block">
                    <div className="max-w-[312px] w-full relative mb-4 mr-1.5 lg:mb-2 md:max-w-full">
                        <DropDown
                            height="h-[52px]"
                            label="(Enable convenience charge)"
                            labelMarginB="mb-2"
                            labelStyle="paragraphMediumItalic text-neutral-500"
                            shadow="shadow-smallDropDownShadow"
                            menuItems={itemMenu}
                        />
                    </div>
                    <div className="max-w-[312px] w-full relative mb-4 ml-1.5 lg:mb-0 lg:ml-0 md:max-w-full">
                        <DefaultInputField
                            boxHeight="[52px]"
                            labelMarginB="mb-2"
                            placeholder="Enter convenience charges in rupees"
                            label="(Convenience charge)"
                            labelStyle="paragraphMediumItalic text-neutral-500"
                            shadow="shadow-smallDropDownShadow"
                        />
                    </div>
                </div>
                <div className="mb-4">
                    <Header title="Takeaway mode" headerBottomLine="These are the outlet-wise charges for the customers displayed in the cart section." />
                </div>
                <div className="flex flex-ro lg:block">
                    <div className="max-w-[312px] w-full relative mb-4 mr-1.5 lg:mb-2 md:max-w-full">
                        <DropDown
                            height="h-[52px]"
                            label="(Enable packaging charge)"
                            labelMarginB="mb-2"
                            labelStyle="paragraphMediumItalic text-neutral-500"
                            shadow="shadow-smallDropDownShadow"
                            menuItems={itemMenu}
                        />
                    </div>
                    <div className="max-w-[312px] w-full relative mb-4 ml-1.5 lg:ml-0 md:max-w-full">
                        <DefaultInputField
                            boxHeight="[52px]"
                            labelMarginB="mb-2"
                            label="(Packaging charge)"
                            placeholder="Enter packaging charges in rupees"
                            labelStyle="paragraphMediumItalic text-neutral-500"
                            shadow="shadow-smallDropDownShadow"
                        />
                    </div>
                </div>
                <div className="flex flex-row mb-6 pb-6 border-b border-neutral-300 lg:block">
                    <div className="max-w-[312px] w-full relative mb-4 mr-1.5 lg:mb-2 md:max-w-full">
                        <DropDown
                            height="h-[52px]"
                            label="(Enable packaging charge)"
                            labelMarginB="mb-2"
                            labelStyle="paragraphMediumItalic text-neutral-500"
                            shadow="shadow-smallDropDownShadow"
                            menuItems={itemMenu}
                        />
                    </div>
                    <div className="max-w-[312px] w-full relative mb-4 ml-1.5 lg:ml-0 lg:mb-0 md:max-w-full">
                        <DefaultInputField
                            boxHeight="[52px]"
                            labelMarginB="mb-2"
                            placeholder="Enter convenience charges in rupees"
                            label="(Convenience charge)"
                            labelStyle="paragraphMediumItalic text-neutral-500"
                            shadow="shadow-smallDropDownShadow"
                        />
                    </div>
                </div>
                <div className="mb-4">
                    <Header title="Dine in mode" headerBottomLine="These are the outlet-wise charges for the customers displayed in the cart section." />
                </div>
                <div className="flex flex-row lg:block">
                    <div className="max-w-[312px] w-full relative mb-4 mr-1.5 lg:mb-2 md:max-w-full">
                        <DropDown
                            height="h-[52px]"
                            label="(Enable packaging charge)"
                            labelMarginB="mb-2"
                            labelStyle="paragraphMediumItalic text-neutral-500"
                            shadow="shadow-smallDropDownShadow"
                            menuItems={itemMenu}
                        />
                    </div>
                    <div className="max-w-[312px] w-full relative mb-4 ml-1.5 lg:ml-0 md:max-w-full">
                        <DefaultInputField
                            boxHeight="[52px]"
                            labelMarginB="mb-2"
                            label="(Packaging charge)"
                            placeholder="Enter packaging charges in rupees"
                            labelStyle="paragraphMediumItalic text-neutral-500"
                            shadow="shadow-smallDropDownShadow"
                        />
                    </div>
                </div>
                <div className="flex flex-row mb-6 lg:block">
                    <div className="max-w-[312px] w-full relative mb-4 mr-1.5 lg:mb-2 md:max-w-full">
                        <DropDown
                            height="h-[52px]"
                            label="(Enable packaging charge)"
                            labelMarginB="mb-2"
                            labelStyle="paragraphMediumItalic text-neutral-500"
                            shadow="shadow-smallDropDownShadow"
                            menuItems={itemMenu}
                        />
                    </div>
                    <div className="max-w-[312px] w-full relative mb-4 ml-1.5 lg:ml-0 md:max-w-full">
                        <DefaultInputField
                            boxHeight="[52px]"
                            labelMarginB="mb-2"
                            placeholder="Enter convenience charges in rupees"
                            label="(Convenience charge)"
                            labelStyle="paragraphMediumItalic text-neutral-500"
                            shadow="shadow-smallDropDownShadow"
                        />
                    </div>
                </div>
            </div>
        </>
    );
}
