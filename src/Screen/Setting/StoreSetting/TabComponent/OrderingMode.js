import React, { useState } from "react";
import Header from "../../Components/Header";
import ToggleSwitch from "../../../../Components/ToggleSwitch/ToggleSwitch";
import { LargePrimaryButton } from "../../../../Components/Buttons/Button";
import { ReactComponent as LinearMenuIcon } from "../../../../Assets/linear-menu.svg";
import { ReactComponent as RearrangeIcon } from "../../../../Assets/re-arrange.svg";
import { ListViewOutletOrderingMode } from "../../../../Components/ListView/ListViewStoreSetting";

export default function OrderingMode() {
    const tableDetails = [
        {
            modeName: "Delivery",
            orderType: "QSR",
            orderTab: "Dine in",
        },
        {
            modeName: "Takeaway",
            orderType: "Fine dine",
            orderTab: "Dine in",
        },
        {
            modeName: "QSR",
            orderType: "QSR",
            orderTab: "Dine in",
        },
        {
            modeName: "Fine dine",
            orderType: "Fine dine",
            orderTab: "Dine in",
        },
    ];

    const [rearrangeTable, setRearrangeTable] = useState(false);

    const handleClickRearrange = () => {
        setRearrangeTable(!rearrangeTable);
    };
    return (
        <>
            <div className="w-full flex flex-row justify-between xl:block">
                <div className="mb-6 max-w-[636px]">
                    <Header title="Ordering mode" headerBottomLine="Choose to enable or disable the ordering mode a customer can select for your outlet." />
                </div>

                <div className="max-w-[159px] xl:mb-6 md:max-w-full cursor-pointer" onClick={handleClickRearrange}>
                    <LargePrimaryButton name="Re-arrange" leftIconDefault={<RearrangeIcon stroke="#FFFFFF" />} leftIconClick={<RearrangeIcon stroke="#C4BEED" />} />
                </div>
            </div>
            <div className="max-w-[636px] w-full md:max-w-full">
                <div className="flex flex-row items-center mb-6 lg:mb-4">
                    <span className="paragraphLargeMedium mr-4">Use In-store instead of Dine-in</span>
                    <ToggleSwitch enable={true} />
                </div>

                <div className="w-full rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden border-neutral-300 border lg:max-w-[462px]">
                    <table className="w-full break-words tableMediaLibrary">
                        <thead>
                            <tr className="shadow-innerShadow bg-neutral-50 text-left justify-center h-11">
                                <th className="px-6 min-w-[184px] paragraphOverlineSmall text-neutral-700">MODE NAME</th>
                                <th className="px-6 min-w-[159px] paragraphOverlineSmall text-neutral-700">ORDER TYPE</th>
                                <th className="px-6 min-w-[167px] paragraphOverlineSmall text-neutral-700">ORDER TAB</th>
                                <th className="px-6 min-w-[124px] paragraphOverlineSmall text-neutral-700">STATUS</th>
                            </tr>
                        </thead>
                        <tbody>
                            {tableDetails.map((el, index) => (
                                <tr className={`paragraphSmallRegular ${index !== 0 && "border-t"} border-neutral-300 justify-center h-[70px]`} key={index}>
                                    <td className="px-6">
                                        <div className="flex flex-row items-center">
                                            <div className={`${!rearrangeTable && "hidden"} mr-3`}>
                                                <LinearMenuIcon />
                                            </div>
                                            <span className="paragraphSmallRegular">{el.modeName}</span>
                                        </div>
                                    </td>
                                    <td className="px-6">{el.orderTab}</td>
                                    <td className="px-6">{el.orderType}</td>
                                    <td className="px-6">{<ToggleSwitch />}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
                <div className="pb-[70px] hidden md:block">
                    {tableDetails.map((el, index) => (
                        <div className="mt-2" key={index}>
                            <ListViewOutletOrderingMode modeName={el.modeName} orderType={el.orderType} orderTab={el.orderTab} />
                        </div>
                    ))}
                </div>
            </div>
        </>
    );
}
