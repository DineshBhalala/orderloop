import React from "react";
import { DefaultInputField } from "../../../../Components/InputField/InputField";
import Tag from "../../../../Components/Tag/Tag";
import Header from "../../Components/Header";

export default function PaymentSetting() {
    const paymentMenu = ["Stripe", "Paytm"];
    return (
        <>
            <div className="max-w-[636px] w-full md:max-w-full md:pb-20">
                <div className="pb-6 mb-6 border-b border-neutral-300">
                    <div className="pb-1">
                        <Header
                            title="Select payment gateways"
                            headerBottomLine="Select your payment gateway from the list. You can set up multiple payment providers if you have accounts available with them."
                            label="(Select payment providers)"
                            buttonTextColor="neutral-300"
                            dropdownLabel="Select multiple providers"
                            menuItems={paymentMenu}
                            labelMarginB="mb-2"
                            labelStyle="paragraphMediumItalic text-neutral-500"
                            shadow="shadow-smallDropDownShadow"
                            hasDropDown
                        />
                    </div>

                    <Tag tag="Paytm" />
                </div>

                <div className="mb-4">
                    <Header
                        title="Paytm gateway"
                        headerBottomLine="Enter your Paytm payment gateway API details."
                        labelMarginB="mb-2"
                        label="(Merchant key)"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        placeholder="Enter merchant key"
                        shadow="shadow-smallDropDownShadow"
                        hasInputField
                    />
                </div>

                <div className="max-w-[312px] w-full mb-4 md:max-w-full">
                    <DefaultInputField
                        boxHeight="[52px]"
                        labelMarginB="mb-2"
                        label="(Merchant ID)"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        placeholder="Enter merchant ID"
                        shadow="shadow-smallDropDownShadow"
                    />
                </div>

                <div className="max-w-[312px] w-full mb-4 md:max-w-full">
                    <DefaultInputField
                        boxHeight="[52px]"
                        labelMarginB="mb-2"
                        label="(Child merchant ID)"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        placeholder="Enter child merchant ID"
                        shadow="shadow-smallDropDownShadow"
                    />
                </div>

                <div className="max-w-[312px] w-full mb-4 md:max-w-full">
                    <DefaultInputField
                        boxHeight="[52px]"
                        labelMarginB="mb-2"
                        label="(Industry type ID)"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        placeholder="Enter industry type ID"
                        shadow="shadow-smallDropDownShadow"
                    />
                </div>

                <div className="max-w-[312px] w-full mb-4 md:max-w-full">
                    <DefaultInputField
                        boxHeight="[52px]"
                        labelMarginB="mb-2"
                        label="(Website)"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        placeholder="Enter website"
                        shadow="shadow-smallDropDownShadow"
                    />
                </div>

                <div className="max-w-[312px] w-full mb-6 md:max-w-full">
                    <DefaultInputField
                        boxHeight="[52px]"
                        labelMarginB="mb-2"
                        label="(Base API URL)"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        placeholder="Enter base API URL"
                        shadow="shadow-smallDropDownShadow"
                    />
                </div>
            </div>
        </>
    );
}
