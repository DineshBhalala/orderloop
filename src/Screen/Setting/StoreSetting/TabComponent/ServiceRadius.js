import React from "react";
import Header from "../../Components/Header";
import { DefaultInputField } from "../../../../Components/InputField/InputField";

export default function ServiceRadius() {
    const serviceabilityMenuItems = ["Yes", "No"];
    return (
        <>
            <div className="max-w-[636px] w-full md:max-w-full md:pb-20">
                <div className="mb-4">
                    <Header
                        title="Outlet location"
                        headerBottomLine="Provide your outlet's Latitude and Longitude. You can find this with the help of Google Maps. Please make sure to be accurate, as this is used in a variety of calculations."
                        labelMarginB="mb-2"
                        label="(Latitude)"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        placeholder="Enter latitude coordinate"
                        shadow="shadow-smallDropDownShadow"
                        hasInputField
                    />
                </div>

                <div className="mb-4 max-w-[312px] w-full md:max-w-full">
                    <DefaultInputField
                        boxHeight="[52px]"
                        labelMarginB="mb-2"
                        label="(Longitude)"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        placeholder="Enter longitude coordinate"
                        shadow="shadow-smallDropDownShadow"
                    />
                </div>
                <div className="mb-4">
                    <Header
                        title="Service radius"
                        headerBottomLine="The service radius is the limit for your delivery orders. Customers trying to order beyond the service radius will not be able to place an order for that location."
                        labelMarginB="mb-2"
                        label="(Service availability radius in kms)"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        placeholder="Enter radius in kms"
                        shadow="shadow-smallDropDownShadow"
                        hasInputField
                    />
                </div>

                <div className="mb-4">
                    <Header
                        title="Serviceability"
                        headerBottomLine="Select if you would like to accept the orders from the customers in your outlet service radius and 3rd party delivery areas combined."
                        menuItems={serviceabilityMenuItems}
                        shadow="shadow-smallDropDownShadow"
                        labelMarginB="mb-2"
                        hasDropDown
                    />
                </div>
            </div>
        </>
    );
}
