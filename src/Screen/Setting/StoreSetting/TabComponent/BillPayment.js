import React, { useState } from "react";
import Header from "../../Components/Header";
import ToggleSwitch from "../../../../Components/ToggleSwitch/ToggleSwitch";
import DropDown from "../../../../Components/DropDown/DropDown";
import { DefaultInputField } from "../../../../Components/InputField/InputField";
import CalenderField from "../../../../Components/Calender/CalenderField";

export default function BillPayment() {
    const [billPaymentEnable, setBillPaymentEnable] = useState(false);

    const handleChangeBillPayment = (e) => {
        setBillPaymentEnable(e);
    };

    const customizableType = ["Simple discount"];

    const discountType = ["Percentage discount"];

    const addDiscountUpto = ["No"];

    const addMinimumAmount = ["No"];

    const addMaximumUsageCount = ["No"];

    const addMaximumUsageCountPeruser = ["No"];

    const [showDiscountDetails, setShowDiscountDetails] = useState(false);

    const handleChangeDiscount = (e) => {
        setShowDiscountDetails(e);
    };

    return (
        <>
            <div className="max-w-[636px] w-full lg:max-w-[462px] md:max-w-full md:pb-20">
                <div className="mb-6 border-b pb-6 border-neutral-300">
                    <Header hasSwitch={true} title="Enable/disable bill payments" headerBottomLine="Enable or disable bill payment settings." showStatus={handleChangeBillPayment} />
                </div>
                <div className="mb-6 pb-6 border-b border-neutral-300">
                    <div className="flex flex-row items-center">
                        <h1 className={`paragraphLargeMedium ${!billPaymentEnable && "text-neutral-300"} mr-4`}>Enable/disable discount</h1>
                        <div className="pt-1">
                            <ToggleSwitch enable={billPaymentEnable} showStatus={handleChangeDiscount} />
                        </div>
                    </div>

                    <div className={`mt-4 ${!showDiscountDetails && "hidden"}`}>
                        <div className="mb-4 max-w-[312px] relative md:max-w-full">
                            <DropDown
                                menuItems={customizableType}
                                label="Customizable type"
                                shadow="shadow-smallDropDownShadow"
                                labelMarginB="mb-2"
                                height="h-[52px]"
                                labelStyle="paragraphLargeMedium"
                            />
                        </div>

                        <div className="mb-4 max-w-[312px] relative md:max-w-full">
                            <DropDown menuItems={discountType} label="Type of discount" shadow="shadow-smallDropDownShadow" labelMarginB="mb-2" height="h-[52px]" labelStyle="paragraphLargeMedium" />
                        </div>

                        <div className="mb-4 max-w-[312px] md:max-w-full">
                            <DefaultInputField placeholder="Enter percentage amount" label="Percentage amount" labelMarginB="mb-2" shadow="shadow-smallDropDownShadow" />
                        </div>

                        <div className="mb-4 max-w-[312px] relative md:max-w-full">
                            <DropDown
                                menuItems={addMinimumAmount}
                                label="Add minimum amount"
                                shadow="shadow-smallDropDownShadow"
                                labelMarginB="mb-2"
                                height="h-[52px]"
                                labelStyle="paragraphLargeMedium"
                            />
                        </div>

                        <div className="mb-4 max-w-[312px] relative md:max-w-full">
                            <DropDown
                                menuItems={addDiscountUpto}
                                label="Add discount upto amount"
                                shadow="shadow-smallDropDownShadow"
                                labelMarginB="mb-2"
                                height="h-[52px]"
                                labelStyle="paragraphLargeMedium"
                            />
                        </div>

                        <div className="mb-4 max-w-[312px] relative md:max-w-full">
                            <DropDown
                                menuItems={addMaximumUsageCount}
                                label="Add maximum usage count"
                                shadow="shadow-smallDropDownShadow"
                                labelMarginB="mb-2"
                                height="h-[52px]"
                                labelStyle="paragraphLargeMedium"
                            />
                        </div>

                        <div className="mb-4 max-w-[312px] relative md:max-w-full">
                            <DropDown
                                menuItems={addMaximumUsageCountPeruser}
                                label="Add maximum usage count per user"
                                shadow="shadow-smallDropDownShadow"
                                labelMarginB="mb-2"
                                height="h-[52px]"
                                labelStyle="paragraphLargeMedium"
                            />
                        </div>

                        <div className="mb-2">
                            <Header title="Coupon validity" headerBottomLine="Define the duration for the coupon validity." />
                        </div>

                        <div className="flex flex-row md:block">
                            <div className="mr-2 md:mr-0 md:mb-2">
                                <span className="paragraphLargeMedium">Start date</span>
                                <div className="mt-2">
                                    <CalenderField buttonStyle="shadow-smallDropDownShadow max-w-[217px] md:max-w-full md:w-full" label="28 Nov 2022" />
                                </div>
                            </div>

                            <div className="ml-2 md:ml-0">
                                <span className="paragraphLargeMedium">End date</span>
                                <div className="mt-2">
                                    <CalenderField buttonStyle="shadow-smallDropDownShadow max-w-[217px] md:max-w-full md:w-full" label="13 Dec 2022" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="flex flex-row items-center mb-6">
                    <h1 className={`paragraphLargeMedium ${!billPaymentEnable && "text-neutral-300"} mr-4`}>Enable/disable convenient charge</h1>
                    <div className="pt-1">
                        <ToggleSwitch enable={billPaymentEnable} />
                    </div>
                </div>
            </div>
        </>
    );
}
