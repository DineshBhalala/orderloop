import React from "react";
import Header from "../../Components/Header";
import { DefaultInputField, InputArea } from "../../../../Components/InputField/InputField";
import DropDown from "../../../../Components/DropDown/DropDown";

export default function Address() {
    return (
        <>
            <div className="max-w-[636px] w-full pb-4 lg:max-w-[462px] md:max-w-full md:pb-[120px]">
                <div className="mb-4">
                    <Header title="Outlet address" headerBottomLine="The customers see this address. The accurately detailed address will help customers find your outlet." />
                </div>

                <div className="flex flex-row items-center mb-4 lg:block">
                    <div className="mr-1.5 w-1/2 lg:w-full lg:mr-0 lg:max-w-[312px] lg:mb-2 md:max-w-full">
                        <InputArea placeholder="Enter address line 1 in english" paddingT="pt-4" label="(Address line 1)" type="storeSetting" shadow="shadow-smallDropDownShadow" />
                    </div>
                    <div className="ml-1.5 w-1/2 lg:w-full lg:ml-0 lg:max-w-[312px] lg:mb-2 md:max-w-full">
                        <InputArea placeholder="Enter address line 1 in english" paddingT="pt-4" label="(ગુજરાતી: Address line 1)" type="storeSetting" shadow="shadow-smallDropDownShadow" />
                    </div>
                </div>

                <div className="flex flex-row items-center mb-4 lg:block">
                    <div className="mr-1.5 w-1/2 lg:w-full lg:mr-0 lg:max-w-[312px] lg:mb-2 md:max-w-full">
                        <InputArea placeholder="Enter address line 1 in english" paddingT="pt-4" label="(Address line 2)" type="storeSetting" shadow="shadow-smallDropDownShadow" />
                    </div>
                    <div className="ml-1.5 w-1/2 lg:w-full lg:ml-0 lg:max-w-[312px] lg:mb-2 md:max-w-full">
                        <InputArea placeholder="Enter address line 1 in english" paddingT="pt-4" label="(ગુજરાતી: Address line 2)" type="storeSetting" shadow="shadow-smallDropDownShadow" />
                    </div>
                </div>

                <div className="flex flex-row items-center mb-4 lg:block">
                    <div className="mr-1.5 w-1/2 lg:w-full lg:mr-0 lg:max-w-[312px] lg:mb-2 md:max-w-full">
                        <DefaultInputField
                            boxHeight="[52px]"
                            labelMarginB="mb-2"
                            label="(City)"
                            labelStyle="paragraphMediumItalic text-neutral-500"
                            placeholder="Enter city in english"
                            shadow="shadow-smallDropDownShadow"
                        />
                    </div>
                    <div className="ml-1.5 w-1/2 lg:w-full lg:ml-0 lg:max-w-[312px] lg:mb-2 md:max-w-full">
                        <DefaultInputField
                            boxHeight="[52px]"
                            labelMarginB="mb-2"
                            label="(ગુજરાતી: City)"
                            labelStyle="paragraphMediumItalic text-neutral-500"
                            placeholder="Enter city in ગુજરાતી"
                            shadow="shadow-smallDropDownShadow"
                        />
                    </div>
                </div>

                <div className="flex flex-row items-center mb-4 lg:block">
                    <div className="mr-1.5 w-1/2 lg:w-full lg:mr-0 lg:max-w-[312px] lg:mb-2 md:max-w-full">
                        <DefaultInputField
                            boxHeight="[52px]"
                            labelMarginB="mb-2"
                            label="(State)"
                            labelStyle="paragraphMediumItalic text-neutral-500"
                            placeholder="Enter state in english"
                            shadow="shadow-smallDropDownShadow"
                        />
                    </div>
                    <div className="ml-1.5 w-1/2 lg:w-full lg:ml-0 lg:max-w-[312px] lg:mb-2 md:max-w-full">
                        <DefaultInputField
                            boxHeight="[52px]"
                            labelMarginB="mb-2"
                            label="(ગુજરાતી: State)"
                            labelStyle="paragraphMediumItalic text-neutral-500"
                            placeholder="Enter state in ગુજરાતી"
                            shadow="shadow-smallDropDownShadow"
                        />
                    </div>
                </div>

                <div className="w-1/2 mr-1.5 relative lg:w-full lg:max-w-[312px] md:max-w-full pb-4">
                    <DropDown label="(Country)" labelStyle="paragraphMediumItalic text-neutral-500" labelMarginB="mb-2" />
                </div>
            </div>
        </>
    );
}
