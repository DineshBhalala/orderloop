import React from "react";
import Header from "../../Components/Header";
import TimeSlotBanner from "../../../../Components/TimeSlot/TimeSlot";

export default function PreparationTime() {
    const rushHourMenu = ["No rush hours", "Specific time for all days", "Specific time for each days"];
    const days = ["Sundays", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    return (
        <>
            <div className="md:pb-20">
                <div className="mb-4">
                    <Header
                        labelMarginB="mb-2"
                        label="(Average time to prepare the dish)"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        placeholder="Enter time in minutes"
                        shadow="shadow-smallDropDownShadow"
                        title="Kitchen preparation time"
                        hasInputField
                        headerBottomLine="This time is what your kitchen usually takes to process and order. We ask this every time you accept the order unless you have enabled “Auto accept orders” in the settings."
                    />
                </div>

                <div className="mb-4">
                    <Header
                        menuItems={rushHourMenu}
                        shadow="shadow-smallDropDownShadow"
                        labelMarginB="mb-2"
                        hasDropDown
                        title="Kitchen rush hours"
                        headerBottomLine="Set kitchen Rush hours which will override the normal kitchen preparation time during the rush hour time."
                    />
                </div>

                <div className="max-w-[450px] md:max-w-full">
                    {days.map((el, index) => (
                        <div className="mb-4" key={index}>
                            <TimeSlotBanner dayLabel={el} />
                        </div>
                    ))}
                </div>
            </div>
        </>
    );
}
