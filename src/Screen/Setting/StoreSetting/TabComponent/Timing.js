import React from "react";
import Header from "../../Components/Header";
import DropDown from "../../../../Components/DropDown/DropDown";
import { TimerSlotStoreSetting } from "../../../../Components/TimeSlot/TimeSlot";

export default function Timing() {
    const timingMenu = ["Specific time for all days", "Full-time"];
    return (
        <>
            <div className="max-w-[636px] w-full md:max-w-full md:pb-20">
                <div className="pb-6 mb-6 border-b border-neutral-300">
                    <div className="mb-1">
                        <Header title="Outlet timings" headerBottomLine="This is the time when the customers will be able to order from any mode." />
                    </div>
                    <div className="mb-4">
                        <span className="paragraphMediumItalic text-primary-500">Each day can have only 6 time slots.</span>
                    </div>
                    <div className="max-w-[312px] w-full relative mb-4 md:max-w-full">
                        <DropDown menuItems={timingMenu} shadow="shadow-smallDropDownShadow" height="h-[52px]" />
                    </div>
                    <div className="max-w-[411px] md:max-w-full">
                        <TimerSlotStoreSetting />
                    </div>
                </div>
                <div className=" pb-6 mb-6 border-b border-neutral-300">
                    <div className="mb-1">
                        <Header title="Delivery timings" headerBottomLine="Please Specify the timings when this item will be available on orderloop." />
                    </div>
                    <div className="mb-4">
                        <span className="paragraphMediumItalic text-primary-500">Each day can have only 6 time slots.</span>
                    </div>
                    <div className="max-w-[312px] w-full relative md:max-w-full">
                        <DropDown menuItems={timingMenu} shadow="shadow-smallDropDownShadow" height="h-[52px]" />
                    </div>
                </div>
                <div className=" pb-6 mb-6 border-b border-neutral-300">
                    <div className="mb-1">
                        <Header title="Takeaway timings" headerBottomLine="Please Specify the timings when this item will be available on orderloop." />
                    </div>
                    <div className="mb-4">
                        <span className="paragraphMediumItalic text-primary-500">Each day can have only 6 time slots.</span>
                    </div>
                    <div className="max-w-[312px] w-full relative md:max-w-full">
                        <DropDown menuItems={timingMenu} shadow="shadow-smallDropDownShadow" height="h-[52px]" />
                    </div>
                </div>
                <div className="mb-6">
                    <div className="mb-1">
                        <Header title="Fine dine timings" headerBottomLine="Please Specify the timings when this item will be available on orderloop." />
                    </div>
                    <div className="mb-4">
                        <span className="paragraphMediumItalic text-primary-500">Each day can have only 6 time slots.</span>
                    </div>
                    <div className="max-w-[312px] w-full relative md:max-w-full">
                        <DropDown menuItems={timingMenu} shadow="shadow-smallDropDownShadow" height="h-[52px]" />
                    </div>
                </div>
            </div>
        </>
    );
}
