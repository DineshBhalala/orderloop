import React from "react";
import { DefaultInputField } from "../../../../Components/InputField/InputField";
import Header from "../../Components/Header";

export default function Taxes() {
    const menuItem = ["Yes", "No"];
    return (
        <>
            <div className="md:pb-20">
                <div className="mb-6">
                    <Header
                        hasDropDown
                        title="Taxes"
                        headerBottomLine="Select if taxes apply to your outlet and set your license number for your customers to see on the invoice."
                        label="(Charge GST)"
                        labelMarginB="mb-2"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        shadow="shadow-smallDropDownShadow"
                        menuItems={menuItem}
                    />
                </div>

                <div className="max-w-[312px] w-full mb-4 md:max-w-full">
                    <DefaultInputField
                        boxHeight="[52px]"
                        labelMarginB="mb-2"
                        label="(GST number)"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        placeholder="Enter GST number"
                        shadow="shadow-smallDropDownShadow"
                    />
                </div>
            </div>
        </>
    );
}
