import React from "react";
import Header from "../../Components/Header";

export default function CodLimit() {
    return (
        <>
            <div className="md:pb-20">
                <div className="mb-4">
                    <Header
                        title="Cash limit on delivery orders"
                        headerBottomLine="Set limit for cash payment on delivery orders."
                        placeholder="Enter amount in rupees"
                        shadow="shadow-smallDropDownShadow"
                        hasInputField
                    />
                </div>

                <div className="mb-4">
                    <Header
                        title="Cash limit on takeaway orders"
                        headerBottomLine="Set limit for cash payment on takeaway orders."
                        placeholder="Enter amount in rupees"
                        shadow="shadow-smallDropDownShadow"
                        hasInputField
                    />
                </div>

                <div className="mb-4">
                    <Header
                        title="Cash limit on dine in orders"
                        headerBottomLine="Set limit for cash payment on dine in orders."
                        placeholder="Enter amount in rupees"
                        shadow="shadow-smallDropDownShadow"
                        hasInputField
                    />
                </div>
            </div>
        </>
    );
}
