import React from "react";
import Header from "../../Components/Header";
import DropDown from "../../../../Components/DropDown/DropDown";
import ToggleSwitch from "../../../../Components/ToggleSwitch/ToggleSwitch";
import { ListViewPaymentMethod } from "../../../../Components/ListView/ListViewStoreSetting";

export default function PaymentMethod() {
    const tableDetails = ["Delivery", "Takeaway", "QSR", "Fine dine"];
    return (
        <>
            <div className="pb-20">
                <div className="mb-4">
                    <Header
                        label="(Credit/Debit card)"
                        buttonTextColor="neutral-300"
                        labelMarginB="mb-2"
                        dropdownLabel="Select multiple providers"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        shadow="shadow-smallDropDownShadow"
                        title="Payment methods"
                        headerBottomLine="Select a payment gateway from the list for the respective payment method."
                        hasDropDown
                    />
                </div>

                <div className="max-w-[312px] w-full relative mb-4 md:max-w-full">
                    <DropDown
                        height="h-[52px]"
                        label="(Net banking)"
                        buttonTextColor="neutral-300"
                        dropdownLabel="Select multiple providers"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        labelMarginB="mb-2"
                        shadow="shadow-smallDropDownShadow"
                    />
                </div>

                <div className="max-w-[312px] w-full relative mb-4 md:max-w-full">
                    <DropDown
                        height="h-[52px]"
                        label="(Balance)"
                        buttonTextColor="neutral-300"
                        dropdownLabel="Select multiple providers"
                        labelMarginB="mb-2"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        shadow="shadow-smallDropDownShadow"
                    />
                </div>

                <div className="max-w-[312px] w-full relative mb-4 md:max-w-full">
                    <DropDown
                        height="h-[52px]"
                        label="(UPI)"
                        buttonTextColor="neutral-300"
                        dropdownLabel="Select multiple providers"
                        labelMarginB="mb-2"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        shadow="shadow-smallDropDownShadow"
                    />
                </div>

                <div className="max-w-[312px] w-full relative mb-6 md:max-w-full">
                    <DropDown
                        height="h-[52px]"
                        label="(UPI Intent)"
                        buttonTextColor="neutral-300"
                        dropdownLabel="Select multiple providers"
                        labelMarginB="mb-2"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        shadow="shadow-smallDropDownShadow"
                    />
                </div>

                <div className="mb-4">
                    <Header title="Enable cash" headerBottomLine="Select ordering modes for in which cash payment will be accepted." />
                </div>

                <div className="w-full rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden border-neutral-300 border max-w-[310px] mb-6">
                    <table className="w-full break-words tableMediaLibrary">
                        <thead>
                            <tr className="shadow-innerShadow bg-neutral-50 text-left justify-center h-11">
                                <th className="px-6 min-w-[184px] paragraphOverlineSmall text-neutral-700">MODE NAME</th>
                                <th className="px-6 min-w-[126px] paragraphOverlineSmall text-neutral-700">STATUS</th>
                            </tr>
                        </thead>
                        <tbody>
                            {tableDetails.map((el, index) => (
                                <tr className={`paragraphSmallRegular ${index !== 0 && "border-t"} border-neutral-300 justify-center h-[70px]`} key={index}>
                                    <td className="px-6">{el}</td>
                                    <td className="px-6">
                                        <ToggleSwitch />
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
                <div className="hidden md:block">
                    {tableDetails.map((el, index) => (
                        <div className="mt-2" key={index}>
                            <ListViewPaymentMethod modeName={el} />
                        </div>
                    ))}
                </div>
            </div>
        </>
    );
}
