import React, { useState } from "react";
import Header from "../../Components/Header";
import { LargePrimaryButton, MediumPrimaryButton } from "../../../../Components/Buttons/Button";
import { DefaultInputField } from "../../../../Components/InputField/InputField";
import AddThresholdPopup from "../Components/Popup";
import { ReactComponent as AddIcon } from "../../../../Assets/add.svg";
import { ReactComponent as EditIcon } from "../../../../Assets/edit.svg";
import { ReactComponent as SelectIcon } from "../../../../Assets/select.svg";

export default function DeliveryCharges() {
    const tableDetails = [
        {
            deliveryThreshold: "₹199",
            belowThreshold: "₹40",
            aboveThreshold: "₹20",
        },
        {
            deliveryThreshold: "₹99",
            belowThreshold: "₹40",
            aboveThreshold: "₹20",
        },
        {
            deliveryThreshold: "₹199",
            belowThreshold: "₹40",
            aboveThreshold: "₹20",
        },
        {
            deliveryThreshold: "₹199",
            belowThreshold: "₹40",
            aboveThreshold: "₹20",
        },
    ];
    const gstSlabMenu = ["18%"];

    const [showAddThresholdPopup, setShowAddThresholdPopup] = useState(false);

    const handleClickAddThreshold = () => {
        setShowAddThresholdPopup(!showAddThresholdPopup);
    };

    return (
        <>
            <div className="lg:max-w-[464px] md:max-w-full md:pb-20">
                <div className="mb-4">
                    <Header title="Delivery charges" headerBottomLine="Set delivery charges for your customers to pay on checkout for delivery orders only." />
                </div>

                <div className="flex flex-row w-full mb-4">
                    <div className="max-w-[157px] mr-2 lg:mr-1 md:min-w-[64px] md:w-full cursor-pointer" onClick={handleClickAddThreshold}>
                        <MediumPrimaryButton
                            fontsWeight="font-medium"
                            name="Add threshold"
                            hideName="md:hidden"
                            isDefault={false}
                            leftIconDefault={<AddIcon stroke="#FFFFFF" />}
                            leftIconClick={<AddIcon stroke="#C4BEED" />}
                        />
                    </div>

                    <div className="max-w-[156px] mx-2 lg:mx-1 md:min-w-[64px] md:w-full">
                        <MediumPrimaryButton
                            fontsWeight="font-medium"
                            name="Edit threshold"
                            hideName="md:hidden"
                            leftIconDefault={<EditIcon stroke="#FFFFFF" />}
                            leftIconClick={<EditIcon stroke="#C4BEED" />}
                        />
                    </div>

                    <div className="max-w-[136px] ml-2 lg:ml-1 md:min-w-[64px] md:w-full">
                        <MediumPrimaryButton
                            fontsWeight="font-medium"
                            name="Bulk select"
                            hideName="md:hidden"
                            leftIconDefault={<SelectIcon stroke="#FFFFFF" />}
                            leftIconClick={<SelectIcon stroke="#C4BEED" />}
                        />
                    </div>
                </div>

                <div className="w-full rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden border-neutral-300 border max-w-[566px] mb-6 lg:max-w-[464px]">
                    <table className="w-full break-words tableMediaLibrary">
                        <thead>
                            <tr className="shadow-innerShadow bg-neutral-50 text-left justify-center h-11">
                                <th className="px-6 min-w-[201px] paragraphOverlineSmall text-neutral-700">DELIVERY THRESHOLD</th>
                                <th className="px-6 min-w-[181px] paragraphOverlineSmall text-neutral-700">ABOVE THRESHOLD</th>
                                <th className="px-6 min-w-[183px] paragraphOverlineSmall text-neutral-700">BELOW THRESHOLD</th>
                            </tr>
                        </thead>

                        <tbody>
                            {tableDetails.map((el, index) => (
                                <tr className={`paragraphSmallRegular ${index !== 0 && "border-t"} border-neutral-300 justify-center h-[70px]`} key={index}>
                                    <td className="px-6">{el.deliveryThreshold}</td>
                                    <td className="px-6">{el.aboveThreshold}</td>
                                    <td className="px-6">{el.belowThreshold}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>

                <div className="mb-4">
                    <Header
                        hasSwitch={true}
                        title="Distance fees"
                        headerBottomLine="The distance fees can be set above a normal radius. Charges set above the normal radius will apply only if the delivery is beyond the radius. A distance fee per kilometre can be charged to the customers."
                        labelMarginB="mb-2"
                        label="(Normal delivery radius)"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        placeholder="Enter distance in kms"
                        shadow="shadow-smallDropDownShadow"
                        hasInputField
                    />
                </div>

                <div className="max-w-[312px] w-full mb-6 md:max-w-full">
                    <DefaultInputField
                        boxHeight="[52px]"
                        labelMarginB="mb-2"
                        label="(Extra charge)"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        placeholder="Enter extra charge in rupees"
                        shadow="shadow-smallDropDownShadow"
                    />
                </div>

                <div className="mb-4">
                    <Header
                        label="(GST Slabs)"
                        menuItems={gstSlabMenu}
                        labelMarginB="mb-2"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        shadow="shadow-smallDropDownShadow"
                        hasDropDown
                        title="Taxes on delivery charged"
                        headerBottomLine="You can choose to separately display and collect taxes on delivery charges for the customers."
                        hasSwitch={true}
                    />
                </div>
            </div>

            <div className={`${!showAddThresholdPopup && "hidden"}`}>
                <AddThresholdPopup handleClickClose={handleClickAddThreshold} />
            </div>
        </>
    );
}
