import React, { useState } from "react";
import { DefaultInputField } from "../../../../Components/InputField/InputField";
import Header from "../../Components/Header";
import PhoneInput from "react-phone-number-input";

export default function Contact() {
  const [value, setValue] = useState();
  return (
    <>
      <div className="max-w-[636px] w-full md:max-w-full md:pb-20">
        <div className="mb-1">
          <Header title="Outlet name" headerBottomLine="Name your outlet for customers to identify it quickly." />
        </div>

        <div className="paragraphMediumItalic text-neutral-500 mb-4">
          <span className="text-neutral-900">NOTE: </span>
          It is always better to name your outlet based on the area/locality.
        </div>

        <div className="flex flex-row items-center mb-4 lg:block">
          <div className="mr-1.5 w-1/2 lg:w-full lg:mb-2 lg:max-w-[312px] md:max-w-full md:mr-0">
            <DefaultInputField
              labelMarginB="mb-2"
              label="(English)"
              labelStyle="paragraphMediumItalic text-neutral-500"
              placeholder="Enter outlet name in english"
              shadow="shadow-smallDropDownShadow"
            />
          </div>
          <div className="ml-1.5 w-1/2 lg:w-full lg:max-w-[312px] md:max-w-full md:ml-0">
            <DefaultInputField
              labelMarginB="mb-2"
              label="(ગુજરાતી)"
              labelStyle="paragraphMediumItalic text-neutral-500"
              placeholder="Enter outlet name in ગુજરાતી"
              shadow="shadow-smallDropDownShadow"
            />
          </div>
        </div>

        <div className="mb-4">
          <Header title="Contact number" headerBottomLine="The contact number of your outlet is for your customers to contact the outlet for any help directly." />
        </div>

        <div className="relative removeSpinButton max-w-[312px] md:max-w-full">
          <DefaultInputField placeholder="Enter mobile number" paddingLeft="pl-[112px]" inputType="number" shadow="shadow-smallDropDownShadow" />
          <div className="phoneinputbox absolute bottom-3 left-4">
            <PhoneInput international countryCallingCodeEditable={false} defaultCountry="IN" value={value} onChange={setValue} />
          </div>
        </div>
      </div>
    </>
  );
}
