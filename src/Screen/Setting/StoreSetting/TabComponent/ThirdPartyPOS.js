import React from "react";
import { DefaultInputField } from "../../../../Components/InputField/InputField";
import Header from "../../Components/Header";

export default function ThirdPartyPOS() {
    const menuItem = ["Yes", "No"];
    return (
        <>
            <div className="md:pb-20">
                <div className="mb-4">
                    <Header
                        title="Enable Pet Pooja integration"
                        hasDropDown
                        headerBottomLine="Enable to start integrating pej pooja menu."
                        label="(Enable third party POS)"
                        labelMarginB="mb-2"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        shadow="shadow-smallDropDownShadow"
                        menuItems={menuItem}
                    />
                </div>
                <div className="max-w-[312px] w-full md:max-w-full">
                    <DefaultInputField
                        boxHeight="[52px]"
                        placeholderTextColor="text-neutral-900"
                        labelMarginB="mb-2"
                        label="(App key)"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        placeholder="Enter app key"
                        shadow="shadow-smallDropDownShadow"
                    />
                </div>
                <div className="max-w-[312px] w-full mt-4 md:max-w-full">
                    <DefaultInputField
                        boxHeight="[52px]"
                        placeholderTextColor="text-neutral-900"
                        labelMarginB="mb-2"
                        label="(App secret)"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        placeholder="Enter app secret"
                        shadow="shadow-smallDropDownShadow"
                    />
                </div>
                <div className="max-w-[312px] w-full mt-4 md:max-w-full">
                    <DefaultInputField
                        boxHeight="[52px]"
                        placeholderTextColor="text-neutral-900"
                        labelMarginB="mb-2"
                        label="(Access token)"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        placeholder="Enter access token"
                        shadow="shadow-smallDropDownShadow"
                    />
                </div>
                <div className="max-w-[312px] w-full mt-4 md:max-w-full">
                    <DefaultInputField
                        boxHeight="[52px]"
                        placeholderTextColor="text-neutral-900"
                        labelMarginB="mb-2"
                        label="(Restaurant ID)"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        placeholder="Yes"
                        shadow="shadow-smallDropDownShadow"
                    />
                </div>
                <div className="mb-4 mt-6">
                    <Header
                        title="Enable Pet Pooja orders"
                        headerBottomLine="Enable to start getting orders to pet pooja."
                        label="(Enable third party POS orders)"
                        labelMarginB="mb-2"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        shadow="shadow-smallDropDownShadow"
                        menuItems={menuItem}
                        hasDropDown
                    />
                </div>
            </div>
        </>
    );
}
