import React, { useState } from "react";
import Header from "../../Components/Header";
import { CategoryLayoutTable } from "../Components/TableComponents";
import { LargeDestructiveButton, LargePrimaryButton } from "../../../../Components/Buttons/Button";
import { ReactComponent as LinearMenuIcon } from "../../../../Assets/linear-menu.svg";
import { ReactComponent as EditIcon } from "../../../../Assets/edit.svg";
import { ReactComponent as TrashIcon } from "../../../../Assets/trash.svg";
import { ReactComponent as AddIcon } from "../../../../Assets/add.svg";
import mobileImage from "../../../../Assets/mobile.png";
import { Tab } from "../../../../Components/Tabs/Tabs";
import DraggableList from "react-draggable-lists";
import { ListViewAppearance } from "../../../../Components/ListView/ListViewStoreSetting";

export default function Appearance() {
    const [activeTab, setActiveTab] = useState("category");

    const handleClickHeaderTab = (tab) => {
        setActiveTab(tab);
    };

    const tableDetails = [{ categoryDisplayCount: 1 }, { categoryDisplayCount: 2 }, { categoryDisplayCount: 3 }, { categoryDisplayCount: 3 }];

    const menuItems = ["01", "02", "03"];

    const tabData = [
        {
            label: "Category layout",
            key: "category",
            headerTitle: "Category layout",
            headerBottomLine: "Define row-wise category layout for your mobile application.",
        },
        {
            label: "Home screen layout",
            key: "home",
            headerTitle: "Home screen layout",
            headerBottomLine: "Define the hierarchy of the sections that your customers will view in the mobile application.",
        },
    ];

    const layoutSection = ["Banner_Section", "Secondary_banner_section", "Category_list_section", "Tertiary_banner_section"];

    const [editLayout, setEditLayout] = useState(false);

    const handleClickEditLayout = () => {
        setEditLayout(!editLayout);
    };

    return (
        <>
            <div className="flex flex-row justify-between xl:block xl:mb-0 h-full">
                <div className="min-w-[661px] pr-6 -mb-20 border-r border-neutral-300 w-full xl:pr-0 xl:pb-2 xl:border-r-0 xl:border-b xl:mb-6 xl:min-w-full md:pb-0">
                    <div className="flex flex-row items-center mb-6 mobile:block">
                        {tabData.map((tab, index) => (
                            <div
                                className={`${index === 0 ? "mr-2 mobile:mr-0 mobile:mb-2 md:w-1/2 mobile:w-full" : "mobile:w-full md:w-1/2 ml-2 mobile:ml-0 min-w-[164px]"} cursor-pointer`}
                                key={tab.key}
                                onClick={() => handleClickHeaderTab(tab.key)}
                            >
                                <Tab label={tab.label} isActive={activeTab === tab.key} />
                            </div>
                        ))}
                    </div>

                    <div className="mb-4">
                        <Header title={tabData.find((tab) => tab.key === activeTab)?.headerTitle} headerBottomLine={tabData.find((tab) => tab.key === activeTab)?.headerBottomLine} />
                    </div>

                    <div className={`${activeTab === "home" && "hidden"}`}>
                        <div className={`w-full rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden border-neutral-300 border mb-6`}>
                            <table className="w-full break-words tableMediaLibrary">
                                <thead>
                                    <tr className="shadow-innerShadow bg-neutral-50 text-left justify-center h-11">
                                        <th className="px-6 min-w-[142px] paragraphOverlineSmall text-neutral-700">ROW NUMBER</th>
                                        <th className="px-6 min-w-[237px] paragraphOverlineSmall text-neutral-700">CATEGORY DISPLAY COUNT</th>
                                        <th className="px-6 paragraphOverlineSmall text-neutral-700 min-w-[259px]">ACTIONS</th>
                                    </tr>
                                </thead>
                                
                                <tbody>
                                    {tableDetails.map((el, index) => (
                                        <CategoryLayoutTable rowNumber={index + 1} categoryDisplayCount={el.categoryDisplayCount} key={index} />
                                    ))}
                                </tbody>
                            </table>
                        </div>

                        <div className="hidden md:block w-full mb-4">
                            <div className="flex flex-row pb-2">
                                <div className="mr-2 w-full mobile:mr-1">
                                    <LargePrimaryButton name="Add row" leftIconDefault={<AddIcon stroke="#FFFFFF" />} leftIconClick={<AddIcon stroke="#C4BEED" />} />
                                </div>

                                <div className="ml-2 w-full mobile:ml-1">
                                    <LargeDestructiveButton name="Delete row" leftIconDefault={<TrashIcon stroke="#FFFFFF" />} leftIconClick={<TrashIcon stroke="#D7EDEB" />} />
                                </div>
                            </div>

                            {tableDetails.map((el, index) => (
                                <div className="mt-2" key={index}>
                                    <ListViewAppearance categoryDisplayCount={el.categoryDisplayCount} />
                                </div>
                            ))}
                        </div>

                        <div className="mb-4">
                            <Header
                                hasDropDown
                                menuItems={menuItems}
                                title="Default row categories"
                                headerBottomLine="This value will set the number of categories in all the rows below the last row in the table above."
                            />
                        </div>
                    </div>

                    <div className={`${activeTab === "category" && "hidden"}`}>
                        <div className="max-w-[153px] mb-4 md:max-w-full md:pb-2 md:mb-0 cursor-pointer" onClick={handleClickEditLayout}>
                            <LargePrimaryButton name="Edit layout" leftIconDefault={<EditIcon stroke="#FFFFFF" />} leftIconClick={<EditIcon stroke="#C4BEED" />} />
                        </div>

                        <div
                            className={`w-full rounded-lg paragraphSmallRegular overflow-auto [&::-webkit-scrollbar]:hidden md:hidden border-neutral-300 border mb-6 ${activeTab === "category" && "hidden"
                                }`}
                        >
                            <table className="w-full break-words tableMediaLibrary">
                                <tbody>
                                    {layoutSection.map((el, index) => (
                                        <tr className={`h-[70px] justify-center paragraphSmallRegular ${index !== 0 && "border-t"} border-neutral-300`} key={index}>
                                            <td className="px-6">
                                                <div className="flex flex-auto items-center">
                                                    <div className={`${!editLayout && "hidden"} mr-3`}>
                                                        <LinearMenuIcon />
                                                    </div>
                                                    <span className="">{el}</span>
                                                </div>
                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>

                        <div className="hidden md:block">
                            {layoutSection.map((el, index) => (
                                <div className="mt-2" key={index}>
                                    <ListViewAppearance sectionName={el} />
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
                <div className="w-full flex justify-center">
                    <div className="w-full max-w-[329px] md:max-w-[280px] ml-8 xl:ml-0">
                        <img src={mobileImage} alt="" />
                    </div>
                </div>
            </div>
        </>
    );
}
