import React from "react";
import Header from "../../Components/Header";
import { InputArea } from "../../../../Components/InputField/InputField";

export default function BillSetting() {
  return (
    <>
      <div className="max-w-[636px] w-full md:max-w-full md:pb-20">
        <div className="mb-4">
          <Header
            title="Legal name"
            placeholder="Enter legal name"
            shadow="shadow-smallDropDownShadow"
            hasInputField
            marginBetween="mt-2"
            headerBottomLine="Enter the legal name of your outlet that will be printed on the bill."
          />
        </div>
        {/* <div className="max-w-[311px] mb-4">
          <DefaultInputField  />
        </div> */}
        <div className="mb-2">
          <Header title="Bill header" headerBottomLine="Enter the bill header for your outlet that will be printed on the bill." />
        </div>
        <div className="mb-4">
          <InputArea placeholder="Enter bill header" shadow="shadow-smallDropDownShadow" paddingT="pt-4" />
        </div>
        <div className="mb-2">
          <Header title="Bill footer" headerBottomLine="Enter the bill footer for your outlet that will be printed on the bill." />
        </div>
        <div className="mb-4">
          <InputArea placeholder="Enter bill footer" shadow="shadow-smallDropDownShadow" paddingT="pt-4" />
        </div>
      </div>
    </>
  );
}
