import React from "react";
import Header from "../../Components/Header";

export default function OtherSetting() {
    const menuItem = ["Yes", "No"];
    return (
        <>
            <div className="mb-4">
                <Header
                    title="Auto acceptance"
                    hasDropDown
                    headerBottomLine="SSelect if you would like to auto accept the incoming orders."
                    label="(Auto accept)"
                    labelMarginB="mb-2"
                    labelStyle="paragraphMediumItalic text-neutral-500"
                    shadow="shadow-smallDropDownShadow"
                    menuItems={menuItem}
                />
            </div>
        </>
    );
}
