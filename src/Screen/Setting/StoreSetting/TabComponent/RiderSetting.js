import React, { useState } from "react";
import Header from "../../Components/Header";
import DropDown from "../../../../Components/DropDown/DropDown";
import { DefaultInputField } from "../../../../Components/InputField/InputField";
import { LargePrimaryButton } from "../../../../Components/Buttons/Button";
import { ReactComponent as RearrangeIcon } from "../../../../Assets/re-arrange.svg";
import { ReactComponent as EditIcon } from "../../../../Assets/edit.svg";
import { ReactComponent as TimerIcon } from "../../../../Assets/timer.svg";
import { Tab } from "../../../../Components/Tabs/Tabs";

export default function RiderSetting() {
    const [activeTab, setActiveTab] = useState(0);

    const handleClickHeaderTab = (tab) => {
        setActiveTab(tab);
    };

    const tabData = [{ label: "Rider settings" }, { label: "Auto-assign riders" }];

    const menuItems = ["Yes", "No"];

    const menuItemsSelectPreference = ["Priority based", "Preference based"];

    const tableDetails = [
        {
            riderType: "In-house riders",
            waitingTime: 3,
        },
        {
            riderType: "ELT riders",
            waitingTime: 3,
        },
        {
            riderType: "Dunzo riders",
            waitingTime: 3,
        },
        {
            riderType: "Wefast riders",
            waitingTime: 3,
        },
    ];

    return (
        <>
            <div className="max-w-[636px] w-full md:max-w-full md:pb-20">
                <div className="flex flex-row items-center mb-6 lg:mb-4">
                    {tabData.map((tab, index) => (
                        <div
                            className={`${index === 0 ? "mr-2 mobile:mr-1 md:w-1/2 mobile:w-full" : "mobile:w-full md:w-1/2 ml-2 mobile:ml-0 min-w-[160px]"} cursor-pointer`}
                            key={index}
                            onClick={() => handleClickHeaderTab(index)}
                        >
                            <Tab label={tab.label} isActive={activeTab === index} />
                        </div>
                    ))}
                </div>
                <div className={`${activeTab === 1 && "hidden"}`}>
                    <div className="pb-6 border-b border-neutral-300 mb-6">
                        <Header
                            label="(Currency)"
                            labelMarginB="mb-2"
                            buttonTextColor="neutral-300"
                            dropdownLabel="Select currency"
                            labelStyle="paragraphMediumItalic text-neutral-500"
                            shadow="shadow-smallDropDownShadow"
                            menuItems={menuItems}
                            hasDropDown
                            title="In-house riders"
                            headerBottomLine="Set this on if you own riders at your outlet to fulfill the delivery orders."
                        />
                    </div>
                    <div className="pb-6 border-b border-neutral-300 mb-6">
                        <div className="mb-4">
                            <Header
                                title="ELT riders"
                                headerBottomLine="Set ELT riders API for your outlet."
                                label="(Enable service)"
                                buttonTextColor="neutral-300"
                                dropdownLabel="Select currency"
                                labelMarginB="mb-2"
                                labelStyle="paragraphMediumItalic text-neutral-500"
                                shadow="shadow-smallDropDownShadow"
                                menuItems={menuItems}
                                hasDropDown
                            />
                        </div>

                        <div className="max-w-[312px] w-full mb-4 md:max-w-full">
                            <DefaultInputField
                                boxHeight="[52px]"
                                labelMarginB="mb-2"
                                label="(API key)"
                                labelStyle="paragraphMediumItalic text-neutral-500"
                                placeholder="Enter API key"
                                shadow="shadow-smallDropDownShadow"
                            />
                        </div>
                        <div className="max-w-[312px] w-full mb-4 md:max-w-full">
                            <DefaultInputField
                                boxHeight="[52px]"
                                labelMarginB="mb-2"
                                label="(Primary address ID)"
                                labelStyle="paragraphMediumItalic text-neutral-500"
                                placeholder="Enter primary address ID"
                                shadow="shadow-smallDropDownShadow"
                            />
                        </div>
                        <div className="max-w-[312px] w-full md:max-w-full">
                            <DefaultInputField
                                boxHeight="[52px]"
                                labelMarginB="mb-2"
                                label="URL"
                                labelStyle="paragraphMediumItalic text-neutral-500"
                                placeholder="Enter URL"
                                shadow="shadow-smallDropDownShadow"
                            />
                        </div>
                    </div>
                    <div className="pb-6 border-b border-neutral-300 mb-6">
                        <div className="mb-4">
                            <Header
                                label="(Enable service)"
                                buttonTextColor="neutral-300"
                                dropdownLabel="Select currency"
                                labelMarginB="mb-2"
                                labelStyle="paragraphMediumItalic text-neutral-500"
                                shadow="shadow-smallDropDownShadow"
                                menuItems={menuItems}
                                hasDropDown
                                title="Dunzo riders"
                                headerBottomLine="Set Dunzo riders API for your outlet."
                            />
                        </div>
                    </div>
                    <div className="pb-6 border-b border-neutral-300 mb-6">
                        <div className="mb-4">
                            <Header
                                title="Wefast riders"
                                label="(Enable service)"
                                buttonTextColor="neutral-300"
                                dropdownLabel="Select currency"
                                labelMarginB="mb-2"
                                labelStyle="paragraphMediumItalic text-neutral-500"
                                shadow="shadow-smallDropDownShadow"
                                menuItems={menuItems}
                                hasDropDown
                                headerBottomLine="Set Wefast riders API for your outlet."
                            />
                        </div>
                    </div>
                    <div className="mb-6">
                        <div className="mb-4">
                            <Header
                                title="Shadowfax riders"
                                label="(Enable service)"
                                labelMarginB="mb-2"
                                buttonTextColor="neutral-300"
                                dropdownLabel="Select currency"
                                labelStyle="paragraphMediumItalic text-neutral-500"
                                shadow="shadow-smallDropDownShadow"
                                menuItems={menuItems}
                                hasDropDown
                                headerBottomLine="Set Shadowfax riders API for your outlet."
                            />
                        </div>
                    </div>
                </div>
                <div className={`${activeTab === 0 && "hidden"}`}>
                    <div className="mb-6">
                        <Header
                            hasSwitch={true}
                            title="Auto-assign riders"
                            headerBottomLine="When turned ON, the system will automatically assign a rider for an order based on the number of factors such as available delivery channels, assignment selection, intervention time etc."
                        />
                    </div>
                    <div className="mb-4">
                        <Header
                            title="Auto-assign riders preference"
                            headerBottomLine="Set either priority or preference for rider assignment. Priority will assign an order to all agencies, the one that accepts the order first will be assigned the order. In contrast, preference selection will give orders in sequence to the preference list."
                        />
                    </div>
                    <div className="max-w-[312px] w-full relative md:max-w-full">
                        <DropDown label="(Select preference)" labelStyle="paragraphMediumItalic text-neutral-500 mb-2" shadow="shadow-smallDropDownShadow" menuItems={menuItemsSelectPreference} />
                    </div>
                    <div className="flex flex-row my-4">
                        <div className="mr-2 md:w-1/2 mobile:mr-1">
                            <LargePrimaryButton name="Re-arrange" hideName="mobile:hidden" leftIconClick={<RearrangeIcon stroke="#C4BEED" />} leftIconDefault={<RearrangeIcon stroke="#FFFFFF" />} />
                        </div>
                        <div className="ml-2 md:w-1/2 mobile:ml-1">
                            <LargePrimaryButton name="Edit waiting time" hideName="mobile:hidden" leftIconClick={<EditIcon stroke="#C4BEED" />} leftIconDefault={<EditIcon stroke="#FFFFFF" />} />
                        </div>
                    </div>

                    <div className={`w-full rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden border-neutral-300 border mb-6 max-w-[380px] md:max-w-full`}>
                        <table className="w-full break-words tableMediaLibrary">
                            <thead>
                                <tr className="shadow-innerShadow bg-neutral-50 text-left justify-center h-11">
                                    <th className="px-6 min-w-[200px] paragraphOverlineSmall text-neutral-700">RIDER TYPE</th>
                                    <th className="px-6 min-w-[180px] paragraphOverlineSmall text-neutral-700">WAITING TIME</th>
                                </tr>
                            </thead>
                            <tbody>
                                {tableDetails.map((el, index) => (
                                    <tr key={index} className={`h-[70px] justify-center paragraphSmallRegular ${index !== 0 && "border-t"} border-neutral-300`}>
                                        <td className="px-6">{el.riderType}</td>
                                        <td className="px-6">
                                            <div className="flex flex-row items-center">
                                                <TimerIcon />
                                                <span className="ml-1">{el.waitingTime}</span>
                                            </div>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>

                    <div className="mb-4 mt-6">
                        <Header
                            title="Manual intervention"
                            headerBottomLine="This will enable the system to wait for someone to decide before automatically assigning the order to a rider / 3rd party rider agency."
                        />
                    </div>
                    <div className="max-w-[312px] w-full relative mb-4 md:max-w-full">
                        <DropDown label="(Enable intervention)" labelStyle="paragraphMediumItalic text-neutral-500 mb-2" shadow="shadow-smallDropDownShadow" menuItems={menuItems} />
                    </div>
                    <div className="flex flex-row">
                        <div className="max-w-[164px] w-full mb-4 mr-2 md:max-w-full">
                            <DefaultInputField boxHeight="[52px]" label="Minutes" placeholder="Enter minutes" shadow="shadow-smallDropDownShadow" />
                        </div>
                        <div className="max-w-[164px] w-full mb-4 ml-2 md:max-w-full">
                            <DefaultInputField boxHeight="[52px]" label="Seconds" placeholder="Enter seconds" shadow="shadow-smallDropDownShadow" />
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
