import React from "react";
import Header from "../../Components/Header";

export default function Currency() {
    const currencyMenu = ["India(INR) - ₹"];
    return (
        <>
            <div className="mb-4">
                <Header
                    title="Currency"
                    headerBottomLine="Select your outlet currency according to your nationality."
                    label="(Currency)"
                    buttonTextColor="neutral-300"
                    dropdownLabel="Select currency"
                    labelMarginB="mb-2"
                    labelStyle="paragraphMediumItalic text-neutral-500"
                    shadow="shadow-smallDropDownShadow"
                    menuItems={currencyMenu}
                    hasDropDown
                />
            </div>
        </>
    );
}
