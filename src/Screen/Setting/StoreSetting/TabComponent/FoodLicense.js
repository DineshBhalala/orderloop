import React from "react";
import Header from "../../Components/Header";

export default function FoodLicense() {
    return (
        <>
            <div className="mb-4">
                <Header
                    hasInputField
                    title="Food license"
                    headerBottomLine="Enter food license number of your outlet."
                    labelMarginB="mb-2"
                    label="(License number)"
                    labelStyle="paragraphMediumItalic text-neutral-500"
                    placeholder="Enter license number"
                    shadow="shadow-smallDropDownShadow"
                />
            </div>
        </>
    );
}
