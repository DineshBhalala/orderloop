import React, { useEffect, useState } from "react";
import Address from "./TabComponent/Address";
import Appearance from "./TabComponent/Appearance";
import OrderingMode from "./TabComponent/OrderingMode";
import BillSetting from "./TabComponent/BillSetting";
import BillPayment from "./TabComponent/BillPayment";
import Contact from "./TabComponent/Contact";
import ServiceRadius from "./TabComponent/ServiceRadius";
import PreparationTime from "./TabComponent/PreparationTime";
import DeliveryCharges from "./TabComponent/DeliveryCharges";
import PaymentSetting from "./TabComponent/PaymentSetting";
import PaymentMethod from "./TabComponent/PaymentMethod";
import FoodLicense from "./TabComponent/FoodLicense";
import Currency from "./TabComponent/Currency";
import Timing from "./TabComponent/Timing";
import RiderSetting from "./TabComponent/RiderSetting";
import Taxes from "./TabComponent/Taxes";
import OtherSetting from "./TabComponent/OtherSetting";
import OtherCharges from "./TabComponent/OtherCharges";
import ThirdPartyPOS from "./TabComponent/ThirdPartyPOS";
import CodLimit from "./TabComponent/CodLimit";
import { LargeDestructiveButton, LargePrimaryButton } from "../../../Components/Buttons/Button";
import { MultipleTab } from "../../../Components/Tabs/Tabs";

export default function StoreSetting() {
    const menuItems = [
        "Ordering mode",
        "Appearance",
        "Address",
        "Bill setting",
        "Bill payment",
        "Contact",
        "Service radius",
        "Preparation time",
        "Delivery charges",
        "Payment setting",
        "Payment method",
        "Currency",
        "Timing",
        "Rider setting",
        "Taxes",
        "Food license",
        "Other setting",
        "Other charges",
        "Third party POS",
        "COD limit",
    ];

    const [activeTab, setActiveTab] = useState(0);

    const handleTabClick = (index) => {
        setActiveTab(index);
    };

    useEffect(() => {
        switch (activeTab) {
            case 0:
                setRenderPage(<OrderingMode />);
                break;

            case 1:
                setRenderPage(<Appearance />);
                break;

            case 2:
                setRenderPage(<Address />);
                break;

            case 3:
                setRenderPage(<BillSetting />);
                break;

            case 4:
                setRenderPage(<BillPayment />);
                break;

            case 5:
                setRenderPage(<Contact />);
                break;

            case 6:
                setRenderPage(<ServiceRadius />);
                break;

            case 7:
                setRenderPage(<PreparationTime />);
                break;

            case 8:
                setRenderPage(<DeliveryCharges />);
                break;

            case 9:
                setRenderPage(<PaymentSetting />);
                break;

            case 10:
                setRenderPage(<PaymentMethod />);
                break;

            case 11:
                setRenderPage(<Currency />);
                break;

            case 12:
                setRenderPage(<Timing />);
                break;

            case 13:
                setRenderPage(<RiderSetting />);
                break;

            case 14:
                setRenderPage(<Taxes />);
                break;

            case 15:
                setRenderPage(<FoodLicense />);
                break;

            case 16:
                setRenderPage(<OtherSetting />);
                break;

            case 17:
                setRenderPage(<OtherCharges />);
                break;

            case 18:
                setRenderPage(<ThirdPartyPOS />);
                break;

            case 19:
                setRenderPage(<CodLimit />);
                break;

            default:
                break;
        }
    }, [activeTab]);

    const [renderPage, setRenderPage] = useState("");

    return (
        <>
            <div className="flex flex-row md:block">
                <div className="flex flex-col mt-4 pr-6 mr-5 border-r min-w-[194px] border-neutral-300 h-auto pb-20 scrollbarStyle md:hidden overflow-auto [&::-webkit-scrollbar]:hidden lg:pr-4 lg:mr-3 lg:min-w-[186px]">
                    {menuItems.map((el, index) => (
                        <div className="max-w-[169px] mb-4 lg:mb-2" key={index}>
                            <MultipleTab maxWidth="max-w-[169px]" label={el} index={index} onClick={handleTabClick} isActive={activeTab === index} />
                        </div>
                    ))}
                </div>

                <div className="mt-4 w-full pl-1 md:pl-0 md:mt-0 overflow-auto pb-20">{renderPage}</div>
            </div>
            <div className="-mx-8 lg:-mx-4 sticky z-[100] md:z-[8] md:fixed md:border-none md:shadow-dropShadow left-0 right-0 bottom-0 bg-white  border-t border-neutral-300">
                <div className="flex flex-row justify-end w-full md:justify-center px-8 py-6 lg:py-4 md:py-0 md:pb-1 md:pt-2">
                    <div className="min-w-[196px] mr-5 md:w-1/2 md:mr-[7.5px] md:min-w-0">
                        <LargeDestructiveButton name="Discard" />
                    </div>
                    <div className="min-w-[196px] md:w-1/2 md:ml-[7.5px] md:min-w-0">
                        <LargePrimaryButton name="Save" />
                    </div>
                </div>
            </div>
        </>
    );
}
