import React from "react";
import { ReactComponent as CloseIcon } from "../../../../Assets/close.svg";
import { ReactComponent as LeftArrowIcon } from "../../../../Assets/chevron-down.svg";
import { DefaultInputField } from "../../../../Components/InputField/InputField";
import { LargePrimaryButton } from "../../../../Components/Buttons/Button";

export default function AddThresholdPopup(props) {
  return (
    // <div className="fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-[101] flex md:relative px-10 md:px-0 overflow-auto">
    <div className={`fixed bg-black bg-opacity-50 inset-0 z-[101] flex md:absolute md:z-[9] md:top-0 md:block md:overflow-hidden`}>
      <div className="max-w-[475px] w-full rounded-xl bg-shades-50 pt-6 pb-9 px-8 m-auto max-h-[969px] md:max-w-full md:rounded-none md:py-4 md:px-4 md:h-screen md:overflow-hidden">
        <div className="flex flex-row items-center justify-between mb-6 md:hidden">
          <div>
            <h3 className="paragraphLargeMedium">Add threshold</h3>
            <div className="flex flex-row items-center">
              <p className="paragraphMediumItalic text-neutral-500">Enter threshold values to set within the system</p>
            </div>
          </div>
          <span className="cursor-pointer" onClick={props.handleClickClose}>
            <CloseIcon />
          </span>
        </div>

        <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 px-8 md:px-0 cursor-pointer" onClick={() => props.handleClickClose()}>
          <LeftArrowIcon className="rotate-90" />
          <span className="ml-1">Back to delivery charges</span>
        </div>

        <div className="mb-4">
          <DefaultInputField labelMarginB="mb-2" labelStyle="paragraphLargeMedium" label="Delivery charge threshold" placeholder="Enter amount in rupees" shadow="shadow-shadowXsmall" />
        </div>
        <div className="mb-4">
          <DefaultInputField labelMarginB="mb-2" labelStyle="paragraphLargeMedium" label="Charge above threshold" placeholder="Enter amount in rupees" shadow="shadow-shadowXsmall" />
        </div>
        <div className="mb-12">
          <DefaultInputField labelMarginB="mb-2" labelStyle="paragraphLargeMedium" label="Charge below threshold" placeholder="Enter amount in rupees" shadow="shadow-shadowXsmall" />
        </div>

        <div className="sticky z-[1000] md:fixed md:justify-center md:border-none md:py-0 md:pb-1 md:pt-2 md:shadow-dropShadow left-0 right-0 bottom-0 flex flex-row w-full mx-auto bg-white md:px-4">
          <LargePrimaryButton name="Add charges" />
        </div>
      </div>
    </div>
  );
}
