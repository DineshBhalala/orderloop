import React from "react";
import { ReactComponent as AddIcon } from "../../../../Assets/add.svg";
import { ReactComponent as TrashIcon } from "../../../../Assets/trash.svg";
import { ReactComponent as DropDownIcon } from "../../../../Assets/chevron-down.svg";

export const CategoryLayoutTable = (props) => {
  return (
    <>
      <tr className={`paragraphSmallRegular text-left justify-center h-[70px] ${props.rowNumber !== 1 && "border-t"} border-neutral-300`}>
        <td className="px-6">{props.rowNumber < 10 ? "0" + props.rowNumber : props.rowNumber}</td>
        <td className="px-6">
          <div className="flex flex-row items-center">
            <span className="mr-3">{props.categoryDisplayCount < 10 ? "0" + props.categoryDisplayCount : props.categoryDisplayCount}</span>
            <DropDownIcon height={18} />
          </div>
        </td>

        <td className="px-6">
          <div className="flex flex-row items-center">
            <div className="flex flex-row items-center mr-6">
              <AddIcon stroke="#6C5DD3" />
              <span className="ml-1">Add row</span>
            </div>
            <div className="flex flex-row items-center">
              <TrashIcon stroke="#EF4444" />
              <span className="ml-1 text-destructive-500">Delete row</span>
            </div>
          </div>
        </td>
      </tr>
    </>
  );
};
