import React, { useState } from "react";
import { DefaultBreadcrumbs } from "../../Components/Breadcrumbs/Breadcrumbs";
import { ReactComponent as SettingIcon } from "../../Assets/settings.svg";
import { Tab } from "../../Components/Tabs/Tabs";
import UserSetting from "./UserSetting/UserSetting";
import StoreSetting from "./StoreSetting/StoreSetting";
import GlobalSetting from "./GlobalSetting/GlobalSetting";
import { DropDownTabs } from "../../Components/DropDown/DropDownTabs";

export default function Setting() {
    const [activeSetting, setActiveSetting] = useState("user");

    const handleClickSetting = (setting) => {
        setActiveSetting(setting);
    };

    return (
        <>
            <div className="bg-[#fafafa]">
                <div className="px-8 lg:px-4 pt-4 w-full max-w-[1336px] mx-auto bg-white relative md:max-w-full">
                    <div className="mb-4 md:hidden">
                        <DefaultBreadcrumbs mainTab="Settings" icon={<SettingIcon className="h-5 w-5" />} />
                    </div>

                    <div className="flex flex-row items-center mb-4 pb-4 border-b border-neutral-300 md:hidden">
                        <div className="mr-4 lg:mr-2 cursor-pointer" onClick={() => handleClickSetting("user")}>
                            <Tab label="User settings" isActive={activeSetting === "user"} />
                        </div>
                        <div className="mr-4 lg:mr-2 cursor-pointer" onClick={() => handleClickSetting("store")}>
                            <Tab label="Store settings" isActive={activeSetting === "store"} />
                        </div>
                        <div className="cursor-pointer" onClick={() => handleClickSetting("global")}>
                            <Tab label="Global settings" isActive={activeSetting === "global"} />
                        </div>
                    </div>

                    <div className="hidden md:block w-full mb-4 pb-4 border-b border-neutral-300">
                        <DropDownTabs
                            menuItems={[
                                { item: "User setting", onClick: () => handleClickSetting("user") },
                                { item: "Store setting", onClick: () => handleClickSetting("store") },
                                { item: "Global setting", onClick: () => handleClickSetting("global") },
                            ]}
                        />
                    </div>

                    <div className={`${activeSetting !== "user" && "hidden"}`}>
                        <UserSetting />
                    </div>

                    <div className={`${activeSetting !== "store" && "hidden"} -mt-4 md:-mt-0`}>
                        <StoreSetting />
                    </div>

                    <div className={`${activeSetting !== "global" && "hidden"} -mt-4 md:-mt-0`}>
                        <GlobalSetting />
                    </div>
                </div>
            </div>
        </>
    );
}
