import React, { useEffect, useState } from "react";
import AppSetting from "./Components/AppSetting";
import Brand from "./Components/Brand";
import SMSGateway from "./Components/SMSGateway";
import Maps from "./Components/Maps";
import Languages from "./Components/Languages";
// import AboutUs from "./Components/AboutUs";
import AboutUs from './Components/AboutUs'
import Legal from "./Components/Legal";
import CashbackSettings from "./Components/CashbackSettings";
import OrderingMode from "./Components/OrderingMode";
import { MultipleTab } from "../../../Components/Tabs/Tabs";
import { LargeDestructiveButton, LargePrimaryButton } from "../../../Components/Buttons/Button";
import ContactUs from "./Components/ContactUs";

export default function StoreSetting() {
    const menuItems = ["App settings", "Brand", "SMS gateway", "Maps", "Languages", "About us", "Contact us", "Legal", "Cashback settings", "Ordering mode"];

    const [activeTab, setActiveTab] = useState(0);

    const handleTabClick = (index) => {
        setActiveTab(index);
    };

    useEffect(() => {
        switch (activeTab) {
            case 0:
                setRenderPage(<AppSetting />);
                break;
            case 1:
                setRenderPage(<Brand />);
                break;

            case 2:
                setRenderPage(<SMSGateway />);
                break;

            case 3:
                setRenderPage(<Maps />);
                break;

            case 4:
                setRenderPage(<Languages />);
                break;

            case 5:
                setRenderPage(<AboutUs />);
                break;

            case 6:
                setRenderPage(<ContactUs />);
                break;

            case 7:
                setRenderPage(<Legal />);
                break;

            case 8:
                setRenderPage(<CashbackSettings />);
                break;
            case 9:
                setRenderPage(<OrderingMode />); //this can be used from store setting orderingMode component
                break;

            default:
                break;
        }
    }, [activeTab]);

    const [renderPage, setRenderPage] = useState("");

    return (
        <>
            <div className="flex flex-row md:block">
                <div className="flex flex-col mt-4 pr-6 mr-5 border-r min-w-[200px] border-neutral-300 h-auto scrollbarStyle md:hidden overflow-auto [&::-webkit-scrollbar]:hidden lg:pr-4 lg:mr-3 lg:min-w-[190px] pb-20">
                    {menuItems.map((el, index) => (
                        <div className="min-w-[175px] w-full mb-4 lg:mb-2" key={index}>
                            <MultipleTab label={el} index={index} onClick={handleTabClick} isActive={activeTab === index} maxWidth="max-w-[175px]" />
                        </div>
                    ))}
                </div>

                <div className="mt-4 w-full pl-1 md:pl-0 md:mt-0 overflow-auto pb-20">{renderPage}</div>
            </div>
            <div className="-mx-8 lg:-mx-4 sticky z-[100] md:z-[8] md:fixed md:border-none md:shadow-dropShadow left-0 right-0 bottom-0 bg-white  border-t border-neutral-300">
                <div className="flex flex-row justify-end w-full md:justify-center px-8 py-6 lg:py-4 md:py-0 md:pb-1 md:pt-2">
                    <div className="min-w-[196px] mr-5 md:w-1/2 md:mr-[7.5px] md:min-w-0">
                        <LargeDestructiveButton name="Discard" />
                    </div>
                    <div className="min-w-[196px] md:w-1/2 md:ml-[7.5px] md:min-w-0">
                        <LargePrimaryButton name="Save" />
                    </div>
                </div>
            </div>
        </>
    );
}
