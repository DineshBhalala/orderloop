import React from "react";
import Header from "../../Components/Header";

export default function Maps() {
    return (
        <>
            <div className="">
                <Header
                    title="Google maps"
                    headerBottomLine="API settings for your Google Maps account."
                    marginBetween="mt-2"
                    hasInputField
                    label="(Google API key)"
                    placeholder="Enter Google API Key"
                    labelMarginB="mb-2"
                    shadow="shadow-smallDropDownShadow"
                    labelStyle="paragraphMediumItalic text-neutral-500"
                />
            </div>
        </>
    );
}
