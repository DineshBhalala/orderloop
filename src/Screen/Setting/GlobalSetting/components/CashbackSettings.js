import React from "react";
import Header from "../../Components/Header";
import DropDownUnableDisable from "./DropDownUnableDisable";
import ToggleSwitch from "../../../../Components/ToggleSwitch/ToggleSwitch";
import { DefaultInputField } from "../../../../Components/InputField/InputField";

export default function CashbackSettings() {
    return (
        <>
            <div className="max-w-[636px] pb-20 md:max-w-full">
                <div className="mb-2">
                    <Header title="Cashback settings" headerBottomLine="Enable or disable cashback for your mobile application and set the parameters for cashback." />
                </div>
                <div className="max-w-[312px] mb-4 md:max-w-full">
                    <DropDownUnableDisable />
                </div>
                <div className="mb-4">
                    <Header
                        title="Cashback percentage"
                        headerBottomLine="Enter the percent of cashback earnings that the customers will get on the order amount."
                        hasInputField
                        placeholder="Enter cashback in percentage"
                        marginBetween="mt-2"
                        shadow="shadow-smallDropDownShadow"
                    />
                </div>
                <div className="mb-6 pb-6 border-b border-nuetral-300">
                    <Header
                        title="Signup cashback"
                        headerBottomLine="Enter the amount of cashback a user will get when performing sign-up action within the mobile application."
                        hasInputField
                        placeholder="Enter cashback amount in rupees"
                        marginBetween="mt-2"
                        shadow="shadow-smallDropDownShadow"
                    />
                </div>
                <div className="mb-4">
                    <Header title="Advance settings" hasSwitch headerBottomLine="Enable or disable advance cashback settings for your mobile application and set the parameters for cashback." />
                </div>
                <div className="mb-4">
                    <div className="flex flex-row items-center mb-1">
                        <h1 className="paragraphLargeMedium mr-4 md:paragraphMediumMedium">Same configuration for all ordering modes</h1>
                        <ToggleSwitch />
                    </div>
                </div>
                <div className="mb-4">
                    <Header title="Cashback redeem percentage" hasInputField placeholder="Enter cashback in percentage" marginBetween="mt-2" shadow="shadow-smallDropDownShadow" />
                </div>
                <div className="pb-6 mb-6 border-b border-neutral-300">
                    <Header title="Combine offers" headerBottomLine="Combine other offers with the cashback." />
                    <div className="max-w-[312px] mt-2 md:max-w-full">
                        <DropDownUnableDisable />
                    </div>
                </div>
                <div className="mb-4">
                    <Header title="Earnings delay" headerBottomLine="Set the delay time for the cashback to be rewarded to the customer on their orders." hasSwitch />
                </div>
                <div className="mb-4">
                    <div className="flex flex-row items-center mb-1">
                        <h1 className="paragraphLargeMedium mr-4 md:paragraphMediumMedium">Same configuration for all ordering modes</h1>
                        <ToggleSwitch />
                    </div>
                </div>
                <div className="mb-2">
                    <h1 className="paragraphLargeMedium mr-4 md:paragraphMediumMedium">Cashback earnings delay</h1>
                </div>
                <div className="flex flex-row lg:block max-w-[312px] md:max-w-full">
                    <div className="mr-1.5 w-1/2 lg:w-full lg:mr-0 lg:mb-2">
                        <DefaultInputField placeholder="Enter hours" shadow="shadow-smallDropDownShadow" />
                    </div>
                    <div className="ml-1.5 w-1/2 lg:w-full lg:ml-0">
                        <DefaultInputField placeholder="Enter minutes" shadow="shadow-smallDropDownShadow" />
                    </div>
                </div>
            </div>
        </>
    );
}
