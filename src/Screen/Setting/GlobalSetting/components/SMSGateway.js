import React from "react";
import Header from "../../Components/Header";

export default function SMSGateway() {
    return (
        <>
            <div className="">
                <Header
                    title="2Factor SMS gateway"
                    headerBottomLine="API Settings for your 2Factor SMS account"
                    marginBetween="mt-2"
                    hasInputField
                    label="(2Factor SMS API key)"
                    labelMarginB="mb-2"
                    placeholder="Enter SMS API Key"
                    shadow="shadow-smallDropDownShadow"
                    labelStyle="paragraphMediumItalic text-neutral-500"
                />
            </div>
        </>
    );
}
