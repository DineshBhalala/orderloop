import React from "react";
import Tag from "../../../../Components/Tag/Tag";
import Header from "../../Components/Header";

export default function Languages() {
    return (
        <>
            <div className="mb-4">
                <div className="pb-1">
                    <Header
                        title="Application languages"
                        headerBottomLine="Select the languages you want your mobile application to be in. "
                        label="(Select languages)"
                        buttonTextColor="neutral-300"
                        dropdownLabel="Select languages"
                        labelMarginB="mb-2"
                        marginBetween="mt-2"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        shadow="shadow-smallDropDownShadow"
                        hasDropDown
                    />
                </div>

                <div className="flex flex-row">
                    <div className="mr-2">
                        <Tag tag="English" />
                    </div>
                    <div className="mr-2">
                        <Tag tag="ગુજરાતી" />
                    </div>
                </div>
            </div>

            <div className="paragraphMediumItalic text-neutral-500 max-w-[636px]">
                <p>
                    <span className="text-neutral-900">NOTE:</span>You will have to add the menu and other details in all the languages you select.
                </p>
            </div>
        </>
    );
}
