import React from "react";
import Header from "../../Components/Header";
import { DefaultInputField } from "../../../../Components/InputField/InputField";
import DropDownUnableDisable from "./DropDownUnableDisable";
import mobileImage from "../../../../Assets/mobile.png";

export default function AppSetting() {
    return (
        <>
            <div className="flex flex-row justify-between xl:block -mb-20 xl:mb-0">
                <div className="min-w-[661px] pr-6 pb-20 border-r border-neutral-300 w-full xl:pr-0 xl:pb-6 xl:border-r-0 xl:border-b xl:mb-6 xl:min-w-full">
                    <div className="mb-2">
                        <Header title="Restaurant name" headerBottomLine="Please enter the name of your restaurant/brand you want your customers to see in various places." />
                    </div>
                    <div className="flex flex-row items-center mb-4 lg:block">
                        <div className="mr-1.5 w-1/2 lg:w-full lg:mb-2 lg:max-w-[312px] md:max-w-full lg:mr-0">
                            <DefaultInputField
                                labelMarginB="mb-2"
                                label="(English)"
                                labelStyle="paragraphMediumItalic text-neutral-500"
                                placeholder="Enter restaurant name in English"
                                shadow="shadow-smallDropDownShadow"
                            />
                        </div>
                        <div className="ml-1.5 w-1/2 lg:w-full lg:max-w-[312px] md:max-w-full lg:ml-0">
                            <DefaultInputField
                                labelMarginB="mb-2"
                                label="(ગુજરાતી)"
                                labelStyle="paragraphMediumItalic text-neutral-500"
                                placeholder="Enter restaurant name in ગુજરાતી"
                                shadow="shadow-smallDropDownShadow"
                            />
                        </div>
                    </div>

                    <div className="mb-4">
                        <Header
                            title="Application display name"
                            headerBottomLine="Please enter the display name of your restaurant that you want your customers to see in various parts of the application."
                            hasInputField
                            placeholder="Enter application display name"
                        />
                    </div>

                    <div className="mb-4">
                        <Header title="Enable customer email compulsory" headerBottomLine="Enabling this will make customers input their email id compulsorily." />
                    </div>
                    <div className="max-w-[312px] mb-4 md:max-w-full">
                        <DropDownUnableDisable />
                    </div>

                    <div className="mb-4">
                        <Header title="Apply mask on category images" headerBottomLine="Enabling this will mask the images you upload to the categories for a better look and view." />
                    </div>
                    <div className="max-w-[312px] mb-4 md:max-w-full">
                        <DropDownUnableDisable />
                    </div>
                    <div className="mb-4">
                        <Header
                            title="Logo height"
                            headerBottomLine="Adjust the height of your brand logo on the mobile application's home screen."
                            hasInputField
                            placeholder="Enter logo height in pixels"
                        />
                    </div>
                    <div className="">
                        <Header title="Offer usage time" headerBottomLine="Set Minutes, after which a second offer can apply." hasInputField placeholder="Enter time in minutes" />
                    </div>
                </div>
                <div className="w-full flex justify-center">
                    <div className="w-full max-w-[329px] md:max-w-[280px] ml-8 xl:ml-0">
                        <img src={mobileImage} alt="" />
                    </div>
                </div>
            </div>
        </>
    );
}
