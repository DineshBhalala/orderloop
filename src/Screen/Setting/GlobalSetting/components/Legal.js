import React from "react";
import { InputArea } from "../../../../Components/InputField/InputField";
import Header from "../../Components/Header";

export default function Legal() {
    return (
        <>
            <div className="max-w-[636px] md:max-w-full md:pb-20">
                <div className="mb-2">
                    <Header title="Return policy" headerBottomLine="Write about your return policy for the customer's orders." />
                </div>
                <div className="mb-4">
                    <InputArea placeholder="Enter return policy in english" shadow="shadow-smallDropDownShadow" paddingT="pt-4" label="(English)" labelStyle="paragraphMediumItalic text-neutral-500" />
                </div>
                <div className="mb-4">
                    <InputArea placeholder="Enter return policy in ગુજરાતી" shadow="shadow-smallDropDownShadow" paddingT="pt-4" label="(ગુજરાતી)" labelStyle="paragraphMediumItalic text-neutral-500" />
                </div>
            </div>
        </>
    );
}
