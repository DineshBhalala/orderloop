import React from "react";
import Header from "../../Components/Header";
import ToggleSwitch from "../../../../Components/ToggleSwitch/ToggleSwitch";
import { LargePrimaryButton } from "../../../../Components/Buttons/Button";
import { ReactComponent as AddIcon } from "../../../../Assets/add.svg";
import { ListViewOutletOrderingMode } from "../../../../Components/ListView/ListViewStoreSetting";

export default function OrderingMode(props) {
    const tableDetails = [
        {
            modeName: "Delivery",
            orderType: "QSR",
            orderTab: "Dine in",
        },
        {
            modeName: "Takeaway",
            orderType: "Fine dine",
            orderTab: "Dine in",
        },
        {
            modeName: "QSR",
            orderType: "QSR",
            orderTab: "Dine in",
        },
        {
            modeName: "Fine dine",
            orderType: "Fine dine",
            orderTab: "Dine in",
        },
    ];

    return (
        <>
            <div className="w-full flex flex-row justify-between xl:block">
                <div className="mb-6 max-w-[636px] xl:mb-2">
                    <Header title="Ordering mode" headerBottomLine="Create a new ordering mode or choose to enable or disable the ordering mode a customer can select for your outlet." />
                </div>

                <div className="max-w-[187px] xl:mb-4 md:max-w-full">
                    <LargePrimaryButton name="Ordering mode" leftIconDefault={<AddIcon stroke="#FFFFFF" />} leftIconClick={<AddIcon stroke="#C4BEED" />} />
                </div>
            </div>
            <div className="max-w-[636px] w-full md:max-w-full">
                <div className="w-full rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden border-neutral-300 border lg:max-w-[462px]">
                    <table className="w-full break-words tableMediaLibrary">
                        <thead>
                            <tr className="shadow-innerShadow bg-neutral-50 text-left justify-center h-11">
                                <th className="px-6 min-w-[124px] paragraphOverlineSmall text-neutral-700">STATUS</th>
                                <th className="px-6 min-w-[184px] paragraphOverlineSmall text-neutral-700">MODE NAME</th>
                                <th className="px-6 min-w-[159px] paragraphOverlineSmall text-neutral-700">ORDER TYPE</th>
                                <th className="px-6 min-w-[167px] paragraphOverlineSmall text-neutral-700">ORDER TAB</th>
                            </tr>
                        </thead>
                        <tbody>
                            {tableDetails.map((el, index) => (
                                <tr className={`paragraphSmallRegular ${index !== 0 && "border-t"} border-neutral-300 justify-center h-[70px]`} key={index}>
                                    <td className="px-6">{<ToggleSwitch />}</td>
                                    <td className="px-6">{el.modeName}</td>
                                    <td className="px-6">{el.orderTab}</td>
                                    <td className="px-6">{el.orderType}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
                <div className="pb-[70px] hidden md:block">
                    {tableDetails.map((el, index) => (
                        <div className="mt-2" key={index}>
                            <ListViewOutletOrderingMode modeName={el.modeName} orderType={el.orderType} orderTab={el.orderTab} />
                        </div>
                    ))}
                </div>
            </div>
        </>
    );
}
