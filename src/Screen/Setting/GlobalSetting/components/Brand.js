import React, { useState } from "react";
import Header from "../../Components/Header";
import { DefaultInputField } from "../../../../Components/InputField/InputField";
import mobileImage from "../../../../Assets/mobile.png";
import UploadImage from "../../../../Components/UploadImage/UploadImage";
import wolfPuckBrandLogo from "../../../../Assets/wolf-puck-brand-logo.png";
import wolfPuckBrandLogoFooter from "../../../../Assets/wolf-puck-footer.png";
import { LargeTertiaryButton } from "../../../../Components/Buttons/Button";

export default function Brand() {
    const uploadImageShowHideController = () => {
        setShowUploadImagePopup(!showUploadImagePopup);
    };

    const selectType = <DefaultInputField label="Select type" placeholder="Brand logos" placeholderTextColor="neutral-900" />;

    const [showUploadImagePopup, setShowUploadImagePopup] = useState(false);

    const [showSelectedImage, setShowSelectedImage] = useState(false);

    const handleClickSelect = () => {
        setShowSelectedImage(!showSelectedImage);
    };

    return (
        <>
            <div className="flex flex-row justify-between xl:block -mb-20 xl:mb-0">
                <div className="min-w-[661px] pr-6 pb-20 border-r border-neutral-300 w-full xl:pr-0 xl:pb-2 xl:border-r-0 xl:border-b xl:mb-6 xl:min-w-full md:pb-0">
                    <div className="mb-2">
                        <Header title="Brand logos" headerBottomLine="Upload the brand logos for the application." />
                    </div>
                    <div className="max-w-[411px]">
                        <div className="mb-2">
                            <span className="paragraphMediumItalic text-neutral-500">Brand logo(1920px x 1080px or 16:9 ratio)</span>
                        </div>

                        <div className={`${!showSelectedImage ? "hidden" : "flex"} flex-row items-start mb-4`}>
                            <img src={wolfPuckBrandLogo} alt="" className="p-2.5 border border-neutral-300 rounded max-w-[235px]" />
                            <div className="ml-2">
                                <div className="mb-2">
                                    <LargeTertiaryButton name="Upload another" />
                                </div>
                                <div className="">
                                    <LargeTertiaryButton name="Replace image" />
                                </div>
                            </div>
                        </div>

                        {/* this component can be used from banner page */}

                        <div className={`${showSelectedImage ? "hidden" : "flex"} flex-row md:flex-col justify-between mb-4 md:max-w-[197px]`}>
                            <button
                                className="justify-center py-3 paragraphMediumRegular border-neutral-300 rounded-md border w-1/2 mr-2 mobile:text-sm md:mb-2 md:mr-0 md:w-full cursor-pointer"
                                onClick={uploadImageShowHideController}
                            >
                                Upload an image
                            </button>
                            <button className="justify-center py-3 paragraphMediumRegular border-neutral-300 rounded-md border w-1/2 ml-2 md:ml-0 mobile:text-sm md:w-full cursor-pointer" onClick={handleClickSelect}>
                                Select from library
                            </button>
                        </div>
                        <div className="mb-2">
                            <span className="paragraphMediumItalic text-neutral-500">Footer logo(1080px x 1080px or 1:1 ratio)</span>
                        </div>

                        <div className={`${!showSelectedImage ? "hidden" : "flex"} flex-row items-start mb-4`}>
                            <img src={wolfPuckBrandLogoFooter} alt="" className="p-2.5 border border-neutral-300 rounded max-w-[235px]" />
                            <div className="ml-2">
                                <div className="mb-2">
                                    <LargeTertiaryButton name="Upload another" />
                                </div>
                                <div className="">
                                    <LargeTertiaryButton name="Replace image" />
                                </div>
                            </div>
                        </div>

                        <div className={`${showSelectedImage ? "hidden" : "flex"} flex flex-row md:flex-col justify-between mb-4 md:max-w-[197px]`}>
                            <button
                                className="justify-center py-3 paragraphMediumRegular border-neutral-300 rounded-md border w-1/2 mr-2 md:mr-0 mobile:text-sm md:mb-2 md:w-full"
                                onClick={uploadImageShowHideController}
                            >
                                Upload an image
                            </button>
                            <button className="justify-center py-3 paragraphMediumRegular border-neutral-300 rounded-md border w-1/2 ml-2 md:ml-0 mobile:text-sm md:w-full" onClick={handleClickSelect}>
                                Select from library
                            </button>
                        </div>
                    </div>

                    <div className="mb-2">
                        <Header title="Brand colors" headerBottomLine="Select your brand colors for the application." />
                    </div>

                    <div className="mb-2">
                        <span className="paragraphMediumItalic text-neutral-500">Primary brand color</span>
                    </div>

                    <div className="flex flex-row justify-between mb-4 max-w-[197px] w-full">
                        <button className="w-full justify-center py-3 paragraphMediumRegular border-neutral-300 rounded-md border mr-2 mobile:mr-1 mobile:text-sm">Select color</button>
                    </div>

                    <div className="mb-2">
                        <span className="paragraphMediumItalic text-neutral-500">Secondary brand color</span>
                    </div>

                    <div className="flex flex-row justify-between mb-4 max-w-[197px] w-full">
                        <button className="justify-center py-3 paragraphMediumRegular border-neutral-300 w-full rounded-md border mr-2 mobile:mr-1 mobile:text-sm">Select color</button>
                    </div>

                    <div className="mb-2">
                        <Header title="Greeting message" headerBottomLine="Write up a quirky greeting message that will be displayed above categories on the home screen of the app." />
                    </div>
                    <div className="flex flex-row items-center mb-4 lg:block">
                        <div className="mr-1.5 w-1/2 lg:w-full lg:mb-2 lg:max-w-[312px] md:max-w-full lg:mr-0">
                            <DefaultInputField
                                labelMarginB="mb-2"
                                label="(English)"
                                labelStyle="paragraphMediumItalic text-neutral-500"
                                placeholder="Enter greeting message in english"
                                shadow="shadow-smallDropDownShadow"
                            />
                        </div>
                        <div className="ml-1.5 w-1/2 lg:w-full lg:max-w-[312px] md:max-w-full lg:ml-0">
                            <DefaultInputField
                                labelMarginB="mb-2"
                                label="(ગુજરાતી)"
                                labelStyle="paragraphMediumItalic text-neutral-500"
                                placeholder="Enter greeting message in ગુજરાતી"
                                shadow="shadow-smallDropDownShadow"
                            />
                        </div>
                    </div>
                    <div className="mb-2">
                        <Header title="Special instruction message" headerBottomLine="This message will be displayed on cart screen for special instruction box." />
                    </div>
                    <div className="flex flex-row items-center mb-4 lg:block">
                        <div className="mr-1.5 w-1/2 lg:w-full lg:mb-2 lg:max-w-[312px] md:max-w-full lg:mr-0">
                            <DefaultInputField
                                labelMarginB="mb-2"
                                label="(English)"
                                labelStyle="paragraphMediumItalic text-neutral-500"
                                placeholder="Enter instruction message in english"
                                shadow="shadow-smallDropDownShadow"
                            />
                        </div>
                        <div className="ml-1.5 w-1/2 lg:w-full lg:max-w-[312px] md:max-w-full lg:ml-0">
                            <DefaultInputField
                                labelMarginB="mb-2"
                                label="(ગુજરાતી)"
                                labelStyle="paragraphMediumItalic text-neutral-500"
                                placeholder="Enter instruction message in ગુજરાતી"
                                shadow="shadow-smallDropDownShadow"
                            />
                        </div>
                    </div>
                </div>
                <div className="w-full flex justify-center">
                    <div className="w-full max-w-[329px] md:max-w-[280px] ml-8 xl:ml-0">
                        <img src={mobileImage} alt="" />
                    </div>
                </div>
            </div>

            <div className={`${!showUploadImagePopup && "hidden"}`}>
                <UploadImage showHideUploadImagePage={uploadImageShowHideController} selectType={selectType} />
            </div>
        </>
    );
}
