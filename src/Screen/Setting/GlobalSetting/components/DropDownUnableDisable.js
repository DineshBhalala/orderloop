import React, { Fragment, useState } from "react";
import { Menu, Transition } from "@headlessui/react";
import { ReactComponent as DownArrowIcon } from "../../../../Assets/chevron-down.svg";
import { ReactComponent as EnableIcon } from "../../../../Assets/success-tick.svg";
import { ReactComponent as DisableIcon } from "../../../../Assets/cancel.svg";

export default function DropDownUnableDisable() {
  const menuItems = [
    { icon: <EnableIcon stroke="#FAFAFA" />, label: "Enable" },
    { icon: <DisableIcon stroke="#FAFAFA" />, label: "Disable" },
  ];
  const [dropDownLabel, setDropDownLabel] = useState(menuItems[0]);

  const handleClickMenuItem = (el) => {
    setDropDownLabel(el);
  };

  return (
    <>
      <div className="relative">
        <Menu as="div">
          <div className="dropDownIcon">
            <Menu.Button
              className={`shadow-shadowXsmall w-full flex flex-row justify-between mobile:max-w-full rounded-md outline-none focus:border-primary-500 border py-3 focus:ring-4 focus:ring-primary-100 appearance-none px-4 border-neutral-300 h-12 paragraphSmallRegular`}>
              <div className="flex flex-row items-center">
                {dropDownLabel.icon}
                <span className="ml-2 paragraphSmallRegular">{dropDownLabel.label}</span>
              </div>
              <DownArrowIcon className={`dropDownIconRotate min-w-[24px] min-h-[24px]`} />
            </Menu.Button>
          </div>
          <Transition
            as={Fragment}
            enter="transition ease-out duration-100"
            enterFrom="transform opacity-0 scale-95"
            enterTo="transform opacity-100 scale-100"
            leave="transition ease-in duration-75"
            leaveFrom="transform opacity-100 scale-100"
            leaveTo="transform opacity-0 scale-95">
            <Menu.Items className="absolute left-0 right-0 mt-2 px-4 py-2 border paragraphSmallMedium rounded-md shadow-shadowMedium bg-shades-50 font-normal z-50">
              {menuItems.map((el, index) => {
                return (
                  <div className="pt-2 mb-2 cursor-pointer" key={index} onClick={() => handleClickMenuItem(el)}>
                    <Menu.Item>
                      <div className="flex flow-row items-center">
                        {el.icon}
                        <span className="paragraphSmallRegular ml-1">{el.label}</span>
                      </div>
                    </Menu.Item>
                  </div>
                );
              })}
            </Menu.Items>
          </Transition>
        </Menu>
      </div>
    </>
  );
}
