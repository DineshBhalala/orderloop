import React from "react";
import Header from "../../Components/Header";
import { InputArea } from "../../../../Components/InputField/InputField";

export default function AboutUs() {
    return (
        <>
            <div className="max-w-[636px] md:max-w-full md:pb-20">
                <div className="mb-2">
                    <Header title="About us" headerBottomLine="Write about us information that will go into the mobile application for your customers to see in the help section." />
                </div>
                <div className="mb-4">
                    <InputArea
                        placeholder="Enter about us information in english"
                        shadow="shadow-smallDropDownShadow"
                        paddingT="pt-4"
                        label="(English)"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                    />
                </div>
                <div className="mb-4">
                    <InputArea
                        placeholder="Enter about us information in ગુજરાતી"
                        shadow="shadow-smallDropDownShadow"
                        paddingT="pt-4"
                        label="(ગુજરાતી)"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                    />
                </div>
            </div>
        </>
    );
}
