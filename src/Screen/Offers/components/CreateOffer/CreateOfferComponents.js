import React, { useState } from "react";
import TimeSlotBanner from "../../../../Components/TimeSlot/TimeSlot";
import DropDown from "../../../../Components/DropDown/DropDown";
import ToggleSwitch from "../../../../Components/ToggleSwitch/ToggleSwitch";
import { DefaultInputField, InputArea } from "../../../../Components/InputField/InputField";

export const OfferType = () => {
    return (
        <div className="md:w-full">
            <h1 className="paragraphLargeMedium md:paragraphMediumMedium mb-1">Offer type</h1>
            <span className="paragraphMediumItalic text-neutral-500 md:paragraphSmallItalic">Select the type of offer you would like to create for your customers.</span>
            <div className="max-w-[311px] w-full relative mt-4 md:max-w-full">
                <DropDown labelMarginB="mb-2" dropdownLabel="Select type of offer" buttonTextColor="neutral-300" />
            </div>
        </div>
    );
};

export const BasicDetails = () => {
    return (
        <>
            <div className="max-w-[636px] md:max-w-full lg:mb-24 lg:max-w-[459px] w-full">
                <div className="flex flex-row justify-between mb-6 w-full">
                    <div className="">
                        <span className="paragraphLargeMedium">Hide offer</span>
                        <p className="paragraphMediumItalic text-neutral-500">Enable this will hide this offer when exhausted.</p>
                    </div>
                    <div className="ml-2">
                        <ToggleSwitch />
                    </div>
                </div>

                <div className="mb-4">
                    <span className="paragraphLargeMedium mb-1">Coupon code</span>
                    <p className="paragraphMediumItalic text-neutral-500">Please enter the code of the coupon you would like to create.</p>
                </div>

                <div className="max-w-[311px] w-full mb-6 md:max-w-full">
                    <DefaultInputField labelMarginB="mb-2" placeholder="Enter coupon code" />
                </div>

                <div className="mb-4">
                    <span className="paragraphLargeMedium">Offer title</span>
                    <p className="paragraphMediumItalic text-neutral-500">Please enter the title of the offer your customers will see within the application.</p>
                </div>

                <div className="flex flex-row justify-between w-full mb-6 lg:block">
                    <div className="max-w-[311px] w-full lg:mb-3 md:max-w-full">
                        <DefaultInputField labelMarginB="mb-2" label="(English)" placeholder="Enter title in English" labelStyle="paragraphMediumItalic text-neutral-500" />
                    </div>

                    <div className="max-w-[311px] w-full md:max-w-full">
                        <DefaultInputField labelMarginB="mb-2" label="(ગુજરાતી)" placeholder="Enter title in ગુજરાતી" labelStyle="paragraphMediumItalic text-neutral-500" />
                    </div>
                </div>

                <div className="mb-4">
                    <span className="paragraphLargeMedium">Offer description</span>
                    <p className="paragraphMediumItalic text-neutral-500">Please enter the description of the offer your customers will see within the application.</p>
                </div>

                <div className="flex flex-row justify-between w-full mb-6 lg:block lg:mb-0">
                    <div className="max-w-[311px] w-full lg:mb-3 md:max-w-full">
                        <InputArea label="(English)" placeholder="Enter description in English" boxHeight="h-[120px]" type="offer" />
                    </div>

                    <div className="max-w-[311px] w-full md:max-w-full">
                        <InputArea label="(ગુજરાતી)" placeholder="Enter description in ગુજરાતી" boxHeight="h-[120px]" type="offer" />
                    </div>
                </div>
            </div>
        </>
    );
};

export const Configure = () => {
    return (
        <>
            <div className="max-w-[636px] md:max-w-full lg:mb-24 lg:max-w-[459px] w-full">
                <div className="mb-4">
                    <span className="paragraphLargeMedium">Simultaneous offer time</span>
                    <p className="paragraphMediumItalic text-neutral-500">Enter the time in minutes, after which a customer can use this offer again.</p>
                </div>

                <div className="max-w-[311px] w-full mb-6 md:max-w-full">
                    <DefaultInputField labelMarginB="mb-2" placeholder="Enter time in minutes" />
                </div>

                <div className="mb-4">
                    <span className="paragraphLargeMedium">Minimum cart value</span>
                    <p className="paragraphMediumItalic text-neutral-500">Select whether you would like to keep a minimum cart value to apply this offer.</p>
                </div>

                <div className="flex flex-row justify-between w-full mb-6 lg:block">
                    <div className="max-w-[311px] w-full relative lg:mb-4 md:max-w-full">
                        <DropDown
                            labelMarginB="mb-2"
                            label="(Select preference)"
                            buttonTextColor="neutral-300"
                            dropdownLabel="Select your preference"
                            labelStyle="paragraphMediumItalic text-neutral-500"
                        />
                    </div>

                    <div className="max-w-[311px] w-full md:max-w-full">
                        <DefaultInputField labelMarginB="mb-2" label="(Enter amount in rupees)" placeholder="Enter minimum cart value in rupees" labelStyle="paragraphMediumItalic text-neutral-300" />
                    </div>
                </div>

                <div className="mb-4">
                    <span className="paragraphLargeMedium">Offer implementation</span>
                    <p className="paragraphMediumItalic text-neutral-500">Select categories and dishes on which this offer would be applicable.</p>
                </div>

                <div className="max-w-[311px] w-full mb-4 relative md:max-w-full">
                    <DropDown labelMarginB="mb-2" label="(Select presets)" buttonTextColor="neutral-300" dropdownLabel="Select your preference" labelStyle="paragraphMediumItalic text-neutral-500" />
                </div>

                <div className="max-w-[392px] w-full mb-2 relative md:max-w-full">
                    <label className="paragraphMediumItalic text-neutral-500">(Select categories and dishes) - Needs to be edited</label>
                </div>

                <div className="max-w-[311px] w-full mb-4 relative md:max-w-full">
                    <DropDown labelMarginB="mb-2" buttonTextColor="neutral-300" dropdownLabel="Select your preference" />
                </div>
            </div>
        </>
    );
};

export const Clubbing = () => {
    const [selectedItemFromDropDown, setSelectedItemFromDropDown] = useState();

    return (
        <>
            <div className="max-w-[636px] md:max-w-full lg:mb-24 lg:max-w-[459px] w-full">
                <div className="mb-6">
                    <h1 className="paragraphLargeMedium mb-1">Club offer</h1>
                    <span className="paragraphMediumItalic text-neutral-500">Select whether you would like to club this offer with other existing offers.</span>
                    <div className="max-w-[311px] w-full relative mt-4 md:max-w-full">
                        <DropDown
                            labelMarginB="mb-2"
                            dropdownLabel="Select your preference"
                            buttonTextColor="neutral-300"
                            menuItems={["Yes", "No"]}
                            setSelectedItemFromDropDown={setSelectedItemFromDropDown}
                        />
                    </div>
                </div>

                <div className={`${(selectedItemFromDropDown === "No" || !selectedItemFromDropDown) && "hidden"}`}>
                    <div className="mb-6">
                        <h1 className="paragraphLargeMedium mb-1">Auto-apply offer</h1>
                        <span className="paragraphMediumItalic text-neutral-500">Select whether you would like to auto-apply this offer with other existing offers.</span>
                        <div className="max-w-[311px] w-full relative mt-4 md:max-w-full">
                            <DropDown
                                labelMarginB="mb-2"
                                dropdownLabel="Select your preference"
                                buttonTextColor="neutral-300"
                                menuItems={["Yes", "No"]}
                                setSelectedItemFromDropDown={setSelectedItemFromDropDown}
                            />
                        </div>
                    </div>

                    <div className="mb-6">
                        <h1 className="paragraphLargeMedium mb-1">Club offer</h1>
                        <span className="paragraphMediumItalic text-neutral-500">Select whether you would like to combine this offer with reward points.</span>
                        <div className="max-w-[311px] w-full relative mt-4 md:max-w-full">
                            <DropDown
                                labelMarginB="mb-2"
                                dropdownLabel="Select your preference"
                                buttonTextColor="neutral-300"
                                menuItems={["Yes", "No"]}
                                setSelectedItemFromDropDown={setSelectedItemFromDropDown}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export const VisibilityAndLinking = () => {
    return (
        <>
            <div className="max-w-[636px] md:max-w-full lg:mb-24 lg:max-w-[459px] w-full">
                <div className="mb-6">
                    <h1 className="paragraphLargeMedium mb-1">Coupon page</h1>
                    <span className="paragraphMediumItalic text-neutral-500">Select whether you would like to make the offer visible on coupon page.</span>
                    <div className="max-w-[311px] w-full relative mt-4 md:max-w-full">
                        <DropDown labelMarginB="mb-2" dropdownLabel="Select your preference" buttonTextColor="neutral-300" menuItems={["Yes", "No"]} />
                    </div>
                </div>

                <div className="mb-6">
                    <h1 className="paragraphLargeMedium mb-1">Offer tab</h1>
                    <span className="paragraphMediumItalic text-neutral-500">Select whether you would like to make the offer visible in offer tab.</span>
                    <div className="max-w-[311px] w-full relative mt-4 md:max-w-full">
                        <DropDown labelMarginB="mb-2" dropdownLabel="Select your preference" buttonTextColor="neutral-300" menuItems={["Yes", "No"]} />
                    </div>
                </div>

                <div className="mb-6">
                    <h1 className="paragraphLargeMedium mb-1">Select users</h1>
                    <span className="paragraphMediumItalic text-neutral-500">Select whether you would like to provide this offer to everyone or specific users.</span>
                    <div className="max-w-[311px] w-full relative mt-4 md:max-w-full">
                        <DropDown labelMarginB="mb-2" dropdownLabel="Select your preference" buttonTextColor="neutral-300" menuItems={["Yes", "No"]} />
                    </div>
                </div>
            </div>
        </>
    );
};

export const ValidityAndApplicability = () => {
    return (
        <>
            <div className="max-w-[636px] md:max-w-full lg:mb-24 lg:max-w-[459px] w-full">
                <div className="mb-6">
                    <h1 className="paragraphLargeMedium mb-1">Coupon limit</h1>
                    <span className="paragraphMediumItalic text-neutral-500">Select the number of times the coupon could be used in total.</span>
                    <div className="max-w-[311px] w-full relative mt-4 md:max-w-full">
                        <DropDown labelMarginB="mb-2" dropdownLabel="Select your preference" buttonTextColor="neutral-300" menuItems={["Yes", "No"]} />
                    </div>
                </div>

                <div className="mb-6">
                    <h1 className="paragraphLargeMedium mb-1">Usage numbers</h1>
                    <span className="paragraphMediumItalic text-neutral-500">Select whether you would like to set the number of times an individual can use the coupon.</span>
                    <div className="max-w-[311px] w-full relative mt-4 md:max-w-full">
                        <DropDown labelMarginB="mb-2" dropdownLabel="Select your preference" buttonTextColor="neutral-300" menuItems={["Yes", "No"]} />
                    </div>
                </div>

                <div className="mb-4">
                    <span className="paragraphLargeMedium">Order counts</span>
                    <p className="paragraphMediumItalic text-neutral-500">Select minimum and maximum order counts required to use this offer.</p>
                </div>

                <div className="flex flex-row justify-between w-full mb-6 lg:block">
                    <div className="max-w-[311px] w-full lg:mb-4 md:max-w-full">
                        <DefaultInputField labelMarginB="mb-2" label="(Minimum order count)" placeholder="Enter minimum order count" labelStyle="paragraphMediumItalic text-neutral-300" />
                    </div>

                    <div className="max-w-[311px] w-full md:max-w-full">
                        <DefaultInputField labelMarginB="mb-2" label="(Maximum order count)" placeholder="Enter maximum order count" labelStyle="paragraphMediumItalic text-neutral-300" />
                    </div>
                </div>

                <div className="mb-6">
                    <h1 className="paragraphLargeMedium mb-1">Offer validity</h1>
                    <span className="paragraphMediumItalic text-neutral-500">Select whether you would like to make this offer valid for all time.</span>
                    <div className="max-w-[311px] w-full relative mt-4 md:max-w-full">
                        <DropDown labelMarginB="mb-2" dropdownLabel="Select your preference" buttonTextColor="neutral-300" menuItems={["Yes", "No"]} />
                    </div>
                </div>
            </div>
        </>
    );
};

export const OfferTiming = () => {
    const days = ["Sundays", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    return (
        <>
            <div className="max-w-[636px] md:max-w-full lg:mb-24 lg:max-w-[459px] w-full">
                <div className="mb-4">
                    <h1 className="paragraphLargeMedium mb-1">Offer timings</h1>
                    <span className="paragraphMediumItalic text-neutral-500">This is the time when the customers will be able to use this offer.</span>
                    <div className="">
                        <span className="paragraphMediumItalic text-primary-500">Each day can have only 6 time slots.</span>
                    </div>
                </div>

                <div className="relative md:max-w-full">
                    <DropDown labelMarginB="mb-2" dropdownLabel="Specific time for each day" />
                </div>
                <div className="pb-[104px]">
                    {days.map((el, index) => (
                        <div className="mt-6" key={index}>
                            <TimeSlotBanner dayLabel={el} />
                        </div>
                    ))}
                </div>
            </div>
        </>
    );
};

export const TermsAndConditions = () => {
    return (
        <>
            <div className="max-w-[636px] md:max-w-full lg:mb-24 lg:max-w-[459px] w-full">
                <div className="mb-4">
                    <h1 className="paragraphLargeMedium mb-4">Terms and condition</h1>
                    <span className="paragraphMediumItalic text-neutral-500">Enter the desired terms and condition in both languages.</span>
                </div>

                <div className="w-full mb-4">
                    <InputArea label="(English)" placeholder="Enter terms and conditions in English" boxHeight="h-[120px]" type="offer" />
                </div>

                <div className="w-full mb-4">
                    <InputArea label="(ગુજરાતી)" placeholder="Enter terms and conditions in ગુજરાતી`" boxHeight="h-[120px]" type="offer" />
                </div>
            </div>
        </>
    );
};

export const NotificationSettings = () => {
    return (
        <>
            <div className="max-w-[636px] md:max-w-full lg:mb-24 lg:max-w-[459px] w-full">
                <div className="mb-4">
                    <h1 className="paragraphLargeMedium mb-1">Notification title</h1>
                    <span className="paragraphMediumItalic text-neutral-500">Please enter the notification title that your customers will see for the offer.</span>
                </div>
                <div className="flex flex-row justify-between w-full mb-6 lg:block">
                    <div className="max-w-[311px] w-full lg:mb-4 md:max-w-full">
                        <DefaultInputField labelMarginB="mb-2" label="(English)" placeholder="Enter title in English" labelStyle="paragraphMediumItalic text-neutral-500" />
                    </div>

                    <div className="max-w-[311px] w-full md:max-w-full">
                        <DefaultInputField labelMarginB="mb-2" label="(ગુજરાતી)" placeholder="Enter title in ગુજરાતી" labelStyle="paragraphMediumItalic text-neutral-500" />
                    </div>
                </div>
                <div className="mb-4">
                    <h1 className="paragraphLargeMedium mb-1">Notification description</h1>
                    <span className="paragraphMediumItalic text-neutral-500">Please enter the notification description that your customers will see for the offer.</span>
                </div>

                <div className="flex flex-row justify-between w-full mb-6 lg:block">
                    <div className="max-w-[311px] w-full lg:mb-4 md:ax-w-full md:max-w-full">
                        <InputArea label="(English)" placeholder="Enter description in English" boxHeight="h-[120px]" type="offer" />
                    </div>

                    <div className="max-w-[311px] w-full md:ax-w-full md:max-w-full">
                        <InputArea label="(ગુજરાતી)" placeholder="Enter description in ગુજરાતી" boxHeight="h-[120px]" type="offer" />
                    </div>
                </div>
            </div>
        </>
    );
};
