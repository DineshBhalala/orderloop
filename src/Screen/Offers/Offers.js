import React, { useState } from "react";
import { DefaultBreadcrumbs } from "../../Components/Breadcrumbs/Breadcrumbs";
import { ReactComponent as OfferIcon } from "../../Assets/offers.svg";
import { ReactComponent as AddIcon } from "../../Assets/add.svg";
import { ReactComponent as SelectIcon } from "../../Assets/select.svg";
import { ReactComponent as LinkIcon } from "../../Assets/link.svg";
import { ReactComponent as ReArrangeIcon } from "../../Assets/re-arrange.svg";
import { ReactComponent as FilterIcon } from "../../Assets/filter.svg";
import { ReactComponent as EditIcon } from "../../Assets/edit.svg";
import { ReactComponent as SearchIcon } from "../../Assets/search.svg";
import { Tab } from "../../Components/Tabs/Tabs";
import { LargePrimaryButton } from "../../Components/Buttons/Button";
import DropDown from "../../Components/DropDown/DropDown";
import PaginationWithNumber from "../../Components/Pagination/PaginationWithNumber";
import ToggleSwitch from "../../Components/ToggleSwitch/ToggleSwitch";
import LinkOutletPopup from "./Components/LinkOutletPopup";
import { LinkOffer } from "../../Components/LinkOffer/LinkOffer";
import { useNavigate } from "react-router-dom";
import { DefaultInputField } from "../../Components/InputField/InputField";
import ListViewOffer from "../../Components/ListView/ListViewOffer";

export default function Offers() {
  const [isOfferListActive, setIsOfferListActive] = useState(false);
  const handleClickOfferList = () => {
    setIsOfferListActive(true);
  };
  const handleClickOutletWiseOffer = () => {
    setIsOfferListActive(false);
  };
  const outletWiseOffer = [
    {
      couponCode: "TRYNEW120",
      offerType: "Flat discount",
      title: "Big Discount - 120!",
      description: "Get ₹120 off on orders above ₹250.",
    },
    {
      couponCode: "TRYNEW50",
      offerType: "Flat discount",
      title: "Big Discount - 50!",
      description: "Get ₹50 off on orders above ₹150.",
    },
    {
      couponCode: "WELCOME20",
      offerType: "Percentage discount",
      title: "We Welcome You",
      description: "Thanks for returning! We welcome you with 20% discount on your cart!",
    },
    {
      couponCode: "BOGO2022",
      offerType: "Buy X get Y free",
      title: "Exclusive 2022 BOGO",
      description: "Buy 1 dish and get another one absolutely free!",
    },
  ];
  const offerList = [
    {
      couponCode: "TRYNEW120",
      offerType: "Flat discount",
      title: "Big Discount - 120!",
      description: "Get ₹120 off on orders above ₹250.",
      links: "4",
    },
    {
      couponCode: "TRYNEW50",
      offerType: "Flat discount",
      title: "Big Discount - 50!",
      description: "Get ₹50 off on orders above ₹150.",
      links: "4",
    },
    {
      couponCode: "WELCOME20",
      offerType: "Percentage discount",
      title: "We Welcome You",
      description: "Thanks for returning! We welcome you with 20% discount on your cart!",
      links: "4",
    },
    {
      couponCode: "BOGO2022",
      offerType: "Buy X get Y free",
      title: "Exclusive 2022 BOGO",
      description: "Buy 1 dish and get another one absolutely free!",
      links: "4",
    },
  ];
  const [showLinkOutlet, setShowLinkOutlet] = useState(false);
  const inverseLinkOutletPopupVisibility = () => {
    setShowLinkOutlet(!showLinkOutlet);
  };
  const navigate = useNavigate();
  const handleClickCreateOffer = () => {
    navigate("/create-offer");
  };
  return (
    <>
      <div className="bg-[#FAFAFA]">
        <div className={`px-8 pb-[70px] lg:px-4 lg:pb-[23px] pt-4 w-full max-w-[1336px] mx-auto bg-white ${showLinkOutlet && "md:hidden"}`}>
          <div className="mb-4 md:hidden">
            <DefaultBreadcrumbs mainTab="Offers" icon={<OfferIcon className="h-5 w-5" />} />
          </div>

          <div className="flex flex-row items-center justify-between pb-4 mb-4 border-b border-neutral-300 md:block">
            <div className="flex flex-row items-center md:mb-4">
              <div className="mr-2 md:w-1/2 lg:mr-1 cursor-pointer" onClick={handleClickOfferList}>
                <Tab label="Offers list" isActive={isOfferListActive} type="offer" />
              </div>

              <div className="ml-2 md:w-1/2 lg:ml-1 cursor-pointer" onClick={handleClickOutletWiseOffer}>
                <Tab label="Outlet-wise offers" isActive={!isOfferListActive} type="offer" />
              </div>
            </div>

            <div className="cursor-pointer" onClick={handleClickCreateOffer}>
              <LargePrimaryButton name="Create offer" leftIconDefault={<AddIcon stroke="#FFFFFF" />} leftIconClick={<AddIcon stroke="#C4BEED" />} />
            </div>
          </div>

          <div className="flex flex-row items-center justify-between mb-6 md:block md:mb-4">
            <div className="flex flex-row items-center md:mb-4">
              <div className={`min-w-[375px] lg:min-w-[298px] mr-4 relative ${isOfferListActive && "hidden"} md:min-w-0 md:w-full lg:mr-2`}>
                <DropDown dropdownLabel="Dominos Pizza - Kalawad Outlet" type="offer" />
              </div>

              <div className={`${!isOfferListActive && "hidden"} w-full min-w-[375px] mr-4 lg:min-w-[298px] lg:mr-2 md:min-w-0`}>
                <DefaultInputField placeholder="Search offer" placeholderIcon={<SearchIcon />} />
              </div>

              <div className="min-w-[156px] md:min-w-[64px]">
                <LargePrimaryButton name="Bulk select" hideName="md:hidden" leftIconDefault={<SelectIcon stroke="#FFFFFF" />} leftIconClick={<SelectIcon stroke="#C4BEED" />} />
              </div>
            </div>

            <div className={`max-w-[159px] ${isOfferListActive && "hidden"} md:max-w-full`}>
              <LargePrimaryButton name="Re-arrange" leftIconDefault={<ReArrangeIcon stroke="#ffffff" />} leftIconClick={<ReArrangeIcon stroke="#C4BEED" />} />
            </div>

            <div className={`flex flex-row items-center ${!isOfferListActive && "hidden"}`}>
              <div className="md:w-1/2 md:mr-2">
                <LargePrimaryButton name="Filters" leftIconDefault={<FilterIcon fill="#FFFFFF" />} leftIconClick={<FilterIcon fill="#C4BEED" />} />
              </div>

              <div className="min-w-[143px] w-full ml-4 lg:min-w-0 lg:w-16 lg:ml-2 md:w-1/2">
                <LargePrimaryButton name="Edit offer" hideName="lg:hidden md:block" leftIconDefault={<EditIcon stroke="#FFFFFF" />} leftIconClick={<EditIcon stroke="#C4BEED" />} />
              </div>
            </div>
          </div>

          <div className="w-full border border-neutral-300 rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden">
            <table className="w-full break-words">
              <thead>
                <tr className="bg-neutral-50 uppercase paragraphOverlineSmall text-neutral-700 h-11">
                  {isOfferListActive && (
                    <th className="text-left min-w-[100px] pl-6 shadow-innerShadow">
                      <span>STATUS</span>
                    </th>
                  )}
                  <th className={`text-left ${isOfferListActive ? "min-w-[192px]" : "min-w-[230px]"} pl-6 shadow-innerShadow`}>COUPON CODE</th>
                  <th className={`text-left ${isOfferListActive ? "min-w-[195px]" : "min-w-[226px]"} pl-6 shadow-innerShadow`}>OFFER TYPE</th>
                  <th className={`text-left ${isOfferListActive ? "min-w-[228px]" : "min-w-[262px]"} pl-6 shadow-innerShadow`}>TITLE</th>
                  <th className="text-left min-w-[383px] pl-6 shadow-innerShadow">DESCRIPTION</th>
                  <th className="text-left min-w-[169px] pl-6 shadow-innerShadow">UNLINK OFFER</th>
                </tr>
              </thead>
              <tbody>
                {(isOfferListActive ? offerList : outletWiseOffer).map((el, index) => (
                  <tr key={index} className={`even:bg-neutral-50 ${index !== 0 && "border-t"} border-neutral-300 paragraphSmallRegular justify-center h-[70px]`}>
                    {isOfferListActive && (
                      <td className="pl-6">
                        <ToggleSwitch />
                      </td>
                    )}
                    <td className="pl-6">{el.couponCode}</td>
                    <td className="pl-6">{el.offerType}</td>
                    <td className="pl-6">{el.title}</td>
                    <td className="pl-6">{el.description}</td>
                    {isOfferListActive ? (
                      <td className="pl-6 cursor-pointer" onClick={inverseLinkOutletPopupVisibility}>
                        <LinkOffer linkOfferNumber={el.links} />
                      </td>
                    ) : (
                      <td className="pl-6">
                        <div className="flex flex-row items-center">
                          <LinkIcon height={24} width={24} stroke="#6C5DD3" />
                          <span className="ml-1 paragraphSmallRegular text-primary-500">Unlink</span>
                        </div>
                      </td>
                    )}
                  </tr>
                ))}
              </tbody>
            </table>
          </div>

          <div className="hidden md:block">
            {(isOfferListActive ? offerList : outletWiseOffer).map((el, index) => (
              <div className="mt-2" key={index}>
                <ListViewOffer
                  couponCode={el.couponCode}
                  offerType={el.offerType}
                  title={el.title}
                  description={el.description}
                  linkOffer={el.links}
                  handleClickViewDetails={inverseLinkOutletPopupVisibility}
                />
              </div>
            ))}
          </div>

          <div className="mt-4 md:hidden">
            <PaginationWithNumber />
          </div>
        </div>

        <div className={`${!showLinkOutlet && "hidden"}`}>
          <LinkOutletPopup handleClickClose={inverseLinkOutletPopupVisibility} headerBottomLine="Enable the offer for the outlets" />
        </div>
      </div>
    </>
  );
}
