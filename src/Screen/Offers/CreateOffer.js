import React, { useEffect, useState } from "react";
import { DefaultBreadcrumbs } from "../../Components/Breadcrumbs/Breadcrumbs";
import { ReactComponent as OfferIcon } from "../../Assets/offers.svg";
import { ReactComponent as LeftArrowIcon } from "../../Assets/chevron-down.svg";
import { LargeDestructiveButton, LargePrimaryButton } from "../../Components/Buttons/Button";
import { MultipleTab } from "../../Components/Tabs/Tabs";
import {
    BasicDetails,
    Clubbing,
    Configure,
    NotificationSettings,
    OfferTiming,
    OfferType,
    TermsAndConditions,
    ValidityAndApplicability,
    VisibilityAndLinking,
} from "./Components/CreateOffer/CreateOfferComponents";
import { useNavigate } from "react-router-dom";
import DropDown from "../../Components/DropDown/DropDown";

export default function CreateOffer() {
    const [activeTab, setActiveTab] = useState(0);

    const handleTabClick = (index) => {
        setActiveTab(index);
    };

    const navigate = useNavigate();

    const handleClickClose = () => {
        navigate("/offers");
    };

    const menuItems = ["Offer Type", "Basic Details", "Configure", "Clubbing", "Visibility and Linking", "Validity and Applicability", "Offer Timing", "Terms and Conditions", "Notification Settings"];

    const [selectedItemFromDropDown, setSelectedItemFromDropDown] = useState();

    useEffect(() => {
        menuItems.filter((el, index) => el === selectedItemFromDropDown && setActiveTab(index));
    }, [selectedItemFromDropDown]);

    useEffect(() => {
        switch (activeTab) {
            case 0:
                setRenderPage(<OfferType />);
                break;

            case 1:
                setRenderPage(<BasicDetails />);
                break;

            case 2:
                setRenderPage(<Configure />);
                break;

            case 3:
                setRenderPage(<Clubbing />);
                break;

            case 4:
                setRenderPage(<VisibilityAndLinking />);
                break;

            case 5:
                setRenderPage(<ValidityAndApplicability />);
                break;

            case 6:
                setRenderPage(<OfferTiming />);
                break;

            case 7:
                setRenderPage(<TermsAndConditions />);
                break;

            case 8:
                setRenderPage(<NotificationSettings />);
                break;

            default:
                break;
        }
    }, [activeTab]);

    const [renderPage, setRenderPage] = useState(<OfferType />);

    return (
        <>
            <div className="bg-[#fafafa]">
                <div className="lg:pb-[23px] pt-4 w-full max-w-[1336px] mx-auto bg-white relative md:max-w-full">
                    <div className="px-8 lg:px-4">
                        <div className="mb-4 md:hidden">
                            <DefaultBreadcrumbs mainTab="Offers" icon={<OfferIcon className="h-5 w-5" />} tab1="Create offer" />
                        </div>
                        <div className="hidden md:block mb-4">
                            <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer" onClick={handleClickClose}>
                                <LeftArrowIcon className="rotate-90" />
                                <span className="paragraphMediumMedium">Back to offers</span>
                            </div>
                            <div className="relative">
                                <DropDown menuItems={menuItems} setSelectedItemFromDropDown={setSelectedItemFromDropDown} />
                            </div>
                        </div>
                        <div className="flex flex-row">
                            <div className="flex flex-col pr-6 border-r mr-6 pb-[140px] md:hidden">
                                {menuItems.map((el, index) => (
                                    <div className="mb-4 lg:mb-2">
                                        <MultipleTab label={el} key={index} index={index} onClick={handleTabClick} isActive={activeTab === index} />
                                    </div>
                                ))}
                            </div>
                            {renderPage}
                        </div>
                    </div>
                    <div className="sticky md:fixed md:justify-center md:border-none md:py-0 md:pb-1 md:pt-2 md:shadow-dropShadow left-0 right-0 bottom-0 flex px-8 lg:px-4 flex-row justify-end w-full max-w-[1336px] mx-auto bg-white py-6 border-t border-neutral-300">
                        <div className="min-w-[196px] mr-5 md:w-1/2 md:mr-[7.5px] md:min-w-0">
                            <LargeDestructiveButton name="Discard" />
                        </div>
                        <div className="min-w-[196px] md:w-1/2 md:ml-[7.5px] md:min-w-0">
                            <LargePrimaryButton name="Save" />
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
