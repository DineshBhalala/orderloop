import React, { useState } from "react";
import { ReactComponent as Close } from "../../Assets/close.svg";
import { ReactComponent as SuccessTickIcon } from "../../Assets/success-tick.svg";
import { ReactComponent as Riders } from "../../Assets/riders.svg";
import { ReactComponent as Card } from "../../Assets/card.svg";
import { ReactComponent as LeftArrow } from "../../Assets/chevron-down.svg";
import { ReactComponent as VegetableIcon } from "../../Assets/vegetable-icon.svg";
import { ReactComponent as OrderRatingIcon } from "../..//Assets//order-ratings.svg";

export default function CustomerPastOrderDetailsPopup(props) {
    const [isShoRiderFeedBack, setIsShoRiderFeedBack] = useState(false);
    const [ShowHideText, setShowHideText] = useState("Show");
    const handleClickShowHide = () => {
        setIsShoRiderFeedBack(!isShoRiderFeedBack);
        setShowHideText(isShoRiderFeedBack ? "Show" : "Hide");
    };
    return (
        <>
            <div className="fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 flex justify-center items-center overflow-auto md:bg-white md:relative">
                <div className="max-w-[652px] w-full rounded-xl md:rounded-none bg-shades-50 px-8 pt-6 pb-8 md:px-0 md:py-0 m-auto md:max-w-full">
                    <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer" onClick={() => props.handleClickCustomerPastOrderDetails()}>
                        <LeftArrow className="rotate-90" />
                        <span className="ml-1">Back to cashback</span>
                    </div>

                    <div className="flex flex-row justify-between items-center mb-2 pb-4 border-b border-neutral-300 md:items-start">
                        <div>
                            <div className="paragraphLargeMedium mobile:text-[17px]">Customer order details</div>
                            <div className="flex items-center">
                                <span className="paragraphMediumItalic text-neutral-500 mobile:text-[15px]">Order #BBQR</span>
                                <div className="flex items-center md:hidden">
                                    <span className="w-1 h-1 rounded bg-neutral-500 mx-3">&nbsp;</span>
                                    <SuccessTickIcon />
                                    <span className="paragraphSmallRegular ml-1">Delivered</span>
                                </div>
                            </div>
                        </div>
                        <div className="md:hidden cursor-pointer" onClick={() => props.handleClickCustomerPastOrderDetails()}>
                            <Close />
                        </div>
                        <div className="flex-row items-center hidden md:flex pt-0.5">
                            <SuccessTickIcon />
                            <span className="paragraphSmallRegular ml-1.5 mobile:ml-1">Delivered</span>
                        </div>
                    </div>

                    <div className="flex flex-row justify-between pb-4 border-b border-neutral-300 mb-4 md:block">
                        <div>
                            <div className="mb-1.5 pt-1.5 flex items-center">
                                <span className="paragraphMediumMedium text-neutral-500">Order bill name:</span>
                                <span className="paragraphMediumRegular ml-2 flex items-center">Amrendra Bahubali</span>
                            </div>
                            <div className="mb-1.5 pt-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Order date:</span>
                                <span className="paragraphMediumRegular ml-2">18 Nov 2022</span>
                            </div>
                            <div className="mb-1.5 pt-1.5 flex flex-row items-center">
                                <span className="paragraphMediumMedium text-neutral-500">Order type:</span>
                                <span className="paragraphSmallRegular ml-2 items-center flex flex-row">
                                    <Riders className="mr-1" />
                                    Delivery
                                </span>
                            </div>
                            <div className="mb-1.5 pt-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Order taken by:</span>
                                <span className="paragraphMediumRegular ml-2">John Doe</span>
                            </div>

                            <div className="pt-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Ordered via:</span>
                                <span className="paragraphMediumRegular ml-2">iOS device</span>
                            </div>
                        </div>
                        <div>
                            <div className="pt-1.5 mb-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Customer’s order:</span>
                                <span className="paragraphMediumRegular ml-2">713</span>
                            </div>
                            <div className="mb-1.5 pt-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Order time:</span>
                                <span className="paragraphMediumRegular ml-2">17:59 PM</span>
                            </div>
                            <div className="mb-1.5 pt-1.5 flex flex-row items-center">
                                <span className="paragraphMediumMedium text-neutral-500">Payment method:</span>
                                <span className="paragraphMediumRegular ml-2 items-center flex flex-row">
                                    <Card className="mr-1" />
                                    Debit card
                                </span>
                            </div>
                            <div className="mb-1.5 pt-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Dishes ordered:</span>
                                <span className="paragraphMediumRegular ml-2">12</span>
                            </div>
                            <div className="pt-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Platform:</span>
                                <span className="paragraphMediumRegular ml-2">Dine-in</span>
                            </div>
                        </div>
                    </div>

                    <div className="flex flex-row justify-between mb-4 paragraphMediumSemiBold pr-3.5">
                        <span>Ordered dishes</span>
                        <span>Amount</span>
                    </div>
                    <div className="border-b border-neutral-300 mb-3 pb-1">
                        <div className={`scrollbarStyle md:[&::-webkit-scrollbar]:hidden overflow-auto h-[150px] pr-2 md:pr-0 ${props.pageName === "loyaltyCashback" && "md:hidden"}`}>
                            <div className="flex flex-row justify-between paragraphMediumRegular">
                                <div className="flex flex-row items-center md:items-start w-full">
                                    <span className="md:min-w-[23px]">2 x</span>
                                    <span className="mx-2">Double Cheese Margherita Pizza</span>
                                    <span className="md:pt-1 md:ml-auto">
                                        <VegetableIcon />
                                    </span>
                                </div>
                                <span className="md:min-w-[104px] mobile:min-w-[90px] md:text-right">₹1,118.00/-</span>
                            </div>

                            <div className="flex flex-row justify-between paragraphMediumRegular mt-3">
                                <div className="flex flex-row items-center md:items-start w-full">
                                    <span className="md:min-w-[23px]">1 x</span>
                                    <span className="mx-2 md:max-w-[184.4px]">Indi Tandoori Paneer</span>
                                    <span className="md:pt-1 md:ml-auto">
                                        <VegetableIcon />
                                    </span>
                                </div>
                                <span className="md:min-w-[104px] mobile:min-w-[90px] md:text-right">₹529.00/-</span>
                            </div>

                            <div className="flex flex-row justify-between paragraphMediumRegular mt-3">
                                <div className="flex flex-row items-center md:items-start w-full">
                                    <span className="md:min-w-[23px]">1 x</span>
                                    <span className="mx-2 md:max-w-[184.4px]">Mexican Green Wave</span>
                                    <span className="md:pt-1 md:ml-auto">
                                        <VegetableIcon />
                                    </span>
                                </div>
                                <span className="md:min-w-[104px] mobile:min-w-[90px] md:text-right">₹529.00/-</span>
                            </div>

                            <div className="flex flex-row justify-between paragraphMediumRegular mt-3">
                                <div className="flex flex-row items-center md:items-start w-full">
                                    <span className="md:min-w-[23px]">2 x</span>
                                    <span className="mx-2 md:max-w-[184.4px]">Peppy Paneer</span>
                                    <span className="md:pt-1 md:ml-auto">
                                        <VegetableIcon />
                                    </span>
                                </div>
                                <span className="md:min-w-[104px] mobile:min-w-[90px] md:text-right">₹3,118.00/-</span>
                            </div>

                            <div className="flex flex-row justify-between paragraphMediumRegular mt-3">
                                <div className="flex flex-row items-center md:items-start w-full">
                                    <span className="md:min-w-[23px]">1 x</span>
                                    <span className="mx-2 md:max-w-[184.4px]">Mexican Green Wave</span>
                                    <span className="md:pt-1 md:ml-auto">
                                        <VegetableIcon />
                                    </span>
                                </div>
                                <span className="md:min-w-[104px] mobile:min-w-[90px] md:text-right">₹529.00/-</span>
                            </div>

                            <div className="flex flex-row justify-between paragraphMediumRegular mt-3">
                                <div className="flex flex-row items-center md:items-start w-full">
                                    <span className="md:min-w-[23px]">2 x</span>
                                    <span className="mx-2 md:max-w-[184.4px]">Peppy Paneer</span>
                                    <span className="md:pt-1 md:ml-auto">
                                        <VegetableIcon />
                                    </span>
                                </div>
                                <span className="md:min-w-[104px] mobile:min-w-[90px] md:text-right">₹3,118.00/-</span>
                            </div>
                        </div>
                    </div>
                    <div className="paragraphMediumRegular">
                        <div className="flex flex-row justify-between items-center mb-1.5 pt-1.5">
                            <span>Gross total</span>
                            <span>₹4,518.00/-</span>
                        </div>
                        <div className="flex flex-row justify-between items-center mb-1.5 pt-1.5">
                            <span>Other charges & taxes</span>
                            <span>₹718.00/-</span>
                        </div>
                        <div className="flex flex-row justify-between items-center mb-1.5 pt-1.5">
                            <span>Delivery charge</span>
                            <span>₹118.00/-</span>
                        </div>
                        <div className="flex flex-row justify-between items-center pt-2.5 paragraphMediumSemiBold">
                            <div className="flex items-center">
                                <span>Total bill amount</span>
                                <span className="text-tertiary-800 text-[10px] border font-medium w-[37px] py-1 text-center border-tertiary-800 rounded bg-tertiary-100 ml-2 leading-none">Paid</span>
                            </div>
                            <span>₹5,325.00/-</span>
                        </div>

                        <div className="flex flex-row justify-between items-center my-4 py-4 border-y border-neutral-300">
                            <h3 className="paragraphMediumMedium">Order feedback</h3>
                            <span className="paragraphMediumRegular text-primary-500 cursor-pointer">Show</span>
                        </div>
                        <div className="border-b border-neutral-300 pb-4 mb-4">
                            <div className="flex flex-row justify-between items-center">
                                <h3 className="paragraphMediumMedium">Rider feedback</h3>
                                <span className="paragraphMediumRegular text-primary-500 cursor-pointer" onClick={handleClickShowHide}>
                                    {ShowHideText}
                                </span>
                            </div>
                            <div className={`mt-4 ${!isShoRiderFeedBack && "hidden"}`}>
                                <div>
                                    <h3 className="paragraphMediumItalic mb-2 text-neutral-500">Rating given on</h3>
                                    <span className="font-normal text-base leading-6">13 Nov 2022</span>
                                </div>

                                <div className="mt-3">
                                    <h3 className="paragraphMediumItalic mb-2 text-neutral-500">Rider rating</h3>
                                    <div className="flex flex-row items-center">
                                        <OrderRatingIcon fill="#FEF2F2" stroke="#EF4444" />
                                        <div className="w-1 h-1 mx-3 bg-neutral-500" />
                                        <span className="font-normal text-base leading-6">1.0 star</span>
                                    </div>
                                </div>

                                <div className="mt-3">
                                    <h3 className="paragraphMediumItalic mb-2 text-neutral-500">Rider review</h3>
                                    <p className="font-normal text-base leading-6">
                                        Rider was very rude and came very late. Even arriving very late and being rude, he pointed out me for not writing address properly. Didn't like the overall
                                        delivery of the order. Disappointed!
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="flex flex-row justify-between items-center">
                            <h3 className="paragraphMediumMedium">Order history</h3>
                            <span className="paragraphMediumRegular text-primary-500 cursor-pointer">Show</span>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
