import React, { useState } from "react";
import { ReactComponent as OrderIcon } from "../../Assets/orders.svg";
import { LargePrimaryButton } from "../../Components/Buttons/Button";
import { ReactComponent as Add } from "../../Assets/add.svg";
import { ReactComponent as FilterIcon } from "../../Assets/filter.svg";
import { ReactComponent as Export } from "../../Assets/export.svg";
import { ReactComponent as SuccessTick } from "../../Assets/success-tick.svg";
import { ReactComponent as Reimbursement } from "../../Assets/reimbursement.svg";
import { ReactComponent as LiveOrder } from "../../Assets/live-order.svg";
import { ReactComponent as PastOrder } from "../../Assets/past-order.svg";
import { ReactComponent as FailedOrder } from "../../Assets/failed-order.svg";
import { TabBig, Tab } from "../../Components/Tabs/Tabs";
import PaginationWithNumber from "../../Components/Pagination/PaginationWithNumber";
import { DropDownTabs } from "../../Components/DropDown/DropDownTabs";
import { DefaultBreadcrumbs } from "../../Components/Breadcrumbs/Breadcrumbs";
import CalenderField from "../../Components/Calender/CalenderField";
import { ReactComponent as DineIn } from "../../Assets/dine-in.svg";
import { ReactComponent as Cancel } from "../../Assets/cancel.svg";
import { ReactComponent as Card } from "../../Assets/card.svg";
import { ReactComponent as UpiIcon } from "../../Assets/UPI.svg";
import { ReactComponent as Cash } from "../../Assets/cash.svg";
import { ReactComponent as Riders } from "../../Assets/riders.svg";
import { OrderStatus } from "./OrderStatus";
import { ModeAndCustomerName } from "./ModeAndCustomerName";
import OrdersCard from "./OrdersCard";
import { ReactComponent as SwiggyIcon } from "../../Assets/swiggy.svg";
import CustomerPastOrderDetailsDropDown from "./CustomerPastOrderDetailsDropDown";
import CustomerPastOrderDetailsPopup from "./CustomerPastOrderDetailsPopup";
import CustomerFailedOrderDetailsDropDown from "./CustomerFailedOrderDetailsDropDown";
import CustomerFailedOrderDetailsPopup from "./CustomerFailedOrderDetailsPopup";
import { OrdersIcon, OrdersIcons, OrdersStatusMobileIcons } from "./PastOrderIcons";
import OrdersDetailsPopup from "./OrdersDetailsPopup";

const Orders = () => {
    const liveOrdersDetail = [
        {
            modeCustomerName: <ModeAndCustomerName modeBg="bg-primary-500" modeIcon={<DineIn stroke="#ffffff" />} customerName="Sarthak Kanchan" />,
            orderAge: "10 mins ago",
            orderAmount: "₹559.00/-",
            orderStatus: <OrderStatus orderStatusStyle="text-primary-500 bg-primary-50 border-primary-500" OrderStatusLabel="Pending" />,
        },
        {
            modeCustomerName: <ModeAndCustomerName modeBg="bg-primary-500" modeIcon={<DineIn stroke="#ffffff" />} customerName="Arjun patel" />,
            orderAge: "15 mins ago",
            orderAmount: "₹759.00/-",
            orderStatus: <OrderStatus orderStatusStyle="text-primary-500 bg-primary-50 border-primary-500" OrderStatusLabel="Pending" />,
        },
        {
            modeCustomerName: <ModeAndCustomerName modeBg="bg-secondary-700" modeIcon={<DineIn stroke="#ffffff" />} customerName="Bhrugen Joshi" />,
            orderAge: "2 hrs 15 mins",
            orderAmount: "₹1,789.00/-",
            orderStatus: <OrderStatus orderStatusStyle="text-primary-500 bg-primary-50 border-primary-500" OrderStatusLabel="Pending" />,
        },
        {
            modeCustomerName: <ModeAndCustomerName modeBg="bg-secondary-700" modeIcon={<DineIn stroke="#ffffff" />} customerName="Bhargav Joshi" />,
            orderAge: "4 hrs 20 mins",
            orderAmount: "₹123.00/-",
            orderStatus: <OrderStatus orderStatusStyle="text-secondary-800 bg-secondary-50 border-secondary-800" OrderStatusLabel="Preparing" />,
        },
        {
            modeCustomerName: <ModeAndCustomerName modeBg="bg-tertiary-800" modeIcon={<DineIn stroke="#ffffff" />} customerName="Ananya Panday" />,
            orderAge: "5 hrs 15 mins",
            orderAmount: "₹5,238.00/-",
            orderStatus: <OrderStatus orderStatusStyle="text-secondary-800 bg-secondary-50 border-secondary-800" OrderStatusLabel="Preparing" />,
        },
        {
            modeCustomerName: <ModeAndCustomerName modeBg="bg-tertiary-800" modeIcon={<DineIn stroke="#ffffff" />} customerName="Kanneganti Brahmanandam" />,
            orderAge: "5 hrs 34 mins",
            orderAmount: "₹3,559.00/-",
            orderStatus: <OrderStatus orderStatusStyle="text-tertiary-800 bg-tertiary-50 border-tertiary-800" OrderStatusLabel="Prepared" />,
        },
        {
            modeCustomerName: <ModeAndCustomerName modeBg="bg-tertiary-800" modeIcon={<DineIn stroke="#ffffff" />} customerName="Amrendra Bahubali" />,
            orderAge: "6 hrs 18 mins",
            orderAmount: "₹1,559.00/-",
            orderStatus: <OrderStatus orderStatusStyle="text-primary-500 bg-primary-50 border-primary-500" OrderStatusLabel="Pending" />,
        },
        {
            modeCustomerName: <ModeAndCustomerName modeBg="bg-tertiary-800" modeIcon={<DineIn stroke="#ffffff" />} customerName="Riddhi Shah" />,
            orderAge: "1 day",
            orderAmount: "₹749.00/-",
            orderStatus: <OrderStatus orderStatusStyle="text-tertiary-800 bg-tertiary-50 border-tertiary-800" OrderStatusLabel="Prepared" />,
        },
        {
            modeCustomerName: <ModeAndCustomerName modeBg="bg-tertiary-800" modeIcon={<DineIn stroke="#ffffff" />} customerName="Shreya Khanna" />,
            orderAge: "6 hrs 18 mins",
            orderAmount: "₹1,559.00/-",
            orderStatus: <OrderStatus orderStatusStyle="text-tertiary-800 bg-tertiary-50 border-tertiary-800" OrderStatusLabel="Prepared" />,
        },
    ];

    const orderDetails = [
        {
            icon: <SwiggyIcon height={40} width={40} />,
            orderingMode: "Order number",
            orderLabel: "#BBQR",
            items: [
                {
                    displayName: "Double Cheese Margherita Pizza",
                    note: "Keep the pizza loaded with ample amount of cheese",
                    foodType: "veg",
                    quantity: 2,
                    addons: "Keep the pizza loaded with ample amount of cheese",
                    customization: [
                        {
                            displayName: "Extra cheese",
                        },
                        {
                            displayName: "Extra veggies",
                        },
                    ],
                },
                {
                    displayName: "Farmhouse Extraveganza Veggie",
                    foodType: "veg",
                    quantity: 3,
                    customization: [
                        {
                            displayName: "Extra cheese",
                        },
                        {
                            displayName: "Extra veggies",
                        },
                    ],
                },
                {
                    displayName: "Mexican Green Wave",
                    foodType: "veg",
                    quantity: 2,
                },
                {
                    displayName: "Italian Pesto Pasta",
                    foodType: "veg",
                    quantity: 2,
                },
            ],
            timeElapsed: [
                {
                    mins: 15,
                },
                {
                    seconds: 16,
                },
            ],
        },
    ];

    const pastOrdersDetails = [
        {
            customerName: "Riddhi Shah",
            orderDate: "18 Nov 2022",
            orderType: <OrdersIcon icon={<DineIn />} label="Dine-In" />,
            ordersStatusMobile: <OrdersStatusMobileIcons iconSuccess={<SuccessTick />} />,
            ordersStatus: <OrdersIcon icon={<SuccessTick />} label="Delivered" />,
            itemsOrdered: "12",
            orderAmount: "₹5,325.00/-",
            paymentMode: <OrdersIcon icon={<Cash />} label="Cash" />,
        },
        {
            customerName: "Amrendra Bahubali",
            orderDate: "13 Nov 2022",
            orderType: <OrdersIcon icon={<DineIn />} label="Dine-In" />,
            ordersStatusMobile: <OrdersStatusMobileIcons iconSuccess={<Cancel />} iconClose={<Cancel />} />,
            ordersStatus: <OrdersIcons iconSuccess={<Cancel />} labelSuccess="Refund failed" iconClose={<Cancel />} labelClose="Cancelled" />,
            itemsOrdered: "05",
            orderAmount: "₹235.00/-",
            paymentMode: <OrdersIcon icon={<Card />} label="Debit card" />,
        },
        {
            customerName: "Jay Sharma",
            orderDate: "05 Nov 2022",
            orderType: <OrdersIcon icon={<OrderIcon />} label="Takeaway" />,
            ordersStatusMobile: <OrdersStatusMobileIcons iconSuccess={<SuccessTick />} />,
            ordersStatus: <OrdersIcon icon={<SuccessTick />} label="Delivered" />,
            itemsOrdered: "03",
            orderAmount: "₹120.00/-",
            paymentMode: <OrdersIcon icon={<Cash />} label="Cash" />,
        },
        {
            customerName: "Rashi Agrawal",
            orderDate: "04 Nov 2022",
            orderType: <OrdersIcon icon={<Riders />} label="Delivery" />,
            ordersStatusMobile: <OrdersStatusMobileIcons iconSuccess={<SuccessTick />} />,
            ordersStatus: <OrdersIcon icon={<SuccessTick />} label="Delivered" />,
            itemsOrdered: " 01",
            orderAmount: "₹99.00/-",
            paymentMode: <OrdersIcon icon={<Card />} label="Credit card" />,
        },
        {
            customerName: "Shraddha Kapoor",
            orderDate: "04 Nov 2022",
            orderType: <OrdersIcon icon={<DineIn />} label="Dine-In" />,
            ordersStatusMobile: <OrdersStatusMobileIcons iconClose={<Cancel />} />,
            ordersStatus: <OrdersIcon icon={<Cancel />} label="Cancelled" />,
            itemsOrdered: "17",
            orderAmount: "₹2,559.00/-",
            paymentMode: <OrdersIcon icon={<UpiIcon />} label="UPI" />,
        },
        {
            customerName: "Shreya Khanna",
            orderDate: "01 Nov 2022",
            orderType: <OrdersIcon icon={<Riders />} label="Delivery" />,
            ordersStatusMobile: <OrdersStatusMobileIcons iconSuccess={<SuccessTick />} />,
            ordersStatus: <OrdersIcon icon={<SuccessTick />} label="Delivered" />,
            itemsOrdered: "09",
            orderAmount: "₹1,759.00/-",
            paymentMode: <OrdersIcon icon={<UpiIcon />} label="UPI" />,
        },
    ];

    const failedOrdersDetails = [
        {
            customerName: "Riddhi Shah",
            orderDate: "18 Nov 2022",
            orderType: <OrdersIcon icon={<Riders />} label="Dine-In" />,
            ordersStatusMobile: <OrdersStatusMobileIcons iconClose={<Cancel />} iconSuccess={<SuccessTick />} />,
            ordersStatus: <OrdersIcons iconClose={<Cancel />} labelClose="Failed order" iconSuccess={<SuccessTick />} labelSuccess="Refund success" />,
            itemsOrdered: "12",
            orderAmount: "₹5,325.00/-",
            paymentMode: <OrdersIcon icon={<UpiIcon />} label="UPI" />,
        },
        {
            customerName: "Amrendra Bahubali",
            orderDate: "13 Nov 2022",
            orderType: <OrdersIcon icon={<Riders />} label="Dine-In" />,
            ordersStatusMobile: <OrdersStatusMobileIcons iconClose={<Cancel />} iconSuccess={<SuccessTick />} />,
            ordersStatus: <OrdersIcons iconClose={<Cancel />} labelClose="Failed order" iconSuccess={<SuccessTick />} labelSuccess="Refund success" />,
            itemsOrdered: "05",
            orderAmount: "₹235.00/-",
            paymentMode: <OrdersIcon icon={<UpiIcon />} label="UPI" />,
        },
        {
            customerName: "Jay Sharma",
            orderDate: "05 Nov 2022",
            orderType: <OrdersIcon icon={<OrderIcon />} label="Takeaway" />,
            ordersStatusMobile: <OrdersStatusMobileIcons iconClose={<Cancel />} iconSuccess={<Reimbursement />} />,
            ordersStatus: <OrdersIcons iconClose={<Cancel />} labelClose="Failed order" iconSuccess={<Reimbursement />} labelSuccess="Refund initiated" />,
            itemsOrdered: "03",
            orderAmount: "₹120.00/-",
            paymentMode: <OrdersIcon icon={<UpiIcon />} label="UPI" />,
        },
        {
            customerName: "Rashi Agrawal",
            orderDate: "04 Nov 2022",
            orderType: <OrdersIcon icon={<Riders />} label="Delivery" />,
            ordersStatusMobile: <OrdersStatusMobileIcons iconClose={<Cancel />} iconSuccess={<SuccessTick />} />,
            ordersStatus: <OrdersIcons iconClose={<Cancel />} labelClose="Failed order" iconSuccess={<SuccessTick />} labelSuccess="Refund success" />,
            itemsOrdered: " 01",
            orderAmount: "₹99.00/-",
            paymentMode: <OrdersIcon icon={<UpiIcon />} label="UPI" />,
        },
        {
            customerName: "Shraddha Kapoor",
            orderDate: "04 Nov 2022",
            orderType: <OrdersIcon icon={<Riders />} label="Dine-In" />,
            ordersStatusMobile: <OrdersStatusMobileIcons iconClose={<Cancel />} iconSuccess={<Reimbursement />} />,
            ordersStatus: <OrdersIcons iconClose={<Cancel />} labelClose="Failed order" iconSuccess={<Reimbursement />} labelSuccess="Refund initiated" />,
            itemsOrdered: "17",
            orderAmount: "₹2,559.00/-",
            paymentMode: <OrdersIcon icon={<UpiIcon />} label="UPI" />,
        },
        {
            customerName: "Shreya Khanna",
            orderDate: "01 Nov 2022",
            orderType: <OrdersIcon icon={<Riders />} label="Delivery" />,
            ordersStatusMobile: <OrdersStatusMobileIcons iconClose={<Cancel />} iconSuccess={<SuccessTick />} />,
            ordersStatus: <OrdersIcons iconClose={<Cancel />} labelClose="Failed order" iconSuccess={<SuccessTick />} labelSuccess="Refund success" />,
            itemsOrdered: "09",
            orderAmount: "₹1,759.00/-",
            paymentMode: <OrdersIcon icon={<UpiIcon />} label="UPI" />,
        },
    ];

    const [isActiveLiveOrders, setLiveOrders] = useState(true);
    const [isActivePastOrder, setPastOrder] = useState(false);
    const [isAcitveFailedOrder, setFailedOrder] = useState(false);

    const handleClickLiveOrders = () => {
        setFailedOrder(false);
        setPastOrder(false);
        setLiveOrders(true);
    };
    const handleClickPastOrder = () => {
        setLiveOrders(false);
        setFailedOrder(false);
        setPastOrder(true);
    };
    const handleClickFailedOrder = () => {
        setPastOrder(false);
        setLiveOrders(false);
        setFailedOrder(true);
    };

    const emptyFunction = () => {};

    const [activeTab, setActiveTab] = useState("allPending");

    const tabs = [
        { label: "All", badgeText: "P - 6", value: "allPending" },
        { label: "Dine-in", badgeText: "P - 0", value: "dineIn" },
        { label: "Takeaway", badgeText: "P - 2", value: "takeaway" },
        { label: "Delivery", badgeText: "P - 4", value: "delivery" },
    ];
    const handleClickTab = (tab) => {
        setActiveTab(tab);
    };

    const [activeBigTab, setActiveBigTab] = useState("all");
    const bigTabs = [
        { label: "All", value: "all" },
        { label: "Pending", value: "pending" },
        { label: "Preparing", value: "preparing" },
        { label: "Prepared", value: "prepared" },
        { label: "Dispatched", value: "dispatched" },
    ];

    const handleClickBigTab = (bigTab) => {
        setActiveBigTab(bigTab);
    };

    const [showCustomerPastOrderDetails, setShowCustomerPastOrderDetails] = useState(false);
    const handleClickCustomerPastOrderDetails = () => {
        setShowCustomerPastOrderDetails(!showCustomerPastOrderDetails);
    };

    const [showCustomerFailedOrderDetails, setShowCustomerFailedOrderDetails] = useState(false);

    const handleClickCustomerFailedOrderDetails = () => {
        setShowCustomerFailedOrderDetails(!showCustomerFailedOrderDetails);
    };

    const [showOrdersDetails, setShowOrdersDetails] = useState(false);
    const handleClickOrdersDetails = () => {
        setShowOrdersDetails(!showOrdersDetails);
    };

    return (
        <>
            <div className="bg-[#fafafa]">
                <div className="px-8 pb-[70px] lg:px-4 lg:pb-[23px] pt-4 w-full max-w-[1336px] mx-auto bg-white">
                    <div className={`${(showCustomerPastOrderDetails || showCustomerFailedOrderDetails || showOrdersDetails) && "md:hidden"}`}>
                        <div className="mb-4 md:hidden">
                            <DefaultBreadcrumbs icon={<OrderIcon className="h-5 w-5" />} mainTab="Orders" />
                        </div>
                        <div className="flex flex-row justify-between pb-4 border-b border-b-neutral-300 mb-4">
                            <div className="flex flex-row md:hidden">
                                <div className="w-[148px] md:w-24 cursor-pointer" onClick={handleClickLiveOrders}>
                                    <TabBig label="Live orders" IconDefault={<LiveOrder />} IconClick={<LiveOrder stroke="#6C5DD3" />} isActive={isActiveLiveOrders} />
                                </div>
                                <div className="ml-4 xl:ml-2 w-[151px] md:w-24" onClick={handleClickPastOrder}>
                                    <TabBig label="Past orders" IconDefault={<PastOrder />} IconClick={<PastOrder stroke="#6C5DD3" />} isActive={isActivePastOrder} />
                                </div>
                                <div className="ml-4 xl:ml-2 w-[163px] md:w-24" onClick={handleClickFailedOrder}>
                                    <TabBig label="Failed orders" IconDefault={<FailedOrder />} IconClick={<FailedOrder stroke="#6C5DD3" />} isActive={isAcitveFailedOrder} />
                                </div>
                            </div>

                            <div className="hidden md:block w-full">
                                <DropDownTabs
                                    menuItem={[
                                        { item: "Live orders", onClick: handleClickLiveOrders },
                                        { item: "Past orders", onClick: handleClickPastOrder },
                                        { item: "Failed orders", onClick: handleClickFailedOrder },
                                    ]}
                                    // onClickMenuItem1={handleClickLiveOrders}
                                    // onClickMenuItem2={handleClickPastOrder}
                                    // onClickMenuItem3={handleClickFailedOrder}
                                    // menuItem1="Live orders"
                                    // menuItem2="Past orders"
                                    // menuItem3="Failed orders"
                                />
                            </div>
                            <div className={isActivePastOrder || isAcitveFailedOrder === true ? "hidden" : "block"}>
                                <div className="flex flex-row lg:hidden">
                                    <div className="w-[128px] xl:w-[124px]">
                                        <LargePrimaryButton isDefault={false} leftIconDefault={<Add stroke="#ffffff" />} leftIconClick={<Add stroke="#C4BEED" />} fontsWeight={true} name="Dine-in" />
                                    </div>
                                    <div className="w-[149px] xl:w-[140px] mx-4 xl:mx-1">
                                        <LargePrimaryButton isDefault={false} leftIconDefault={<Add stroke="#ffffff" />} leftIconClick={<Add stroke="#C4BEED" />} fontsWeight={true} name="Takeaway" />
                                    </div>
                                    <div className="w-[135px] xl:w-[130px]">
                                        <LargePrimaryButton isDefault={false} leftIconDefault={<Add stroke="#ffffff" />} leftIconClick={<Add stroke="#C4BEED" />} fontsWeight={true} name="Delivery" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className={isActiveLiveOrders === true ? "hidden" : "block"}>
                            <div className="flex md:block justify-between mb-6 md:mb-0">
                                <div className="md:mb-4">
                                    <CalenderField label="25 Sept 2022 - 09 Oct 2022" buttonStyle="md:w-full" />
                                </div>
                                <div className="flex justify-between">
                                    <div className="w-[120px] mx-4 lg:mx-2 md:ml-0 md:w-full mobile:mr-1">
                                        <LargePrimaryButton leftIconDefault={<FilterIcon fill="#ffffff" />} leftIconClick={<FilterIcon fill="#C4BEED" />} name="Filters" />
                                    </div>
                                    <div className="w-[161px] md:ml-2 md:w-full mobile:ml-1">
                                        <LargePrimaryButton leftIconDefault={<Export stroke="#ffffff" />} leftIconClick={<Export stroke="#C4BEED" />} name="Export data" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className={isActiveLiveOrders === true ? "block" : "hidden"}>
                            <div className="flex justify-between mb-6 xl:block md:hidden">
                                <div className="flex justify-between xl:justify-start xl:mb-2">
                                    {tabs.map((tab) => (
                                        <div key={tab.value} onClick={() => handleClickTab(tab.value)} className="mr-4 2xl:mr-2 cursor-pointer">
                                            <Tab label={tab.label} badgeText={tab.badgeText} isActive={activeTab === tab.value} />
                                        </div>
                                    ))}
                                </div>
                                <div className="flex justify-between xl:justify-end">
                                    {bigTabs.map((bigTab) => (
                                        <div key={bigTab.value} onClick={() => handleClickBigTab(bigTab.value)} className="ml-4 2xl:ml-2 cursor-pointer">
                                            <TabBig label={bigTab.label} isActive={activeBigTab === bigTab.value} />
                                        </div>
                                    ))}
                                </div>
                            </div>
                            <div className="hidden md:block">
                                <div className="flex mobile:block justify-between mb-4">
                                    <div className="min-w-[205px] w-full pr-[15px] mobile:min-w-full mobile:pr-0 mobile:pb-4">
                                        <DropDownTabs
                                            tabStyle="paragraphSmallMedium text-primary-500"
                                            menuItems={[
                                                { item: "All", badgeText: "P - 6" },
                                                { item: "Dine-in", badgeText: "P - 0" },
                                                { item: "Takeaway", badgeText: "P - 2" },
                                                { item: "Delivery", badgeText: "P - 4" },
                                            ]}
                                            // menuItem1="All"
                                            // badgeText1="P - 6"
                                            // menuItem2="Dine-in"
                                            // badgeText2="P - 0"
                                            // menuItem3="Takeaway"
                                            // badgeText3="P - 2"
                                            // menuItem4="Delivery"
                                            // badgeText4="P - 4"
                                        />
                                    </div>
                                    <div className="min-w-[138px] w-full mobile:min-w-full">
                                        {/* <DropDownTabs
                      tabStyle="paragraphSmallMedium text-primary-500"
                      menuItem1="All"
                      onClickMenuItem1={emptyFunction}
                      menuItem2="Pending"
                      onClickMenuItem2={emptyFunction}
                      menuItem3="Preparing"
                      onClickMenuItem3={emptyFunction}
                      menuItem4="Prepared"
                      onClickMenuItem4={emptyFunction}
                      menuItem5="Dispatched"
                      onClickMenuItem5={emptyFunction}
                    /> */}
                                        <DropDownTabs
                                            tabStyle="paragraphSmallMedium text-primary-500"
                                            menuItems={[{ item: "All" }, { item: "Pending" }, { item: "Preparing" }, { item: "Prepared" }, { item: "Dispatched" }]}
                                            // menuItem1="All"
                                            // onClickMenuItem1={emptyFunction}
                                            // menuItem2="Pending"
                                            // onClickMenuItem2={emptyFunction}
                                            // menuItem3="Preparing"
                                            // onClickMenuItem3={emptyFunction}
                                            // menuItem4="Prepared"
                                            // onClickMenuItem4={emptyFunction}
                                            // menuItem5="Dispatched"
                                            // onClickMenuItem5={emptyFunction}
                                        />
                                    </div>
                                </div>
                                <div>
                                    {liveOrdersDetail.map((row, index) => (
                                        <div key={index} className="w-full px-4 mobile:px-3 py-[14px] mobile:py-2.5 mb-2 border border-neutral-300 rounded-md" onClick={handleClickOrdersDetails}>
                                            <div className="flex flex-row items-center justify-between">
                                                <div className="paragraphSmallRegular">{row.modeCustomerName}</div>
                                                <div>{row.orderStatus}</div>
                                            </div>
                                        </div>
                                    ))}
                                </div>
                            </div>
                            <div className="flex flex-row justify-between">
                                <div className="w-full pr-5 xl:pr-0">
                                    <div className="w-full border border-neutral-300 rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden">
                                        <table className="w-full break-words">
                                            <thead>
                                                <tr className="bg-neutral-50 uppercase paragraphOverlineSmall text-neutral-700">
                                                    <th className="text-left h-11 pl-6 shadow-innerShadow min-w-[273px] 2xl:min-w-[220px]">Mode &amp; customer Name</th>
                                                    <th className="text-left h-11 pl-6 shadow-innerShadow min-w-[144px] 2xl:min-w-[130px]">order age</th>
                                                    <th className="text-left h-11 pl-6 shadow-innerShadow min-w-[163px] 2xl:min-w-[155px]">order amount</th>
                                                    <th className="text-left h-11 pl-6 shadow-innerShadow min-w-[153px] 2xl:min-w-[145px]">order status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {liveOrdersDetail.map((row, index) => (
                                                    <tr
                                                        key={index}
                                                        className={`even:bg-neutral-50 cursor-pointer ${
                                                            index !== 0 && "border-t"
                                                        } border-neutral-300 paragraphSmallRegular cursor-default hover:bg-primary-100 hover:border-primary-500 hover:border-y hover:last:border-b-0`}
                                                        onClick={handleClickOrdersDetails}
                                                    >
                                                        <td className="h-[70px] pl-6">{row.modeCustomerName}</td>
                                                        <td className="h-[70px] pl-6">{row.orderAge}</td>
                                                        <td className="h-[70px] pl-6">{row.orderAmount}</td>
                                                        <td className="h-[70px] pl-6">{row.orderStatus}</td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                    <div className="md:hidden my-4">
                                        <PaginationWithNumber />
                                    </div>
                                </div>
                                <div className="min-w-[518px] w-full 2xl:min-w-min 2xl:max-w-[450px] xl:hidden">
                                    {orderDetails.map((el, index) => (
                                        <div key={index}>
                                            <OrdersCard
                                                ordersIcon={el.icon}
                                                isUpdated={el.isUpdated}
                                                orderLabel={el.orderLabel}
                                                orderingMode={el.orderingMode}
                                                timeElapsed={el.timeElapsed}
                                                items={el.items}
                                            />
                                        </div>
                                    ))}
                                </div>
                            </div>
                        </div>
                        <div className={isActivePastOrder === true ? "block" : "hidden"}>
                            <div className="my-4">
                                <div className="w-full border border-neutral-300 rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden">
                                    <table className="w-full break-words">
                                        <thead>
                                            <tr className="bg-neutral-50 uppercase paragraphOverlineSmall text-neutral-700">
                                                <th className="text-left h-11 pl-6 min-w-[234px] lg:min-w-[195px] shadow-innerShadow">customer name</th>
                                                <th className="text-left h-11 pl-6 min-w-[174px] shadow-innerShadow">order date</th>
                                                <th className="text-left h-11 pl-6 min-w-[166px] shadow-innerShadow">Order type</th>
                                                <th className="text-left h-11 pl-6 min-w-[167px] shadow-innerShadow">order status</th>
                                                <th className="text-left h-11 pl-6 min-w-[163px] shadow-innerShadow">items ordered</th>
                                                <th className="text-left h-11 pl-6 min-w-[192px] shadow-innerShadow">order amount</th>
                                                <th className="text-left h-11 pl-6 min-w-[176px] shadow-innerShadow">payment mode</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {pastOrdersDetails.map((row, index) => (
                                                <tr key={index} className={`even:bg-neutral-50 ${index !== 0 && "border-t"} border-neutral-300 paragraphSmallRegular`}>
                                                    <td className="py-2 pl-6 h-[70px] cursor-pointer" onClick={handleClickCustomerPastOrderDetails}>
                                                        {row.customerName}
                                                    </td>
                                                    <td className="py-2 pl-6">{row.orderDate}</td>
                                                    <td className="py-2 pl-6">{row.orderType}</td>
                                                    <td className="py-2 pl-6">{row.ordersStatus}</td>
                                                    <td className="py-2 pl-6">{row.itemsOrdered} </td>
                                                    <td className="py-2 pl-6">{row.orderAmount} </td>
                                                    <td className="py-2 pl-6">{row.paymentMode} </td>
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                </div>
                                <div className="hidden md:block mt-3">
                                    {pastOrdersDetails.map((el, index) => {
                                        return (
                                            <div className="mb-1 pt-1" key={index}>
                                                <CustomerPastOrderDetailsDropDown
                                                    customerName={el.customerName}
                                                    ordersStatusMobile={el.ordersStatusMobile}
                                                    orderDate={el.orderDate}
                                                    orderType={el.orderType}
                                                    ordersStatus={el.ordersStatus}
                                                    itemsOrdered={el.itemsOrdered}
                                                    orderAmount={el.orderAmount}
                                                    paymentMode={el.paymentMode}
                                                    handleClickCustomerPastOrderDetails={handleClickCustomerPastOrderDetails}
                                                />
                                            </div>
                                        );
                                    })}
                                </div>
                            </div>
                            <div className="md:hidden">
                                <PaginationWithNumber />
                            </div>
                        </div>

                        <div className={isAcitveFailedOrder === true ? "block" : "hidden"}>
                            <div className="my-4">
                                <div className="w-full border border-neutral-300 rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden">
                                    <table className="w-full break-words">
                                        <thead>
                                            <tr className="bg-neutral-50 uppercase paragraphOverlineSmall text-neutral-700">
                                                <th className="text-left h-11 pl-6 min-w-[234px] lg:min-w-[195px] shadow-innerShadow">customer name</th>
                                                <th className="text-left h-11 pl-6 min-w-[174px] shadow-innerShadow">order date</th>
                                                <th className="text-left h-11 pl-6 min-w-[166px] shadow-innerShadow">Order type</th>
                                                <th className="text-left h-11 pl-6 min-w-[167px] shadow-innerShadow">order status</th>
                                                <th className="text-left h-11 pl-6 min-w-[163px] shadow-innerShadow">items ordered</th>
                                                <th className="text-left h-11 pl-6 min-w-[192px] shadow-innerShadow">order amount</th>
                                                <th className="text-left h-11 pl-6 min-w-[176px] shadow-innerShadow">payment mode</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {failedOrdersDetails.map((row, index) => (
                                                <tr key={index} className={`even:bg-neutral-50 ${index !== 0 && "border-t"} border-neutral-300 paragraphSmallRegular`}>
                                                    <td className="py-2 pl-6 h-[70px] cursor-pointer" onClick={handleClickCustomerFailedOrderDetails}>
                                                        {row.customerName}
                                                    </td>
                                                    <td className="py-2 pl-6">{row.orderDate}</td>
                                                    <td className="py-2 pl-6">{row.orderType}</td>
                                                    <td className="py-2 pl-6">{row.ordersStatus}</td>
                                                    <td className="py-2 pl-6">{row.itemsOrdered} </td>
                                                    <td className="py-2 pl-6">{row.orderAmount} </td>
                                                    <td className="py-2 pl-6">{row.paymentMode} </td>
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                </div>
                                <div className="hidden md:block mt-3">
                                    {failedOrdersDetails.map((el, index) => {
                                        return (
                                            <div className="mb-1 pt-1" key={index}>
                                                <CustomerFailedOrderDetailsDropDown
                                                    customerName={el.customerName}
                                                    ordersStatusMobile={el.ordersStatusMobile}
                                                    orderDate={el.orderDate}
                                                    orderType={el.orderType}
                                                    ordersStatus={el.ordersStatus}
                                                    itemsOrdered={el.itemsOrdered}
                                                    orderAmount={el.orderAmount}
                                                    paymentMode={el.paymentMode}
                                                    handleClickCustomerFailedOrderDetails={handleClickCustomerFailedOrderDetails}
                                                />
                                            </div>
                                        );
                                    })}
                                </div>
                            </div>
                            <div className="md:hidden">
                                <PaginationWithNumber />
                            </div>
                        </div>
                    </div>

                    <div className={`${!showCustomerPastOrderDetails && "hidden"}`}>
                        <CustomerPastOrderDetailsPopup handleClickCustomerPastOrderDetails={handleClickCustomerPastOrderDetails} />
                    </div>
                    <div className={`${!showCustomerFailedOrderDetails && "hidden"}`}>
                        <CustomerFailedOrderDetailsPopup handleClickCustomerFailedOrderDetails={handleClickCustomerFailedOrderDetails} />
                    </div>
                    <div className="hidden xl:block">
                        <div className={`${!showOrdersDetails && "hidden"}`}>
                            <OrdersDetailsPopup handleClickOrdersDetails={handleClickOrdersDetails} />
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Orders;
