import React from "react";
import { ReactComponent as LeftArrow } from "../../Assets/chevron-down.svg";
import { ReactComponent as SwiggyIcon } from "../../Assets/swiggy.svg";
import OrdersCard from "./OrdersCard";

export default function OrdersDetailsPopup(props) {
    const orderDetails = [
        {
            icon: <SwiggyIcon height={40} width={40} />,
            orderingMode: "Order number",
            orderLabel: "#BBQR",
            items: [
                {
                    displayName: "Double Cheese Margherita Pizza",
                    note: "Keep the pizza loaded with ample amount of cheese",
                    foodType: "veg",
                    quantity: 2,
                    addons: "Keep the pizza loaded with ample amount of cheese",
                    customization: [
                        {
                            displayName: "Extra cheese",
                        },
                        {
                            displayName: "Extra veggies",
                        },
                    ],
                },
                {
                    displayName: "Farmhouse Extraveganza Veggie",
                    foodType: "veg",
                    quantity: 3,
                    customization: [
                        {
                            displayName: "Extra cheese",
                        },
                        {
                            displayName: "Extra veggies",
                        },
                    ],
                },
                {
                    displayName: "Mexican Green Wave",
                    foodType: "veg",
                    quantity: 2,
                },
                {
                    displayName: "Italian Pesto Pasta",
                    foodType: "veg",
                    quantity: 2,
                },
            ],
            timeElapsed: [
                {
                    mins: 15,
                },
                {
                    seconds: 16,
                },
            ],
        },
    ];
    return (
        <>
            <div
                className="fixed bg-black bg-opacity-50 inset-0 md:z-[8] z-[49] flex justify-center items-center overflow-auto md:bg-white md:relative md:p-4"
                onClick={() => props.handleClickOrdersDetails()}
            >
                <div className="max-w-[518px] w-full rounded-xl md:rounded-none bg-shades-50 md:px-0 md:py-0 m-auto md:max-w-full md:z-[9] z-50">
                    <div className="hidden md:flex paragraphMediumMedium flex-row ml-1 cursor-pointer" onClick={() => props.handleClickOrdersDetails()}>
                        <LeftArrow className="rotate-90" />
                        <span className="ml-1">Back to live orders</span>
                    </div>
                    {orderDetails.map((el, index) => (
                        <div key={index}>
                            <OrdersCard ordersIcon={el.icon} isUpdated={el.isUpdated} orderLabel={el.orderLabel} orderingMode={el.orderingMode} timeElapsed={el.timeElapsed} items={el.items} />
                        </div>
                    ))}
                </div>
            </div>
        </>
    );
}
