import React from 'react'

export const OrdersIcon = (props) => {
    return (
        <>
            <div className='flex items-center'>
                <span>{props.icon}</span>
                <span className='pl-1'>{props.label}</span>
            </div>
        </>
    )
}

export const OrdersIcons = (props) => {
    return (
        <>
            <div className='flex items-center pb-0.5'>
                <span>{props.iconClose}</span>
                <span className='pl-1'>{props.labelClose}</span>
            </div>
            <div className='flex items-center pt-[3px]'>
                <span>{props.iconSuccess}</span>
                <span className='pl-1'>{props.labelSuccess}</span>
            </div>
        </>
    )
}

export const OrdersStatusMobileIcons = (props) => {
    return (
        <>
            <div className='flex items-center'>
                <span className='ml-1'>{props.iconSuccess}</span>
                <span className='ml-1'>{props.iconClose}</span>
            </div>
        </>
    )
}
