import React from 'react'

export const ModeAndCustomerName = (props) => {
    return (
        <>
            <div className='flex items-center'>
                <div className={`flex min-w-[40px] h-10 rounded-lg ${props.modeBg}`}><span className='m-auto'>{props.modeIcon}</span></div>
                <div className='pl-2'><span className='hidden md:block uppercase paragraphOverlineSmall mobile:text-[11px] mobile:leading-none'>mode &amp; customer name:</span><span>{props.customerName}</span></div>
            </div>
        </>
    )
}
