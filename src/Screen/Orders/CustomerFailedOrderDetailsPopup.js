import React from "react";
import { ReactComponent as Close } from "../../Assets/close.svg";
import { ReactComponent as Reimbursement } from "../../Assets/reimbursement.svg";
import { ReactComponent as OrderIcon } from "../../Assets/orders.svg";
import { ReactComponent as UpiIcon } from "../../Assets/UPI.svg";
import { ReactComponent as LeftArrow } from "../../Assets/chevron-down.svg";
import { ReactComponent as VegetableIcon } from "../../Assets/vegetable-icon.svg";

export default function CustomerFailedOrderDetailsPopup(props) {
    return (
        <>
            <div className="fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 flex justify-center items-center overflow-auto md:bg-white md:relative">
                <div className="max-w-[652px] w-full rounded-xl md:rounded-none bg-shades-50 px-8 py-6 md:px-0 md:py-0 m-auto md:max-w-full">
                    <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer" onClick={() => props.handleClickCustomerFailedOrderDetails()}>
                        <LeftArrow className="rotate-90" />
                        <span className="ml-1">Back to cashback</span>
                    </div>

                    <div className="flex flex-row justify-between items-center mb-2 pb-4 border-b border-neutral-300 md:items-start">
                        <div>
                            <div className="paragraphLargeMedium mobile:text-[15px]">Customer order details</div>
                            <div className="flex items-center">
                                <span className="paragraphMediumItalic text-neutral-500 mobile:text-sm">Order #BBQR</span>
                                <div className="flex items-center md:hidden">
                                    <span className="w-1 h-1 rounded bg-neutral-500 mx-3">&nbsp;</span>
                                    <Reimbursement />
                                    <span className="paragraphSmallRegular ml-1">Refund initiated</span>
                                </div>
                            </div>
                        </div>
                        <div className="md:hidden cursor-pointer" onClick={() => props.handleClickCustomerFailedOrderDetails()}>
                            <Close />
                        </div>
                        <div className="flex-row items-center hidden md:flex pt-0.5">
                            <Reimbursement />
                            <span className="paragraphSmallRegular ml-1.5 mobile:ml-1 mobile:text-xs">Refund initiated</span>
                        </div>
                    </div>

                    <div className="flex flex-row justify-between pb-4 border-b border-neutral-300 mb-4 md:block">
                        <div>
                            <div className="mb-1.5 pt-1.5 flex items-center">
                                <span className="paragraphMediumMedium text-neutral-500">Order bill name:</span>
                                <span className="paragraphMediumRegular ml-2 flex items-center">Amrendra Bahubali</span>
                            </div>
                            <div className="mb-1.5 pt-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Order date:</span>
                                <span className="paragraphMediumRegular ml-2">18 Nov 2022</span>
                            </div>
                            <div className="mb-1.5 pt-1.5 flex flex-row items-center">
                                <span className="paragraphMediumMedium text-neutral-500">Order type:</span>
                                <span className="paragraphSmallRegular ml-2 items-center flex flex-row">
                                    <OrderIcon className="mr-1" />
                                    Takeaway
                                </span>
                            </div>
                            <div className="mb-1.5 pt-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Order taken by:</span>
                                <span className="paragraphMediumRegular ml-2">John Doe</span>
                            </div>

                            <div className="pt-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Ordered via:</span>
                                <span className="paragraphMediumRegular ml-2">iOS device</span>
                            </div>
                        </div>
                        <div>
                            <div className="pt-1.5 mb-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Customer’s order:</span>
                                <span className="paragraphMediumRegular ml-2">713</span>
                            </div>
                            <div className="mb-1.5 pt-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Order time:</span>
                                <span className="paragraphMediumRegular ml-2">17:59 PM</span>
                            </div>
                            <div className="mb-1.5 pt-1.5 flex flex-row items-center">
                                <span className="paragraphMediumMedium text-neutral-500">Payment method:</span>
                                <span className="paragraphMediumRegular ml-2 items-center flex flex-row">
                                    <UpiIcon className="mr-1" />
                                    UPI
                                </span>
                            </div>
                            <div className="mb-1.5 pt-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Dishes ordered:</span>
                                <span className="paragraphMediumRegular ml-2">06</span>
                            </div>
                            <div className="pt-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Platform:</span>
                                <span className="paragraphMediumRegular ml-2">Swiggy</span>
                            </div>
                        </div>
                    </div>

                    <div className="flex flex-row justify-between mb-4 paragraphMediumSemiBold">
                        <span>Ordered dishes</span>
                        <span>Amount</span>
                    </div>
                    <div className="border-b border-neutral-300 mb-4 pb-1">
                        <div className={`scrollbarStyle md:[&::-webkit-scrollbar]:hidden overflow-auto h-[150px] pr-3 md:pr-0 ${props.pageName === "loyaltyCashback" && "md:hidden"}`}>
                            <div className="flex flex-row justify-between paragraphMediumRegular">
                                <div className="flex flex-row items-center md:items-start w-full">
                                    <span className="md:min-w-[23px]">2 x</span>
                                    <span className="mx-2">Double Cheese Margherita Pizza</span>
                                    <span className="md:pt-1 md:ml-auto">
                                        <VegetableIcon />
                                    </span>
                                </div>
                                <span className="md:min-w-[104px] mobile:min-w-[90px] md:text-right">₹1,118.00/-</span>
                            </div>

                            <div className="flex flex-row justify-between paragraphMediumRegular mt-3">
                                <div className="flex flex-row items-center md:items-start w-full">
                                    <span className="md:min-w-[23px]">1 x</span>
                                    <span className="mx-2 md:max-w-[184.4px]">Indi Tandoori Paneer</span>
                                    <span className="md:pt-1 md:ml-auto">
                                        <VegetableIcon />
                                    </span>
                                </div>
                                <span className="md:min-w-[104px] mobile:min-w-[90px] md:text-right">₹529.00/-</span>
                            </div>

                            <div className="flex flex-row justify-between paragraphMediumRegular mt-3">
                                <div className="flex flex-row items-center md:items-start w-full">
                                    <span className="md:min-w-[23px]">1 x</span>
                                    <span className="mx-2 md:max-w-[184.4px]">Mexican Green Wave</span>
                                    <span className="md:pt-1 md:ml-auto">
                                        <VegetableIcon />
                                    </span>
                                </div>
                                <span className="md:min-w-[104px] mobile:min-w-[90px] md:text-right">₹529.00/-</span>
                            </div>

                            <div className="flex flex-row justify-between paragraphMediumRegular mt-3">
                                <div className="flex flex-row items-center md:items-start w-full">
                                    <span className="md:min-w-[23px]">2 x</span>
                                    <span className="mx-2 md:max-w-[184.4px]">Peppy Paneer</span>
                                    <span className="md:pt-1 md:ml-auto">
                                        <VegetableIcon />
                                    </span>
                                </div>
                                <span className="md:min-w-[104px] mobile:min-w-[90px] md:text-right">₹3,118.00/-</span>
                            </div>

                            <div className="flex flex-row justify-between paragraphMediumRegular mt-3">
                                <div className="flex flex-row items-center md:items-start w-full">
                                    <span className="md:min-w-[23px]">1 x</span>
                                    <span className="mx-2 md:max-w-[184.4px]">Mexican Green Wave</span>
                                    <span className="md:pt-1 md:ml-auto">
                                        <VegetableIcon />
                                    </span>
                                </div>
                                <span className="md:min-w-[104px] mobile:min-w-[90px] md:text-right">₹529.00/-</span>
                            </div>

                            <div className="flex flex-row justify-between paragraphMediumRegular mt-3">
                                <div className="flex flex-row items-center md:items-start w-full">
                                    <span className="md:min-w-[23px]">2 x</span>
                                    <span className="mx-2 md:max-w-[184.4px]">Peppy Paneer</span>
                                    <span className="md:pt-1 md:ml-auto">
                                        <VegetableIcon />
                                    </span>
                                </div>
                                <span className="md:min-w-[104px] mobile:min-w-[90px] md:text-right">₹3,118.00/-</span>
                            </div>
                        </div>
                    </div>
                    <div className="paragraphMediumRegular">
                        <div className="flex flex-row justify-between items-center mb-1.5">
                            <span>Gross total</span>
                            <span>₹3,218.00/-</span>
                        </div>
                        <div className="flex flex-row justify-between items-center mb-1.5 pt-1.5">
                            <span>Other charges & taxes</span>
                            <span>₹323.00/-</span>
                        </div>
                        <div className="flex flex-row justify-between items-center mb-1.5 pt-1.5">
                            <span>Delivery charge</span>
                            <span>₹120.00/-</span>
                        </div>
                        <div className="flex flex-row justify-between items-center pt-2.5 paragraphMediumSemiBold">
                            <div className="flex items-center">
                                <span>Total bill amount</span>
                                <span className="text-destructive-600 text-[10px] border font-medium w-[50px] py-1 text-center border-destructive-600 rounded bg-destructive-100 ml-2 leading-none">
                                    Unpaid
                                </span>
                            </div>
                            <span>₹3,756.00/-</span>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
