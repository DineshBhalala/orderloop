import React, { useState } from "react";
import { ReactComponent as VegIcon } from "../../Assets/vegetable-icon.svg";
import { ReactComponent as NonVegIcon } from "../../Assets/non-veg.svg";
import { ReactComponent as DropDownIcon } from "../../Assets/chevron-down.svg";
import { LargeDestructiveButton, LargePrimaryButton, LargeTertiaryButton } from "../../Components/Buttons/Button";

const OrdersCard = (props) => {
    const [showWithItems, setShowWithItems] = useState(false);

    const handleClickWithDropDown = () => {
        setShowWithItems(!showWithItems);
    };
    return (
        <>
            <div className="w-full border border-neutral-300 rounded-lg pt-4 bg-white md:border-none">
                <div className="px-4 md:px-0">
                    <div className="flex flex-row justify-between items-center mb-4 pb-4 border-b border-neutral-300">
                        <div className="flex flex-row items-center">
                            <div>{props.ordersIcon}</div>
                            <div className="ml-[6px]">
                                <div className="paragraphMediumItalic text-neutral-500">{props.orderingMode}</div>
                                <div className="paragraphLargeMedium">Order {props.orderLabel}</div>
                            </div>
                        </div>
                        <div>
                            <div className="paragraphMediumItalic text-neutral-500">Order details</div>
                            <div className="leading-7">
                                <span className="text-lg font-normal text-primary-500 border-b border-primary-500 leading-none cursor-pointer">View details</span>
                            </div>
                        </div>
                    </div>

                    <div className={`mb-4 pb-4 border-b border-neutral-300 ${!props.isUpdated && "hidden"} text-center`}>
                        <span className="paragraphOverlineLarge text-tertiary-800">ORDERED DISHES UPDATED!</span>
                    </div>

                    <div className="flex flex-row justify-between mb-4">
                        <span className="text-base leading-4 border-b border-neutral-900">Ordered dishes</span>
                        <span className="text-base leading-4 border-b border-neutral-900">Amount</span>
                    </div>
                </div>
                <div className="pl-4 pr-2.5 md:px-0">
                    <div className="mb-4 md:mb-0 scrollbarStyle md:[&::-webkit-scrollbar]:hidden overflow-auto max-h-[325px] pr-2">
                        <div>
                            {props.items.map((el, index) => (
                                <div className="mb-3" key={index}>
                                    <div className="flex flex-row justify-between mb-1">
                                        <div className="flex flex-row">
                                            <span className={`flex ${el.isPrepared ? "paragraphMediumStrikethrough" : "paragraphMediumSemiBold"} ${el.updatedQuantity && "text-tertiary-800"}`}>
                                                {el.displayName}
                                            </span>
                                            <div className="min-w-[15.6px] min-h-[15.6px] pt-1 ml-2">{el.foodType === "veg" ? <VegIcon /> : <NonVegIcon />}</div>
                                        </div>
                                        <div className="min-w-[70px] text-right">
                                            <span
                                                className={`${el.isDeleted ? "paragraphMediumStrikethrough" : "paragraphMediumSemiBold"} ${
                                                    el.updatedQuantity && "paragraphXSmallStrikethrough text-tertiary-800"
                                                }`}
                                            >
                                                {("0" + el.quantity).slice(-2)}
                                            </span>
                                            {el.updatedQuantity && <span className="paragraphMediumSemiBold text-tertiary-800 ml-1">{("0" + el.updatedQuantity).slice(-2)}</span>}
                                        </div>
                                    </div>
                                    <div className="pl-[21px] pr-11 ml-2 border-l border-neutral-300">
                                        {el.customization && (
                                            <div className="">
                                                <div className="flex flex-row items-center">
                                                    <span className={`${el.isPrepared ? "paragraphSmallStrikethrough mr-1" : "paragraphSmallMedium"} titleSap relative`}>With</span>
                                                    <div className={`${!el.isPrepared && "hidden"} cursor-pointer`} onClick={handleClickWithDropDown}>
                                                        <DropDownIcon height={20} className={`${showWithItems && "rotate-180"}`} />
                                                    </div>
                                                </div>
                                                <div className={`flex flex-col paragraphSmallItalic text-neutral-500 mb-1 ${!showWithItems && el.isPrepared && "hidden"}`}>
                                                    {el.customization.map((el, index) => (
                                                        <div className="mt-1" key={index}>
                                                            {el.displayName}
                                                        </div>
                                                    ))}
                                                </div>
                                            </div>
                                        )}
                                        {el.addons && (
                                            <div className="w-full">
                                                <span className="paragraphSmallMedium titleSap relative">Note</span>
                                                <p className="mt-1 flex flex-col paragraphSmallItalic text-neutral-500 pr-48 2xl:pr-28 xl:pr-5">{el.addons}</p>
                                            </div>
                                        )}
                                    </div>
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
                <div className="px-4 md:px-0">
                    <div className="paragraphMediumRegular border-t pt-4 border-neutral-300">
                        <div className="flex flex-row justify-between items-center mb-3">
                            <span>Gross total</span>
                            <span>₹4,518.00/-</span>
                        </div>
                        <div className="flex flex-row justify-between items-center mb-3">
                            <span>Other charges & taxes</span>
                            <span>₹718.00/-</span>
                        </div>
                        <div className="flex flex-row justify-between items-center mb-3">
                            <span>Delivery charge</span>
                            <span>₹118.00/-</span>
                        </div>
                        <div className="flex flex-row justify-between paragraphMediumSemiBold pt-1 pb-4 border-b border-neutral-300 md:pb-32">
                            <div className="flex items-center">
                                <span>Total bill amount</span>
                                <span className="text-success-600 text-[10px] border font-medium w-[37px] py-1 text-center border-success-600 rounded bg-success-50 ml-2 leading-none">Paid</span>
                            </div>
                            <span>
                                ₹5,325.00/-
                                {/* {totalQuantity}    */}
                            </span>
                        </div>
                    </div>
                </div>
                <div className="p-4 md:fixed md:bottom-0 md:left-0 md:right-0 md:bg-white md:shadow-dropShadow">
                    <div className="flex flex-row justify-between">
                        <div className="w-1/2 pr-[7px]">
                            <LargeDestructiveButton isDefault={false} fontsWeight={true} name="Cancel order" />
                        </div>
                        <div className="w-1/2 pl-[7px]">
                            <LargePrimaryButton isDefault={false} fontsWeight={true} name="Accept order" />
                        </div>
                    </div>
                    <div className="w-full mt-2">
                        <LargeTertiaryButton name="Rider assignment" />
                    </div>
                </div>
            </div>
        </>
    );
};

export default OrdersCard;
