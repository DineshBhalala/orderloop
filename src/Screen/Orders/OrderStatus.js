import React from 'react'

export const OrderStatus = (props) => {
    return (
        <>
            <div className='flex items-center'>
                <span className={`border rounded paragraphSmallMedium px-2 py-1 cursor-pointer mobile:text-[10px] mobile:py-0.5 ${props.orderStatusStyle}`}>{props.OrderStatusLabel}</span>
            </div>
        </>
    )
}
