import { DefaultBreadcrumbs } from "../../../Components/Breadcrumbs/Breadcrumbs";
import { ReactComponent as MenuIcon } from "../../../Assets/menu.svg";
import { ReactComponent as LeftArrow } from "../../../Assets/chevron-down.svg";
import React, { useEffect, useState } from "react";
import { MultipleTab } from "../../../Components/Tabs/Tabs";
import { LargeDestructiveButton, LargePrimaryButton } from "../../../Components/Buttons/Button";
import BasicDetails from "./TabComponents/BasicDetails";
import ExposeDish from "./TabComponents/ExposeDish";
import DishPricing from "./TabComponents/DishPricing/DishPricing";
import Badges from "./TabComponents/Badges/Badges";
import LinkAddOnGroup from "./TabComponents/LinkAddOnGroup/LinkAddOnGroup";
import DishTiming from "./TabComponents/DishTiming";
import UpSellItems from "./TabComponents/UpSellItems";
import UpSellCurrentCart from "./TabComponents/UpSellCurrentCart";
import { useLocation, useNavigate } from "react-router-dom";
import { DropDownTabs } from "../../../Components/DropDown/DropDownTabs";

export default function MenuManagementAddDish() {
    const location = useLocation();

    const state = location.state;

    const menuItems = ["Basic details", "Expose dish", "Dish pricing", "Badges", "Link add-on groups", "Dish timings", "Up-sell dishes", "Up-sell current dish in cart"];

    const [activeTab, setActiveTab] = useState(state ? 2 : 0);

    const handleTabClick = (index) => {
        setActiveTab(index);
    };

    useEffect(() => {
        switch (activeTab) {
            case 0:
                setRenderPage(<BasicDetails />);
                break;

            case 1:
                setRenderPage(<ExposeDish />);
                break;

            case 2:
                setRenderPage(<DishPricing />);
                break;

            case 3:
                setRenderPage(<Badges />);
                break;

            case 4:
                setRenderPage(<LinkAddOnGroup />);
                break;

            case 5:
                setRenderPage(<DishTiming />);
                break;

            case 6:
                setRenderPage(<UpSellItems />);
                break;

            case 7:
                setRenderPage(<UpSellCurrentCart />);
                break;

            default:
                break;
        }
    }, [activeTab]);

    const [renderPage, setRenderPage] = useState(<BasicDetails />);

    const navigate = useNavigate();

    const handleClickBack = () => {
        navigate("/menu");
    };
    return (
        <>
            <div className="bg-[#fafafa]">
                <div className="px-8 lg:px-4 pt-4 w-full max-w-[1336px] mx-auto bg-white relative md:max-w-full">
                    <div className="md:hidden">
                        <DefaultBreadcrumbs mainTab="Menu management" icon={<MenuIcon height={20} width={20} />} tab1={state ? "Edit dish" : "Add dish"} />
                    </div>
                    <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer" onClick={handleClickBack}>
                        <LeftArrow className="rotate-90" />
                        <span className="ml-1">Back to catalogue</span>
                    </div>
                    <div className="hidden md:block mb-4 pb-4 border-b border-neutral-300">
                        <DropDownTabs
                            menuItems={[
                                { item: "Basic details", onClick: () => handleTabClick(0) },
                                { item: "Expose dish", onClick: () => handleTabClick(1) },
                                { item: "Dish pricing", onClick: () => handleTabClick(2) },
                                { item: "Badges", onClick: () => handleTabClick(3) },
                                { item: "Link add-on groups", onClick: () => handleTabClick(4) },
                                { item: "Dish timings", onClick: () => handleTabClick(5) },
                                { item: "Up-sell dishes", onClick: () => handleTabClick(6) },
                                { item: "Up-sell current dish in cart", onClick: () => handleTabClick(7) },
                            ]}
                        />
                    </div>
                    <div className="flex flex-row md:block">
                        <div className="flex flex-col mt-4 pr-6 mr-5 border-r min-w-[197px] h-auto border-neutral-300 scrollbarStyle md:hidden overflow-auto [&::-webkit-scrollbar]:hidden lg:pr-4 lg:mr-3 lg:min-w-[190px] pb-20">
                            {menuItems.map((el, index) => (
                                <div className="lg:max-w-[173px] w-full mb-4" key={index}>
                                    <MultipleTab
                                        label={el}
                                        index={index}
                                        onClick={handleTabClick}
                                        isActive={activeTab === index}
                                        minWidth="min-w-[173px]"
                                        disabled={!((state && [2, 5].includes(index)) || state === null)}
                                    />
                                </div>
                            ))}
                        </div>

                        <div className="mt-4 w-full pl-1 md:pl-0 md:mt-0 overflow-auto pb-20">{renderPage}</div>
                    </div>
                </div>
                <div className="sticky z-[100] md:z-[8] md:fixed md:justify-center md:border-none md:py-0 md:pb-1 md:pt-2 md:shadow-dropShadow left-0 right-0 bottom-0 flex px-8 lg:px-4 flex-row justify-end w-full max-w-[1336px] mx-auto bg-white py-6 border-t border-neutral-300 lg:py-4">
                    <div className="min-w-[196px] mr-5 md:w-1/2 md:mr-[7.5px] md:min-w-0">
                        <LargeDestructiveButton name="Discard" />
                    </div>
                    <div className="min-w-[196px] md:w-1/2 md:ml-[7.5px] md:min-w-0">
                        <LargePrimaryButton name="Save" />
                    </div>
                </div>
            </div>
        </>
    );
}
