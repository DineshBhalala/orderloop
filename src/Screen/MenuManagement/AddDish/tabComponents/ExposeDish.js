import React from "react";
import Header from "../../../Setting/Components/Header";
import { CheckBoxWithoutLabels } from "../../../../Components/FormControl/FormControls";

export default function ExposeDish() {
    const modes = ["Dine-in", "Takeaway", "Delivery", "QSR", "Self-serve"];
    return (
        <>
            <div className="mb-6">
                <Header title="Expose dish" page="menuManagement" headerBottomLine="Select the ordering modes in which you would like the dish to appear to the in-house workers and the customers." />
            </div>
            <div className="mb-4">
                <span className="paragraphMediumItalic text-neutral-500">Selected modes(active) - 01</span>
            </div>
            <div className="">
                {modes.map((el, index) => (
                    <div className="flex flex-row items-center mb-4" key={index}>
                        <CheckBoxWithoutLabels />
                        <span className="paragraphMediumRegular ml-6">{el}</span>
                    </div>
                ))}
            </div>
        </>
    );
}
