import React, { useState } from "react";
import Header from "../../../Setting/Components/Header";
import { TimerSlotStoreSetting } from "../../../../Components/TimeSlot/TimeSlot";

export default function DishTiming() {
    const [sameTimingEnabled, setSameTimingEnabled] = useState(false);

    const handleChangeSameTiming = (value) => {
        setSameTimingEnabled(value);
    };

    return (
        <>
            <div className="max-w-[636px]">
                <div className="mb-6">
                    <Header title="Dish timings" page="menuManagement" headerBottomLine="Set the availability of dish throughout the day and week according to the ordering modes." />
                </div>

                <div className="mb-6">
                    <Header
                        page="menuManagement"
                        showStatus={handleChangeSameTiming}
                        title="Enable same timings for all ordering modes"
                        hasSwitch
                        headerBottomLine="Enabling the same timings for all ordering modes will set the dish availability time the same across the selected exposed dish modes."
                    />
                </div>

                {/* timer slot with dropdown is wholly can be a single component */}
                {sameTimingEnabled ? (
                    <div className="">
                        <div className="mb-4">
                            <Header
                                title="Set timings"
                                headerBottomLine="This is the time when the customers will be able to order the dish from the mobile application."
                                label="Each day can have only 6 time slots."
                                labelStyle="paragraphMediumItalic text-primary-500"
                                hasDropDown
                                labelMarginB="mb-4"
                                shadow="shadow-smallDropDownShadow"
                                marginBetween="mt-1"
                            />
                        </div>

                        <div className="max-w-[411px]">
                            <TimerSlotStoreSetting />
                        </div>
                    </div>
                ) : (
                    <div className="">
                        <div className="mb-6 pb-6 border-b border-neutral-300">
                            <Header
                                title="Set delivery timings"
                                headerBottomLine="Please Specify the timings when this item will be available on orderloop."
                                hasDropDown
                                label="Each day can have only 6 time slots."
                                labelStyle="paragraphMediumItalic text-primary-500"
                                height="h-[52px]"
                                dropdownLabel="Full time"
                                labelMarginB="mb-4"
                                marginBetween="mt-1"
                                shadow="shadow-smallDropDownShadow"
                            />
                        </div>
                        <div className="">
                            <Header
                                page="menuManagement"
                                title="Set takeaway timings"
                                headerBottomLine="Please Specify the timings when this item will be available on orderloop."
                                hasDropDown
                                label="Each day can have only 6 time slots."
                                labelStyle="paragraphMediumItalic text-primary-500"
                                height="h-[52px]"
                                dropdownLabel="Full time"
                                labelMarginB="mb-4"
                                marginBetween="mt-1"
                                shadow="shadow-smallDropDownShadow"
                            />
                        </div>
                    </div>
                )}
            </div>
        </>
    );
}
