import React from "react";
import Header from "../../../../Setting/Components/Header";
import SearchDropDown from "../../../Components/SearchDropDown";
import { LargePrimaryButton } from "../../../../../Components/Buttons/Button";
import { ReactComponent as ReArrangeIcon } from "../../../../../Assets/re-arrange.svg";
import { ReactComponent as SelectIcon } from "../../../../../Assets/select.svg";

export default function LinkAddOnGroup() {
    const tableDetails = [
        { groupName: "No add-on group selected", dishes: [] },
        { groupName: "Beverages", dishes: ["Sprite(250 ml) - ₹20.00/-"] },
        { groupName: "Nachos", dishes: ["Nachos with Cheese Dip - ₹200.00/- ", "Nachos with Salsa & Cheese Dip - ₹250.00/- ", "Cheese Loaded Nachos - ₹150.00/- "] },
        { groupName: "Mini Samosas", dishes: ["Cheese Loaded Nachos - ₹150.00/- ", "Nachos with Salsa & Cheese Dip - ₹250.00/- ", "Nachos with Cheese Dip - ₹200.00/- "] },
        { groupName: "French Fries", dishes: ["Salted Fries (Single Serve) - ₹70.00/- ", "Salted Fries (Single Serve) - ₹80.00/- ", "Cheese Loaded Fries - ₹120.00/- "] },
    ];
    const menuItems = ["Beverages", "French Fries", "Mini Samosas", "Nachos", "Garlic Breads"];

    return (
        <>
            <div className="mb-4">
                <Header
                    page="menuManagement"
                    title="Link add-on groups"
                    headerBottomLine="By linking add-on groups to the dish you can offer customization options to the customers. You can define it as mandatory or optional for the customers too."
                />
            </div>
            <span className="paragraphMediumItalic text-neutral-500">(Select add-on groups)</span>
            <div className="mb-4 max-w-[312px] relative">
                <SearchDropDown searchInputPlaceholder="Search add-on by typing..." menuItems={menuItems} />
            </div>
            <div className="flex flex-row items-center mb-4">
                <div className="mr-2">
                    <LargePrimaryButton name="Re-arrange" leftIconDefault={<ReArrangeIcon stroke="#FFFFFF" />} leftIconClick={<ReArrangeIcon stroke="#C2BEED" />} />
                </div>
                <div className="ml-2">
                    <LargePrimaryButton name="Bulk select" leftIconDefault={<SelectIcon stroke="#FFFFFF" />} leftIconClick={<SelectIcon stroke="#C4BEED" />} />
                </div>
            </div>
            <div className="w-full rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden border-neutral-300 border lg:max-w-[462px] mb-4 max-w-[598px]">
                <table className="w-full break-words tableMediaLibrary">
                    <thead>
                        <tr className="shadow-innerShadow bg-neutral-50 text-left justify-center h-11">
                            <th className="px-6 min-w-[265px] lg:min-w-[231px] paragraphOverlineSmall text-neutral-700">ADD-ON GROUP NAME</th>
                            <th className="px-6 min-w-[331px] lg:min-w-[227px] paragraphOverlineSmall text-neutral-700">ADD-ON DISHES & PRICES</th>
                        </tr>
                    </thead>
                    <tbody>
                        {tableDetails.map((el, index) => (
                            <tr className={`paragraphSmallRegular ${index !== 0 && "border-t"} border-neutral-300 justify-center `} key={index}>
                                <td className="p-6">{el.groupName}</td>
                                <td className="py-[14px] px-6">
                                    {el.dishes.map((dishEl, dishIndex) => (
                                        <div className="flex flex-row items-start mt-0.5 pb-0.5" key={dishIndex}>
                                            <span className="bg-black rounded-full mt-2 h-1.5 w-full max-w-[6px] mr-5" />
                                            <span className="paragraphSmallRegular">{dishEl}</span>
                                        </div>
                                    ))}
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </>
    );
}
