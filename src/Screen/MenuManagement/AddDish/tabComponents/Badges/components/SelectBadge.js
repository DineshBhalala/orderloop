import React from "react";
import Header from "../../../../../Setting/Components/Header";

export default function SelectBadge() {
    return (
        <>
            <div className="">
                <Header
                    title="Badges"
                    headerBottomLine="Create a new badge or select an existing one to attach it with the dish. The attached badge will be reflected within the mobile application."
                    hasDropDown
                    label="(Select badge)"
                    height="h-[52px]"
                    buttonTextColor="neutral-300"
                    dropdownLabel="Select a badge for the dish "
                    labelMarginB="mb-2"
                    labelStyle="paragraphMediumItalic text-neutral-500"
                    shadow="smallDropDownShadow"
                />
            </div>
        </>
    );
}
