import React from "react";
import Header from "../../../../../Setting/Components/Header";
import { LargePrimaryButton } from "../../../../../../Components/Buttons/Button";
import { ReactComponent as AddIcon } from "../../../../../../Assets/add.svg";
import { ReactComponent as EditIcon } from "../../../../../../Assets/edit.svg";
import { ReactComponent as SelectIcon } from "../../../../../../Assets/select.svg";

export default function BadgeLibrary() {
    const tableDetails = [
        {
            badgeTitle: "Bestseller",
            badgeColor: "secondary-700",
            badgeColorCode: "#FFA704",
        },
        {
            badgeTitle: "Must try",
            badgeColor: "tertiary-800",
            badgeColorCode: "#3D8C82",
        },
        {
            badgeTitle: "Spicy",
            badgeColor: "primary-500",
            badgeColorCode: "#6C5DD3",
        },
        {
            badgeTitle: "Signature",
            badgeColor: "success-700",
            badgeColorCode: "#15803D",
        },
    ];

    return (
        <>
            <div className="mb-4">
                <Header title="Badge Library" headerBottomLine="Create a new badge or edit an existing one." />
            </div>
            <div className="flex flex-row mb-4">
                <div className="mr-4">
                    <LargePrimaryButton name="Add badge" hideName="lg:hidden" leftIconDefault={<AddIcon stroke="#FFFFFF" />} leftIconClick={<AddIcon stroke="#C4BEED" />} />
                </div>
                <div className="mr-4">
                    <LargePrimaryButton name="Edit badge" leftIconDefault={<EditIcon stroke="#FFFFFF" />} leftIconClick={<EditIcon stroke="#C4BEED" />} />
                </div>
                <div className="">
                    <LargePrimaryButton name="Bulk select" leftIconDefault={<SelectIcon stroke="#FFFFFF" />} leftIconClick={<SelectIcon stroke="#C4BEED" />} />
                </div>
            </div>

            <div className="w-fit rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden border-neutral-300 border lg:max-w-[462px] mb-4">
                <table className="w-full break-words tableMediaLibrary">
                    <thead>
                        <tr className="shadow-innerShadow bg-neutral-50 text-left justify-center h-11">
                            <th className="px-6 w-[200px] paragraphOverlineSmall text-neutral-700">BADGE TITLE</th>
                            <th className="px-6 w-[196px] paragraphOverlineSmall text-neutral-700">BADGE COLOR</th>
                            <th className="px-6 w-[202px] paragraphOverlineSmall text-neutral-700">BADGE PREVIEW</th>
                        </tr>
                    </thead>
                    <tbody>
                        {tableDetails.map((el, index) => (
                            <tr className={`paragraphSmallRegular ${index !== 0 && "border-t"} border-neutral-300 justify-center h-[70px]`} key={index}>
                                <td className="px-6">{el.badgeTitle}</td>
                                <td className="px-6">
                                    <div className="flex flex-row items-center">
                                        <div style={{ backgroundColor: el.badgeColorCode }} className={`h-6 w-6 rounded mr-3`} />
                                        <span className="paragraphSmallRegular">{el.badgeColorCode}</span>
                                    </div>
                                </td>
                                <td className="px-6">
                                    <div style={{ backgroundColor: el.badgeColorCode }} className={`px-2 py-1 text-white rounded w-fit`}>
                                        {el.badgeTitle}
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </>
    );
}
