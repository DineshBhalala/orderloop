import React from "react";
import Header from "../../../Setting/Components/Header";
import { DefaultInputField } from "../../../../Components/InputField/InputField";
import mobile from "../../../../Assets/mobile.png";

export default function BasicDetails() {
    return (
        <>
            <div className="flex flex-row justify-between xl:block -mb-20 xl:mb-0">
                <div className="min-w-[661px] pr-6 pb-20 border-r border-neutral-300 w-full xl:pr-0 xl:pb-2 xl:border-r-0 xl:border-b xl:mb-6 xl:min-w-full md:pb-0">
                    <div className="mb-4">
                        <Header
                            page="menuManagement"
                            title="Basic details"
                            headerBottomLine="Enter the basic details of the dish that you would like to create. The created dish will be reflected in the mobile application and the POS."
                        />
                    </div>
                    <div className="mb-4">
                        <Header title="Dish image" page="menuManagement" headerBottomLine="Select a dish image that will be displayed within the category in the mobile application and the POS." />
                    </div>
                    <div className="flex flex-row paragraphMediumRegular mb-4 md:block">
                        <button className="justify-center h-12 border-neutral-300 rounded-md border max-w-[197px] w-full mr-2 md:mr-0 md:mb-2 md:block">Upload an image</button>
                        <button className="justify-center h-12 border-neutral-300 rounded-md border max-w-[197px] w-full ml-2 md:ml-0 md:block">Select from library</button>
                    </div>

                    <div className="mb-4">
                        <Header
                            page="menuManagement"
                            labelMarginB="mb-2"
                            title="Category"
                            headerBottomLine="Select a category into which the dish will be created. The dish wil be displayed within the category in the mobile application and the POS."
                            hasDropDown
                            label="(Select category)"
                            labelStyle="paragraphMediumItalic text-neutral-500"
                            dropdownLabel="Enter city in english"
                            shadow="shadow-smallDropDownShadow"
                            buttonTextColor="neutral-300"
                        />
                    </div>
                    <div className="mb-4">
                        <Header
                            page="menuManagement"
                            title="Internal name - title"
                            headerBottomLine="Select an internal name for the dish which will be used to access the dish information across the software application."
                            placeholder="Enter internal name title"
                            hasInputField
                        />
                    </div>
                    <div className="mb-4">
                        <Header
                            page="menuManagement"
                            title="Display name"
                            headerBottomLine="Select a display name for the dish which will be used to display the dish information across the mobile application and POS. "
                        />
                    </div>

                    <div className="flex flex-row items-center mb-6 lg:block">
                        <div className="mr-1.5 w-1/2 lg:w-full lg:mb-2 lg:max-w-[312px] md:max-w-full md:mr-0">
                            <DefaultInputField
                                labelMarginB="mb-2"
                                label="(English)"
                                labelStyle="paragraphMediumItalic text-neutral-500"
                                placeholder="Enter display name in English"
                                shadow="shadow-smallDropDownShadow"
                            />
                        </div>
                        <div className="ml-1.5 w-1/2 lg:w-full lg:max-w-[312px] md:max-w-full md:ml-0">
                            <DefaultInputField
                                labelMarginB="mb-2"
                                label="(ગુજરાતી)"
                                labelStyle="paragraphMediumItalic text-neutral-500"
                                placeholder="Enter display name in ગુજરાતી"
                                shadow="shadow-smallDropDownShadow"
                            />
                        </div>
                    </div>
                </div>
                <div className="w-full flex justify-center">
                    <div className="w-full max-w-[329px] md:max-w-[280px] ml-8 xl:ml-0">
                        <img src={mobile} alt="" />
                    </div>
                </div>
            </div>
        </>
    );
}
