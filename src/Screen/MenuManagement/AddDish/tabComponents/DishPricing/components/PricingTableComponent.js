import React, { useState } from "react";
import Header from "../../../../../Setting/Components/Header";
import { LargePrimaryButton } from "../../../../../../Components/Buttons/Button";
import { ReactComponent as EditIcon } from "../../../../../../Assets/edit.svg";
import { ReactComponent as SelectIcon } from "../../../../../../Assets/select.svg";
import { CheckBoxWithoutLabels } from "../../../../../../Components/FormControl/FormControls";
import EditPricePopUp from "./EditPricePopUp";
import ToggleSwitch from "../../../../../../Components/ToggleSwitch/ToggleSwitch";

export default function PricingTableComponent() {
    const tableDetails = [
        { orderingMode: "Dine-in", price: "₹200.00/-" },
        { orderingMode: "Takeaway", price: "₹180.00/-" },
        { orderingMode: "Delivery", price: "₹220.00/-" },
        { orderingMode: "QSR", price: "₹150.00/-" },
    ];
    const [editIcon, setEditIcon] = useState(false);

    const handleClickEdit = () => {
        setEditIcon(!editIcon);
    };

    const [showEditPopUp, setShowEditPopUp] = useState(false);

    const handleClickPriceEdit = () => {
        editIcon && setShowEditPopUp(!showEditPopUp);
    };

    return (
        <>
            <div className="mb-6">
                <Header
                    page="menuManagement"
                    title="Pricing table"
                    headerBottomLine="Pricing table consists of all the pricing structure set for the current dish. You can view and modify the prices from the table too."
                />
            </div>

            <div className="flex flex-row mb-4">
                <div className="mr-2 cursor-pointer" onClick={handleClickEdit}>
                    <LargePrimaryButton name="Edit price" leftIconDefault={<EditIcon stroke="#FFFFFF" />} leftIconClick={<EditIcon stroke="#C4BEED" />} />
                </div>
                <div className="ml-2">
                    <LargePrimaryButton name="Bulk select" leftIconDefault={<SelectIcon stroke="#FFFFFF" />} leftIconClick={<SelectIcon stroke="#C4BEED" />} />
                </div>
            </div>

            <div className="mb-4">
                <p className="paragraphLargeMedium">
                    Variant 1 <span className="text-neutral-500 paragraphLargeItalic">(Variant name)</span>
                </p>
            </div>

            <div className="w-full rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden border-neutral-300 border mb-4">
                <table className="w-full break-words tableMediaLibrary">
                    <thead>
                        <tr className="shadow-innerShadow bg-neutral-50 text-left justify-center h-11">
                            <th className="px-6 min-w-[155px] paragraphOverlineSmall text-neutral-700">STATUS</th>
                            <th className="px-6 min-w-[247px] paragraphOverlineSmall text-neutral-700">ORDERING MODE</th>
                            <th className="px-6 min-w-[234px] paragraphOverlineSmall text-neutral-700">DISH PRICE</th>
                        </tr>
                    </thead>
                    <tbody>
                        {tableDetails.map((el, index) => (
                            <tr className={`paragraphSmallRegular ${index !== 0 && "border-t"} border-neutral-300 justify-center h-[70px]`} key={index}>
                                <td className="px-6">
                                    <CheckBoxWithoutLabels />
                                </td>
                                <td className="px-6">{el.orderingMode}</td>
                                <td className={`px-6  ${editIcon && "cursor-pointer"}`} onClick={handleClickPriceEdit}>
                                    <div className="flex flex-row items-center">
                                        {editIcon && (
                                            <span className="mr-3">
                                                <EditIcon />
                                            </span>
                                        )}
                                        <span>{el.price}</span>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>

            <div className="mb-4">
                <p className="paragraphLargeMedium">
                    Variant 2 <span className="text-neutral-500 paragraphLargeItalic">(Variant name)</span>
                </p>
            </div>

            <div className="w-full rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden border-neutral-300 border">
                <table className="w-full break-words tableMediaLibrary">
                    <thead>
                        <tr className="shadow-innerShadow bg-neutral-50 text-left justify-center h-11">
                            <th className="px-6 min-w-[155px] paragraphOverlineSmall text-neutral-700 md:min-w-0">STATUS</th>
                            <th className="px-6 min-w-[247px] paragraphOverlineSmall text-neutral-700 md:min-w-0">ORDERING MODE</th>
                            <th className="px-6 min-w-[234px] paragraphOverlineSmall text-neutral-700 md:min-w-0">DISH PRICE</th>
                        </tr>
                    </thead>
                    <tbody>
                        {tableDetails.map((el, index) => (
                            <tr className={`paragraphSmallRegular ${index !== 0 && "border-t"} border-neutral-300 justify-center h-[70px]`} key={index}>
                                <td className="px-6">
                                    <CheckBoxWithoutLabels />
                                </td>
                                <td className="px-6">{el.orderingMode}</td>
                                <td className="px-6">{el.price}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
            <div className="flex-row items-center my-6 py-6 border-y border-neutral-300 flex">
                <h1 className="paragraphLargeMedium mr-4">Enable/disable dish on all ordering modes</h1>
                <ToggleSwitch />
            </div>

            <div className="">
                <Header
                    page="menuManagement"
                    title="Enable/disable same price across all ordering modes"
                    hasSwitch
                    label="(Set price)"
                    hasInputField
                    shadow=""
                    labelStyle="paragraphMediumItalic text-neutral-500"
                    placeholder="Enter price in rupees"
                />
            </div>

            <div className={`${!showEditPopUp && "hidden"}`}>
                <EditPricePopUp handleClickClose={handleClickPriceEdit} />
            </div>
        </>
    );
}
