import React from "react";
import { DefaultInputField } from "../../../../../../Components/InputField/InputField";
import Header from "../../../../../Setting/Components/Header";
import DropDown from "../../../../../../Components/DropDown/DropDown";

export default function SetSeparatePrice() {
    return (
        <>
            <div className="mb-4">
                <Header
                    page="menuManagement"
                    title="Dish price"
                    headerBottomLine="Set the dish price. Current price defines the amount that customer needs to pay whereas compare price defines the price that was earlier."
                />
            </div>

            <div className="flex flex-row items-center mb-6 lg:block">
                <div className="mr-1.5 w-1/2 lg:w-full lg:mb-2 lg:max-w-[312px] md:max-w-full lg:mr-0">
                    <DefaultInputField
                        boxHeight="[52px]"
                        labelMarginB="mb-2"
                        label="(Current price)"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        placeholder="Enter current price in rupees"
                        shadow="shadow-smallDropDownShadow"
                    />
                </div>

                <div className="ml-1.5 w-1/2 lg:w-full lg:max-w-[312px] md:max-w-full lg:ml-0">
                    <DefaultInputField
                        boxHeight="[52px]"
                        labelMarginB="mb-2"
                        label="(Compare/strike-through price)"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        placeholder="Enter compare price in rupees"
                        shadow="shadow-smallDropDownShadow"
                    />
                </div>
            </div>

            <div className="mb-4">
                <Header page="menuManagement" title="Packaging price and GST rate" headerBottomLine="Set the packaging price(if required) and select a GST slab(if applicable)." />
            </div>

            <div className="flex flex-row items-center mb-6 lg:block">
                <div className="mr-1.5 w-1/2 lg:w-full lg:mb-2 lg:max-w-[312px] md:max-w-full lg:mr-0">
                    <DefaultInputField
                        boxHeight="[52px]"
                        labelMarginB="mb-2"
                        label="(Packaging price)"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        placeholder="Enter packaging price in rupees"
                        shadow="shadow-smallDropDownShadow"
                    />
                </div>
                <div className="ml-1.5 w-1/2 lg:w-full lg:max-w-[312px] md:max-w-full lg:ml-0">
                    <DropDown
                        height="h-[52px]"
                        labelMarginB="mb-2"
                        label="(GST)"
                        dropdownLabel="Select a GST slab"
                        shadow="shadow-smallDropDownShadow"
                        buttonTextColor="neutral-300"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                    />
                </div>
            </div>

            <div className="mb-4">
                <Header title="Quantity" page="menuManagement" headerBottomLine="Set the quantity and select the unit to define the overall dish size." />
            </div>

            <div className="flex flex-row items-center mb-6 lg:block">
                <div className="mr-1.5 w-1/2 lg:w-full lg:mb-2 lg:max-w-[312px] md:max-w-full lg:mr-0">
                    <DefaultInputField
                        boxHeight="[52px]"
                        labelMarginB="mb-2"
                        label="(Quantity)"
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        placeholder="Enter quantity"
                        shadow="shadow-smallDropDownShadow"
                    />
                </div>
                <div className="ml-1.5 w-1/2 lg:w-full lg:max-w-[312px] md:max-w-full lg:ml-0">
                    <DropDown
                        labelStyle="paragraphMediumItalic text-neutral-500"
                        height="h-[52px]"
                        labelMarginB="mb-2"
                        label="(Quantity unit)"
                        dropdownLabel="Select a quantity unit"
                        shadow="shadow-smallDropDownShadow"
                        buttonTextColor="neutral-300"
                    />
                </div>
            </div>
        </>
    );
}
