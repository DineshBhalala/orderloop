import React, { useState } from "react";
import { ReactComponent as AddIcon } from "../../../../../../Assets/add.svg";
import { ReactComponent as DeleteIcon } from "../../../../../../Assets/trash.svg";
import SetSeparatePrice from "./SetSeparatePrice";
import { MultipleTab } from "../../../../../../Components/Tabs/Tabs";
import { LargeDestructiveButton, LargePrimaryButton } from "../../../../../../Components/Buttons/Button";
import Header from "../../../../../Setting/Components/Header";
import DeleteVariantPopUp from "./DeleteVariantPopUp";

export default function SetPrice() {
    const [showSetDineInPricing, setShowSetDineInPricing] = useState(false);

    const hideShowDinIinPrice = () => {
        setShowSetDineInPricing(!showSetDineInPricing);
    };

    const [enableSamePrice, setEnableSamePrice] = useState(false);

    const handleChangeSamePrice = () => {
        setEnableSamePrice(!enableSamePrice);
    };

    const [showSetTakeAwayPrice, setShowSetTakeAwayPrice] = useState(false);

    const handleShowTakeAwayPrice = () => {
        setShowSetTakeAwayPrice(!showSetTakeAwayPrice);
    };

    const [numberOfVariants, setNumberOfVariants] = useState(0);

    const handleClickAddVariant = () => {
        setNumberOfVariants(numberOfVariants + 1);
    };

    const [deleteVariant, setDeleteVariant] = useState(false);

    const handleClickDeleteVariant = () => {
        setDeleteVariant(!deleteVariant);
    };

    const [activeVariant, setActiveVariant] = useState(0);

    const [showDeleteVariantPopup, setShowDeleteVariantPopup] = useState(false);

    const handleClickVariant = (index) => {
        setActiveVariant(index);
        deleteVariant && setShowDeleteVariantPopup(!showDeleteVariantPopup);
    };

    const renderVariants = () => {
        return Array.from({ length: numberOfVariants }, (_, index) => (
            <div className="mr-4 mb-4 lg:mb-2 inline-block align-top" key={index}>
                <MultipleTab
                    minWidth="min-w-[143px]"
                    label={`Variant ${index + 1}`}
                    icon={<DeleteIcon stroke={index === activeVariant ? "#6C5DD3" : "#3F3D46"} />}
                    showIcon={deleteVariant}
                    index={index}
                    isActive={index === activeVariant}
                    onClick={() => handleClickVariant(index)}
                />
            </div>
        ));
    };

    return (
        <>
            <div className="mb-6">
                <Header
                    title="Dish pricing"
                    page="menuManagement"
                    headerBottomLine="Set the dish price for different types of ordering modes. You can set different pricing for different ordering modes."
                />
            </div>

            <div className="mb-2 lg:hidden">
                <div className="inline-block align-top">{renderVariants()}</div>
                <div className="mr-2 mb-4 inline-block align-top cursor-pointer" onClick={handleClickAddVariant}>
                    <LargePrimaryButton name="Add variant" leftIconDefault={<AddIcon stoke="#FFFFFF" />} leftIconClick={<AddIcon stroke="#C4BEED" />} isDefault={false} />
                </div>

                <div className="ml-2 mb-4 min-w-[146px] inline-block align-top cursor-pointer" onClick={handleClickDeleteVariant}>
                    <LargeDestructiveButton name="Delete variant" disabled={numberOfVariants === 0 && true} />
                </div>
            </div>

            <div className="mb-6">
                <Header
                    page="menuManagement"
                    title="Enable same price"
                    headerBottomLine="Enabling same price will keep the pricing same throughout all ordering modes."
                    hasSwitch
                    showStatus={handleChangeSamePrice}
                />
            </div>

            <div className={`${!enableSamePrice && "hidden"}`}>
                <SetSeparatePrice />
            </div>

            <div className={`${enableSamePrice && "hidden"} border-b border-neutral-300 mb-6`}>
                <div className={`mb-6`}>
                    <div className="flex flex-row items-center justify-between mb-1">
                        <h1 className="paragraphLargeMedium md:paragraphMediumMedium">Set dine-in pricing</h1>
                        <span className="paragraphMediumMedium text-primary-500 cursor-pointer" onClick={hideShowDinIinPrice}>
                            {!showSetDineInPricing ? "Show" : "Hide"}
                        </span>
                    </div>
                    <span className="paragraphMediumItalic text-neutral-500">Please specify the pricing of the dish for the dine-in ordering mode.</span>
                </div>

                <div className={`${!showSetDineInPricing && "hidden"}`}>
                    <SetSeparatePrice />
                </div>
            </div>

            <div className={`${enableSamePrice && "hidden"}`}>
                <div className="mb-6">
                    <div className="flex flex-row items-center justify-between mb-1">
                        <h1 className="paragraphLargeMedium md:paragraphMediumMedium">Set takeAway pricing</h1>
                        <span className="paragraphMediumMedium text-primary-500 cursor-pointer" onClick={handleShowTakeAwayPrice}>
                            {!showSetTakeAwayPrice ? "Show" : "Hide"}
                        </span>
                    </div>
                    <span className="paragraphMediumItalic text-neutral-500">Please specify the pricing of the dish for the takeaway ordering mode.</span>
                </div>

                <div className={`${!showSetTakeAwayPrice && "hidden"}`}>
                    <SetSeparatePrice />
                </div>
            </div>
                
            <div className={`${!showDeleteVariantPopup && "hidden"}`}>
                <DeleteVariantPopUp handleClickClose={handleClickVariant} variantNumber={activeVariant + 1} />
            </div>
        </>
    );
}
