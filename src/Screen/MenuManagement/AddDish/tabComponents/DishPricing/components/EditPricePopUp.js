import React from "react";
import { ReactComponent as CloseIcon } from "../../../../../../Assets/close.svg";
import { ReactComponent as LeftArrowIcon } from "../../../../../../Assets/chevron-down.svg";
import { DefaultInputField } from "../../../../../../Components/InputField/InputField";
import { LargeDestructiveButton, LargePrimaryButton } from "../../../../../../Components/Buttons/Button";

export default function EditPricePopUp(props) {
  return (
    <>
      <div className={`fixed bg-black bg-opacity-50 inset-0 z-[101] flex md:absolute md:z-[9] md:top-0 md:block md:overflow-hidden`}>
        <div className="max-w-[475px] w-full rounded-xl bg-shades-50 pt-6 pb-6 px-8 m-auto max-h-[969px] md:max-w-full md:rounded-none md:py-4 md:px-4 md:h-screen md:overflow-hidden">
          <div className="flex flex-row items-center justify-between mb-6 md:hidden">
            <div>
              <h3 className="paragraphLargeMedium">Edit price</h3>
              <div className="flex flex-row items-center">
                <p className="paragraphMediumItalic text-neutral-500">Edit existing price for the ordering mode</p>
              </div>
            </div>
            <span className="cursor-pointer" onClick={props.handleClickClose}>
              <CloseIcon />
            </span>
          </div>
          <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 px-8 md:px-0 cursor-pointer" onClick={() => props.handleClickClose()}>
            <LeftArrowIcon className="rotate-90" />
            <span className="ml-1">Back to delivery charges</span>
          </div>
          <div className="mb-20">
            <DefaultInputField placeholder="₹200.00/-" placeholderTextColor="text-neutral-900" label="Dish price" boxHeight="[44px]" shadow="shadow-shadowXsmall" />
          </div>
          <div className="flex flex-row">
            <div className="mr-2.5 w-1/2 cursor-pointer" onClick={props.handleClickClose}>
              <LargeDestructiveButton name="Discard" />
            </div>
            <div className="ml-2.5 w-1/2">
              <LargePrimaryButton name="Save" />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
