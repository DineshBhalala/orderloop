import React, { useState } from "react";
import { MultipleTab } from "../../../../../Components/Tabs/Tabs";
import SetPrice from "./Components/SetPrice";
import PricingTableComponent from "./Components/PricingTableComponent";

export default function DishPricing() {
    const handleClickPriceTab = (index) => {
        setActiveTab(index);
    };

    const tabName = ["Set price", "Pricing table"];

    const [activeTab, setActiveTab] = useState(0);

    return (
        <>
            <div className="max-w-[640px] md:max-w-full">
                <div className="flex flex-row items-center mb-6">
                    {tabName.map((el, index) => (
                        <div className="mr-4" key={index}>
                            <MultipleTab label={el} maxWidth={index === 0 ? "max-w-[102px]" : "max-w-[129px]"} isActive={index === activeTab} onClick={() => handleClickPriceTab(index)} />
                        </div>
                    ))}
                </div>
                {activeTab === 0 ? <SetPrice /> : <PricingTableComponent />}
            </div>
        </>
    );
}
