import React from "react";
import Header from "../../../Setting/Components/Header";

export default function UpSellCurrentCart() {
    const menuItems = ["Yes", "No"];
    return (
        <>
            <div className="">
                <Header
                    page="menuManagement"
                    title="Up-sell current dish in cart"
                    headerBottomLine="Would you like to up-sell the current dish in the cart? It will be recommended in the cart section of the ordering process within the mobile application."
                    hasDropDown
                    marginBetween="mt-6"
                    label="(Up-sell current dish)"
                    menuItems={menuItems}
                    labelStyle="paragraphMediumItalic text-neutral-500"
                    labelMarginB="mb-2"
                />
            </div>
        </>
    );
}
