import React from "react";
import Header from "../../../Setting/Components/Header";
import DropDown from "../../../../Components/DropDown/DropDown";
import { LargePrimaryButton } from "../../../../Components/Buttons/Button";
import { ReactComponent as ReArrangeIcon } from "../../../../Assets/re-arrange.svg";
import { ReactComponent as SelectIcon } from "../../../../Assets/select.svg";
import SearchDropDown from "../../Components/SearchDropDown";

export default function UpSellItems() {
    const tableDetails = [
        {
            category: "Burger",
            name: "Paneer burger",
            price: "₹200.00/-",
        },
        {
            category: "Burger",
            name: "Pizza burger",
            price: "₹250.00/-",
        },
    ];

    const categories = ["Burger", "Pizza"];

    const dishes = ["Cheese Burger", "Paneer Burger", "Pizza Burger", "Mini Burger", "Cheese Paneer Burger"];

    return (
        <>
            <div className="max-w-[636px]">
                <div className="mb-6">
                    <Header
                        page="menuManagement"
                        title="Up-selling dishes"
                        headerBottomLine="You can add multiple up-selling dishes to your current dish to offer upgradation or include add-ons to the dish that customers are ordering. "
                    />
                </div>
                <div className="flex flex-row mb-6">
                    <div className="mr-1.5 w-1/2 relative">
                        <DropDown
                            height="h-[52px]"
                            labelMarginB="mb-2"
                            label="(Select category)"
                            labelStyle="paragraphMediumItalic text-neutral-500"
                            dropdownLabel="Select category"
                            buttonTextColor="neutral-300"
                            menuItems={categories}
                        />
                    </div>
                    <div className="ml-1.5 w-1/2 relative">
                        <SearchDropDown
                            menuItems={dishes}
                            type="upSell"
                            labelColor="text-neutral-300"
                            searchInputPlaceholder="Search dish by typing..."
                            label="(Select dish)"
                            labelStyle="paragraphMediumItalic text-neutral-300"
                            dropdownLabel="Select category"
                            buttonTextColor="neutral-300"
                            dropdownIconColor="#D3D2D8"
                        />
                    </div>
                </div>

                <div className="flex flex-row mb-4">
                    <div className="mr-2">
                        <LargePrimaryButton name="Re-arrange" leftIconDefault={<ReArrangeIcon stroke="#FFFFFF" />} leftIconClick={<ReArrangeIcon stroke="#C4BEED" />} />
                    </div>
                    <div className="ml-2">
                        <LargePrimaryButton name="Bulk select" leftIconDefault={<SelectIcon stroke="#FFFFFF" />} leftIconClick={<SelectIcon stroke="#C4BEED" />} />
                    </div>
                </div>

                <div className="w-full rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden border-neutral-300 border lg:max-w-[462px] mb-4">
                    <table className="w-full break-words tableMediaLibrary">
                        <thead>
                            <tr className="shadow-innerShadow bg-neutral-50 text-left justify-center h-11">
                                <th className="px-6 min-w-[231px] paragraphOverlineSmall text-neutral-700">DISH CATEGORY</th>
                                <th className="px-6 min-w-[263px] paragraphOverlineSmall text-neutral-700">DISH NAME</th>
                                <th className="px-6 min-w-[140px] paragraphOverlineSmall text-neutral-700">DISH PRICE</th>
                            </tr>
                        </thead>
                        <tbody>
                            {tableDetails.length === 0 ? (
                                <tr className="justify-center h-[70px]">
                                    <td className="pl-6 paragraphSmallRegular">No up-selling dishes selected</td>
                                </tr>
                            ) : (
                                tableDetails.map((el, index) => (
                                    <tr className={`paragraphSmallRegular ${index !== 0 && "border-t"} border-neutral-300 justify-center h-[70px]`} key={index}>
                                        <td className="px-6">{el.category}</td>
                                        <td className="px-6">{el.name}</td>
                                        <td className="px-6">{el.price}</td>
                                    </tr>
                                ))
                            )}
                        </tbody>
                    </table>
                </div>
            </div>
        </>
    );
}
