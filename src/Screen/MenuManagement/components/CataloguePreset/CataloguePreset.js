import React, { useState } from "react";
import { DefaultInputField } from "../../../../Components/InputField/InputField";
import { ReactComponent as SearchIcon } from "../../../../Assets/search.svg";
import { ReactComponent as SelectIcon } from "../../../../Assets/select.svg";
import { ReactComponent as AddIcon } from "../../../../Assets/add.svg";
import { ReactComponent as MenuIcon } from "../../../../Assets/menu.svg";
import { LargePrimaryButton } from "../../../../Components/Buttons/Button";
import { LinkOffer } from "../../../../Components/LinkOffer/LinkOffer";
import PaginationWithNumber from "../../../../Components/Pagination/PaginationWithNumber";
import LinkOutletPopup from './Components/LinkOutlet'
import CreatePresetPopupStep2 from "../Catalogue/CreatePresetPopupStep2";
import pizza1 from "../../../../Assets/mediaLibrary/pizza1.png";
import pizza2 from "../../../../Assets/mediaLibrary/pizza2.png";
import pizza3 from "../../../../Assets/mediaLibrary/pizza3.png";
import pizza4 from "../../../../Assets/mediaLibrary/pizza4.png";
import pizza5 from "../../../../Assets/mediaLibrary/pizza5.png";
import pizza6 from "../../../../Assets/mediaLibrary/pizza6.png";
import pizza7 from "../../../../Assets/mediaLibrary/pizza7.png";
import pizza8 from "../../../../Assets/mediaLibrary/pizza8.png";
import pizza9 from "../../../../Assets/mediaLibrary/pizza9.png";
import ListViewCataloguePresets from "../../../../Components/ListView/ListViewCataloguePresets";

export default function CataloguePreset() {
  const tableDetails = [
    {
      presetName: "Menu - 007",
      categories: "12",
      dishes: "128",
      createdBy: "Sarthak Kanchan",
      createdOn: "20 November, 2022",
      lastUpdate: "20 November, 2022",
      linkOutlet: 4,
    },
    {
      presetName: "Medium Scale Menu - 006",
      categories: "08",
      dishes: "96",
      createdBy: "Sarthak Kanchan",
      createdOn: "20 November, 2022",
      lastUpdate: "20 November, 2022",
      linkOutlet: 4,
    },
    {
      presetName: "Medium Scale Menu - 005",
      categories: "08",
      dishes: "120",
      createdBy: "Sarthak Kanchan",
      createdOn: "20 November, 2022",
      lastUpdate: "20 November, 2022",
      linkOutlet: 4,
    },
    {
      presetName: "Medium Scale Menu - 004",
      categories: "08",
      dishes: "96",
      createdBy: "Sarthak Kanchan",
      createdOn: "20 November, 2022",
      lastUpdate: "20 November, 2022",
      linkOutlet: 8,
    },
  ];

  const [showLinkOutlet, setShowLinkOutlet] = useState(false);

  const handleClickLinkOutlet = () => {
    setShowLinkOutlet(!showLinkOutlet);
  };

  const [showSelectCategoryDishesPopUp, setShowSelectCategoryDishesPopUp] = useState(false);

  const handleClickPreset = () => {
    setShowSelectCategoryDishesPopUp(!showSelectCategoryDishesPopUp);
  };

  const pizzaDishes = [
    {
      title: "Mexican Green Wave Pizza Text Extended",
      variants: "Yes(3)",
      image: pizza7,
    },
    {
      title: "Peppy Paneer",
      variants: "No",
    },
    {
      title: "Deluxe Veggie",
      variants: "Yes(3)",
    },
    {
      title: "Paneer Makhani",
      variants: "Yes(3)",
      image: pizza8,
    },
    {
      title: "Cheese n Tomato",
      variants: "Yes(3)",
    },
    {
      title: "Veg Extraveganza",
      variants: "Yes(3)",
      image: pizza9,
    },
  ];

  const categoriesDetails = [
    {
      title: "Sides",
      dishes: "21 dishes",
      img: pizza1,
      type: "veg-nonVeg",
      subCategoryDetails: [
        {
          title: "Garlic Bread",
          dishes: "09 dishes",
          img: pizza2,
          type: "nonVeg",
        },
        {
          title: "French Fries Text Extended",
          dishes: "12 dishes",
          img: pizza3,
          type: "veg",
        },
      ],
    },

    {
      title: "Pizza",
      dishes: "32 dishes",
      img: pizza4,
      type: "veg-nonVeg",
    },
    {
      title: "Burger",
      dishes: "03 dishes",
      img: pizza5,
      type: "veg-nonVeg",
    },
    {
      title: "Shakes",
      dishes: "07 dishes",
      img: pizza6,
      type: "veg-nonVeg",
    },
    {
      title: "Salad",
      dishes: "10 dishes",
      img: pizza7,
      type: "veg-nonVeg",
    },
    {
      title: "Pastries",
      dishes: "10 dishes",
      img: pizza8,
      type: "veg-nonVeg",
    },
  ];

  return (
    <>
      <div className="flex flex-row justify-between mb-6 md:block md:pb-2 md:mb-0">
        <div className="flex flex-row md:mb-4">
          <div className="w-full mr-2 min-w-[375px] lg:min-w-[298px] lg:mr-1 md:min-w-0">
            <DefaultInputField placeholder="Search preset" placeholderIcon={<SearchIcon stroke="#D3D2D8" />} />
          </div>
          <div className="ml-2 lg:ml-1 md:min-w-[64px]">
            <LargePrimaryButton name="Bulk select" leftIconDefault={<SelectIcon stroke="#FFFFFF" />} leftIconClick={<SelectIcon stroke="#C4BEED" />} hideName="md:hidden" />
          </div>
        </div>
        <div className="md:w-full">
          <LargePrimaryButton name="Create preset" leftIconClick={<AddIcon stroke="#C4BEED" />} leftIconDefault={<AddIcon stroke="#FFFFFF" />} />
        </div>
      </div>

      <div className="w-full rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden border-neutral-300 border mb-4">
        <table className="w-full break-words tableMediaLibrary">
          <thead>
            <tr className="shadow-innerShadow bg-neutral-50 text-left justify-center h-11">
              <th className="px-6 min-w-[273px] lg:min-w-[250px] paragraphOverlineSmall text-neutral-700">PRESET NAME</th>
              <th className="px-6 min-w-[256px] lg:min-w-[237px] paragraphOverlineSmall text-neutral-700">CATEGORIES AND DISHES</th>
              <th className="px-6 min-w-[199px] lg:min-w-[162px] paragraphOverlineSmall text-neutral-700">CREATED BY</th>
              <th className="px-6 min-w-[179px] paragraphOverlineSmall text-neutral-700">CREATED ON</th>
              <th className="px-6 min-w-[179px] paragraphOverlineSmall text-neutral-700">LAST UPDATE</th>
              <th className="px-6 min-w-[184px] paragraphOverlineSmall text-neutral-700">LINK OUTLET</th>
            </tr>
          </thead>
          <tbody>
            {tableDetails.map((el, index) => (
              <tr className={`paragraphSmallRegular ${index !== 0 && "border-t"} border-neutral-300 justify-center h-[70px]`} key={index}>
                <td className="px-6 cursor-pointer" onClick={handleClickPreset}>
                  {el.presetName}
                </td>
                <td className="px-6">
                  <div className="flex flex-col">
                    <div className="flex flex-row items-center mb-1">
                      <MenuIcon />
                      <span className="paragraphSmallRegular ml-2">Categories - {el.categories}</span>
                    </div>
                    <div className="flex flex-row items-center">
                      <MenuIcon />
                      <span className="paragraphSmallRegular ml-2">Dishes - {el.dishes}</span>
                    </div>
                  </div>
                </td>
                <td className="px-6 text-primary-500">{el.createdBy}</td>
                <td className="px-6">{el.createdOn}</td>
                <td className="px-6">{el.lastUpdate}</td>
                <td className="px-6">
                  <div className="cursor-pointer" onClick={handleClickLinkOutlet}>
                    <LinkOffer linkOfferNumber={el.linkOutlet} />
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>

      <div className="hidden md:block">
        {tableDetails.map((el, index) => (
          <div className="mt-2" key={index}>
            <ListViewCataloguePresets
              presetName={el.presetName}
              categories={el.categories}
              dishes={el.dishes}
              createdBy={el.createdBy}
              createdOn={el.createdOn}
              lastUpdatedOn={el.lastUpdate}
              handleClickViewDetails={handleClickPreset}
              handleLClickLink={handleClickLinkOutlet}
            />
          </div>
        ))}
      </div>

      <div className="md:hidden">
        <PaginationWithNumber />
      </div>

      <div className={`${!showLinkOutlet && "hidden"}`}>
        <LinkOutletPopup headerBottomLine="Link the menu with the desired outlets" page="cataloguePreset" handleClickClose={handleClickLinkOutlet} />
      </div>

      <div className={`${!showSelectCategoryDishesPopUp && "hidden"}`}>
        <CreatePresetPopupStep2 categoriesDetails={categoriesDetails} pizzaDishes={pizzaDishes} page="cataloguePreset" handleClickClose={handleClickPreset} />
      </div>
    </>
  );
}
