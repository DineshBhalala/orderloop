import React from "react";
import { ReactComponent as CloseIcon } from "../../../../../Assets/close.svg";
import { ReactComponent as FilterIcon } from "../../../../../Assets/filter.svg";
import { ReactComponent as LeftArrowIcon } from "../../../../../Assets/chevron-down.svg";
import { ReactComponent as SearchIcon } from "../../../../../Assets/search.svg";
import { DefaultInputField } from "../../../../../Components/InputField/InputField";
import ToggleSwitch from "../../../../../Components/ToggleSwitch/ToggleSwitch";
import { LargeDestructiveButton, LargePrimaryButton } from "../../../../../Components/Buttons/Button";
import ListViewOfferLink from "../../../../../Components/ListView/ListViewOfferLink";

export default function LinkOutletPopup(props) {
    const linkOutletTableDetails = [
        {
            outletName: "Domino's Pizza - Master Outlet(All)",
            state: "Gujarat",
            city: "Rajkot",
            address: "Shyamal Infinity, beneath Radio Mirchi, kalawad road",
        },
        {
            outletName: "Domino's Pizza - Master Outlet(All)",
            state: "Gujarat",
            city: "Rajkot",
            address: "Shyamal Infinity, beneath Radio Mirchi, kalawad road",
        },
        {
            outletName: "Domino's Pizza - Master Outlet(All)",
            state: "Gujarat",
            city: "Rajkot",
            address: "Shyamal Infinity, beneath Radio Mirchi, kalawad road",
        },
        {
            outletName: "Domino's Pizza - Master Outlet(All)",
            state: "Gujarat",
            city: "Rajkot",
            address: "Shyamal Infinity, beneath Radio Mirchi, kalawad road",
        },
    ];
    return (
        <>
            <div className="fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 flex justify-center items-center overflow-auto md:bg-white md:absolute px-4">
                <div className="max-w-[997px] w-full rounded-xl md:rounded-none bg-shades-50 px-8 py-6 md:px-4 md:py-4 m-auto md:max-w-full lg:max-w-[710px] md:absolute md:top-0">
                    <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer" onClick={() => props.handleClickClose()}>
                        <LeftArrowIcon className="rotate-90" />
                        <span className="paragraphMediumMedium ml-1">Back to catalogue preset</span>
                    </div>

                    <div className="flex flex-row justify-between items-center mb-2 pb-4 md:hidden">
                        <div>
                            <span className="paragraphLargeMedium">Link outlets</span>
                            <div className="flex flex-row items-center">
                                <span className="font-normal italic text-base leading-6 text-neutral-500">{props.headerBottomLine}</span>
                                <div className="w-1 h-1 rounded-full mx-3 bg-neutral-500 md:hidden" />
                                <span className="paragraphMediumItalic text-neutral-500">Outlet selected - 04</span>
                            </div>
                        </div>
                        <div className="md:hidden cursor-pointer" onClick={() => props.handleClickClose()}>
                            <CloseIcon />
                        </div>
                    </div>

                    <div className="flex flex-row justify-between mb-6 md:block md:mb-4">
                        <div className="max-w-[375px] w-full lg:max-w-[298px] md:max-w-full md:mb-4">
                            <DefaultInputField placeholderIcon={<SearchIcon stroke="#D3D2D8" />} placeholder="Search outlet" />
                        </div>
                        <div className="flex flex-row items-center md:justify-between">
                            <LargePrimaryButton name="Filters" leftIconDefault={<FilterIcon fill="#FFFFFF" />} leftIconClick={<FilterIcon fill="#C4BEED" />} />
                        </div>
                    </div>

                    <div className="w-full rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden border-neutral-300 border">
                        <table className="w-full break-words tableMediaLibrary">
                            <thead>
                                <tr className="paragraphOverlineSmall text-neutral-700 shadow-innerShadow bg-neutral-50 text-left justify-center h-11">
                                    <th className="px-6 min-w-[90px]">STATUS</th>
                                    <th className="px-6 min-w-[302px]">OUTLET NAME</th>
                                    <th className="px-6 min-w-[102px] lg:min-w-[135px]">STATE</th>
                                    <th className="px-6 min-w-[92px] lg:min-w-[151px]">CITY</th>
                                    <th className="px-6 min-w-[334px]">ADDRESS</th>
                                </tr>
                            </thead>
                            <tbody>
                                {linkOutletTableDetails.map((el, index) => {
                                    return (
                                        <tr className={`paragraphSmallRegular ${index !== 0 && "border-t"} justify-center h-[70px]`} key={index}>
                                            <td className="px-6">
                                                <ToggleSwitch enable={true} />
                                            </td>
                                            <td className="px-6">{el.outletName}</td>
                                            <td className="px-6">{el.state}</td>
                                            <td className="px-6">{el.city}</td>
                                            <td className="px-6">{el.address}</td>
                                        </tr>
                                    );
                                })}
                            </tbody>
                        </table>
                    </div>

                    <div className="hidden md:block">
                        {linkOutletTableDetails.map((el, index) => (
                            <div className="mt-2" key={index}>
                                <ListViewOfferLink outletName={el.outletName} state={el.state} city={el.city} address={el.address} />
                            </div>
                        ))}
                    </div>

                    <div className="flex justify-end mt-12 md:fixed md:bottom-0 md:justify-center md:block md:w-full md:pb-1 md:pt-2 md:shadow-dropShadow md:bg-white md:-ml-4 md:px-4">
                        <div className="flex flex-row">
                            <div className="mr-2 md:w-full">
                                <LargeDestructiveButton name="Discard changes" />
                            </div>
                            <div className="ml-2 md:w-full">
                                <LargePrimaryButton name="Save changes" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
