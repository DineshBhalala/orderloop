import React, { useState } from "react";
import { DefaultInputField } from "../../../../Components/InputField/InputField";
import { LargePrimaryButton } from "../../../../Components/Buttons/Button";

import pizza1 from "../../../../Assets/mediaLibrary/pizza1.png";
import pizza2 from "../../../../Assets/mediaLibrary/pizza2.png";
import pizza3 from "../../../../Assets/mediaLibrary/pizza3.png";
import pizza4 from "../../../../Assets/mediaLibrary/pizza4.png";
import pizza5 from "../../../../Assets/mediaLibrary/pizza5.png";
import pizza6 from "../../../../Assets/mediaLibrary/pizza6.png";
import pizza7 from "../../../../Assets/mediaLibrary/pizza7.png";
import pizza8 from "../../../../Assets/mediaLibrary/pizza8.png";
import pizza9 from "../../../../Assets/mediaLibrary/pizza9.png";

import { ReactComponent as SearchIcon } from "../../../../Assets/search.svg";
import { ReactComponent as MenuIcon } from "../../../../Assets/menu.svg";
import { ReactComponent as SelectIcon } from "../../../../Assets/select.svg";
import { ReactComponent as EditIcon } from "../../../../Assets/edit.svg";
import { ReactComponent as AddIcon } from "../../../../Assets/add.svg";
import { ReactComponent as ReArrangeIcon } from "../../../../Assets/re-arrange.svg";
import { SmallPrimaryButton } from "../../../../Components/Buttons/Button";
import { Categories, Dishes } from "../../../ItemAvailability/Components/card";
import DishDetailsList from "./DishDetailsList";
import DetailsDropDown from "./DetailsDropDown";
import CreatePresetPopupStep1 from "./CreatePresetPopupStep1";
import CreatePresetPopupStep2 from "./CreatePresetPopupStep2";
import { useNavigate } from "react-router-dom";

export default function Catalogue(props) {

    const { page } = props;

    const headerButton = [
        {
            label: "Edit",
            leftIconDefault: <EditIcon stroke="#FFFFFF" />,
            leftIconClick: <EditIcon stroke="#C4BEED" />,
            onClick: () => {
                handleClickEdit();
            },
        },
        { label: "Re-arrange", leftIconDefault: <ReArrangeIcon stroke="#FFFFFF" />, leftIconClick: <ReArrangeIcon stroke="#C4BEED" />, onClick: () => {} },
        { label: "Add-on groups", onClick: () => {} },
        page === "outlet-menu"
            ? {
                  label: "Manage menu",
                  hasPopup: true,
                  leftIconDefault: <MenuIcon stroke="#FFFFFF" />,
                  leftIconClick: <MenuIcon stroke="#C4BEED" />,
                  onClick: () => {
                      showHideSelectCategoryPopup();
                  },
              }
            : {
                  label: "Create preset",
                  hasPopup: true,
                  leftIconDefault: <AddIcon stroke="#FFFFFF" />,
                  leftIconClick: <AddIcon stroke="#C4BEED" />,
                  onClick: () => {
                      handleClickCretePreset();
                  },
              },
    ];

    const showHideSelectCategoryPopup = () => {
        setShowSelectCategoryPopup(!showSelectCategoryPopup);
    };

    const categoriesDetails = [
        {
            title: "Sides",
            dishes: "21 dishes",
            img: pizza1,
            type: "veg-nonVeg",
            subCategoryDetails: [
                {
                    title: "Garlic Bread",
                    dishes: "09 dishes",
                    img: pizza2,
                    type: "nonVeg",
                },
                {
                    title: "French Fries Text Extended",
                    dishes: "12 dishes",
                    img: pizza3,
                    type: "veg",
                },
            ],
        },

        {
            title: "Pizza",
            dishes: "32 dishes",
            img: pizza4,
            type: "veg-nonVeg",
        },
        {
            title: "Burger",
            dishes: "03 dishes",
            img: pizza5,
            type: "veg-nonVeg",
        },
        {
            title: "Shakes",
            dishes: "07 dishes",
            img: pizza6,
            type: "veg-nonVeg",
        },
        {
            title: "Salad",
            dishes: "10 dishes",
            img: pizza7,
            type: "veg-nonVeg",
        },
        {
            title: "Pastries",
            dishes: "10 dishes",
            img: pizza8,
            type: "veg-nonVeg",
        },
        {
            title: "Pastries",
            dishes: "10 dishes",
            img: pizza8,
            type: "veg-nonVeg",
        },
    ];

    const dishesGarlicBreadDetails = [
        {
            title: "Garlic Breadsticks",
            variants: "No",
            position: "left",
        },
        {
            title: "Classic Stuffed Garlic Bread",
            variants: "Yes (2)",
            position: "middle",
        },
        {
            title: "Paneer Tikka Stuffed Garlic Bread",
            variants: "Yes (2)",
            position: "right",
        },
    ];

    const dishesFriesDetails = [
        {
            title: "Crinkle Fries",
            variants: "No",
        },
        {
            title: "Piri Piri French Fries",
            variants: "Yes (4)",
        },
    ];

    const dishDetailsList = ["Basic details", "Expose dish", "Dish pricing", "Badges", "Link add-on groups", "Dish timing", "Up-sell items", "Up-sell current dish in cart"];

    const friesDishes = [
        { title: "Crinkle Fries", variants: "No" },
        { title: "Piri Piri French Fries", variants: "Yes(2)" },
        { title: "Cheese Garlic French Fries", variants: "Yes(3)" },
        { title: "Tika Tika Masala Fries", variants: "Yes(3)" },
    ];

    const pizzaDishes = [
        {
            title: "Mexican Green Wave Pizza Text Extended",
            variants: "Yes(3)",
            image: pizza7,
        },
        {
            title: "Peppy Paneer",
            variants: "No",
        },
        {
            title: "Deluxe Veggie",
            variants: "Yes(3)",
        },
        {
            title: "Paneer Makhani",
            variants: "Yes(3)",
            image: pizza8,
        },
        {
            title: "Cheese n Tomato",
            variants: "Yes(3)",
        },
        {
            title: "Veg Extraveganza",
            variants: "Yes(3)",
            image: pizza9,
        },
        {
            title: "Veg Extraveganza",
            variants: "Yes(3)",
            image: pizza9,
        },
    ];

    const handleClickCategory = (categoryName) => {
        setActiveCategory(categoryName);
    };

    const [activeCategory, setActiveCategory] = useState(categoriesDetails[0].title);

    const [activeDish, setActiveDish] = useState(pizzaDishes[0].title);

    const handleClickDish = (dishName) => {
        setActiveDish(dishName);
    };
    const [activeDetails, setActiveDetails] = useState("Dish details");

    const handleChangeDropDown = (item) => {
        setActiveDetails(item);
    };

    const [showCretePreset, setShowCretePreset] = useState(false);

    const handleClickCretePreset = () => {
        setShowCretePreset(!showCretePreset);
    };

    const [showSelectCategoryPopup, setShowSelectCategoryPopup] = useState(false);

    const handleClickProceed = () => {
        setShowSelectCategoryPopup(!showSelectCategoryPopup);
        setShowCretePreset(!showCretePreset);
    };

    const navigate = useNavigate();

    const handleClickAddCategory = () => {
        navigate("/menu/add-category");
    };

    const handleClickAddDish = () => {
        navigate("/menu/add-dish");
    };

    const handleClickEditCategoryDish = () => {
        navigate("/menu/add-dish", { state: true });
    };

    const [editMenu, setEditMenu] = useState(false);

    const handleClickEdit = () => {
        setEditMenu(!editMenu);
    };

    return (
        <>
            <div className="flex flex-row justify-between mb-6 xl:block md:mb-4">
                <div className="flex flex-row items-center xl:mb-4 md:mb-1">
                    <div className="min-w-[375px] mr-4 w-full pageContentSmall:min-w-[330px] xl:min-w-0 xl:max-w-[298px] pageContentSmall:mr-2 md:max-w-full">
                        <DefaultInputField placeholder="Search category or dish" placeholderIcon={<SearchIcon stroke="#D3D2D8" />} shadow="shadow-shadowXsmall" />
                    </div>

                    <div className="max-w-[156px] md:hidden">
                        <LargePrimaryButton name="Bulk select" leftIconDefault={<SelectIcon stroke="#ffffff" />} leftIconClick={<SelectIcon stroke="#C4BEED" />} />
                    </div>

                    <div className="hidden min-w-[64px] md:block">
                        <LargePrimaryButton leftIconDefault={<SelectIcon stroke="#ffffff" />} leftIconClick={<SelectIcon stroke="#C4BEED" />} />
                    </div>
                </div>

                <div className="flex flex-row items-center xl:justify-end md:block">
                    {headerButton.map((el, index) => (
                        <div className="ml-4 pageContentSmall:ml-2 md:ml-0 md:inline-block md:w-1/2 odd:md:pr-2 even:md:pl-2 md:mt-2 md:align-top cursor-pointer" onClick={el.onClick} key={index}>
                            <LargePrimaryButton name={el.label} leftIconClick={el.leftIconClick} isDefault={el.hasPopup && false} leftIconDefault={el.leftIconDefault} hideName="mobile:text-sm" />
                        </div>
                    ))}
                </div>
            </div>

            <div className="flex flex-row items-start xl:block">
                <div className="flex justify-between md:block">
                    <div className="pr-8 mr-8 border-r border-neutral-300 lg:pr-4 lg:ml-4 md:border-0 md:ml-0 md:pr-0 md:mr-0 md:mb-4">
                        <div className="flex flex-row justify-between w-full items-center md:pb-2">
                            <span className="paragraphLargeMedium text-[#000000]">Categories</span>
                            <div className="max-w-[131px] cursor-pointer" onClick={handleClickAddCategory}>
                                <SmallPrimaryButton
                                    leftIconClick={<AddIcon stroke="#C4BEED" height={20} width={20} />}
                                    leftIconDefault={<AddIcon stroke="#FFFFFF" height={20} width={20} />}
                                    name="Add category"
                                />
                            </div>
                        </div>

                        {categoriesDetails.map((el, index) => {
                            return (
                                <div className={`mt-4 w-full max-w-[303px] md:max-w-full md:mt-2 relative ${editMenu && "md:pl-2.5"}`} key={index}>
                                    <Categories
                                        dropDownCategoryMarginL="ml-4 lg:ml-2"
                                        minWidth="min-w-[303px]"
                                        img={el.img}
                                        title={el.title}
                                        dishes={el.dishes}
                                        type={el.type}
                                        subCategoryDetails={el.subCategoryDetails}
                                        page="menuManagement"
                                        imageSize="h-[78px] w-[78px]"
                                        isActive={el.title === activeCategory}
                                        handleClickCategory={handleClickCategory}
                                        activeCategory={activeCategory}
                                        isEdit={editMenu}
                                        handleCLickEdit={handleClickEditCategoryDish}
                                    />
                                </div>
                            );
                        })}
                    </div>

                    <div className="max-w-[443px] w-full xl:max-w-full mr-8 pr-8 pageContentSmall:mr-4 pageContentSmall:pr-4 border-r border-neutral-300 xl:mr-0 xl:pr-0 xl:border-none">
                        <div className="flex flex-row justify-between items-center w-full">
                            <span className="paragraphLargeMedium text-[#000000]">Dishes</span>
                            <div className="max-w-[131px] cursor-pointer" onClick={handleClickAddDish}>
                                <SmallPrimaryButton
                                    leftIconClick={<AddIcon stroke="#C4BEED" height={20} width={20} />}
                                    leftIconDefault={<AddIcon stroke="#FFFFFF" height={20} width={20} />}
                                    name="Add dish"
                                />
                            </div>
                        </div>

                        <div className="mt-2">
                            <span className="paragraphLargeItalic text-neutral-500">Garlic Bread</span>
                        </div>

                        <div className="mb-4">
                            {dishesGarlicBreadDetails.map((el, index) => {
                                return (
                                    <div className={`mt-4 h-[104px] md:h-full min-w-[411px] lg:min-w-0 md:mt-2 ${editMenu && "md:pl-2.5"}`} key={index}>
                                        <Dishes title={el.title} variants={el.variants} position={el.position} page="menuManagement" isEdit={editMenu} />
                                    </div>
                                );
                            })}
                        </div>

                        <div className="">
                            <span className="paragraphLargeItalic text-neutral-500">French Fries Text Extended</span>
                        </div>

                        {friesDishes.map((el, index) => (
                            <div className={`mt-4 min-w-[411px] md:mt-2 lg:min-w-0 ${editMenu && "md:pl-2.5"}`} key={index}>
                                <Dishes title={el.title} variants={el.variants} page="menuManagement" isEdit={editMenu} />
                            </div>
                        ))}

                        {/* it can be rendered as array refactoring */}

                        {/* {pizzaDishes.map((el, index) => (
                            <div className="mt-4 min-w-[411px]" key={index}>
                                <Dishes
                                    titleStyle="headingH6MediumDesktop"
                                    handleCLickEdit={handleClickEditCategoryDish}
                                    isEdit={editMenu}
                                    title={el.title}
                                    image={el.image}
                                    variants={el.variants}
                                    page="menuManagement"
                                    handleClickDish={handleClickDish}
                                    isActive={activeDish === el.title}
                                    boxHeight="h-[104px]"
                                />
                            </div>
                        ))} */}
                    </div>
                </div>

                <div className="max-w-[430px] w-full xl:max-w-full xl:border-t xl:border-neutral-300 xl:mt-8 xl:pt-6 md:mt-5 md:pt-5">
                    <div className="flex flex-row justify-between items-center w-full pt-0.5 mb-4 md:mb-2 md:pt-0">
                        <span className="paragraphLargeMedium">View details</span>
                        <DetailsDropDown handleChangeDropDown={handleChangeDropDown} />
                    </div>

                    {activeDetails === "Dish details" ? (
                        dishDetailsList.map((el, index) => (
                            <div className="mt-4 md:mt-2" key={index}>
                                <DishDetailsList
                                    title={el}
                                    category={activeCategory}
                                    dishTitleEnglish={activeDish}
                                    image={pizza9}
                                    interName="Paneer Makhani 122-212"
                                    dishTitleGujarati=" પનીર મખાણી"
                                    descriptionEnglish="Flavorful trio of juicy paneer, crisp capsicum with spicy red paprika"
                                    descriptionGujarati="રસદાર પનીર, મસાલેદાર લાલ પૅપ્રિકા સાથે ક્રિસ્પ કેપ્સિકમની સ્વાદિષ્ટ ત્રિપુટી"
                                />
                            </div>
                        ))
                    ) : (
                        <div className="">
                            <div className="mb-4 md:mb-2">
                                <DishDetailsList
                                    title="Category details"
                                    category={activeCategory}
                                    dishTitleEnglish={activeDish}
                                    image={pizza9}
                                    interName="Paneer Makhani 122-212"
                                    dishTitleGujarati=" પનીર મખાણી"
                                    descriptionEnglish="Flavorful trio of juicy paneer, crisp capsicum with spicy red paprika"
                                    descriptionGujarati="રસદાર પનીર, મસાલેદાર લાલ પૅપ્રિકા સાથે ક્રિસ્પ કેપ્સિકમની સ્વાદિષ્ટ ત્રિપુટી"
                                    dishSizeEnglish="Regular"
                                    dishSizeGujarati="નિયમિત"
                                    dishServesEnglish="1 person"
                                    dishServeGujarati="૧ વ્યક્તિ"
                                    tilePOsition="Top left"
                                    theme="Full-size image"
                                />
                            </div>

                            <div className="flex flex-row mb-4 md:block md:mb-0">
                                <div className="flex flex-row items-center mr-4 cursor-pointer md:mb-2">
                                    <AddIcon stroke="#6C5DD3" height={24} />
                                    <span className="paragraphMediumMedium text-primary-500 ml-0.5">Add sub-category</span>
                                </div>
                                <div className="flex flex-row items-center cursor-pointer md:mb-2">
                                    <AddIcon stroke="#6C5DD3" height={24} />
                                    <span className="paragraphMediumMedium text-primary-500 ml-0.5">Map item</span>
                                </div>
                            </div>

                            <div className="flex flex-row md:block">
                                <div className="flex flex-row items-center mr-4 cursor-pointer md:mb-2">
                                    <AddIcon stroke="#6C5DD3" height={24} />
                                    <span className="paragraphMediumMedium text-primary-500 ml-0.5">Copy existing item</span>
                                </div>
                                <div className="flex flex-row items-center cursor-pointer">
                                    <AddIcon stroke="#6C5DD3" height={24} />
                                    <span className="paragraphMediumMedium text-primary-500 ml-0.5">Move existing item</span>
                                </div>
                            </div>
                        </div>
                    )}
                </div>
            </div>

            <div className={`${!showCretePreset && "hidden"}`}>
                <CreatePresetPopupStep1 handleClickProceed={handleClickProceed} handleClickClose={handleClickCretePreset} />
            </div>

            <div className={`${!showSelectCategoryPopup && "hidden"}`}>
                <CreatePresetPopupStep2 handleClickClose={page ? showHideSelectCategoryPopup : handleClickProceed} categoriesDetails={categoriesDetails} pizzaDishes={pizzaDishes} page={page} />
            </div>
        </>
    );
}
