import { Menu, Transition } from "@headlessui/react";
import { Fragment, useState } from "react";
import { ReactComponent as DropDownIcon } from "../../../../Assets/chevron-down.svg";

export default function DetailsDropDown(props) {
    const [label, setLabel] = useState("Dish details");

    const handleClickItem = (item) => {
        props.handleChangeDropDown(item);
        setLabel(item);
    };

    return (
        <div className="dropDownIcon">
            <Menu as="div" className="relative inline-block text-left">
                <div>
                    <Menu.Button className="paragraphMediumRegular inline-flex items-center text-primary-500 w-full justify-center rounded-md hover:bg-opacity-30 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75">
                        {label}
                        <span className="-mr-1.5">
                            <DropDownIcon className="dropDownIconRotate" height={16} fill="#6C5DD3" />
                        </span>
                    </Menu.Button>
                </div>
                <Transition
                    as={Fragment}
                    enter="transition ease-out duration-100"
                    enterFrom="transform opacity-0 scale-95"
                    enterTo="transform opacity-100 scale-100"
                    leave="transition ease-in duration-75"
                    leaveFrom="transform opacity-100 scale-100"
                    leaveTo="transform opacity-0 scale-95"
                >
                    <Menu.Items className="absolute shadow-shadowMedium right-0 mt-2.5 p-4 w-full min-w-[164px] origin-top-right rounded-md bg-white ring-1 ring-black ring-opacity-5 focus:outline-none paragraphSmallMedium">
                        <div className="pb-4 cursor-pointer" onClick={() => handleClickItem("Dish details")}>
                            <Menu.Item>
                                <span className="hover:text-primary-500">Dish details</span>
                            </Menu.Item>
                        </div>
                        <div className="cursor-pointer" onClick={() => handleClickItem("Category details")}>
                            <Menu.Item>
                                <span className="hover:text-primary-500">Category details</span>
                            </Menu.Item>
                        </div>
                    </Menu.Items>
                </Transition>
            </Menu>
        </div>
    );
}
