import React, { useState } from "react";

export default function DishDetailsList(props) {
    const {
        title,
        image,
        category,
        interName,
        dishTitleEnglish,
        dishTitleGujarati,
        descriptionEnglish,
        descriptionGujarati,
        dishSizeEnglish,
        dishSizeGujarati,
        dishServesEnglish,
        dishServeGujarati,
        tilePOsition,
        theme,
        // categoryVisibility,
    } = props;

    const categoryVisibility = ["Dine-in", "Delivery", "Takeaway", "QSR"];

    const [showDetails, setShowDetails] = useState(false);

    const handleClickCard = () => {
        setShowDetails(title === "Basic details" || title === "Category details" ? !showDetails : false);
    };

    return (
        <>
            <div className="border border-neutral-300 px-4 py-[15px] rounded-lg w-full ">
                <div className="flex flex-row justify-between cursor-pointer" onClick={handleClickCard}>
                    <span className="paragraphMediumMedium">{title}</span>
                    <span className="paragraphMediumRegular text-primary-500 cursor-pointer">{!showDetails ? "Show" : "Hide"}</span>
                </div>

                <div className={`${!showDetails && "hidden"} mt-2`}>
                    <div className="flex flex-row items-start mb-2">
                        <img src={image} alt="" className="w-[144px] h-[96px] mr-3 rounded" />
                        <div className="flex flex-col">
                            <div className="flex flex-row mb-4">
                                <span className="paragraphSmallRegular text-neutral-500 mr-2">Category:</span>
                                <span className="paragraphSmallRegular">{category}</span>
                            </div>
                            <div className="flex flex-row mr-4">
                                <span className="paragraphSmallRegular text-neutral-500 mr-2 min-w-[94px]">Internal name:</span>
                                <span className="paragraphSmallRegular break-words">{interName}</span>
                            </div>
                        </div>
                    </div>

                    <div className="">
                        <div className="flex flex-row items-start mb-4">
                            <span className="paragraphSmallRegular text-neutral-500 mr-2">Dish title:</span>
                            <div className="flex flex-col items-start">
                                <span className="paragraphSmallRegular mb-2">{dishTitleEnglish}</span>
                                <span className="paragraphSmallRegular">{dishTitleGujarati}</span>
                            </div>
                        </div>
                        <div className="flex flex-row items-start">
                            <span className="paragraphSmallRegular text-neutral-500 mr-2">Description:</span>
                            <div className="flex flex-col items-start">
                                <span className="paragraphSmallRegular mb-2">{descriptionEnglish}</span>
                                <span className="paragraphSmallRegular">{descriptionGujarati}</span>
                            </div>
                        </div>

                        <div className={`${title !== "Category details" && "hidden"}`}>
                            <div className="flex flex-row items-start my-4">
                                <span className="paragraphSmallRegular text-neutral-500 mr-2">Dish size:</span>
                                <div className="flex flex-col items-start">
                                    <span className="paragraphSmallRegular mb-2">{dishSizeEnglish}</span>
                                    <span className="paragraphSmallRegular">{dishSizeGujarati}</span>
                                </div>
                            </div>

                            <div className="flex flex-row items-start mb-4">
                                <span className="paragraphSmallRegular text-neutral-500 mr-2">Dish serves:</span>
                                <div className="flex flex-col items-start">
                                    <span className="paragraphSmallRegular mb-2">{dishServesEnglish}</span>
                                    <span className="paragraphSmallRegular">{dishServeGujarati}</span>
                                </div>
                            </div>

                            <div className="flex flex-row items-center mb-4">
                                <span className="paragraphSmallRegular text-neutral-500 mr-2">Tile position:</span>
                                <span className="paragraphSmallRegular">{tilePOsition}</span>
                            </div>

                            <div className="flex flex-row items-center mb-2">
                                <span className="paragraphSmallRegular text-neutral-500 mr-2">Theme:</span>
                                <span className="paragraphSmallRegular">{theme}</span>
                            </div>

                            <div className="flex flex-row items-start">
                                <span className="paragraphSmallRegular text-neutral-500 mr-2 mt-2">Category visibility:</span>
                                <div className="flex flex-col items-start">
                                    {categoryVisibility.map((el, index) => (
                                        <span className="paragraphSmallRegular mt-2" key={index}>
                                            {el}
                                        </span>
                                    ))}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
