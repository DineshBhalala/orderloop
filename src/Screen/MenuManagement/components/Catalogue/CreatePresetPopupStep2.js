import React, { useState } from "react";
import { ReactComponent as CloseIcon } from "../../../../Assets/close.svg";
import { ReactComponent as LeftArrowIcon } from "../../../../Assets/chevron-down.svg";
import { DefaultInputField } from "../../../../Components/InputField/InputField";
import { LargeDestructiveButton, LargePrimaryButton } from "../../../../Components/Buttons/Button";
import { CheckBoxWithoutLabels } from "../../../../Components/FormControl/FormControls";
import { Categories, Dishes } from "../../../ItemAvailability/Components/card";

export default function CreatePresetPopupStep2(props) {
    const { handleClickClose, categoriesDetails, pizzaDishes, page } = props;

    const [showDishesPopup, setShowDishesPopup] = useState(true);

    const handleClickDishes = () => {
        showDishesPopup && setShowDishesPopup(!showDishesPopup);
    };

    const handleClickBack = () => {
        showDishesPopup ? handleClickClose() : setShowDishesPopup(!showDishesPopup);
    };

    return (
        <div className={`fixed bg-black bg-opacity-50 inset-0 z-[101] flex py-[18px] md:py-0 md:absolute md:z-[9] md:top-0 md:block md:overflow-hidden`}>
            <div className="max-w-[722px] w-full rounded-xl bg-shades-50 pt-6 pb-6 px-8 m-auto h-full max-h-[989px] md:max-w-full md:max-h-full md:rounded-none md:py-4 md:px-4 overflow-auto">
                <div className="flex flex-row items-center justify-between mb-6 md:hidden">
                    <div>
                        <h3 className="paragraphLargeMedium">Select categories and dishes</h3>
                        <div className="flex flex-row items-center">
                            <p className="paragraphMediumItalic text-neutral-500">Select a set of categories and dishes you would like to include in the preset</p>
                        </div>
                    </div>

                    <span className="cursor-pointer" onClick={handleClickClose}>
                        <CloseIcon />
                    </span>
                </div>

                <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 px-8 md:px-0 cursor-pointer" onClick={handleClickBack}>
                    <LeftArrowIcon className="rotate-90" />
                    <span className="ml-1">Back to {showDishesPopup ? "catalogue presets" : "categories"}</span>
                </div>

                <div className="mb-[17px] md:hidden">
                    <DefaultInputField
                        placeholder="Menu - 007"
                        Addon="Change"
                        boxHeight="[52px]"
                        shadow="shadow-smallDropDownShadow"
                        placeholderTextColor="neutral-900"
                        addonStyle="text-primary-500 paragraphMediumUnderline "
                    />
                </div>

                <div className="flex flex-row items-start mb-12 md:flex-col-reverse">
                    <div className={`w-[333px] border-r border-neutral-300 pr-[14px] md:pr-0 md:w-full md:border-0 ${!showDishesPopup && "md:hidden"}`}>
                        <div className="flex flex-row items-center mb-4">
                            <span className="mr-1 text-black paragraphLargeMedium">Categories</span>
                            <span className="paragraphSmallItalic text-neutral-500">(Selected - 00) </span>
                        </div>

                        <div className="flex flex-row items-center mb-[18px]">
                            <CheckBoxWithoutLabels />
                            <span className="paragraphSmallMedium ml-2">Select/deselect all</span>
                        </div>

                        <div className="overflow-auto h-[616px] scrollbarStyle md:overflow-hidden md:h-full md:mb-4">
                            {categoriesDetails.map((el, index) => {
                                return (
                                    <div className={`${index !== categoriesDetails.length - 1 && "mb-2"} pr-3 w-full md:max-w-full md:mt-2 md:pr-0 md:mb-0`} key={index}>
                                        <Categories
                                            img={el.img}
                                            title={el.title}
                                            dishes={el.dishes}
                                            minWidth="min-w-[273px] md:min-w-0"
                                            type={el.type}
                                            imageSize="h-[74px] w-[74px]"
                                            isFromSelection={true}
                                            imageMargin="my-2.5"
                                            subCategoryDetails={el.subCategoryDetails}
                                            page="menuManagement"
                                            dropDownCategoryMarginL="ml-2"
                                            // 😇😌😜
                                            dishNumberTextStyle="paragraphXSmallItalic"
                                            titleStyle="paragraphSmallMedium mb-1"
                                            contentMarginL="ml-1"
                                            boxPadding="px-3"
                                            dropdownIconSize="max-w-[16px]"
                                        />
                                    </div>
                                );
                            })}
                        </div>
                    </div>

                    <div className="w-full ml-6 md:ml-0">
                        <div className="flex flex-row items-center mb-4 md:cursor-pointer" onClick={handleClickDishes}>
                            <span className="mr-1 text-black paragraphLargeMedium">Dishes</span>
                            <span className="paragraphSmallItalic text-neutral-500">(Selected - 00) </span>
                        </div>

                        <div className={`${showDishesPopup && "md:hidden"}`}>
                            <div className="flex flex-row items-center mb-[18px]">
                                <CheckBoxWithoutLabels />
                                <span className="paragraphSmallMedium ml-2">Select/deselect all</span>
                            </div>

                            <div className="overflow-auto h-[616px] scrollbarStyle -mr-[18px] md:mr-0">
                                {pizzaDishes.map((el, index) => (
                                    <div className={`${index !== pizzaDishes.length - 1 && "mb-2"} pr-3 md:pr-0`} key={index}>
                                        <Dishes
                                            boxHeight="h-[96px]"
                                            title={el.title}
                                            variants={el.variants}
                                            image={el.image}
                                            page="menuManagement"
                                            imageSize="h-[74px] w-[74px]"
                                            boxPadding="py-2.5"
                                            isFromSelection={true}
                                            titleStyle="paragraphSmallMedium"
                                        />
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>
                </div>

                <div className={`flex flex-row items-center justify-between w-full ${page && "hidden"}`}>
                    <span className="paragraphLargeMedium">Step 2/2</span>
                    <div className="max-w-[156px] w-full">
                        <LargePrimaryButton name="Proceed" />
                    </div>
                </div>

                <div className={`flex justify-end mt-12 md:fixed md:bottom-0 md:block md:w-full md:pb-1 md:pt-2 md:shadow-dropShadow md:bg-white md:-ml-4 md:px-4 ${!page && "hidden"}`}>
                    <div className="flex flex-row">
                        <div className="mr-2 md:w-full">
                            <LargeDestructiveButton name="Discard changes" />
                        </div>

                        <div className="ml-2 md:w-full">
                            <LargePrimaryButton name="Save changes" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
