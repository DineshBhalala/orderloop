import React from "react";
import { ReactComponent as CloseIcon } from "../../../../Assets/close.svg";
import { ReactComponent as LeftArrowIcon } from "../../../../Assets/chevron-down.svg";
import { DefaultInputField } from "../../../../Components/InputField/InputField";
import { LargePrimaryButton } from "../../../../Components/Buttons/Button";

export default function CreatePresetPopupStep1(props) {
    const { handleClickClose, handleClickProceed } = props;

    return (
        <div className={`fixed bg-black bg-opacity-50 inset-0 z-[101] flex md:absolute md:z-[9] md:top-0 md:block md:overflow-hidden`}>
            <div className="max-w-[475px] w-full rounded-xl bg-shades-50 pt-6 pb-9 px-8 m-auto max-h-[969px] md:max-w-full md:rounded-none md:py-4 md:px-4 md:h-full md:max-h-full md:overflow-hidden">
                <div className="flex flex-row items-center justify-between mb-6 md:hidden">
                    <div>
                        <h3 className="paragraphLargeMedium">Create preset</h3>
                        <div className="flex flex-row items-center">
                            <p className="paragraphMediumItalic text-neutral-500">Create a new preset by naming it.</p>
                        </div>
                    </div>
                    <span className="cursor-pointer" onClick={handleClickClose}>
                        <CloseIcon />
                    </span>
                </div>

                <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 px-8 md:px-0 cursor-pointer" onClick={handleClickClose}>
                    <LeftArrowIcon className="rotate-90" />
                    <span className="ml-1">Back to delivery charges</span>
                </div>

                <div className="mb-20">
                    <DefaultInputField label="Preset name" placeholder="Enter preset name" shadow="shadow-shadowXsmall" />
                </div>

                <div className="flex flex-row items-center justify-between w-full">
                    <span className="paragraphLargeMedium">Step 1/2</span>
                    <div className="max-w-[156px] w-full cursor-pointer" onClick={handleClickProceed}>
                        <LargePrimaryButton name="Proceed" isDefault={false} />
                    </div>
                </div>
            </div>
        </div>
    );
}
