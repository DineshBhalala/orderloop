import React, { useState } from "react";
import { Menu, Transition } from "@headlessui/react";
import { Fragment } from "react";
import { ReactComponent as DownArrow } from "../../../Assets/chevron-down.svg";
import { ReactComponent as SearchIcon } from "../../../Assets/search.svg";
import { CheckBoxWithoutLabels } from "../../../Components/FormControl/FormControls";

export default function SearchDropDown(props) {
    const { searchInputPlaceholder, label, type, menuItems } = props;
    const [boxLabel, setBoxLabel] = useState(null);
    const handleClickMenuItem = (menuItem) => {
        setBoxLabel(menuItem);
    };
    return (
        <>
            <label className={`paragraphMediumItalic flex ${props.labelColor ?? "text-neutral-500"}`}>{label}</label>
            <div className="mt-2">
                <Menu as="div" className="">
                    <div className="dropDownIcon">
                        <Menu.Button className="shadow-smallDropDownShadow flex flex-row justify-between w-full mobile:max-w-full rounded-md outline-none focus:border-primary-500 border py-[13px] focus:ring-4 focus:ring-primary-100 appearance-none px-4 border-neutral-300 paragraphSmallRegular mobile:px-1">
                            <div className={`${type === "upSell" && "hidden"}`}>
                                <p className={`paragraphSmallRegular text-neutral-300 pt-0.5 ${boxLabel !== null && "hidden"}`}>
                                    Select <span className="font-normal italic text-sm leading-5 ">add-on groups</span>
                                </p>
                                <p className={`${boxLabel === null && "hidden"} pt-0.5 text-left justify-start paragraphSmallRegular`}>{boxLabel}</p>
                            </div>
                            <div className={`${type !== "upSell" && "hidden"} pt-0.5 ${boxLabel !== null && "hidden"}`}>
                                <span className="paragraphSmallRegular text-neutral-300">Select dish</span>
                            </div>
                            <p className={`${(boxLabel === null || type !== "upSell") && "hidden"} pt-0.5 text-left justify-start paragraphSmallRegular`}>{boxLabel}</p>
                            <DownArrow className={`dropDownIconRotate ml-3`} fill={props.dropdownIconColor ?? "#131126"} height={24} width={24} />
                        </Menu.Button>
                    </div>
                    <Transition
                        as={Fragment}
                        enter="transition ease-out duration-100"
                        enterFrom="transform opacity-0 scale-95"
                        enterTo="transform opacity-100 scale-100"
                        leave="transition ease-in duration-75"
                        leaveFrom="transform opacity-100 scale-100"
                        leaveTo="transform opacity-0 scale-95"
                    >
                        <Menu.Items className="absolute mt-2 left-0 right-0 mobile:w-full md:max-w-full ring-1 ring-black ring-opacity-5 focus:outline-none px-4 py-2 border border-neutral-300 rounded-md shadow-shadowMedium bg-shades-50 z-50 paragraphSmallRegular">
                            <div className="w-full pt-1 mb-2 flex flex-row items-center">
                                <SearchIcon width={28} height={28} />
                                <input placeholder={searchInputPlaceholder} className="ml-2 placeholder:paragraphSmallRegular placeholder:text-neutral-300 outline-none w-full" />
                            </div>
                            {menuItems.map((el, index) => (
                                <div key={index} className="pt-2 mb-2 cursor-pointer relative h-8" onClick={() => handleClickMenuItem(el)}>
                                    <Menu.Item>
                                        <div className="ml-8 w-full flex flex-row items-center paragraphSmallRegular">{el}</div>
                                    </Menu.Item>

                                    <div className={`absolute mr-2 ${type !== "upSell" && "hidden"} top-[7px]`}>
                                        <CheckBoxWithoutLabels className="mr-2" />
                                    </div>
                                </div>
                            ))}
                        </Menu.Items>
                    </Transition>
                </Menu>
            </div>
        </>
    );
}
