import React from "react";
import { DefaultInputField } from "../../../../Components/InputField/InputField";
import { ReactComponent as SearchIcon } from "../../../../Assets/search.svg";
import { ReactComponent as SelectIcon } from "../../../../Assets/select.svg";
import { ReactComponent as MenuIcon } from "../../../../Assets/menu.svg";
import { LargePrimaryButton } from "../../../../Components/Buttons/Button";
import PaginationWithNumber from "../../../../Components/Pagination/PaginationWithNumber";
import { useNavigate } from "react-router-dom";
import ListViewOutletMenu from "../../../../Components/ListView/ListViewOutletMenu";

export default function OutletMenu() {
    const tableDetails = [
        {
            outletName: "Domino's Pizza - Tagore Road Outlet",
            state: "Gujarat",
            city: "Rajkot",
            address: "Shyamal Infinity, beneath Radio Mirchi, kalawad road",
            presetAttached: "Menu - 007",
            categories: "12",
            dishes: "128",
        },
        {
            outletName: "Domino's Pizza - University Road Outlet",
            state: "Gujarat",
            city: "Rajkot",
            address: "Shyamal Infinity, beneath Radio Mirchi, kalawad road",
            presetAttached: "Medium Scale Menu - 006",
            categories: "08",
            dishes: "96",
        },
        {
            outletName: "Domino's Pizza - Ahmedabad Road Outlet",
            state: "Gujarat",
            city: "Rajkot",
            address: "Shyamal Infinity, beneath Radio Mirchi, kalawad road",
            presetAttached: "Medium Scale Menu - 005",
            categories: "08",
            dishes: "120",
        },
        {
            outletName: "Domino's Pizza - Jamnagar Road Outlet",
            state: "Gujarat",
            city: "Rajkot",
            address: "Shyamal Infinity, beneath Radio Mirchi, kalawad road",
            presetAttached: "Medium Scale Menu - 004",
            categories: "03",
            dishes: "36",
        },
    ];
    const navigate = useNavigate();

    const handleClickOutletName = () => {
        navigate("/menu/outlet-menu", { state: { page: "outlet-menu" } });
    };
    return (
        <>
            <div className="flex flex-row mb-6 md:mb-0 md:pb-2">
                <div className="w-full max-w-[375px] mr-2 md:max-w-full">
                    <DefaultInputField placeholder="Search outlet" placeholderIcon={<SearchIcon stroke="#D3D2D8" />} />
                </div>
                <div className="ml-2 md:min-w-[64px]">
                    <LargePrimaryButton name="Bulk select" leftIconDefault={<SelectIcon stroke="#FFFFFF" />} leftIconClick={<SelectIcon stroke="#C4BEED" />} hideName="md:hidden" />
                </div>
            </div>

            <div className="w-full rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden border-neutral-300 border mb-4">
                <table className="w-full break-words tableMediaLibrary">
                    <thead>
                        <tr className="shadow-innerShadow bg-neutral-50 text-left justify-center h-11">
                            <th className="px-6 min-w-[289px] paragraphOverlineSmall text-neutral-700">OUTLET NAME</th>
                            <th className="px-6 min-w-[132px] paragraphOverlineSmall text-neutral-700">STATE</th>
                            <th className="px-6 min-w-[109px] paragraphOverlineSmall text-neutral-700">CITY</th>
                            <th className="px-6 min-w-[284px] paragraphOverlineSmall text-neutral-700">ADDRESS</th>
                            <th className="px-6 min-w-[223px] paragraphOverlineSmall text-neutral-700">PRESET ATTACHED</th>
                            <th className="px-6 min-w-[233px] paragraphOverlineSmall text-neutral-700">CATEGORIES AND DISHES</th>
                        </tr>
                    </thead>
                    <tbody>
                        {tableDetails.map((el, index) => (
                            <tr className={`paragraphSmallRegular ${index !== 0 && "border-t"} border-neutral-300 justify-center h-[70px]`} key={index}>
                                <td className="px-6 cursor-pointer" onClick={handleClickOutletName}>
                                    {el.outletName}
                                </td>
                                <td className="px-6">{el.state}</td>
                                <td className="px-6">{el.city}</td>
                                <td className="px-6">{el.address}</td>
                                <td className="px-6">{el.presetAttached}</td>
                                <td className="px-6">
                                    <div className="flex flex-col">
                                        <div className="flex flex-row items-center mb-1">
                                            <MenuIcon />
                                            <span className="paragraphSmallRegular ml-2">Categories - {el.categories}</span>
                                        </div>
                                        <div className="flex flex-row items-center">
                                            <MenuIcon />
                                            <span className="paragraphSmallRegular ml-2">Dishes - {el.dishes}</span>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>

            <div className="hidden md:block">
                {tableDetails.map((el, index) => (
                    <div className="mt-2" key={index}>
                        <ListViewOutletMenu
                            outletName={el.outletName}
                            state={el.state}
                            city={el.city}
                            address={el.address}
                            presetAttached={el.presetAttached}
                            categories={el.categories}
                            dishes={el.dishes}
                            handleClickViewDetails={handleClickOutletName}
                        />
                    </div>
                ))}
            </div>

            <div className="md:hidden">
                <PaginationWithNumber />
            </div>
        </>
    );
}
