import React from "react";
import { DefaultBreadcrumbs } from "../../Components/Breadcrumbs/Breadcrumbs";
import { ReactComponent as MenuIcon } from "../../Assets/menu.svg";
import { ReactComponent as LeftArrow } from "../../Assets/chevron-down.svg";
import { ReactComponent as AddIcon } from "../../Assets/add.svg";
import DishDetailsList from "./Components/Catalogue/DishDetailsList";

export default function MenuManagementDishDetails() {
    const dishDetailsList = ["Basic details", "Expose dish", "Dish pricing", "Badges", "Link add-on groups", "Dish timing", "Up-sell dishes", "Up-sell current dish in cart"];

    return (
        <>
            <div className="bg-[#fafafa]">
                <div className="px-8 pb-[70px] lg:px-4 lg:pb-[23px] pt-4 w-full max-w-[1336px] mx-auto bg-white">
                    <div className="mb-4 md:hidden">
                        <DefaultBreadcrumbs mainTab="Menu management" tab1="Dish details" icon={<MenuIcon height={20} />} />
                    </div>

                    <div className="flex flex-row items-center justify-between w-full mb-4 md:hidden">
                        <span className="paragraphLargeMedium">View details</span>
                        <span className="paragraphLargeMedium">Garlic breadsticks details</span>
                    </div>

                    <div className="hidden md:block">
                        <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer">
                            <LeftArrow className="rotate-90" />
                            <span className="pl-1">Back to outlet dishes</span>
                        </div>
                    </div>

                    {dishDetailsList.map((el, index) => (
                        <div className="mt-4" key={index}>
                            <DishDetailsList
                                title={el}
                                dishTitleEnglish="Paneet makhni"
                                interName="Paneer Makhani 122-212"
                                dishTitleGujarati=" પનીર મખાણી"
                                descriptionEnglish="Flavorful trio of juicy paneer, crisp capsicum with spicy red paprika"
                                descriptionGujarati="રસદાર પનીર, મસાલેદાર લાલ પૅપ્રિકા સાથે ક્રિસ્પ કેપ્સિકમની સ્વાદિષ્ટ ત્રિપુટી"
                            />
                        </div>
                    ))}

                    <div className="mt-4">
                        <div className="mb-2">
                            <DishDetailsList
                                title="Category details"
                                interName="Paneer Makhani 122-212"
                                dishTitleGujarati=" પનીર મખાણી"
                                descriptionEnglish="Flavorful trio of juicy paneer, crisp capsicum with spicy red paprika"
                                descriptionGujarati="રસદાર પનીર, મસાલેદાર લાલ પૅપ્રિકા સાથે ક્રિસ્પ કેપ્સિકમની સ્વાદિષ્ટ ત્રિપુટી"
                                dishSizeEnglish="Regular"
                                dishSizeGujarati="નિયમિત"
                                dishServesEnglish="1 person"
                                dishServeGujarati="૧ વ્યક્તિ"
                                tilePOsition="Top left"
                                theme="Full-size image"
                            />
                        </div>

                        <div className="flex flex-row items-center mr-4 cursor-pointer mb-2">
                            <AddIcon stroke="#6C5DD3" height={24} />
                            <span className="paragraphMediumMedium text-primary-500 ml-0.5">Add sub-category</span>
                        </div>

                        <div className="flex flex-row items-center cursor-pointer mb-2">
                            <AddIcon stroke="#6C5DD3" height={24} />
                            <span className="paragraphMediumMedium text-primary-500 ml-0.5">Map item</span>
                        </div>

                        <div className="flex flex-row items-center mr-4 cursor-pointer mb-2">
                            <AddIcon stroke="#6C5DD3" height={24} />
                            <span className="paragraphMediumMedium text-primary-500 ml-0.5">Copy existing item</span>
                        </div>

                        <div className="flex flex-row items-center cursor-pointer mb-2">
                            <AddIcon stroke="#6C5DD3" height={24} />
                            <span className="paragraphMediumMedium text-primary-500 ml-0.5">Move existing item</span>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
