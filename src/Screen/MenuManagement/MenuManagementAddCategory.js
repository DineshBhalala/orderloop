import React from "react";
import { DefaultBreadcrumbs } from "../../Components/Breadcrumbs/Breadcrumbs";
import { ReactComponent as MenuIcon } from "../../Assets/menu.svg";
import { ReactComponent as LeftArrow } from "../../Assets/chevron-down.svg";
import Header from "../Setting/Components/Header";
import { DefaultInputField, InputArea } from "../../Components/InputField/InputField";
import { LargeDestructiveButton, LargePrimaryButton } from "../../Components/Buttons/Button";
import mobile from "../../Assets/mobile.png";
import { useNavigate } from "react-router-dom";

export default function MenuManagementAddCategory() {
    const navigate = useNavigate();

    const handleClickClose = () => {
        navigate("/menu");
    };

    return (
        <>
            <div className="bg-[#fafafa]">
                <div className="px-8 lg:px-4 pt-4 w-full max-w-[1336px] mx-auto bg-white relative md:max-w-full pb-20">
                    <div className="mb-4 md:hidden">
                        <DefaultBreadcrumbs mainTab="Menu management" icon={<MenuIcon height={20} />} tab1="Add category" />
                    </div>
                    <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer" onClick={handleClickClose}>
                        <LeftArrow className="rotate-90" />
                        <span className="ml-1">Back to catalogue</span>
                    </div>
                    <div className="flex flex-row xl:block -mb-20 xl:mb-0">
                        <div className="min-w-[747px] w-full pr-[110px] border-r border-neutral-300 xl:pr-0 xl:pb-4 xl:border-r-0 xl:border-b xl:mb-6 xl:min-w-full pb-20 md:pb-2">
                            <div className="mb-6">
                                <Header
                                    title="Category image"
                                    headerBottomLine="Select a category image that will be displayed with the category name in the mobile application."
                                    page="menuManagement"
                                />
                            </div>

                            <div className="flex flex-row paragraphMediumRegular mb-6 md:block">
                                <button className="justify-center h-12 border-neutral-300 rounded-md border max-w-[197px] w-full mr-2 md:mr-0 md:mb-2 md:block">Upload an image</button>
                                <button className="justify-center h-12 border-neutral-300 rounded-md border max-w-[197px] w-full ml-2 md:ml-0 md:block">Select from library</button>
                            </div>

                            <div className="mb-6">
                                <Header
                                    page="menuManagement"
                                    title="Internal name"
                                    headerBottomLine=" Internal name will be used  to search the item."
                                    hasInputField
                                    placeholder="Enter internal name"
                                    shadow="shadow-smallDropDownShadow"
                                    marginBetween="mt-6"
                                />
                            </div>

                            <div className="mb-2">
                                <Header
                                    title="Title"
                                    page="menuManagement"
                                    headerBottomLine="Please enter the category titles that will be displayed with the category image in the mobile application."
                                />
                            </div>

                            <div className="flex flex-row items-center mb-6 md:block">
                                <div className="mr-1.5 w-1/2 lg:w-full lg:max-w-[312px] md:max-w-full md:mr-0 md:mb-2">
                                    <DefaultInputField
                                        boxHeight="[52px]"
                                        labelMarginB="mb-2"
                                        label="(English)"
                                        labelStyle="paragraphMediumItalic text-neutral-500"
                                        placeholder="Enter title in English"
                                        shadow="shadow-smallDropDownShadow"
                                    />
                                </div>
                                <div className="ml-1.5 w-1/2 lg:w-full lg:max-w-[312px] md:max-w-full md:ml-0">
                                    <DefaultInputField
                                        boxHeight="[52px]"
                                        labelMarginB="mb-2"
                                        label="(ગુજરાતી)"
                                        labelStyle="paragraphMediumItalic text-neutral-500"
                                        placeholder="Enter title in ગુજરાતી"
                                        shadow="shadow-smallDropDownShadow"
                                    />
                                </div>
                            </div>

                            <div className="mb-2">
                                <Header title="Description" page="menuManagement" headerBottomLine="Please enter the description of the category that will be displayed in the mobile application." />
                            </div>

                            <div className="flex flex-row items-center mb-4 md:block">
                                <div className="mr-1.5 w-1/2 md:mb-2 md:w-full md:mr-0">
                                    <InputArea placeholder="Enter description in English" paddingT="pt-4" label="(English)" type="storeSetting" shadow="shadow-smallDropDownShadow" />
                                </div>
                                <div className="ml-1.5 w-1/2 md:w-full md:ml-0">
                                    <InputArea placeholder="Enter description in ગુજરાતી" paddingT="pt-4" label="(ગુજરાતી)" type="storeSetting" shadow="shadow-smallDropDownShadow" />
                                </div>
                            </div>
                        </div>
                        <div className="w-full flex justify-center">
                            <div className="w-full max-w-[329px] md:max-w-[280px] ml-8 xl:ml-0">
                                <img src={mobile} alt="" />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="sticky z-[100] md:z-[8] md:fixed md:justify-center md:border-none md:py-0 md:pb-1 md:pt-2 md:shadow-dropShadow left-0 right-0 bottom-0 flex px-8 lg:px-4 flex-row justify-end w-full max-w-[1336px] mx-auto bg-white py-6 border-t border-neutral-300 lg:py-4">
                    <div className="min-w-[196px] mr-5 md:w-1/2 md:mr-[7.5px] md:min-w-0">
                        <LargeDestructiveButton name="Discard" />
                    </div>
                    <div className="min-w-[196px] md:w-1/2 md:ml-[7.5px] md:min-w-0">
                        <LargePrimaryButton name="Save" />
                    </div>
                </div>
            </div>
        </>
    );
}
