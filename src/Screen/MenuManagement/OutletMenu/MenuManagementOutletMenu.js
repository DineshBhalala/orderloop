import React, { useState } from "react";
import { DefaultBreadcrumbs } from "../../../Components/Breadcrumbs/Breadcrumbs";
import { ReactComponent as MenuIcon } from "../../../Assets/menu.svg";
import { ReactComponent as DownArrow } from "../../../Assets/chevron-down.svg";
import Catalogue from "../Components/Catalogue/Catalogue";
import { useLocation } from "react-router-dom";
import { useNavigate } from "react-router-dom";

export default function MenuManagementOutletMenu() {
  const location = useLocation();
  const page = location.state?.page;
  const [isShowDetails, setIsShowDetails] = useState(false);

  const handleClickShowDetails = () => {
    setIsShowDetails(!isShowDetails);
  };

  const navigate = useNavigate();
  const handleClickBack = () => {
    navigate("/menu");
  };

  return (
    <>
      <div className="bg-[#fafafa]">
        <div className="px-8 pb-[70px] lg:px-4 lg:pb-[23px] pt-4 w-full max-w-[1336px] mx-auto bg-white">
          <div className="mb-4 md:hidden">
            <DefaultBreadcrumbs mainTab="Menu management" tab1="Outlet menu" icon={<MenuIcon height={20} />} />
          </div>
          <div className="md:hidden">
            <div className="grid grid-cols-[minmax(380px,489px)_minmax(193px,302px)_minmax(185px,294px)_187px] xl:grid-cols-[minmax(232px,1336px)_263px]">
              <div className="mb-3">
                <span className="paragraphMediumSemiBold text-neutral-400">Outlet name:</span>
                <span className="ParagraphMediumRegular ml-1">Domino's Pizza - Tagore Road Outlet</span>
              </div>

              <div className="mb-3">
                <span className="paragraphMediumSemiBold text-neutral-400">State:</span>
                <span className="ParagraphMediumRegular ml-1">Gujarat</span>
              </div>

              <div className="mb-3">
                <span className="paragraphMediumSemiBold text-neutral-400">City:</span>
                <span className="ParagraphMediumRegular ml-1">Rajkot</span>
              </div>

              <div className="mb-3">
                <span className="paragraphMediumSemiBold text-neutral-400">Menu name:</span>
                <span className="ParagraphMediumRegular ml-1">Menu - 007</span>
              </div>

              <div className="mb-3">
                <span className="paragraphMediumSemiBold text-neutral-400">Created by:</span>
                <span className="ParagraphMediumRegular ml-1">Sarthak Kanchan</span>
              </div>

              <div className="mb-3">
                <span className="paragraphMediumSemiBold text-neutral-400">Created on:</span>
                <span className="ParagraphMediumRegular ml-1">20 Nov 2022</span>
              </div>

              <div className="mb-3">
                <span className="paragraphMediumSemiBold text-neutral-400">Categories attached:</span>
                <span className="ParagraphMediumRegular ml-1">12</span>
              </div>

              <div className="mb-3">
                <span className="paragraphMediumSemiBold text-neutral-400">Dishes attached:</span>
                <span className="ParagraphMediumRegular ml-1">128</span>
              </div>
            </div>
            <div className="mb-4 pb-4 border-b border-neutral-300">
              <span className="paragraphMediumSemiBold text-neutral-400">Outlet address:</span>
              <span className="ParagraphMediumRegular ml-1">Shyamal Infinity, beneath Radio Mirchi, kalawad road</span>
            </div>
          </div>
          <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer" onClick={handleClickBack}>
            <DownArrow className="rotate-90" />
            <span className="paragraphMediumMedium ml-1">Back to outlet menu</span>
          </div>
          <div className="mb-4 pb-4 border-b border-neutral-300 hidden md:block">
            <div className="w-full px-4 py-2.5 border border-neutral-300 rounded-md">
              <div className="flex flex-row items-center justify-between cursor-pointer" onClick={handleClickShowDetails}>
                <div className="max-w-[223px]">
                  <h3 className="paragraphOverlineSmall text-neutral-700">OUTLET NAME:</h3>
                  <div className="paragraphSmallRegular flex mt-1">Domino's Pizza - Univerisity Road Outlet</div>
                </div>
                <div className="flex flex-row items-center">
                  <div className={`${isShowDetails && "rotate-180"}`}>
                    <DownArrow />
                  </div>
                </div>
              </div>
              <div className={`${!isShowDetails && "hidden"} mt-1`}>
                <div className="mb-[5px]">
                  <span className="paragraphOverlineSmall text-neutral-700">STATE:</span>
                  <span className="paragraphSmallRegular ml-1">Gujarat</span>
                </div>
                <div className="mb-[5px]">
                  <span className="paragraphOverlineSmall text-neutral-700">CITY:</span>
                  <span className="paragraphSmallRegular ml-1">Rajkot</span>
                </div>
                <div className="mb-[5px] flex flex-row">
                  <span className="paragraphOverlineSmall text-neutral-700">ADDRESS:</span>
                  <div className="paragraphSmallRegular ml-1 break-words">Shyamal Infinity, beneath Radio Mirchi, kalawad road</div>
                </div>

                <div className="mb-[5px]">
                  <span className="paragraphOverlineSmall text-neutral-700">MENU NAME:</span>
                  <span className="paragraphSmallRegular ml-1">Menu - 007</span>
                </div>

                <div className="mb-[5px]">
                  <span className="paragraphOverlineSmall text-neutral-700">CREATED BY:</span>
                  <span className="paragraphSmallRegular ml-1">Sarthak Kanchan</span>
                </div>

                <div className="mb-[5px]">
                  <span className="paragraphOverlineSmall text-neutral-700">CREATED ON:</span>
                  <span className="paragraphSmallRegular ml-1">20 Nov 2022</span>
                </div>

                <div className="mb-[5px]">
                  <span className="paragraphOverlineSmall text-neutral-700">CATEGORIES ATTACHED:</span>
                  <span className="paragraphSmallRegular ml-1">12</span>
                </div>

                <div className="">
                  <span className="paragraphOverlineSmall text-neutral-700">DISHES ATTACHED:</span>
                  <span className="paragraphSmallRegular ml-1">08</span>
                </div>
              </div>
            </div>
          </div>

          <Catalogue page={page} />
        </div>
      </div>
    </>
  );
}
