import React, { useEffect, useState } from "react";
import { DefaultBreadcrumbs } from "../../Components/Breadcrumbs/Breadcrumbs";
import { Tab } from "../../Components/Tabs/Tabs";
import { ReactComponent as MenuIcon } from "../../Assets/menu.svg";
import Catalogue from "./Components/Catalogue/Catalogue";
import CataloguePreset from "./Components/CataloguePreset/CataloguePreset";
import OutletMenu from "./Components/OutletMenu/OutletMenu";
import { DropDownTabs } from "../../Components/DropDown/DropDownTabs";

export default function MenuManagement() {
    const headerTab = ["Catalogue", "Catalogue presets", "Outlet menu", "Petpooja link"];

    const handleClickHeaderTab = (index) => {
        setActiveTab(index);
    };
    const [activeTab, setActiveTab] = useState(0);

    const [renderPage, setRenderPage] = useState(<Catalogue />);

    useEffect(() => {
        switch (activeTab) {
            case 0:
                setRenderPage(<Catalogue />);
                break;

            case 1:
                setRenderPage(<CataloguePreset />);
                break;

            case 2:
                setRenderPage(<OutletMenu />);
                break;

            default:
                break;
        }
    }, [activeTab]);

    const handleChangeDropDownItem = (value) => {
        setActiveTab(value);
    };

    return (
        <>
            <div className="bg-[#fafafa]">
                <div className="px-8 lg:px-4 pt-4 w-full max-w-[1336px] mx-auto bg-white relative md:max-w-full pb-20 md:pb-10">
                    <div className="mb-4 md:hidden">
                        <DefaultBreadcrumbs mainTab="Menu management" icon={<MenuIcon height={20} width={20} />} />
                    </div>

                    {/* use this component for multiple tabs */}

                    <div className="flex flex-row items-center mb-4 pb-4 border-b border-neutral-300 md:hidden">
                        {headerTab.map((el, index) => (
                            <div className="mr-4 lg:mr-2 cursor-pointer" onClick={() => handleClickHeaderTab(index)} key={index}>
                                <Tab label={el} isActive={activeTab === index} />
                            </div>
                        ))}
                    </div>

                    <div className="hidden md:block mb-4 pb-4 border-b border-neutral-300">
                        <DropDownTabs
                            menuItems={[
                                { item: "Catalogue", onClick: () => handleChangeDropDownItem(0) },
                                { item: "Catalogue presets", onClick: () => handleChangeDropDownItem(1) },
                                { item: "Outlet menu", onClick: () => handleChangeDropDownItem(2) },
                                { item: "Petpooja lnk", onClick: () => handleChangeDropDownItem(3) },
                            ]}
                        />
                    </div>
                    {renderPage}
                </div>
            </div>
        </>
    );
}
