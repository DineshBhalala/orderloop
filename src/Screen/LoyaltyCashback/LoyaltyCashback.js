import React, { useState } from "react";
import { LargePrimaryButton } from "../../Components/Buttons/Button";
import { ReactComponent as RiderIcon } from "../../Assets/riders.svg";
import { ReactComponent as Filter } from "../../Assets/filter.svg";
import { ReactComponent as OrderIcon } from "../../Assets/order.svg";
import { ReactComponent as CashIcon } from "../../Assets/cash.svg";
import { ReactComponent as SelectIcon } from "../../Assets/select.svg";
import { ReactComponent as LoyaltyCashbackIcon } from "../../Assets/loyalty-cashback.svg";
import { ReactComponent as ExportIcon } from "../../Assets/export.svg";
import DashBoardCard from "../../Components/DashBoardCard/DashBoardCard";
import PaginationNumber from "../../Components/Pagination/PaginationWithNumber";
import SliderDashboard from "react-slick";
import { DefaultBreadcrumbs } from "../../Components/Breadcrumbs/Breadcrumbs";
import CustomerOrderDetails from "../../Components/CustomerOrderDetails/CustomerOrderDetailsPopup";
import ListViewCashback from "../../Components/ListView/ListViewCashback";
import CalenderField from "../../Components/Calender/CalenderField";

export default function LoyaltyCashback() {
    const orderDetails = [
        {
            orderId: "#BBQR",
            customerName: "Riddhi Shah",
            orderData: "18 Nov 2022",
            orderAmount: "₹1,000.00/-",
            cashbackEarned: 1500,
        },
        {
            orderId: "#BBQR",
            customerName: "Amrendra Bahubali",
            orderData: "18 Nov 2022",
            orderAmount: "₹1,000.00/-",
            cashbackEarned: 250,
        },
        {
            orderId: "#BBQR",
            customerName: "Riddhi Shah",
            orderData: "18 Nov 2022",
            orderAmount: "₹1,000.00/-",
            cashbackEarned: 250,
        },
        {
            orderId: "#BBQR",
            customerName: "Riddhi Shah",
            orderData: "18 Nov 2022",
            orderAmount: "₹1,000.00/-",
            cashbackEarned: 1500,
        },
        {
            orderId: "#BBQR",
            customerName: "Riddhi Shah",
            orderData: "18 Nov 2022",
            orderAmount: "₹1,000.00/-",
            cashbackEarned: 250,
        },
        {
            orderId: "#BBQR",
            customerName: "Riddhi Shah",
            orderData: "18 Nov 2022",
            orderAmount: "₹1,000.00/-",
            cashbackEarned: 1500,
        },
        {
            orderId: "#BBQR",
            customerName: "Riddhi Shah",
            orderData: "18 Nov 2022",
            orderAmount: "₹1,000.00/-",
            cashbackEarned: 250,
        },
    ];

    const [showAddRiderPage, setShowAddRiderPage] = useState(false);

    const handleClickCustomerName = () => {
        setShowAddRiderPage(!showAddRiderPage);
    };

    var settingsDashboardSlider = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        className: "dashboardSlide",
    };

    return (
        <>
            <div className="bg-[#fafafa]">
                <div className={`px-8 pb-[70px] lg:px-4 lg:pb-[23px] pt-4 w-full max-w-[1336px] mx-auto bg-white ${showAddRiderPage && "md:hidden"}`}>
                    <div className="mb-4 md:hidden">
                        <DefaultBreadcrumbs mainTab="Loyalty cashbacks" icon={<LoyaltyCashbackIcon className="h-5 w-5" />} />
                    </div>

                    <div className="flex flex-row justify-between border-b border-neutral-300 pb-4 lg:mb-4 md:block">
                        <div className="flex flex-row md:justify-between">
                            <CalenderField label="25 Sept 2022 - 09 Oct 2022" buttonStyle="mobile:pr-0 mobile:pl-1 md:w-full" labelStyle="mobile:text-sm mobile:mx-0" />
                            <div className="max-w-[156px] w-full md:max-w-full md:w-16 ml-4 lg:ml-2">
                                <LargePrimaryButton name="Bulk select" hideName="md:hidden" leftIconDefault={<SelectIcon stroke="#ffffff" />} leftIconClick={<SelectIcon stroke="#C4BEED" />} />
                            </div>
                        </div>
                        <div className="flex flex-row md:justify-between md:mt-4">
                            <div className="mx-4 lg:mx-2 md:w-1/2 md:mr-[8.5px] md:ml-0 mobile:mr-1">
                                <LargePrimaryButton name="Filters" leftIconDefault={<Filter fill="#FFFFFF" />} leftIconClick={<Filter fill="#C4BEED" />} />
                            </div>
                            <div className="md:w-1/2 md:ml-[8.5px] mobile:ml-1">
                                <LargePrimaryButton
                                    name="Export data"
                                    leftIconDefault={<ExportIcon stroke="#FFFFFF" />}
                                    leftIconClick={<ExportIcon stroke="#C4BEED" />}
                                    hideName="lg:hidden md:block"
                                />
                            </div>
                        </div>
                    </div>

                    <div className="-mx-[11px] mb-6 lg:-mx-[1px] md:hidden lg:mb-4">
                        <div className="max-w-[303px] w-full inline-block mx-2.5 mt-4 lg:max-w-full lg:w-1/2 lg:mx-0 lg:pr-1 lg:mt-[9px]">
                            <DashBoardCard
                                isReimbursementIcon={false}
                                title="Total cashback rewarded"
                                amount="4,525"
                                reimbursement="Total cashback rewarded to 125 customers this week"
                                focusReimbursementContentColor="tertiary-800"
                                reimbursementFocusContent="125"
                                percentage="10.2"
                                profit={true}
                                icon={<RiderIcon stroke="#ffffff" />}
                            />
                        </div>

                        <div className="max-w-[303px] w-full inline-block mx-2.5 mt-4 lg:max-w-full lg:w-1/2 lg:mx-0 lg:pl-1 lg:mt-[9px]">
                            <DashBoardCard
                                profit={false}
                                isReimbursementIcon={false}
                                percentage="05.2"
                                title="Total cashback redeemed"
                                amount="1,350"
                                reimbursement="Total cashback redeemed by 75 customers this week"
                                focusReimbursementContentColor="primary-500"
                                reimbursementFocusContent="75"
                                icon={<CashIcon stroke="#ffffff" />}
                            />
                        </div>
                        <div className="max-w-[303px] w-full inline-block mx-2.5 mt-4 lg:max-w-full lg:w-1/2 lg:mx-0 lg:pr-1 lg:mt-[9px]">
                            <DashBoardCard
                                isReimbursementIcon={false}
                                title="Most rewarded customer"
                                amount="Jay Sharma"
                                reimbursement="Jay Sharma was rewarded a total of 1,500 cashback points this week"
                                focusReimbursementContentColor="primary-500"
                                reimbursementFocusContent="1,500"
                                icon={<OrderIcon stroke="#ffffff" />}
                            />
                        </div>
                        <div className="max-w-[303px] w-full inline-block mx-2.5 mt-4 lg:max-w-full lg:w-1/2 lg:mx-0 lg:pl-1 lg:mt-[9px]">
                            <DashBoardCard
                                title="Most redeeming customer"
                                amount="Riddhi Shah"
                                reimbursement="Riddhi Shah redeemed a total of 750 cashback points this week"
                                isReimbursementIcon={false}
                                reimbursementFocusContent="750"
                                focusReimbursementContentColor="primary-500"
                                icon={<RiderIcon stroke="#ffffff" />}
                            />
                        </div>
                    </div>

                    <div className="hidden md:block md:-mx-4">
                        <div>
                            <SliderDashboard {...settingsDashboardSlider}>
                                <DashBoardCard
                                    isReimbursementIcon={false}
                                    title="Total cashback rewarded"
                                    amount="4,525"
                                    reimbursement="Total cashback rewarded to 125 customers this week"
                                    focusReimbursementContentColor="tertiary-800"
                                    reimbursementFocusContent="125"
                                    percentage="10.2"
                                    profit={true}
                                    icon={<RiderIcon stroke="#ffffff" />}
                                />
                                <DashBoardCard
                                    profit={false}
                                    isReimbursementIcon={false}
                                    percentage="05.2"
                                    title="Total cashback redeemed"
                                    amount="1,350"
                                    reimbursement="Total cashback redeemed by 75 customers this week"
                                    focusReimbursementContentColor="primary-500"
                                    reimbursementFocusContent="75"
                                    icon={<CashIcon stroke="#ffffff" />}
                                />
                                <DashBoardCard
                                    isReimbursementIcon={false}
                                    title="Most rewarded customer"
                                    amount="Jay Sharma"
                                    reimbursement="Jay Sharma was rewarded a total of 1,500 cashback points this week"
                                    focusReimbursementContentColor="primary-500"
                                    reimbursementFocusContent="1,500"
                                    icon={<OrderIcon stroke="#ffffff" />}
                                />
                                <DashBoardCard
                                    title="Most redeeming customer"
                                    amount="Riddhi Shah"
                                    reimbursement="Riddhi Shah redeemed a total of 750 cashback points this week"
                                    isReimbursementIcon={false}
                                    reimbursementFocusContent="750"
                                    focusReimbursementContentColor="primary-500"
                                    icon={<RiderIcon stroke="#ffffff" />}
                                />
                            </SliderDashboard>
                        </div>
                    </div>

                    <div className="w-full border border-neutral-300 rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden">
                        <table className="w-full break-words">
                            <thead>
                                <tr className="bg-neutral-50 text-left paragraphOverlineSmall text-neutral-700 shadow-innerShadow h-11 justify-center">
                                    <th className="px-6 min-w-[187px] lg:min-w-[122px]">ORDER ID</th>
                                    <th className="px-6 min-w-[279px] lg:min-w-[230px]">CUSTOMER NAME</th>
                                    <th className="px-6 min-w-[262px] lg:min-w-[163px]">ORDER DATE</th>
                                    <th className="px-6 min-w-[275px] lg:min-w-[166px]">ORDER AMOUNT</th>
                                    <th className="px-6 min-w-[269px] lg:min-w-[269px]">CASHBACK EARNED</th>
                                </tr>
                            </thead>
                            <tbody>
                                {orderDetails.map((el, index) => {
                                    return (
                                        <tr className={`paragraphSmallRegular ${index !== 0 && "border-t"} odd:bg-neutral-50 border-neutral-300 h-[70px] justify-center`} key={index}>
                                            <td className="px-6">{el.orderId}</td>

                                            <td className="px-6 cursor-pointer" onClick={handleClickCustomerName}>
                                                {el.customerName}
                                            </td>
                                            <td className="px-6">{el.orderData}</td>
                                            <td className="px-6">{el.orderAmount}</td>
                                            <td className="px-6">
                                                <div className="flex flex-row items-center">
                                                    <LoyaltyCashbackIcon />
                                                    <span className="pl-1">{el.cashbackEarned}</span>
                                                </div>
                                            </td>
                                        </tr>
                                    );
                                })}
                            </tbody>
                        </table>
                    </div>

                    <div className="hidden md:block mt-3">
                        {orderDetails.map((el, index) => {
                            return (
                                <div className="mb-1 pt-1" key={index}>
                                    <ListViewCashback
                                        customerName={el.customerName}
                                        cashback={el.cashbackEarned}
                                        orderId={el.orderId}
                                        orderDate={el.orderData}
                                        orderAmount={el.orderAmount}
                                        handleClickCustomerName={handleClickCustomerName}
                                    />
                                </div>
                            );
                        })}
                    </div>

                    <div className="pt-4 md:hidden">
                        <PaginationNumber />
                    </div>
                </div>
                <div className={`${!showAddRiderPage && "hidden"}`}>
                    <CustomerOrderDetails pageName="loyaltyCashback" handleClickClose={handleClickCustomerName} />
                </div>
            </div>
        </>
    );
}
