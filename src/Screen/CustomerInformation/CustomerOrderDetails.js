import React, { useState } from "react";
import { LargePrimaryButton } from "../../Components/Buttons/Button";
import { ReactComponent as CustomerIcon } from "../../Assets/customers.svg";
import { ReactComponent as Calender } from "../../Assets/calendar.svg";
import { ReactComponent as Export } from "../../Assets/export.svg";
import { ReactComponent as LeftArrow } from "../../Assets/chevron-down.svg";
import { ReactComponent as DineInIcon } from "../../Assets/dine-in.svg";
import { ReactComponent as TakeAwayIcon } from "../../Assets/orders.svg";
import { ReactComponent as DeliveryIcon } from "../../Assets/riders.svg";
import { DefaultBreadcrumbs } from "../../Components/Breadcrumbs/Breadcrumbs";
import { ReactComponent as SelectIcon } from "../../Assets/select.svg";
import { ReactComponent as SuccessTickIcon } from "../../Assets/success-tick.svg";
import { ReactComponent as CancelIcon } from "../../Assets/cancel.svg";
import { ReactComponent as UPIIcon } from "../../Assets/UPI.svg";
import { ReactComponent as CashIcon } from "../../Assets/cash.svg";
import { ReactComponent as CardIcon } from "../../Assets/card.svg";
import { ReactComponent as SendIcon } from "../../Assets/send.svg";
import { ReactComponent as FilterIcon } from "../../Assets/filter.svg";
import PaginationWithNumber from "../../Components/Pagination/PaginationWithNumber";
import CustomerOrderDetailsPopup from "../../Components/CustomerOrderDetails/CustomerOrderDetailsPopup";
import SelectOfferPage from "../../Components/SelectOffer/SelectOfferPage";
import ListViewCustomerIndividualDetails from "../../Components/ListView/ListViewCustomerIndividualDetails";
import ListViewCustomerOrder from "../../Components/ListView/ListViewCustomerOrder";
import { useNavigate } from "react-router-dom";

export default function CustomerOrderDetails() {
    const tableDetails = [
        {
            billName: "Riddhi Shah",
            orderDate: "22 Sep 2022",
            orderType: "Dine-In",
            orderStatus: "Delivered",
            itemsOrdered: "01",
            revenueGenerated: "₹99.00/-",
            paymentMode: "Credit card",
        },
        {
            billName: "Riddhi Shah",
            orderDate: "22 Sep 2022",
            orderType: "Dine-In",
            orderStatus: "Cancelled",
            itemsOrdered: "01",
            revenueGenerated: "₹99.00/-",
            paymentMode: "Credit card",
        },
        {
            billName: "Riddhi Shah",
            orderDate: "22 Sep 2022",
            orderType: "Dine-In",
            orderStatus: "Delivered",
            itemsOrdered: "01",
            revenueGenerated: "₹99.00/-",
            paymentMode: "Cash",
        },
        {
            billName: "Riddhi Shah",
            orderDate: "22 Sep 2022",
            orderType: "Dine-In",
            orderStatus: "Delivered",
            itemsOrdered: "01",
            revenueGenerated: "₹99.00/-",
            paymentMode: "Cash",
        },
        {
            billName: "Riddhi Shah",
            orderDate: "22 Sep 2022",
            orderType: "Dine-In",
            orderStatus: "Cancelled",
            itemsOrdered: "01",
            revenueGenerated: "₹99.00/-",
            paymentMode: "Debit card",
        },
        {
            billName: "Riddhi Shah",
            orderDate: "22 Sep 2022",
            orderType: "Dine-In",
            orderStatus: "Delivered",
            itemsOrdered: "01",
            revenueGenerated: "₹99.00/-",
            paymentMode: "UPI",
        },
        {
            billName: "Riddhi Shah",
            orderDate: "22 Sep 2022",
            orderType: "Dine-In",
            orderStatus: "Delivered",
            itemsOrdered: "01",
            revenueGenerated: "₹99.00/-",
            paymentMode: "UPI",
        },
        {
            billName: "Riddhi Shah",
            orderDate: "22 Sep 2022",
            orderType: "Dine-In",
            orderStatus: "Delivered",
            itemsOrdered: "01",
            revenueGenerated: "₹99.00/-",
            paymentMode: "Cash",
        },
    ];
    const [showOrderDetails, setShowOrderDetails] = useState(false);

    const handleClickBillName = () => {
        setShowOrderDetails(!showOrderDetails);
    };

    const [ShowSendOffer, setShowSendOffer] = useState(false);

    const handleClickSendOffer = () => {
        setShowSendOffer(!ShowSendOffer);
    };
    const navigate = useNavigate();

    const handleClickCustomerCartDetails = () => {
        navigate("/customer-information");
    };
    return (
        <>
            <div className="bg-[#fafafa]">
                <div className={`px-8 pb-[70px] lg:px-4 lg:pb-[23px] pt-4 w-full max-w-[1336px] mx-auto bg-white ${showOrderDetails && "md:hidden"}`}>
                    <div className="mb-4 md:hidden">
                        <DefaultBreadcrumbs mainTab="Customers information" tab1="customer details" icon={<CustomerIcon className="h-5 w-5" />} />
                    </div>

                    <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer" onClick={handleClickCustomerCartDetails}>
                        <LeftArrow className="rotate-90" />
                        <span className="ml-1">Back to individuals</span>
                    </div>

                    <div className="grid grid-cols-[minmax(198px,306px)_minmax(263px,371px)_minmax(232px,340px)_255px] xl:grid-cols-[minmax(232px,1336px)_263px] pb-4 mb-4 border-b border-neutral-300 md:hidden">
                        <div className="mb-3">
                            <span className="paragraphMediumSemiBold text-neutral-400">Name:</span>
                            <span className="ParagraphMediumRegular ml-1">Amrendra Bahubali</span>
                        </div>

                        <div className="mb-3">
                            <span className="paragraphMediumSemiBold text-neutral-400">Mobile number:</span>
                            <span className="ParagraphMediumRegular ml-1">(+91) 8816681288</span>
                        </div>

                        <div className="mb-3">
                            <span className="paragraphMediumSemiBold text-neutral-400">Email ID:</span>
                            <span className="ParagraphMediumRegular ml-1">ab2017@orderloop.in</span>
                        </div>

                        <div className="mb-3">
                            <span className="paragraphMediumSemiBold text-neutral-400">Revenue generated:</span>
                            <span className="ParagraphMediumRegular ml-1">₹21,758.00/-</span>
                        </div>

                        <div className="mb-3">
                            <span className="paragraphMediumSemiBold text-neutral-400">Total orders:</span>
                            <span className="ParagraphMediumRegular ml-1">1235</span>
                        </div>

                        <div className="mb-3">
                            <span className="paragraphMediumSemiBold text-neutral-400">Last order:</span>
                            <span className="ParagraphMediumRegular ml-1">18 Nov 2022</span>
                        </div>

                        <div className="mb-3">
                            <span className="paragraphMediumSemiBold text-neutral-400">First order:</span>
                            <span className="ParagraphMediumRegular ml-1">14 Feb 2021</span>
                        </div>

                        <div className="mb-3">
                            <span className="paragraphMediumSemiBold text-neutral-400">Join date:</span>
                            <span className="ParagraphMediumRegular ml-1">13 Feb 2021</span>
                        </div>

                        <div className="xl:mb-3">
                            <span className="paragraphMediumSemiBold text-neutral-400">Order rating:</span>
                            <span className="ParagraphMediumRegular ml-1">4.52</span>
                        </div>

                        <div className="">
                            <span className="paragraphMediumSemiBold text-neutral-400">Cashback earned:</span>
                            <span className="ParagraphMediumRegular ml-1">₹200.00/-</span>
                        </div>

                        <div className="">
                            <span className="paragraphMediumSemiBold text-neutral-400">Discount availed:</span>
                            <span className="ParagraphMediumRegular ml-1">₹2,500.00/-</span>
                        </div>
                    </div>
                    <div className="hidden md:block mb-4 pb-4 border-b border-neutral-300">
                        <ListViewCustomerIndividualDetails
                            customerName="Amrendra Bahubali"
                            mobileNumber="(+91) 8816681288"
                            totalOrders="1,235"
                            cashbackEarned="₹200.00/-"
                            lastOrder="20 Nov 2022"
                            orderRating="4.52"
                            emailId="ab2017@orderloop.in"
                            revenueGenerated="₹21,785.00/-"
                            firstOrder="14 Feb 2021"
                            joinDate="13 Feb 2021"
                            discountAvailed="ab2017@orderloop.in"
                        />
                    </div>
                    <div className="flex flex-row justify-between md:block mb-6 md:mb-4">
                        <div className="flex flex-row md:justify-between">
                            <div className="min-w-[280px] mobile:min-w-0 w-full">
                                <button className="border px-3 py-[11px] mobile:px-0 flex flex-row items-center rounded-md border-neutral-300 w-full">
                                    <Calender className="mx-1 mobile:ml-1.5" />
                                    <span className="paragraphMediumRegular mx-1 mobile:mx-0 mobile:text-sm">25 Sept 2022 - 09 Oct 2022</span>
                                </button>
                            </div>
                            <div className="max-w-[156px] w-full md:max-w-full md:w-[60px] ml-4 xl:ml-2">
                                <LargePrimaryButton name="Bulk select" hideName="md:hidden" leftIconDefault={<SelectIcon stroke="#ffffff" />} leftIconClick={<SelectIcon stroke="#C4BEED" />} />
                            </div>
                        </div>
                        <div className="flex flex-row md:justify-between md:mt-4">
                            <div className="mx-4 xl:mx-1 md:ml-0 md:mr-2 md:w-16">
                                <LargePrimaryButton leftIconDefault={<FilterIcon fill="#ffffff" />} leftIconClick={<FilterIcon fill="#C4BEED" />} name="Filters" hideName="lg:hidden" />
                            </div>
                            <div className="md:w-1/2">
                                <LargePrimaryButton name="Export data" leftIconDefault={<Export stroke="#FFFFFF" />} leftIconClick={<Export stroke="#C4BEED" />} hideName="lg:hidden md:block" />
                            </div>
                            <div className="md:w-1/2 mobile:w-16 ml-4 xl:ml-1 md:ml-2 cursor-pointer" onClick={handleClickSendOffer}>
                                <LargePrimaryButton
                                    isDefault={false}
                                    name="Send offer"
                                    leftIconDefault={<SendIcon stroke="#FFFFFF" />}
                                    leftIconClick={<SendIcon stroke="#C4BEED" />}
                                    hideName="lg:hidden md:block mobile:hidden"
                                />
                            </div>
                        </div>
                    </div>
                    <div className="w-full rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden border-neutral-300 border">
                        <table className="w-full break-words tableMediaLibrary">
                            <thead>
                                <tr className="paragraphOverlineSmall text-neutral-700 shadow-innerShadow bg-neutral-50 text-left justify-center h-11">
                                    <th className="px-6 min-w-[231px]">ORDER BILL NAME</th>
                                    <th className="px-6 min-w-[174px]">ORDER DATE</th>
                                    <th className="px-6 min-w-[155px]">ORDER TYPE</th>
                                    <th className="px-6 min-w-[167px]">ORDER STATUS</th>
                                    <th className="px-6 min-w-[163px]">ITEMS ORDERED</th>
                                    <th className="px-6 min-w-[197px]">REVENUE GENERATED</th>
                                    <th className="px-6 min-w-[183px]">PAYMENT MODE</th>
                                </tr>
                            </thead>
                            <tbody>
                                {tableDetails.map((el, index) => {
                                    return (
                                        <tr className={`paragraphSmallRegular ${index !== 0 && "border-t"} border-neutral-300 justify-center h-[70px]`} key={index}>
                                            <td className="px-6 cursor-pointer" onClick={handleClickBillName}>
                                                {el.billName}
                                            </td>
                                            <td className="px-6">{el.orderDate}</td>
                                            <td className="px-6">
                                                <div className="flex flex-row items-center">
                                                    {el.orderType === "Dine-In" ? <DineInIcon /> : el.orderType === "Takeaway" ? <TakeAwayIcon /> : <DeliveryIcon />}
                                                    <span className="ml-1">{el.orderType}</span>
                                                </div>
                                            </td>
                                            <td className="px-6">
                                                <div className="flex flex-row items-center">
                                                    {el.orderStatus === "Delivered" ? <SuccessTickIcon /> : <CancelIcon />}
                                                    <span className="ml-1">{el.orderStatus}</span>
                                                </div>
                                            </td>
                                            <td className="px-6">{el.itemsOrdered}</td>
                                            <td className="px-6">{el.revenueGenerated}</td>
                                            <td className="px-6">
                                                <div className="flex flex-row items-center">
                                                    {el.paymentMode === "UPI" ? <UPIIcon /> : el.paymentMode === "Cash" ? <CashIcon /> : <CardIcon />}
                                                    <span className="ml-1"> {el.paymentMode}</span>
                                                </div>
                                            </td>
                                        </tr>
                                    );
                                })}
                            </tbody>
                        </table>
                    </div>
                    <div className="hidden md:block">
                        {tableDetails.map((el, index) => {
                            return (
                                <div className="mb-2" key={index}>
                                    <ListViewCustomerOrder
                                        billName={el.billName}
                                        orderDate={el.orderDate}
                                        orderType={el.orderType}
                                        orderStatus={el.orderStatus}
                                        itemsOrdered={el.itemsOrdered}
                                        revenueGenerated={el.revenueGenerated}
                                        paymentMode={el.paymentMode}
                                        viewDetails={handleClickBillName}
                                    />
                                </div>
                            );
                        })}
                    </div>
                    <div className="mt-4 md:hidden">
                        <PaginationWithNumber />
                    </div>
                </div>
                <div className={`${!showOrderDetails && "hidden"}`}>
                    <CustomerOrderDetailsPopup handleClickClose={handleClickBillName} page="customerInformation" headerTabletScreen="Back to individual details" />
                </div>
                <div className={`${!ShowSendOffer && "hidden"}`}>
                    <SelectOfferPage handleClickClose={handleClickSendOffer} />
                </div>
            </div>
        </>
    );
}
