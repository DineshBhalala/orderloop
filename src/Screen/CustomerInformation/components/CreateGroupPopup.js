import React from "react";
import { ReactComponent as CloseIcon } from "../../../Assets/close.svg";
import { DefaultInputField } from "../../../Components/InputField/InputField";
import { LargePrimaryButton } from "../../../Components/Buttons/Button";

export default function CreateGroupPopup(props) {
  return (
    <>
      <div className="fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 md:px-4 flex">
        <div className="max-w-[475px] w-full rounded-xl bg-shades-50 px-8 py-6 m-auto md:w-full md:px-4 md:py-4">
          <div className="flex justify-between items-center mb-6">
            <div>
              <h3 className="paragraphLargeMedium">Create group</h3>
              <p className="paragraphMediumItalic text-neutral-500">Name a group for the selected customers</p>
            </div>
            <span className="cursor-pointer" onClick={props.handleClickCreateGroup}>
              <CloseIcon />
            </span>
          </div>

          <div className="mb-12">
            <DefaultInputField boxHeight="11" label="Group name" placeholder="Enter group name" />
          </div>

          <LargePrimaryButton name="Save" />
        </div>
      </div>
    </>
  );
}
