import React, { useEffect, useState } from "react";
import { CheckBox } from "../../../Components/FormControl/FormControls";

export default function IndividualTableRow(props) {
  const [isSelected, setIsSelected] = useState(false);

  const handleClickRow = () => {
    if (!props.isShowBulkSelectButton) {
      return;
    }
    setIsSelected(!isSelected);
  };

  
  useEffect(() => {
    !props.isShowBulkSelectButton && setIsSelected(false);
  }, [props.isShowBulkSelectButton]);

  return (
    <tr
      className={`paragraphSmallRegular ${props.index !== 0 && "border-t"} cursor-pointer justify-center h-[70px] ${
        isSelected ? "bg-primary-50 border border-primary-500" : "even:bg-neutral-50 border-neutral-300"
      }`}
      onClick={handleClickRow}>
      <td className="px-6 cursor-pointer" onClick={props.handleClickCustomerName}>
        <div className="flex flex-row">
          {isSelected && (
            <div className="mr-1">
              <CheckBox checked={true} />
            </div>
          )}
          <span>{props.el.customerName}</span>
        </div>
      </td>
      <td className="px-6">{props.el.mobileNumber}</td>
      <td className="px-6 text-primary-500">{props.el.emailId}</td>
      <td className="px-6">{props.el.totalOrder}</td>
      <td className="px-6">{props.el.revenueGenerated}</td>
      <td className="px-6">{props.el.lastOrder}</td>
    </tr>
  );
}
