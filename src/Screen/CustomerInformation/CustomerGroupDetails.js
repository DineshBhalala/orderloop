import React, { useState } from "react";
import { LargePrimaryButton } from "../../Components/Buttons/Button";
import { ReactComponent as CustomerIcon } from "../../Assets/customers.svg";
import { ReactComponent as Calender } from "../../Assets/calendar.svg";
import { ReactComponent as Export } from "../../Assets/export.svg";
import { ReactComponent as LinkIcon } from "../../Assets/link.svg";
import { ReactComponent as LeftArrow } from "../../Assets/chevron-down.svg";
import { DefaultBreadcrumbs } from "../../Components/Breadcrumbs/Breadcrumbs";
import { ReactComponent as SelectIcon } from "../../Assets/select.svg";
import PaginationWithNumber from "../../Components/Pagination/PaginationWithNumber";
import LinkOfferPopup from "./Components/LinkOfferPopup";
import ListViewCustomerGroupDetails from "../../Components/ListView/ListViewCustomerGroupDetails";
import ListViewCustomerGroupCustomerDetails from "../../Components/ListView/ListViewCustomerGroupCustomerDetails";
import { useNavigate } from "react-router-dom";

export default function CustomerGroupDetails() {
    const [windowWidth, setWindowWidth] = useState(window.innerWidth);

    const handleResize = () => {
        setWindowWidth(window.innerWidth);
    };

    window.onresize = handleResize;
    const navigate = useNavigate();
    const tableDetails = [
        {
            customerName: "Riddhi Shah",
            mobileNumber: "(+91) 8866886688",
            emailId: "riddhi.shah@gmail.com",
            totalOrder: "713",
            revenueGenerated: "₹559.00/-",
            lastOrder: "22 Sep 2022",
        },
        {
            customerName: "Amrendra Bahubali",
            mobileNumber: "(+91) 8866886688",
            emailId: "riddhi.shah@gmail.com",
            totalOrder: "713",
            revenueGenerated: "₹559.00/-",
            lastOrder: "22 Sep 2022",
        },
    ];

    const [showLinkOfferPopUp, setShowLinkOfferPopUp] = useState(false);

    const handleClickLinkOffers = () => {
        setShowLinkOfferPopUp(!showLinkOfferPopUp);
    };
    const handleClickBack = () => {
        navigate("/customer-information");
    };
    return (
        <>
            <div className="bg-[#fafafa]">
                <div className={`${showLinkOfferPopUp && "md:hidden"}`}>
                    <div className="px-8 pb-[70px] lg:px-4 lg:pb-[23px] pt-4 w-full max-w-[1336px] mx-auto bg-white">
                        <div className="mb-4 md:hidden">
                            <DefaultBreadcrumbs mainTab="Customers information" tab1="Group details" icon={<CustomerIcon className="h-5 w-5" />} />
                        </div>

                        <div className="grid grid-cols-[minmax(261px,387.33px)_minmax(164px,290.33px)_minmax(223px,349.33px)_245px] pb-4 mb-4 border-b border-neutral-300 xl:grid-cols-[minmax(261px,1336px)_245px] md:hidden">
                            <div className="mb-3">
                                <span className="paragraphMediumSemiBold text-neutral-400">Group name:</span>
                                <span className="paragraphMediumRegular ml-1">Family</span>
                            </div>

                            <div className="mb-3">
                                <span className="paragraphMediumSemiBold text-neutral-400">Customers count:</span>
                                <span className="paragraphMediumRegular ml-1">02</span>
                            </div>

                            <div className="mb-3">
                                <span className="paragraphMediumSemiBold text-neutral-400">Created by:</span>
                                <span className="paragraphMediumRegular ml-1 text-primary-500">Sarthak Kanchan</span>
                            </div>

                            <div className="mb-3">
                                <span className="paragraphMediumSemiBold text-neutral-400">Created on:</span>
                                <span className="paragraphMediumRegular ml-1">20 November, 2022</span>
                            </div>

                            <div className="">
                                <span className="paragraphMediumSemiBold text-neutral-400">Last updated:</span>
                                <span className="paragraphMediumRegular ml-1">20 November, 2022</span>
                            </div>

                            <div className="">
                                <span className="paragraphMediumSemiBold text-neutral-400">Notification sent:</span>
                                <span className="paragraphMediumRegular ml-1">13</span>
                            </div>
                        </div>

                        <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer" onClick={handleClickBack}>
                            <LeftArrow className="rotate-90" />
                            <span className="ml-1">Back to groups</span>
                        </div>

                        <div className="hidden md:block mb-4 pb-4 border-b border-neutral-300">
                            <ListViewCustomerGroupDetails
                                groupName="Family"
                                createdBy="02"
                                lastUpdated="Sarthak Kanchan"
                                customerCount="20 November, 2022"
                                createdOn="20 November, 2022"
                                notificationsSent="13"
                            />
                        </div>

                        <div className="flex flex-row justify-between md:block">
                            <div className="flex flex-row md:justify-between">
                                <div className="min-w-[280px] mobile:min-w-0 w-full">
                                    <button className="border px-3 py-[11px] mobile:px-0 flex flex-row items-center rounded-md border-neutral-300 w-full">
                                        <Calender className="mx-1 mobile:ml-1.5" />
                                        <span className="paragraphMediumRegular mx-1 mobile:mx-0 mobile:text-sm">25 Sept 2022 - 09 Oct 2022</span>
                                    </button>
                                </div>
                                <div className="max-w-[156px] w-full md:max-w-full md:w-[60px] ml-4 lg:ml-2">
                                    <LargePrimaryButton name="Bulk select" hideName="md:hidden" leftIconDefault={<SelectIcon stroke="#ffffff" />} leftIconClick={<SelectIcon stroke="#C4BEED" />} />
                                </div>
                            </div>
                            <div className="flex flex-row md:justify-between md:my-4">
                                <div className="md:w-1/2 md:pl-[3.5px] lg:mx-2 mx-4 md:mx-0 md:mr-2 mobile:mr-1 cursor-pointer" onClick={handleClickLinkOffers}>
                                    <LargePrimaryButton
                                        isDefault={false}
                                        name={"Link offer"}
                                        leftIconDefault={windowWidth > 357 && <LinkIcon stroke="#FFFFFF" />}
                                        leftIconClick={windowWidth > 357 && <LinkIcon stroke="#C4BEED" />}
                                        badgeText={4}
                                        hideName="lg:hidden md:block"
                                    />
                                </div>
                                <div className="md:w-1/2 md:mx-0 md:ml-2 mobile:ml-1">
                                    <LargePrimaryButton name="Export data" leftIconDefault={<Export stroke="#FFFFFF" />} leftIconClick={<Export stroke="#C4BEED" />} hideName="lg:hidden md:block" />
                                </div>
                            </div>
                        </div>

                        <div className="w-full rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden border-neutral-300 border mt-6">
                            <table className="w-full break-words tableMediaLibrary">
                                <thead>
                                    <tr className="paragraphOverlineSmall text-neutral-700 shadow-innerShadow bg-neutral-50 text-left h-11 justify-center">
                                        <th className="px-6 min-w-[216px] lg:min-w-[187px]">CUSTOMER NAME</th>
                                        <th className="px-6 min-w-[213px] lg:min-w-[178px]">MOBILE NUMBER</th>
                                        <th className="px-6 min-w-[204px] lg:min-w-[202px]">EMAIL ID</th>
                                        <th className="px-6 min-w-[219px] lg:min-w-[163px]">TOTAL ORDER</th>
                                        <th className="px-6 min-w-[219px]">REVENUE GENERATED</th>
                                        <th className="px-6 min-w-[199px]">LAST ORDER</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {tableDetails.map((el, index) => {
                                        return (
                                            <tr className={`paragraphSmallRegular ${index !== 0 && "border-t"} border-neutral-300 justify-center h-[70px]`} key={index}>
                                                <td className="px-6">{el.customerName}</td>
                                                <td className="px-6">{el.mobileNumber}</td>
                                                <td className="px-6 text-primary-500">{el.emailId}</td>
                                                <td className="px-6">{el.totalOrder}</td>
                                                <td className="px-6">{el.revenueGenerated}</td>
                                                <td className="px-6">{el.lastOrder}</td>
                                            </tr>
                                        );
                                    })}
                                </tbody>
                            </table>
                        </div>

                        <div className="hidden md:block">
                            {tableDetails.map((el, index) => {
                                return (
                                    <div className="mb-2" key={index}>
                                        <ListViewCustomerGroupCustomerDetails
                                            customerName={el.customerName}
                                            mobileNumb={el.mobileNumber}
                                            emailId={el.emailId}
                                            totalOrder={el.totalOrder}
                                            revenue={el.revenueGenerated}
                                            lastOrder={el.lastOrder}
                                        />
                                    </div>
                                );
                            })}
                        </div>

                        <div className="mt-4 md:hidden">
                            <PaginationWithNumber />
                        </div>
                    </div>
                </div>

                <div className={`${!showLinkOfferPopUp && "hidden"}`}>
                    <LinkOfferPopup handleClickClose={handleClickLinkOffers} selectedOffer={"04"} />
                </div>
            </div>
        </>
    );
}
