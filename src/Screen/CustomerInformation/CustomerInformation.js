import React, { useEffect, useState } from "react";
import { LargeDestructiveButton, LargePrimaryButton, LargeTertiaryButton } from "../../Components/Buttons/Button";
import { ReactComponent as SearchIcon } from "../../Assets/search.svg";
import { ReactComponent as Filter } from "../../Assets/filter.svg";
import { ReactComponent as CustomerIcon } from "../../Assets/customers.svg";
import { ReactComponent as SelectIcon } from "../../Assets/select.svg";
import { ReactComponent as ExportIcon } from "../../Assets/export.svg";
import { DefaultBreadcrumbs } from "../../Components/Breadcrumbs/Breadcrumbs";
import { ReactComponent as AddIcon } from "../../Assets/add.svg";
import { DefaultInputField } from "../../Components/InputField/InputField";
import { Tab } from "../../Components/Tabs/Tabs";
import { useNavigate } from "react-router-dom";
import PaginationWithNumber from "../../Components/Pagination/PaginationWithNumber";
import { LinkOffer } from "../../Components/LinkOffer/LinkOffer";
import IndividualTableRow from "./Components/IndividualTableRow";
import CreateGroupPopup from "./Components/CreateGroupPopup";
import ListViewCustomerIndividual from "../../Components/ListView/ListViewCustomerIndividual";
import ListViewCustomerGroups from "../../Components/ListView/ListViewCustomerGroups";

export default function CustomerInformation() {
    const [windowWidth, setWindowWidth] = useState(window.innerWidth);

    const navigate = useNavigate();

    useEffect(() => {
        const handleWindowResize = () => {
            setWindowWidth(window.innerWidth);
        };

        window.addEventListener("resize", handleWindowResize);
    });

    const [showIndividual, setshowIndividual] = useState(false);

    const handleClickIndividual = () => {
        setshowIndividual(true);
    };

    const handleClickGroup = () => {
        setshowIndividual(false);
    };

    const groupTableDetails = [
        {
            groupName: "Family",
            customersCount: "02",
            createdBy: "Sarthak Kanchan",
            createdOn: "20 November, 2022",
            lastUpdated: "20 November, 2022",
            linkOffers: "4",
        },
        {
            groupName: "Top customers",
            customersCount: "195",
            createdBy: "Arjun Patel",
            createdOn: "20 November, 2022",
            lastUpdated: "20 November, 2022",
            linkOffers: "4",
        },
        {
            groupName: "Forgotten customers",
            customersCount: "98",
            createdBy: "Sarthak Kanchan",
            createdOn: "20 November, 2022",
            lastUpdated: "20 November, 2022",
            linkOffers: "4",
        },
        {
            groupName: "Regular customers",
            customersCount: "02",
            createdBy: "Sarthak Kanchan",
            createdOn: "20 November, 2022",
            lastUpdated: "20 November, 2022",
            linkOffers: "4",
        },
    ];

    const individualTableDetails = [
        {
            customerName: "Riddhi Shah",
            mobileNumber: "(+91) 8866886688",
            emailId: "riddhi.shah@gmail.com",
            totalOrder: "713",
            revenueGenerated: "₹559.00/-",
            lastOrder: "22 Sep 2022",
        },
        {
            customerName: "Amrendra Bahubali",
            mobileNumber: "(+91) 8866886688",
            emailId: "riddhi.shah@gmail.com",
            totalOrder: "713",
            revenueGenerated: "₹559.00/-",
            lastOrder: "22 Sep 2022",
        },
        {
            customerName: "Amrendra Bahubali",
            mobileNumber: "(+91) 8866886688",
            emailId: "riddhi.shah@gmail.com",
            totalOrder: "713",
            revenueGenerated: "₹559.00/-",
            lastOrder: "22 Sep 2022",
        },
        {
            customerName: "Amrendra Bahubali",
            mobileNumber: "(+91) 8866886688",
            emailId: "riddhi.shah@gmail.com",
            totalOrder: "713",
            revenueGenerated: "₹559.00/-",
            lastOrder: "22 Sep 2022",
        },
        {
            customerName: "Amrendra Bahubali",
            mobileNumber: "(+91) 8866886688",
            emailId: "riddhi.shah@gmail.com",
            totalOrder: "713",
            revenueGenerated: "₹559.00/-",
            lastOrder: "22 Sep 2022",
        },
        {
            customerName: "Amrendra Bahubali",
            mobileNumber: "(+91) 8866886688",
            emailId: "riddhi.shah@gmail.com",
            totalOrder: "713",
            revenueGenerated: "₹559.00/-",
            lastOrder: "22 Sep 2022",
        },
        {
            customerName: "Amrendra Bahubali",
            mobileNumber: "(+91) 8866886688",
            emailId: "riddhi.shah@gmail.com",
            totalOrder: "713",
            revenueGenerated: "₹559.00/-",
            lastOrder: "22 Sep 2022",
        },
    ];

    const handleClickGroupName = () => {
        navigate("/customer-group-details");
    };

    const [isShowBulkSelectButton, setShowBulkSelectButton] = useState(false);

    const handleClickBulkButton = () => {
        setShowBulkSelectButton(!isShowBulkSelectButton);
    };

    const [IsShowCreateGroupPopUp, SetIsShowCreateGroupPopUp] = useState(false);

    const handleClickCreateGroup = () => {
        SetIsShowCreateGroupPopUp(!IsShowCreateGroupPopUp);
    };

    const handleClickCustomerName = () => {
        if (isShowBulkSelectButton) {
            return;
        }
        navigate("/customer-order-details");
    };

    return (
        <>
            <div className="bg-[#fafafa]">
                <div className="px-8 pb-[70px] lg:px-4 lg:pb-[23px] pt-4 w-full max-w-[1336px] mx-auto bg-white">
                    <div className="mb-4 md:hidden">
                        <DefaultBreadcrumbs mainTab="Customers information" icon={<CustomerIcon className="h-5 w-5" />} />
                    </div>

                    <div className="flex flex-row justify-between border-b border-neutral-300 pb-4 lg:mb-[7px] md:block items-center">
                        <div className="flex flex-row items-center md:block">
                            {/* <div className="min-w-[375px] lg:min-w-[298px] md:min-w-full"> */}
                            <div className="min-w-[375px] w-full mr-2 lg:min-w-[298px] lg:mr-1 md:max-w-full md:mr-0 mobile:min-w-0">
                                <DefaultInputField placeholder="Search media" placeholderIcon={<SearchIcon stroke="#D3D2D8" />} />
                            </div>

                            <div className="w-full ml-4 xl:ml-0 md:mt-4">
                                <div className={`max-w-[156px] md:max-w-full w-full ${isShowBulkSelectButton && "hidden"} cursor-pointer`} onClick={handleClickBulkButton}>
                                    <LargePrimaryButton isDefault={false} name="Bulk select" leftIconDefault={<SelectIcon stroke="#FFFFFF" />} leftIconClick={<SelectIcon stroke="#C4BEED" />} />
                                </div>
                                <div className={`${!isShowBulkSelectButton && "hidden"} flex flex-row items-center`}>
                                    <div className="max-w-[125px] md:max-w-full w-1/2 mr-4 xl:mr-2 cursor-pointer" onClick={handleClickBulkButton}>
                                        <LargeTertiaryButton isDefault={false} name="Cancel" />
                                    </div>
                                    <div className="max-w-[192px] md:max-w-full w-1/2">
                                        <LargeDestructiveButton name="Delete" />
                                    </div>
                                    <span className="paragraphLargeItalic text-neutral-500 min-w-[223px] mx-4 xl:hidden">(Selected images - 02)</span>
                                </div>
                            </div>
                        </div>
                        <div className="flex flex-row md:justify-between md:mt-4">
                            <div className="mx-4 xl:mx-2 md:w-1/2 md:mx-0 md:pr-2 mobile:pr-1">
                                <LargePrimaryButton
                                    name="Filters"
                                    hideName="lg:hidden md:block"
                                    leftIconDefault={windowWidth > 360 && <Filter fill="#FFFFFF" />}
                                    leftIconClick={windowWidth > 360 && <Filter fill="#C4BEED" />}
                                />
                            </div>
                            <div className="md:w-1/2 md:pl-2 mobile:pl-1 min-w-[180px] xl:min-w-[64px]">
                                <LargePrimaryButton
                                    isDefault={false}
                                    name="Add customer"
                                    leftIconDefault={windowWidth > 360 && <AddIcon stroke="#FFFFFF" />}
                                    leftIconClick={windowWidth > 360 && <AddIcon stroke="#C4BEED" />}
                                    hideName="lg:hidden md:block"
                                />
                            </div>
                        </div>
                    </div>

                    <div className="flex flex-row justify-between mb-6 mt-4 md:block md:mb-4">
                        <div className="flex flex-row">
                            <div className="mr-4 max-w-[136px] md:max-w-full md:w-1/2 lg:mr-2 cursor-pointer" onClick={handleClickIndividual}>
                                <Tab label="Individuals" isActive={showIndividual} />
                            </div>
                            <div className="max-w-[131px] md:max-w-full md:w-1/2 md:ml-[7.5px] cursor-pointer" onClick={handleClickGroup}>
                                <Tab label="Groups" isActive={!showIndividual} />
                            </div>
                        </div>

                        <div className="flex flex-row md:mt-4">
                            <div className={`min-w-[173px] w-full mx-4 lg:mx-2 md:min-w-0 md:w-1/2 md:ml-0 md:mr-2 ${!showIndividual && "hidden"} mobile:min-w-0 cursor-pointer`} onClick={handleClickCreateGroup}>
                                <LargePrimaryButton
                                    isDefault={false}
                                    name="Create group"
                                    leftIconDefault={windowWidth > 360 && <AddIcon stroke="#FFFFFF" />}
                                    leftIconClick={windowWidth > 360 && <AddIcon stroke="#C4BEED" />}
                                />
                            </div>
                            <div className={`max-w-[161px] w-full md:max-w-full ${showIndividual && "md:ml-2 md:w-1/2"}`}>
                                <LargePrimaryButton
                                    isDefault={false}
                                    name="Export data"
                                    leftIconDefault={windowWidth > 360 && <ExportIcon stroke="#FFFFFF" />}
                                    leftIconClick={windowWidth > 360 && <ExportIcon stroke="#C4BEED" />}
                                />
                            </div>
                        </div>
                    </div>

                    <div className={`w-full rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden ${showIndividual && "hidden"} border-neutral-300 border`}>
                        <table className="w-full break-words tableMediaLibrary">
                            <thead>
                                <tr className="paragraphOverlineSmall text-neutral-700 shadow-innerShadow bg-neutral-50 text-left justify-center h-11">
                                    <th className="px-6 min-w-[216px] lg:min-w-[190px]">GROUP NAME</th>
                                    <th className="px-6 min-w-[213px] lg:min-w-[187px]">CUSTOMERS COUNT</th>
                                    <th className="px-6 min-w-[204px] lg:min-w-[168px]">CREATED BY</th>
                                    <th className="px-6 min-w-[219px]">CREATED ON</th>
                                    <th className="px-6 min-w-[219px]">LAST UPDATED</th>
                                    <th className="px-6 min-w-[199px]">LINK OFFER</th>
                                </tr>
                            </thead>
                            <tbody>
                                {groupTableDetails.map((el, index) => {
                                    return (
                                        <tr className={`paragraphSmallRegular ${index !== 0 && "border-t"} border-neutral-300 justify-center h-[70px]`} key={index}>
                                            <td className="px-6 cursor-pointer" onClick={handleClickGroupName}>
                                                {el.groupName}
                                            </td>
                                            <td className="px-6">{el.customersCount}</td>
                                            <td className="px-6 text-primary-500">{el.createdBy}</td>
                                            <td className="px-6">{el.createdOn}</td>
                                            <td className="px-6">{el.lastUpdated}</td>
                                            <td className="px-6">
                                                <LinkOffer linkOfferNumber={el.linkOffers} />
                                            </td>
                                        </tr>
                                    );
                                })}
                            </tbody>
                        </table>
                    </div>

                    <div className={`w-full rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden mt-3 md:hidden ${!showIndividual && "hidden"} border-neutral-300 border md:hidden`}>
                        <table className="w-full break-words">
                            <thead>
                                <tr className="paragraphOverlineSmall text-neutral-700 bg-neutral-50 text-left">
                                    <th className="px-6 py-3 min-w-[216px] shadow-innerShadow">CUSTOMER NAME</th>
                                    <th className="px-6 py-3 min-w-[213px] shadow-innerShadow">MOBILE NUMBER</th>
                                    <th className="px-6 py-3 min-w-[204px] shadow-innerShadow">EMAIL ID</th>
                                    <th className="px-6 py-3 min-w-[219px] shadow-innerShadow">TOTAL ORDER</th>
                                    <th className="px-6 py-3 min-w-[219px] shadow-innerShadow">REVENUE GENERATED</th>
                                    <th className="px-6 py-3 min-w-[198px] shadow-innerShadow">LAST ORDER</th>
                                </tr>
                            </thead>
                            <tbody>
                                {individualTableDetails.map((el, index) => {
                                    return <IndividualTableRow key={index} index={index} el={el} isShowBulkSelectButton={isShowBulkSelectButton} handleClickCustomerName={handleClickCustomerName} />;
                                })}
                            </tbody>
                        </table>
                    </div>

                    <div className={`hidden ${!showIndividual && "md:block"}`}>
                        {groupTableDetails.map((el, index) => {
                            return (
                                <div className="mb-2" key={index}>
                                    <ListViewCustomerGroups
                                        groupName={el.groupName}
                                        customerCount={el.customersCount}
                                        createdBy={el.createdBy}
                                        createdOn={el.createdOn}
                                        lastUpdated={el.lastUpdated}
                                        linkOffers={el.linkOffers}
                                        viewDetails={handleClickGroupName}
                                    />
                                </div>
                            );
                        })}
                    </div>

                    <div className={`hidden ${showIndividual && "md:block"}`}>
                        {individualTableDetails.map((el, index) => {
                            return (
                                <div className="mb-2" key={index}>
                                    <ListViewCustomerIndividual
                                        customerName={el.customerName}
                                        mobileNumber={el.mobileNumber}
                                        emailId={el.emailId}
                                        totalOrders={el.totalOrder}
                                        revenue={el.revenueGenerated}
                                        lastOrder={el.lastOrder}
                                        viewDetails={handleClickCustomerName}
                                    />
                                </div>
                            );
                        })}
                    </div>

                    <div className="mt-4 md:hidden">
                        <PaginationWithNumber />
                    </div>

                    <div className={`${!IsShowCreateGroupPopUp && "hidden"}`}>
                        <CreateGroupPopup handleClickCreateGroup={handleClickCreateGroup} />
                    </div>
                </div>
            </div>
        </>
    );
}
