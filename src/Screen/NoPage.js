import React from 'react'

const NoPage = () => {
    return (
        <>
            <div>
                <div className='px-8 pb-[70px] lg:px-4 lg:pb-[23px] pt-4 w-full max-w-[1336px] mx-auto bg-white h-screen'>
                    <h1 className='displaySmallMedium'>404</h1>
                </div>
            </div>
        </>
    )
}

export default NoPage