import React, { useState } from "react";
import ToggleSwitch from "../../../Components/ToggleSwitch/ToggleSwitch";
import { ReactComponent as DominosLogo } from "../../../Assets/dominos-logo.svg";
import { useNavigate } from "react-router-dom";
import ChangeStatusPopup from "./ChangeStatusPopup";

export default function GridViewCard(props) {
    const navigate = useNavigate();

    const handleClickCard = () => {
        navigate("/dashboard");
    };

    const [showChangeStatusPopup, setShowChangeStatusPopup] = useState(false);

    const handleClickChangeOrderStatus = () => {
        setShowChangeStatusPopup(!showChangeStatusPopup);
    };

    return (
        <>
            <div className="relative">
                <div className="border border-neutral-300 rounded-md pt-3 px-4 w-full lg:max-w-full max-w-[329px] cursor-pointer h-[228px] md:w-full md:mx-0 mt-4 md:mt-0" onClick={handleClickCard}>
                    <div className="flex flex-row items-center mb-4">
                        <span>
                            <DominosLogo height={40} width={40} />
                        </span>
                        <span className="paragraphSmallSemiBold max-w-[193px] ml-2 lg:max-w-full lg:pr-12">{props.name}</span>
                    </div>

                    <div className="flex flex-row items-center mb-4">
                        <div className="flex flex-row items-center w-1/2">
                            <div className="paragraphSmallSemiBold text-neutral-400">State:</div>
                            <div className="ml-1 paragraphSmallRegular">Gujarat</div>
                        </div>

                        <div className="flex flex-row items-center w-1/2">
                            <div className="paragraphSmallSemiBold text-neutral-400">City:</div>
                            <div className="ml-1 paragraphSmallRegular">Rajkot</div>
                        </div>
                    </div>

                    <div className="flex flex-row items-start">
                        <div className="paragraphSmallSemiBold text-neutral-400">Address:</div>
                        <p className="paragraphSmallRegular ml-1">Shyamal Infinity, beneath Radio Mirchi, kalawad road</p>
                    </div>
                </div>

                <div className="absolute flex flex-row bottom-12 left-4">
                    <div className="paragraphSmallSemiBold text-neutral-400">Dine-in:</div>
                    <div className="font-normal text-sm leading-5 ml-1 text-destructive-500 cursor-default" onClick={handleClickChangeOrderStatus}>
                        INACTIVE
                    </div>
                </div>

                <div className="absolute flex flex-row bottom-12 left-[161px] lg:left-1/2">
                    <div className="paragraphSmallSemiBold text-neutral-400">Takeaway:</div>
                    <div className="font-normal text-sm leading-5 ml-1 text-destructive-500 cursor-default" onClick={handleClickChangeOrderStatus}>
                        INACTIVE
                    </div>
                </div>

                <div className="absolute flex flex-row bottom-3 left-4">
                    <div className="paragraphSmallSemiBold text-neutral-400">QSR:</div>
                    <div className="font-normal text-sm leading-5 ml-1 text-destructive-500 cursor-default" onClick={handleClickChangeOrderStatus}>
                        INACTIVE
                    </div>
                </div>

                <div className="absolute flex flex-row bottom-3 left-[161px] lg:left-1/2">
                    <div className="paragraphSmallSemiBold text-neutral-400">Delivery:</div>
                    <div className="font-normal text-sm leading-5 ml-1 text-destructive-500 cursor-default" onClick={handleClickChangeOrderStatus}>
                        INACTIVE
                    </div>
                </div>

                <div className="top-5 right-4 absolute">
                    <ToggleSwitch cursorType="cursor-default" />
                </div>
            </div>

            <div className={`${!showChangeStatusPopup && "hidden"}`}>
                <ChangeStatusPopup handleClickClose={handleClickChangeOrderStatus} />
            </div>
        </>
    );
}
