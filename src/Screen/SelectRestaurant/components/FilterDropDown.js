import React, { Fragment, useState } from "react";
import DropDown from "../../../Components/DropDown/DropDown";
import { Menu, Transition } from "@headlessui/react";
import { ReactComponent as DownArrowIcon } from "../../../Assets/chevron-down.svg";

export const FilterDropDownMobile = (props) => {
    const menuItems = ["State", "City", "Dine-in", "Takeaway", "QSR", "Delivery"];

    return (
        <div className="h-[624px] w-[343px] rounded-xl bg-shades-50 px-8 py-6 m-auto mobile:w-full md:px-4 md:py-4">
            <span className="paragraphLargeSemiBold">Select filters</span>
            <div>
                {menuItems.map((el, index) => {
                    return (
                        <div className="pt-[5px] mb-[5px] relative" key={index}>
                            <FilterDropDown label={el} onChange={props.onChange} />
                        </div>
                    );
                })}
            </div>

            <div className="flex flex-row justify-between mt-4">
                <button className="h-12 w-[196px] md:w-1/2 md:mr-[7.5px] paragraphMediumMedium rounded-md text-neutral-500 bg-shades-50 border border-neutral-300" onClick={props.handleClickFilter}>
                    Cancel
                </button>
                <button className="h-12 w-[196px] md:w-1/2 md:ml-[7.5px] paragraphMediumMedium rounded-md text-shades-50 bg-primary-500" onClick={props.handleClickFilter}>
                    Apply
                </button>
            </div>
        </div>
    );
};

export const FilterDropDown = (props) => {
    const menuItems = ["All", "Active", "Inactive"];

    const [dropDownLabel, setDropDownLabel] = useState(menuItems[0]);

    const handleClickMenuItem = (el) => {
        setDropDownLabel(el);
        props.onChange(props.label, el);
    };

    const handleClickClear = () => {
        setDropDownLabel(menuItems[0]);
        props.onChange(props.label, "All");
    };

    return (
        <>
            <div className="flex flex-row justify-between">
                <div className="paragraphSmallMedium mb-1">{props.label}</div>
                {dropDownLabel !== menuItems[0] && (
                    <div className="text-primary-500 paragraphSmallMedium cursor-pointer" onClick={handleClickClear}>
                        Clear
                    </div>
                )}
            </div>

            <div className="relative">
                <Menu as="div">
                    <div className="dropDownIcon">
                        <Menu.Button className="shadow-shadowXsmall w-full flex flex-row justify-between mobile:max-w-full rounded-md outline-none focus:border-primary-500 border py-3 focus:ring-4 focus:ring-primary-100 appearance-none px-4 border-neutral-300 h-12 paragraphSmallRegular">
                            {dropDownLabel}
                            <DownArrowIcon className={`dropDownIconRotate min-w-[24px] min-h-[24px]`} />
                        </Menu.Button>
                    </div>
                    <Transition
                        as={Fragment}
                        enter="transition ease-out duration-100"
                        enterFrom="transform opacity-0 scale-95"
                        enterTo="transform opacity-100 scale-100"
                        leave="transition ease-in duration-75"
                        leaveFrom="transform opacity-100 scale-100"
                        leaveTo="transform opacity-0 scale-95"
                    >
                        <Menu.Items className="absolute left-0 right-0 mt-2 px-4 py-2 border paragraphSmallMedium rounded-md shadow-shadowMedium bg-shades-50 font-normal z-50">
                            {menuItems.map((el, index) => {
                                return (
                                    <div className="pt-2 mb-2 cursor-pointer w-full" key={index} onClick={() => handleClickMenuItem(el)}>
                                        <Menu.Item>
                                            <div className="paragraphSmallRegular hover:paragraphSmallSemiBold hover:text-primary-500">{el}</div>
                                        </Menu.Item>
                                    </div>
                                );
                            })}
                        </Menu.Items>
                    </Transition>
                </Menu>
            </div>
        </>
    );
};
