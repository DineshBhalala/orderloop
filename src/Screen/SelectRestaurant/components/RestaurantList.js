import React, { useState } from "react";
import GridViewCard from "./GridViewCards";
import TableView from "./TableView";

export default function RestaurantList(props) {
    const OutletName = [
        "Domino's Pizza - Master Outlet(All)",
        "Domino's Pizza - Univerisity Road Outlet",
        "Domino's Pizza - Tagore Road Outlet",
        "Domino's Pizza - Raiya Road Outlet",
        "Domino's Pizza - Gondal Road Outlet",
        "Domino's Pizza - Kalawad Road Outlet",
        "Domino's Pizza - Ahmedabad Highway Outlet",
        "Domino's Pizza - Jamnagar Highway Outlet",
    ];

    const [showHideOutletDetails, setShowHideOutletDetails] = useState(false);

    const [showHideText, setShowHideText] = useState("Show all");

    const handleClickShowHideText = () => {
        setShowHideOutletDetails(!showHideOutletDetails);
        setShowHideText(showHideOutletDetails ? "Show all" : "Hide all");
    };

    return (
        <>
            <div className="mt-8 pb-8 border-b border-neutral-300 md:pb-5 md:mt-5">
                <div className="flex flex-row items-center justify-between">
                    <div className="flex flex-row items-center">
                        <h3 className="headingH3MediumDesktop md:headingH6MediumDesktop md:tracking-normal mobile:text-base">{props.name}</h3>
                        <span className="ml-2 paragraphLargeItalic md:paragraphSmallItalic">(7 outlets)</span>
                    </div>

                    <span className="paragraphMediumRegular text-primary-500 md:paragraphSmallRegular md:text-primary-500 cursor-pointer" onClick={handleClickShowHideText}>
                        {showHideText}
                    </span>
                </div>

                <div className={`mt-2 ${!showHideOutletDetails && "hidden"} mobile:mt-1 md:mt-1`}>
                    <div className={`${!props.showGrid && "hidden"} -mx-2.5 lg:-mx-0`}>
                        {OutletName.map((el, index) => {
                            return (
                                <div key={index} className="mx-2.5 inline-block lg:w-1/2 lg:mx-0 lg:even:pl-[4.5px] lg:odd:pr-[4.5px] md:w-full md:even:pl-0 md:odd:pr-0 cursor-pointer md:mt-2">
                                    <GridViewCard name={el} />
                                </div>
                            );
                        })}
                    </div>
                    <div className={`${props.showGrid && "hidden"}`}>
                        <TableView OutletName={OutletName} />
                    </div>
                </div>
            </div>
        </>
    );
}
