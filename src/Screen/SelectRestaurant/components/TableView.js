import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import ToggleSwitch from "../../../Components/ToggleSwitch/ToggleSwitch";
import ListViewSelectRestaurant from "../../../Components/ListView/ListViewSelectRestaurant";
import ChangeStatusPopup from "./ChangeStatusPopup";

export default function TableView(props) {
    const navigate = useNavigate();

    const handleClickRestaurant = () => {
        navigate("/dashboard");
    };

    const [showChangeStatusPopup, setShowChangeStatusPopup] = useState(false);

    const handleClickChangeOrderStatus = () => {
        setShowChangeStatusPopup(!showChangeStatusPopup);
    };

    return (
        <>
            <div className="w-full border border-neutral-300 rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden mt-6 md:hidden">
                <table className="w-full break-words tableMediaLibrary">
                    <thead>
                        <tr className="bg-neutral-50 text-left paragraphOverlineSmall text-neutral-700 h0">
                            <th className="shadow-innerShadow px-6 min-w-[90px] sticky left-0 z-10 bg-neutral-50 h-11 justify-center">STATE</th>
                            <th className="shadow-innerShadow px-6 min-w-[302px] sticky left-[91px] z-10 bg-neutral-50">OUTLET NAME</th>
                            <th className="shadow-innerShadow px-6 min-w-[99px]">STATE</th>
                            <th className="shadow-innerShadow px-6 min-w-[93px]">CITY</th>
                            <th className="shadow-innerShadow px-6 min-w-[344px]">ADDRESS</th>
                            <th className="shadow-innerShadow px-6 min-w-[103px]">DINE-IN</th>
                            <th className="shadow-innerShadow px-6 min-w-[119px]">TAKEAWAY</th>
                            <th className="shadow-innerShadow px-6 min-w-[102px]">QSR</th>
                            <th className="shadow-innerShadow px-6 min-w-[110px]">DELIVERY</th>
                        </tr>
                    </thead>
                    <tbody className="paragraphSmallRegular">
                        {props.OutletName.map((el, index) => (
                            <tr key={index} className={`${index !== 0 && "border-t"} even:bg-neutral-50 border-neutral-300 paragraphSmallRegular h-[70px] justify-center`}>
                                <td className={`px-6 py-[23px] sticky left-0 z-10 ${index !== 0 && "border-t border-neutral-300"}`}>
                                    <ToggleSwitch />
                                </td>
                                <td className="px-6 left-[91px] z-10 sticky cursor-pointer" onClick={handleClickRestaurant}>
                                    {el}
                                </td>
                                <td className="px-6">Gujrat</td>
                                <td className="px-6">Rajkot</td>
                                <td className="px-6">Shyamal Infinity, beneath Radio Mirchi, kalawad road</td>
                                <td className="px-6 text-tertiary-800 text-sm font-normal cursor-pointer" onClick={handleClickChangeOrderStatus}>
                                    ACTIVE
                                </td>
                                <td className="px-6 text-destructive-500 text-sm font-normal cursor-pointer" onClick={handleClickChangeOrderStatus}>
                                    INACTIVE
                                </td>
                                <td className="px-6 text-tertiary-800 text-sm font-normal cursor-pointer" onClick={handleClickChangeOrderStatus}>
                                    ACTIVE
                                </td>
                                <td className="px-6 text-destructive-500 text-sm font-normal cursor-pointer" onClick={handleClickChangeOrderStatus}>
                                    INACTIVE
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
            <div className="hidden md:block mt-2">
                {props.OutletName.map((row, index) => {
                    return (
                        <div key={index} className="py-1">
                            <ListViewSelectRestaurant name={row} />
                        </div>
                    );
                })}
            </div>
            <div className={`${!showChangeStatusPopup && "hidden"}`}>
                <ChangeStatusPopup handleClickClose={handleClickChangeOrderStatus} />
            </div>
        </>
    );
}
