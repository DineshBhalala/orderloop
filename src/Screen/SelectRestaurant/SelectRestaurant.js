import React, { useState, useEffect } from "react";
import { LargePrimaryButton } from "../../Components/Buttons/Button";
import { ReactComponent as Logo } from "../../Assets/orderloop-logo.svg";
import { DefaultInputField } from "../../Components/InputField/InputField";
import { ReactComponent as Search } from "../../Assets/search.svg";
import { ReactComponent as View } from "../../Assets/view.svg";
import { ReactComponent as GridViewIcon } from "../../Assets/grid-view.svg";
import { ReactComponent as Filter } from "../../Assets/filter.svg";
import AvatarsIcon from "../../Components/Avatar/AvatarsIcon";
import AvatarImage from "../../Assets/avatar-image.png";
import { ReactComponent as OrderloopIcon } from "../../Assets/orderloop-logo.svg";
import { Tab } from "../../Components/Tabs/Tabs";
import RestaurantList from "./Components/RestaurantList";
import DropDownAvatar from "../../Components/DropDown/DropDownAvatar";
import { DropDownTabs } from "../../Components/DropDown/DropDownTabs";
import ChangeStatusPopup from "./Components/ChangeStatusPopup";
import { FilterDropDown, FilterDropDownMobile } from "./Components/FilterDropDown";

export default function SelectRestaurant() {
    const [isShownFilters, setIsShownFilters] = useState(false);

    const handleClickFilter = () => {
        setIsShownFilters(!isShownFilters);
    };

    const [showGrid, setShowGrid] = useState(false);

    const handleClickGridTab = () => {
        setShowGrid(true);
    };

    const handleClickListTab = () => {
        setShowGrid(false);
    };

    const restaurantName = ["Domino's Pizza", "La Pino'z Pizza", "La Milano Pizzeria"];

    const [showChangeStatusPopup, setShowChangeStatusPopup] = useState(false);

    const handleClickAddRestaurant = () => {
        setShowChangeStatusPopup(!showChangeStatusPopup);
    };

    const [isHeaderSticky, setHeaderSticky] = useState(false);

    useEffect(() => {
        window.addEventListener("scroll", () => {
            setHeaderSticky(window.scrollY > 0);
        });
    }, []);

    const [filterDropDownSelected, setFilterDropDownSelected] = useState([
        { label: "State", filter: "All" },
        { label: "City", filter: "All" },
        { label: "Dine-in", filter: "All" },
        { label: "Takeaway", filter: "All" },
        { label: "QSR", filter: "All" },
        { label: "Delivery", filter: "All" },
    ]);

    const handleChangeFilterDropDown = (el, filter) => {
        setFilterDropDownSelected((prevState) =>
            prevState.map((item) => {
                if (item.label === el) {
                    return { ...item, filter: filter };
                }
                return item;
            })
        );
    };

    const [badgeTextOnFilterButton, setBadgeTextOnFilterButton] = useState(0);

    useEffect(() => {
        const count = filterDropDownSelected.filter((item) => item.filter !== "All").length;
        setBadgeTextOnFilterButton(count);
    }, [filterDropDownSelected]);

    return (
        <>
            <div>
                <div
                    className={`hidden md:flex items-center justify-between bg-primary-500 px-4 fixed top-0 left-0 right-0 z-10 transitionEffect ${
                        isHeaderSticky ? "py-2.5 shadow-shadowWhite" : "pt-[60px] pb-4"
                    }`}
                >
                    <span className="paragraphLargeSemiBold text-white m-auto">
                        <OrderloopIcon fill="#FFFFFF" height={32} />
                    </span>

                    <span className="absolute right-0 lg:right-4">
                        <AvatarsIcon widthOuter="8" heightOuter="8" imageIcon={AvatarImage} colorIcon="[#22C55E]" widthIcon="3" heightIcon="3" />
                    </span>
                </div>

                <div className="px-8 pb-[70px] lg:px-4 lg:pb-[23px] md:pt-[108px] w-full max-w-[1440px] mx-auto bg-white">
                    <div className="flex flex-row justify-center relative h-20 mb-6 items-center md:hidden lg:border-b lg:border-neutral-300 lg:-mx-4 lg:px-4 lg:bg-[#FAFAFA] lg:sticky lg:top-0 lg:z-20">
                        <div className="w-[217px]">
                            <Logo />
                        </div>

                        <span className="absolute right-0 lg:right-4">
                            <DropDownAvatar label="Shyam T." widthIcon="16" heightIcon="16" />
                        </span>
                    </div>

                    <div className="flex flex-row justify-between md:mt-4 md:block -mb-1">
                        <div className="max-w-[375px] w-full md:max-w-full md:w-full md:mb-4">
                            <DefaultInputField placeholder="Search outlet" placeholderIcon={<Search stroke="#D3D2D8" />} shadow="shadow-shadowXsmall" />
                        </div>

                        <div className="flex flex-row lg:ml-[52px] md:ml-0">
                            <div className="mr-3 md:hidden lg:mr-1 cursor-pointer" onClick={handleClickFilter}>
                                <LargePrimaryButton
                                    leftIconDefault={<Filter fill="#ffffff" />}
                                    leftIconClick={<Filter fill="#C4BEED" />}
                                    name="Filters"
                                    badgeText={badgeTextOnFilterButton !== 0 && badgeTextOnFilterButton}
                                />
                            </div>

                            <div className="hidden md:block w-1/2">
                                <LargePrimaryButton
                                    leftIconDefault={<Filter fill="#ffffff" />}
                                    leftIconClick={<Filter fill="#C4BEED" />}
                                    name="Filters"
                                    isDefault={false}
                                    badgeText={badgeTextOnFilterButton !== 0 && badgeTextOnFilterButton}
                                />
                            </div>

                            <div className="flex flex-row md:hidden">
                                <div onClick={handleClickGridTab} className="h-12 mx-3 lg:mx-1 3xl:w-[130px] cursor-pointer">
                                    <Tab
                                        label="Grid view"
                                        IconDefault={<GridViewIcon />}
                                        IconClick={<GridViewIcon stroke="#6C5DD3" />}
                                        isActive={showGrid}
                                        labelStyle="paragraphSmallMedium ml-2 mr-1"
                                    />
                                </div>

                                <div className="h-12 ml-3 lg:ml-1 3xl:w-[125px] cursor-pointer" onClick={handleClickListTab}>
                                    <Tab label="List view" IconDefault={<View />} IconClick={<View stroke="#6C5DD3" />} isActive={!showGrid} labelStyle="paragraphSmallMedium ml-2 mr-1" />
                                </div>
                            </div>

                            <div className="w-full ml-2 mobile:ml-1 hidden md:block md:w-1/2">
                                <DropDownTabs
                                    menuItems={[
                                        { item: "List view", onClick: handleClickListTab, icon: <View />, iconClick: <View stroke="#6C5DD3" /> },
                                        { item: "Grid view", onClick: handleClickGridTab, icon: <GridViewIcon />, iconClick: <GridViewIcon stroke="#6C5DD3" /> },
                                    ]}
                                />
                            </div>
                        </div>
                    </div>

                    <div className={`${!isShownFilters && "hidden"} md:hidden -mx-2.5 mt-2 lg:-mx-1`}>
                        {filterDropDownSelected.map((el, index) => {
                            return (
                                <div key={index} className="inline-block mobile:hidden w-[213px] lg:w-[178px] mt-4 relative mx-[9.8px] lg:mx-1">
                                    <FilterDropDown label={el.label} onChange={handleChangeFilterDropDown} />
                                </div>
                            );
                        })}
                    </div>

                    {restaurantName.map((el, index) => {
                        return <RestaurantList key={index} name={el} showGrid={showGrid} />;
                    })}
                </div>
            </div>

            <div className={`fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 mobile:px-4 ${!isShownFilters ? "hidden" : "hidden md:flex"}`}>
                <FilterDropDownMobile handleClickFilter={handleClickFilter} onChange={handleChangeFilterDropDown} />
            </div>

            <div className={`${!showChangeStatusPopup && "hidden"}`}>
                <ChangeStatusPopup handleClickClose={handleClickAddRestaurant} />
            </div>
        </>
    );
}
