import React, { useState } from "react";
import { LargePrimaryButton } from "../../Components/Buttons/Button";
import { ReactComponent as RiderIcon } from "../../Assets/riders.svg";
import { ReactComponent as Filter } from "../../Assets/filter.svg";
import { ReactComponent as Export } from "../../Assets/export.svg";
import { ReactComponent as OrderIcon } from "../../Assets/order.svg";
import { ReactComponent as OrderRatingIcon } from "../../Assets/order-ratings.svg";
import { ReactComponent as SelectIcon } from "../../Assets/select.svg";
import { ReactComponent as ReviewIcon } from "../../Assets/review.svg";
import DashBoardCard from "../../Components/DashBoardCard/DashBoardCard";
import PaginationNumber from "../../Components/Pagination/PaginationWithNumber";
import { DefaultBreadcrumbs } from "../../Components/Breadcrumbs/Breadcrumbs";
import CustomerReviewDetails from "./Components/CustomerReviewDetails";
import SliderDashboard from "react-slick";
import ListViewOrderRating from "../../Components/ListView/ListViewOrderRating";
import CalenderField from "../../Components/Calender/CalenderField";

export default function OrderRating() {
  const [showCustomerReview, setshowCustomerReview] = useState(false);

  const handleClickOrderId = () => {
    setshowCustomerReview(!showCustomerReview);
  };

  const orderRattingTableDetails = [
    {
      orderId: "#BBQR",
      customerName: "Riddhi Shah",
      orderRating: Number(5.0),
      orderFeedback: "The food was amazing as usual, completely fell in l... ",
      riderRating: Number(5.0),
      riderFeedback: "-",
      orderDate: "18 Nov 2022",
    },
    {
      orderId: "#BBQR",
      customerName: "Amrendra Bahubali",
      orderRating: Number(5.0),
      orderFeedback: "The food was amazing as usual, completely fell in l... ",
      riderRating: Number(4.5),
      riderFeedback: "-",
      orderDate: "6 Nov 2022",
    },
    {
      orderId: "#BBQR",
      customerName: "Amrendra Bahubali",
      orderRating: Number(2.0),
      orderFeedback: "The food was amazing as usual, completely fell in l... ",
      riderRating: Number(3),
      riderFeedback: "The food was amazing as usual, completely fell in l... ",
      orderDate: "5 Nov 2022",
    },
    {
      orderId: "#BBQR",
      customerName: "Amrendra Bahubali",
      orderRating: Number(5.0),
      orderFeedback: "The food was amazing as usual, completely fell in l... ",
      riderRating: Number(4.5),
      riderFeedback: "-",
      orderDate: "4 Nov 2022",
    },
    {
      orderId: "#BBQR",
      customerName: "Amrendra Bahubali",
      orderRating: Number(3),
      orderFeedback: "The food was amazing as usual, completely fell in l... ",
      riderRating: Number(4.5),
      riderFeedback: "The food was amazing as usual, completely fell in l... ",
      orderDate: "4 Nov 2022",
    },
    {
      orderId: "#BBQR",
      customerName: "Amrendra Bahubali",
      orderRating: Number(3),
      orderFeedback: "The food was amazing as usual, completely fell in l... ",
      riderRating: Number(4.5),
      riderFeedback: "The food was amazing as usual, completely fell in l... ",
      orderDate: "4 Nov 2022",
    },
  ];

  var settingsDashboardSlider = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    className: "dashboardSlide",
  };

  return (
    <>
      <div className="bg-[#fafafa]">
        <div className={`px-8 pb-[70px] lg:px-4 lg:pb-[23px] pt-4 w-full max-w-[1336px] mx-auto bg-white ${showCustomerReview && "md:hidden"}`}>
          <div className="mb-4 md:hidden">
            <DefaultBreadcrumbs mainTab="Order ratings" icon={<OrderRatingIcon className="h-5 w-5" />} />
          </div>

          <div className="flex flex-row justify-between border-b border-neutral-300 pb-4 lg:mb-[7px] md:block">
            <div className="flex flex-row md:justify-between">
              <CalenderField label="25 Sept 2022 - 09 Oct 2022" buttonStyle="mobile:pr-0 mobile:pl-1 md:w-full" labelStyle="mobile:text-sm mobile:mx-0" />
              <div className="max-w-[156px] w-full md:max-w-full md:w-[56px] ml-4 lg:ml-2">
                <LargePrimaryButton name="Bulk select" leftIconDefault={<SelectIcon stroke="#ffffff" />} leftIconClick={<SelectIcon stroke="#C4BEED" />} hideName="md:hidden" />
              </div>
            </div>
            <div className="flex flex-row md:justify-between md:mt-4">
              <div className="mx-4 md:w-1/2 md:ml-0 md:mr-2 mobile:mr-1">
                <LargePrimaryButton name="Filters" leftIconDefault={<Filter fill="#FFFFFF" />} leftIconClick={<Filter fill="#C4BEED" />} />
              </div>
              <div className="md:w-1/2 md:ml-2 mobile:ml-1">
                <LargePrimaryButton
                  isDefault={false}
                  name="Export data"
                  leftIconDefault={<Export stroke="#FFFFFF" />}
                  leftIconClick={<Export stroke="#C4BEED" />}
                  hideName="lg:hidden md:block"
                />
              </div>
            </div>
          </div>

          <div className="-mx-[11px] mb-4 lg:-mx-[1px] md:hidden">
            <div className="max-w-[303px] w-full inline-block mx-2.5 mt-4 lg:max-w-full lg:w-1/2 lg:mx-0 lg:pr-1 lg:mt-[9px] align-top">
              <DashBoardCard
                isReimbursementIcon={false}
                title="Total reviews"
                amount="1.5k"
                percentage="10.2"
                profit={true}
                reimbursement="Growth in new reviews recorded this week"
                icon={<ReviewIcon stroke="#ffffff" />}
              />
            </div>

            <div className="lg:pl-1 inline-block align-top max-w-[303px] w-full mx-2.5 mt-4 lg:max-w-full lg:w-1/2 lg:mx-0 lg:mt-[9px]">
              <div className="px-4 py-3 border border-neutral-300 rounded-md">
                <div className="flex flex-row items-center paragraphXSmallRegular">
                  <div className="flex flex-row items-center">
                    <OrderRatingIcon fill="#EBF6F5" stroke="#3D8C82" className="h-5 w-5 lg:h-4 lg:w-4" />
                    <span className="ml-1">5.0</span>
                  </div>
                  <div className="mx-2 w-full border border-neutral-300 rounded-[20px] h-1.5 relative">
                    <div className="absolute h-1.5 bg-tertiary-800 rounded-[20px] -left-[1px] -top-[1px]" style={{ width: "89%" }} />
                  </div>
                  <span>1.2k</span>
                </div>

                <div className="flex flex-row items-center paragraphXSmallRegular my-[3.5px]">
                  <div className="flex flex-row items-center">
                    <OrderRatingIcon fill="#EBF6F5" stroke="#3D8C82" className="h-5 w-5 lg:h-4 lg:w-4" />
                    <span className="ml-1">4.0</span>
                  </div>
                  <div className="mx-2 w-full border border-neutral-300 rounded-[20px] h-1.5 relative">
                    <div className="absolute h-1.5 bg-tertiary-800 rounded-[20px] -left-[1px] -top-[1px]" style={{ width: "61%" }} />
                  </div>
                  <span>150</span>
                </div>

                <div className="flex flex-row items-center paragraphXSmallRegular">
                  <div className="flex flex-row items-center">
                    <OrderRatingIcon fill="#FFF5E3" stroke="#FFA704" className="h-5 w-5 lg:h-4 lg:w-4" />
                    <span className="ml-1">3.0</span>
                  </div>
                  <div className="mx-2 w-full border border-neutral-300 rounded-[20px] h-1.5 relative">
                    <div className="absolute h-1.5 bg-secondary-700 rounded-[20px] -left-[1px] -top-[1px]" style={{ width: "32%" }} />
                  </div>
                  <span>98</span>
                </div>

                <div className="flex flex-row items-center paragraphXSmallRegular my-[3.5px]">
                  <div className="flex flex-row items-center">
                    <OrderRatingIcon fill="#FEF2F2" stroke="#EF4444" className="h-5 w-5 lg:h-4 lg:w-4" />
                    <span className="ml-1">2.0</span>
                  </div>
                  <div className="mx-2 w-full border border-neutral-300 rounded-[20px] h-1.5 relative">
                    <div className="absolute h-1.5 bg-destructive-500 rounded-[20px] -left-[1px] -top-[1px]" style={{ width: "13%" }} />
                  </div>
                  <span>25</span>
                </div>

                <div className="flex flex-row items-center paragraphXSmallRegular">
                  <div className="flex flex-row items-center">
                    <OrderRatingIcon fill="#FEF2F2" stroke="#EF4444" className="h-5 w-5 lg:h-4 lg:w-4" />
                    <span className="ml-1.5">1.0</span>
                  </div>
                  <div className="mx-2 w-full border border-neutral-300 rounded-[20px] h-1.5 relative">
                    <div className="absolute h-1.5 bg-destructive-500 rounded-[20px] -left-[1px] -top-[1px]" style={{ width: "22%" }} />
                  </div>
                  <span>45</span>
                </div>
              </div>
            </div>

            <div className="max-w-[303px] w-full inline-block mx-2.5 mt-4 lg:max-w-full lg:w-1/2 lg:mx-0 lg:pr-1 lg:mt-[9px] align-top">
              <DashBoardCard
                isReimbursementIcon={false}
                title="Average order rating"
                amount="750"
                reimbursement="Order rating received was good by 1.5k customers this week"
                focusReimbursementContentColor="tertiary-800"
                reimbursementFocusContent="good"
                icon={<OrderIcon stroke="#ffffff" />}
              />
            </div>

            <div className="max-w-[303px] w-full inline-block mx-2.5 mt-4 lg:max-w-full lg:w-1/2 lg:mx-0 lg:pl-1 lg:mt-[9px] align-top">
              <DashBoardCard
                ratingCard={true}
                title="Average rider rating"
                amount="3.0"
                reimbursement="Rider rating received was average by 1.5k customers this week"
                isReimbursementIcon={false}
                reimbursementFocusContent="average"
                focusReimbursementContentColor="secondary-700"
                icon={<RiderIcon stroke="#ffffff" />}
                startFillColor="#FFF5E3"
                startStrokeColor="#FFA704"
              />
            </div>
          </div>

          <div className="hidden md:block -mx-4">
            <SliderDashboard {...settingsDashboardSlider}>
              <div className="max-w-[303px] w-full inline-block mt-4 lg:max-w-full lg:mt-[9px] align-top">
                <DashBoardCard
                  isReimbursementIcon={false}
                  title="Total reviews"
                  amount="1.5k"
                  percentage="10.2"
                  profit={true}
                  reimbursement="Growth in new reviews recorded this week"
                  icon={<ReviewIcon stroke="#ffffff" />}
                />
              </div>

              <div className="inline-block align-top max-w-[303px] w-full mt-4 lg:max-w-full lg:mt-[9px]">
                <div className="px-4 py-3 border border-neutral-300 rounded-md">
                  <div className="flex flex-row items-center paragraphXSmallRegular">
                    <div className="flex flex-row items-center">
                      <OrderRatingIcon fill="#EBF6F5" stroke="#3D8C82" className="h-5 w-5 lg:h-4 lg:w-4" />
                      <span className="ml-1">5.0</span>
                    </div>
                    <div className="mx-2 w-full border border-neutral-300 rounded-[20px] h-1.5 relative">
                      <div className="absolute h-1.5 bg-tertiary-800 rounded-[20px] -left-[1px] -top-[1px]" style={{ width: "89%" }} />
                    </div>
                    <span>1.2k</span>
                  </div>

                  <div className="flex flex-row items-center paragraphXSmallRegular my-[3.5px]">
                    <div className="flex flex-row items-center">
                      <OrderRatingIcon fill="#EBF6F5" stroke="#3D8C82" className="h-5 w-5 lg:h-4 lg:w-4" />
                      <span className="ml-1">4.0</span>
                    </div>
                    <div className="mx-2 w-full border border-neutral-300 rounded-[20px] h-1.5 relative">
                      <div className="absolute h-1.5 bg-tertiary-800 rounded-[20px] -left-[1px] -top-[1px]" style={{ width: "61%" }} />
                    </div>
                    <span>150</span>
                  </div>

                  <div className="flex flex-row items-center paragraphXSmallRegular">
                    <div className="flex flex-row items-center">
                      <OrderRatingIcon fill="#FFF5E3" stroke="#FFA704" className="h-5 w-5 lg:h-4 lg:w-4" />
                      <span className="ml-1">3.0</span>
                    </div>
                    <div className="mx-2 w-full border border-neutral-300 rounded-[20px] h-1.5 relative">
                      <div className="absolute h-1.5 bg-secondary-700 rounded-[20px] -left-[1px] -top-[1px]" style={{ width: "32%" }} />
                    </div>
                    <span>98</span>
                  </div>

                  <div className="flex flex-row items-center paragraphXSmallRegular my-[3.5px]">
                    <div className="flex flex-row items-center">
                      <OrderRatingIcon fill="#FEF2F2" stroke="#EF4444" className="h-5 w-5 lg:h-4 lg:w-4" />
                      <span className="ml-1">2.0</span>
                    </div>
                    <div className="mx-2 w-full border border-neutral-300 rounded-[20px] h-1.5 relative">
                      <div className="absolute h-1.5 bg-destructive-500 rounded-[20px] -left-[1px] -top-[1px]" style={{ width: "13%" }} />
                    </div>
                    <span>25</span>
                  </div>

                  <div className="flex flex-row items-center paragraphXSmallRegular">
                    <div className="flex flex-row items-center">
                      <OrderRatingIcon fill="#FEF2F2" stroke="#EF4444" className="h-5 w-5 lg:h-4 lg:w-4" />
                      <span className="ml-1.5">1.0</span>
                    </div>
                    <div className="mx-2 w-full border border-neutral-300 rounded-[20px] h-1.5 relative">
                      <div className="absolute h-1.5 bg-destructive-500 rounded-[20px] -left-[1px] -top-[1px]" style={{ width: "22%" }} />
                    </div>
                    <span>45</span>
                  </div>
                </div>
              </div>

              <div className="max-w-[303px] w-full inline-block mt-4 lg:max-w-full lg:mt-[9px] align-top">
                <DashBoardCard
                  isReimbursementIcon={false}
                  title="Average order rating"
                  amount="750"
                  reimbursement="Order rating received was good by 1.5k customers this week"
                  focusReimbursementContentColor="tertiary-800"
                  reimbursementFocusContent="good"
                  icon={<OrderIcon stroke="#ffffff" />}
                />
              </div>

              <div className="max-w-[303px] w-full inline-block mt-4 lg:max-w-full lg:mt-[9px] align-top">
                <DashBoardCard
                  ratingCard={true}
                  title="Average rider rating"
                  amount="3.0"
                  reimbursement="Rider rating received was average by 1.5k customers this week"
                  isReimbursementIcon={false}
                  reimbursementFocusContent="average"
                  focusReimbursementContentColor="secondary-700"
                  icon={<RiderIcon stroke="#ffffff" />}
                  startFillColor="#FFF5E3"
                  startStrokeColor="#FFA704"
                />
              </div>
            </SliderDashboard>
          </div>

          <div className="flex justify-end mb-6 md:fixed md:bottom-0 md:left-0 md:right-0 md:mb-0">
            <div className="md:px-4 md:pt-2 md:pb-1 md:bg-white md:shadow-dropShadow max-w-[464px] w-full md:max-w-full">
              <div className="flex flex-row px-4 py-3 border border-neutral-300 justify-between items-center w-full rounded-md">
                <span className="md:hidden">Rating guide</span>
                <div className="flex flex-row items-center">
                  <div className="h-6 w-6 rounded border-[1.5px] bg-tertiary-100 border-tertiary-800 mr-1" />
                  <span className="paragraphMediumRegular text-[#000000]">Good</span>
                </div>
                <div className="flex flex-row items-center">
                  <div className="h-6 w-6 rounded border-[1.5px] bg-secondary-100 border-secondary-700 mr-1" />
                  <span className="paragraphMediumRegular text-[#000000]">Average</span>
                </div>
                <div className="flex flex-row items-center">
                  <div className="h-6 w-6 rounded border-[1.5px] bg-destructive-50 border-destructive-500 mr-1" />
                  <span className="paragraphMediumRegular text-[#000000]">Bad</span>
                </div>
              </div>
            </div>
          </div>

          <div className="w-full border border-neutral-300 rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden">
            <table className="w-full break-words">
              <thead>
                <tr className="bg-neutral-50 text-left paragraphOverlineSmall text-neutral-700 shadow-innerShadow">
                  <th className="px-6 py-3 min-w-[120px] lg:min-w-[113px]">ORDER ID</th>
                  <th className="px-6 py-3 min-w-[230px] lg:min-w-[177px]">CUSTOMER NAME</th>
                  <th className="px-6 py-3 min-w-[162px] lg:min-w-[150px]">ORDER RATING</th>
                  <th className="px-6 py-3 min-w-[227px] lg:min-w-[222px]">ORDER FEEDBACK</th>
                  <th className="px-6 py-3 min-w-[147px]">RIDER RATING</th>
                  <th className="px-6 py-3 min-w-[227px] lg:min-w-[]">RIDER FEEDBACK</th>
                  <th className="px-6 py-3 min-w-[157px] lg:min-w-[]">ORDER DATE</th>
                </tr>
              </thead>
              <tbody>
                {orderRattingTableDetails.map((el, index) => {
                  return (
                    <tr key={index} className="paragraphSmallRegular even:bg-neutral-50 border-t border-neutral-300">
                      <td className="py-[25px] px-6 cursor-pointer" onClick={handleClickOrderId}>
                        {el.orderId}
                      </td>
                      <td className="py-[25px] px-6">{el.customerName}</td>
                      <td className="py-[25px] px-6">
                        <div className="flex flex-row items-center">
                          <OrderRatingIcon
                            fill={el.orderRating > 3 ? "#EBF6F5" : el.orderRating === 3 ? "#FFF5E3" : "#FEF2F2"}
                            stroke={el.orderRating > 3 ? "#3D8C82" : el.orderRating === 3 ? "#FFA704" : "#EF4444"}
                          />
                          <span className="ml-1">{el.orderRating.toFixed(1)}</span>
                        </div>
                      </td>
                      <td className="py-[15px] px-6">{el.orderFeedback}</td>
                      <td className="py-[25px] px-6">
                        <div className="flex flex-row items-center">
                          <OrderRatingIcon
                            fill={el.riderRating > 3 ? "#EBF6F5" : el.riderRating === 3 ? "#FFF5E3" : "#FEF2F2"}
                            stroke={el.riderRating > 3 ? "#3D8C82" : el.riderRating === 3 ? "#FFA704" : "#EF4444"}
                          />
                          <span className="ml-1">{el.riderRating.toFixed(1)}</span>
                        </div>
                      </td>
                      <td className="py-[15px] px-6">{el.riderFeedback}</td>
                      <td className="py-[25px] px-6">{el.orderDate}</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>

          <div className="hidden md:block mb-12 mt-4">
            {orderRattingTableDetails.map((el, index) => {
              return (
                <div key={index} className="mb-2">
                  <ListViewOrderRating
                    customeName={el.customerName}
                    riderRating={el.riderRating}
                    orderDate={el.orderDate}
                    orderFeedback={el.orderFeedback}
                    orderId={el.orderId}
                    orderRating={el.orderRating}
                    riderFeedback={el.riderFeedback}
                    handleClickOrderId={handleClickOrderId}
                  />
                </div>
              );
            })}
          </div>

          <div className="pt-4 md:hidden">
            <PaginationNumber />
          </div>
        </div>
      </div>
      <div className={`${!showCustomerReview && "hidden"}`}>
        <CustomerReviewDetails handleClickOrderId={handleClickOrderId} />
      </div>
    </>
  );
}
