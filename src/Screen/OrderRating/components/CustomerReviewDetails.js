import React, { useState } from "react";
import { ReactComponent as Close } from "../../../Assets/close.svg";
import { ReactComponent as SuccessTickIcon } from "../../../Assets/success-tick.svg";
import { ReactComponent as UpiIcon } from "../../../Assets/UPI.svg";
import { ReactComponent as RiderIcon } from "../../../Assets/riders.svg";
import { ReactComponent as OrderRatingIcon } from "../../../Assets//order-ratings.svg";
import { ReactComponent as LeftArrow } from "../../../Assets/chevron-down.svg";
import { LargeDestructiveButton, LargePrimaryButton } from "../../../Components/Buttons/Button";
import SelectOfferPage from "../../../Components/SelectOffer/SelectOfferPage";

export default function CustomerReviewDetails(props) {
  const [isShoRiderFeedBack, setIsShoRiderFeedBack] = useState(false);
  const [ShowHideText, setShowHideText] = useState("Show");
  const handleClickShowHide = () => {
    setIsShoRiderFeedBack(!isShoRiderFeedBack);
    setShowHideText(isShoRiderFeedBack ? "Show" : "Hide");
  };
  const [showOfferpage, setShowOfferPage] = useState(false);

  const handleClickSelectOffer = () => {
    setShowOfferPage(!showOfferpage);
  };

  return (
    <>
      <div className={`fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 flex justify-center items-center md:bg-white md:relative ${showOfferpage && "hidden md:flex"}`}>
        <div className="max-w-[644px] w-full rounded-xl md:rounded-none bg-shades-50 pl-8 pr-[14px] py-6 md:px-4 md:py-4 m-auto md:max-w-full max-h-[904px] h-full md:max-h-full overflow-auto [&::-webkit-scrollbar]:hidden">
          <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer" onClick={() => props.handleClickOrderId()}>
            <LeftArrow className="rotate-90" />
            <span className="ml-1">Back to order ratings</span>
          </div>

          <div className="flex flex-row justify-between items-center mb-2 pb-4 border-b mr-[18px] border-neutral-300 md:items-start">
            <div>
              <span className="paragraphLargeMedium">Customer review details</span>
              <div className="flex flex-row items-center">
                <span className="paragraphMediumItalic text-neutral-500">Order #SAWQ</span>
                <div className="w-1 h-1 rounded-full mx-3 bg-neutral-500 md:hidden" />
                <div className="flex flex-row items-center md:hidden">
                  <SuccessTickIcon />
                  <span className="paragraphSmallRegular ml-1.5">Delivered</span>
                </div>
              </div>
            </div>
            <div className="md:hidden cursor-pointer" onClick={() => props.handleClickOrderId()}>
              <Close />
            </div>
            <div className="flex-row items-center hidden md:flex">
              <SuccessTickIcon />
              <span className="paragraphSmallRegular ml-1.5">Delivered</span>
            </div>
          </div>

          <div className="overflow-auto scrollbarStyle md:overflow-hidden h-[706px] md:h-full pr-3">
            <div className="flex flex-row items-center justify-between pb-4 border-b border-neutral-300 mb-4 md:block">
              <div>
                <div className="mb-1.5 pt-1.5 flex flex-row items-center">
                  <span className="paragraphMediumMedium text-neutral-500">Order bill name:</span>
                  <div className="flex flex-row items-center">
                    <span className="paragraphMediumRegular ml-2">Riddhi Shah</span>
                    <span className="text-primary-500 leading-3 text-[10px] border font-medium px-2 py-1 border-primary-500 rounded bg-primary-50 h-5 ml-2">New</span>
                  </div>
                </div>
                <div className="mb-1.5 pt-1.5">
                  <span className="paragraphMediumMedium text-neutral-500">Order date:</span>
                  <span className="paragraphMediumRegular ml-2">13 Nov 2022</span>
                </div>
                <div className="mb-1.5 pt-1.5 flex flex-row items-center">
                  <span className="paragraphMediumMedium text-neutral-500">Payment method:</span>
                  <span className="paragraphMediumRegular ml-2 items-center flex flex-row">
                    <UpiIcon className="mr-1" />
                    UPI
                  </span>
                </div>
                <div className="mb-1.5 pt-1.5">
                  <span className="paragraphMediumMedium text-neutral-500">Ordered via:</span>
                  <span className="paragraphMediumRegular ml-2">iOS device</span>
                </div>
                <div className="pt-1.5">
                  <span className="paragraphMediumMedium text-neutral-500">Delivery area:</span>
                  <span className="paragraphMediumRegular ml-2">Race course</span>
                </div>
              </div>

              <div>
                <div className="mb-1.5 pt-1.5 flex flex-row items-center">
                  <span className="paragraphMediumMedium text-neutral-500">Order type:</span>
                  <span className="paragraphMediumRegular ml-2 items-center flex flex-row">
                    <RiderIcon className="mr-1" />
                    Delivery
                  </span>
                </div>
                <div className="mb-1.5 pt-1.5">
                  <span className="paragraphMediumMedium text-neutral-500">Order time:</span>
                  <span className="paragraphMediumRegular ml-2">17:59 PM</span>
                </div>
                <div className="mb-1.5 pt-1.5 flex flex-row items-center">
                  <span className="paragraphMediumMedium text-neutral-500">Dishes ordered:</span>
                  <span className="paragraphMediumRegular ml-2">06</span>
                </div>
                <div className="mb-1.5 pt-1.5">
                  <span className="paragraphMediumMedium text-neutral-500">Platform:</span>
                  <span className="paragraphMediumRegular ml-2">swiggy</span>
                </div>
                <div className="pt-1.5">
                  <span className="paragraphMediumMedium text-neutral-500">Delivery distance:</span>
                  <span className="paragraphMediumRegular ml-2">07 km</span>
                </div>
              </div>
            </div>

            <div className="flex flex-row justify-between items-center mb-4 pb-4 border-b">
              <h3 className="paragraphMediumMedium">Order details</h3>
              <span className="paragraphMediumRegular text-primary-500">Show</span>
            </div>

            <div className="border-b border-neutral-300 pb-4 mb-[18px] md:mb-10">
              <div className="flex flex-row justify-between items-center">
                <h3 className="paragraphMediumMedium">Order rating and review</h3>
                <span className="paragraphMediumRegular text-primary-500 cursor-pointer" onClick={handleClickShowHide}>
                  {ShowHideText}
                </span>
              </div>
              <div className={`mt-4 ${!isShoRiderFeedBack && "hidden"}`}>
                <div>
                  <h3 className="paragraphMediumItalic mb-2 text-neutral-500">Rating given on</h3>
                  <span className="font-normal text-base leading-6">13 Nov 2022</span>
                </div>

                <div className="mt-3">
                  <h3 className="paragraphMediumItalic mb-2 text-neutral-500">Order rating</h3>
                  <div className="flex flex-row items-center">
                    <div className="flex flex-row">
                      <OrderRatingIcon fill="#EBF6F5" stroke="#3D8C82" />
                      <OrderRatingIcon fill="#EBF6F5" stroke="#3D8C82" className="mx-1" />
                      <OrderRatingIcon fill="#EBF6F5" stroke="#3D8C82" />
                      <OrderRatingIcon fill="#EBF6F5" stroke="#3D8C82" className="mx-1" />
                      <OrderRatingIcon fill="#EBF6F5" stroke="#3D8C82" />
                    </div>
                    <div className="w-1 h-1 mx-3 bg-neutral-500" />
                    <span className="font-normal text-base leading-6">5.0 star</span>
                  </div>
                </div>

                <div className="mt-3">
                  <h3 className="paragraphMediumItalic mb-2 text-neutral-500">Order review</h3>
                  <div className="font-normal text-base leading-6">
                    <p>
                      I had heard a lot of Reviews about this Place, especially the Fusion Pizzas & The Melting Cheese Burger,So Definitely had to try the 2 Pizzas as exciting and
                      delicious as they sound:
                    </p>
                    <p className="mt-5">1.The Pastawala Pizza</p>
                    <p>
                      2. The French Fries PizzaI had ordered from swiggy & first of all have to say the Packaging was completely upto the mark.The Taste was Really Unique & Delicious, I
                      would definitely recommend to try this Pizzas for once, you won't regret it!
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="flex flex-row mt-4 mr-[18px] md:hidden">
            <div className="mr-2 w-1/2">
              <LargeDestructiveButton name="Delete review" />
            </div>
            <div className="ml-2 w-1/2 cursor-pointer" onClick={handleClickSelectOffer}>
              <LargePrimaryButton name="Select offer" isDefault={false} />
            </div>
          </div>
        </div>
        <div className="fixed bottom-0 flex-row hidden md:flex w-full pt-2 shadow-dropShadow bg-white pb-1 px-4">
          <div className="mr-[7.5px] w-1/2">
            <LargeDestructiveButton name="Delete review" />
          </div>
          <div className="ml-[7.5px] w-1/2 cursor-pointer" onClick={handleClickSelectOffer}>
            <LargePrimaryButton name="Select offer" isDefault={false} />
          </div>
        </div>
      </div>

      <div className={`${!showOfferpage && "hidden"}`}>
        <SelectOfferPage handleClickClose={handleClickSelectOffer} />
      </div>
    </>
  );
}
