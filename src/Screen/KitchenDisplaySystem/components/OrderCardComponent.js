import React, { useState } from "react";
import { ReactComponent as VegIcon } from "../../../Assets/vegetable-icon.svg";
import { ReactComponent as NonVegIcon } from "../../../Assets/non-veg.svg";
import { ReactComponent as DropDownIcon } from "../../../Assets/chevron-down.svg";

export const DishItems = (props) => {
    const [itemPrepared, setItemPrepared] = useState(false);

    const handleClickItem = () => {
        if (isGreen) {
            setIsGreen(false);
            props.handleUpdateOrder(true);
            return;
        }
        setItemPrepared(!itemPrepared);
    };

    const [showWithItems, setShowWithItems] = useState(false);

    const handleClickWithDropDown = () => {
        setShowWithItems(!showWithItems);
    };

    const [showAddonItems, setShowAddonIcon] = useState(false);

    const handleClickAddonDropDown = () => {
        setShowAddonIcon(!showAddonItems);
    };

    const [isGreen, setIsGreen] = useState(props.el.updatedQuantity && true);

    return (
        <>
            <div className="flex flex-row justify-between items-start cursor-pointer" onClick={handleClickItem}>
                <div className="flex flex-row items-start max-w-[199px]">
                    <div className={`${itemPrepared ? "paragraphMediumStrikethrough" : "paragraphMediumSemiBold"} ${isGreen && "text-tertiary-800"} `}>{props.el.displayName}</div>

                    <span className="mt-1 ml-2">{props.el.foodType === "veg" ? <VegIcon height={16} width={16} /> : <NonVegIcon height={16} width={16} />}</span>
                </div>

                <div className="flex flex-row items-baseline">
                    {props.el.updatedQuantity && <div className={`paragraphXSmallStrikethrough mr-1 ${isGreen ? "text-tertiary-800" : "hidden"}`}>{("0" + props.el.quantity).slice(-2)}</div>}

                    <div className={`${itemPrepared ? "paragraphMediumStrikethrough" : "paragraphMediumSemiBold"} ${isGreen && "text-tertiary-800"}`}>
                        {("0" + (props.el.updatedQuantity ?? props.el.quantity)).slice(-2)}
                    </div>
                </div>
            </div>

            <div className="pl-[21px] ml-2 border-l border-neutral-300 mt-1">
                {props.el.customization && (
                    <div className="">
                        <div className="flex flex-row items-center">
                            <div className={`${itemPrepared ? "paragraphSmallStrikethrough mr-1" : "paragraphSmallMedium"} titleSap relative`}>With</div>
                            <div className={`${!itemPrepared && "hidden"} cursor-pointer`} onClick={handleClickWithDropDown}>
                                <DropDownIcon height={20} className={`${showWithItems && "rotate-180"}`} />
                            </div>
                        </div>

                        <div className={`flex flex-col paragraphSmallItalic text-neutral-500 mb-1 ${!showWithItems && itemPrepared && "hidden"}`}>
                            {props.el.customization.map((customizationEl, index) => (
                                <div className="mt-1" key={index}>
                                    {customizationEl.displayName}
                                </div>
                            ))}
                        </div>
                    </div>
                )}

                {props.el.addons && (
                    <div className="w-full max-w-[199px]">
                        <div className="flex flex-row items-center">
                            <div className={`${itemPrepared ? "paragraphSmallStrikethrough" : "paragraphSmallMedium"} titleSap relative`}>Note</div>
                            <div className={`${!itemPrepared && "hidden"} cursor-pointer`} onClick={handleClickAddonDropDown}>
                                <DropDownIcon height={20} className={`${showAddonItems && "rotate-180"}`} />
                            </div>
                        </div>
                        <p className={`mt-1 flex flex-col paragraphSmallItalic text-neutral-500 ${!showAddonItems && itemPrepared && "hidden"}`}>{props.el.addons}</p>
                    </div>
                )}
            </div>
        </>
    );
};
