import React, { useState } from "react";
import { ReactComponent as TimerIcon } from "../../../Assets/timer.svg";
import { ReactComponent as DineInIcons } from "../../../Assets/dine-in.svg";
import { DishItems } from "./OrderCardComponent";

export default function OrderCard(props) {
    const totalQuantity = props.items.reduce((acc, current) => acc + Number(current.quantity), 0);

    const [isUpdated, setIsUpdated] = useState(!props.isUpdated);

    const handleUpdateOrder = (value) => {
        setIsUpdated(value);
    };

    return (
        <>
            <div className="w-full border border-neutral-300 rounded-xl p-4">
                <div className="flex flex-row justify-between items-center mb-4 pb-4 border-b border-neutral-300">
                    <div className="flex flex-row">
                        {props.orderingMode === "Dine-In order" ? (
                            <>
                                <div className="p-2 bg-primary-500 rounded-lg">
                                    <DineInIcons height={32} width={32} stroke="#FAFAFA" />
                                </div>
                            </>
                        ) : (
                            props.orderIcon
                        )}
                        <div className="">
                            <div className="ml-2.5 paragraphMediumSemiBold">{props.orderingMode}</div>
                            <div className="ml-2 paragraphMediumItalic text-neutral-500">Order {props.orderLabel}</div>
                        </div>
                    </div>
                    <div className="paragraphMediumSemiBold justify-end flex flex-col text-right">
                        <div className="">KOT</div>
                        <div className="">{props.KOT < 10 ? "#0" + props.KOT : "#" + props.KOT}</div>
                    </div>
                </div>

                <div className={`mb-4 pb-4 border-b border-neutral-300 ${isUpdated && "hidden"} text-center`}>
                    <span className="paragraphOverlineLarge text-tertiary-800">ORDERED DISHES UPDATED!</span>
                </div>

                <div className="flex flex-row justify-between mb-4">
                    <div className="text-base leading-4 border-b border-neutral-900">Ordered dishes</div>
                    <div className="text-base leading-4 border-b border-neutral-900">Quantity</div>
                </div>

                <div className="border-b border-neutral-300 mb-4">
                    {props.items.map((el, index) => (
                        <div className="mb-4" key={index}>
                            <DishItems el={el} handleUpdateOrder={handleUpdateOrder} />
                        </div>
                    ))}
                </div>

                <div className="flex flex-row justify-between paragraphMediumSemiBold mb-4 pb-4 border-b border-neutral-300">
                    <span>Total dishes quantity</span>
                    <span>{totalQuantity}</span>
                </div>

                <div className="flex flex-row justify-between paragraphMediumSemiBold border-neutral-300 text-tertiary-800">
                    <span>Time elapsed</span>
                    <div className="flex flex-row items-center">
                        <TimerIcon height={24} width={24} stroke="#3D8C82" />
                        <span className="ml-2">
                            {props.timeElapsed[0].mins} mins {props.timeElapsed[1].seconds} secs
                        </span>
                    </div>
                </div>
            </div>
        </>
    );
}
