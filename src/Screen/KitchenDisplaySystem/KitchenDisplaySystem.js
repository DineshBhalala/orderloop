import React, { useState } from "react";
import { DefaultBreadcrumbs } from "../../Components/Breadcrumbs/Breadcrumbs";
import { ReactComponent as KdsIcon } from "../../Assets/kitchen-display-system.svg";
import { ReactComponent as FilterIcon } from "../../Assets/filter.svg";
import { ReactComponent as OrderIcon } from "../../Assets/order.svg";
import { ReactComponent as SwiggyIcon } from "../../Assets/swiggy.svg";
import { ReactComponent as ZomatoIcon } from "../../Assets/zomato.svg";
import { ReactComponent as Vegicon } from "../../Assets/vegetable-icon.svg";
import { ReactComponent as NonVegIcon } from "../../Assets/non-veg.svg";
import { ReactComponent as MaximizeIcon } from "../../Assets/maximize.svg";
import { ReactComponent as LeftArrowIcon } from "../../Assets/chevron-down.svg";
import { Tab } from "../../Components/Tabs/Tabs";
import { LargePrimaryButton } from "../../Components/Buttons/Button";
import OrderCard from "./Components/OrderCard";
import FilterDropDown from "./Components/FilterDropDown";
import { DropDownTabs } from "../../Components/DropDown/DropDownTabs";

export default function KitchenDisplaySystem(props) {
    const orders = [
        {
            icon: <SwiggyIcon height={48} width={48} />,
            orderingMode: "Delivery order",
            orderLabel: "#BBQA",
            items: [
                {
                    displayName: "Double Cheese Margherita Pizza",
                    note: "Keep the pizza loaded with ample amount of cheese",
                    foodType: "veg",
                    quantity: 2,
                    addons: "Keep the pizza loaded with ample amount of cheese",
                    customization: [
                        {
                            displayName: "Extra cheese",
                        },
                        {
                            displayName: "Extra veggies",
                        },
                    ],
                },
                {
                    displayName: "Indi Tandoori Paneer",
                    foodType: "veg",
                    quantity: 1,
                    isPrepared: true,
                },
                {
                    displayName: "Farmhouse Extraveganza Veggie",
                    foodType: "veg",
                    quantity: 3,
                    customization: [
                        {
                            displayName: "Extra cheese",
                        },
                        {
                            displayName: "Extra veggies",
                        },
                    ],
                },
                {
                    displayName: "Mexican Green Wave",
                    foodType: "veg",
                    quantity: 2,
                },
                {
                    displayName: "Italian Pesto Pasta",
                    foodType: "veg",
                    quantity: 2,
                },
            ],
            timeElapsed: [
                {
                    mins: 15,
                },
                {
                    seconds: 16,
                },
            ],
        },
        {
            orderingMode: "Dine-In order",
            orderLabel: "#BBQA",
            items: [
                {
                    displayName: "Double Cheese Margherita Pizza",
                    foodType: "veg",
                    quantity: 2,
                    customization: [
                        {
                            displayName: "Extra cheese",
                        },
                        {
                            displayName: "Extra veggies",
                        },
                    ],
                },
                {
                    displayName: "Farmhouse",
                    foodType: "veg",
                    quantity: 1,
                    isPrepared: true,
                    customization: [
                        {
                            displayName: "Extra cheese",
                        },
                        {
                            displayName: "Extra veggies",
                        },
                    ],
                },
                {
                    displayName: "Indi Tandoori Paneer",
                    foodType: "veg",
                    quantity: 2,
                    customization: [
                        {
                            displayName: "Extra cheese",
                        },
                        {
                            displayName: "Extra veggies",
                        },
                    ],
                },
            ],
            timeElapsed: [
                {
                    mins: 10,
                },
                {
                    seconds: 10,
                },
            ],
        },
        {
            orderingMode: "Dine-In order",
            orderLabel: "#BBQA",
            items: [
                {
                    displayName: "Double Cheese Margherita Pizza",
                    foodType: "veg",
                    quantity: 2,
                    addons: "Keep the pizza loaded with ample amount of cheese",
                    customization: [
                        {
                            displayName: "Extra cheese",
                        },
                        {
                            displayName: "Extra veggies",
                        },
                    ],
                },
            ],
            timeElapsed: [
                {
                    mins: 5,
                },
                {
                    seconds: 10,
                },
            ],
        },
        {
            orderingMode: "Dine-In order",
            orderLabel: "#BBQA",
            isUpdated: true,
            items: [
                {
                    displayName: "Double Cheese Margherita Pizza",
                    foodType: "veg",
                    quantity: 2,
                },
                {
                    displayName: "Farmhouse",
                    foodType: "veg",
                    quantity: 1,
                    updatedQuantity: 3,
                },
                {
                    displayName: "Indi Tandoori Paneer",
                    foodType: "veg",
                    quantity: 1,
                },
            ],
            timeElapsed: [
                {
                    mins: 3,
                },
                {
                    seconds: 10,
                },
            ],
        },
        {
            icon: <ZomatoIcon />,
            orderingMode: "Delivery order",
            orderLabel: "#BBQA",

            isUpdated: true,
            items: [
                {
                    displayName: "Double Cheese Margherita Pizza",
                    foodType: "veg",
                    quantity: 2,
                },
                {
                    displayName: "Farmhouse",
                    foodType: "veg",
                    quantity: 1,
                    updatedQuantity: 3,
                },
                {
                    displayName: "Indi Tandoori Paneer",
                    foodType: "veg",
                    quantity: 1,
                    updatedQuantity: 3,
                },
            ],
            timeElapsed: [
                {
                    mins: 3,
                },
                {
                    seconds: 10,
                },
            ],
        },
        {
            orderingMode: "Dine-In order",
            orderLabel: "#BBQA",
            isUpdated: true,
            items: [
                {
                    displayName: "Double Cheese Margherita Pizza",
                    foodType: "veg",
                    quantity: 2,
                },
                {
                    displayName: "Farmhouse",
                    foodType: "veg",
                    quantity: 1,
                    updatedQuantity: 3,
                },
                {
                    displayName: "Indi Tandoori Paneer",
                    foodType: "veg",
                    quantity: 1,
                    updatedQuantity: 3,
                },
            ],
            timeElapsed: [
                {
                    mins: 3,
                },
                {
                    seconds: 10,
                },
            ],
        },
        {
            icon: <SwiggyIcon height={48} width={48} />,
            orderingMode: "Delivery order",
            orderLabel: "#BBQA",

            isUpdated: true,
            items: [
                {
                    displayName: "Double Cheese Margherita Pizza",
                    foodType: "veg",
                    quantity: 2,
                },
                {
                    displayName: "Farmhouse",
                    foodType: "veg",
                    quantity: 1,
                    updatedQuantity: 3,
                },
                {
                    displayName: "Indi Tandoori Paneer",
                    foodType: "veg",
                    quantity: 1,
                    updatedQuantity: 3,
                },
            ],
            timeElapsed: [
                {
                    mins: 3,
                },
                {
                    seconds: 10,
                },
            ],
        },
        {
            orderingMode: "Dine-In order",
            orderLabel: "#BBQA",
            isUpdated: true,
            items: [
                {
                    displayName: "Double Cheese Margherita Pizza",
                    foodType: "veg",
                    quantity: 2,
                },
                {
                    displayName: "Farmhouse",
                    foodType: "veg",
                    quantity: 1,
                    updatedQuantity: 3,
                },
                {
                    displayName: "Indi Tandoori Paneer",
                    foodType: "veg",
                    quantity: 1,
                },
            ],
            timeElapsed: [
                {
                    mins: 3,
                },
                {
                    seconds: 10,
                },
            ],
        },
    ];

    const [showFilters, setShowFilters] = useState(false);

    const handleClickFilter = () => {
        setShowFilters(!showFilters);
    };

    const dishesSummery = [
        { orderedDishes: "Double Cheese Margherita Pizza", quantity: 6, type: "veg" },
        { orderedDishes: "Farmhouse Extraveganza Veggie", quantity: 6, type: "veg" },
        { orderedDishes: "Indi Tandoori Paneer", quantity: 6, type: "veg" },
        { orderedDishes: "Italian Pesto Pasta", quantity: 6, type: "veg" },
        { orderedDishes: "Farmhouse", quantity: 6, type: "veg" },
        { orderedDishes: "Mexican Green Wave", quantity: 6, type: "veg" },
        { orderedDishes: "Peppy Paneer", quantity: 6, type: "veg" },
        { orderedDishes: "Cheese n Corn", quantity: 23, type: "veg" },
        { orderedDishes: "The Unthinkable Pizza", quantity: 6, type: "veg" },
        { orderedDishes: "Farmhouse", quantity: 6, type: "veg" },
        { orderedDishes: "Mexican Green Wave", quantity: 6, type: "veg" },
        { orderedDishes: "Peppy Paneer", quantity: 6, type: "veg" },
        { orderedDishes: "Cheese n Corn", quantity: 6, type: "veg" },
        { orderedDishes: "The Unthinkable Pizza", quantity: 6, type: "veg" },
        { orderedDishes: "Farmhouse", quantity: 6, type: "veg" },
        { orderedDishes: "Mexican Green Wave", quantity: 6, type: "veg" },
        { orderedDishes: "Peppy Paneer", quantity: 6, type: "veg" },
        { orderedDishes: "Cheese n Corn", quantity: 6, type: "veg" },
        { orderedDishes: "The Unthinkable Pizza", quantity: 6, type: "veg" },
        { orderedDishes: "Farmhouse", quantity: 6, type: "veg" },
        { orderedDishes: "Mexican Green Wave", quantity: 6, type: "veg" },
        { orderedDishes: "Peppy Paneer", quantity: 6, type: "veg" },
        { orderedDishes: "Cheese n Corn", quantity: 6, type: "veg" },
        { orderedDishes: "The Unthinkable Pizza", quantity: 6, type: "veg" },
        { orderedDishes: "Farmhouse", quantity: 6, type: "veg" },
        { orderedDishes: "Mexican Green Wave", quantity: 6, type: "veg" },
        { orderedDishes: "Peppy Paneer", quantity: 6, type: "veg" },
        { orderedDishes: "Cheese n Corn", quantity: 6, type: "veg" },
        { orderedDishes: "The Unthinkable Pizza", quantity: 6, type: "veg" },
        { orderedDishes: "The 4 Cheese Pizza", quantity: 6, type: "veg" },
        { orderedDishes: "Corn n Cheese Parantha Pizza", quantity: 6, type: "veg" },
        { orderedDishes: "Paneer Parantha Pizza", quantity: 6, type: "veg" },
        { orderedDishes: "Moroccan Spicy Pizza", quantity: 6, type: "veg" },
    ];

    const [showDishesSummery, setShowDishesSummery] = useState(false);

    const handleClickDishesSummery = () => {
        setShowDishesSummery(!showDishesSummery);
    };

    //  don'r remove this comment this is for dishes summery whether it is category wise or not
    // const dishesSummery = [
    //     {
    //         category: "Pizza",
    //         orderedDishes: [
    //             { name: "Double Cheese Margherita Pizza", quantity: 10, type: "veg" },
    //             { name: "Farmhouse Extraveganza Veggie", quantity: 2, type: "veg" },
    //             { name: "Indi Tandoori Paneer", quantity: 1, type: "veg" },
    //         ],
    //     },
    //     {
    //         category: "Pizza",
    //         orderedDishes: [
    //             { name: "Double Cheese Margherita Pizza", quantity: 10, type: "veg" },
    //             { name: "Farmhouse Extraveganza Veggie", quantity: 2, type: "veg" },
    //             { name: "Indi Tandoori Paneer", quantity: 1, type: "veg" },
    //         ],
    //     },
    //     {
    //         category: "Pizza",
    //         orderedDishes: [
    //             { name: "Double Cheese Margherita Pizza", quantity: 10, type: "veg" },
    //             { name: "Farmhouse Extraveganza Veggie", quantity: 2, type: "veg" },
    //             { name: "Indi Tandoori Paneer", quantity: 1, type: "veg" },
    //         ],
    //     },
    //     {
    //         category: "Pasta",
    //         orderedDishes: [
    //             { name: "Italian Pesto Pasta", quantity: 10, type: "veg" },
    //             { name: "Cheesy Jalapeno Pasta Veg", quantity: 2, type: "veg" },
    //             { name: "Tikka Masala Pasta Veg", quantity: 14, type: "veg" },
    //         ],
    //     },
    //     {
    //         category: "Beverages",
    //         orderedDishes: [
    //             { name: "Pepsi (500ml)", quantity: 6, type: "veg" },
    //             { name: "7Up (500ml)", quantity: 2, type: "veg" },
    //             { name: "Mirinda (500ml)", quantity: 1, type: "veg" },
    //             { name: "Pepsi Black Can", quantity: 1, type: "veg" },
    //             { name: "Slice (350ml)", quantity: 1, type: "veg" },
    //             { name: "B Natural Pink Guavas from Dakshin India (300 ml)", quantity: 1, type: "veg" },
    //         ],
    //     },
    // ];

    const [showFullScreen, setShowFullScreen] = useState(false);

    const handleClickMaximize = () => {
        props.handleFullScreenMode(!showFullScreen);
        setShowFullScreen(!showFullScreen);
    };

    return (
        <>
            <div className="bg-[#fafafa]">
                <div className={`px-8 pt-4 w-full ${!showFullScreen && "max-w-[1440px]"} mx-auto bg-white lg:px-4 ${showDishesSummery && "md:hidden"}`}>
                    <div className="mb-4 md:hidden flex justify-between items-center">
                        <DefaultBreadcrumbs mainTab="KDS" icon={<KdsIcon height={20} width={20} />} />
                        <span className="cursor-pointer -mb-1 mt-1" onClick={handleClickMaximize}>
                            <MaximizeIcon />
                        </span>
                    </div>

                    <div className="pb-6 border-b border-neutral-300 lg:pb-4">
                        <div className="flex flex-row justify-between items-start lg:block">
                            <div className="flex flex-row items-center md:hidden">
                                <div className="mr-4 xl:mr-2">
                                    <Tab label="Active" badgeText="6" isActive={true} />
                                </div>
                                <div className="mr-4 xl:mr-2">
                                    <Tab label="Scheduled" badgeText="2" />
                                </div>
                                <div className="mr-4 xl:mr-2">
                                    <Tab label="Completed" badgeText="10" />
                                </div>
                                <div className="mr-4 xl:mr-2">
                                    <Tab label="Rejected" badgeText="5" />
                                </div>
                            </div>
                            <div className="hidden md:block w-full">
                                <DropDownTabs
                                    shadow="shadow-shadowXsmall"
                                    menuItems={[{ item: "Active" }, { item: "Scheduled" }, { item: "Completed" }, { item: "Rejected" }]}
                                    tabStyle="paragraphSmallRegular"
                                    itemStyle="paragraphSmallRegular"
                                    boxStyle="border-neutral-300"
                                    dropDownIconFill="#131126"
                                    textColor="text-neutral-900"
                                />
                            </div>
                            <div className="flex items-center flex-row-reverse lg:block">
                                <div className="flex flex-row lg:mt-4">
                                    <div className="lg:w-1/2 cursor-pointer" onClick={handleClickFilter}>
                                        <LargePrimaryButton
                                            name="Filters"
                                            leftIconDefault={<FilterIcon fill="#FFFFFF" />}
                                            leftIconClick={<FilterIcon fill="#C4BEED" />}
                                            hideName="xl:hidden lg:block mobile:hidden"
                                        />
                                    </div>
                                    <div className="ml-4 xl:ml-2 lg:w-1/2 cursor-pointer" onClick={handleClickDishesSummery}>
                                        <LargePrimaryButton
                                            name="Dishes summary"
                                            leftIconDefault={<OrderIcon stroke="#FFFFFF" />}
                                            leftIconClick={<OrderIcon stroke="#C4BEED" />}
                                            hideName="xl:hidden lg:block mobile:hidden"
                                        />
                                    </div>
                                </div>
                                <div className={`min-w-[213px] pageContentSmall:min-w-[140px] w-full mr-4 xl:mr-2 relative lg:mr-0 lg:mt-4 ${!showFilters && "hidden"}`}>
                                    <FilterDropDown />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div
                    className={`flex flex-row w-full ${!showFullScreen && "max-w-[1440px]"} mx-auto bg-white pl-[22px] lg:pl-3 md:pl-4 md:pr-4 pb-6 lg:pb-4 ${
                        !showDishesSummery && "pr-[22px] lg:pr-3"
                    } md:block`}
                >
                    <div className={`${showDishesSummery && "md:hidden"} md:contents`}>
                        {orders.map((el, index) => (
                            <div
                                className={`w-[329px] mx-2.5 inline-block align-top pageContent:mx-0 pageContent:w-1/2 pageContent:min-w-0 pageContent:px-2.5 lg:px-1 pt-6 lg:pt-4 lg:min-w-[364px] md:max-w-full md:min-w-0 md:w-full md:px-0 ${
                                    showDishesSummery && "lg:w-full"
                                }`}
                                key={index}
                            >
                                <OrderCard
                                    KOT={index + 1}
                                    orderIcon={el.icon}
                                    isUpdated={el.isUpdated}
                                    orderLabel={el.orderLabel}
                                    orderingMode={el.orderingMode}
                                    timeElapsed={el.timeElapsed}
                                    items={el.items}
                                />
                            </div>
                        ))}
                    </div>

                    <div className={`border-l border-neutral-300 min-w-[362px] ml-[9px] pr-[18px] ${!showDishesSummery && "hidden"} md:border-0 md:ml-0 md:pr-0 md:min-w-0`}>
                        <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 pt-4 cursor-pointer" onClick={handleClickDishesSummery}>
                            <LeftArrowIcon className="rotate-90" />
                            <span className="ml-1">Back to KDS</span>
                        </div>

                        <div className="pr-[15px] lg:pr-0">
                            <div className="pb-4 border-b border-neutral-300 my-4 pl-4 lg:pl-7 md:pl-0">
                                <span className="paragraphLargeSemiBold mr-2">Dishes summary</span>
                                <span className="paragraphMediumItalic text-neutral-500">(128 dish preparing)</span>
                            </div>
                        </div>

                        <div className="flex flex-row justify-between mb-4 max-w-[297px] mx-0 px-0 pr-0 w-full ml-4 lg:ml-7 md:ml-0 md:max-w-full">
                            <span className="text-base leading-4 border-b border-neutral-900">Ordered dishes</span>
                            <span className="text-base leading-4 border-b border-neutral-900">Quantity</span>
                        </div>

                        {/* don'r remove this comment his is for dishes summery whether it is category wise or not */}
                        {/* <div className="scrollbarStyle h-screen overflow-auto pl-4 lg:pl-7 md:pl-0 md:overflow-hidden md:h-full">
                            <div className="mr-6 h-full md:mr-0">
                                {dishesSummery.map((el, index) => {
                                    return (
                                        <div className="mb-4 pb-4 border-b border-neutral-300 min-w-[297px] mr-6" key={index}>
                                            <div className="flex flex-row justify-between max-w-[297px] w-full md:max-w-full">
                                                <span className="text-primary-500 paragraphMediumSemiBold">{el.category}</span>
                                                <span className="text-primary-500 paragraphMediumSemiBold">{el.orderedDishes.reduce((acc, dish) => acc + dish.quantity, 0)}</span>
                                            </div>
                                            <div>
                                                {el.orderedDishes.map((orderDishesEl, orderDishesIndex) => {
                                                    return (
                                                        <div className="flex flex-row justify-between max-w-[297px] w-full mt-4 md:max-w-full" key={orderDishesIndex}>
                                                            <div className="flex flex-row">
                                                                <span className="paragraphMediumSemiBold mr-2">{orderDishesEl.name}</span>
                                                                <span className="pt-[5px]">{orderDishesEl.type === "veg" ? <Vegicon /> : <NonVegIcon />}</span>
                                                            </div>
                                                            <span className="paragraphMediumSemiBold min-w-[70px] text-right">{orderDishesEl.quantity}</span>
                                                        </div>
                                                    );
                                                })}
                                            </div>
                                        </div>
                                    );
                                })}
                            </div>
                        </div> */}

                        <div className="scrollbarStyle h-screen overflow-auto pl-4 lg:pl-7">
                            <div className="mr-6 lg:mr-[33px]">
                                {dishesSummery.map((el, index) => {
                                    return (
                                        <div className="w-full flex flex-row justify-between items-center mb-4 pb-4 border-b border-neutral-300 min-w-[297px]" key={index}>
                                            <div className="flex flex-row items-center">
                                                <span className="paragraphMediumSemiBold mr-2">{el.orderedDishes}</span>
                                                <span className="pt-[5px]">{el.type === "veg" ? <Vegicon /> : <NonVegIcon />}</span>{" "}
                                            </div>

                                            <span className="paragraphMediumSemiBold min-w-[70px] text-right">{el.quantity}</span>
                                        </div>
                                    );
                                })}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
