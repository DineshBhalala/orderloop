import React, { useState } from "react";
import { ReactComponent as Close } from "../../Assets/close.svg";
import { DefaultInputField, InputArea } from "../../Components/InputField/InputField";
import { LargePrimaryButton, LargeTertiaryButton, LargeDestructiveButton } from "../../Components/Buttons/Button";
import { ReactComponent as LeftArrow } from "../../Assets/chevron-down.svg";
import { DropDownNotification } from "../../Components/DropDown/DashBoardDropDown";
import { ReactComponent as TimerIcon } from "../../Assets/timer.svg";
import { ReactComponent as Calender } from "../../Assets/calendar.svg";
import { ReactComponent as DownArrow } from "../../Assets/chevron-down.svg";
import DropDown from "../../Components/DropDown/DropDown";
import Tag from "../../Components/Tag/Tag";
import UploadImage from "../../Components/UploadImage/UploadImage";
import pizza1 from "../../Assets/mediaLibrary/pizza1.png";

const NotificationDetailsPopup = (props) => {
  const [selectLangs, setSelectLangs] = useState(["English", "ગુજરાતી"]);

  const removeTag = (index) => {
    setSelectLangs((prevTags) => prevTags.filter((_, i) => i !== index));
  };

  const [showSelectContent, setShowSelectContent] = useState(false);

  const selectType = <DefaultInputField label="Select type" placeholder="Banners" placeholderTextColor="neutral-900" />;

  const handleClickSelectImageContent = () => {
    setShowSelectContent(!showSelectContent);
  };
  const [showHideUploadPopup, setShowHideUploadPopup] = useState(false);

  const uploadImageShowHideController = () => {
    setShowHideUploadPopup(!showHideUploadPopup);
  };

  return (
    <>
      <div className={`${showHideUploadPopup && "md:hidden"}`}>
        <div className="fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 flex overflow-auto px-[33px] lg:px-[17px] md:bg-white md:relative md:px-0 md:pb-24">
          <div className="rounded-xl bg-shades-50 px-8 py-6 m-auto md:px-0 md:py-0 lg:w-full">
            <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer" onClick={() => props.handleNotificationDetailPopup()}>
              <LeftArrow className="rotate-90" />
              <span className="pl-1">Back to Notification details</span>
            </div>

            <div className="flex flex-row justify-between items-center border-neutral-300 mb-6 md:hidden">
              <div>
                <span className="paragraphLargeMedium">Notification details</span>
                <div className="flex flex-row items-center">
                  <span className="paragraphMediumItalic text-neutral-500">Edit/Delete/Resend notification</span>
                </div>
              </div>
              <div onClick={props.handleNotificationDetailPopup} className="md:hidden cursor-pointer">
                <Close />
              </div>
            </div>

            <div className="mb-4 relative w-1/2 md:w-full pr-6 md:pr-0">
              <DropDown label="Select language" dropdownLabel="Select desired languages" buttonTextColor="neutral-300" />
              <div className="flex flex-row">
                {selectLangs.map((el, index) => (
                  <div className="mr-2" key={index}>
                    <Tag tag={el} onClose={() => removeTag(index)} />
                  </div>
                ))}
              </div>
            </div>
            <div className="flex flex-row justify-between md:block">
              <div className="w-[411px] lg:w-[360px] md:w-full">
                <div className="mb-4">
                  <div className="flex justify-between items-center paragraphSmallMedium mb-1">
                    <span>Notification title</span>
                    <div className="text-primary-500 flex justify-between items-center cursor-pointer">
                      <span className="mr-[3px]">Add meta tag</span>
                      <DownArrow fill="#6C5DD3" width={16} height={16} />
                    </div>
                  </div>
                  <DefaultInputField placeholder="Enter notification title" />
                </div>
                <div className="mb-2.5">
                  <div className="flex justify-between items-center paragraphSmallMedium mb-1">
                    <span>Description</span>
                    <div className="text-primary-500 flex justify-between items-center cursor-pointer">
                      <span className="mr-[3px]">Add meta tag</span>
                      <DownArrow fill="#6C5DD3" width={16} height={16} />
                    </div>
                  </div>
                  <InputArea boxHeight="h-[88px]" boxPaddingB="pb-4" resizeNone="resize-none" placeholder="Enter notification description" />
                </div>
                <div className="mb-4 relative">
                  <DropDown label="Display screen" dropdownLabel="Select display screen" buttonTextColor="neutral-300" />
                </div>

                <div className="mb-4">
                  <span className="paragraphSmallMedium mb-1 block">Schedule notification</span>
                  <div className="flex flex-row">
                    <div className="w-1/2 mr-[4.5px]">
                      <DropDownNotification menuIcon={<Calender width={20} height={20} />} truncateText="w-[98px] md:w-[85px] truncate block text-left" />
                    </div>
                    <div className="w-1/2 ml-[4.5px]">
                      <DropDownNotification menuIcon={<TimerIcon />} menuItems={["12:45 PM"]} disableMenuIcon={<TimerIcon stroke="#D3D2D8" />} disable={true} />
                    </div>
                  </div>
                </div>
                <div>
                  <div className="mb-2">
                    <span className="paragraphSmallMedium">Select image</span>
                    <span className="paragraphSmallItalic text-neutral-500 ml-1">(1080px x 1080px recommended)</span>
                  </div>

                  <div className={`flex flex-row justify-between ${showSelectContent && "hidden"}`}>
                    <button className="justify-center py-3 border-neutral-300 rounded-md border w-1/2 mr-[8.5px] mobile:mr-1 mobile:text-sm cursor-pointer" onClick={uploadImageShowHideController}>
                      Upload an image
                    </button>
                    <button className="justify-center py-3 border-neutral-300 rounded-md border w-1/2 ml-[8.5px] mobile:ml-1 mobile:text-sm cursor-pointer" onClick={handleClickSelectImageContent}>
                      Select from library
                    </button>
                  </div>

                  <div className={`${!showSelectContent ? "hidden" : "flex"} flex-row justify-between`}>
                    <img src={pizza1} className="w-[197px] h-[104px] rounded-md" alt="img" />
                    <div className="max-w-[197px] w-full pl-4">
                      <div className="mb-2">
                        <LargeTertiaryButton name="Replace image" />
                      </div>
                      <LargeTertiaryButton name="Upload another" />
                    </div>
                  </div>
                </div>
              </div>
              <div className="pl-6 ml-6 border-l border-neutral-300 md:pl-0 md:ml-0 md:border-l-0 md:border-t md:pt-6 md:mt-6">
                <div className="w-[411px] lg:w-full md:w-full">
                  <div className="mb-4">
                    <div className="flex justify-between items-center paragraphSmallMedium mb-1">
                      <span>ગુજરાતી: Notification title</span>
                      <div className="text-primary-500 flex justify-between items-center cursor-pointer">
                        <span className="mr-[3px]">Add meta tag</span>
                        <DownArrow fill="#6C5DD3" width={16} height={16} />
                      </div>
                    </div>
                    <DefaultInputField placeholder="Enter notification title" />
                  </div>
                  <div className="mb-2.5">
                    <div className="flex justify-between items-center paragraphSmallMedium mb-1">
                      <span>ગુજરાતી: Description</span>
                      <div className="text-primary-500 flex justify-between items-center cursor-pointer">
                        <span className="mr-[3px]">Add meta tag</span>
                        <DownArrow fill="#6C5DD3" width={16} height={16} />
                      </div>
                    </div>
                    <InputArea boxHeight="h-[88px]" boxPaddingB="pb-4" resizeNone="resize-none" placeholder="Enter notification description" />
                  </div>
                  <div className="mb-4 relative">
                    <DropDown label="ગુજરાતી: Display screen" dropdownLabel="Select display screen" buttonTextColor="neutral-300" />
                  </div>

                  <div className="mb-4">
                    <span className="paragraphSmallMedium mb-1 block">ગુજરાતી: Schedule notification</span>
                    <div className="flex flex-row">
                      <div className="w-1/2 mr-[4.5px]">
                        <DropDownNotification menuIcon={<Calender width={20} height={20} />} truncateText="w-[98px] md:w-[85px] truncate block text-left" />
                      </div>
                      <div className="w-1/2 ml-[4.5px]">
                        <DropDownNotification menuIcon={<TimerIcon />} menuItems={["12:45 PM"]} disableMenuIcon={<TimerIcon stroke="#D3D2D8" />} disable={true} />
                      </div>
                    </div>
                  </div>
                  <div>
                    <div className="mb-2">
                      <span className="paragraphSmallMedium">ગુજરાતી: Select image</span>
                      <span className="paragraphSmallItalic text-neutral-500 ml-1">(1080px x 1080px recommended)</span>
                    </div>

                    <div className="flex flex-row justify-between">
                      <button className="justify-center py-3 border-neutral-300 rounded-md border w-1/2 mr-[8.5px] mobile:mr-1 mobile:text-sm cursor-pointer" onClick={uploadImageShowHideController}>
                        Upload an image
                      </button>
                      <button className="justify-center py-3 border-neutral-300 rounded-md border w-1/2 ml-[8.5px] mobile:ml-1 mobile:text-sm cursor-pointer" onClick={handleClickSelectImageContent}>
                        Select from library
                      </button>
                    </div>

                    <div className="lg:mr-[8.5px]">
                      <button className="justify-center h-12 border-neutral-300 rounded-md border w-[197px] lg:w-1/2 mt-2.5 mobile:text-sm">Same as English</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="flex items-center justify-between mt-12 md:fixed md:left-0 md:right-0 md:bottom-0 md:bg-white md:shadow-dropShadow md:px-4 md:pt-2 md:pb-6">
              <div className="w-full max-w-[411px] mr-[9px]">
                <LargeDestructiveButton name="Delete" />
              </div>
              <div className="w-full max-w-[411px] ml-[9px]">
                <LargePrimaryButton name="Resend" />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className={`${!showHideUploadPopup && "hidden"}`}>
        <UploadImage
          isShowBackArrow={true}
          showHideUploadImagePage={uploadImageShowHideController}
          selectType={selectType}
          page="banner"
          mobilePadding="md:px-0 md:py-0 md:pb-4"
          mobileBg="bg-transparent"
        />
      </div>
    </>
  );
};

export default NotificationDetailsPopup;
