import React, { useState } from "react";
import { ReactComponent as DownArrow } from "../../Assets/chevron-down.svg";

const EditPresetDetailsDropdown = (props) => {
    const [isShowDetails, setIsShowDetails] = useState(false);

    const handleClickShowDetails = () => {
        setIsShowDetails(!isShowDetails);
    };

    return (
        <>
            <div className="w-full px-4 py-3 border border-neutral-300 rounded-md cursor-pointer" onClick={handleClickShowDetails}>
                <div className="flex flex-row items-center justify-between">
                    <div>
                        <h3 className="paragraphOverlineSmall text-neutral-700 mb-1 uppercase">Preset Name:</h3>
                        <span className="paragraphSmallRegular">{props.preseName}</span>
                    </div>
                    <div className={`${isShowDetails && "rotate-180"}`}>
                        <DownArrow />
                    </div>
                </div>
                <div className={`${!isShowDetails && "hidden"}`}>
                    <div>
                        <span className="paragraphOverlineSmall text-neutral-700 uppercase">Notification Title:</span>
                        <div className="paragraphSmallRegular">{props.notificationTitle}</div>
                    </div>
                    <div className="pt-2">
                        <span className="paragraphOverlineSmall text-neutral-700 uppercase">Description:</span>
                        <div className="paragraphSmallRegular">{props.description}</div>
                    </div>
                    <div className="pt-2 flex items-center">
                        <span className="paragraphOverlineSmall text-neutral-700 uppercase">Date:</span>
                        <span className="paragraphSmallRegular ml-1">{props.modificationDate}</span>
                    </div>

                    <div className="pt-2 flex flex-row justify-between items-center">
                        <div className="flex flex-row items-center">
                            <span className="paragraphOverlineSmall text-neutral-700 uppercase">Time:</span>
                            <div className="paragraphSmallRegular ml-1 flex flex-row items-center">
                                <span className="paragraphSmallRegular ml-1">{props.modificationTime}</span>
                            </div>
                        </div>
                        <span className="paragraphSmallUnderline text-primary-500 cursor-pointer" onClick={() => props.handleshowPresetpopup()}>View details</span>
                    </div>
                </div>
            </div>
        </>
    );
}

export default EditPresetDetailsDropdown
