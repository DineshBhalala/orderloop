import React, { useState } from "react";
import { DefaultBreadcrumbs } from "../../Components/Breadcrumbs/Breadcrumbs";
import { ReactComponent as NotificationIcon } from "../../Assets/notifications.svg";
import { LargePrimaryButton } from "../../Components/Buttons/Button";
import { ReactComponent as Calender } from "../../Assets/calendar.svg";
import { ReactComponent as Add } from "../../Assets/add.svg";
import { ReactComponent as Select } from "../../Assets/select.svg";
import { TabBig } from "../../Components/Tabs/Tabs";
import PaginationWithNumber from "../../Components/Pagination/PaginationWithNumber";
import ModificationDateTime from "./ModificationDateTime";
import { ReactComponent as Timer } from "../../Assets/timer.svg";
import EditPresetPopup from "./EditPresetPopup";
import CreateNotificationPopup from "./CreateNotificationPopup";
import NotificationDetailsPopup from "./NotificationDetailsPopup";
import NotificationsDetailsDropdown from "./NotificationsDetailsDropdown";
import EditPresetDetailsDropdown from "./EditPresetDetailsDropdown";
import CalenderField from "../../Components/Calender/CalenderField";

const Notifications = () => {
  const notificationsLists = [
    {
      notificationTitle: "Welcome new member",
      description: "Welcome to the mobile app",
      screen: "Home",
      date: <ModificationDateTime icon={<Calender />} statusLabel="21 November, 2022" />,
      time: <ModificationDateTime icon={<Timer />} statusLabel="17:54 PM" />,
    },
    {
      notificationTitle: "Hola people",
      description: "We reached 20k downloads!!!",
      screen: "Offer",
      date: <ModificationDateTime icon={<Calender />} statusLabel="18 November, 2022" />,
      time: <ModificationDateTime icon={<Timer />} statusLabel="21:00 PM" />,
    },
    {
      notificationTitle: "Welcome to Wolf Puck",
      description: "Wolf Puck opens up a new branch in the middletown city. Now available near your place.",
      screen: "Cart",
      date: <ModificationDateTime icon={<Calender />} statusLabel="31 October, 2022" />,
      time: <ModificationDateTime icon={<Timer />} statusLabel="10:00 AM" />,
    },
    {
      notificationTitle: "We miss you!!!",
      description: "Hello %customer_name%, we miss you as you haven’t order in a long time. We are here for an...",
      screen: "Home",
      date: <ModificationDateTime icon={<Calender />} statusLabel="07 May, 2022" />,
      time: <ModificationDateTime icon={<Timer />} statusLabel="14:15 PM" />,
    },
  ];

  const presetsList = [
    {
      preseName: "Abandoned carts",
      notificationTitle: "Don’t leave us!",
      description: "We feel sad that you went away form us. That’s why we generated a special coupon code for yo...",
      modificationDate: <ModificationDateTime icon={<Calender />} statusLabel="21 November, 2022" />,
      modificationTime: <ModificationDateTime icon={<Timer />} statusLabel="17:54 PM" />,
    },
    {
      preseName: "Cashback notifications",
      notificationTitle: "Congratulation %customer_name%!",
      description: "Congratulations %customer_name%, you have won a cashback of ₹100 on your current order.",
      modificationDate: <ModificationDateTime icon={<Calender />} statusLabel="18 November, 2022" />,
      modificationTime: <ModificationDateTime icon={<Timer />} statusLabel="21:00 PM" />,
    },
    {
      preseName: "Forgotten customers",
      notificationTitle: "We miss you %customer_name%!",
      description: "%customer_name%, this is just for you because we value %customer_name% as our esteemed c...",
      modificationDate: <ModificationDateTime icon={<Calender />} statusLabel="31 October, 2022" />,
      modificationTime: <ModificationDateTime icon={<Timer />} statusLabel="10:00 AM" />,
    },
    {
      preseName: "Outlet notifications",
      notificationTitle: "%outlet_name%",
      description: "%outlet_name% introduces you the BOGO offer for this week. Visit us and redeem the offer via...",
      modificationDate: <ModificationDateTime icon={<Calender />} statusLabel="07 May, 2022" />,
      modificationTime: <ModificationDateTime icon={<Timer />} statusLabel="14:15 PM" />,
    },
  ];

  const [isActiveNotificationsList, setNotificationsList] = useState(true);
  const [isAcitvePresetsList, setPresetsList] = useState(false);
  const handleClickNotifications = () => {
    setPresetsList(false);
    setNotificationsList(true);
  };
  const handleClickPresets = () => {
    setPresetsList(true);
    setNotificationsList(false);
  };

  const [showPresetpopup, setShowPresetpopup] = useState(false);
  const handleshowPresetpopup = () => {
    setShowPresetpopup(!showPresetpopup);
  };

  const [showNotificationPopup, setShowNotificationPopup] = useState(false);
  const handleNotificationPopup = () => {
    setShowNotificationPopup(!showNotificationPopup);
  };

  const [showNotificationDetailPopup, setshowNotificationDetailPopup] = useState(false);
  const handleNotificationDetailPopup = () => {
    setshowNotificationDetailPopup(!showNotificationDetailPopup);
  };

  return (
    <>
      <div className="bg-[#fafafa]">
        <div className="px-8 pb-[70px] lg:px-4 lg:pb-[23px] pt-4 w-full max-w-[1336px] mx-auto bg-white">
          <div className={`${(showPresetpopup || showNotificationPopup || showNotificationDetailPopup) && "md:hidden"}`}>
            <div className="mb-4 md:hidden">
              <DefaultBreadcrumbs icon={<NotificationIcon className="h-5 w-5" />} mainTab="Notifications" />
            </div>
            <div className="pb-4 border-b md:border-none border-b-neutral-300 mb-4 md:mb-0">
              <div className="flex flex-row">
                <div className="w-[154px] md:mr-[7px] mobile:mr-1 md:w-1/2 mobile:w-full" onClick={handleClickNotifications}>
                  <TabBig label="Notifications list" isActive={isActiveNotificationsList} />
                </div>
                <div className="w-[116px] ml-4 md:ml-[7px] mobile:ml-1 md:w-1/2 mobile:min-w-[120px] cursor-pointer" onClick={handleClickPresets}>
                  <TabBig label="Presets list" isActive={isAcitvePresetsList} />
                </div>
              </div>
            </div>
            <div className={isActiveNotificationsList === true ? "block" : "hidden"}>
              <div className="flex md:block justify-between lg:justify-start">
                <div className="flex">
                  <CalenderField label="25 Sept 2022 - 09 Oct 2022" buttonStyle="mobile:pr-0 mobile:pl-1 md:w-full" labelStyle="mobile:text-sm mobile:mx-0" />
                  <div className="max-w-[156px] w-full md:max-w-full md:w-[56px] ml-4 lg:ml-2">
                    <LargePrimaryButton leftIconDefault={<Select stroke="#ffffff" />} leftIconClick={<Select stroke="#C4BEED" />} name="Bulk select" hideName="md:hidden" />
                  </div>
                </div>
                <div className="w-[214px] lg:ml-2 md:ml-0 md:mt-4 md:w-full cursor-pointer" onClick={handleNotificationPopup}>
                  <LargePrimaryButton isDefault={false} leftIconDefault={<Add stroke="#ffffff" />} leftIconClick={<Add stroke="#C4BEED" />} name="Create notification" />
                </div>
              </div>
              <div className="mt-6 mb-4 md:m-0">
                <div className="w-full border border-[#CBD5E1] rounded-lg md:hidden overflow-auto [&::-webkit-scrollbar]:hidden">
                  <table className="w-full table-fixed break-words">
                    <thead>
                      <tr className="bg-neutral-50 uppercase paragraphOverlineSmall text-neutral-700">
                        <th className="text-left pl-6 w-[236px] h-11">Notification Title</th>
                        <th className="text-left pl-6 w-[378px]">Description</th>
                        <th className="text-left pl-6 w-[202px]">Screen</th>
                        <th className="text-left pl-6 w-[234px]">Date</th>
                        <th className="text-left pl-6 w-[222px]">Time</th>
                      </tr>
                    </thead>
                    <tbody>
                      {notificationsLists.map((row, index) => (
                        <tr key={index} className="even:bg-neutral-50 border-t border-[#CBD5E1] paragraphSmallRegular">
                          <td className="py-[14px] pl-6 h-[70px] cursor-pointer" onClick={handleNotificationDetailPopup}>
                            {row.notificationTitle}
                          </td>
                          <td className="py-[14px] pl-6">{row.description}</td>
                          <td className="py-[14px] pl-6">{row.screen}</td>
                          <td className="py-[14px] pl-6">{row.date}</td>
                          <td className="py-[14px] pl-6">{row.time} </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
                <div className="hidden md:block mt-3">
                  {notificationsLists.map((el, index) => {
                    return (
                      <div className="mb-1 pt-1" key={index}>
                        <NotificationsDetailsDropdown
                          notificationTitle={el.notificationTitle}
                          description={el.description}
                          screen={el.screen}
                          date={el.date}
                          time={el.time}
                          handleNotificationDetailPopup={handleNotificationDetailPopup}
                        />
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>

            <div className={isAcitvePresetsList === true ? "block" : "hidden"}>
              <div className="mt-6 mb-4 md:m-0">
                <div className="w-full border border-[#CBD5E1] rounded-lg md:hidden overflow-auto [&::-webkit-scrollbar]:hidden">
                  <table className="w-full table-fixed break-words">
                    <thead>
                      <tr className="bg-neutral-50 uppercase paragraphOverlineSmall text-neutral-700">
                        <th className="text-left pl-6 w-[198px] h-11">Preset Name</th>
                        <th className="text-left pl-6 w-[236px]">Notification Title</th>
                        <th className="text-left pl-6 w-[378px]">Description</th>
                        <th className="text-left pl-6 w-[225px]">Modification Date</th>
                        <th className="text-left pl-6 w-[234px]">Modification Time</th>
                      </tr>
                    </thead>
                    <tbody>
                      {presetsList.map((row, index) => (
                        <tr key={index} className="even:bg-neutral-50 border-t border-[#CBD5E1] paragraphSmallRegular">
                          <td className="py-[14px] pl-6 h-[70px] cursor-pointer" onClick={handleshowPresetpopup}>
                            {row.preseName}
                          </td>
                          <td className="py-[14px] pl-6">{row.notificationTitle}</td>
                          <td className="py-[14px] pl-6">{row.description}</td>
                          <td className="py-[14px] pl-6">{row.modificationDate}</td>
                          <td className="py-[14px] pl-6">{row.modificationTime}</td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
                <div className="hidden md:block">
                  {presetsList.map((el, index) => {
                    return (
                      <div className="mb-1 pt-1" key={index}>
                        <EditPresetDetailsDropdown
                          preseName={el.preseName}
                          notificationTitle={el.notificationTitle}
                          description={el.description}
                          modificationDate={el.modificationDate}
                          modificationTime={el.modificationTime}
                          handleshowPresetpopup={handleshowPresetpopup}
                        />
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>

            <div className="md:hidden">
              <PaginationWithNumber />
            </div>
          </div>
          <div className={`${!showPresetpopup && "hidden"}`}>
            <EditPresetPopup handleshowPresetpopup={handleshowPresetpopup} />
          </div>
          <div className={`${!showNotificationPopup && "hidden"}`}>
            <CreateNotificationPopup handleNotificationPopup={handleNotificationPopup} />
          </div>
          <div className={`${!showNotificationDetailPopup && "hidden"}`}>
            <NotificationDetailsPopup handleNotificationDetailPopup={handleNotificationDetailPopup} />
          </div>
        </div>
      </div>
    </>
  );
};
export default Notifications;
