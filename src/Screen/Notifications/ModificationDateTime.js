import React from 'react'

const ModificationDateTime = (props) => {
    return (
        <>
            <div className='flex items-center'>
                <span>{props.icon}</span>
                <span className='pl-1'>{props.statusLabel}</span>
            </div>
        </>
    )
}

export default ModificationDateTime