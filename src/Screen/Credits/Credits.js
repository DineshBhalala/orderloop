import React, { useState } from "react";
import { ReactComponent as Notifications } from "../../Assets/notifications.svg";
import { ReactComponent as Credit } from "../../Assets/credits.svg";
import { LargePrimaryButton } from "../../Components/Buttons/Button";
import { ReactComponent as Calender } from "../../Assets/calendar.svg";
import { ReactComponent as Add } from "../../Assets/add.svg";
import { ReactComponent as Select } from "../../Assets/select.svg";
import { ReactComponent as Export } from "../../Assets/export.svg";
import { ReactComponent as Recharge } from "../../Assets/recharge.svg";
import { ReactComponent as SuccessTick } from "../../Assets/success-tick.svg";
import { ReactComponent as Reimbursement } from "../../Assets/reimbursement.svg";
import { ReactComponent as Schedule } from "../../Assets/schedule.svg";
import { ReactComponent as Close } from "../../Assets/close.svg";
import { TabBig } from "../../Components/Tabs/Tabs";
import DashBoardCard from "../../Components/DashBoardCard/DashBoardCard";
import CreditStatus from "./CreditStatus";
import RechargeDate from "./RechargeDate";
import PaginationWithNumber from "../../Components/Pagination/PaginationWithNumber";
import { DefaultInputField } from "../../Components/InputField/InputField";
import { DropDownTabs } from "../../Components/DropDown/DropDownTabs";
import { DefaultBreadcrumbs } from "../../Components/Breadcrumbs/Breadcrumbs";
import CalenderField from "../../Components/Calender/CalenderField";

const Credits = () => {
    const infrastructureContent = [
        {
            title: "Available infrastructure credits",
            amount: "4,500.00",
            reimbursement: "These credits are not real-time",
            icon: <Credit className="h-5 w-5" stroke="#ffffff" />,
        },
        {
            title: "Number of recharges",
            amount: "5",
            reimbursement: "Total number of recharge done this week",
            icon: <Recharge className="h-5 w-5" stroke="#ffffff" />,
        },
    ];
    const creditsContent = [
        {
            title: "Available service credits",
            amount: "503.00",
            reimbursement: "These credits are not real-time",
            icon: <Credit className="h-5 w-5" stroke="#ffffff" />,
        },
    ];

    const infrastructureReferences = [
        {
            referenceId: "XFrwLGhOiN",
            type: "Deduct",
            service: "google_geocode_api",
            status: <CreditStatus icon={<SuccessTick />} statusLabel="Deducted" />,
            amount: "₹50.00/-",
            rechargeDate: <RechargeDate iconDate={<Calender />} rechargeDate="28 Nov 2022" iconTime={<Schedule />} rechargeTime={"12:05 PM"} />,
        },
        {
            referenceId: "bI5iYbFDTm",
            type: "Deduct",
            service: "google_geocode_api",
            status: <CreditStatus icon={<SuccessTick />} statusLabel="Deducted" />,
            amount: "₹750.00/-",
            rechargeDate: <RechargeDate iconDate={<Calender />} rechargeDate="25 Nov 2022" iconTime={<Schedule />} rechargeTime={"01:07 PM"} />,
        },
        {
            referenceId: "4R6QMLUuu6",
            type: "Deduct",
            service: "google_distance_matrix_api",
            status: <CreditStatus icon={<SuccessTick />} statusLabel="Deducted" />,
            amount: "₹285.00/-",
            rechargeDate: <RechargeDate iconDate={<Calender />} rechargeDate="19 Nov 2022" iconTime={<Schedule />} rechargeTime={"06:00 PM"} />,
        },
        {
            referenceId: "36rZJPTzJc",
            type: "Deduct",
            service: "google_geocode_api",
            status: <CreditStatus icon={<SuccessTick />} statusLabel="Deducted" />,
            amount: "₹125.00/-",
            rechargeDate: <RechargeDate iconDate={<Calender />} rechargeDate="15 Nov 2022" iconTime={<Schedule />} rechargeTime={"08:30 PM"} />,
        },
        {
            referenceId: "GtcgjJl2SK",
            type: "Deduct",
            service: "google_distance_matrix_api",
            status: <CreditStatus icon={<SuccessTick />} statusLabel="Deducted" />,
            amount: "₹75.00/-",
            rechargeDate: <RechargeDate iconDate={<Calender />} rechargeDate={"10 Nov 2022"} iconTime={<Schedule />} rechargeTime={"05:00 PM"} />,
        },
        {
            referenceId: "iZlLvLEF1n",
            type: "Deduct",
            service: "google_geocode_api",
            status: <CreditStatus icon={<SuccessTick />} statusLabel="Deducted" />,
            amount: "₹95.00/-",
            rechargeDate: <RechargeDate iconDate={<Calender />} rechargeDate={"04 Nov 2022"} iconTime={<Schedule />} rechargeTime={"03:00 AM"} />,
        },
    ];

    const serviceReferences = [
        {
            referenceId: "XFrwLGhOiN",
            type: "Credit",
            service: "Reimbursement",
            status: <CreditStatus icon={<Reimbursement />} statusLabel={"Reimbursement"} />,
            amount: "₹50.00/-",
            rechargeDate: <RechargeDate iconDate={<Calender />} rechargeDate="28 Nov 2022" iconTime={<Schedule />} rechargeTime={"12:05 PM"} />,
        },
        {
            referenceId: "bI5iYbFDTm",
            type: "Deduct",
            service: "Paytm refund",
            status: <CreditStatus icon={<SuccessTick />} statusLabel="Deducted" />,
            amount: "₹750.00/-",
            rechargeDate: <RechargeDate iconDate={<Calender />} rechargeDate="25 Nov 2022" iconTime={<Schedule />} rechargeTime={"01:07 PM"} />,
        },
        {
            referenceId: "4R6QMLUuu6",
            type: "Credit",
            service: "Reimbursement",
            status: <CreditStatus icon={<Reimbursement />} statusLabel={"Reimbursement"} />,
            amount: "₹285.00/-",
            rechargeDate: <RechargeDate iconDate={<Calender />} rechargeDate="19 Nov 2022" iconTime={<Schedule />} rechargeTime={"06:00 PM"} />,
        },
        {
            referenceId: "36rZJPTzJc",
            type: "Deduct",
            service: "Paytm refund",
            status: <CreditStatus icon={<SuccessTick />} statusLabel="Deducted" />,
            amount: "₹125.00/-",
            rechargeDate: <RechargeDate iconDate={<Calender />} rechargeDate="15 Nov 2022" iconTime={<Schedule />} rechargeTime={"08:30 PM"} />,
        },
        {
            referenceId: "GtcgjJl2SK",
            type: "Deduct",
            service: "Paytm refund",
            status: <CreditStatus icon={<SuccessTick />} statusLabel="Deducted" />,
            amount: "₹75.00/-",
            rechargeDate: <RechargeDate iconDate={<Calender />} rechargeDate={"10 Nov 2022"} iconTime={<Schedule />} rechargeTime={"05:00 PM"} />,
        },
        {
            referenceId: "iZlLvLEF1n",
            type: "Credit",
            service: "Reimbursement",
            status: <CreditStatus icon={<Reimbursement />} statusLabel={"Reimbursement"} />,
            amount: "₹95.00/-",
            rechargeDate: <RechargeDate iconDate={<Calender />} rechargeDate={"04 Nov 2022"} iconTime={<Schedule />} rechargeTime={"03:00 AM"} />,
        },
    ];

    const [isActiveInfrastructure, setInfrastructure] = useState(true);
    const [isAcitveCredit, setCredt] = useState(false);
    const handleClickInfrastructure = () => {
        setCredt(false);
        setInfrastructure(true);
    };
    const handleClickCredit = () => {
        setCredt(true);
        setInfrastructure(false);
    };
    const [showAlertPopup, setShowAlertPopup] = useState(false);
    const [showAddCredits, setshowAddCredits] = useState(false);

    return (
        <>
            <div className="bg-[#fafafa]">
                <div className="px-8 pb-[70px] lg:px-4 lg:pb-[23px] pt-4 w-full max-w-[1336px] mx-auto bg-white">
                    <div className="mb-4 md:hidden">
                        <DefaultBreadcrumbs icon={<Credit className="h-5 w-5" />} mainTab="Credits" />
                    </div>
                    <div className="flex flex-row justify-between pb-4 border-b md:border-none border-b-neutral-300 mb-4 md:mb-0">
                        <div className="flex flex-row md:hidden">
                            <div className="w-[191px] md:w-24 cursor-pointer" onClick={handleClickInfrastructure}>
                                <TabBig label="Infrastructure credits" isActive={isActiveInfrastructure} />
                            </div>
                            <div className="ml-4 lg:ml-2 w-[145px] md:w-24 cursor-pointer" onClick={handleClickCredit}>
                                <TabBig label="Service credits" isActive={isAcitveCredit} />
                            </div>
                        </div>

                        <div className="hidden md:block w-full">
                            <DropDownTabs
                                menuItems={[
                                    { item: "Infrastructure credits", onClick: handleClickInfrastructure },
                                    { item: "Service credits", onClick: handleClickCredit },
                                ]}
                                tabStyle="md:paragraphSmallMedium md:text-primary-500 md:w-[134px] md:truncate md:block md:text-left"
                                itemStyle="paragraphSmallMedium"
                            />
                        </div>

                        <div className="flex flex-row">
                            <div className="w-[138px] md:w-[64px] mobile:w-1/2 mx-4 lg:mx-2 mobile:mr-0 cursor-pointer" onClick={() => setShowAlertPopup(true)}>
                                <LargePrimaryButton
                                    isDefault={false}
                                    leftIconDefault={<Notifications stroke="#ffffff" />}
                                    leftIconClick={<Notifications stroke="#C4BEED" />}
                                    name="Set alert"
                                    hideName="md:hidden"
                                />
                            </div>
                            <div className="w-[161px] md:w-[64px] mobile:w-1/2 mobile:ml-1 cursor-pointer" onClick={() => setshowAddCredits(true)}>
                                <LargePrimaryButton
                                    isDefault={false}
                                    leftIconDefault={<Add stroke="#ffffff" />}
                                    leftIconClick={<Add stroke="#C4BEED" />}
                                    fontsWeight={true}
                                    name="Add credits"
                                    hideName="md:hidden"
                                />
                            </div>
                        </div>
                    </div>
                    <div className="flex md:block justify-between mb-6 md:mb-0">
                        <div className="md:mb-4">
                            <CalenderField label="25 Sept 2022 - 09 Oct 2022" buttonStyle="md:w-full" />
                        </div>
                        <div className="w-full flex justify-between">
                            <div className="w-[156px] mx-4 lg:mx-2 md:ml-0 md:w-full mobile:mr-1">
                                <LargePrimaryButton leftIconDefault={<Select stroke="#ffffff" />} leftIconClick={<Select stroke="#C4BEED" />} name="Bulk select" />
                            </div>
                            <div className="w-[161px] md:ml-2 md:w-full mobile:ml-1">
                                <LargePrimaryButton leftIconDefault={<Export stroke="#ffffff" />} leftIconClick={<Export stroke="#C4BEED" />} name="Export data" />
                            </div>
                        </div>
                    </div>

                    <div className={isActiveInfrastructure === true ? "block" : "hidden"}>
                        <div className="-mx-2.5 lg:mx-0">
                            {infrastructureContent.map((el, index) => {
                                return (
                                    <div key={index} className="inline-block align-top mx-2.5 md:mt-4 lg:mx-0 w-[303px] lg:w-1/2 creditBox lg:odd:pr-1 lg:even:pl-1 md:w-full">
                                        <DashBoardCard title={el.title} amount={el.amount} reimbursement={el.reimbursement} icon={el.icon} cardType="credit" isReimbursementIcon={false} />
                                    </div>
                                );
                            })}
                        </div>
                        <div className="my-4 lg:mt-0">
                            <div className="w-full border border-neutral-300 rounded-lg lg:hidden overflow-auto [&::-webkit-scrollbar]:hidden">
                                <table className="w-full table-fixed break-words">
                                    <thead>
                                        <tr className="bg-neutral-50 uppercase paragraphOverlineSmall text-neutral-700 shadow-innerShadow">
                                            <th className="text-left py-[11.5px] pl-6">Reference Id</th>
                                            <th className="text-left py-[11.5px] pl-6">Type</th>
                                            <th className="text-left py-[11.5px] pl-6">Service</th>
                                            <th className="text-left py-[11.5px] pl-6">Status</th>
                                            <th className="text-left py-[11.5px] pl-6">Amount</th>
                                            <th className="text-left py-[11.5px] pl-6">Recharge Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {infrastructureReferences.map((row, index) => (
                                            <tr key={index} className={`even:bg-neutral-50 ${index !== 0 && "border-t"} border-neutral-300 paragraphSmallRegular`}>
                                                <td className="py-2 pl-6">{row.referenceId}</td>
                                                <td className="py-2 pl-6">{row.type}</td>
                                                <td className="py-2 pl-6">{row.service}</td>
                                                <td className="py-2 pl-6">{row.status}</td>
                                                <td className="py-2 pl-6">{row.amount} </td>
                                                <td className="py-2 pl-6">{row.rechargeDate}</td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                            <div className="hidden lg:block">
                                {infrastructureReferences.slice(1, 5).map((row, index) => (
                                    <div key={index} className="mt-4 w-1/2 md:w-full odd:pr-1 even:pl-1 md:odd:pr-0 md:even:pl-0 inline-block align-top">
                                        <div className="border border-neutral-300 rounded-lg p-4 paragraphSmallRegular">
                                            <div className="flex items-center pb-3">
                                                <span className="uppercase paragraphOverlineSmall pr-2">Reference Id:</span>
                                                <span>{row.referenceId}</span>
                                            </div>
                                            <div className="flex items-center pb-3">
                                                <span className="uppercase paragraphOverlineSmall pr-2">Type:</span>
                                                <span>{row.type}</span>
                                            </div>
                                            <div className="flex items-center pb-3">
                                                <span className="uppercase paragraphOverlineSmall pr-2">Service:</span>
                                                <span>{row.service}</span>
                                            </div>
                                            <div className="flex items-center pb-3">
                                                <span className="uppercase paragraphOverlineSmall pr-2">Status:</span>
                                                <span>{row.status}</span>
                                            </div>
                                            <div className="flex items-center pb-3">
                                                <span className="uppercase paragraphOverlineSmall pr-2">Amount:</span>
                                                <span>{row.amount}</span>
                                            </div>
                                            <div className="flex">
                                                <span className="uppercase paragraphOverlineSmall pr-2">Recharge Date:</span>
                                                <span>{row.rechargeDate}</span>
                                            </div>
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>

                    <div className={isAcitveCredit === true ? "block" : "hidden"}>
                        <div className="-mx-2.5 lg:mx-0">
                            {creditsContent.map((el, index) => {
                                return (
                                    <div key={index} className="inline-block align-top mx-2.5 md:mt-4 lg:mx-0 w-[303px] lg:w-1/2 creditBox lg:odd:pr-1 lg:even:pl-1 md:w-full">
                                        <DashBoardCard title={el.title} amount={el.amount} reimbursement={el.reimbursement} icon={el.icon} cardType="credit" isReimbursementIcon={false} />
                                    </div>
                                );
                            })}
                        </div>
                        <div className="my-4 lg:mt-0">
                            <div className="w-full border border-neutral-300 rounded-lg lg:hidden overflow-auto [&::-webkit-scrollbar]:hidden">
                                <table className="w-full table-fixed break-words">
                                    <thead>
                                        <tr className="bg-neutral-50 uppercase paragraphOverlineSmall text-neutral-700 shadow-innerShadow">
                                            <th className="text-left py-[11.5px] pl-6">Reference Id</th>
                                            <th className="text-left py-[11.5px] pl-6">Type</th>
                                            <th className="text-left py-[11.5px] pl-6">Service</th>
                                            <th className="text-left py-[11.5px] pl-6">Status</th>
                                            <th className="text-left py-[11.5px] pl-6">Amount</th>
                                            <th className="text-left py-[11.5px] pl-6">Recharge Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {serviceReferences.map((row, index) => (
                                            <tr key={index} className={`even:bg-neutral-50 ${index !== 0 && "border-t"} border-neutral-300 paragraphSmallRegular`}>
                                                <td className="py-2 pl-6">{row.referenceId}</td>
                                                <td className="py-2 pl-6">{row.type}</td>
                                                <td className="py-2 pl-6">{row.service}</td>
                                                <td className="py-2 pl-6">{row.status}</td>
                                                <td className="py-2 pl-6">{row.amount} </td>
                                                <td className="py-2 pl-6">{row.rechargeDate}</td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                            <div className="hidden lg:block">
                                {serviceReferences.slice(1, 5).map((row, index) => (
                                    <div key={index} className="mt-4 w-1/2 md:w-full odd:pr-1 even:pl-1 md:odd:pr-0 md:even:pl-0 inline-block align-top">
                                        <div className="border border-neutral-300 rounded-lg p-4 paragraphSmallRegular">
                                            <div className="flex items-center pb-3">
                                                <span className="uppercase paragraphOverlineSmall pr-2">Reference Id:</span>
                                                <span>{row.referenceId}</span>
                                            </div>
                                            <div className="flex items-center pb-3">
                                                <span className="uppercase paragraphOverlineSmall pr-2">Type:</span>
                                                <span>{row.type}</span>
                                            </div>
                                            <div className="flex items-center pb-3">
                                                <span className="uppercase paragraphOverlineSmall pr-2">Service:</span>
                                                <span>{row.service}</span>
                                            </div>
                                            <div className="flex items-center pb-3">
                                                <span className="uppercase paragraphOverlineSmall pr-2">Status:</span>
                                                <span>{row.status}</span>
                                            </div>
                                            <div className="flex items-center pb-3">
                                                <span className="uppercase paragraphOverlineSmall pr-2">Amount:</span>
                                                <span>{row.amount}</span>
                                            </div>
                                            <div className="flex">
                                                <span className="uppercase paragraphOverlineSmall pr-2">Recharge Date:</span>
                                                <span>{row.rechargeDate}</span>
                                            </div>
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>

                    <div className="md:hidden">
                        <PaginationWithNumber />
                    </div>
                </div>
            </div>

            <div className={`fixed bg-black bg-opacity-50 inset-0 z-50 flex px-4 ${showAlertPopup === false ? "hidden" : "block"}`}>
                <div className="w-[475px] rounded-xl bg-white px-8 py-6 md:p-4 m-auto">
                    <div className="flex justify-between items-center">
                        <div>
                            <h3 className="paragraphLargeMedium">Set alert</h3>
                            <p className="paragraphMediumItalic text-neutral-500">Select minimum amount to trigger alert</p>
                        </div>
                        <span className="cursor-pointer" onClick={() => setShowAlertPopup(false)}>
                            <Close />
                        </span>
                    </div>
                    <div className="pt-6 pb-12 md:py-3">
                        <DefaultInputField label="Alert amount" placeholder="Enter amount" boxHeight="11" />
                    </div>
                    <div>
                        <LargePrimaryButton name="Save" />
                    </div>
                </div>
            </div>
            <div className={`fixed bg-black bg-opacity-50 inset-0 z-50 flex px-4 ${showAddCredits === false ? "hidden" : "block"}`}>
                <div className="w-[475px] rounded-xl bg-white px-8 py-6 md:p-4 m-auto">
                    <div className="flex justify-between items-center">
                        <div>
                            <h3 className="paragraphLargeMedium">Add credits</h3>
                            <p className="paragraphMediumItalic text-neutral-500">Min. recharge amount required is ₹250.00</p>
                        </div>
                        <span className="cursor-pointer" onClick={() => setshowAddCredits(false)}>
                            <Close />
                        </span>
                    </div>
                    <div className="pt-6 pb-2">
                        <DefaultInputField label="Add recharge amount" placeholder="Add amount" boxHeight="11" />
                    </div>
                    <div>
                        <div className="inline-block align-top px-4 pt-2.5 pb-2 border border-primary-500 rounded text-center mr-2 mb-3 cursor-default">
                            <div className="flex">
                                <Add stroke="#6C5DD3" width={"18"} height={"18"} className="mx-auto" />
                            </div>
                            <div className="paragraphSmallRegular text-primary-500 pt-1.5">₹250.00</div>
                        </div>
                        <div className="inline-block align-top px-4 pt-2.5 pb-2 border border-primary-500 rounded text-center mr-2 mb-3 cursor-default">
                            <div className="flex">
                                <Add stroke="#6C5DD3" width={"18"} height={"18"} className="mx-auto" />
                            </div>
                            <div className="paragraphSmallRegular text-primary-500 pt-1.5">₹500.00</div>
                        </div>
                        <div className="inline-block align-top px-4 pt-2.5 pb-2 border border-primary-500 rounded text-center mr-2 mb-3 cursor-default">
                            <div className="flex">
                                <Add stroke="#6C5DD3" width={"18"} height={"18"} className="mx-auto" />
                            </div>
                            <div className="paragraphSmallRegular text-primary-500 pt-1.5">₹1,000.00</div>
                        </div>
                        <div className="inline-block align-top px-4 pt-2.5 pb-2 border border-primary-500 rounded text-center mr-2 mb-3 cursor-default">
                            <div className="flex">
                                <Add stroke="#6C5DD3" width={"18"} height={"18"} className="mx-auto" />
                            </div>
                            <div className="paragraphSmallRegular text-primary-500 pt-1.5">₹1,500.00</div>
                        </div>
                    </div>
                    <div className="paragraphMediumItalic pt-3 pb-12 md:hidden">250 credits = ₹250.00 + processing fees </div>
                    <div>
                        <LargePrimaryButton name="Proceed to payment" />
                    </div>
                </div>
            </div>
        </>
    );
};

export default Credits;
