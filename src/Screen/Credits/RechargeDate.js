import React from 'react'

const RechargeDate = (props) => {
    return (
        <>
            <div className='flex items-center pb-0.5'>
                <span>{props.iconDate}</span>
                <span className='pl-1'>{props.rechargeDate}</span>
            </div>
            <div className='flex items-center pt-[3px]'>
                <span>{props.iconTime}</span>
                <span className='pl-1'>{props.rechargeTime}</span>
            </div>
        </>
    )
}

export default RechargeDate