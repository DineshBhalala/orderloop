import React from "react";
import { ReactComponent as LeftArrowIcon } from "../../../Assets/chevron-down.svg";
import { ReactComponent as CloseIcon } from "../../../Assets/close.svg";
import { DefaultInputField } from "../../../Components/InputField/InputField";
import DropDown from "../../../Components/DropDown/DropDown";
import { LargeDestructiveButton, LargePrimaryButton, LargeTertiaryButton } from "../../../Components/Buttons/Button";
import qrCode from "../../../Assets/qr-code.png";

export default function TablePopup(props) {
    const subSectionMenuItems = ["Indoor", "Outdoor"];

    const QRCodeSection = (header) => (
        <div className="flex flex-col">
            <span className="paragraphSmallMedium">{header} QR code</span>
            <div className="flex flex-row items-start mt-2">
                <img src={qrCode} alt="" className="mr-4 md:mr-2" />
                <div className="flex flex-col w-[197px]">
                    <div className="mb-2">
                        <LargeTertiaryButton name="Download" />
                    </div>

                    <LargeTertiaryButton name="Share" />
                </div>
            </div>
        </div>
    );

    return (
        <>
            <div className="fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 flex justify-center items-center overflow-auto md:bg-white md:relative px-4">
                <div
                    className={`${props.popupName === "editTable" ? "max-w-[934px] lg:max-w-[734px]" : "max-w-[475px]"
                        } w-full rounded-xl md:rounded-none bg-shades-50 px-8 py-6 md:px-0 md:py-4 m-auto md:max-w-full`}
                >
                    <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer" onClick={() => props.handleClickClose()}>
                        <LeftArrowIcon className="rotate-90" />
                        <span className="paragraphMediumMedium">Back to table management</span>
                    </div>
                    <div className="flex flex-row justify-between items-center mb-6 md:hidden">
                        <div className="flex flex-col">
                            <span className="paragraphLargeMedium">{props.popupName === "editTable" ? "Edit table" : "Add new table"}</span>
                            <span className="paragraphMediumItalic text-neutral-500">
                                {props.popupName === "editTable" ? "Edit your existing table details" : "Create a new table for your restaurant outlet"}
                            </span>
                        </div>

                        <div className="md:hidden cursor-pointer" onClick={() => props.handleClickClose()}>
                            <CloseIcon />
                        </div>
                    </div>
                    <div className="flex flex-row items-start md:mb-20 md:block">
                        <div
                            className={`${props.popupName === "editTable"
                                    ? "max-w-[435px] w-full pr-6 border-r border-neutral-300 lg:max-w-[335px] md:max-w-full md:pr-0 md:border-b md:border-r-0 md:pb-4 md:mb-4"
                                    : "w-full"
                                }`}
                        >
                            <div className="mb-4">
                                <DefaultInputField label="Table name" placeholder="Enter new table name" />
                            </div>

                            <div className="mb-4 relative">
                                <DropDown label="Select section" menuItems={["Terrace"]} placeholder="Enter new table name" />
                            </div>

                            <div className="mb-4 relative">
                                <DropDown label="Select sub-section" menuItems={subSectionMenuItems} placeholder="Enter new table name" />
                            </div>

                            <div className="mb-4">
                                <DefaultInputField label="Table capacity" placeholder="Enter table capacity" />
                            </div>

                            <DefaultInputField label="Customer URL" placeholder="Enter deeplink URL" />
                        </div>
                        <div className={`${props.popupName !== "editTable" ? "hidden" : "flex flex-col"} ml-6 md:ml-0`}>
                            <div className="mb-6">{QRCodeSection("Table")}</div>
                            {QRCodeSection("Deeplink")}
                        </div>
                    </div>
                    {props.popupName === "editTable" ? (
                        <div className="flex flex-row border-none px-0 py-0 md:z-[8] md:fixed md:justify-center md:border-none md:py-0 md:pb-1 md:pt-2 md:shadow-dropShadow left-0 right-0 bottom-0 w-full mx-auto bg-white border-t border-neutral-300 md:px-4 mt-12">
                            <div className="w-1/2 mr-2">
                                <LargeDestructiveButton name="Delete table" />
                            </div>
                            <div className="w-1/2 ml-2">
                                <LargePrimaryButton name="Save changes" />
                            </div>
                        </div>
                    ) : (
                        <div className="border-none px-0 py-0 md:z-[8] md:fixed md:justify-center md:border-none md:py-0 md:pb-1 md:pt-2 md:shadow-dropShadow left-0 right-0 bottom-0 flex w-full mx-auto bg-white border-t border-neutral-300 md:px-4 mt-12 md:mt-0">
                            <div className="w-full">
                                <LargePrimaryButton name="Add table" />
                            </div>
                            {/* <div className="w-full mt-12">
                            <LargePrimaryButton name="Add table" />
                        </div> */}
                        </div>
                    )}
                </div>
            </div>
        </>
    );
}
