import React, { useState } from "react";
import { ReactComponent as CloseIcon } from "../../../Assets/close.svg";
import { ReactComponent as LeftArrowIcon } from "../../../Assets/chevron-down.svg";
import { DefaultInputField } from "../../../Components/InputField/InputField";
import DropDown from "../../../Components/DropDown/DropDown";
import { LargeDestructiveButton, LargePrimaryButton } from "../../../Components/Buttons/Button";

export default function SectionPopup(props) {
    const orderingType = ["Dine-in", "Takeaway", "QSR", "Fine-dine"];

    const handleClickAddSection = () => {
        setSubSection(subSection + 1);
    };
    const handleClickRemoveSection = () => {
        setSubSection(0);
    };

    const [subSection, setSubSection] = useState(0);

    const handleClickRemoveSection2 = () => {
        setSubSection(1);
    };

    return (
        <>
            <div className="fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 flex justify-center items-center overflow-auto md:bg-white md:relative px-4">
                <div className="max-w-[475px] w-full rounded-xl md:rounded-none bg-shades-50 px-8 py-6 md:px-0 md:py-4 m-auto md:max-w-full">
                    <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer" onClick={() => props.handleClickClose()}>
                        <LeftArrowIcon className="rotate-90" />
                        <span className="paragraphMediumMedium pl-1">Back to table management</span>
                    </div>

                    <div className="flex flex-row justify-between items-center mb-6 md:hidden">
                        <div className="flex flex-col">
                            <span className="paragraphLargeMedium">{props.popupName === "editSection" ? "Edit section" : "Add new section"}</span>
                            <span className="paragraphMediumItalic text-neutral-500">
                                {props.popupName === "editSection" ? "Edit your existing restaurant section" : "Create a new section for your restaurant outlet"}
                            </span>
                        </div>

                        <div className="md:hidden cursor-pointer cursor-pointer" onClick={() => props.handleClickClose()}>
                            <CloseIcon />
                        </div>
                    </div>

                    <div className="mb-4">
                        <DefaultInputField placeholder="Enter amount" label="Section name" Action="+ Add sub-section" handleClickAction={handleClickAddSection} />
                    </div>

                    <div className={`${subSection === 0 && "hidden"} mb-4`}>
                        <DefaultInputField
                            placeholder="Enter amount"
                            label="Sub-section name"
                            Action="- Remove sub-section"
                            actionTextColor="text-destructive-500"
                            handleClickAction={handleClickRemoveSection}
                            removeSectionStyle="mobile:text-xs"
                        />
                    </div>

                    <div className={`${(subSection === 0 || subSection === 1) && "hidden"} mb-4`}>
                        <DefaultInputField
                            placeholder="Enter amount"
                            label="Sub-section name - 2"
                            Action="- Remove sub-section"
                            actionTextColor="text-destructive-500"
                            handleClickAction={handleClickRemoveSection2}
                            removeSectionStyle="mobile:text-xs"
                        />
                    </div>

                    <div className="mb-12 relative">
                        <DropDown label="Select ordering types" menuItems={orderingType} />
                    </div>

                    <div className="w-full">
                        {props.popupName === "editSection" ? (
                            <div className="flex flex-row border-none px-0 py-0 md:z-[8] md:fixed md:justify-center md:border-none md:py-0 md:pb-1 md:pt-2 md:shadow-dropShadow left-0 right-0 bottom-0 w-full mx-auto bg-white border-t border-neutral-300 md:px-4">
                                <div className="w-1/2 mr-2">
                                    <LargeDestructiveButton name="Delete section" />
                                </div>
                                <div className="w-1/2 ml-2">
                                    <LargePrimaryButton name="Save changes" />
                                </div>
                            </div>
                        ) : (
                            <div className="border-none px-0 py-0 md:z-[8] md:fixed md:justify-center md:border-none md:py-0 md:pb-1 md:pt-2 md:shadow-dropShadow left-0 right-0 bottom-0 flex w-full mx-auto bg-white border-t border-neutral-300 md:px-4">
                                <div className="w-full">
                                    <LargePrimaryButton name="Add section" />
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        </>
    );
}
