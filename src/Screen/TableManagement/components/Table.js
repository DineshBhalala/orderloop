import React, { useEffect, useState } from "react";
import { ReactComponent as CustomersIcon } from "../../../Assets/customers.svg";
import { ReactComponent as EditIcon } from "../../../Assets/edit.svg";
import { ReactComponent as TimerIcon } from "../../../Assets/timer.svg";
import EnterTableCustomerDetailsPopup from "../../POS/TableView/Components/EnterTableCustomerDetailsPopup";

const Table = (props) => {
    const { tableAllocation, tableName, maxCapacity } = props;

    const [bgColor, setBgColor] = useState("bg-white");

    const [borderColor, setBorderColor] = useState("border-neutral-300");

    const [showCapacity, setShowCapacity] = useState(true);

    const [reserveTablePopup, setReserveTablePopup] = useState(false);

    const handleClickReserveTable = () => {
        setReserveTablePopup(!reserveTablePopup);
    };

    useEffect(() => {
        switch (tableAllocation) {
            case "empty":
                break;

            case "seated":
                setBgColor("bg-primary-50");
                setBorderColor("border-primary-500");
                setShowCapacity(false);
                break;

            case "reserved":
                setBgColor("bg-secondary-50");
                setBorderColor("border-secondary-500");
                setShowCapacity(false);
                break;

            case "disabled":
                setBgColor("bg-neutral-200");
                setBorderColor("border-neutral-300");
                break;

            default:
                break;
        }
    }, []);

    return (
        <>
            <div className="px-2.5 pt-4 pb-2 inline-block align-top md:px-1 md:pt-1 md:pb-1 cursor-default" onClick={handleClickReserveTable}>
                <div>
                    <div className="flex items-center justify-center">
                        <span className={`${bgColor} border ${borderColor} min-w-[62px] lg:min-w-[52px] h-[18px] lg:h-4 rounded mr-[11px] md:min-w-[48px]`}></span>
                        <span className={`${bgColor} border ${borderColor} min-w-[62px] h-[18px] lg:h-4 rounded lg:min-w-[52px] md:min-w-[48px]`}></span>
                    </div>
                    <div className="flex items-center justify-center">
                        <span className={`${bgColor} border ${borderColor} min-w-[18px] lg:min-w-[16px] lg:h-[52px] h-[62px] rounded md:h-12`}></span>
                        <div
                            className={`${bgColor} border ${borderColor} min-w-[151px] lg:min-w-[124px] lg:h-[88px] h-[104px] md:min-w-[132px] md:h-[88px] rounded m-1 lg:m-0.5 flex ${props.price ? "paragraphMediumRegular" : "paragraphLargeRegular"
                                } relative`}
                        >
                            <div className={`${props.price ? "mt-2 mb-3 mx-auto" : "m-auto"} ${!tableName && "hidden"}`}>
                                <div className="flex items-center justify-center">
                                    {tableName}
                                    <div className={`${showCapacity ? "hidden" : "flex items-center justify-center ml-1"}`}>
                                        <span> - </span>
                                        <span className="mx-1">
                                            <CustomersIcon width={`20`} height={`20`} />
                                        </span>
                                        {maxCapacity}
                                    </div>
                                </div>
                                {props.price && <div className="paragraphMediumRegular my-2 lg:my-0.5 lg:text-sm text-center">{props.price}</div>}
                                {props.time && (
                                    <div className="flex flex-row items-center">
                                        <span className="w-6 lg:w-4"><TimerIcon className="w-full h-auto" /></span>
                                        <span className="paragraphSmallRegular ml-1 lg:ml-0.5 lg:text-xs">{props.time}</span>
                                    </div>
                                )}
                                <div className={`${!showCapacity ? "hidden" : "flex"} items-center justify-center paragraphSmallItalic text-neutral-500`}>Max. Cap. - {maxCapacity}</div>
                            </div>
                            {props.edit && (
                                <div className={`absolute right-2 top-2 cursor-pointer ${!tableName && "hidden"} cursor-pointer`} onClick={props.handleClickEditTable}>
                                    <EditIcon />
                                </div>
                            )}
                            {!tableName && <div className="m-auto text-black paragraphLargeRegular">No tables</div>}
                        </div>
                        <span className={`${bgColor} border ${borderColor} min-w-[18px] lg:min-w-[16px] lg:h-[52px] h-[62px] md:h-12 rounded`}></span>
                    </div>
                    <div className="flex items-center justify-center">
                        <span className={`${bgColor} border ${borderColor} min-w-[62px] lg:min-w-[52px] h-[18px] lg:h-4 md:min-w-[48px] rounded mr-[11px]`}></span>
                        <span className={`${bgColor} border ${borderColor} min-w-[62px] lg:min-w-[52px] h-[18px] lg:h-4 md:min-w-[48px] rounded`}></span>
                    </div>
                </div>
            </div>
            <div className={`${!reserveTablePopup && "hidden"}`}>
                <EnterTableCustomerDetailsPopup tableName={tableName} handleClickClose={handleClickReserveTable} />
            </div>
        </>
    );
};

export default Table;
