import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { DefaultBreadcrumbs } from "../../Components/Breadcrumbs/Breadcrumbs";
import { ReactComponent as TableManagementIcon } from "../../Assets/table-management.svg";
import { ReactComponent as EditIcon } from "../../Assets/edit.svg";
import { LargePrimaryButton } from "../../Components/Buttons/Button";
import { ReactComponent as Add } from "../../Assets/add.svg";
import { ReactComponent as Redirect } from "../../Assets/redirect.svg";
import { MultipleTab } from "../../Components/Tabs/Tabs";
import Table from "./Components/Table";
import SectionPopup from "./Components/SectionPopup";
import GridBlockPlus from "./Components/GridBlockPlus";
import TablePopup from "./Components/TablePopup";
import ColorDescription from "../../Components/ColorDescription/ColorDescription";
import { DropDownTabs } from "../../Components/DropDown/DropDownTabs";

const TableManagement = () => {
    const [windowWidth, setWindowWidth] = useState(window.innerWidth);

    useEffect(() => {
        const handleResize = () => {
            setWindowWidth(window.innerWidth);
        };

        window.addEventListener("resize", handleResize);

        return () => {
            window.removeEventListener("resize", handleResize);
        };
    }, []);

    const tableLocationTab = ["All", "Ground floor", "First floor", "Balcony", "Garden", "Banquet Hall"];

    const [activeTableLocationTab, setActiveTableLocationTab] = useState(0);

    const handleClickTableLocationTab = (index) => {
        setActiveTableLocationTab(index);
        edit && handleClickEditSection();
    };

    const tableState = ["All", "Empty", "Seated", "Reserve"];

    const [activeTableState, setActiveTableState] = useState(0);

    const handleClickTableStateTab = (index) => {
        setActiveTableState(index);
    };

    const [showSectionPopup, setShowSectionPopup] = useState(false);

    const handleClickAddSection = () => {
        setShowSectionPopup(!showSectionPopup);
    };

    const [showAddTablePopup, setShowAddTablePopup] = useState(false);

    const handleClickAddTable = () => {
        setShowAddTablePopup(!showAddTablePopup);
    };

    const [edit, setEdit] = useState(false);

    const handleClickEdit = () => {
        setEdit(!edit);
    };

    const numberOfTables = 7;

    const gridBlock = windowWidth < 767 ? Math.ceil(numberOfTables / 2) * 2 : windowWidth < 1354 ? Math.ceil(numberOfTables / 4) * 4 : Math.ceil(numberOfTables / 6) * 6;

    const [showEditSection, setShowEditSection] = useState(false);

    const handleClickEditSection = () => {
        setShowEditSection(!showEditSection);
    };

    const [showEditTable, setShowEditTable] = useState(false);

    const handleClickEditTable = () => {
        setShowEditTable(!showEditTable);
    };
    const emptyFunction = () => { };

    const navigate = useNavigate();
    const handleClickLiveOrders = () => {
        navigate("/pos-orders");
    };
    const handleClickBillPayments = () => {
        navigate("/bill-payments");
    };
    return (
        <>
            <div className="bg-[#fafafa]">
                <div
                    className={`px-8 pb-6 relative lg:px-4 pt-4 w-full max-w-[1336px] mx-auto bg-white h-full ${(showSectionPopup || showEditTable || showEditSection || showAddTablePopup) && "md:hidden"
                        }`}
                >
                    <div>
                        <div className="mb-4 md:hidden">
                            <DefaultBreadcrumbs icon={<TableManagementIcon className="h-5 w-5" />} mainTab="Table management" />
                        </div>
                        <div className="flex flex-row justify-between pb-4 border-b border-b-neutral-300 mb-4 md:block">
                            <div className="flex flex-row md:mb-4">
                                <div className="md:w-1/2 md:mr-2 mobile:mr-1" onClick={handleClickLiveOrders}>
                                    <LargePrimaryButton isDefault={false} leftIconDefault={<Redirect fill="#ffffff" />} leftIconClick={<Redirect fill="#C4BEED" />} name="Live orders" />
                                </div>
                                <div className="ml-4 lg:ml-1 md:w-1/2 md:ml-2 mobile:ml-1" onClick={handleClickBillPayments}>
                                    <LargePrimaryButton isDefault={false} leftIconDefault={<Redirect fill="#ffffff" />} leftIconClick={<Redirect fill="#C4BEED" />} name="Bill payments" />
                                </div>
                            </div>
                            <div className="flex flex-row">
                                <div onClick={handleClickEdit} className="min-w-[64px] cursor-pointer">
                                    <LargePrimaryButton leftIconDefault={<EditIcon stroke="#ffffff" />} leftIconClick={<EditIcon stroke="#C4BEED" />} hideName="lg:hidden" name="Edit" />
                                </div>
                                <div className="mx-4 lg:mx-1 md:mx-2 md:w-full cursor-pointer" onClick={handleClickAddSection}>
                                    <LargePrimaryButton
                                        isDefault={false}
                                        leftIconDefault={<Add stroke="#ffffff" />}
                                        leftIconClick={<Add stroke="#C4BEED" />}
                                        name={windowWidth < 1023 && windowWidth > 767 ? "Section" : windowWidth < 375 ? "Section" : "Add section"}
                                    />
                                </div>
                                <div onClick={handleClickAddTable} className="md:w-full cursor-pointer">
                                    <LargePrimaryButton
                                        isDefault={false}
                                        leftIconDefault={<Add stroke="#ffffff" />}
                                        leftIconClick={<Add stroke="#C4BEED" />}
                                        name={windowWidth < 1023 && windowWidth > 767 ? "Table" : windowWidth < 375 ? "Table" : "Add table"}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="flex justify-between mb-6 pageContentSmall:hidden">
                            <div className="flex justify-between">
                                <div className="flex flex-row">
                                    {tableLocationTab.map((el, index) => (
                                        <div key={index} className="mr-2 3xl:mr-4 pageContent:mr-2">
                                            <MultipleTab
                                                icon={<EditIcon stroke={`${activeTableLocationTab === index ? "#6C5DD3" : "#131126"}`} />}
                                                showIcon={edit}
                                                label={el}
                                                isActive={activeTableLocationTab === index}
                                                index={index}
                                                onClick={handleClickTableLocationTab}
                                                maxWidth="max-w-none"
                                            />
                                        </div>
                                    ))}
                                </div>
                            </div>

                            <div className="flex flex-row">
                                {tableState.map((el, index) => (
                                    <div key={index} className="ml-2 3xl:ml-4 first:ml-0 pageContent:ml-2">
                                        <MultipleTab label={el} isActive={activeTableState === index} index={index} onClick={handleClickTableStateTab} />
                                    </div>
                                ))}
                            </div>
                        </div>
                        <div className="hidden pageContentSmall:flex justify-between flex-row w-full mb-6 md:mb-4">
                            <div className="w-full max-w-[185px] md:w-1/2 md:mr-2 md:max-w-full">
                                <DropDownTabs
                                    menuItems={[
                                        { item: "All", icon: edit && <EditIcon />, onClick: handleClickTableLocationTab },
                                        { item: "Ground floor", onClick: handleClickTableLocationTab, icon: edit && <EditIcon /> },
                                        { item: "First floor", onClick: handleClickTableLocationTab, icon: edit && <EditIcon /> },
                                        { item: "Balcony", onClick: handleClickTableLocationTab, icon: edit && <EditIcon /> },
                                        { item: "Garden", onClick: handleClickTableLocationTab, icon: edit && <EditIcon /> },
                                    ]}
                                    itemStyle="paragraphSmallRegular"
                                    tabStyle="paragraphSmallRegular"
                                    boxStyle="border-neutral-300"
                                    textColor="text-neutral-900"
                                    menuButtonStyle="border-neutral-300"
                                    dropDownIconFill="#131126"
                                    shadow="shadow-shadowXsmall"
                                />
                            </div>
                            <div className="w-full max-w-[185px] md:w-1/2 md:ml-2 md:max-w-full">
                                <DropDownTabs
                                    menuItems={[{ item: "All" }, { item: "Empty" }, { item: "Seated" }, { item: "Reserved" }]}
                                    menuButtonStyle="border-neutral-300"
                                    itemStyle="paragraphSmallRegular"
                                    tabStyle="paragraphSmallRegular"
                                    textColor="text-neutral-900"
                                    boxStyle="border-neutral-300"
                                    dropDownIconFill="#131126"
                                    shadow="shadow-shadowXsmall"
                                />
                            </div>
                        </div>
                        <div className="flex flex-col md:mb-1">
                            <span className="paragraphLargeMedium pb-4 md:pb-2">Ground Floor</span>
                            <span className="paragraphLargeMedium">AC</span>
                        </div>

                        <div className="relative mb-2">
                            <div className="grid grid-cols-6 pageContentSmall:grid-cols-4 relative z-[7] md:grid-cols-2 md:-mx-2 mobile:inline-block mobile:text-center">
                                {[...Array(numberOfTables)].map((_, index) => (
                                    <Table
                                        key={index}
                                        edit={edit}
                                        handleClickEditTable={handleClickEditTable}
                                        tableAllocation={index < 2 && "seated"}
                                        tableName={"T" + (index + 1)}
                                        maxCapacity={index + 3}
                                    />
                                ))}
                            </div>

                            <div className={`grid grid-cols-6 pageContentSmall:grid-cols-4 absolute top-0 w-full h-full z-[5] md:grid-cols-2 mobile:hidden ${!edit && "hidden"}`}>
                                {[...Array(gridBlock)].map((_, index) => (
                                    <GridBlockPlus key={index} edit={edit} />
                                ))}
                            </div>
                            <div className="absolute left-0 top-0 h-full w-6 z-[6] -ml-2.5 bg-white" />
                        </div>

                        {/* <div className="mb-2 lg:mb-0">
                            <span className="paragraphLargeMedium">Non-AC</span>
                        </div>

                        <div className="relative mb-4">
                            <div className="grid grid-cols-6 pageContentSmall:grid-cols-4 relative z-[7]">
                                {[...Array(4)].map((_, index) => (
                                    <Table
                                        key={index}
                                        edit={edit}
                                        handleClickEditTable={handleClickEditTable}
                                        tableAllocation={index === 3 && "disabled"}
                                        tableName={"T" + (index + 1)}
                                        maxCapacity={index + 3}
                                    />
                                ))}
                            </div>

                            <div className={`grid grid-cols-6 pageContentSmall:grid-cols-4 absolute top-0 w-full h-full z-[5] ${!edit && "hidden"}`}>
                                {[...Array(6)].map((_, index) => (
                                    <GridBlockPlus key={index} edit={edit} />
                                ))}
                            </div>
                            <div className="absolute left-0 top-0 h-full w-6 z-[6] -ml-2.5 bg-white" />
                        </div>

                        <div className="mb-2 lg:mb-0">
                            <span className="paragraphLargeMedium">First floor</span>
                        </div>

                        <div className="relative mb-4">
                            <div className="grid grid-cols-6 pageContentSmall:grid-cols-4 relative z-[7]">
                                {[...Array(4)].map((_, index) => (
                                    <Table key={index} edit={edit} handleClickEditTable={handleClickEditTable} tableAllocation="empty" tableName={"T" + (index + 1)} maxCapacity={index + 3} />
                                ))}
                            </div>

                            <div className={`grid grid-cols-6 pageContentSmall:grid-cols-4 absolute top-0 w-full h-full z-[5] ${!edit && "hidden"}`}>
                                {[...Array(6)].map((_, index) => (
                                    <GridBlockPlus key={index} edit={edit} />
                                ))}
                            </div>
                            <div className="absolute left-0 top-0 h-full w-6 z-[6] -ml-2.5 bg-white" />
                        </div> */}
                    </div>

                    <div className="sticky bottom-6 z-40 md:hidden">
                        <div className="border border-neutral-300 shadow-shadowMedium px-4 py-3 rounded-lg flex-row flex bg-white w-max">
                            <ColorDescription border="border-neutral-300" label={windowWidth < 1023 ? "Empty | 25" : "Empty seats | 25"} marginR="mr-10" />
                            <ColorDescription border="border-primary-300" bgColor="bg-primary-50" label={windowWidth < 1023 ? "Empty | 2" : "Empty seats | 2"} marginR="mr-10" />
                            <ColorDescription border="border-secondary-500" bgColor="bg-secondary-50" label={windowWidth < 1023 ? "Reserved | 1" : "Reserved seats | 1"} marginR="mr-10" />
                            <ColorDescription border="border-neutral-300" bgColor="bg-neutral-200" label={windowWidth < 1023 ? "Disabled | 2" : "Disabled seats | 2"} />
                        </div>
                    </div>
                </div>
            </div>

            <div className={`${!showSectionPopup && "hidden"}`}>
                <SectionPopup handleClickClose={handleClickAddSection} />
            </div>

            <div className={`${!showEditSection && "hidden"}`}>
                <SectionPopup handleClickClose={handleClickEditSection} popupName="editSection" />
            </div>

            <div className={`${!showAddTablePopup && "hidden"}`}>
                <TablePopup handleClickClose={handleClickAddTable} />
            </div>

            <div className={`${!showEditTable && "hidden"}`}>
                <TablePopup handleClickClose={handleClickEditTable} popupName="editTable" />
            </div>
        </>
    );
};

export default TableManagement;
