import React from "react";
import { ReactComponent as Edit } from "../../..//Assets/edit.svg";
import { ReactComponent as Trash } from "../../..//Assets/trash.svg";

function EditDelete() {
  return (
    <>
      <div className="flex-row flex">
        <div className="h-1 w-1 rounded-full bg-neutral-900" />
        <div className="h-1 w-1 rounded-full bg-neutral-900 mx-0.5" />
        <div className="h-1 w-1 rounded-full bg-neutral-900" />
      </div>
      <div className="w-[137px] h-[76px] rounded-md border border-neutral-300 shadow-shadowXsmall absolute right-0 mt-2 px-4 py-2 bg-white">
        <div className="flex flex-row items-center pb-3">
          <Edit className="w-5 h-5 mr-1"/>
          <span className="paragraphMediumRegular">Rename</span>
        </div>
        <div className="flex flex-row items-center">
          <Trash className="w-5 h-5 mr-1" stroke="#EF4444" />
          <span className="paragraphMediumRegular text-destructive-500">
            Delete
          </span>
        </div>
      </div>
    </>
  );
}

export default EditDelete;
