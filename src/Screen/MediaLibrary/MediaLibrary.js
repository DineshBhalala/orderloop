import React, { useState } from "react";
import { LargeDestructiveButton, LargePrimaryButton } from "../../Components/Buttons/Button";
import DropDown from "../../Components/DropDown/DropDown";
import { DefaultInputField } from "../../Components/InputField/InputField";
import { Tab } from "../../Components/Tabs/Tabs";
import { ReactComponent as MediaLibraryIcon } from "../..//Assets/media-library.svg";
import { ReactComponent as Search } from "../..//Assets/search.svg";
import { ReactComponent as Add } from "../..//Assets/add.svg";
import { ReactComponent as Select } from "../..//Assets/select.svg";
import { ReactComponent as GridViewIcon } from "../..//Assets/grid-view.svg";
import { ReactComponent as View } from "../..//Assets/view.svg";
import { ReactComponent as Close } from "../..//Assets/close.svg";
import { ReactComponent as SelectIcon } from "../..//Assets/select.svg";
import PaginationNumber from "../../Components/Pagination/PaginationWithNumber";
import MediaLibraryCard from "../../Components/MediaLibraryCard/MediaLibraryCard";
import pizza1 from "../../Assets/mediaLibrary/pizza1.png";
import pizza2 from "../../Assets/mediaLibrary/pizza2.png";
import pizza3 from "../../Assets/mediaLibrary/pizza3.png";
import pizza4 from "../../Assets/mediaLibrary/pizza4.png";
import pizza5 from "../../Assets/mediaLibrary/pizza5.png";
import pizza6 from "../../Assets/mediaLibrary/pizza6.png";
import pizza7 from "../../Assets/mediaLibrary/pizza7.png";
import pizza8 from "../../Assets/mediaLibrary/pizza8.png";
import pizza9 from "../../Assets/mediaLibrary/pizza9.png";
import pizza10 from "../../Assets/mediaLibrary/pizza10.png";
import cake from "../../Assets/mediaLibrary/cake.png";
import bevarages from "../..//Assets/mediaLibrary/bevarages.png";
import bevarages2 from "../..//Assets/mediaLibrary/bevarages2.png";
import butter from "../..//Assets/mediaLibrary/butter.png";
import fizz from "../..//Assets/mediaLibrary/fizz.png";
import icecreame from "../..//Assets/mediaLibrary/icecreame.png";
import paneer from "../..//Assets/mediaLibrary/paneer.png";
import slice from "../..//Assets/mediaLibrary/slice.png";
import UploadImage from "../../Components/UploadImage/UploadImage";
import { DefaultBreadcrumbs } from "../../Components/Breadcrumbs/Breadcrumbs";
import { DropDownTabs } from "../../Components/DropDown/DropDownTabs";
import ListViewMediaCard from "../../Components/ListView/ListViewMediaCard";

export default function MediaLibrary() {
    const menuItems = ["All", "Categories", "Dishes", "Banners", "Variants", "Notification", "Intro Logo", "Footer", "Riders"];

    const foodItem = [
        {
            imageTitle: "Creamy Tomato Pasta Pizza - Veg",
            category: "Dishes",
            subCategory: "Veg-pizza",
            uploader: "Sarthak Kanchan",
            uploadedOn: "11 November, 2022",
            icon: pizza1,
        },
        {
            imageTitle: "Moroccan Spice Pasta Pizza - Veg",
            category: "Dishes",
            subCategory: "Veg-pizza",
            uploader: "Sarthak Kanchan",
            uploadedOn: "11 November, 2022",
            icon: pizza2,
        },
        {
            imageTitle: "Corn n Cheese Paratha Pizza",
            category: "Dishes",
            subCategory: "Paratha",
            uploader: "Sarthak Kanchan",
            uploadedOn: "11 November, 2022",
            icon: pizza3,
        },
        {
            imageTitle: "Margherita",
            category: "Dishes",
            subCategory: "Veg-pizza",
            uploader: "Sarthak Kanchan",
            uploadedOn: "11 November, 2022",
            icon: pizza4,
        },
        {
            imageTitle: "Peppy Paneer",
            category: "Dishes",
            subCategory: "Veg-pizza",
            uploader: "Sarthak Kanchan",
            uploadedOn: "11 November, 2022",
            icon: pizza5,
        },
        {
            imageTitle: "Farmhouse",
            category: "Dishes",
            subCategory: "Veg-pizza",
            uploader: "Sarthak Kanchan",
            uploadedOn: "11 November, 2022",
            icon: pizza6,
        },
        {
            imageTitle: "The 4 Cheese Pizza",
            category: "Dishes",
            subCategory: "Veg-pizza",
            uploader: "Sarthak Kanchan",
            uploadedOn: "11 November, 2022",
            icon: pizza7,
        },
        {
            imageTitle: "Paneer Tikka Stuffed Garlic Bread",
            category: "Dishes",
            subCategory: "Veg-pizza",
            uploader: "Sarthak Kanchan",
            uploadedOn: "11 November, 2022",
            icon: pizza8,
        },
        {
            imageTitle: "Pepsi (500ml)",
            category: "Dishes",
            subCategory: "Veg-pizza",
            uploader: "Sarthak Kanchan",
            uploadedOn: "11 November, 2022",
            icon: bevarages,
        },
        {
            imageTitle: "Indi Tandoori Paneer",
            category: "Dishes",
            subCategory: "Veg-pizza",
            uploader: "Sarthak Kanchan",
            uploadedOn: "11 November, 2022",
            icon: pizza9,
        },
        {
            imageTitle: "7Up (500ml)",
            category: "Dishes",
            subCategory: "Veg-pizza",
            uploader: "Sarthak Kanchan",
            uploadedOn: "11 November, 2022",
            icon: bevarages2,
        },
        {
            imageTitle: "Red Velvet Lava Cake",
            category: "Dishes",
            subCategory: "Veg-pizza",
            uploader: "Sarthak Kanchan",
            uploadedOn: "11 November, 2022",
            icon: cake,
        },
        {
            imageTitle: "",
            category: "Dishes",
            subCategory: "Veg-pizza",
            uploader: "Sarthak Kanchan",
            uploadedOn: "11 November, 2022",
            icon: fizz,
        },
        {
            imageTitle: "",
            category: "Dishes",
            subCategory: "Veg-pizza",
            uploader: "Sarthak Kanchan",
            uploadedOn: "11 November, 2022",
            icon: pizza10,
        },
        {
            imageTitle: "",
            category: "Dishes",
            subCategory: "Veg-pizza",
            uploader: "Sarthak Kanchan",
            uploadedOn: "11 November, 2022",
            icon: slice,
        },
        {
            imageTitle: "",
            category: "Dishes",
            subCategory: "Veg-pizza",
            uploader: "Sarthak Kanchan",
            uploadedOn: "11 November, 2022",
            icon: paneer,
        },
        {
            imageTitle: "",
            category: "Dishes",
            subCategory: "Veg-pizza",
            uploader: "Sarthak Kanchan",
            uploadedOn: "11 November, 2022",
            icon: butter,
        },
        {
            imageTitle: "",
            category: "Dishes",
            subCategory: "Veg-pizza",
            uploader: "Sarthak Kanchan",
            uploadedOn: "11 November, 2022",
            icon: icecreame,
        },
    ];

    const [showList, setShowList] = useState(true);
    const [showGrid, setShowGrid] = useState(false);

    const handleClickGridTab = () => {
        setShowList(false);
        setShowGrid(true);
    };

    const handleClickListTab = () => {
        setShowList(true);
        setShowGrid(false);
    };

    const [showUploadImagePage, setShowUploadImagePage] = useState(false);

    const showHideUploadImagePage = () => {
        setShowUploadImagePage(!showUploadImagePage);
    };

    const [isShowBulkSelectButton, setShowBulkSelectButton] = useState(false);
    const handleClickBulkButton = () => {
        setShowBulkSelectButton(!isShowBulkSelectButton);
    };

    return (
        <>
            <div className="bg-[#fafafa]">
                <div className={`px-8 pb-[70px] lg:px-4 lg:pb-[23px] pt-4 w-full max-w-[1336px] mx-auto bg-white ${showUploadImagePage && "hidden"}`}>
                    <div className="mb-4 md:hidden">
                        <DefaultBreadcrumbs icon={<MediaLibraryIcon className="h-5 w-5" />} mainTab="Media library" />
                    </div>

                    <div className="flex flex-row justify-between pb-4 border-b mb-4 border-neutral-30 md:border-b-0 md:pb-0">
                        <div className="flex flex-row w-full">
                            <div className="max-w-[375px] w-full mr-2 lg:max-w-[206px] lg:mr-1 md:w-full md:max-w-full md:mr-0">
                                <DefaultInputField placeholder="Search media" placeholderIcon={<Search stroke="#D3D2D8" />} />
                            </div>
                            <div className="max-w-[213px] w-full mx-2 relative xl:w-[185px] lg:mx-1 md:hidden">
                                <DropDown menuItems={menuItems} textColor="text-neutral-500" dropdownLabel="All" />
                            </div>
                            <div className="max-w-[213px] w-full ml-2 relative xl:w-[185px] md:hidden">
                                <DropDown menuItems={menuItems} textColor="text-neutral-500" dropdownLabel="All" />
                            </div>
                        </div>
                        <div onClick={showHideUploadImagePage} className="ml-4 max-w-[159px] w-full lg:ml-2 lg:max-w-[64px] cursor-pointer">
                            <LargePrimaryButton isDefault={false} name="Add image" hideName="lg:hidden" leftIconDefault={<Add />} leftIconClick={<Add stroke="#C4BEED" />} />
                        </div>
                    </div>
                    <div className="hidden md:flex flex-row mb-4">
                        <div className="w-1/2 pr-2 relative">
                            <DropDown menuItems={menuItems} textColor="text-neutral-500" dropdownLabel="All" />
                        </div>
                        <div className="w-1/2 pl-2 relative">
                            <DropDown menuItems={menuItems} textColor="text-neutral-500" dropdownLabel="All" />
                        </div>
                    </div>

                    <div className="flex flex-row justify-between mb-3">
                        <div className={`max-w-[156px] w-full ${isShowBulkSelectButton && "hidden"} md:max-w-full md:w-1/2 md:pr-2 cursor-pointer`} onClick={handleClickBulkButton}>
                            <LargePrimaryButton isDefault={false} name="Bulk select" leftIconDefault={<Select stroke="#FFFFFF" />} leftIconClick={<Select stroke="#C4BEED" />} />
                        </div>
                        <div className={`${!isShowBulkSelectButton && "hidden"} flex flex-row items-center md:w-1/2 justify-between md:pr-2`}>
                            <div className="max-w-[125px] mr-4 mobile:mr-2 md:w-1/2 md:max-w-full cursor-pointer" onClick={handleClickBulkButton}>
                                <LargePrimaryButton isDefault={false} name="Cancel" hideName="md:hidden" leftIconDefault={<Close stroke="#FFFFFF" />} leftIconClick={<Close stroke="#C4BEED" />} />
                            </div>
                            <div className="max-w-[192px] md:w-1/2 md:max-w-full">
                                <LargeDestructiveButton name="Delete selected" leftIconDefault={<SelectIcon stroke="#FFFFFF" />} leftIconClick={<SelectIcon stroke="#D7EDEB" />} hideName="md:hidden" />
                            </div>
                            <span className="paragraphLargeItalic text-neutral-500 mx-4 md:hidden">(Selected images - 1)</span>
                        </div>
                        <div className="md:hidden">
                            <div className="flex flex-row">
                                <div className="mr-4 max-w-[136px] cursor-pointer" onClick={handleClickGridTab}>
                                    <Tab label="Grid view" hideName="lg:hidden" IconDefault={<GridViewIcon />} IconClick={<GridViewIcon stroke="#6C5DD3" />} isActive={showGrid} />
                                </div>
                                <div className="max-w-[131px] cursor-pointer" onClick={handleClickListTab}>
                                    <Tab label="List view" hideName="lg:hidden" IconDefault={<View />} IconClick={<View stroke="#6C5DD3" />} isActive={showList} />
                                </div>
                            </div>
                        </div>
                        <div className="hidden md:block w-1/2 pl-2">
                            <DropDownTabs
                                menuItems={[
                                    { item: "List view", onClick: handleClickListTab, icon: <View />, iconClick: <View stroke="#6C5DD3" /> },
                                    { item: "Grid view", onClick: handleClickGridTab, icon: <GridViewIcon />, iconClick: <GridViewIcon stroke="#6C5DD3" /> },
                                ]}
                            />
                        </div>
                    </div>

                    <div className={`w-full border rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden ${!showList && "hidden"} mt-3 md:hidden`}>
                        <table className="w-full break-words tableMediaLibrary">
                            <thead>
                                <tr className="bg-neutral-50 text-left paragraphOverlineSmall text-neutral-700">
                                    <th className="shadow-innerShadow bg-neutral-50 py-[11.5px] px-6 min-w-[326px] sticky left-0 z-10">IMAGE TITLE</th>
                                    <th className="shadow-innerShadow py-[11.5px] px-6 min-w-[132px]">CATEGORY</th>
                                    <th className="shadow-innerShadow py-[11.5px] px-6 min-w-[156px]">SUB CATEGORY</th>
                                    <th className="shadow-innerShadow py-[11.5px] px-6 min-w-[183px]">UPLOADER</th>
                                    <th className="shadow-innerShadow py-[11.5px] px-6 min-w-[160px]">UPLOADED ON</th>
                                </tr>
                            </thead>
                            <tbody>
                                {foodItem.slice(1, 9).map((row, index) => (
                                    <tr key={index} className={`${index !== 0 && "border-t"} paragraphSmallRegular`}>
                                        <td className="pl-6 min-w-[326px] sticky left-0 z-10">
                                            <div className="flex flex-row items-center">
                                                <img src={row.icon} alt="Media library" className="rounded mr-3 h-12 w-12" />
                                                <span>{row.imageTitle}</span>
                                            </div>
                                        </td>
                                        <td className="py-[24.5px] pl-6">{row.category}</td>
                                        <td className="py-[24.5px] pl-6">{row.subCategory}</td>
                                        <td className="py-[24.5px] pl-6 text-primary-500">{row.uploader}</td>
                                        <td className="py-[24.5px] pl-6">11 November, 2022</td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>

                    <div className={`hidden ${!showGrid && "md:block"}`}>
                        {foodItem.map((row, index) => {
                            return (
                                <div key={index} className="py-1">
                                    <ListViewMediaCard
                                        image={row.icon}
                                        title={row.imageTitle}
                                        category={row.category}
                                        subCategory={row.subCategory}
                                        uploader={row.uploader}
                                        upoadedOn={row.uploadedOn}
                                    />
                                </div>
                            );
                        })}
                    </div>

                    <div className={`-mx-2.5 ${!showGrid && "hidden"} lg:-mx-1 md:-mx-[3.5px]`}>
                        {foodItem.map((el, index) => {
                            return (
                                <div
                                    key={index}
                                    className="inline-block h-[280px] w-[196px] mx-[9.5px] align-top my-3 lg:w-[160px] lg:mx-1 lg:h-[268px] md:w-[168px] md:h-[276px] md:mx-[3.5px] md:my-1 mobile:w-full mobile:h-full"
                                >
                                    <MediaLibraryCard title={el.imageTitle} category={`#${el.category}, #${el.subCategory}`} icon={el.icon} />
                                </div>
                            );
                        })}
                    </div>

                    <div className="mt-4 lg:hidden">
                        <PaginationNumber />
                    </div>
                </div>
                <div className={`${!showUploadImagePage && "hidden"}`}>
                    <UploadImage showHideUploadImagePage={showHideUploadImagePage} backToWhichPageName="library" />
                </div>
            </div>
        </>
    );
}
