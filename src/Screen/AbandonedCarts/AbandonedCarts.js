import React, { useState } from "react";
import SliderAbandonedCarts from "react-slick";
import { ReactComponent as AbandonedCart } from "../../Assets/abandoned-cart.svg";
import { DefaultBreadcrumbs } from "../../Components/Breadcrumbs/Breadcrumbs";
import { ReactComponent as Filter } from "../../Assets/filter.svg";
import { LargePrimaryButton } from "../../Components/Buttons/Button";
import { ReactComponent as SelectIcon } from "../../Assets/select.svg";
import { ReactComponent as ExportIcon } from "../../Assets/export.svg";
import { ReactComponent as Riders } from "../../Assets/riders.svg";
import { ReactComponent as User } from "../../Assets/user.svg";
import { ReactComponent as Order } from "../../Assets/order.svg";
import { ReactComponent as Credit } from "../../Assets/credits.svg";
import AbandonedOrderType from "./Components/AbandonedOrderType";
import CustomerType from "./Components/CustomerType";
import PaginationWithNumber from "../../Components/Pagination/PaginationWithNumber";
import CustomerCartDetailsPopup from "./Components/CustomerCartDetailsPopup";
import CustomerCartDetailsDropDown from "./Components/CustomerCartDetailsDropDown";
import CalenderField from "../../Components/Calender/CalenderField";
import Card from "./Components/Card";

const AbandonedCarts = () => {
    const card = [
        {
            title: "Customer type",
            header: "New",
            description: "Maximum carts abandoned by 7 New customers  this week",
            icon: <User height={24} width={24} stroke="#ffffff" />,
            highLightedPart: "7 New",
        },
        {
            title: "Dish",
            header: "Sprite",
            description: "Maximum dish found in abandoned carts were 24 sprites this week",
            icon: <Order height={24} width={24} stroke="#ffffff" />,
            highLightedPart: "24 sprites",
        },
        {
            title: "Avg. price",
            header: "₹250.00",
            description: "Avg. price of abandoned carts recorded this week was ₹250.00",
            icon: <Credit height={24} width={24} stroke="#ffffff" />,
            highLightedPart: "₹250.00",
        },
    ];

    const order = [
        {
            customerName: "Rashi Agrawal",
            customerType: <CustomerType customerLabel="New" />,
            orderDate: "18 Nov 2022",
            orderAmount: "₹559.00/-",
            orderType: <AbandonedOrderType icon={<Riders />} statusLabel="Delivery" />,
        },
        {
            customerName: "Amrendra Bahubali",
            customerType: <CustomerType customerLabel="Old" />,
            orderDate: "13 Nov 2022",
            orderAmount: "₹759.00/-",
            orderType: <AbandonedOrderType icon={<Riders />} statusLabel="Delivery" />,
        },
        {
            customerName: "Jay Sharma",
            customerType: <CustomerType customerLabel="New" />,
            orderDate: "05 Nov 2022",
            orderAmount: "₹1,789.00/-",
            orderType: <AbandonedOrderType icon={<Riders />} statusLabel="Delivery" />,
        },
        {
            customerName: "Rashi Agrawal",
            customerType: <CustomerType customerLabel="New" />,
            orderDate: "04 Nov 2022",
            orderAmount: "₹123.00/-",
            orderType: <AbandonedOrderType icon={<Riders />} statusLabel="Delivery" />,
        },
        {
            customerName: "Shraddha Kapoor",
            customerType: <CustomerType customerLabel="Old" />,
            orderDate: "04 Nov 2022",
            orderAmount: "₹5,238.00/-",
            orderType: <AbandonedOrderType icon={<Riders />} statusLabel="Delivery" />,
        },
        {
            customerName: "Shreya Khanna",
            customerType: <CustomerType customerLabel="New" />,
            orderDate: "01 Nov 2022",
            orderAmount: "₹3,559.00/-",
            orderType: <AbandonedOrderType icon={<Riders />} statusLabel="Delivery" />,
        },
        {
            customerName: "Justin Beiber",
            customerType: <CustomerType customerLabel="New" />,
            orderDate: "30 Oct 2022",
            orderAmount: "₹1,559.00/-",
            orderType: <AbandonedOrderType icon={<Riders />} statusLabel="Delivery" />,
        },
    ];

    const [showCustomerCartDetail, setShowCustomerCartDetail] = useState(false);

    const handleClickCustomerCartDetails = () => {
        setShowCustomerCartDetail(!showCustomerCartDetail);
    };

    var settingsAbandonedCarts = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        className: "abandonedCartSlide",
    };

    return (
        <>
            <div className="bg-[#fafafa]">
                <div className={`pb-10 lg:pb-[23px] pt-4 w-full max-w-[1336px] mx-auto bg-white ${showCustomerCartDetail && "md:hidden"}`}>
                    <div className="px-8 lg:px-4">
                        <div className="mb-4 md:hidden">
                            <DefaultBreadcrumbs icon={<AbandonedCart className="h-5 w-5" />} mainTab="Abandoned carts" />
                        </div>
                        <div className="flex flex-row justify-between border-b border-neutral-300 pb-4 mb-2 md:mb-4 md:block">
                            <div className="flex flex-row md:justify-between">
                                <CalenderField label="25 Sept 2022 - 09 Oct 2022" buttonStyle="mobile:pr-0 mobile:pl-1 md:w-full" labelStyle="mobile:text-sm mobile:mx-0" />
                                <div className="min-w-[156px] w-full md:max-w-full md:w-[56px] ml-4 lg:ml-2">
                                    <LargePrimaryButton name="Bulk select" leftIconDefault={<SelectIcon stroke="#ffffff" />} leftIconClick={<SelectIcon stroke="#C4BEED" />} hideName="md:hidden" />
                                </div>
                            </div>
                            <div className="flex flex-row md:justify-between md:mt-4">
                                <div className="mx-4 lg:mx-2 md:w-1/2 md:mr-[8.5px] md:ml-0 mobile:mr-1 min-w-[120px] w-full">
                                    <LargePrimaryButton name="Filters" leftIconDefault={<Filter fill="#FFFFFF" />} leftIconClick={<Filter fill="#C4BEED" />} />
                                </div>
                                <div className="md:w-1/2 md:ml-[8.5px] mobile:ml-1">
                                    <LargePrimaryButton
                                        name="Export data"
                                        leftIconDefault={<ExportIcon stroke="#FFFFFF" />}
                                        leftIconClick={<ExportIcon stroke="#C4BEED" />}
                                        hideName="lg:hidden md:block"
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="md:hidden">
                            <div className="-mx-2.5 lg:mx-0">
                                {card.map((el, index) => {
                                    return (
                                        <div key={index} className="inline-block align-top px-2.5 mt-2 w-1/3 lg:w-1/2 creditBox lg:px-0 lg:odd:pr-1 lg:even:pl-1 mg:odd:pr-0 md:even:pl-0 md:w-full">
                                            <Card {...el} />
                                        </div>
                                    );
                                })}
                            </div>
                        </div>
                    </div>
                    {/* Slider AbandonedCarts start */}
                    <div className="hidden md:block">
                        <div>
                            <SliderAbandonedCarts {...settingsAbandonedCarts}>
                                {card.map((el, mapIndex) => {
                                    return (
                                        <div key={mapIndex}>
                                            <div className="px-4">
                                                <Card {...el} />
                                            </div>
                                        </div>
                                    );
                                })}
                            </SliderAbandonedCarts>
                        </div>
                    </div>
                    {/* Slider AbandonedCarts end */}
                    <div className="px-8 lg:px-4">
                        <div className="my-4">
                            <div className="w-full border border-neutral-300 rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden">
                                <table className="w-full break-words">
                                    <thead>
                                        <tr className="bg-neutral-50 uppercase paragraphOverlineSmall text-neutral-700 h-[44px] justify-center">
                                            <th className="text-left pl-6 min-w-[261px] lg:min-w-[202px] shadow-innerShadow">customer name</th>
                                            <th className="text-left pl-6 min-w-[253px] lg:min-w-[165px] shadow-innerShadow">customer type</th>
                                            <th className="text-left pl-6 min-w-[234px] lg:min-w-[147px] shadow-innerShadow">order date</th>
                                            <th className="text-left pl-6 min-w-[262px] lg:min-w-[165px] shadow-innerShadow">order header</th>
                                            <th className="text-left pl-6 min-w-[260px] lg:min-w-[165px] shadow-innerShadow">Order type</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {order.map((row, index) => (
                                            <tr key={index} className={`even:bg-neutral-50 ${index !== 0 && "border-t"} border-neutral-300 paragraphSmallRegular justify-center h-[70px]`}>
                                                <td className="pl-6 cursor-pointer" onClick={handleClickCustomerCartDetails}>
                                                    {row.customerName}
                                                </td>
                                                <td className="pl-6">{row.customerType}</td>
                                                <td className="pl-6">{row.orderDate}</td>
                                                <td className="pl-6">{row.orderAmount}</td>
                                                <td className="pl-6">{row.orderType} </td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                            <div className="hidden md:block mt-3">
                                {order.map((el, index) => {
                                    return (
                                        <div className="mb-1 pt-1" key={index}>
                                            <CustomerCartDetailsDropDown
                                                customerName={el.customerName}
                                                customerType={el.customerType}
                                                orderDate={el.orderDate}
                                                orderAmount={el.orderAmount}
                                                orderType={el.orderType}
                                                handleClickCustomerCartDetails={handleClickCustomerCartDetails}
                                            />
                                        </div>
                                    );
                                })}
                            </div>
                        </div>
                        <div className="md:hidden">
                            <PaginationWithNumber />
                        </div>
                    </div>
                </div>
                <div className={`${!showCustomerCartDetail && "hidden"}`}>
                    <CustomerCartDetailsPopup handleClickCustomerCartDetails={handleClickCustomerCartDetails} />
                </div>
            </div>
        </>
    );
};

export default AbandonedCarts;
