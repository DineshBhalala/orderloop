import React from "react";
import { ReactComponent as Close } from "../../../Assets/close.svg";
import { ReactComponent as SuccessTickIcon } from "../../../Assets/success-tick.svg";
import { ReactComponent as Riders } from "../../../Assets/riders.svg";
import { ReactComponent as LeftArrow } from "../../../Assets/chevron-down.svg";
import { ReactComponent as VegetableIcon } from "../../../Assets/vegetable-icon.svg";

export default function CustomerCartDetailsPopup(props) {
    return (
        <>
            <div className="fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 flex justify-center items-center overflow-auto md:bg-white md:relative px-4">
                <div className="max-w-[652px] w-full rounded-xl md:rounded-none bg-shades-50 px-8 py-6 md:px-0 md:py-0 m-auto md:max-w-full">
                    <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer mt-4" onClick={() => props.handleClickCustomerCartDetails()}>
                        <LeftArrow className="rotate-90" />
                        <span className="ml-1">Back to abandoned carts</span>
                    </div>

                    <div className="flex flex-row justify-between items-center mb-2 pb-4 border-b border-neutral-300 md:items-start">
                        <div>
                            <div className="paragraphLargeMedium md:paragraphMediumMedium md:mb-1">Customer cart details</div>
                            <span className="paragraphMediumItalic text-neutral-500">Cart #BBQR</span>
                        </div>
                        <div className="md:hidden cursor-pointer" onClick={() => props.handleClickCustomerCartDetails()}>
                            <Close />
                        </div>
                        <div className="flex-row items-center hidden md:flex">
                            <SuccessTickIcon />
                            <span className="paragraphSmallRegular ml-1.5">Delivered</span>
                        </div>
                    </div>

                    <div className="flex flex-row justify-between pb-4 border-b border-neutral-300 mb-4 md:block">
                        <div>
                            <div className="mb-1.5 pt-1.5 flex items-center">
                                <span className="paragraphMediumMedium text-neutral-500">Order bill name:</span>
                                <span className="paragraphMediumRegular ml-2 flex items-center">
                                    Rashi agrawal
                                    <span className="text-primary-500 text-[10px] border font-medium px-2 py-1 border-primary-500 rounded bg-primary-50 h-5 ml-1 leading-none">New</span>
                                </span>
                            </div>

                            <div className="mb-1.5 pt-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Order date:</span>
                                <span className="paragraphMediumRegular ml-2">18 Nov 2022</span>
                            </div>

                            <div className="mb-1.5 pt-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Cart active time:</span>
                                <span className="paragraphMediumRegular ml-2">05 mins</span>
                            </div>

                            <div className="pt-1.5 mb-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Ordered via:</span>
                                <span className="paragraphMediumRegular ml-2">iOS device</span>
                            </div>

                            <div className="pt-1.5 md:mb-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Delivery area:</span>
                                <span className="paragraphMediumRegular ml-2">Race course</span>
                            </div>
                        </div>
                        <div>
                            <div className="mb-1.5 pt-1.5 flex flex-row items-center">
                                <span className="paragraphMediumMedium text-neutral-500">Order type:</span>
                                <span className="paragraphMediumRegular ml-2 items-center flex flex-row">
                                    <Riders className="mr-1" />
                                    Delivery
                                </span>
                            </div>

                            <div className="mb-1.5 pt-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Order time:</span>
                                <span className="paragraphMediumRegular ml-2">17:59 PM</span>
                            </div>

                            <div className="mb-1.5 pt-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Dishes in cart:</span>
                                <span className="paragraphMediumRegular ml-2">06</span>
                            </div>

                            <div className="pt-1.5 mb-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Platform:</span>
                                <span className="paragraphMediumRegular ml-2">Swiggy</span>
                            </div>

                            <div className="pt-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Delivery distance:</span>
                                <span className="paragraphMediumRegular ml-2">07 km</span>
                            </div>
                        </div>
                    </div>

                    <div className="flex flex-row justify-between mb-4 paragraphMediumSemiBold pr-[14px]">
                        <span>Ordered dishes</span>
                        <span>Amount</span>
                    </div>

                    <div className="border-b border-neutral-300 mb-4 pb-2 -mr-1.5">
                        <div className={`scrollbarStyle overflow-auto h-[137px] pr-3 ${props.pageName === "loyaltyCashback" && "md:hidden"}`}>
                            <div className="flex flex-row justify-between paragraphMediumRegular">
                                <div className="flex flex-row items-center md:items-start w-full">
                                    <span className="md:min-w-[23px]">2 x</span>
                                    <span className="mx-2">Double Cheese Margherita Pizza</span>
                                    <span className="md:pt-1 md:ml-auto">
                                        <VegetableIcon />
                                    </span>
                                </div>
                                <span className="md:min-w-[104px] mobile:min-w-[90px] md:text-right">₹1,118.00/-</span>
                            </div>

                            <div className="flex flex-row justify-between paragraphMediumRegular mt-3">
                                <div className="flex flex-row items-center md:items-start w-full">
                                    <span className="md:min-w-[23px]">1 x</span>
                                    <span className="mx-2 md:max-w-[184.4px]">Indi Tandoori Paneer</span>
                                    <span className="md:pt-1 md:ml-auto">
                                        <VegetableIcon />
                                    </span>
                                </div>
                                <span className="md:min-w-[104px] mobile:min-w-[90px] md:text-right">₹529.00/-</span>
                            </div>

                            <div className="flex flex-row justify-between paragraphMediumRegular mt-3">
                                <div className="flex flex-row items-center md:items-start w-full">
                                    <span className="md:min-w-[23px]">1 x</span>
                                    <span className="mx-2 md:max-w-[184.4px]">Mexican Green Wave</span>
                                    <span className="md:pt-1 md:ml-auto">
                                        <VegetableIcon />
                                    </span>
                                </div>
                                <span className="md:min-w-[104px] mobile:min-w-[90px] md:text-right">₹529.00/-</span>
                            </div>

                            <div className="flex flex-row justify-between paragraphMediumRegular mt-3">
                                <div className="flex flex-row items-center md:items-start w-full">
                                    <span className="md:min-w-[23px]">2 x</span>
                                    <span className="mx-2 md:max-w-[184.4px]">Mexican Green Wave</span>
                                    <span className="md:pt-1 md:ml-auto">
                                        <VegetableIcon />
                                    </span>
                                </div>
                                <span className="md:min-w-[104px] mobile:min-w-[90px] md:text-right">₹3,118.00/-</span>
                            </div>

                            <div className="flex flex-row justify-between paragraphMediumRegular mt-3">
                                <div className="flex flex-row items-center md:items-start w-full">
                                    <span className="md:min-w-[23px]">1 x</span>
                                    <span className="mx-2 md:max-w-[184.4px]">Mexican Green Wave</span>
                                    <span className="md:pt-1 md:ml-auto">
                                        <VegetableIcon />
                                    </span>
                                </div>
                                <span className="md:min-w-[104px] mobile:min-w-[90px] md:text-right">₹529.00/-</span>
                            </div>

                            <div className="flex flex-row justify-between paragraphMediumRegular mt-3">
                                <div className="flex flex-row items-center md:items-start w-full">
                                    <span className="md:min-w-[23px]">2 x</span>
                                    <span className="mx-2 md:max-w-[184.4px]">Mexican Green Wave</span>
                                    <span className="md:pt-1 md:ml-auto">
                                        <VegetableIcon />
                                    </span>
                                </div>
                                <span className="md:min-w-[104px] mobile:min-w-[90px] md:text-right">₹3,118.00/-</span>
                            </div>
                        </div>
                    </div>
                    <div className="paragraphMediumRegular">
                        <div className="flex flex-row justify-between items-center mb-1.5">
                            <span>Gross total</span>
                            <span>₹3,218.00/-</span>
                        </div>
                        <div className="flex flex-row justify-between items-center mb-1.5 pt-1.5">
                            <span>Other charges & taxes</span>
                            <span>₹323.00/-</span>
                        </div>
                        <div className="flex flex-row justify-between items-center mb-1.5 pt-1.5">
                            <span>Delivery charge</span>
                            <span>₹120.00/-</span>
                        </div>
                        <div className="flex flex-row justify-between items-center pt-2.5 paragraphMediumSemiBold">
                            <span>Total bill amount</span>
                            <span>₹3,756.00/-</span>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
