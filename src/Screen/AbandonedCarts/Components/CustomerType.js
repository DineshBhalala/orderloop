import React from 'react'

const CustomerType = (props) => {
    return (
        <>
            <div className='flex items-center'>
                <span className={`border rounded paragraphSmallMedium px-2 py-1 md:text-xs ${props.customerLabel === "New" ? "text-primary-500 border-primary-500 bg-primary-50" : "text-secondary-800 border-secondary-800 bg-secondary-100"}`}>{props.customerLabel}</span>
            </div>
        </>
    )
}

export default CustomerType