import React from "react";

export default function Card(props) {
    const heighLightedPart = props.highLightedPart;

    const text = props.description;

    const index = text.indexOf(heighLightedPart);

    return (
        <>
            <div className="relative border border-neutral-300 rounded-md px-4 pt-3 min-h-[136px] w-full">
                <div className="paragraphSmallSemiBold text-neutral-500">{props.title}</div>
                <h6 className="headingH6SemiBoldDesktop py-3">{props.header}</h6>
                <p className="paragraphXSmallRegular text-neutral-500 lg:max-w-[256px] md:max-w-full">
                    {text.substring(0, index)}
                    <span className="paragraphXSmallMedium text-destructive-500">{text.substring(index, index + heighLightedPart.length)}</span>
                    {text.substring(index + heighLightedPart.length)}
                </p>

                <div className="absolute top-3 right-4">
                    <div className="p-1 bg-primary-500 rounded-lg">{props.icon}</div>
                </div>
            </div>
        </>
    );
}
