import React, { useState } from "react";
import { ReactComponent as DownArrow } from "../../../Assets/chevron-down.svg";

const CustomercartdetailsDropDown = (props) => {
    const [isShowDetails, setIsShowDetails] = useState(false);

    const handleClickShowDetails = () => {
        setIsShowDetails(!isShowDetails);
    };

    return (
        <>
            <div className="w-full px-4 py-3 border border-neutral-300 rounded-md cursor-pointer" onClick={handleClickShowDetails}>
                <div className="flex flex-row items-center justify-between">
                    <div>
                        <h3 className="paragraphOverlineSmall text-neutral-700 mb-1 uppercase">customer name:</h3>
                        <span className="paragraphSmallRegular">{props.customerName}</span>
                    </div>
                    <div className="flex flex-row items-center">
                        <div className={`flex flex-row items-center mr-1 ${isShowDetails && "hidden"}`}>
                            <span className="paragraphSmallRegular ml-1">{props.customerType}</span>
                        </div>
                        <div className={`${isShowDetails && "rotate-180"}`}>
                            <DownArrow />
                        </div>
                    </div>
                </div>
                <div className={`${!isShowDetails && "hidden"}`}>
                    <div className="pt-2 flex items-center">
                        <span className="paragraphOverlineSmall text-neutral-700 uppercase">customer type:</span>
                        <span className="paragraphSmallRegular ml-1">{props.customerType}</span>
                    </div>
                    <div className="pt-2">
                        <span className="paragraphOverlineSmall text-neutral-700 uppercase">order date:</span>
                        <span className="paragraphSmallRegular ml-1">{props.orderDate}</span>
                    </div>
                    <div className="pt-2">
                        <span className="paragraphOverlineSmall text-neutral-700 uppercase">order amount:</span>
                        <span className="paragraphSmallRegular ml-1">{props.orderAmount}</span>
                    </div>

                    <div className="pt-2 flex flex-row justify-between items-center mobile:block">
                        <div className="flex flex-row items-center">
                            <span className="paragraphOverlineSmall text-neutral-700 uppercase">Order type:</span>
                            <div className="paragraphSmallRegular ml-1 flex flex-row items-center">
                                <span className="paragraphSmallRegular ml-1">{props.orderType}</span>
                            </div>
                        </div>
                        <span className="paragraphSmallUnderline text-primary-500 cursor-pointer" onClick={() => props.handleClickCustomerCartDetails()}>
                            View details
                        </span>
                    </div>
                </div>
            </div>
        </>
    );
};

export default CustomercartdetailsDropDown;
