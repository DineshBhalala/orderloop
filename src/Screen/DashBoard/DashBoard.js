import React, { useState } from "react";
import SliderDashboard from "react-slick";
import DashBoardCard from "../../Components/DashBoardCard/DashBoardCard";
import { LargePrimaryButton } from "../../Components/Buttons/Button";
import { ReactComponent as Dashboard } from "../../Assets/dashboard.svg";
import { ReactComponent as Filter } from "../../Assets/filter.svg";
import { ReactComponent as Export } from "../../Assets/export.svg";
import { ReactComponent as Credits } from "../../Assets/credits.svg";
import { ReactComponent as NetSales } from "../../Assets/net-sales.svg";
import { ReactComponent as NoOfOrder } from "../../Assets/no-of-orders.svg";
import { ReactComponent as Expenses } from "../../Assets/expenses.svg";
import { ReactComponent as CashCollection } from "../../Assets/cash-collection.svg";
import { ReactComponent as OnlineOrders } from "../../Assets/online-orders.svg";
import { ReactComponent as Taxes } from "../../Assets/taxes.svg";
import { ReactComponent as Discount } from "../../Assets/discount.svg";
import { ReactComponent as Mobile } from "../../Assets/mobile.svg";
import { ReactComponent as Swiggy } from "../../Assets/swiggy.svg";
import { ReactComponent as Zomato } from "../../Assets/zomato.svg";
import { ReactComponent as Banner } from "../../Assets/banners.svg";
import { ReactComponent as Dowload } from "../../Assets/download.svg";
import { ReactComponent as UniqeCustomer } from "../../Assets/unique-customer.svg";
import { ReactComponent as DineIn } from "../../Assets/dine-in.svg";
import { ReactComponent as Order } from "../../Assets/order.svg";
import { ReactComponent as Rider } from "../../Assets/riders.svg";
import garlicBread from "../..//Assets/garlic-bread.svg";
import baverages from "../..//Assets/beverages.svg";
import classicPizza from "../..//Assets/classic-pizza.svg";
import { DefaultBreadcrumbs } from "../../Components/Breadcrumbs/Breadcrumbs";
import TotalSalesPopup from "./Components/TotalSalesPopup";
import { DropdownFavourite, OutletStatisticsDetails } from "./Components/DropDownCards";
import CalenderField from "../../Components/Calender/CalenderField";

export default function DashBoard() {
    const cardContent = [
        {
            title: "Total sales",
            amount: "₹4,50,000.00",
            percentage: "10.2",
            focusContent: "₹10,500.00",
            content: "You made an extra total sales of ₹10,500.00 this week",
            reimbursement: "Total sales of 23 outlets",
            focusContentColor: "tertiary-800",
            profit: true,
            icon: <Credits height={24} width={24} stroke="#ffffff" />,
        },
        {
            title: "Net sales",
            amount: "₹3,67,680.50",
            percentage: "8.2",
            focusContent: "₹9,500.00",
            reimbursement: "Net sales of 23 outlets",
            content: "You made an extra net sales of ₹9,500.00 this week",
            focusContentColor: "tertiary-800",
            profit: true,
            icon: <NetSales height={24} width={24} stroke="#ffffff" />,
        },
        {
            title: "No. of orders",
            amount: "₹354",
            percentage: "5.2",
            focusContent: "₹125",
            reimbursement: "No. of invoices generated",
            content: "You made ₹125 orders less than last week",
            focusContentColor: "destructive-600",
            profit: false,
            icon: <NoOfOrder height={24} width={24} stroke="#ffffff" />,
        },
        {
            title: "Expenses",
            amount: "₹0.00",
            percentage: "",
            focusContent: "₹10,500.00",
            reimbursement: "Expenses recorded",
            content: "You made ₹10,500.00 of offline sales collected via cash",
            focusContentColor: "destructive-600",
            profit: false,
            icon: <Expenses height={24} width={24} stroke="#ffffff" />,
        },
        {
            title: "Cash collection",
            amount: "₹1,25,000.00",
            percentage: "10.2",
            focusContent: "22.42%",
            reimbursement: "Cash collection of 23 outlets",
            content: "You made 22.42% of offline sales collected via cash",
            focusContentColor: "tertiary-800",
            profit: true,
            icon: <CashCollection height={24} width={24} stroke="#ffffff" />,
        },
        {
            title: "Online sales",
            amount: "₹3,67,680.50",
            percentage: "8.2",
            focusContent: "77.58%",
            reimbursement: "Online sale of 23 outlets",
            content: "You made 77.58% of sales generated from online",
            focusContentColor: "tertiary-800",
            profit: true,
            icon: <OnlineOrders height={24} width={24} stroke="#ffffff" />,
        },
        {
            title: "Taxes",
            amount: "₹1,256.52",
            percentage: "8.2",
            focusContent: "₹1256.52",
            reimbursement: "Taxes record of 23 outlets",
            content: "You paid ₹1256.52 taxes recorded on POS",
            focusContentColor: "tertiary-800",
            profit: true,
            icon: <Taxes height={24} width={24} stroke="#ffffff" />,
        },
        {
            title: "Discounts",
            amount: "₹3,680.50",
            focusContent: "30.8%",
            reimbursement: "30.83% of my amount",
            content: "You gave a discount of 30.8% amount",
            focusContentColor: "tertiary-800",
            profit: true,
            icon: <Discount height={24} width={24} stroke="#ffffff" />,
        },
    ];

    const favoriteFood = [
        {
            dish: "Paneer tikka pizza",
            order: 345,
            revenue: "₹18,234",
        },
        {
            dish: "Burn to hell pizza",
            order: 256,
            revenue: "₹9,632",
        },
        {
            dish: "7 cheese pizza",
            order: 76,
            revenue: "₹4,234",
        },
    ];

    const favoriteCategory = [
        {
            dish: "Classic pizza",
            order: "10,456",
            revenue: "₹1,07,234",
            icon: classicPizza,
        },
        {
            dish: "Garlic bread",
            order: "5,983",
            revenue: "₹29,632",
            icon: garlicBread,
        },
        { dish: "Beverage", order: "2,765", revenue: "₹14,286", icon: baverages },
    ];

    const topCustomers = [
        {
            name: "Sarthak kanchan",
            order: 345,
            revenue: "₹18,234",
        },
        {
            name: "Arjun patel",
            order: 265,
            revenue: "₹9,632",
        },
        {
            name: "John Doe",
            order: 76,
            revenue: "₹4,234",
        },
    ];

    const platformSalesCardContent = [
        {
            title: "Mobile Application",
            amount: "₹4,50,000.00",
            percentage: "10.2",
            content: "You made an extra total sales of ₹10,500.00 this week",
            focusContent: "₹10,500.00",
            reimbursement: "Total sales of 23 outlets",
            focusContentColor: "tertiary-800",
            profit: true,
            icon: <Mobile height={24} width={24} stroke="#ffffff" />,
        },
        {
            title: "Swiggy",
            amount: "₹1,25,000.00",
            percentage: "20.2",
            content: "You made 22.42% of offline sales collected via cash",
            focusContent: "22.42%",
            reimbursement: "Total sales of 23 outlets",
            focusContentColor: "tertiary-800",
            profit: true,
            icon: <Swiggy />,
        },
        {
            title: "Zomato",
            amount: "₹1,25,000.00",
            percentage: "32.2",
            content: "You made 22.42% of offline sales collected via cash",
            focusContent: "22.42%",
            reimbursement: "Total sales of 23 outlets",
            focusContentColor: "tertiary-800",
            profit: true,
            icon: <Zomato height={32} width={32} stroke="#ffffff" />,
        },
        {
            title: "Website",
            amount: "₹1,25,000.00",
            percentage: "5.2",
            content: "You made 22.42% of offline sales collected via cash",
            focusContent: "22.42%",
            reimbursement: "Total sales of 23 outlets",
            focusContentColor: "tertiary-800",
            profit: false,
            icon: <Banner height={24} width={24} stroke="#ffffff" />,
        },
    ];

    const totalCardContent = [
        {
            title: "Mobile Application",
            amount: "2,722",
            percentage: "7.2",
            content: "Your application was downloaded 950 times more than last week",
            focusContent: "950",
            reimbursement: "Total downloads from all cities",
            focusContentColor: "tertiary-800",
            profit: true,
            icon: <Dowload height={24} width={24} stroke="#ffffff" />,
            header: "Total downloads",
        },
        {
            title: "Unique customers",
            amount: "12,456",
            percentage: "20.2",
            content: "Your served 9,150 new customers more than last week",
            focusContent: "9,150",
            reimbursement: "Total customers of 23 outlets",
            focusContentColor: "tertiary-800",
            profit: true,
            icon: <UniqeCustomer height={24} width={24} stroke="#ffffff" />,
            header: "Total customers",
        },
        {
            title: "Refunds",
            amount: "0",
            reimbursement: "Total refunds of 23 outlets",
            focusContentColor: "tertiary-800",
            profit: true,
            icon: <CashCollection height={24} width={24} stroke="#ffffff" />,
            header: "Total refunds",
        },
        {
            title: "Voids",
            amount: "0",
            reimbursement: "Total refunds of 23 outlets",
            focusContentColor: "tertiary-800",
            profit: true,
            icon: <Taxes height={24} width={24} stroke="#ffffff" />,
            header: "Total voids",
        },
    ];

    const serviceSales = [
        {
            title: "Dine-In",
            amount: "₹4,50,000.00",
            percentage: "10.2",
            content: "You made an extra total sales of ₹10,500.00 this week",
            focusContent: "₹10,500.00",
            reimbursement: "Total sales of 23 outlets",
            focusContentColor: "tertiary-800",
            profit: true,
            icon: <DineIn height={24} width={24} stroke="#ffffff" />,
        },
        {
            title: "Takeaway",
            amount: "₹4,50,000.00",
            percentage: "10.2",
            content: "You made an extra total sales of ₹10,500.00 this week",
            focusContent: "₹10,500.00",
            reimbursement: "Total sales of 23 outlets",
            focusContentColor: "tertiary-800",
            profit: true,
            icon: <Order height={24} width={24} stroke="#ffffff" />,
        },
        {
            title: "Delivery",
            amount: "₹4,50,000.00",
            percentage: "10.2",
            content: "You made an extra total sales of ₹10,500.00 this week",
            focusContent: "₹10,500.00",
            reimbursement: "Total sales of 23 outlets",
            focusContentColor: "tertiary-800",
            profit: true,
            icon: <Rider height={24} width={24} stroke="#ffffff" />,
        },
        {
            title: "Dine-In",
            amount: "₹4,50,000.00",
            percentage: "10.2",
            content: "You made an extra total sales of ₹10,500.00 this week",
            focusContent: "₹10,500.00",
            reimbursement: "Total sales of 23 outlets",
            focusContentColor: "tertiary-800",
            profit: true,
            icon: <DineIn height={24} width={24} stroke="#ffffff" />,
        },
    ];

    const [showTotalSalesCard, setShowTotalSalesCard] = useState("hidden");

    const handleClickTotalSalesCard = (el) => {
        if (el.title !== "Total sales") {
            return;
        }
        setShowTotalSalesCard("");
    };

    const OutletName = [
        {
            name: "Domino's Pizza - Master Outlet(All)",
            orders: "1,245",
            sales: "₹1,08,234",
            tax: "₹2,395",
            discount: "₹4,999",
            modified: "0",
            reprinted: "0",
            waiwedoff: "0.00",
        },
        {
            name: "Domino's Pizza - Univerisity Road Outlet",
            orders: "356",
            sales: "₹35,234",
            tax: "₹986",
            discount: "₹795",
            modified: "0",
            reprinted: "0",
            waiwedoff: "0.00",
        },
        {
            name: "Domino's Pizza - Tagore Road Outlet",
            orders: "794",
            sales: "₹75,234",
            tax: "₹1,564",
            discount: "₹2,999",
            modified: "0",
            reprinted: "0",
            waiwedoff: "0.00",
        },
        {
            name: "Domino's Pizza - Raiya Road Outlet",
            orders: "198",
            sales: "₹8,234",
            tax: "₹345",
            discount: "₹1,765",
            modified: "0",
            reprinted: "0",
            waiwedoff: "0.00",
        },
    ];

    const handleClickClose = () => {
        setShowTotalSalesCard(!showTotalSalesCard);
    };

    var settingsDashboardSlider = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        className: "dashboardSlide",
    };

    return (
        <>
            <div className="bg-[#fafafa]">
                <div className="pb-10 lg:pb-[23px] pt-4 w-full max-w-[1336px] mx-auto bg-white">
                    <div className={`${!showTotalSalesCard && "md:hidden"}`}>
                        <div className="px-8 lg:px-4">
                            <div className="mb-4 md:hidden">
                                <DefaultBreadcrumbs icon={<Dashboard height={20} width={20} />} mainTab="Dashboard" />
                            </div>

                            <div className="flex flex-row md:block justify-between pb-4 border-b border-b-neutral-300 mb-6 md:mb-4">
                                <CalenderField label="25 Sept 2022 - 09 Oct 2022" buttonStyle="md:w-full" />
                                <div className="flex flex-row md:w-full md:mt-4 md:justify-center">
                                    <div className="w-[120px] mx-4 lg:mx-2 md:ml-0 md:mr-2 mobile:mr-0.5 md:w-full">
                                        <LargePrimaryButton leftIconDefault={<Filter fill="#ffffff" />} leftIconClick={<Filter fill="#C4BEED" />} name="Filters" />
                                    </div>
                                    <div className="w-[161px] md:ml-2 mobile:ml-0.5 md:w-full">
                                        <LargePrimaryButton leftIconDefault={<Export stroke="#ffffff" />} leftIconClick={<Export stroke="#C4BEED" />} name="Export data" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="px-8 lg:px-4 md:px-0">
                            <div className="paragraphLargeMedium text-black md:px-4 md:pb-2">Overview</div>
                            <div className="-mx-[11px] lg:-mx-0 md:hidden">
                                {cardContent.map((el, index) => {
                                    return (
                                        <div
                                            key={index}
                                            className="align-top cursor-pointer inline-block mx-2.5 mt-4 lg:mx-0 lg:w-1/2 w-[303px] lg:even:pl-1 lg:odd:pr-1 md:flex md:mx-2.5 min-w-[303px]"
                                            onClick={() => handleClickTotalSalesCard(el)}
                                        >
                                            <DashBoardCard
                                                isReimbursementIcon={true}
                                                focusContentColor={el.focusContentColor}
                                                profit={el.profit}
                                                title={el.title}
                                                amount={el.amount}
                                                percentage={el.percentage}
                                                content={el.content}
                                                focusContent={el.focusContent}
                                                rightContent={el.rightContent}
                                                reimbursement={el.reimbursement}
                                                icon={el.icon}
                                            />
                                        </div>
                                    );
                                })}
                            </div>
                            {/* Slider dashboard overview start */}
                            <div className="hidden md:block">
                                <div>
                                    <SliderDashboard {...settingsDashboardSlider}>
                                        {cardContent.slice(0, 4).map((mapElement, mapIndex) => {
                                            return (
                                                <div key={mapIndex}>
                                                    <div className="w-[303px] mobile:w-[288px] cursor-pointer" onClick={() => handleClickTotalSalesCard(mapElement)}>
                                                        <DashBoardCard
                                                            isReimbursementIcon={true}
                                                            focusContentColor={mapElement.focusContentColor}
                                                            profit={mapElement.profit}
                                                            title={mapElement.title}
                                                            amount={mapElement.amount}
                                                            percentage={mapElement.percentage}
                                                            content={mapElement.content}
                                                            focusContent={mapElement.focusContent}
                                                            rightContent={mapElement.rightContent}
                                                            reimbursement={mapElement.reimbursement}
                                                            icon={mapElement.icon}
                                                        />
                                                    </div>
                                                </div>
                                            );
                                        })}
                                    </SliderDashboard>
                                </div>
                                <div className="mt-1.5">
                                    <SliderDashboard {...settingsDashboardSlider}>
                                        {cardContent.slice(4, 8).map((mapElement, mapIndex) => {
                                            return (
                                                <div key={mapIndex}>
                                                    <div className="w-[303px] mobile:w-[288px] cursor-pointer" onClick={() => handleClickTotalSalesCard(mapElement)}>
                                                        <DashBoardCard
                                                            isReimbursementIcon={true}
                                                            focusContentColor={mapElement.focusContentColor}
                                                            profit={mapElement.profit}
                                                            title={mapElement.title}
                                                            amount={mapElement.amount}
                                                            percentage={mapElement.percentage}
                                                            content={mapElement.content}
                                                            focusContent={mapElement.focusContent}
                                                            rightContent={mapElement.rightContent}
                                                            reimbursement={mapElement.reimbursement}
                                                            icon={mapElement.icon}
                                                        />
                                                    </div>
                                                </div>
                                            );
                                        })}
                                    </SliderDashboard>
                                </div>
                            </div>
                            {/* Slider dashboard overview end */}
                        </div>

                        <div className="px-8 lg:px-4">
                            <div className="md:hidden">
                                <div className="my-4">
                                    <span className="paragraphLargeMedium text-black">Outlet wise statistics</span>
                                </div>
                                <div className="w-full border border-neutral-300 rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden">
                                    <table className="w-full break-words">
                                        <thead>
                                            <tr className="bg-neutral-50 paragraphOverlineSmall text-neutral-700 shadow-innerShadow uppercase text-left">
                                                <th className="py-[11.5px] px-6 min-w-[322px]">
                                                    outlet <div>name</div>
                                                </th>
                                                <th className="py-[11.5px] px-6">
                                                    total <div>orders</div>
                                                </th>
                                                <th className="py-[11.5px] px-6">
                                                    total <div>sales</div>
                                                </th>
                                                <th className="py-[11.5px] px-6">
                                                    total <div>Tax</div>
                                                </th>
                                                <th className="py-[11.5px] px-6">
                                                    total <div>discount</div>
                                                </th>
                                                <th className="py-[11.5px] px-6">
                                                    bill <div>modified</div>
                                                </th>
                                                <th className="py-[11.5px] px-6">
                                                    bill <div>re-printed</div>
                                                </th>
                                                <th className="py-[11.5px] px-6">
                                                    total <div>waived off</div>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {OutletName.map((row, index) => (
                                                <tr key={index} className={`${index !== 0 && "border-t"} border-neutral-300 even:bg-neutral-50 paragraphXSmallRegular`}>
                                                    <td className="py-[24.5px] pl-6 min-w-[322px]">{row.name}</td>
                                                    <td className="py-[24.5px] pl-6">{row.orders}</td>
                                                    <td className="py-[24.5px] pl-6">{row.sales}</td>
                                                    <td className="py-[24.5px] pl-6">{row.tax}</td>
                                                    <td className="py-[24.5px] pl-6">{row.discount}</td>
                                                    <td className="py-[24.5px] pl-6">{row.modified}</td>
                                                    <td className="py-[24.5px] pl-6">{row.reprinted}</td>
                                                    <td className="py-[24.5px] pl-6">{row.waiwedoff}</td>
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div className="mt-4 mb-4 md:mb-2">
                                <span className="paragraphLargeMedium text-black">Outlet wise statistics</span>
                            </div>
                            <div className="hidden md:block">
                                {OutletName.map((el, index) => {
                                    return (
                                        <div className="pb-2" key={index}>
                                            <OutletStatisticsDetails
                                                name={el.name}
                                                orders={el.orders}
                                                sales={el.sales}
                                                tax={el.tax}
                                                discount={el.discount}
                                                modified={el.modified}
                                                reprinted={el.reprinted}
                                                waiwedoff={el.waiwedoff}
                                                content={el.name}
                                                title="OUTLET NAME:"
                                                details={el}
                                            />
                                        </div>
                                    );
                                })}
                            </div>

                            <div className="flex flex-row lg:flex-none lg:block">
                                <div className="max-w-[626px] mr-5 w-full lg:w-full lg:max-w-full">
                                    <div className="mt-4 mb-4 md:mb-2">
                                        <span className="paragraphLargeMedium text-black">Favorite food</span>
                                    </div>
                                    <div className="w-full border border-neutral-300 rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden">
                                        <table className="w-full break-words">
                                            <thead>
                                                <tr className="bg-neutral-50 paragraphOverlineSmall text-neutral-700 shadow-innerShadow">
                                                    <th className="text-left py-[11.5px] px-6 lg:w-[238px]">FAVORITE DISH</th>
                                                    <th className="text-left py-[11.5px] px-6 lg:w-[208px]">TOTAL ORDERS</th>
                                                    <th className="text-left py-[11.5px] px-6 lg:w-[218px]">REVENUE GENERATED</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {favoriteFood.map((row, index) => (
                                                    <tr key={index} className={`${index !== 0 && "border-t"} border-neutral-300 even:bg-neutral-50 paragraphXSmallRegular`}>
                                                        <td className="py-[24.5px] pl-6">{row.dish}</td>
                                                        <td className="py-[24.5px] pl-6">{row.order}</td>
                                                        <td className="py-[24.5px] pl-6">{row.revenue}</td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                    <div className="hidden md:block">
                                        {favoriteFood.map((el, index) => {
                                            return (
                                                <div key={index} className="mb-2">
                                                    <DropdownFavourite order={el.order} revenue={el.revenue} content={el.dish} title="FAVORITE DISH:" icon={el.icon} details={el} />
                                                </div>
                                            );
                                        })}
                                    </div>
                                </div>
                                <div className="max-w-[626px] w-full lg:max-w-full lg:w-full">
                                    <div className="mt-4 mb-4 md:mb-2">
                                        <span className="paragraphLargeMedium text-black">Favorite Category</span>
                                    </div>
                                    <div className="w-full border border-neutral-300 rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden">
                                        <table className="w-full break-words">
                                            <thead>
                                                <tr className="bg-neutral-50 paragraphOverlineSmall text-neutral-700 shadow-innerShadow">
                                                    <th className="text-left py-[11.5px] px-6 lg:w-[238px]">FAVORITE CATEGORY</th>
                                                    <th className="text-left py-[11.5px] px-6 lg:w-[208px]">TOTAL ORDERS</th>
                                                    <th className="text-left py-[11.5px] px-6 lg:w-[218px]">REVENUE GENERATED</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {favoriteCategory.map((row, index) => (
                                                    <tr key={index} className={`${index !== 0 && "border-t"} border-neutral-300 even:bg-neutral-50 paragraphXSmallRegular`}>
                                                        <td className="p-0">
                                                            <div className="max-w-[214px] w-full py-[14.5px] pl-6 flex flex-row items-center">
                                                                <img src={row.icon} alt="" className="mr-3" />
                                                                <span className="">{row.dish}</span>
                                                            </div>
                                                        </td>
                                                        <td className="py-[24.5px] pl-6">{row.order}</td>
                                                        <td className="py-[24.5px] pl-6">{row.revenue}</td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                    <div className="hidden md:block">
                                        {favoriteCategory.map((el, index) => {
                                            return (
                                                <div key={index} className="mb-2">
                                                    <DropdownFavourite order={el.order} revenue={el.revenue} content={el.dish} title="FAVORITE CATEGORY:" icon={el.icon} details={el} />
                                                </div>
                                            );
                                        })}
                                    </div>
                                </div>
                            </div>

                            <div className="flex flex-row lg:flex-none lg:block">
                                <div className="w-1/2 mt-6 lg:mt-4 lg:w-full">
                                    <span className="paragraphLargeMedium text-black">Total revenue</span>
                                    <div className="h-[321px] rounded-md border mt-4"></div>
                                </div>
                                <div className="mt-6 ml-5 w-1/2 lg:mt-4 lg:w-full lg:ml-0">
                                    <span className="paragraphLargeMedium text-black">Platform sales</span>
                                    <div className="h-[321px] rounded-md border mt-4"></div>
                                </div>
                            </div>

                            <div className="flex flex-row lg:flex-none lg:block md:hidden">
                                <div className="max-w-[626px] mr-5 w-full lg:w-full lg:max-w-full">
                                    <div className="mt-6 mb-4">
                                        <span className="paragraphLargeMedium text-black">Top customers</span>
                                    </div>
                                    <div className="w-full border border-neutral-300 rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden">
                                        <table className="w-full break-words">
                                            <thead>
                                                <tr className="bg-neutral-50 paragraphOverlineSmall text-neutral-700 shadow-innerShadow">
                                                    <th className="text-left py-3 px-6 lg:w-[238px]">CUSTOMER NAME</th>
                                                    <th className="text-left py-3 px-6 lg:w-[208px]">ORDER MADE</th>
                                                    <th className="text-left py-3 px-6 lg:w-[218px]">REVENUE GENERATED</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {topCustomers.map((row, index) => (
                                                    <tr key={index} className={`${index !== 0 && "border-t"} border-neutral-300 even:bg-neutral-50 paragraphXSmallRegular`}>
                                                        <td className="py-[25px] pl-6">{row.name}</td>
                                                        <td className="py-[25px] pl-6">{row.order}</td>
                                                        <td className="py-[25px] pl-6">{row.revenue}</td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="max-w-[626px] w-full lg:max-w-full lg:w-full">
                                    <div className="mt-6 mb-4">
                                        <span className="paragraphLargeMedium text-black">Top employees</span>
                                    </div>
                                    <div className="w-full border border-neutral-300 rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden">
                                        <table className="w-full break-words">
                                            <thead>
                                                <tr className="bg-neutral-50 paragraphOverlineSmall text-neutral-700 shadow-innerShadow">
                                                    <th className="text-left py-3 px-6 lg:w-[238px]">EMPLOYEE NAME</th>
                                                    <th className="text-left py-3 px-6 lg:w-[208px]">ORDERS MADE</th>
                                                    <th className="text-left py-3 px-6 lg:w-[218px]">REVENUE GENERATED</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {topCustomers.map((row, index) => (
                                                    <tr key={index} className={`${index !== 0 && "border-t"} border-neutral-300 even:bg-neutral-50 paragraphXSmallRegular`}>
                                                        <td className="py-[25px] pl-6">{row.name}</td>
                                                        <td className="py-[25px] pl-6">{row.order}</td>
                                                        <td className="py-[25px] pl-6">{row.revenue}</td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="px-8 lg:px-4 md:px-0">
                            <div className="paragraphLargeMedium text-black md:px-4 md:pb-4 mt-6 lg:mt-4">Platform wise sales</div>
                            <div className="-mx-[11px] lg:-mx-0 md:hidden">
                                {platformSalesCardContent.map((el, index) => {
                                    return (
                                        <div key={index} className="align-top inline-block mx-2.5 mt-4 lg:mx-0 lg:w-1/2 w-[303px] lg:even:pl-1 lg:odd:pr-1 md:mx-2.5 md:flex min-w-[303px]">
                                            <DashBoardCard
                                                isReimbursementIcon={true}
                                                focusContentColor={el.focusContentColor}
                                                profit={el.profit}
                                                title={el.title}
                                                amount={el.amount}
                                                percentage={el.percentage}
                                                content={el.content}
                                                focusContent={el.focusContent}
                                                rightContent={el.rightContent}
                                                reimbursement={el.reimbursement}
                                                icon={el.icon}
                                                // iconBG={el.iconBG}
                                            />
                                        </div>
                                    );
                                })}
                            </div>

                            {/* Slider dashboard Platform wise sales start */}
                            <div className="hidden md:block">
                                <div>
                                    <SliderDashboard {...settingsDashboardSlider}>
                                        {platformSalesCardContent.map((mapElement, mapIndex) => {
                                            return (
                                                <div key={mapIndex} className="w-[303px] mobile:w-[288px] cursor-pointer" onClick={() => handleClickTotalSalesCard(mapElement)}>
                                                    <DashBoardCard
                                                        isReimbursementIcon={true}
                                                        focusContentColor={mapElement.focusContentColor}
                                                        profit={mapElement.profit}
                                                        title={mapElement.title}
                                                        amount={mapElement.amount}
                                                        percentage={mapElement.percentage}
                                                        content={mapElement.content}
                                                        focusContent={mapElement.focusContent}
                                                        rightContent={mapElement.rightContent}
                                                        reimbursement={mapElement.reimbursement}
                                                        icon={mapElement.icon}
                                                    />
                                                </div>
                                            );
                                        })}
                                    </SliderDashboard>
                                </div>
                            </div>
                            {/* Slider dashboard Platform wise sales end */}
                        </div>

                        <div className="px-8 lg:px-4">
                            <div className="-mx-[11px] lg:-mx-0 md:-mx-0 md:mt-4 align-top">
                                {totalCardContent.map((el, index) => {
                                    return (
                                        <div className="inline-block md:block lg:w-1/2 md:w-full  align-top  lg:even:pl-1 lg:odd:pr-1" key={index}>
                                            <div className="mt-6 md:mt-4">
                                                <span className="paragraphLargeMedium text-black px-2.5 lg:px-0">{el.header}</span>
                                            </div>
                                            <div className="inline-block mx-2.5 mt-4 lg:mx-0 w-[303px] lg:w-full md:mx-0">
                                                <DashBoardCard
                                                    isReimbursementIcon={true}
                                                    focusContentColor={el.focusContentColor}
                                                    profit={el.profit}
                                                    title={el.title}
                                                    amount={el.amount}
                                                    percentage={el.percentage}
                                                    content={el.content}
                                                    focusContent={el.focusContent}
                                                    rightContent={el.rightContent}
                                                    reimbursement={el.reimbursement}
                                                    icon={el.icon}
                                                    cardType="dashboard"
                                                />
                                            </div>
                                        </div>
                                    );
                                })}
                            </div>
                        </div>
                        <div className="px-8 lg:px-4 md:px-0">
                            <div className="paragraphLargeMedium text-black md:px-4 md:pb-4 mt-6 lg:mt-4">Service type sales</div>
                            <div className="-mx-[11px] lg:-mx-0 md:hidden">
                                {serviceSales.map((el, index) => {
                                    return (
                                        <div key={index} className="align-top inline-block mx-2.5 mt-4 lg:mx-0 lg:w-1/2 w-[303px] lg:even:pl-1 lg:odd:pr-1 md:mx-2.5 md:flex min-w-[303px]">
                                            <DashBoardCard
                                                isReimbursementIcon={true}
                                                focusContentColor={el.focusContentColor}
                                                profit={el.profit}
                                                title={el.title}
                                                amount={el.amount}
                                                percentage={el.percentage}
                                                content={el.content}
                                                focusContent={el.focusContent}
                                                rightContent={el.rightContent}
                                                reimbursement={el.reimbursement}
                                                icon={el.icon}
                                            />
                                        </div>
                                    );
                                })}
                            </div>
                            {/* Slider dashboard Platform wise sales start */}
                            <div className="hidden md:block">
                                <div>
                                    <SliderDashboard {...settingsDashboardSlider}>
                                        {serviceSales.map((mapElement, mapIndex) => {
                                            return (
                                                <div key={mapIndex} className="w-[303px] mobile:w-[288px] cursor-pointer" onClick={() => handleClickTotalSalesCard(mapElement)}>
                                                    <DashBoardCard
                                                        isReimbursementIcon={true}
                                                        focusContentColor={mapElement.focusContentColor}
                                                        profit={mapElement.profit}
                                                        title={mapElement.title}
                                                        amount={mapElement.amount}
                                                        percentage={mapElement.percentage}
                                                        content={mapElement.content}
                                                        focusContent={mapElement.focusContent}
                                                        rightContent={mapElement.rightContent}
                                                        reimbursement={mapElement.reimbursement}
                                                        icon={mapElement.icon}
                                                    />
                                                </div>
                                            );
                                        })}
                                    </SliderDashboard>
                                </div>
                            </div>
                            {/* Slider dashboard Platform wise sales end */}
                        </div>
                    </div>
                    <div className={`${showTotalSalesCard && "hidden"} -mt-4`}>
                        <TotalSalesPopup handleClickClose={handleClickClose} />
                    </div>
                </div>
            </div>
        </>
    );
}
