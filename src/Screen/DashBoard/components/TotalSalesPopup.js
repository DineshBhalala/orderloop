import React from "react";
import { ReactComponent as LeftArrow } from "../../../Assets/chevron-down.svg";
import { ReactComponent as Close } from "../../../Assets/close.svg";

export default function TotalSalesPopup(props) {
    const salesCardContent = [
        { "My Amount": "₹5,50,000.00" },
        {
            Discount: "- ₹1,10,000.00",
        },
        {
            "Delivery charge": "₹1,000.00",
        },
        {
            "Container charge": "₹600.00",
        },
        {
            "Service charge": "₹0.00",
        },
        {
            Tax: "₹7,000.00",
        },
        { "Waived off": "₹0.00" },
        {
            "Round off": "- ₹0.00",
        },
    ];

    const totalSalesINR = salesCardContent.reduce((acc, obj) => {
        const value = parseFloat(Object.values(obj)[0].replace(/[₹, ]/g, ""));
        return acc + value;
    }, 0);
    return (
        <>
            <div className="fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 flex md:relative">
                <div className="h-[484px] max-w-[710px] w-full md:h-full md:overflow-auto rounded-xl md:rounded-none bg-shades-50 px-8 py-6 md:py-0 m-auto [&::-webkit-scrollbar]:hidden md:px-4 md:max-w-full">
                    <div className="hidden md:flex flex-row mb-4 pt-4 cursor-pointer" onClick={() => props.handleClickClose()}>
                        <div className="w-6 h-6">
                            <LeftArrow className="rotate-90" />
                        </div>
                        <span className="ml-1 paragraphMediumMedium">Back to dashboard</span>
                    </div>
                    <div className="flex flex-row justify-between items-center mb-6">
                        <div className="">
                            <span className="paragraphLargeMedium">Total sales</span>
                            <p className="paragraphMediumItalic text-neutral-500">Total sales defines the overall collection restaurant made</p>
                        </div>
                        <div className="md:hidden cursor-pointer" onClick={() => props.handleClickClose()}>
                            <Close />
                        </div>
                    </div>

                    <div className="flex flex-row justify-between items-center md:block">
                        <div className="max-w-[251px] h-[302px] w-full md:max-w-full">
                            <div className="h-[240px] w-[240px] rounded-full bg-tertiary-600 border mx-[5.5px] mb-[38px] flex items-center justify-center md:mx-auto">
                                <div className="h-[155px] w-[155px] rounded-full bg-white"></div>
                            </div>
                            <div className="flex flex-row justify-between md:justify-center">
                                <div className="flex flex-row items-center">
                                    <div className="h-[11px] w-[11px] rounded-full bg-tertiary-600 mr-2" />
                                    <span className="paragraphMediumRegular">Online Sales</span>
                                </div>

                                <div className="flex flex-row items-center md:ml-6">
                                    <div className="h-[11px] w-[11px] rounded-full bg-secondary-600 mr-2" />
                                    <span className="paragraphMediumRegular">Offline Sales</span>
                                </div>
                            </div>
                        </div>
                        <div className="max-w-[315px] w-full md:mt-10 md:max-w-full">
                            <div className="border-b border-neutral-300 mb-4">
                                {salesCardContent.map((el, index) => {
                                    return (
                                        <div key={index} className="flex flex-row justify-between mb-4">
                                            <span className="paragraphMediumRegular">{Object.keys(el)}</span>
                                            <span className="paragraphMediumRegular">{Object.values(el)}</span>
                                        </div>
                                    );
                                })}
                            </div>
                            <div className="flex flex-row justify-between max-w-[315px] md:max-w-full">
                                <span className="paragraphMediumSemiBold">Total Sales</span>
                                <span className="paragraphMediumSemiBold">{`₹${totalSalesINR.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,")}`}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
