import { useState } from "react";
import { ReactComponent as DownArrow } from "../../../Assets/chevron-down.svg";

export const DropdownFavourite = (props) => {
  const [showDetails, setShowDetails] = useState(false);
  const handleClickDropDown = () => {
    setShowDetails(!showDetails);
  };
  return (
    <>
      <div className="px-2 py-3 border border-neutral-300 rounded-md">
        <div className="w-full flex flex-row justify-between items-center">
          <div className="flex flex-row items-center">
            {props.icon && (
              <>
                <div className="w-10 h-10 mx-2">
                  <img src={props.icon} alt="" className="" />
                </div>
              </>
            )}
            <div className="ml-2 h-[44px]">
              <div className="paragraphOverlineSmall text-neutral-700 mb-1">
                {props.title}
              </div>
              <div className="paragraphXSmallRegular text-neutral-900">
                {props.content}
              </div>
            </div>
          </div>
          <div
            className={`w-6 h-6 mr-2 ${showDetails && "rotate-180"} cursor-pointer`}
            onClick={handleClickDropDown}
          >
            <DownArrow />
          </div>
        </div>
        <div
          className={`${!showDetails && "hidden"} ${props.title === "FAVORITE DISH:" ? "pl-2" : "pl-16"
            }`}
        >
          <div>
            <span className="paragraphOverlineSmall text-neutral-700">
              TOTAL ORDERS:
            </span>
            <span className="paragraphXSmallRegular ml-1">{props.order}</span>
          </div>
          <div>
            <span className="paragraphOverlineSmall text-neutral-700">
              REVENUE GENERATED:
            </span>
            <span className="paragraphXSmallRegular ml-1">{props.revenue}</span>
          </div>
        </div>
      </div>
    </>
  );
};

export const OutletStatisticsDetails = (props) => {
  const [showDetails, setShowDetails] = useState(false);
  const handleClickDropDown = () => {
    setShowDetails(!showDetails);
  };
  return (
    <>
      <div className="px-2 py-3 border border-neutral-300 rounded-md">
        <div className="w-full flex flex-row justify-between items-center">
          <div className="flex flex-row items-center">
            {props.icon && (
              <>
                <div className="w-10 h-10 mx-2">
                  <img src={props.icon} alt="" className="" />
                </div>
              </>
            )}
            <div className="ml-2 h-[44px]">
              <div className="paragraphOverlineSmall text-neutral-700 mb-1">
                {props.title}
              </div>
              <div className="paragraphXSmallRegular text-neutral-900">
                {props.content}
              </div>
            </div>
          </div>
          <div
            className={`w-6 h-6 mr-2 ${showDetails && "rotate-180"} cursor-pointer`}
            onClick={handleClickDropDown}
          >
            <DownArrow />
          </div>
        </div>
        <div className={`${!showDetails && "hidden"} pl-2`}>
          <div>
            <span className="paragraphOverlineSmall text-neutral-700">
              TOTAL ORDERS:
            </span>
            <span className="paragraphXSmallRegular ml-1">{props.orders}</span>
          </div>
          <div>
            <span className="paragraphOverlineSmall text-neutral-700">
              TOTAL SALES:
            </span>
            <span className="paragraphXSmallRegular ml-1">{props.sales}</span>
          </div>
          <div>
            <span className="paragraphOverlineSmall text-neutral-700">
              TOTAL TAX:
            </span>
            <span className="paragraphXSmallRegular ml-1">{props.tax}</span>
          </div>
          <div>
            <span className="paragraphOverlineSmall text-neutral-700">
              TOTAL DISCOUNT:
            </span>
            <span className="paragraphXSmallRegular ml-1">
              {props.discount}
            </span>
          </div>
          <div>
            <span className="paragraphOverlineSmall text-neutral-700">
              BILL MODIFIED:
            </span>
            <span className="paragraphXSmallRegular ml-1">
              {props.modified}
            </span>
          </div>
          <div>
            <span className="paragraphOverlineSmall text-neutral-700">
              BILL RE-PRINTED:
            </span>
            <span className="paragraphXSmallRegular ml-1">
              {props.reprinted}
            </span>
          </div>
          <div>
            <span className="paragraphOverlineSmall text-neutral-700">
              TOTAL WAIVED OFF:
            </span>
            <span className="paragraphXSmallRegular ml-1">
              {props.waiwedoff}
            </span>
          </div>
        </div>
      </div>
    </>
  );
};
