import React from 'react'
import { ReactComponent as Calender } from "../../Assets/calendar.svg";

const CalenderField = (props) => {
    return (
        <>
            <div className='w-full'>
                <button className={`flex flex-row items-center rounded-md border border-neutral-300 px-[11px] 3xl:w-[278px] w-[284px] h-12 ${props.buttonStyle}`}>
                    <Calender className="mx-1" />
                    <span className={`paragraphMediumRegular mx-1 ${props.labelStyle}`}>{props.label}</span>
                </button>
            </div>
        </>
    )
}

export default CalenderField