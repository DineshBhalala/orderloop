import React from "react";

export const Tab = (props) => {
    return (
        <>
            <button
                className={`w-full rounded-md px-3 mobile:px-1 group border ${props.isActive ? "bg-primary-50" : "bg-shades-50"} ${!props.isActive ? "border-neutral-300" : "border-primary-500"
                    } disabled:border-neutral-300 disabled:bg-shades-50 h-12 flex justify-center ${props.shadow} ${props.tabStyle}`}
                disabled={props.disabled}
            >
                <div className="flex flex-row items-center h-full">
                    {props.IconDefault && (
                        <>
                            <div className="">{props.isActive ? props.IconClick : props.IconDefault}</div>
                        </>
                    )}
                    {props.label && (
                        <span
                            className={`${props.labelStyle ?? "paragraphMediumRegular mx-1"} ${props.type === "offer" && "md:text-[15px] mobile:text-xs"} ${props.isActive ? "text-primary-500" : "text-neutral-700 group-hover:text-neutral-900"
                                } group-disabled:text-neutral-300 ${props.bannerTabStyle} ${props.hideName}`}
                        >
                            {props.label}
                        </span>
                    )}
                    {props.badgeText && (
                        <div className="mx-1">
                            <div
                                className={`px-2.5 paragraphXSmallMedium m-auto group-disabled:text-neutral-400 ${props.isActive ? "text-shades-50" : "text-neutral-700 group-hover:text-neutral-900"
                                    } h-6 rounded-3xl py-0.5 ${props.isActive ? "bg-primary-500 text-shades-50" : "bg-neutral-200"} disabled:bg-neutral-200`}
                            >
                                <span className="h-5">{props.badgeText}</span>
                            </div>
                        </div>
                    )}
                </div>
            </button>
        </>
    );
};

export const TabWithTwoImage = (props) => {
    return (
        <>
            <button
                className={`w-full rounded-md px-3 md:px-1 mobile:px-0 group border border-neutral-300 group ${props.isActive ? "bg-primary-50" : "bg-shades-50"} ${!props.isActive ? "border-neutral-300" : "border-primary-500"
                    } disabled:border-neutral-300 disabled:bg-shades-50 py-3 h-12 justify-center`}
                disabled={props.disabled}
            >
                <div className="flex flex-row items-center justify-center">
                    {props.IconDefault && (
                        <>
                            <div className="mx-1">{props.isActive ? props.IconClick : props.IconDefault}</div>
                        </>
                    )}
                    {props.label && (
                        <span
                            className={`paragraphSmallMedium mx-1 mobile:mx-0 ${props.isActive ? "text-primary-500" : "text-neutral-700 group-hover:text-neutral-900"} group-disabled:text-neutral-300`}
                        >
                            {props.label}
                        </span>
                    )}
                    {props.IconRight && (
                        <>
                            <div className="px-1">{props.isActive ? props.IconRightActive : props.IconRight}</div>
                        </>
                    )}
                </div>
            </button>
        </>
    );
};

export const TabBig = (props) => {
    return (
        <>
            <button
                className={`w-full rounded-md px-2.5 group border group ${props.isActive ? "bg-primary-50" : "bg-shades-50"} ${!props.isActive ? "border-neutral-300" : "border-primary-500"
                    } disabled:border-neutral-300 disabled:bg-shades-50 py-[11px] h-12 flex justify-center`}
                disabled={props.disabled}
            >
                <div className="flex flex-row items-center">
                    {props.IconDefault && (
                        <>
                            <div className="mx-1">{props.isActive ? props.IconClick : props.IconDefault}</div>
                        </>
                    )}
                    {props.label && (
                        <span className={`paragraphMediumRegular mx-1 ${props.isActive ? "text-primary-500" : "text-neutral-700 group-hover:text-neutral-900"} group-disabled:text-neutral-300 `}>
                            {props.label}
                        </span>
                    )}
                    {props.badgeText && (
                        <div className="mx-1">
                            <div className={`w-6 h-5 rounded-3xl ${props.isActive ? "bg-primary-500" : "bg-neutral-200"} group-disabled:bg-neutral-200`}>
                                <span className={`leading-none m-auto group-disabled:text-neutral-400 ${props.isActive ? "text-shades-50" : "text-neutral-700 group-hover:text-neutral-900"}`}>
                                    {props.badgeText}
                                </span>
                            </div>
                        </div>
                    )}
                </div>
            </button>
        </>
    );
};

export const MultipleTab = (props) => {
    return (
        <button
            disabled={props.disabled}
            onClick={() => props.onClick(props.index)}
            className={`${props.maxWidth ?? "max-w-[173px]"} ${props.minWidth} w-full border ${props.isActive ? "border-primary-500 bg-primary-50 text-primary-500" : "border-neutral-300 hover:text-neutral-900"
                } rounded-lg text-left px-4 py-2.5 max-h-[72px] min-h-[48px] paragraphMediumRegular text-neutral-700 disabled:text-neutral-300 flex flex-row`}
        >
            <div className="flex flex-row items-center">
                {props.icon && <div className={`${!props.showIcon && "hidden"} mr-2`}>{props.icon}</div>}
                {props.label}
            </div>
            {props.badgeText && (
                <div className="ml-2">
                    <div
                        className={`px-2.5 paragraphXSmallMedium m-auto group-disabled:text-neutral-400 ${props.isActive ? "text-shades-50" : "text-neutral-700 group-hover:text-neutral-900"
                            } h-6 rounded-3xl py-0.5 ${props.isActive ? "bg-primary-500 text-shades-50" : "bg-neutral-200"} disabled:bg-neutral-200`}
                    >
                        <span className="h-5">{props.badgeText}</span>
                    </div>
                </div>
            )}
        </button>
    );
};
