import React, { useEffect, useState } from "react";

export const LargePrimaryButton = (props) => {
    const [isClicked, setIsClicked] = useState(false);

    useEffect(() => {
        if (typeof props.isClicked === "boolean") {
            setIsClicked(props.isClicked);
        }
    }, [props.isClicked]);

    const handleClickBtn = () => {
        setIsClicked(props.isDefault ?? !isClicked);
    };

    return (
        <>
            <button
                className={`
          ${isClicked ? "bg-primary-600" : "bg-primary-500"} h-12 rounded-md hover:bg-primary-600 disabled:bg-primary-200 group w-full items-center flex justify-center
       ${props.buttonStyle} `}
                onClick={handleClickBtn}
                disabled={props.disabled}
            >
                <div className={`flex flex-row ${props.padding ?? "px-4"} md:px-1 items-center ${props.containerStyle}`}>
                    {props.leftIconDefault && (
                        <>
                            <div className="mx-1">{isClicked ? props.leftIconClick : props.leftIconDefault}</div>
                        </>
                    )}

                    {props.name && (
                        <span
                            className={`${isClicked ? "text-primary-200" : "text-shades-50"} truncate ${props.fontsWeight ?? "font-normal"} ${
                                props.fontsSize ?? "text-base"
                            } paragraphMediumMedium mx-1 ${props.hideName}`}
                        >
                            {props.name}
                        </span>
                    )}

                    {props.rightIconDefault && (
                        <>
                            <div className="mx-1">{isClicked ? props.rightIconClick : props.rightIconDefault}</div>
                        </>
                    )}
                    {props.badgeText && (
                        <span className="px-2.5 py-0.5 rounded-3xl text-primary-500 bg-white group-hover:text-primary-600 mx-1 group-disabled:text-primary-200 paragraphXSmallMedium">
                            {props.badgeText}
                        </span>
                    )}
                </div>
            </button>
        </>
    );
};

export const LargeTertiaryButton = (props) => {
    const [isClicked, setIsClicked] = useState(false);

    const handleClickBtn = () => {
        setIsClicked(props.isDefault ?? !isClicked);
    };

    return (
        <>
            <button
                className={`
          ${isClicked ? "bg-tertiary-600" : "bg-tertiary-500"} h-12 rounded-md hover:bg-tertiary-600 disabled:bg-tertiary-200 group w-full items-center flex justify-center
        `}
                onClick={handleClickBtn}
                disabled={props.disabled}
            >
                <div className="flex flex-row px-4 md:px-1 items-center">
                    {props.leftIconDefault && (
                        <>
                            <div className="mx-1">{isClicked ? props.leftIconClick : props.leftIconDefault}</div>
                        </>
                    )}

                    {props.name && (
                        <span
                            className={`${isClicked ? "text-tertiary-200" : "text-shades-50"} ${props.fontsWeight ?? "font-normal"} paragraphMediumMedium ${props.nameMargin ?? "mx-1"} ${
                                props.hideName
                            }`}
                        >
                            {props.name}
                        </span>
                    )}

                    {props.rightIconDefault && (
                        <>
                            <div className="mx-1">{isClicked ? props.rightIconClick : props.rightIconDefault}</div>
                        </>
                    )}
                    {props.badgeText && (
                        <span className="px-2.5 py-0.5 rounded-3xl text-tertiary-500 bg-white group-hover:text-tertiary-600 mx-1 group-disabled:text-tertiary-200">{props.badgeText}</span>
                    )}
                </div>
            </button>
        </>
    );
};

export const LargeSecondaryButton = (props) => {
    const [isClicked, setIsClicked] = useState(false);

    const handleClickBtn = () => {
        setIsClicked(props.isDefault ?? !isClicked);
    };

    return (
        <>
            <button
                className={`
          ${isClicked ? "bg-secondary-600" : "bg-secondary-500"} h-12 rounded-md hover:bg-secondary-600 disabled:bg-secondary-200 group w-full items-center flex justify-center
        `}
                onClick={handleClickBtn}
                disabled={props.disabled}
            >
                <div className="flex flex-row px-4 md:px-1 items-center">
                    {props.leftIconDefault && (
                        <>
                            <div className="mx-1">{isClicked ? props.leftIconClick : props.leftIconDefault}</div>
                        </>
                    )}

                    <span className={`${isClicked ? "text-secondary-200" : "text-shades-50"} ${props.fontsWeight ?? "font-normal"} paragraphMediumMedium mx-1`}>{props.name}</span>
                    {props.rightIconDefault && (
                        <>
                            <div className="mx-1">{isClicked ? props.rightIconClick : props.rightIconDefault}</div>
                        </>
                    )}
                    {props.badgeText && (
                        <span className="px-2.5 py-0.5 rounded-3xl text-secondary-500 bg-white group-hover:text-secondary-600 mx-1 group-disabled:text-secondary-200">{props.badgeText}</span>
                    )}
                </div>
            </button>
        </>
    );
};

export const LargeDestructiveButton = (props) => {
    const [isClicked, setIsClicked] = useState(false);

    const handleClickBtn = () => {
        setIsClicked(props.isDefault ?? !isClicked);
    };

    return (
        <>
            <button
                className={`
          ${isClicked ? "bg-destructive-600" : "bg-destructive-500"} h-12 rounded-md hover:bg-destructive-600 disabled:bg-destructive-200 group w-full items-center flex justify-center
        `}
                onClick={handleClickBtn}
                disabled={props.disabled}
            >
                <div className="flex flex-row px-4 md:px-1 items-center">
                    {props.leftIconDefault && (
                        <>
                            <div className="mx-1">{isClicked ? props.leftIconClick : props.leftIconDefault}</div>
                        </>
                    )}
                    {props.name && (
                        <span
                            className={`${isClicked ? "text-tertiary-200" : "text-shades-50"} ${props.fontsWeight ?? "font-normal"} ${props.fontsSize ?? "text-base"} paragraphMediumMedium mx-1 ${
                                props.hideName
                            }`}
                        >
                            {props.name}
                        </span>
                    )}
                    {props.rightIconDefault && (
                        <>
                            <div className="mx-1">{isClicked ? props.rightIconClick : props.rightIconDefault}</div>
                        </>
                    )}
                    {props.badgeText && (
                        <span className="px-2.5 py-0.5 rounded-3xl text-destructive-500 bg-white group-hover:text-destructive-600 mx-1 group-disabled:text-destructive-200">{props.badgeText}</span>
                    )}
                </div>
            </button>
        </>
    );
};

export const MediumPrimaryButton = (props) => {
    const [isClicked, setIsClicked] = useState(false);

    const handleClickBtn = () => {
        setIsClicked(props.isDefault ?? !isClicked);
    };

    return (
        <>
            <button
                className={`
          ${isClicked ? "bg-primary-600" : "bg-primary-500"} h-10 rounded-md hover:bg-primary-600 disabled:bg-primary-200 group w-full items-center flex justify-center
        `}
                onClick={handleClickBtn}
                disabled={props.disabled}
            >
                <div className="flex flex-row px-[13px] md:px-1 items-center">
                    {props.leftIconDefault && (
                        <>
                            <div className="mx-[3px]">{isClicked ? props.leftIconClick : props.leftIconDefault}</div>
                        </>
                    )}

                    <span className={`${isClicked ? "text-primary-200" : "text-shades-50"} ${props.fontsWeight ?? "font-normal"} paragraphSmallMedium mx-[3px]`}>{props.name}</span>
                    {props.rightIconDefault && (
                        <>
                            <div className="mx-[3px]">{isClicked ? props.rightIconClick : props.rightIconDefault}</div>
                        </>
                    )}
                    {props.badgeText && (
                        <span className="px-2 py-0.5 rounded-3xl text-primary-500 bg-white group-hover:text-primary-600 mx-[3px] group-disabled:text-primary-200">{props.badgeText}</span>
                    )}
                </div>
            </button>
        </>
    );
};

export const MediumSecondaryButton = (props) => {
    const [isClicked, setIsClicked] = useState(false);

    const handleClickBtn = () => {
        setIsClicked(props.isDefault ?? !isClicked);
    };

    return (
        <>
            <button
                className={`
          ${isClicked ? "bg-secondary-600" : "bg-secondary-500"} h-10 rounded-md hover:bg-secondary-600 disabled:bg-secondary-200 group w-full items-center flex justify-center
        `}
                onClick={handleClickBtn}
                disabled={props.disabled}
            >
                <div className="flex flex-row px-[13px] md:px-1 items-center">
                    {props.leftIconDefault && (
                        <>
                            <div className="mx-[3px]">{isClicked ? props.leftIconClick : props.leftIconDefault}</div>
                        </>
                    )}

                    <span className={`${isClicked ? "text-secondary-200" : "text-shades-50"} ${props.fontsWeight ?? "font-normal"} paragraphSmallMedium mx-[3px]`}>{props.name}</span>
                    {props.rightIconDefault && (
                        <>
                            <div className="mx-[3px]">{isClicked ? props.rightIconClick : props.rightIconDefault}</div>
                        </>
                    )}
                    {props.badgeText && (
                        <span className="px-2 py-0.5 rounded-3xl text-secondary-500 bg-white group-hover:text-secondary-600 mx-[3px] group-disabled:text-secondary-200">{props.badgeText}</span>
                    )}
                </div>
            </button>
        </>
    );
};

export const MediumTertiaryButton = (props) => {
    const [isClicked, setIsClicked] = useState(false);

    const handleClickBtn = () => {
        setIsClicked(props.isDefault ?? !isClicked);
    };

    return (
        <>
            <button
                className={`
          ${isClicked ? "bg-tertiary-600" : "bg-tertiary-500"} h-10 rounded-md hover:bg-tertiary-600 disabled:bg-tertiary-200 group w-full items-center flex justify-center
        `}
                onClick={handleClickBtn}
                disabled={props.disabled}
            >
                <div className="flex flex-row px-[13px] md:px-1 items-center">
                    {props.leftIconDefault && (
                        <>
                            <div className="mx-[3px]">{isClicked ? props.leftIconClick : props.leftIconDefault}</div>
                        </>
                    )}

                    <span className={`${isClicked ? "text-tertiary-200" : "text-shades-50"} ${props.fontsWeight ?? "font-normal"} paragraphSmallMedium mx-[3px]`}>{props.name}</span>
                    {props.rightIconDefault && (
                        <>
                            <div className="mx-[3px]">{isClicked ? props.rightIconClick : props.rightIconDefault}</div>
                        </>
                    )}
                    {props.badgeText && (
                        <span className="px-2 py-0.5 rounded-3xl text-tertiary-500 bg-white group-hover:text-tertiary-600 mx-[3px] group-disabled:text-tertiary-200">{props.badgeText}</span>
                    )}
                </div>
            </button>
        </>
    );
};

export const MediumDestructiveButton = (props) => {
    const [isClicked, setIsClicked] = useState(false);

    const handleClickBtn = () => {
        setIsClicked(props.isDefault ?? !isClicked);
    };

    return (
        <>
            <button
                className={`
          ${isClicked ? "bg-destructive-600" : "bg-destructive-500"} h-10 rounded-md hover:bg-destructive-600 disabled:bg-destructive-200 group w-full items-center flex justify-center
        `}
                onClick={handleClickBtn}
                disabled={props.disabled}
            >
                <div className="flex flex-row px-[13px] md:px-1 items-center">
                    {props.leftIconDefault && (
                        <>
                            <div className="mx-[3px]">{isClicked ? props.leftIconClick : props.leftIconDefault}</div>
                        </>
                    )}

                    <span className={`${isClicked ? "text-destructive-200" : "text-shades-50"} ${props.fontsWeight ?? "font-normal"} paragraphSmallMedium mx-[3px]`}>{props.name}</span>
                    {props.rightIconDefault && (
                        <>
                            <div className="mx-[3px]">{isClicked ? props.rightIconClick : props.rightIconDefault}</div>
                        </>
                    )}
                    {props.badgeText && (
                        <span className="px-2 py-0.5 rounded-3xl text-destructive-500 bg-white group-hover:text-destructive-600 mx-[3px] group-disabled:text-destructive-200">{props.badgeText}</span>
                    )}
                </div>
            </button>
        </>
    );
};

export const SmallDestructiveButton = (props) => {
    const [isClicked, setIsClicked] = useState(false);

    const handleClickBtn = () => {
        setIsClicked(props.isDefault ?? !isClicked);
    };

    return (
        <>
            <button
                className={`
          ${isClicked ? "bg-destructive-600" : "bg-destructive-500"} h-8 rounded-md hover:bg-destructive-600 disabled:bg-destructive-200 group w-full items-center flex justify-center
        `}
                onClick={handleClickBtn}
                disabled={props.disabled}
            >
                <div className="flex flex-row px-[9px] md:px-1 items-center">
                    {props.leftIconDefault && (
                        <>
                            <div className="mx-[3px]">{isClicked ? props.leftIconClick : props.leftIconDefault}</div>
                        </>
                    )}

                    <span className={`${isClicked ? "text-destructive-200" : "text-shades-50"} ${props.fontsWeight ?? "font-normal"} paragraphXSmallMedium mx-[3px]`}>{props.name}</span>
                    {props.rightIconDefault && (
                        <>
                            <div className="mx-[3px]">{isClicked ? props.rightIconClick : props.rightIconDefault}</div>
                        </>
                    )}
                </div>
            </button>
        </>
    );
};

export const SmallPrimaryButton = (props) => {
    const [isClicked, setIsClicked] = useState(false);

    const handleClickBtn = () => {
        setIsClicked(props.isDefault ?? !isClicked);
    };

    return (
        <>
            <button
                className={`
          ${isClicked ? "bg-primary-600" : "bg-primary-500"} h-8 rounded-md hover:bg-primary-600 disabled:bg-primary-200 group w-full items-center flex justify-center
        `}
                onClick={handleClickBtn}
                disabled={props.disabled}
            >
                <div className="flex flex-row px-[9px] md:px-1 items-center">
                    {props.leftIconDefault && (
                        <>
                            <div className="mx-[3px]">{isClicked ? props.leftIconClick : props.leftIconDefault}</div>
                        </>
                    )}

                    <span className={`${isClicked ? "text-primary-200" : "text-shades-50"} ${props.fontsWeight ?? "font-normal"} paragraphXSmallMedium mx-[3px]`}>{props.name}</span>
                    {props.rightIconDefault && (
                        <>
                            <div className="mx-[3px]">{isClicked ? props.rightIconClick : props.rightIconDefault}</div>
                        </>
                    )}
                </div>
            </button>
        </>
    );
};

export const SmallSecondaryButton = (props) => {
    const [isClicked, setIsClicked] = useState(false);

    const handleClickBtn = () => {
        setIsClicked(props.isDefault ?? !isClicked);
    };

    return (
        <>
            <button
                className={`
          ${isClicked ? "bg-secondary-600" : "bg-secondary-500"} h-8 rounded-md hover:bg-secondary-600 disabled:bg-secondary-200 group w-full items-center flex justify-center
        `}
                onClick={handleClickBtn}
                disabled={props.disabled}
            >
                <div className="flex flex-row px-[9px] md:px-1 items-center">
                    {props.leftIconDefault && (
                        <>
                            <div className="mx-[3px]">{isClicked ? props.leftIconClick : props.leftIconDefault}</div>
                        </>
                    )}

                    <span className={`${isClicked ? "text-secondary-200" : "text-shades-50"} ${props.fontsWeight ?? "font-normal"} paragraphXSmallMedium mx-[3px]`}>{props.name}</span>
                    {props.rightIconDefault && (
                        <>
                            <div className="mx-[3px]">{isClicked ? props.rightIconClick : props.rightIconDefault}</div>
                        </>
                    )}
                </div>
            </button>
        </>
    );
};

export const SmallTertiaryButton = (props) => {
    const [isClicked, setIsClicked] = useState(false);

    const handleClickBtn = () => {
        setIsClicked(props.isDefault ?? !isClicked);
    };

    return (
        <>
            <button
                className={`
          ${isClicked ? "bg-tertiary-600" : "bg-tertiary-500"} h-8 rounded-md hover:bg-tertiary-600 disabled:bg-tertiary-200 group w-full items-center flex justify-center
        `}
                onClick={handleClickBtn}
                disabled={props.disabled}
            >
                <div className="flex flex-row px-[9px] md:px-1 items-center">
                    {props.leftIconDefault && (
                        <>
                            <div className="mx-[3px]">{isClicked ? props.leftIconClick : props.leftIconDefault}</div>
                        </>
                    )}

                    <span className={`${isClicked ? "text-tertiary-200" : "text-shades-50"} ${props.fontsWeight ?? "font-normal"} paragraphXSmallMedium mx-[3px]`}>{props.name}</span>
                    {props.rightIconDefault && (
                        <>
                            <div className="mx-[3px]">{isClicked ? props.rightIconClick : props.rightIconDefault}</div>
                        </>
                    )}
                </div>
            </button>
        </>
    );
};
