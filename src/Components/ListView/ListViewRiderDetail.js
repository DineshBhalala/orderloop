import React, { useState } from "react";
import { ReactComponent as OrderRatingIcon } from "../../Assets/order-ratings.svg";
import { ReactComponent as DownArrow } from "../../Assets/chevron-down.svg";
import riderImage from "../../Assets/rider-image.png";

export default function ListViewRiderDetail(props) {
    const [isShowDetails, setIsShowDetails] = useState(false);

    const handleClickShowDetails = () => {
        setIsShowDetails(!isShowDetails);
    };

    return (
        <>
            <div className="w-full px-4 py-3 border border-neutral-300 rounded-md">
                <div className="flex flex-row items-center justify-between cursor-pointer" onClick={handleClickShowDetails}>
                    <div className="flex flex-row items-center">
                        <img src={riderImage} alt="" className="h-10 w-10 mr-1 rounded" />
                        <span className="paragraphSmallRegular">{props.riderName}</span>
                    </div>
                    <div className="flex flex-row items-center">
                        <div className={`flex flex-row items-center mr-1 ${isShowDetails && "hidden"}`}>
                            <OrderRatingIcon
                                height={24}
                                width={24}
                                fill={props.riderRating > 3 ? "#EBF6F5" : props.riderRating === 3 ? "#FFF5E3" : "#FEF2F2"}
                                stroke={props.riderRating > 3 ? "#3D8C82" : props.riderRating === 3 ? "#FFA704" : "#EF4444"}
                            />
                            <span className="paragraphSmallRegular ml-1">{props.orderRatting.toFixed(1)}</span>
                        </div>
                        <div className={`${isShowDetails && "rotate-180"}`}>
                            <DownArrow />
                        </div>
                    </div>
                </div>
                <div className={`${!isShowDetails && "hidden"}`}>
                    <div className="pt-2">
                        <span className="paragraphOverlineSmall text-neutral-700">MOBILE NUMBER:</span>
                        <span className="paragraphSmallRegular ml-1">(+91) 8866886688</span>
                    </div>
                    <div className="pt-1.5">
                        <span className="paragraphOverlineSmall text-neutral-700">ORDERS DELIVERED:</span>
                        <span className="paragraphSmallRegular ml-1">18 Nov 2022</span>
                    </div>
                    <div className="pt-1.5">
                        <span className="paragraphOverlineSmall text-neutral-700">DELIVERY RADIUS:</span>
                        <span className="paragraphSmallRegular ml-1">15 kms</span>
                    </div>
                    <div className="pt-1.5">
                        <span className="paragraphOverlineSmall text-neutral-700">DELIVERY RATE:</span>
                        <span className="paragraphSmallRegular ml-1">15 kms</span>
                    </div>
                    <div className="pt-1.5">
                        <span className="paragraphOverlineSmall text-neutral-700">JOINING DATE:</span>
                        <span className="paragraphSmallRegular ml-1">15 kms</span>
                    </div>
                    <div className="pt-1.5">
                        <span className="paragraphOverlineSmall text-neutral-700">LAST ORDER:</span>
                        <span className="paragraphSmallRegular ml-1">15 kms</span>
                    </div>
                    <div className="pt-1.5">
                        <div className="flex flex-row items-center">
                            <span className="paragraphOverlineSmall text-neutral-700">RIDER AVG. RATING:</span>
                            <span className="paragraphSmallRegular ml-1">
                                <div className="flex flex-row items-center">
                                    <OrderRatingIcon
                                        height={20}
                                        width={20}
                                        fill={props.riderRating > 3 ? "#EBF6F5" : props.riderRating === 3 ? "#FFF5E3" : "#FEF2F2"}
                                        stroke={props.riderRating > 3 ? "#3D8C82" : props.riderRating === 3 ? "#FFA704" : "#EF4444"}
                                    />
                                    <span className="paragraphSmallRegular ml-1">{props.orderRatting.toFixed(1)}</span>
                                </div>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
