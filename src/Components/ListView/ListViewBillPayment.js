import React, { useState } from "react";
import { ReactComponent as CashIcon } from "../../Assets/cash.svg";
import { ReactComponent as DownArrow } from "../../Assets/chevron-down.svg";
import { ReactComponent as UpiIcon } from "../../Assets/UPI.svg";
import { ReactComponent as CardIcon } from "../../Assets/card.svg";

export default function ListViewBillPayment(props) {
  const [isShowDetails, setIsShowDetails] = useState(false);

  const handleClickShowDetails = () => {
    setIsShowDetails(!isShowDetails);
  };

  return (
    <>
      <div className="w-full px-4 py-3 border border-neutral-300 rounded-md">
        <div className="flex flex-row items-center justify-between cursor-pointer" onClick={handleClickShowDetails}>
          <div>
            <h3 className="paragraphOverlineSmall text-neutral-700">CODE:</h3>
            <span className="paragraphSmallRegular">{props.code}</span>
          </div>
          <div className="flex flex-row items-center">
            <div className={`flex flex-row items-center mr-1 ${isShowDetails && "hidden"}`}>
              {props.paymentStatus === "Paid" ? (
                <span className="text-tertiary-800 paragraphSmallMedium border px-2 py-1 border-tertiary-800 rounded bg-tertiary-100">Paid</span>
              ) : (
                <span className="text-destructive-600 paragraphSmallMedium border px-2 py-1 border-destructive-600 rounded bg-destructive-100">Unpaid</span>
              )}
            </div>
            <div className={`${isShowDetails && "rotate-180"}`}>
              <DownArrow />
            </div>
          </div>
        </div>
        <div className={`${!isShowDetails && "hidden"}`}>
          <div className="mt-1.5">
            <span className="paragraphOverlineSmall text-neutral-700">CUSTOMER NAME:</span>
            <span className="paragraphSmallRegular ml-1">{props.customerName}</span>
          </div>
          <div className="mt-1.5">
            <span className="paragraphOverlineSmall text-neutral-700">ORDER AMOUNT:</span>
            <span className="paragraphSmallRegular ml-1">{props.orderAmount}</span>
          </div>
          <div className="mt-1.5">
            <span className="paragraphOverlineSmall text-neutral-700">ORDER DATE:</span>
            <span className="paragraphSmallRegular ml-1">{props.orderDate}</span>
          </div>
          <div className="mt-1.5 flex flex-row items-center">
            <span className="paragraphOverlineSmall text-neutral-700">PAYMENT MODE:</span>

            <div className="flex flex-row items-center ml-1">{props.paymentMode === "Cash" ? <CashIcon /> : props.paymentMode === "UPI" ? <UpiIcon /> : <CardIcon />}</div>
            <span className="paragraphSmallRegular ml-1">{props.paymentMode}</span>
          </div>
          <div className="mt-2.5 flex flex-row justify-between items-center mobile:block">
            <div className="flex flex-row items-center">
              <span className="paragraphOverlineSmall text-neutral-700">PAYMENT STATUS:</span>
              <span className="paragraphSmallRegular ml-1">
                {props.paymentStatus === "Paid" ? (
                  <div className="text-tertiary-800 paragraphSmallMedium h-7 border px-2 py-1 border-tertiary-800 rounded bg-tertiary-100">Paid</div>
                ) : (
                  <div className="text-destructive-600 paragraphSmallMedium border px-2 py-1 border-destructive-600 rounded bg-destructive-100">Unpaid</div>
                )}
              </span>
            </div>
            <a className="paragraphSmallUnderline text-primary-500 cursor-pointer" href="/" onClick={() => props.handleClickViewDetails()}>
              View details
            </a>
          </div>
        </div>
      </div>
    </>
  );
}
