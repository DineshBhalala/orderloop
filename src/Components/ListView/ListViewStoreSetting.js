import React, { useState } from "react";
import ToggleSwitch from "../ToggleSwitch/ToggleSwitch";
import { ReactComponent as DownArrow } from "../../Assets/chevron-down.svg";

export const ListViewOutletOrderingMode = (props) => {
  const [showDetails, setShowDetails] = useState(false);

  const handleClickDownArrow = () => {
    setShowDetails(!showDetails);
  };

  return (
    <>
      <div className="px-4 py-3 border border-neutral-300 rounded-md">
        <div className="flex flex-row items-center justify-between">
          <div className="flex flex-row items-center">
            <ToggleSwitch enable={true} />
            <div className="flex flex-col ml-4">
              <span className="paragraphOverlineSmall mb-1">MODE NAME:</span>
              <span className="paragraphSmallRegular">{props.modeName}</span>
            </div>
          </div>
          <div className="cursor-pointer" onClick={handleClickDownArrow}>
            <DownArrow className={`${showDetails && "rotate-180"}`} />
          </div>
        </div>
        <div className={`${!showDetails && "hidden"} mt-1.5 ml-[56px]`}>
          <div className="mb-[5px]">
            <span className="paragraphOverlineSmall text-neutral-700">ORDER TYPE:</span>
            <span className="paragraphSmallRegular ml-1">{props.orderType}</span>
          </div>
          <div className="">
            <span className="paragraphOverlineSmall text-neutral-700">ORDER TAB:</span>
            <span className="paragraphSmallRegular ml-1">{props.orderTab}</span>
          </div>
        </div>
      </div>
    </>
  );
};

export const ListViewAppearance = (props) => {
  return (
    <>
      <div className="px-4 py-3 border flex flex-col border-neutral-300 rounded-md">
        <span className="paragraphOverlineSmall mb-1">{props.categoryDisplayCount ? "CATEGORY DISPLAY COUNT:" : "SECTION NAME:"}</span>
        <span className="paragraphSmallRegular">{props.categoryDisplayCount ?? props.sectionName}</span>
      </div>
    </>
  );
};

export const ListViewPaymentMethod = (props) => {
  return (
    <>
      <div className="px-4 py-3 border border-neutral-300 rounded-md">
        <div className="flex flex-row items-center">
          <ToggleSwitch enable={true} />
          <div className="flex flex-col ml-4">
            <span className="paragraphOverlineSmall mb-1">MODE NAME:</span>
            <span className="paragraphSmallRegular">{props.modeName}</span>
          </div>
        </div>
      </div>
    </>
  );
};
