import React, { useState } from "react";
import { ReactComponent as DownArrow } from "../../Assets/chevron-down.svg";
import { ReactComponent as LinkIcon } from "../../Assets/link.svg";

export default function UltimateListViewFOrAllCustomerPage(props) {
  const [isShowDetails, setIsShowDetails] = useState(false);

  const handleClickShowDetails = () => {
    setIsShowDetails(!isShowDetails);
  };

  const detailsArray = Object.entries(props.details);

  return (
    <>
      <div className="w-full px-4 py-2.5 border border-neutral-300 rounded-md">
        <div
          className="flex flex-row items-center justify-between cursor-pointer"
          onClick={handleClickShowDetails}
        >
          <div>
            <h3 className="paragraphOverlineSmall text-neutral-700 mb-1">
              {props.title}
            </h3>
            <span className="paragraphSmallRegular">{props.titleValue}</span>
          </div>
          <div className="flex flex-row items-center">
            <div className={`${isShowDetails && "rotate-180"}`}>
              <DownArrow />
            </div>
          </div>
        </div>

        <div className={`${!isShowDetails && "hidden"} relative`}>
          {detailsArray.map(
            ([key, value]) =>
              key.replace(/([a-z])([A-Z])/g, "$1 $2").toUpperCase() !==
                props.title && (
                <div className="mt-[5px] flex flex-row items-center" key={key}>
                  <span className="paragraphOverlineSmall text-neutral-700">
                    {key.replace(/([a-z])([A-Z])/g, "$1 $2").toUpperCase() !==
                      props.title &&
                      key.replace(/([a-z])([A-Z])/g, "$1 $2").toUpperCase()}
                    :
                  </span>

                  {key.replace(/([a-z])([A-Z])/g, "$1 $2").toUpperCase() ===
                  "LINK OFFERS" ? (
                    <div className="paragraphSmallRegular ml-1 items-center flex flex-row">
                      <LinkIcon stroke="#6C5DD3" className="h-5 w-5" />
                      <span className="ml-1 text-primary-500">{value}</span>
                    </div>
                  ) : (
                    <span className="paragraphSmallRegular ml-1">{value}</span>
                  )}
                </div>
              )
          )}
          <div
            className={`absolute bottom-0 right-0 mobile:relative mobile:left-0 ${
              !props.viewDetails && "hidden"
            }`}
          >
            <span
              className="paragraphSmallUnderline text-primary-500 cursor-pointer"
              onClick={() => props.viewDetails()}
            >
              View full details
            </span>
          </div>
        </div>
      </div>
    </>
  );
}
