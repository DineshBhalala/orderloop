import React, { useState } from "react";
import { ReactComponent as DownArrow } from "../../Assets/chevron-down.svg";
import { ReactComponent as LinkIcon } from "../../Assets/link.svg";
import ToggleSwitch from "../ToggleSwitch/ToggleSwitch";

export default function ListViewOffer(props) {
  const [isShowDetails, setIsShowDetails] = useState(false);

  const handleClickShowDetails = () => {
    setIsShowDetails(!isShowDetails);
  };

  return (
    <div className="w-full px-4 py-2.5 border border-neutral-300 rounded-md">
      <div className="flex flex-row items-center justify-between">
        <div className="flex flex-row items-center">
          {props.linkOffer && (
            <div className="mr-4">
              <ToggleSwitch />
            </div>
          )}
          <div>
            <h3 className="paragraphOverlineSmall text-neutral-700 mb-1 flex">COUPON CODE:</h3>
            <span className="paragraphSmallRegular flex">{props.couponCode}</span>
          </div>
        </div>
        <div className="flex flex-row items-center">
          <div className={`${isShowDetails && "rotate-180"} cursor-pointer`} onClick={handleClickShowDetails}>
            <DownArrow />
          </div>
        </div>
      </div>
      <div className={`${!isShowDetails && "hidden"} mt-[5px] ${props.linkOffer && "ml-14"}`}>
        <div className="mb-[7px]">
          <span className="paragraphOverlineSmall text-neutral-700">OFFER TYPE:</span>
          <span className="paragraphSmallRegular ml-1">{props.offerType}</span>
        </div>
        <div className="mb-[7px]">
          <span className="paragraphOverlineSmall text-neutral-700">TITLE:</span>
          <span className="paragraphSmallRegular ml-1">{props.title}</span>
        </div>
        <div className="mb-[7px] flex mobile:block">
          <span className="paragraphOverlineSmall text-neutral-700">DESCRIPTION:</span>
          <div className="paragraphSmallRegular ml-1 mobile:ml-0">{props.description}</div>
        </div>

        <div className="flex flex-row justify-between items-center mobile:block">
          <div className="flex flex-row items-center mobile:mb-1">
            <span className="paragraphOverlineSmall text-neutral-700">LINKED OFFERS:</span>
            <div className="paragraphSmallRegular ml-1 flex flex-row items-center text-primary-500">
              <LinkIcon className="h-5 w-5 mr-1" stroke="#6C5DD3" />
              {props.linkOffer ?? "Unlink"}
            </div>
          </div>
          {props.linkOffer && (
            <span className="paragraphSmallUnderline text-primary-500 cursor-pointer" onClick={() => props.handleClickViewDetails()}>
              View details
            </span>
          )}
        </div>
      </div>
    </div>
  );
}
