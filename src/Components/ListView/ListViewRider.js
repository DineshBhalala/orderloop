import React, { useState } from "react";
import { ReactComponent as OrderRatingIcon } from "../../Assets/order-ratings.svg";
import { ReactComponent as DownArrow } from "../../Assets/chevron-down.svg";

export default function ListViewRider(props) {
    const [isShowDetails, setIsShowDetails] = useState(false);

    const handleClickShowDetails = () => {
        setIsShowDetails(!isShowDetails);
    };

    return (
        <>
            <div className="w-full px-4 py-3 border border-neutral-300 rounded-md">
                <div className="flex flex-row items-center justify-between cursor-pointer" onClick={handleClickShowDetails}>
                    <div>
                        <h3 className="paragraphOverlineSmall text-neutral-700 mb-0.5">RIDER NAME:</h3>
                        <span className="paragraphSmallRegular">{props.riderName}</span>
                    </div>
                    <div className="flex flex-row items-center">
                        <div className={`flex flex-row items-center mr-1 ${isShowDetails && "hidden"}`}>
                            <OrderRatingIcon
                                height={20}
                                width={20}
                                fill={props.riderRating > 3 ? "#EBF6F5" : props.riderRating === 3 ? "#FFF5E3" : "#FEF2F2"}
                                stroke={props.riderRating > 3 ? "#3D8C82" : props.riderRating === 3 ? "#FFA704" : "#EF4444"}
                            />
                            <span className="paragraphSmallRegular ml-1">{props.riderRating.toFixed(1)}</span>
                        </div>
                        <div className={`${isShowDetails && "rotate-180"}`}>
                            <DownArrow />
                        </div>
                    </div>
                </div>

                <div className={`${!isShowDetails && "hidden"}`}>
                    <div className="pt-0.5">
                        <span className="paragraphOverlineSmall text-neutral-700">MOBILE NUMBER:</span>
                        <span className="paragraphSmallRegular ml-1">(+91) 8866886688</span>
                    </div>
                    <div className="pt-1">
                        <span className="paragraphOverlineSmall text-neutral-700">JOIN DATE:</span>
                        <span className="paragraphSmallRegular ml-1">18 Nov 2022</span>
                    </div>
                    <div className="pt-1">
                        <span className="paragraphOverlineSmall text-neutral-700">DELIVERY RADIUS:</span>
                        <span className="paragraphSmallRegular ml-1">15 kms</span>
                    </div>
                    <div className="pt-1.5 flex flex-row justify-between mobile:block">
                        <div className="flex flex-row">
                            <span className="paragraphOverlineSmall text-neutral-700">AVG. RIDER RATING:</span>
                            <span className="paragraphSmallRegular ml-1">
                                <div className="flex flex-row items-center">
                                    <OrderRatingIcon
                                        className="h-5 w-5 mr-1"
                                        fill={props.riderRating > 3 ? "#EBF6F5" : props.riderRating === 3 ? "#FFF5E3" : "#FEF2F2"}
                                        stroke={props.riderRating > 3 ? "#3D8C82" : props.riderRating === 3 ? "#FFA704" : "#EF4444"}
                                    />
                                    <span className="paragraphSmallRegular">{props.riderRating.toFixed(1)}</span>
                                </div>
                            </span>
                        </div>
                        <span className="paragraphSmallUnderline text-primary-500 cursor-pointer" onClick={props.handleClickViewDetail}>
                            View details
                        </span>
                    </div>
                </div>
            </div>
        </>
    );
}
