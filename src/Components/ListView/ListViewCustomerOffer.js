import React, { useState } from "react";
import ToggleSwitch from "../ToggleSwitch/ToggleSwitch";
import { ReactComponent as DownArrow } from "../../Assets/chevron-down.svg";

export default function ListViewCustomerOffer(props) {
    const [showDetails, setShowDetails] = useState(false);

    const handleClickDownArrow = () => {
        setShowDetails(!showDetails);
    };

    return (
        <>
            <div className="px-4 py-3 border border-neutral-300 rounded-md">
                <div className="flex flex-row items-center justify-between">
                    <div className="flex flex-row items-center">
                        <ToggleSwitch enable={true} />
                        <div className="flex flex-col ml-4">
                            <span className="paragraphOverlineSmall mb-1">COUPON CODE:</span>
                            <span className="paragraphSmallRegular">{props.couponCode}</span>
                        </div>
                    </div>
                    <div className="cursor-pointer" onClick={handleClickDownArrow}>
                        <DownArrow className={`${showDetails && "rotate-180"}`} />
                    </div>
                </div>
                <div className={`${!showDetails && "hidden"} mt-1.5 ml-[56px]`}>
                    <div className="mb-[5px]">
                        <span className="paragraphOverlineSmall text-neutral-700">OFFER TYPE:</span>
                        <span className="paragraphSmallRegular ml-1">{props.offerType}</span>
                    </div>
                    <div className="mb-[5px]">
                        <span className="paragraphOverlineSmall text-neutral-700">TITLE:</span>
                        <span className="paragraphSmallRegular ml-1">{props.title}</span>
                    </div>
                    <div className="mb-[5px] flex flex-row">
                        <span className="paragraphOverlineSmall text-neutral-700">DESCRIPTION:</span>
                        <div className="paragraphSmallRegular ml-1">{props.description}</div>
                    </div>
                </div>
            </div>
        </>
    );
}
