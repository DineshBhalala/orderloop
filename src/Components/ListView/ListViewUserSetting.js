import React, { useState } from "react";
import { ReactComponent as DownArrow } from "../../Assets/chevron-down.svg";
import { ReactComponent as LinkIcon } from "../../Assets/link.svg";
import ToggleSwitch from "../ToggleSwitch/ToggleSwitch";

export const ListViewUserSettingUserList = (props) => {
  const [isShowDetails, setIsShowDetails] = useState(false);

  const handleClickShowDetails = () => {
    setIsShowDetails(!isShowDetails);
  };

  return (
    <>
      <div className="w-full px-4 py-3 border border-neutral-300 rounded-md">
        <div className="flex flex-row items-center justify-between cursor-pointer" onClick={handleClickShowDetails}>
          <div>
            <h3 className="paragraphOverlineSmall text-neutral-700 mb-1">USER NAME:</h3>
            <span className="paragraphSmallRegular">{props.userName}</span>
          </div>
          <div className="flex flex-row items-center">
            <div className={`${isShowDetails && "rotate-180"}`}>
              <DownArrow />
            </div>
          </div>
        </div>
        <div className={`${!isShowDetails && "hidden"}`}>
          <div className="pt-2">
            <span className="paragraphOverlineSmall text-neutral-700">USER ROLE:</span>
            <span className="paragraphSmallRegular ml-1">{props.userRole}</span>
          </div>
          <div className="pt-2">
            <span className="paragraphOverlineSmall text-neutral-700">MOBILE NUMBER:</span>
            <span className="paragraphSmallRegular ml-1">{props.mobileNumber}</span>
          </div>
          <div className="pt-2">
            <span className="paragraphOverlineSmall text-neutral-700">EMAIL ID:</span>
            <span className="paragraphSmallRegular ml-1">{props.emailId}</span>
          </div>
          <div className="pt-2 flex flex-row items-center">
            <span className="paragraphOverlineSmall text-neutral-700">OUTLET PERMISSION:</span>
            <div className="paragraphSmallRegular ml-1 items-center flex flex-row">
              <LinkIcon stroke="#6C5DD3" className="h-5 w-5" />
              <span className="ml-1 text-primary-500">{props.outletPermission}</span>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export const ListViewUserSettingPresetList = (props) => {
  const [isShowDetails, setIsShowDetails] = useState(false);

  const handleClickShowDetails = () => {
    setIsShowDetails(!isShowDetails);
  };

  return (
    <>
      <div className="w-full px-4 py-3 border border-neutral-300 rounded-md">
        <div className="flex flex-row items-center justify-between cursor-pointer" onClick={handleClickShowDetails}>
          <div>
            <h3 className="paragraphOverlineSmall text-neutral-700 mb-1">PRESET NAME:</h3>
            <span className="paragraphSmallRegular">{props.presetName}</span>
          </div>
          <div className="flex flex-row items-center">
            <div className={`${isShowDetails && "rotate-180"}`}>
              <DownArrow />
            </div>
          </div>
        </div>
        <div className={`${!isShowDetails && "hidden"}`}>
          <div className="pt-2">
            <span className="paragraphOverlineSmall text-neutral-700">LINKED USERS:</span>
            <span className="paragraphSmallRegular ml-1">{props.userRole}</span>
          </div>
          <div className="pt-2">
            <span className="paragraphOverlineSmall text-neutral-700">CREATED BY:</span>
            <span className="paragraphSmallRegular ml-1">{props.createBy}</span>
          </div>
          <div className="pt-2">
            <span className="paragraphOverlineSmall text-neutral-700">CREATOR NUMBER:</span>
            <span className="paragraphSmallRegular ml-1">{props.creatorNumber}</span>
          </div>
          <div className="pt-2">
            <span className="paragraphOverlineSmall text-neutral-700">CREATED ON:</span>
            <span className="paragraphSmallRegular ml-1">{props.createdOn}</span>
          </div>
        </div>
      </div>
    </>
  );
};

export const ListViewOutletPermission = (props) => {
  const [showDetails, setShowDetails] = useState(false);

  const handleClickDownArrow = () => {
    setShowDetails(!showDetails);
  };

  return (
    <>
      <div className="px-4 py-3 border border-neutral-300 rounded-md">
        <div className="flex flex-row items-center justify-between">
          <div className="flex flex-row items-center">
            <ToggleSwitch enable={true} />
            <div className="flex flex-col ml-4">
              <span className="paragraphOverlineSmall mb-1">OUTLET NAME:</span>
              <span className="paragraphSmallRegular">{props.outletName}</span>
            </div>
          </div>
          <div className="cursor-pointer" onClick={handleClickDownArrow}>
            <DownArrow className={`${showDetails && "rotate-180"}`} />
          </div>
        </div>
        <div className={`${!showDetails && "hidden"} mt-1.5 ml-[56px]`}>
          <div className="mb-[5px]">
            <span className="paragraphOverlineSmall text-neutral-700">STATE:</span>
            <span className="paragraphSmallRegular ml-1">{props.state}</span>
          </div>
          <div className="mb-[5px]">
            <span className="paragraphOverlineSmall text-neutral-700">CITY:</span>
            <span className="paragraphSmallRegular ml-1">{props.city}</span>
          </div>
          <div className="mb-[5px]">
            <span className="paragraphOverlineSmall text-neutral-700">ADDRESS:</span>
            <span className="paragraphSmallRegular ml-1">{props.address}</span>
          </div>
        </div>
      </div>
    </>
  );
};
