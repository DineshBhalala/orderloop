import React, { useState } from "react";
import { ReactComponent as DownArrow } from "../../Assets/chevron-down.svg";
import { ReactComponent as DineInIcon } from "../../Assets/dine-in.svg";
import { ReactComponent as TakeAwayIcon } from "../../Assets/orders.svg";
import { ReactComponent as DeliveryIcon } from "../../Assets/riders.svg";
import { ReactComponent as SuccessTickIcon } from "../../Assets/success-tick.svg";
import { ReactComponent as CancelIcon } from "../../Assets/cancel.svg";
import { ReactComponent as UPIIcon } from "../../Assets/UPI.svg";
import { ReactComponent as CashIcon } from "../../Assets/cash.svg";
import { ReactComponent as CardIcon } from "../../Assets/card.svg";

export default function ListViewCustomerOrder(props) {
  const [isShowDetails, setIsShowDetails] = useState(false);

  const handleClickShowDetails = () => {
    setIsShowDetails(!isShowDetails);
  };

  return (
    <>
      <div className="w-full px-4 py-2.5 border border-neutral-300 rounded-md">
        <div
          className="flex flex-row items-center justify-between cursor-pointer"
          onClick={handleClickShowDetails}
        >
          <div>
            <h3 className="paragraphOverlineSmall text-neutral-700 mb-1">
              ORDER BILL NAME:
            </h3>
            <span className="paragraphSmallRegular">{props.billName}</span>
          </div>
          <div className="flex flex-row items-center">
            <div className={`${isShowDetails && "rotate-180"}`}>
              <DownArrow />
            </div>
          </div>
        </div>

        <div className={`${!isShowDetails && "hidden"} mt-[5px]`}>
          <div className="mb-[5px]">
            <span className="paragraphOverlineSmall text-neutral-700">
              ORDER DATE:
            </span>
            <span className="paragraphSmallRegular ml-1">
              {props.orderDate}
            </span>
          </div>
          <div className="mb-[5px] flex flex-row items-center">
            <span className="paragraphOverlineSmall text-neutral-700 mr-1">
              ORDER TYPE:
            </span>
            <div className="flex flex-row items-center">
              {props.orderType === "Dine-In" ? (
                <DineInIcon />
              ) : props.orderType === "Takeaway" ? (
                <TakeAwayIcon />
              ) : (
                <DeliveryIcon />
              )}
              <span className="ml-1">{props.orderType}</span>
            </div>
          </div>
          <div className="mb-[5px] flex flex-row items-center">
            <span className="paragraphOverlineSmall text-neutral-700 mr-1">
              ORDER STATUS:
            </span>
            <div className="flex flex-row items-center paragraphSmallRegular ml-1">
              {props.orderStatus === "Delivered" ? (
                <SuccessTickIcon />
              ) : (
                <CancelIcon />
              )}
              <span className="ml-1">{props.orderStatus}</span>
            </div>
          </div>
          <div className="mb-[5px]">
            <span className="paragraphOverlineSmall text-neutral-700">
              ITEMS ORDERED:
            </span>
            <span className="paragraphSmallRegular ml-1">
              {props.itemsOrdered}
            </span>
          </div>
          <div className="flex flex-row justify-between items-center mobile:block">
            <div className="flex flex-row items-center">
              <span className="paragraphOverlineSmall text-neutral-700 mr-1">
                PAYMENT MODE:
              </span>

              <div className="flex flex-row items-center">
                {props.paymentMode === "UPI" ? (
                  <UPIIcon className="w-5 h-5" />
                ) : props.paymentMode === "Cash" ? (
                  <CashIcon className="w-5 h-5" />
                ) : (
                  <CardIcon className="w-5 h-5" />
                )}
                <span className="ml-1"> {props.paymentMode}</span>
              </div>
            </div>
            <span
              className="paragraphSmallUnderline text-primary-500 cursor-pointer"
              onClick={() => props.viewDetails()}
            >
              View details
            </span>
          </div>
        </div>
      </div>
    </>
  );
}
