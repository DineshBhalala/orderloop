import React, { useState } from "react";
import { ReactComponent as OrderRatingIcon } from "../../Assets/order-ratings.svg";
import { ReactComponent as DownArrow } from "../../Assets/chevron-down.svg";
import { ReactComponent as TimerIcon } from "../../Assets/timer.svg";

export default function ListViewRiderDetail(props) {
    const [isShowDetails, setIsShowDetails] = useState(false);

    const handleClickShowDetails = () => {
        setIsShowDetails(!isShowDetails);
    };

    return (
        <>
            <div className="w-full px-4 py-3 border border-neutral-300 rounded-md">
                <div className="flex flex-row items-center justify-between">
                    <div>
                        <h3 className="paragraphOverlineSmall text-neutral-700 mb-1">BILL NAME:</h3>
                        <span className="paragraphSmallRegular">{props.billName}</span>
                    </div>
                    <div className="flex flex-row items-center">
                        <div className={`flex flex-row items-center mr-1 ${isShowDetails && "hidden"}`}>
                            <OrderRatingIcon
                                className="h-6 w-6 mr-1.5"
                                fill={props.riderRating > 3 ? "#EBF6F5" : props.riderRating === 3 ? "#FFF5E3" : "#FEF2F2"}
                                stroke={props.riderRating > 3 ? "#3D8C82" : props.riderRating === 3 ? "#FFA704" : "#EF4444"}
                            />
                            <span className="paragraphSmallRegular ml-1">{props.riderRating.toFixed(1)}</span>
                        </div>
                        <div className={`${isShowDetails && "rotate-180"} cursor-pointer`}>
                            <DownArrow onClick={handleClickShowDetails} />
                        </div>
                    </div>
                </div>
                <div className={`${!isShowDetails && "hidden"}`}>
                    <div className="pt-2">
                        <span className="paragraphOverlineSmall text-neutral-700">ORDER ID:</span>
                        <span className="paragraphSmallRegular ml-1">{props.orderId}</span>
                    </div>
                    <div className="pt-2">
                        <span className="paragraphOverlineSmall text-neutral-700">ORDER DATE:</span>
                        <span className="paragraphSmallRegular ml-1">{props.orderDate}</span>
                    </div>
                    <div className="pt-2">
                        <span className="paragraphOverlineSmall text-neutral-700">ORDER AMOUNT:</span>
                        <span className="paragraphSmallRegular ml-1">{props.orderAmount}</span>
                    </div>
                    <div className="pt-2">
                        <span className="paragraphOverlineSmall text-neutral-700">DELIVERY DISTANCE:</span>
                        <span className="paragraphSmallRegular ml-1">{props.deliveryDistance}</span>
                    </div>
                    <div className="pt-2">
                        <span className="paragraphOverlineSmall text-neutral-700">DELIVERY COST:</span>
                        <span className="paragraphSmallRegular ml-1">{props.deliveryCost}</span>
                    </div>
                    <div className="pt-2">
                        <div className="flex flex-row">
                            <span className="paragraphOverlineSmall text-neutral-700">RIDER RATING:</span>
                            <span className="paragraphSmallRegular ml-1">
                                <div className="flex flex-row items-center">
                                    <OrderRatingIcon
                                        className="h-5 w-5 mr-1"
                                        fill={props.riderRating > 3 ? "#EBF6F5" : props.riderRating === 3 ? "#FFF5E3" : "#FEF2F2"}
                                        stroke={props.riderRating > 3 ? "#3D8C82" : props.riderRating === 3 ? "#FFA704" : "#EF4444"}
                                    />
                                    <span className="paragraphSmallRegular ml-1">{props.riderRating.toFixed(1)}</span>
                                </div>
                            </span>
                        </div>
                    </div>
                    <div className="pt-2 flex flex-row justify-between items-center mobile:block">
                        <div className="flex flex-row items-center">
                            <span className="paragraphOverlineSmall text-neutral-700">DELIVERY TIME:</span>
                            <div className="paragraphSmallRegular ml-1 flex flex-row items-center">
                                <TimerIcon className="h-5 w-5 mr-1.5" />
                                {props.deliveryTime}
                            </div>
                        </div>
                        <span className="paragraphSmallUnderline text-primary-500 cursor-pointer" onClick={() => props.handleClickTotalSalesCard()}>
                            View full details
                        </span>
                    </div>
                </div>
            </div>
        </>
    );
}
