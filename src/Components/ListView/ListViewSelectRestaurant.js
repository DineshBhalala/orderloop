import React, { useState } from "react";
import ToggleSwitch from "../ToggleSwitch/ToggleSwitch";
import { ReactComponent as DominosLogo } from "../../Assets/dominos-logo.svg";
import { ReactComponent as DropDownIcon } from "../../Assets/chevron-down.svg";
import ChangeStatusPopup from "../../Screen/SelectRestaurant/Components/ChangeStatusPopup";

export default function ListViewSelectRestaurant(props) {
    const [showDetails, setShowDetails] = useState(false);

    const handleClickDropDown = () => {
        setShowDetails(!showDetails);
    };

    const [showChangeStatusPopup, setShowChangeStatusPopup] = useState(false);

    const handleClickChangeOrderStatus = () => {
        setShowChangeStatusPopup(!showChangeStatusPopup);
    };

    return (
        <>
            <div className="relative">
                <div className={`border border-neutral-300 rounded-md px-4 py-3 ${showDetails ? "h-[228px]" : "h-[66px]"}`}>
                    <div className="flex flex-row justify-between items-center">
                        <div className="flex flex-row items-center ml-[65px] mobile:ml-12">
                            <span>
                                <DominosLogo className="mobile:hidden" height={40} width={40} />
                            </span>
                            <span className="paragraphSmallSemiBold mx-2">{props.name}</span>
                        </div>

                        <DropDownIcon className={`${showDetails && "rotate-180"} cursor-pointer`} onClick={handleClickDropDown} />
                    </div>

                    <div className={`${!showDetails && "hidden"} mt-4`}>
                        <div className="flex flex-row mb-4 h-5">
                            <div className="w-1/2">
                                <span className="text-neutral-400 font-semibold text-sm leading-5">State:</span>
                                <span className="ml-1 font-normal text-sm leading-5 ">All</span>
                            </div>

                            <div className="w-1/2">
                                <span className="text-neutral-400 font-semibold text-sm leading-5">City:</span>
                                <span className="ml-1 font-normal text-sm leading-5 ">All</span>
                            </div>
                        </div>

                        <div className="flex flex-row mb-4 h-10">
                            <span className="text-neutral-400 font-semibold text-sm leading-5">Address:</span>
                            <p className="h-10 ml-1 font-normal text-sm leading-5 ">Shyamal Infinity, beneath Radio Mirchi, kalawad road</p>
                        </div>
                    </div>
                </div>

                <div className={`absolute ${!showDetails && "hidden"} w-full`}>
                    <div className="absolute flex flex-row bottom-12 left-4">
                        <div className="paragraphSmallSemiBold text-neutral-400">Dine-in:</div>
                        <div className="font-normal text-sm leading-5 ml-1 text-destructive-500 cursor-default" onClick={handleClickChangeOrderStatus}>
                            INACTIVE
                        </div>
                    </div>

                    <div className="absolute flex flex-row bottom-12 left-[161px] lg:left-1/2">
                        <div className="paragraphSmallSemiBold text-neutral-400">Takeaway:</div>
                        <div className="font-normal text-sm leading-5 ml-1 text-destructive-500 cursor-default" onClick={handleClickChangeOrderStatus}>
                            INACTIVE
                        </div>
                    </div>

                    <div className="absolute flex flex-row bottom-3 left-4">
                        <div className="paragraphSmallSemiBold text-neutral-400">QSR:</div>
                        <div className="font-normal text-sm leading-5 ml-1 text-destructive-500 cursor-default" onClick={handleClickChangeOrderStatus}>
                            INACTIVE
                        </div>
                    </div>

                    <div className="absolute flex flex-row bottom-3 left-[161px] lg:left-1/2">
                        <div className="paragraphSmallSemiBold text-neutral-400">Delivery:</div>
                        <div className="font-normal text-sm leading-5 ml-1 text-destructive-500 cursor-default" onClick={handleClickChangeOrderStatus}>
                            INACTIVE
                        </div>
                    </div>
                </div>

                <div className="absolute top-5 left-4">
                    <ToggleSwitch />
                </div>
            </div>
            <div className={`${!showChangeStatusPopup && "hidden"}`}>
                <ChangeStatusPopup handleClickClose={handleClickChangeOrderStatus} />
            </div>
        </>
    );
}
