import React, { useState } from "react";
import DominosLogo from "../../Assets/DominosLogo.svg";
import DownArrow from "../../Assets/chevron-down.svg";
import { Switch } from "../FormControl/FormControls";
import ToggleSwitch from "../ToggleSwitch/ToggleSwitch";

export default function ListViewOutlet(props) {
  const [isShownList, setIsShowList] = useState(false);
  const [isShownCard, setIsShowCard] = useState("hidden");
  const handleClick = () => {
      setIsShowList(!isShownList);
      setIsShowCard(!isShownCard);
  };

  return (
    <>
      <div className="my-2">
        <div
          className={`px-4 py-3 h-16 flex flex-row justify-between ${!isShownList&&"hidden"} items-center border border-neutral-300 rounded-md`}
        >
          <div className="flex flex-row items-center">
            <ToggleSwitch />
            <img
              src={DominosLogo}
              alt="Logo"
              className="h-10 w-10 mr-2 ml-[25px]"
            />
            <span className="paragraphSmallSemiBold mr-2">{props.name}</span>
          </div>
          <img onClick={handleClick} src={DownArrow} alt="Logo" className="h-6 w-6 cursor-pointer" />
        </div>

        <div
          className={`bg-white rounded-md border px-4 py-3 border-neutral-300 w-[329px] h-[228px] mr-5 md:mr-[9px] md:w-full mobile:w-[343px] ${!isShownCard&&"hidden"}`}
        >
          <div className="justify-between items-center mb-4 flex-row">
            <div className="flex flex-row justify-between items-center">
              <div className="flex flex-row items-center">
                <img src={DominosLogo} alt="Logo" className="h-10 w-10" />
                <div className="h-10 mr-8">
                  <h2 className="text-sm font-semibold ml-2 leading-5">
                    {props.name}
                  </h2>
                </div>
              </div>

              <img onClick={handleClick} src={DownArrow} alt="Logo" className="h-6 w-6 rotate-180 cursor-pointer" />
            </div>
          </div>

          <div className="flex flex-row mb-4 h-5">
            <div className="w-1/2">
              <span className="text-neutral-400 font-semibold text-sm leading-5">
                State:
              </span>
              <span className="ml-1 font-normal text-sm leading-5 ">All</span>
            </div>
            <div className="w-1/2">
              <span className="text-neutral-400 font-semibold text-sm leading-5">
                City:
              </span>
              <span className="ml-1 font-normal text-sm leading-5 ">All</span>
            </div>
          </div>

          <div className="flex flex-row mb-4 h-10">
            <span className="text-neutral-400 font-semibold text-sm leading-5">
              Address:
            </span>
            <p className="h-10 ml-1 font-normal text-sm leading-5 ">
              Shyamal Infinity, beneath Radio Mirchi, kalawad road
            </p>
          </div>

          <div className="flex flex-row mb-4 h-5">
            <div className="w-1/2">
              <span className="text-neutral-400 font-semibold text-sm leading-5">
                Dine-in:
              </span>
              <span className="ml-1 font-normal text-sm leading-5 text-destructive-500">
                INACTIVE
              </span>
            </div>
            <div className="w-1/2">
              <span className="text-neutral-400 font-semibold text-sm leading-5">
                Takeaway:
              </span>
              <span className="ml-1 font-normal text-sm leading-5 text-destructive-500">
                INACTIVE
              </span>
            </div>
          </div>

          <div className="flex flex-row h-5">
            <div className="w-1/2">
              <span className="text-neutral-400 font-semibold text-sm leading-5">
                QSR:
              </span>
              <span className="ml-1 font-normal text-sm leading-5 text-destructive-500">
                INACTIVE
              </span>
            </div>
            <div className="w-1/2">
              <span className="text-neutral-400 font-semibold text-sm leading-5">
                Delivery:
              </span>
              <span className="ml-1 font-normal text-sm leading-5 text-tertiary-800">
                ACTIVE
              </span>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
