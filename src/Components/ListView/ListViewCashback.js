import React, { useState } from "react";
import { ReactComponent as DownArrow } from "../../Assets/chevron-down.svg";
import { ReactComponent as LoyaltyCashbackIcon } from "../../Assets/loyalty-cashback.svg";

export default function ListViewCashback(props) {
    const [isShowDetails, setIsShowDetails] = useState(false);

    const handleClickShowDetails = () => {
        setIsShowDetails(!isShowDetails);
    };

    return (
        <>
            <div className="w-full px-4 py-3 border border-neutral-300 rounded-md">
                <div className="flex flex-row items-center justify-between cursor-pointer" onClick={handleClickShowDetails}>
                    <div>
                        <h3 className="paragraphOverlineSmall text-neutral-700 mb-1">CUSTOMER NAME:</h3>
                        <span className="paragraphSmallRegular">{props.customerName}</span>
                    </div>
                    <div className="flex flex-row items-center">
                        <div className={`flex flex-row items-center mr-1 ${isShowDetails && "hidden"}`}>
                            <LoyaltyCashbackIcon />
                            <span className="paragraphSmallRegular ml-1">{props.cashback}</span>
                        </div>
                        <div className={`${isShowDetails && "rotate-180"}`}>
                            <DownArrow />
                        </div>
                    </div>
                </div>
                <div className={`${!isShowDetails && "hidden"}`}>
                    <div className="pt-2">
                        <span className="paragraphOverlineSmall text-neutral-700">ORDER ID:</span>
                        <span className="paragraphSmallRegular ml-1">{props.orderId}</span>
                    </div>
                    <div className="pt-2">
                        <span className="paragraphOverlineSmall text-neutral-700">ORDER DATE:</span>
                        <span className="paragraphSmallRegular ml-1">{props.orderDate}</span>
                    </div>
                    <div className="pt-2">
                        <span className="paragraphOverlineSmall text-neutral-700">ORDER AMOUNT:</span>
                        <span className="paragraphSmallRegular ml-1">{props.orderAmount}</span>
                    </div>

                    <div className="pt-2 flex flex-row justify-between items-center mobile:block">
                        <div className="flex flex-row items-center">
                            <span className="paragraphOverlineSmall text-neutral-700">CASHBACK EARNED:</span>
                            <div className="paragraphSmallRegular ml-1 flex flex-row items-center">
                                <LoyaltyCashbackIcon className="h-5 w-5 mr-1" />
                                {props.cashback}
                            </div>
                        </div>
                        <span className="paragraphSmallUnderline text-primary-500 cursor-pointer" onClick={() => props.handleClickCustomerName()}>
                            View details
                        </span>
                    </div>
                </div>
            </div>
        </>
    );
}
