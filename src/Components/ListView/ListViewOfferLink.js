import React, { useState } from "react";
import { ReactComponent as DownArrow } from "../../Assets/chevron-down.svg";
import ToggleSwitch from "../ToggleSwitch/ToggleSwitch";

export default function ListViewOfferLink(props) {
  const [isShowDetails, setIsShowDetails] = useState(false);

  const handleClickShowDetails = () => {
    setIsShowDetails(!isShowDetails);
  };

  return (
    <div className="w-full px-4 py-2.5 border border-neutral-300 rounded-md">
      <div className="flex flex-row items-center justify-between">
        <div className="flex flex-row items-center">
          <div className="mr-4">
            <ToggleSwitch />
          </div>
          <div>
            <h3 className="paragraphOverlineSmall text-neutral-700 mb-1 flex">OUTLET NAME:</h3>
            <span className="paragraphSmallRegular flex">{props.outletName}</span>
          </div>
        </div>
        <div className="flex flex-row items-center">
          <div className={`${isShowDetails && "rotate-180"} cursor-pointer`} onClick={handleClickShowDetails}>
            <DownArrow />
          </div>
        </div>
      </div>
      <div className={`${!isShowDetails && "hidden"} mt-[5px] ml-14`}>
        <div className="mb-[7px]">
          <span className="paragraphOverlineSmall text-neutral-700">STATE</span>
          <span className="paragraphSmallRegular ml-1">{props.state}</span>
        </div>
        <div className="mb-[7px]">
          <span className="paragraphOverlineSmall text-neutral-700">CITY:</span>
          <span className="paragraphSmallRegular ml-1">{props.city}</span>
        </div>
        <div className="mb-[7px] flex">
          <span className="paragraphOverlineSmall text-neutral-700">ADDRESS:</span>
          <span className="paragraphSmallRegular ml-1">{props.address}</span>
        </div>
      </div>
    </div>
  );
}
