import React, { useState } from "react";
import DownArrow from "../../Assets/chevron-down.svg";

export default function ListViewMediaCard(props) {
  const [isShowDetails, setIsShowDetails] = useState(false);

  const handleClickShowDetails = () => {
    setIsShowDetails(!isShowDetails);
  };

  return (
    <>
      <div className="w-full h-full px-4 py-3 border border-neutral-300 rounded-md">
        <div className="flex flex-row justify-between items-center">
          <div className="flex flex-row items-center">
            <img src={props.image} alt="" className="w-10 h-10 rounded-[5px]" />
            <span className="paragraphMediumMedium ml-2">{props.title} </span>
          </div>
          <img
            src={DownArrow}
            alt="Logo"
            className={`h-6 w-6 ${isShowDetails && "rotate-180"} cursor-pointer`}
            onClick={handleClickShowDetails}
          />
        </div>
        <div className={`mt-4 ${!isShowDetails && "hidden"}`}>
          <div className="flex flex-row mb-1">
            <span className="paragraphOverlineSmall text-neutral-700">
              CATEGORY:
            </span>
            <span className="ml-2 paragraphSmallRegular">{props.category}</span>
          </div>
          <div className="flex flex-row py-1">
            <span className="paragraphOverlineSmall text-neutral-700">
              SUB-CATEGORY:
            </span>
            <span className="ml-2 paragraphSmallRegular">{props.subCategory}</span>
          </div>
          <div className="flex flex-row my-1">
            <span className="paragraphOverlineSmall text-neutral-700">
              UPLOADER:
            </span>
            <span className="ml-2 paragraphSmallRegular text-primary-500">
              {props.uploader}
            </span>
          </div>
          <div className="flex flex-row pt-1">
            <span className="paragraphOverlineSmall text-neutral-700">
              UPLOADED ON:
            </span>
            <span className="ml-2 paragraphSmallRegular">
              {props.upoadedOn}
            </span>
          </div>
        </div>
      </div>
    </>
  );
}
