import React, { useState } from "react";
import { ReactComponent as DownArrow } from "../../Assets/chevron-down.svg";

export default function ListViewCustomerGroupDetails(props) {
    const [isShowDetails, setIsShowDetails] = useState(false);

    const handleClickShowDetails = () => {
        setIsShowDetails(!isShowDetails);
    };

    return (
        <>
            <div className="w-full px-4 py-2.5 border border-neutral-300 rounded-md">
                <div className="flex flex-row items-center justify-between cursor-pointer" onClick={handleClickShowDetails}>
                    <div>
                        <h3 className="paragraphOverlineSmall text-neutral-700 mb-1">GROUP NAME:</h3>
                        <span className="paragraphSmallRegular">{props.groupName}</span>
                    </div>
                    <div className="flex flex-row items-center">
                        <div className={`${isShowDetails && "rotate-180"}`}>
                            <DownArrow />
                        </div>
                    </div>
                </div>
                <div className={`${!isShowDetails && "hidden"} mt-[5px]`}>
                    <div className="mb-[5px]">
                        <span className="paragraphOverlineSmall text-neutral-700">CREATED BY:</span>
                        <span className="paragraphSmallRegular ml-1 text-primary-500">{props.createdBy}</span>
                    </div>
                    <div className="mb-[5px]">
                        <span className="paragraphOverlineSmall text-neutral-700">LAST UPDATED:</span>
                        <span className="paragraphSmallRegular ml-1 text-primary-500">{props.lastUpdated}</span>
                    </div>
                    <div className="mb-[5px]">
                        <span className="paragraphOverlineSmall text-neutral-700">CUSTOMER COUNT:</span>
                        <span className="paragraphSmallRegular ml-1">{props.customerCount}</span>
                    </div>

                    <div className="mb-[5px]">
                        <span className="paragraphOverlineSmall text-neutral-700">CREATED ON:</span>
                        <span className="paragraphSmallRegular ml-1">{props.createdOn}</span>
                    </div>
                    <div className="">
                        <span className="paragraphOverlineSmall text-neutral-700">NOTIFICATIONS SENT:</span>
                        <span className="paragraphSmallRegular ml-1">{props.notificationsSent}</span>
                    </div>
                </div>
            </div>
        </>
    );
}
