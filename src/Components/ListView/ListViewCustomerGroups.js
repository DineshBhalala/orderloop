import React, { useState } from "react";
import { ReactComponent as DownArrow } from "../../Assets/chevron-down.svg";
import { ReactComponent as LinkIcon } from "../../Assets/link.svg";

export default function ListViewCustomerGroups(props) {
    const [isShowDetails, setIsShowDetails] = useState(false);

    const handleClickShowDetails = () => {
        setIsShowDetails(!isShowDetails);
    };

    return (
        <>
            <div className="w-full px-4 py-2.5 border border-neutral-300 rounded-md">
                <div className="flex flex-row items-center justify-between cursor-pointer" onClick={handleClickShowDetails}>
                    <div>
                        <h3 className="paragraphOverlineSmall text-neutral-700 mb-1">GROUP NAME:</h3>
                        <span className="paragraphSmallRegular">{props.groupName}</span>
                    </div>
                    <div className="flex flex-row items-center">
                        <div className={`${isShowDetails && "rotate-180"}`}>
                            <DownArrow />
                        </div>
                    </div>
                </div>

                <div className={`${!isShowDetails && "hidden"} mt-[5px]`}>
                    <div className="mb-[5px]">
                        <span className="paragraphOverlineSmall text-neutral-700">CUSTOMERS COUNT:</span>
                        <span className="paragraphSmallRegular ml-1">{props.customerCount}</span>
                    </div>
                    <div className="mb-[5px]">
                        <span className="paragraphOverlineSmall text-neutral-700">CREATED BY:</span>
                        <span className="paragraphSmallRegular ml-1 text-primary-500">{props.createdBy}</span>
                    </div>
                    <div className="mb-[5px]">
                        <span className="paragraphOverlineSmall text-neutral-700">CREATED ON:</span>
                        <span className="paragraphSmallRegular ml-1">{props.createdOn}</span>
                    </div>
                    <div className="mb-[5px]">
                        <span className="paragraphOverlineSmall text-neutral-700">LAST UPDATED:</span>
                        <span className="paragraphSmallRegular ml-1">{props.lastUpdated}</span>
                    </div>
                    <div className="flex flex-row justify-between items-center">
                        <div className="flex flex-row items-center">
                            <span className="paragraphOverlineSmall text-neutral-700">LINKED OFFERS:</span>
                            <div className="paragraphSmallRegular ml-1 items-center flex flex-row">
                                <LinkIcon stroke="#6C5DD3" className="h-5 w-5" />
                                <span className="ml-1 text-primary-500">{props.linkOffers}</span>
                            </div>
                        </div>
                        <span className="paragraphSmallUnderline text-primary-500 cursor-pointer" onClick={() => props.viewDetails()}>
                            View details
                        </span>
                    </div>
                </div>
            </div>
        </>
    );
}
