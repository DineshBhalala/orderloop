import React, { useState } from "react";
import { ReactComponent as DownArrow } from "../../Assets/chevron-down.svg";

export default function ListViewCustomerIndividualDetails(props) {
  const [isShowDetails, setIsShowDetails] = useState(false);

  const handleClickShowDetails = () => {
    setIsShowDetails(!isShowDetails);
  };

  return (
    <>
      <div className="w-full px-4 py-2.5 border border-neutral-300 rounded-md">
        <div
          className="flex flex-row items-center justify-between cursor-pointer"
          onClick={handleClickShowDetails}
        >
          <div>
            <h3 className="paragraphOverlineSmall text-neutral-700 mb-1">
              CUSTOMER NAME:
            </h3>
            <span className="paragraphSmallRegular">{props.customerName}</span>
          </div>
          <div className="flex flex-row items-center">
            <div className={`${isShowDetails && "rotate-180"}`}>
              <DownArrow />
            </div>
          </div>
        </div>

        <div className={`${!isShowDetails && "hidden"} mt-[5px]`}>
          <div className="mb-[5px]">
            <span className="paragraphOverlineSmall text-neutral-700">
              MOBILE NUMBER:
            </span>
            <span className="paragraphSmallRegular ml-1">
              {props.mobileNumber}
            </span>
          </div>
          <div className="mb-[5px]">
            <span className="paragraphOverlineSmall text-neutral-700">
              TOTAL ORDERS:
            </span>
            <span className="paragraphSmallRegular ml-1">
              {props.totalOrders}
            </span>
          </div>
          <div className="mb-[5px]">
            <span className="paragraphOverlineSmall text-neutral-700">
              LAST ORDERS:
            </span>
            <span className="paragraphSmallRegular ml-1">
              {props.lastOrder}
            </span>
          </div>
          <div className="mb-[5px]">
            <span className="paragraphOverlineSmall text-neutral-700">
            ORDER RATING:
            </span>
            <span className="paragraphSmallRegular ml-1">
              {props.orderRating}
            </span>
          </div>
          <div className="mb-[5px]">
            <span className="paragraphOverlineSmall text-neutral-700">
              CASHBACK EARNED:
            </span>
            <span className="paragraphSmallRegular ml-1">
              {props.cashbackEarned}
            </span>
          </div>
          <div className="mb-[5px]">
            <span className="paragraphOverlineSmall text-neutral-700">
              EMAIL ID:
            </span>
            <span className="paragraphSmallRegular ml-1">{props.emailId}</span>
          </div>
          <div className="mb-[5px]">
            <span className="paragraphOverlineSmall text-neutral-700">
              REVENUE GENERATED:
            </span>
            <span className="paragraphSmallRegular ml-1">
              {props.revenueGenerated}
            </span>
          </div>
          <div className="mb-[5px]">
            <span className="paragraphOverlineSmall text-neutral-700">
              FIRST ORDER:
            </span>
            <span className="paragraphSmallRegular ml-1">
              {props.firstOrder}
            </span>
          </div>
          <div className="mb-[5px]">
            <span className="paragraphOverlineSmall text-neutral-700">
              JOIN DATE:
            </span>
            <span className="paragraphSmallRegular ml-1">{props.joinDate}</span>
          </div>
          <div className="mb-[5px]">
            <span className="paragraphOverlineSmall text-neutral-700">
              DISCOUNT AVAILED:
            </span>
            <span className="paragraphSmallRegular ml-1">
              {props.discountAvailed}
            </span>
          </div>
        </div>
      </div>
    </>
  );
}
