import React, { useState } from "react";
import { ReactComponent as DownArrow } from "../../Assets/chevron-down.svg";
import { ReactComponent as MenuIcon } from "../../Assets/menu.svg";

export default function ListViewOutletMenu(props) {
    const [isShowDetails, setIsShowDetails] = useState(false);

    const handleClickShowDetails = () => {
        setIsShowDetails(!isShowDetails);
    };

    return (
        <>
            <div className="w-full px-4 py-2.5 border border-neutral-300 rounded-md">
                <div className="flex flex-row items-center justify-between cursor-pointer" onClick={handleClickShowDetails}>
                    <div>
                        <h3 className="paragraphOverlineSmall text-neutral-700 mb-1">OUTLET NAME:</h3>
                        <span className="paragraphSmallRegular">{props.outletName}</span>
                    </div>
                    <div className="flex flex-row items-center">
                        <div className={`${isShowDetails && "rotate-180"}`}>
                            <DownArrow />
                        </div>
                    </div>
                </div>
                <div className={`${!isShowDetails && "hidden"} mt-[5px]`}>
                    <div className="mb-[5px]">
                        <span className="paragraphOverlineSmall text-neutral-700">STATE:</span>
                        <span className="paragraphSmallRegular ml-1">{props.state}</span>
                    </div>
                    <div className="mb-[5px]">
                        <span className="paragraphOverlineSmall text-neutral-700">CITY:</span>
                        <span className="paragraphSmallRegular ml-1">{props.city}</span>
                    </div>
                    <div className="mb-[5px]">
                        <span className="paragraphOverlineSmall text-neutral-700">ADDRESS:</span>
                        <span className="paragraphSmallRegular ml-1">{props.address}</span>
                    </div>

                    <div className="mb-[5px]">
                        <span className="paragraphOverlineSmall text-neutral-700">PRESET ATTACHED:</span>
                        <span className="paragraphSmallRegular ml-1">{props.presetAttached}</span>
                    </div>
                    <div className="flex flex-row justify-between items-end">
                        <div className="">
                            <span className="paragraphOverlineSmall text-neutral-700 pb-1">CATEGORIES AND DISHES:</span>
                            <div className="flex flex-col">
                                <div className="flex flex-row items-center mb-1">
                                    <MenuIcon />
                                    <span className="paragraphSmallRegular ml-2">Categories - {props.categories}</span>
                                </div>
                                <div className="flex flex-row items-center">
                                    <MenuIcon />
                                    <span className="paragraphSmallRegular ml-2">Dishes - {props.dishes}</span>
                                </div>
                            </div>
                        </div>
                        <span className="paragraphSmallUnderline text-primary-500 cursor-pointer" onClick={() => props.handleClickViewDetails()}>
                            View details
                        </span>
                    </div>
                </div>
            </div>
        </>
    );
}
