import React, { useState } from "react";
import { ReactComponent as DownArrow } from "../../Assets/chevron-down.svg";
import { ReactComponent as LinkIcon } from "../../Assets/link.svg";
import { ReactComponent as MenuIcon } from "../../Assets/menu.svg";

export default function ListViewCataloguePresets(props) {
    const [isShowDetails, setIsShowDetails] = useState(false);

    const handleClickShowDetails = () => {
        setIsShowDetails(!isShowDetails);
    };

    return (
        <>
            <div className="w-full px-4 py-2.5 border border-neutral-300 rounded-md">
                <div className="flex flex-row items-center justify-between cursor-pointer" onClick={handleClickShowDetails}>
                    <div>
                        <h3 className="paragraphOverlineSmall text-neutral-700 mb-1">PRESET NAME:</h3>
                        <span className="paragraphSmallRegular">{props.presetName}</span>
                    </div>
                    <div className="flex flex-row items-center">
                        <div className={`${isShowDetails && "rotate-180"}`}>
                            <DownArrow />
                        </div>
                    </div>
                </div>
                <div className={`${!isShowDetails && "hidden"} mt-[5px]`}>
                    <div className="mb-[5px]">
                        <span className="paragraphOverlineSmall text-neutral-700 pb-1">CATEGORIES AND DISHES:</span>
                        <div className="flex flex-col">
                            <div className="flex flex-row items-center mb-1">
                                <MenuIcon />
                                <span className="paragraphSmallRegular ml-2">Categories - {props.categories}</span>
                            </div>
                            <div className="flex flex-row items-center">
                                <MenuIcon />
                                <span className="paragraphSmallRegular ml-2">Dishes - {props.dishes}</span>
                            </div>
                        </div>
                    </div>
                    <div className="mb-[5px]">
                        <span className="paragraphOverlineSmall text-neutral-700">CREATED BY:</span>
                        <span className="paragraphSmallRegular ml-1 text-primary-500">{props.createdBy}</span>
                    </div>
                    <div className="mb-[5px]">
                        <span className="paragraphOverlineSmall text-neutral-700">CREATED ON:</span>
                        <span className="paragraphSmallRegular ml-1">{props.createdOn}</span>
                    </div>

                    <div className="mb-[5px]">
                        <span className="paragraphOverlineSmall text-neutral-700">LAST UPDATED ON:</span>
                        <span className="paragraphSmallRegular ml-1">{props.lastUpdatedOn}</span>
                    </div>
                    <div className="flex flex-row justify-between items-center">
                        <div className="flex flex-row items-center">
                            <span className="paragraphOverlineSmall mobile:text-[11px]">LINK OUTLET:</span>
                            <div className="flex flex-row items-center mx-1 cursor-pointer" onClick={props.handleLClickLink}>
                                <LinkIcon stroke="#6C5DD3" />
                                <span className="text-primary-500 ml-1 paragraphSmallRegular mobile:text-[11px]">4</span>
                            </div>
                        </div>
                        <span className="paragraphSmallUnderline text-primary-500 cursor-pointer" onClick={() => props.handleClickViewDetails()}>
                            View details
                        </span>
                    </div>
                </div>
            </div>
        </>
    );
}
