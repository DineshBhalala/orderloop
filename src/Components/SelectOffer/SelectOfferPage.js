import React from "react";
import { ReactComponent as Close } from "../../Assets/close.svg";
import DropDown from "../DropDown/DropDown";
import { LargePrimaryButton } from "../Buttons/Button";

export default function SelectOfferPage(props) {
  return (
    <>
      <div className="fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 md:px-4 flex">
        <div className="max-w-[475px] w-full rounded-xl bg-shades-50 px-8 py-6 m-auto md:w-full md:px-4 md:py-4">
          <div className="flex flex-row justify-between items-center mb-6">
            <div>
              <span className="paragraphLargeMedium">Select offer</span>
              <div className="flex flex-row items-center">
                <span className="paragraphMediumItalic text-neutral-500">
                  Select an offer to send it to the customer
                </span>
              </div>
            </div>
            <div onClick={() => props.handleClickClose()} className="cursor-pointer">
              <Close />
            </div>
          </div>

          <div className="mb-12">
            <div className="relative mb-4">
              <DropDown
                label="Select offer type"
                dropdownLabel="Select offer type"
                buttonTextColor="neutral-300"
              />
            </div>
            <div className="relative">
              <DropDown
                label="Select coupon code"
                dropdownLabel="Select coupon code"
                buttonTextColor="neutral-300"
              />
            </div>
          </div>

          <LargePrimaryButton name="Send offer" />
        </div>
      </div>
    </>
  );
}
