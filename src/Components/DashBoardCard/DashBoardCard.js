import React from "react";
import { ReactComponent as CrossArrow } from "../../Assets/cross-arrow.svg";
import { ReactComponent as Reimbursement } from "../../Assets/reimbursement.svg";
import { ReactComponent as OrderRatingIcon } from "../../Assets/order-ratings.svg";

export default function DashBoardCard(props) {
    return (
        <>
            <div className="border border-neutral-300 rounded-md px-4 pb-3 pt-2 w-full relative min-h-[140px]">
                <div className="flex justify-between items-start">
                    <div className="pr-2.5">
                        <span className="paragraphSmallSemiBold text-neutral-500">{props.title}</span>
                        <div className={`flex flex-row ${props.cardType === "dashboard" ? "my-2" : "my-3"} items-center`}>
                            {props.ratingCard ? (
                                <div className="flex flex-row items-center">
                                    <OrderRatingIcon className="h-5 w-5 mr-2" fill={props.startFillColor} stroke={props.startStrokeColor} />
                                    <span className="headingH6SemiBoldDesktop">{props.amount}</span>
                                </div>
                            ) : (
                                <div className="headingH6SemiBoldDesktop mr-2">{props.amount}</div>
                            )}
                            {props.percentage && (
                                <div
                                    className={`px-1 ${props.profit ? "bg-tertiary-100" : "bg-destructive-100"} border ${
                                        props.profit ? "border-tertiary-800" : "border-destructive-600"
                                    } rounded flex flex-row items-center h-4`}
                                >
                                    <div className={`${props.profit ? "text-tertiary-800" : "text-destructive-600"} leading-3 text-[10px] font-medium`}>{props.percentage}&#x25;</div>
                                    <CrossArrow stroke={props.profit ? "#3D8C82" : "#DC2626"} className={`w-[11px] h-[11px] ${!props.profit && "rotate-90"}`} />
                                </div>
                            )}
                        </div>
                    </div>
                    {props.icon && <div className={`p-1 rounded-lg ${!(props.title === "Swiggy" || props.title === "Zomato") && "bg-primary-500 mt-1"} `}>{props.icon}</div>}
                </div>
                <div className={`${(props.content || props.cardType === "dashboard") && "min-h-[40px] mb-3"}`}>
                    {props.content && (
                        <p className="paragraphXSmallRegular text-neutral-500">
                            {props.content.split(" ").map((word, index) =>
                                word === props.focusContent ? (
                                    <span key={index} className={`paragraphXSmallMedium text-${props.focusContentColor}`}>
                                        {word}{" "}
                                    </span>
                                ) : (
                                    <span key={index}>{word} </span>
                                )
                            )}
                        </p>
                    )}
                </div>
                <div>
                    {props.reimbursement && (
                        <>
                            <div className="flex flex-row justify-between items-center">
                                <div className="flex flex-row items-center">
                                    {props.isReimbursementIcon && <Reimbursement fill="#6C5DD3" height={14} width={14} className="mr-1" />}
                                    <p className="paragraphXSmallRegular text-neutral-500">
                                        {props.reimbursement.split(" ").map((word, index) =>
                                            props.reimbursementFocusContent && props.reimbursementFocusContent.split(" ").includes(word) ? (
                                                <span key={index} className={`paragraphXSmallMedium text-${props.focusReimbursementContentColor}`}>
                                                    {word}{" "}
                                                </span>
                                            ) : (
                                                <span key={index}>{word} </span>
                                            )
                                        )}
                                    </p>
                                </div>
                                {props.cardType !== "credit" && (
                                    <div className="flex flex-row ml-6">
                                        <div className="h-1 w-1 rounded-full bg-neutral-500" />
                                        <div className="h-1 w-1 rounded-full bg-neutral-500 mx-0.5" />
                                        <div className="h-1 w-1 rounded-full bg-neutral-500" />
                                    </div>
                                )}
                            </div>
                        </>
                    )}
                </div>
            </div>
        </>
    );
}
