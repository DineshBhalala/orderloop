import React from "react";
import { ReactComponent as LinkIcon } from "../../Assets/link.svg";

export const LinkOffer = (props) => {
  return (
    <>
      <div className="flex flex-row items-center">
        <LinkIcon stroke="#6C5DD3" />
        <span className="paragraphSmallRegular text-primary-500 mx-1">
          {props.linkLabel ?? "Link"}
        </span>
        {props.linkOfferNumber && (
          <div className="bg-primary-500 flex-col flex text-primary-50 h-5 w-5 rounded-full text-center justify-center pr-[1px]">
            <span className="m-auto leading-none items-center paragraphXSmallSemiBold text-primary-50">
              {props.linkOfferNumber}
            </span>
          </div>
        )}
      </div>
    </>
  );
};
