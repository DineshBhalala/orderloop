import React from "react";

export const DefaultInputField = (props) => {
    const {
        label,
        Action,
        labelMarginB,
        labelStyle,
        inputType,
        placeholder,
        boxHeight,
        paddingLeft,
        placeholderIcon,
        Addon,
        addonIcon,
        placeholderTextColor,
        shadow,
        helperText,
        onChange,
        value,
        addonStyle,
        handleClickAction,
        actionTextColor,
        actionTextStyle,
    } = props;

    return (
        <>
            {/* <div className="w-full">
        {(props.label || props.Action) && (
          <div className={`justify-between w-full ${props.labelMarginB ?? "mb-1"} flex flex-row`}>
            <label className={`disabled:text-neutral-300 ${props.labelStyle ?? "paragraphSmallMedium"}`}>{props.label}</label>
            <span className="paragraphSmallMedium text-primary-500 disabled:text-neutral-300">{props.Action}</span>
          </div>
        )}

        <div className="relative">
          <input
            type={props.inputType ?? "text"}
            placeholder={props.placeholder}
            className={`w-full h-${props.boxHeight ?? "12"} paragraphSmallRegular border rounded-md border-neutral-300 ${
              props.paddingLeft ?? (props.placeholderIcon ? "pl-12" : "pl-4")
            } ${props.Addon || props.addonIcon ? "pr-[100px]" : "pr-4"} placeholder:${props.placeholderTextColor ?? "text-neutral-300"} ${
              props.shadow
            } focus:border-primary-500 outline-none focus:outline-none focus:ring-4 focus:ring-primary-100 caret-primary-500 placeholder:paragraphSmallRegular`}
          />
          <div className="flex flex-row absolute right-4 top-3.5">
            <span className={`${props.addonStyle ?? "text-primary-300 paragraphSmallRegular"}`}>{props.Addon}</span>
            {props.addonIcon && <div className="h-6 w-6 ml-1 rounded">{props.addonIcon}</div>}
          </div>
          {props.placeholderIcon && <div className="h-6 w-6 rounded absolute top-3 left-4">{props.placeholderIcon}</div>}
        </div>
        {props.helperText && <p className="w-full pt-2 text-neutral-500 paragraphSmallRegular group-disabled:text-neutral-300">{props.helperText}</p>}
      </div> */}
            <div className="w-full">
                {(label || Action) && (
                    <div className={`justify-between w-full ${labelMarginB ?? "mb-1"} flex flex-row`}>
                        <label className={`disabled:text-neutral-300 ${labelStyle ?? "paragraphSmallMedium"}`}>{label}</label>
                        <span
                            className={`${actionTextStyle ?? "paragraphSmallMedium"} ${actionTextColor ?? "text-primary-500"} disabled:text-neutral-300 cursor-pointer ${props.removeSectionStyle}`}
                            onClick={handleClickAction}
                        >
                            {Action}
                        </span>
                    </div>
                )}

                <div className="relative">
                    <input
                        type={inputType ?? "text"}
                        placeholder={placeholder}
                        className={`w-full h-${boxHeight ? boxHeight : "12"} paragraphSmallRegular border rounded-md border-neutral-300 ${paddingLeft ?? (placeholderIcon ? "pl-12" : "pl-4")} ${
                            Addon || addonIcon ? "pr-[100px]" : "pr-4"
                        } placeholder:${
                            placeholderTextColor ?? "text-neutral-300"
                        } ${shadow} focus:border-primary-500 outline-none focus:outline-none focus:ring-4 focus:ring-primary-100 caret-primary-500 placeholder:paragraphSmallRegular ${
                            props.inputStyle
                        }`}
                        onChange={onChange}
                        value={value}
                    />
                    <div className="flex flex-row absolute right-4 top-3.5">
                        <span className={`${addonStyle ?? "text-primary-300 paragraphSmallRegular"}`}>{Addon}</span>
                        {addonIcon && <div className="h-6 w-6 ml-1 rounded">{addonIcon}</div>}
                    </div>

                    {placeholderIcon && <div className="h-6 w-6 rounded absolute top-3 left-4">{placeholderIcon}</div>}
                </div>
                {helperText && <p className="w-full pt-2 text-neutral-500 paragraphSmallRegular group-disabled:text-neutral-300">{helperText}</p>}
            </div>
        </>
    );
};

export const DestructiveInputField = (props) => {
    return (
        <>
            <div className="w-full">
                {(props.label || props.Action) && (
                    <div className="flex flex-row justify-between w-full mb-1">
                        <label className="h-5 font-medium disabled:text-neutral-300 text-sm leading-5">{props.label}</label>
                        <span className="text-primary-500 font-medium disabled:text-neutral-300">{props.Action}</span>
                    </div>
                )}
                <div className="relative">
                    <input
                        type="text"
                        placeholder={props.placeholder}
                        className={`w-full h-12 border rounded-md border-destructive-300 pl-12 ${
                            props.Addon || props.addonIcon ? "pr-[100px]" : "pr-4"
                        } placeholder:text-neutral-300 font-normal focus:border-destructive-500 outline-none focus:outline-none focus:ring-4 text-sm focus:ring-destructive-100 caret-destructive-500 placeholder:paragraphSmallRegular`}
                    />
                    <div className="flex flex-row absolute right-4 top-3">
                        <span className="text-neutral-300 text-sm leading-5">{props.Addon}</span>
                        {props.addonIcon && <div className="h-6 w-6 ml-1 rounded">{props.addonIcon}</div>}
                    </div>
                    {props.placeholderIcon && <div className="h-6 w-6 rounded absolute top-3 left-4">{props.placeholderIcon}</div>}
                </div>
                {props.helperText && <p className="w-full pt-2 text-destructive-500 font-normal group-disabled:text-neutral-300">{props.helperText}</p>}
            </div>
        </>
    );
};

export const DisabledInputField = (props) => {
    return (
        <>
            <div className="w-full">
                <div className="flex flex-row justify-between w-full">
                    <label className="h-5 text-neutral-300 font-medium">{props.label}</label>
                    <span className="text-neutral-300 font-medium">{props.Action}</span>
                </div>
                <div className="relative mt-1">
                    <input
                        type="text"
                        placeholder={props.placeholder}
                        className={`w-full h-12 border rounded-md border-neutral-500 ${props.placeholderIcon ? "pl-12" : "pl-4"} pr-[100px]
            text-neutral-300 placeholder:text-neutral-300 font-normal outline-none placeholder:paragraphSmallRegular`}
                    />
                    <div className="flex flex-row absolute right-4 top-3">
                        <span className="text-neutral-300">{props.Addon}</span>
                        {props.addonIcon && <img src={props.addonIcon} alt="" className="w-6 h-6 rounded ml-1" />}
                    </div>
                    {props.placeholderIcon && <img src={props.placeholderIcon} alt="" className="h-6 w-6 rounded absolute top-3 left-4" />}
                </div>
                <p className="w-full pt-2 text-neutral-300 font-normal group-disabled:text-neutral-300">{props.helperText}</p>
            </div>
        </>
    );
};

export const InputArea = (props) => {
    return (
        <>
            <div className="w-full">
                {props.label && (
                    <div className={`flex flex-row justify-between w-full ${props.labelMarginB ?? "mb-2"}`}>
                        <label
                            className={`disabled:text-neutral-300 ${
                                props.labelStyle ?? (props.type === "offer" || props.type === "storeSetting") ? "paragraphMediumItalic text-neutral-500" : "paragraphSmallMedium"
                            }`}
                        >
                            {props.label}
                        </label>
                    </div>
                )}
                <div className="relative">
                    <textarea
                        type="text"
                        placeholder={props.placeholder}
                        className={` ${props.paddingT ?? "pt-3"} px-4 ${!props.boxPaddingB ? "pb-3" : props.boxPaddingB} ${props.boxHeight ?? "h-28"} ${props.resizeNone} ${
                            props.shadow
                        } w-full border rounded-md block border-neutral-300 pl-4 placeholder:text-neutral-300 focus:border-primary-500 outline-none focus:outline-none focus:ring-4 focus:ring-primary-100 caret-primary-500 paragraphSmallRegular`}
                    />

                    <div className="flex flex-row absolute right-4 top-3">
                        <span className="text-neutral-300 paragraphSmallRegular">{props.Addon}</span>
                        {props.addonIcon && <div className="h-6 w-6 ml-1 rounded">{props.addonIcon}</div>}
                    </div>
                    {props.placeholderIcon && <div className="h-6 w-6 rounded absolute top-3 left-4">{props.placeholderIcon}</div>}
                </div>
                {props.helperText && <p className="w-full pt-2 text-neutral-500 paragraphSmallRegular group-disabled:text-neutral-300">{props.helperText}</p>}
            </div>
        </>
    );
};
