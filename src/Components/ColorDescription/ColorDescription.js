import React from "react";

export default function ColorDescription(props) {
    return (
        <>
            <div className={`flex flex-row items-center ${props.marginR}`}>
                <div className={`h-6 w-6 rounded border-[1.5px] ${props.bgColor} ${props.border} mr-1`} />
                <span className="paragraphMediumRegular text-[#000000]">{props.label}</span>
            </div>
        </>
    );
}
