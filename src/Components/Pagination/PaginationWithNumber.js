import React from 'react'
import { ReactComponent as Arrow } from "../../Assets/arrow.svg";

const PaginationWithNumber = () => {
  return (
    <>
      <div className="flex flex-row items-center justify-between w-full">
        <span className="paragraphSmallMedium">Showing 06 of 20 results</span>
        <div className="flex flex-row items-center rounded-[5px] border h-[38px] border-neutral-300 paragraphSmallRegular">
          <div className="min-w-[42px] h-full flex flex-col cursor-pointer"><Arrow className="m-auto" /></div>
          <div className="min-w-[42px] h-full flex flex-col border-l cursor-default"><span className="m-auto px-0.5">1</span></div>
          <div className="min-w-[42px] h-full flex flex-col border-l cursor-default"><span className="m-auto px-0.5">2</span></div>
          <div className="min-w-[42px] h-full flex flex-col border-l cursor-default"><span className="m-auto px-0.5">3</span></div>
          <div className="min-w-[42px] h-full flex flex-col border-l cursor-default"><span className="m-auto px-0.5">...</span></div>
          <div className="min-w-[42px] h-full flex flex-col border-l cursor-default"><span className="m-auto px-0.5">8</span></div>
          <div className="min-w-[42px] h-full flex flex-col border-l cursor-default"><span className="m-auto px-0.5">9</span></div>
          <div className="min-w-[42px] h-full flex flex-col border-l cursor-default"><span className="m-auto px-0.5">10</span></div>
          <div className="min-w-[42px] h-full flex flex-col border-l cursor-pointer"><Arrow className="rotate-180 m-auto" /></div>
        </div>
      </div>
    </>
  )
}

export default PaginationWithNumber