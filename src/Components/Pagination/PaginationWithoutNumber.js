import React from 'react'
import { ReactComponent as Arrow } from "../../Assets/arrow.svg";

const PaginationWithoutNumber = () => {
  return (
    <>
      <div className="flex flex-row items-center justify-between w-full">
        <span className="paragraphSmallMedium">Showing 06 of 20 results</span>
        <div className="flex flex-row items-center rounded-[5px] border h-[38px] border-neutral-300 paragraphSmallRegular">
          <div className="min-w-[42px] h-full flex flex-col cursor-pointer"><Arrow className="m-auto" /></div>
          <div className="min-w-[42px] h-full flex flex-col border-l cursor-pointer"><Arrow className="rotate-180 m-auto" /></div>
        </div>
      </div>
    </>
  )
}

export default PaginationWithoutNumber