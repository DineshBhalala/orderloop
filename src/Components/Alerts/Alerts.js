import React from "react";
import close from "../../Assets/close.svg";

export const NutralAlert = (props) => {
    return (
        <>
            <div className="w-full">
                <div className="pt-4 pl-4 pb-4 pr-[21px] flex flex-row items-start border border-neutral-300 rounded-md bg-neutral-50">
                    <img src={props.icon} alt="" className="mr-2 opacity-20 w-6 h-6" />
                    <div>
                        <h1 className="paragraphSmallMedium mb-2">{props.alertHeader}</h1>

                        <p className="paragraphSmallRegular mb-5 text-neutral-500">{props.alertContent}</p>

                        <div className="flex-row">
                            <span className="paragraphSmallMedium mr-4">{props.action1}</span>
                            <span className="paragraphSmallMedium">{props.action2}</span>
                        </div>
                    </div>
                    <img src={close} alt="" className="pl-[13px] ml-auto" />
                </div>
            </div>
        </>
    );
};

export const PrimaryAlert = (props) => {
    return (
        <>
            <div className="w-full">
                <div className="pt-4 pl-4 pb-4 pr-[21px] flex flex-row items-start border border-primary-300 rounded-md bg-primary-50">
                    <img src={props.icon} alt="" className="pr-2 opacity-20" />
                    <div>
                        <h1 className="paragraphSmallMedium text-primary-300 pb-2">{props.alertHeader}</h1>

                        <p className="paragraphSmallRegular mb-5 text-primary-500">{props.alertContent}</p>

                        <div className="flex-row">
                            <span className="paragraphSmallMedium mr-4 text-primary-500">{props.action1}</span>
                            <span className="paragraphSmallMedium text-primary-500">{props.action2}</span>
                        </div>
                    </div>
                    <img src={close} alt="" className="pl-[13px] ml-auto" />
                </div>
            </div>
        </>
    );
};

export const WarningAlert = (props) => {
    return (
        <>
            <div className="w-full">
                <div className={`pt-4 pl-4 pb-4 pr-[21px] flex flex-row items-start border border-secondary-300 rounded-md bg-warning-50 ${props.shadow}`}>
                    {props.icon && <div className={props.iconStyle}> {props.icon}</div>}
                    <div className="ml-3">
                        <h1 className={`${props.headerStyle ?? "paragraphSmallMedium"} text-secondary-800 pb-2`}>{props.alertHeader}</h1>

                        {props.alertContent && <p className="paragraphSmallRegular mb-5 text-warning-500">{props.alertContent}</p>}

                        <div className="flex-row">
                            <span className={`${props.action1Style ?? "paragraphSmallMedium"} mr-4 text-secondary-800 cursor-pointer`}>{props.action1}</span>
                            <span className={`${props.action2Style ?? "paragraphSmallMedium"} mr-4 text-secondary-800 cursor-pointer`}>{props.action2}</span>
                            <span className={`${props.action3Style ?? "paragraphSmallMedium"} mr-4 text-secondary-800 cursor-pointer`}>{props.action3}</span>
                        </div>
                    </div>
                    {props.showCloseIcon && <img src={close} alt="" className="pl-[13px] ml-auto" />}
                </div>
            </div>
        </>
    );
};

export const ErrorAlert = (props) => {
    return (
        <>
            <div className="w-full">
                <div className="pt-4 pl-4 pb-4 pr-[21px] flex flex-row items-start border border-destructive-300 rounded-md bg-destructive-50">
                    <img src={props.icon} alt="" className="pr-2 opacity-20" />
                    <div>
                        <h1 className="paragraphSmallMedium text-destructive-800 pb-2">{props.alertHeader}</h1>

                        <p className="paragraphSmallRegular mb-5 text-destructive-700">{props.alertContent}</p>

                        <div className="flex-row">
                            <span className="paragraphSmallMedium mr-4 text-destructive-800">{props.action1}</span>
                            <span className="paragraphSmallMedium text-destructive-800">{props.action2}</span>
                        </div>
                    </div>
                    <img src={close} alt="" className="pl-[13px] ml-auto" />
                </div>
            </div>
        </>
    );
};

export const SuccessAlert = (props) => {
    return (
        <>
            <div className="w-full">
                <div className="pt-4 pl-4 pb-4 pr-[21px] flex flex-row items-start border border-success-300 rounded-md bg-success-50">
                    <img src={props.icon} alt="" className="pr-2 opacity-20" />
                    <div>
                        <h1 className="paragraphSmallMedium text-success-900 pb-2">{props.alertHeader}</h1>

                        <p className="paragraphSmallRegular mb-5 text-success-700">{props.alertContent}</p>

                        <div className="flex-row">
                            <span className="paragraphSmallMedium mr-4 text-success-900">{props.action1}</span>
                            <span className="paragraphSmallMedium text-success-900">{props.action2}</span>
                        </div>
                    </div>
                    <img src={close} alt="" className="pl-[13px] ml-auto" />
                </div>
            </div>
        </>
    );
};
