import React, { useEffect } from "react";
import { useState } from "react";
import { Switch } from "@headlessui/react";

export default function ToggleSwitch(props) {
    const [enabled, setEnabled] = useState(props.enable ?? false);

    const handleChange = (e) => {
        if (props.enable === false) {
            return;
        }

        setEnabled(e);
        props.showStatus && props.showStatus(e);
    };

    useEffect(() => {
        if (props.enable === false) {
            setEnabled(false);
            props.showStatus && props.showStatus(false);
        }
    }, [props.enable]);

    return (
        <>
            <div>
                <Switch
                    onChange={(e) => handleChange(e)}
                    checked={enabled}
                    className={`${enabled ? "bg-primary-500" : "bg-neutral-300"} relative inline-flex h-6 w-10 shrink-0 ${
                        props.cursorType ?? "cursor-pointer"
                    } rounded-xl border-2 border-transparent transition-colors duration-200 ease-in-out focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75`}
                >
                    <span
                        aria-hidden="true"
                        className={`${
                            enabled ? "translate-x-4" : "translate-x-0"
                        } pointer-events-none inline-block h-5 w-5 transform rounded-full bg-white shadow-lg ring-0 transition duration-200 ease-in-out`}
                    />
                </Switch>
            </div>
        </>
    );
}
