import React, { useState } from "react";
import { Menu, Transition } from "@headlessui/react";
import { Fragment } from "react";
import { ReactComponent as DownArrow } from "../../Assets/chevron-down.svg";

const DropDownOutlet = (props) => {
    const menuItems = props.menuItems ? props.menuItems : [`Domino's Pizza Kalawad Outlet..`, `Domino's Pizza Kalawad Outlet.`];
    const [MenuButton, setMenuButton] = useState("Domino's Pizza Kalawad Outlet");
    return (
        <>
            <label className="paragraphMediumMedium">{props.label}</label>
            <div className="relative">
                <Menu as="div" className="flex dropDownIcon">
                    <Menu.Button className="flex items-center focus:outline-none">
                        <span className="paragraphXSmallItalic text-neutral-500 mr-1">{MenuButton}</span>
                        <DownArrow className="dropDownIconRotate" width="10" height="10" />
                    </Menu.Button>
                    <Transition
                        as={Fragment}
                        enter="transition ease-out duration-100"
                        enterFrom="transform opacity-0 scale-95"
                        enterTo="transform opacity-100 scale-100"
                        leave="transition ease-in duration-75"
                        leaveFrom="transform opacity-100 scale-100"
                        leaveTo="transform opacity-0 scale-95"
                    >
                        <Menu.Items className="absolute w-max mt-6 px-4 py-2 border border-neutral-300 rounded-md shadow-shadowMedium bg-shades-50 focus:outline-none">
                            <div className="pt-2 mb-2">
                                <Menu.Item>
                                    <div
                                        onClick={() => {
                                            setMenuButton("Domino's Pizza Kalawad Outlet");
                                        }}
                                        className="paragraphXSmallRegular leading-normal cursor-pointer"
                                    >
                                        Domino's Pizza Kalawad Outlet
                                    </div>
                                </Menu.Item>
                            </div>
                            {menuItems.map((outletsList, index) => {
                                return (
                                    <div
                                        key={index}
                                        className="py-1.5 cursor-pointer"
                                        onClick={() => {
                                            setMenuButton(outletsList);
                                        }}
                                    >
                                        <Menu.Item>
                                            <div className="paragraphXSmallRegular leading-normal">{outletsList}</div>
                                        </Menu.Item>
                                    </div>
                                );
                            })}
                        </Menu.Items>
                    </Transition>
                </Menu>
            </div>
        </>
    );
};

export default DropDownOutlet;
