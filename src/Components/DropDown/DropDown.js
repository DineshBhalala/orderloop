import React, { useState } from "react";
import { Menu, Transition } from "@headlessui/react";
import { Fragment } from "react";
import { ReactComponent as DownArrow } from "../../Assets/chevron-down.svg";
import { ReactComponent as SearchIcon } from "../../Assets/search.svg";
import EditDelete from "../../Screen/MediaLibrary/Components/EditDelete";

const DropDown = (props) => {
    const menuItems = props.menuItems || ["All", "Active", "Inactive"];

    const [MenuButton, setMenuButton] = useState(props.dropdownLabel || menuItems[0]);

    const [buttonTextColor, setButtonTextColor] = useState(props.buttonTextColor);

    const [itemWithBracket, setItemWithBracket] = useState();

    const handleClickMenuItem = (item, itemWithBracket, itemWithoutBracket) => {
        setItemWithBracket(itemWithBracket);
        setMenuButton(itemWithoutBracket || item);
        setButtonTextColor("neutral-900");
        props.setSelectedItemFromDropDown && props.setSelectedItemFromDropDown(item);
    };

    return (
        <>
            {(props.label || props.action) && (
                <div className={`flex ${props.labelMarginB ?? "mb-1"} justify-between items-center`}>
                    <div className="flex">
                        <label className={`${props.labelStyle ?? "paragraphSmallMedium"}`}>{props.label}</label>
                        <span className="paragraphSmallItalic ml-0.5 text-neutral-500">{props.subTitle}</span>
                    </div>
                </div>
            )}
            <div className="relative">
                <Menu as="div" className="">
                    <div className="dropDownIcon">
                        <Menu.Button
                            className={`${props.shadow ?? "shadow-shadowXsmall"} flex flex-row justify-between ${
                                props.boxWidth ?? "w-full m-auto"
                            } mobile:max-w-full rounded-md outline-none focus:border-primary-500 border py-[14px] focus:ring-4 focus:ring-primary-100 appearance-none px-4 border-neutral-300 ${
                                props.height ?? "h-12"
                            } paragraphSmallRegular mobile:px-1 ${props.buttonStyle}`}
                        >
                            <div className="flex flex-row items-center">
                                {props.menuIcon && <div className="mobile:hidden mr-2 -mt-[3px]">{props.menuIcon}</div>}
                                <span
                                    className={`${props.menuButtonStyle ?? "paragraphSmallRegular"} ${
                                        props.type === "offer" && "md:truncate md:w-[194px] block mobile:w-[138px]"
                                    } text-${buttonTextColor}`}
                                >
                                    {MenuButton}
                                </span>
                                {itemWithBracket && <span className={`text-neutral-500 ${props.type === "posReservation" && "paragraphSmallItalic"}`}>{itemWithBracket}</span>}
                            </div>
                            <DownArrow className={`dropDownIconRotate ${props.type === "offer" && "ml-3"} min-w-[24px] min-h-[24px]`} />
                        </Menu.Button>
                    </div>
                    <Transition
                        as={Fragment}
                        enter="transition ease-out duration-100"
                        enterFrom="transform opacity-0 scale-95"
                        enterTo="transform opacity-100 scale-100"
                        leave="transition ease-in duration-75"
                        leaveFrom="transform opacity-100 scale-100"
                        leaveTo="transform opacity-0 scale-95"
                    >
                        <Menu.Items className="absolute mt-2 left-0 right-0 mobile:w-full md:max-w-full ring-1 ring-black ring-opacity-5 focus:outline-none px-4 py-2 border border-neutral-300 rounded-md shadow-shadowMedium bg-shades-50 z-[5] paragraphSmallRegular">
                            {props.type === "subCategory" && (
                                <Menu.Item>
                                    <div className="w-full pt-2 mb-2">
                                        <span className="paragraphSmallRegular text-primary-500">+ Create new sub-category</span>
                                    </div>
                                </Menu.Item>
                            )}
                            {props.type === "addRider" && (
                                <div className="w-full pt-2 mb-2 flex flex-row items-center">
                                    <SearchIcon />
                                    <input placeholder="Search outlet by typing..." className="ml-2 placeholder:paragraphSmallRegular placeholder:text-neutral-300 outline-none w-full" />
                                </div>
                            )}
                            {props.type === "addUser" && (
                                <Menu.Item>
                                    <div className="w-full pt-2 mb-2">
                                        <span className="paragraphSmallRegular text-primary-500">+ Create new user role</span>
                                    </div>
                                </Menu.Item>
                            )}
                            {menuItems.map((el, index) => {
                                if (props.type === "category" || props.type === "posReservation") {
                                    const itemWithBracket = el.match(/\(.+?\)/);
                                    const itemWithoutBracket = el.replace(/\(.+?\)/, "");
                                    return (
                                        <div key={index} className="pt-2 mb-2 cursor-pointer" onClick={() => handleClickMenuItem(el, itemWithBracket, itemWithoutBracket)}>
                                            <Menu.Item>
                                                <div className="w-full flex flex-row justify-between items-center">
                                                    <div>
                                                        <span
                                                            className="text-neutral-900
                             paragraphSmallRegular cursor-pointer hover:text-primary-500 active:paragraphSmallSemiBold"
                                                        >
                                                            {itemWithoutBracket}
                                                        </span>
                                                        <span
                                                            className={`text-neutral-500
                              hover:text-primary-500 ${props.type === "posReservation" && "paragraphSmallItalic"}`}
                                                        >
                                                            {itemWithBracket}
                                                        </span>
                                                    </div>
                                                </div>
                                            </Menu.Item>
                                        </div>
                                    );
                                }
                                return (
                                    <div key={index} className="pt-2 mb-2 group cursor-pointer" onClick={() => handleClickMenuItem(el)}>
                                        <Menu.Item>
                                            <div className="w-full flex flex-row justify-between items-center cursor-pointer group">
                                                <span className={`${props.textColor || "text-neutral-900"} paragraphSmallRegular group-hover:text-primary-500 active:paragraphSmallSemiBold`}>
                                                    {el}
                                                </span>
                                                {props.type === "subCategory" && (
                                                    <div className="group-hover:block hidden relative">
                                                        <EditDelete />
                                                    </div>
                                                )}
                                            </div>
                                        </Menu.Item>
                                    </div>
                                );
                            })}
                        </Menu.Items>
                    </Transition>
                </Menu>
            </div>
        </>
    );
};
export default DropDown;
