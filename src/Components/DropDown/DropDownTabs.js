import React, { useState } from "react";
import { Menu, Transition } from "@headlessui/react";
import { Fragment } from "react";
import { ReactComponent as DownArrow } from "../../Assets/chevron-down.svg";

export const DropDownTabs = (props) => {
    const [menuButton, setMenuButton] = useState(props.menuItems[0].item);
    const [menuButtonIcon, setMenuButtonIcon] = useState(props.menuItems[0]?.iconClick);
    const [badgeTextMenuButton, setBadgeTextMenuButton] = useState(props.menuItems[0]?.badgeText);

    const handleClickMenuItem = (el) => {
        setMenuButton(el.item);
        el.onClick && el.onClick();
        el.iconClick && setMenuButtonIcon(el.iconClick);
        el.badgeText ? setBadgeTextMenuButton(el.badgeText) : setBadgeTextMenuButton(null)
    };

    return (
        <>
            <div className="relative">
                <Menu as="div">
                    <div className="dropDownIcon">
                        <Menu.Button
                            className={`flex flex-row justify-between ${props.shadow} m-auto w-full paragraphMediumRegular rounded-md focus:outline-none focus:ring-0 font-normal leading-5 appearance-none px-4 py-[14px] border ${props.boxStyle ?? "border-primary-500 bg-primary-50"
                                } h-12 items-center`}
                        >
                            <div className="flex flex-row items-center">
                                <div className={`${props.menuButtonIconStyle ?? "mobile:hidden"}`}>{menuButtonIcon}</div>
                                <span className={`${menuButtonIcon && "ml-2"} ${props.tabStyle} ${props.textColor ?? "text-primary-500"}`}>
                                    {props.fixedLabel && props.fixedLabel + " - "}
                                    {menuButton}
                                </span>
                                {badgeTextMenuButton && (
                                    <div
                                        className={`px-2.5 paragraphXSmallMedium mx-2 group-disabled:text-neutral-400 ${props.badgeTextStyle ?? "text-shades-50 bg-primary-500"
                                            } h-6 rounded-3xl py-0.5 disabled:bg-neutral-200`}
                                    >
                                        <span className="h-5">{badgeTextMenuButton}</span>
                                    </div>
                                )}
                            </div>

                            <DownArrow className="dropDownIconRotate" fill={props.dropDownIconFill ?? "#6C5DD3"} width={24} height={24} />
                        </Menu.Button>
                    </div>
                    <Transition
                        as={Fragment}
                        enter="transition ease-out duration-100"
                        enterFrom="transform opacity-0 scale-95"
                        enterTo="transform opacity-100 scale-100"
                        leave="transition ease-in duration-75"
                        leaveFrom="transform opacity-100 scale-100"
                        leaveTo="transform opacity-0 scale-95"
                    >
                        <Menu.Items className="absolute left-0 right-0 mt-2 px-4 py-2 border paragraphMediumRegular rounded-md shadow-shadowMedium bg-shades-50 font-normal z-50">
                            {props.menuItems.map((el, index) => (
                                <div className="pt-2 mb-2" key={index}>
                                    <Menu.Item>
                                        <div
                                            onClick={() => {
                                                handleClickMenuItem(el);
                                            }}
                                        >
                                            <div className="flex flex-row items-center">
                                                {el.icon}
                                                <span className={`${el.icon && "ml-2"} ${props.itemStyle ?? "paragraphMediumRegular"}`}>{el.item}</span>
                                                {el.badgeText && (
                                                    <div className="px-2.5 paragraphXSmallMedium mx-2 group-disabled:text-neutral-400 h-6 rounded-3xl py-0.5 disabled:bg-neutral-200 text-neutral-700 bg-neutral-200">
                                                        <span className="h-5">{el.badgeText}</span>
                                                    </div>
                                                )}
                                            </div>
                                        </div>
                                    </Menu.Item>
                                </div>
                            ))}
                        </Menu.Items>
                    </Transition>
                </Menu>
            </div>
        </>
    );
};
