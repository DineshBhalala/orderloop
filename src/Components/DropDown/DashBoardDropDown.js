import React, { Fragment, useState } from "react";
import { ReactComponent as DownArrow } from "../../Assets/chevron-down.svg";
import ToggleSwitch from "../ToggleSwitch/ToggleSwitch";
import { Menu, Transition } from "@headlessui/react";

export function DashBoardDropDown(props) {
  return (
    <>
      <div className="w-full flex flex-row justify-between items-center px-2 py-3 border border-neutral-300 rounded-md">
        <div className="flex flex-row items-center">
          {props.icon && (
            <>
              <div className="w-10 h-10 mx-2">
                <img src={props.icon} alt="" className="" />
              </div>
            </>
          )}
          <div className="ml-2 h-[44px]">
            <div className="paragraphOverlineSmall text-neutral-700 mb-1">
              {props.title}
            </div>
            <div className="paragraphXSmallRegular text-neutral-900">
              {props.content}
            </div>
          </div>
        </div>
        <div className="w-6 h-6 mr-2">
          <DownArrow />
        </div>
      </div>
    </>
  );
}

export function SelectRestaurantDropDown(props) {
  return (
    <>
      <div className="w-full flex flex-row justify-between items-center px-2 py-3 border border-neutral-300 rounded-md">
        <div className="flex flex-row items-center">
          <ToggleSwitch />
          {props.icon && (
            <>
              <div className="w-10 h-10 mx-2">
                <img src={props.icon} alt="" className="" />
              </div>
            </>
          )}
          <div className="ml-2 h-[44px]">
            <div className="paragraphOverlineSmall text-neutral-700 mb-1">
              {props.title}
            </div>
            <div className="paragraphXSmallRegular text-neutral-900">
              {props.content}
            </div>
          </div>
        </div>
        <div className="w-6 h-6 mr-2">
          <DownArrow />
        </div>
      </div>
    </>
  );
}

export const DropDownNotification = (props) => {
  const [menuButton, setMenuButton] = useState(
    props.menuItems ? props.menuItems[0] : "2 mins"
  );

  const menuItems = props.menuItems ?? [
    "Instant",
    "2 mins",
    "5 mins",
    "Custom date & time",
  ];

  return (
    <>
      <div className="relative">
        <Menu as="div">
          <div className="dropDownIcon">
            <Menu.Button className="flex flex-row justify-between m-auto w-full mobile:max-w-full rounded-md shadow-sm outline-none focus:border-primary-500 border py-3 focus:ring-4 focus:ring-primary-100 appearance-none px-4 border-neutral-300 h-12 paragraphSmallRegular">
              <div className="flex flex-row items-center">
                {!props.disable ? (
                  <span className="mobile:hidden mr-2">{props.menuIcon}</span>
                ) : (
                  <span className="mobile:hidden mr-2">
                    {props.disableMenuIcon}
                  </span>
                )}
                <span className={`${props.disable && "text-neutral-300"}`}><span className={`${props.truncateText}`}>{menuButton}</span></span>
              </div>
              <DownArrow
                className="dropDownIconRotate"
                fill={`${props.disable && "#D3D2D8"}`}
                width={20}
                height={20}
              />
            </Menu.Button>
          </div>
          <Transition
            as={Fragment}
            enter="transition ease-out duration-100"
            enterFrom="transform opacity-0 scale-95"
            enterTo="transform opacity-100 scale-100"
            leave="transition ease-in duration-75"
            leaveFrom="transform opacity-100 scale-100"
            leaveTo="transform opacity-0 scale-95"
          >
            <Menu.Items className="absolute left-0 right-0 mt-2 px-4 py-2 border paragraphSmallRegular rounded-md shadow-shadowMedium bg-shades-50 font-normal z-50 w-full">
              {menuItems.map((el, index) => {
                return (
                  <Menu.Item key={index}>
                    <div
                      className="pt-2 mb-2 group cursor-pointer"
                      onClick={() => setMenuButton(el)}
                    >
                      <span className="active:paragraphSmallMedium group-hover:text-primary-500">
                        {el}
                      </span>
                    </div>
                  </Menu.Item>
                );
              })}
            </Menu.Items>
          </Transition>
        </Menu>
      </div>
    </>
  );
};
