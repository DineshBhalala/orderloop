import React, { useState } from "react";
import { Menu, Transition } from "@headlessui/react";
import { Fragment } from "react";
import { ReactComponent as DownArrow } from "../../Assets/chevron-down.svg";
import AvatarsIcon from "../Avatar/AvatarsIcon";
import AvatarImage from "../../Assets/avatar-image.png";

const DropDownAvatar = (props) => {
    const menuItems = props.menuItems ? props.menuItems : [`Ghanshyam P.`, `Dinesh B.`, `Abhishek R.`];
    const [MenuButton, setMenuButton] = useState("Shyam T.");
    return (
        <>
            <div className="relative pl-4">
                <Menu as="div" className="flex dropDownIcon">
                    <Menu.Button className="flex items-center focus:outline-none">
                        <AvatarsIcon widthOuter="10" heightOuter="10" imageIcon={AvatarImage} colorIcon="[#22C55E]" widthIcon="[14px]" heightIcon="[14px]" />
                        <div>
                            <label className="paragraphMediumMedium">{MenuButton}</label>
                            <div className="paragraphXSmallItalic text-neutral-500 text-left">Caption</div>
                        </div>
                        <DownArrow className="dropDownIconRotate ml-3" width={`${props.widthIcon}`} height={`${props.heightIcon}`} />
                    </Menu.Button>
                    <Transition
                        as={Fragment}
                        enter="transition ease-out duration-100"
                        enterFrom="transform opacity-0 scale-95"
                        enterTo="transform opacity-100 scale-100"
                        leave="transition ease-in duration-75"
                        leaveFrom="transform opacity-100 scale-100"
                        leaveTo="transform opacity-0 scale-95"
                    >
                        <Menu.Items className={`absolute right-0 w-max mt-12 px-4 py-2 border border-neutral-300 rounded-md shadow-shadowMedium bg-shades-50 focus:outline-none`}>
                            <div className="pt-2 mb-2">
                                <Menu.Item>
                                    <div
                                        onClick={() => {
                                            setMenuButton("Shyam T.");
                                        }}
                                        className="paragraphXSmallRegular leading-normal cursor-pointer"
                                    >
                                        Shyam T.
                                    </div>
                                </Menu.Item>
                            </div>
                            {menuItems.map((avatarList, index) => {
                                return (
                                    <div
                                        key={index}
                                        className="py-1.5 cursor-pointer"
                                        onClick={() => {
                                            setMenuButton(avatarList);
                                        }}
                                    >
                                        <Menu.Item>
                                            <div className="paragraphXSmallRegular leading-normal">{avatarList}</div>
                                        </Menu.Item>
                                    </div>
                                );
                            })}
                        </Menu.Items>
                    </Transition>
                </Menu>
            </div>
        </>
    );
};

export default DropDownAvatar;
