import React from "react";
export const DefaultBreadcrumbs = (props) => {
  return (
    <>
      <div className="flex flex-row items-center">
        <div>{props.icon}</div>
        <span
          className={`pl-1 paragraphSmallMedium active:underline-offset-2 active:underline active:text-primary-500 hover:text-neutral-900 ${
            props.disable ? "text-neutral-300" : "text-neutral-700"
          }`}
        >
          {props.mainTab}
        </span>
        {props.tab1 && (
          <>
            <span className="mx-2"> / </span>
            <span
              className={`paragraphSmallMedium active:underline-offset-2 active:underline active:text-primary-500 hover:text-neutral-900 ${
                props.disable ? "text-neutral-300" : "text-neutral-700"
              }`}
            >
              {props.tab1}
            </span>
          </>
        )}
        {props.tab2 && (
          <>
            <span className="mx-2"> / </span>
            <span
              className={`paragraphSmallMedium active:underline-offset-2 active:underline active:text-primary-500 hover:text-neutral-900 ${
                props.disable ? "text-neutral-300" : "text-neutral-700"
              }`}
            >
              {props.tab2}
            </span>
          </>
        )}
        {props.tab3 && (
          <>
            <span className="mx-2"> / </span>
            <span
              className={`paragraphSmallMedium active:underline-offset-2 active:underline active:text-primary-500 hover:text-neutral-900 ${
                props.disable ? "text-neutral-300" : "text-neutral-700"
              }`}
            >
              {props.tab3}
            </span>
          </>
        )}
      </div>
    </>
  );
};
