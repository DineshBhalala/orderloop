import React from "react";
import { ReactComponent as Close } from "../../Assets/close.svg";
import { ReactComponent as SuccessTickIcon } from "../../Assets/success-tick.svg";
import { ReactComponent as CardIcon } from "../../Assets/card.svg";
import { ReactComponent as LeftArrow } from "../../Assets/chevron-down.svg";
import { ReactComponent as VegetableIcon } from "../../Assets/vegetable-icon.svg";
import { ReactComponent as OrdersIcon } from "../../Assets//orders.svg";
import { ReactComponent as LoyaltyCashbackIcon } from "../../Assets/loyalty-cashback.svg";
import { ReactComponent as ReimbursementIcon } from "../../Assets/reimbursement.svg";
import { LargePrimaryButton } from "../Buttons/Button";

export default function CustomerOrderDetails(props) {
    return (
        <>
            <div className="fixed bg-black bg-opacity-50 inset-0 md:z-[9] z-50 flex justify-center items-center overflow-auto md:bg-white md:relative">
                <div className="max-w-[652px] w-full rounded-xl md:rounded-none bg-shades-50 px-8 py-6 m-auto md:max-w-full md:px-4 md:py-4">
                    <div className="hidden md:flex paragraphMediumMedium flex-row mb-4 cursor-pointer" onClick={() => props.handleClickClose()}>
                        <LeftArrow className="rotate-90" />
                        <span className="ml-1">{props.headerTabletScreen}</span>
                    </div>

                    <div className="flex flex-row justify-between items-center mb-2 pb-4 border-b border-neutral-300 md:items-start">
                        <div>
                            <span className="paragraphLargeMedium">Customer order details</span>
                            <div className="flex flex-row items-center">
                                <span className="paragraphMediumItalic text-neutral-500">Order #SAWQ</span>
                                {props.pageName === "loyaltyCashback" || (props.pageName === "pos" && props.orderStatus !== "refundInitiated") ? (
                                    <>
                                        <div className="w-1 h-1 rounded-full mx-3 bg-neutral-500 md:hidden" />
                                        <div className="flex flex-row items-center md:hidden">
                                            <SuccessTickIcon />
                                            <span className="paragraphSmallRegular ml-1.5">Delivered</span>
                                        </div>
                                    </>
                                ) : (
                                    props.pageName === "billPayment" && (
                                        <div className="flex flex-row items-center md:hidden">
                                            <div className="w-1 h-1 rounded-full mx-3 bg-neutral-500 md:hidden" />
                                            <span className="paragraphMediumItalic text-primary-500">Code: 7333</span>
                                        </div>
                                    )
                                )}
                                {props.orderStatus === "refundInitiated" && (
                                    <>
                                        <div className="w-1 h-1 rounded-full mx-3 bg-neutral-500 md:hidden" />
                                        <div className="flex flex-row items-center md:hidden">
                                            <ReimbursementIcon />
                                            <span className="paragraphSmallRegular ml-1.5">Refund initiated</span>
                                        </div>
                                    </>
                                )}
                            </div>
                        </div>
                        <div className="md:hidden cursor-pointer" onClick={() => props.handleClickClose()}>
                            <Close />
                        </div>
                        <div className="flex-row items-center hidden md:flex">
                            {props.pageName === "loyaltyCashback" ? (
                                <>
                                    <SuccessTickIcon />
                                    <span className="paragraphSmallRegular ml-1.5">Delivered</span>
                                </>
                            ) : (
                                props.pageName === "billPayment" && <span className="mt-0.5 paragraphMediumItalic text-primary-500">Code: 7333</span>
                            )}
                        </div>
                    </div>

                    <div className="flex flex-row justify-between pb-4 border-b border-neutral-300 mb-4 md:block">
                        <div>
                            <div className="mb-1.5 pt-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Order bill name:</span>
                                <span className="paragraphMediumRegular ml-2">Mahendra Bahubali</span>
                            </div>

                            <div className="mb-1.5 pt-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Order date:</span>
                                <span className="paragraphMediumRegular ml-2">18 Nov 2022</span>
                            </div>

                            <div className="mb-1.5 pt-1.5 flex flex-row items-center">
                                <span className="paragraphMediumMedium text-neutral-500">Order type:</span>
                                <span className="paragraphMediumRegular ml-2 items-center flex flex-row">
                                    <OrdersIcon className="mr-1" />
                                    Take away
                                </span>
                            </div>

                            <div className="pt-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Order taken by:</span>
                                <span className="paragraphMediumRegular ml-2">John Doe</span>
                            </div>
                            {props.pageName === "pos" && (
                                <div className="pt-3">
                                    <span className="paragraphMediumMedium text-neutral-500">Ordered via:</span>
                                    <span className="paragraphMediumRegular ml-2">iOS device</span>
                                </div>
                            )}
                        </div>
                        <div>
                            <div className="mb-1.5 pt-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Customer's order:</span>
                                <span className="paragraphMediumRegular ml-2">713</span>
                            </div>

                            <div className="mb-1.5 pt-1.5 flex flex-row items-center">
                                <span className="paragraphMediumMedium text-neutral-500">Order time:</span>
                                <span className="paragraphMediumRegular ml-2 items-center flex flex-row">17:59 PM</span>
                            </div>

                            <div className="mb-1.5 pt-1.5 flex flex-row items-center">
                                <span className="paragraphMediumMedium text-neutral-500">Payment method:</span>
                                <span className="paragraphMediumRegular ml-2 items-center flex flex-row">
                                    <CardIcon className="mr-1" />
                                    Debit card
                                </span>
                            </div>

                            <div className="pt-1.5">
                                <span className="paragraphMediumMedium text-neutral-500">Dishes ordered:</span>
                                <span className="paragraphMediumRegular ml-2">12</span>
                            </div>
                            {props.pageName === "pos" && (
                                <div className="pt-3">
                                    <span className="paragraphMediumMedium text-neutral-500">Platform:</span>
                                    <span className="paragraphMediumRegular ml-2">Dine-in</span>
                                </div>
                            )}
                        </div>
                    </div>

                    <div className={`flex flex-row justify-between mb-4 ${props.pageName === "loyaltyCashback" && "md:hidden"}`}>
                        <span className="paragraphMediumMedium">Ordered dishes</span>
                        <span className="paragraphMediumMedium">Amount</span>
                    </div>
                    <div className="border-b border-neutral-300 mb-4 pb-2">
                        <div className={`scrollbarStyle md:[&::-webkit-scrollbar]:hidden overflow-auto h-[159px] pr-3 md:pr-0 ${props.pageName === "loyaltyCashback" && "md:hidden"}`}>
                            <div className="flex flex-row justify-between paragraphMediumRegular">
                                <div className="flex flex-row items-center md:items-start w-full">
                                    <span className="md:min-w-[23px]">2 x</span>
                                    <span className="mx-2">Double Cheese Margherita Pizza</span>
                                    <span className="md:pt-1 md:ml-auto">
                                        <VegetableIcon />
                                    </span>
                                </div>
                                <span className="md:min-w-[104px] mobile:min-w-[90px] md:text-right">₹1,118.00/-</span>
                            </div>

                            <div className="flex flex-row justify-between paragraphMediumRegular mt-3">
                                <div className="flex flex-row items-center md:items-start w-full">
                                    <span className="md:min-w-[23px]">1 x</span>
                                    <span className="mx-2 md:max-w-[184.4px]">Indi Tandoori Paneer</span>
                                    <span className="md:pt-1 md:ml-auto">
                                        <VegetableIcon />
                                    </span>
                                </div>
                                <span className="md:min-w-[104px] mobile:min-w-[90px] md:text-right">₹529.00/-</span>
                            </div>

                            <div className="flex flex-row justify-between paragraphMediumRegular mt-3">
                                <div className="flex flex-row items-center md:items-start w-full">
                                    <span className="md:min-w-[23px]">1 x</span>
                                    <span className="mx-2 md:max-w-[184.4px]">Mexican Green Wave</span>
                                    <span className="md:pt-1 md:ml-auto">
                                        <VegetableIcon />
                                    </span>
                                </div>
                                <span className="md:min-w-[104px] mobile:min-w-[90px] md:text-right">₹529.00/-</span>
                            </div>

                            <div className="flex flex-row justify-between paragraphMediumRegular mt-3">
                                <div className="flex flex-row items-center md:items-start w-full">
                                    <span className="md:min-w-[23px]">2 x</span>
                                    <span className="mx-2 md:max-w-[184.4px]">Peppy Paneer</span>
                                    <span className="md:pt-1 md:ml-auto">
                                        <VegetableIcon />
                                    </span>
                                </div>
                                <span className="md:min-w-[104px] mobile:min-w-[90px] md:text-right">₹3,118.00/-</span>
                            </div>

                            <div className="flex flex-row justify-between paragraphMediumRegular mt-3">
                                <div className="flex flex-row items-center md:items-start w-full">
                                    <span className="md:min-w-[23px]">1 x</span>
                                    <span className="mx-2 md:max-w-[184.4px]">Mexican Green Wave</span>
                                    <span className="md:pt-1 md:ml-auto">
                                        <VegetableIcon />
                                    </span>
                                </div>
                                <span className="md:min-w-[104px] mobile:min-w-[90px] md:text-right">₹529.00/-</span>
                            </div>

                            <div className="flex flex-row justify-between paragraphMediumRegular mt-3">
                                <div className="flex flex-row items-center md:items-start w-full">
                                    <span className="md:min-w-[23px]">2 x</span>
                                    <span className="mx-2 md:max-w-[184.4px]">Peppy Paneer</span>
                                    <span className="md:pt-1 md:ml-auto">
                                        <VegetableIcon />
                                    </span>
                                </div>
                                <span className="md:min-w-[104px] mobile:min-w-[90px] md:text-right">₹3,118.00/-</span>
                            </div>
                        </div>
                    </div>
                    {props.pageName === "pos" && (
                        <div className="paragraphMediumRegular">
                            <div className="flex flex-row justify-between">
                                <span>Gross total</span>
                                <span>₹4,518.00/-</span>
                            </div>
                            <div className="flex flex-row justify-between my-3">
                                <span>Other charges & taxes</span>
                                <span>₹718.00/-</span>
                            </div>
                            <div className="flex flex-row justify-between mb-3">
                                <span>Delivery charge</span>
                                <span>₹118.00/-</span>
                            </div>
                        </div>
                    )}
                    <div className={`flex flex-row justify-between ${props.orderStatus !== "refundInitiated" && "pb-4 border-b border-neutral-300 mb-4"} md:hidden`}>
                        <div className="flex flex-row items-center">
                            <h3 className="paragraphMediumMedium">Total bill amount</h3>
                            <span className="text-success-600 leading-3 text-[10px] border font-medium px-2 py-1 border-success-600 rounded bg-success-50 h-5 ml-2">Paid</span>
                        </div>
                        <span className="paragraphMediumMedium">₹5,325.00/-</span>
                    </div>

                    <div className={`pb-4 border-b border-neutral-300 mb-4 ${(props.pageName === "customerInformation" || props.pageName === "billPayment" || props.pageName === "pos") && "hidden"}`}>
                        <div className="paragraphMediumMedium flex flex-row justify-between pb-4">
                            <div className="flex flex-row items-center">
                                <LoyaltyCashbackIcon />
                                <span className="ml-1">Cashback redeemed</span>
                            </div>
                            <div>₹200/-</div>
                        </div>

                        <div className="paragraphMediumMedium flex flex-row justify-between">
                            <div className="flex flex-row items-center">
                                <LoyaltyCashbackIcon />
                                <span className="ml-1">Cashback earned</span>
                            </div>
                            <div>₹500/-</div>
                        </div>
                    </div>

                    <div className={`justify-between flex border-b border-neutral-300 pb-4 mb-4 ${(props.pageName === "billPayment" || props.orderStatus === "refundInitiated") && "hidden"}`}>
                        <span className="paragraphMediumMedium">Order feedback</span>
                        <span className="paragraphMediumRegular text-primary-500">Show</span>
                    </div>

                    <div className={`justify-between flex border-b border-neutral-300 pb-4 mb-4 ${(props.pageName === "billPayment" || props.orderStatus === "refundInitiated") && "hidden"}`}>
                        <span className="paragraphMediumMedium">Rider feedback</span>
                        <span className="paragraphMediumRegular text-primary-500">Show</span>
                    </div>

                    <div className={`justify-between flex ${props.pageName === "billPayment" && "border-b border-neutral-300 pb-4 mb-12"} ${props.orderStatus === "refundInitiated" && "hidden"}`}>
                        <span className="paragraphMediumMedium">Order history</span>
                        <span className="paragraphMediumRegular text-primary-500">Show</span>
                    </div>

                    <div className={`${props.pageName !== "billPayment" && "hidden"}`}>
                        <div className="md:hidden flex flex-row mb-2">
                            <div className="w-1/2 mr-2">
                                <LargePrimaryButton name="Print acknowledgement" />
                            </div>
                            <div className="w-1/2 ml-2">
                                <LargePrimaryButton name="Refund" />
                            </div>
                        </div>
                        <div className="fixed bottom-0 flex-row hidden md:flex w-full pt-2 shadow-dropShadow bg-white pb-1 px-4 -ml-4">
                            <div className="mr-[7.5px] w-1/2">
                                <LargePrimaryButton containerStyle="md:max-w-[130px]" name="Print acknowledgement" />
                            </div>
                            <div className="ml-[7.5px] w-1/2">
                                <LargePrimaryButton name="Refund" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
