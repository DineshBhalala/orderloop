import React, { useState } from "react";
import DropDown from "../DropDown/DropDown";
import { ReactComponent as CloseIcon } from "../../Assets/close.svg";
import ToggleSwitch from "../ToggleSwitch/ToggleSwitch";

export default function TimeSlotBanner(props) {
  const [slots, setSlots] = useState([{ id: 1 }]);

  const addSlot = () => {
    const newSlot = { id: slots.length + 1 };
    setSlots([...slots, newSlot]);
  };

  const removeSlot = (id) => {
    const updatedSlots = slots.filter((slot) => slot.id !== id);
    setSlots(updatedSlots);
  };

  const renderSlots = () => {
    return slots.map((slot) => (
      <div className="flex flex-row items-center mt-2 justify-between" key={slot.id}>
        <div className="w-full relative">
          <DropDown dropdownLabel="12:00 PM" shadow="shadow-smallDropDownShadow" />
        </div>
        <span className="mx-[17px] md:mx-[13px] mobile:mx-2">to</span>

        <div className="w-full relative">
          <DropDown dropdownLabel="5:00 PM" shadow="shadow-smallDropDownShadow" />
        </div>
        <div className="min-w-[24px] ml-[17px] mobile:ml-2 cursor-pointer" onClick={() => removeSlot(slot.id)}>
          <CloseIcon stroke="#EF4444" className="cursor-pointer" />
        </div>
      </div>
    ));
  };

  const [isUnAvailable, setIsUnavailable] = useState(false);

  const showStatus = (e) => {
    setIsUnavailable(e);
    !e && setSlots([{ id: 1 }]);
  };

  return (
    <div>
      <div className="flex flex-row items-center justify-between">
        <div className="flex flex-row items-center">
          <ToggleSwitch showStatus={showStatus} />
          <span className="paragraphSmallMedium pl-2">{props.dayLabel}</span>
        </div>
        <span className={`paragraphSmallMedium ${isUnAvailable ? "text-primary-500 cursor-pointer" : "text-neutral-500"} cursor-pointer`} onClick={() => isUnAvailable && addSlot()}>
          {!isUnAvailable ? "Unavailable" : "+ Add slot"}
        </span>
      </div>
      {isUnAvailable && renderSlots()}
    </div>
  );
}

export const TimerSlotStoreSetting = () => {
  const [slots, setSlots] = useState([]);

  const addSlot = () => {
    const newSlot = { id: slots.length + 1 };
    setSlots([...slots, newSlot]);
  };

  const removeSlot = (id) => {
    const updatedSlots = slots.filter((slot) => slot.id !== id);
    setSlots(updatedSlots);
  };

  const renderSlots = () => {
    return slots.map((slot) => (
      <div className="flex flex-row justify-between items-center relative mt-2" key={slot.id}>
        <div className="flex flex-col w-full">
          <span className="paragraphSmallMedium">Start time</span>
          <div className="w-full relative mt-1">
            <DropDown dropdownLabel="12:00 PM" shadow="shadow-smallDropDownShadow" height="h-[52px]" menuButtonStyle="paragraphMediumRegular" />
          </div>
        </div>
        <span className="pt-6 px-4">to</span>
        <div className="flex flex-col w-full">
          <span className="paragraphSmallMedium">End time</span>
          <div className="w-full relative mt-1">
            <DropDown dropdownLabel="12:00 PM" shadow="shadow-smallDropDownShadow" height="h-[52px]" menuButtonStyle="paragraphMediumRegular" />
          </div>
        </div>
        <div
          className="min-w-[24px] pt-7 ml-4 cursor-pointer"
          onClick={() => {
            removeSlot(slot.id);
          }}>
          <CloseIcon stroke="#EF4444" className="cursor-pointer" />
        </div>
        <div className="absolute right-0 top-0 cursor-pointer" onClick={addSlot}>
          <span className="paragraphSmallMedium text-primary-500 cursor-pointer">+ Add slot</span>
        </div>
      </div>
    ));
  };

  return (
    <>
      <div className="flex flex-row justify-between items-center relative">
        <div className="flex flex-col w-full">
          <span className="paragraphSmallMedium">Start time</span>
          <div className="w-full relative mt-1">
            <DropDown dropdownLabel="12:00 PM" shadow="shadow-smallDropDownShadow" height="h-[52px]" menuButtonStyle="paragraphMediumRegular" />
          </div>
        </div>
        <span className="pt-6 px-4">to</span>
        <div className="flex flex-col w-full">
          <span className="paragraphSmallMedium">End time</span>
          <div className="w-full relative mt-1">
            <DropDown dropdownLabel="12:00 PM" shadow="shadow-smallDropDownShadow" height="h-[52px]" menuButtonStyle="paragraphMediumRegular" />
          </div>
        </div>
        <div className="min-w-[24px] pt-7 ml-4">
          <CloseIcon stroke="#EF4444" className="cursor-pointer" />
        </div>
        <div className="absolute right-0 top-0 cursor-pointer" onClick={addSlot}>
          <span className="paragraphSmallMedium text-primary-500 cursor-pointer">+ Add slot</span>
        </div>
      </div>
      {renderSlots()}
    </>
  );
};
