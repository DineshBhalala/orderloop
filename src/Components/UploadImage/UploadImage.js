import React, { useState } from "react";
import { ReactComponent as Close } from "../../Assets/close.svg";
import { DefaultInputField } from "../InputField/InputField";
import DropDown from "../DropDown/DropDown";
import { LargeDestructiveButton, LargePrimaryButton } from "../Buttons/Button";
import { ReactComponent as EditIcon } from "../../Assets/edit.svg";
import { ReactComponent as ReplaceIcon } from "../../Assets/replace.svg";
import { ReactComponent as DeleteIcon } from "../../Assets/trash.svg";
import { ReactComponent as CropIcon } from "../../Assets/crop.svg";
import { ReactComponent as FlipIcon } from "../../Assets/flip-vertical.svg";
import { ReactComponent as RotateIcon } from "../../Assets/rotate.svg";
import { ReactComponent as UndoIcon } from "../../Assets/undo.svg";
import { ReactComponent as LeftArrow } from "../../Assets/chevron-down.svg";
import imageToEdit from "../../Assets/mediaLibrary/image-for-editing.png";

function UploadImage(props) {
  const [windowWidth, setWindowWidth] = useState(window.innerWidth);
  const handleResize = () => {
    setWindowWidth(window.innerWidth);
  };
  window.onresize = handleResize;

  const selectType = [
    "Categories (1280px x 720px)",
    "Dishes (720px x 720px)",
    "Banners (1280px x 720px)",
    "Variants (1280px x 720px)",
    "Notifications (1280px x 720px)",
    "Intro logo (1280px x 720px)",
    "Footer (1280px x 720px)",
    "Riders (1280px x 720px)",
  ];
  const selectSubCategory = ["Non-veg-pizza", "Garlic-bread", "Veg-pizza", "Beverages", "Sides", "Paratha-pizza"];

  const [showImage, setShowImage] = useState(false);
  const [showImageEditor, setShowImageEditor] = useState(false);

  const handleClickSelectFile = () => {
    setShowImage(!showImage);
  };

  const handleClickEditBtn = () => {
    setShowImageEditor(!showImageEditor);
  };

  return (
    <>
      <div className="w-full m-auto bg-white min-h-[1024px]">
        <div className={`fixed ${props.mobileBg ?? "bg-black"} m-auto bg-opacity-50 inset-0 z-[101] md:z-[9] overflow-auto md:relative px-8 md:px-0 flex`}>
          <div
            className={`min-h-[904px] lg:min-h-[899px] max-w-[1123px] lg:max-w-[701px] w-full rounded-xl bg-shades-50 px-8 py-6 m-auto relative md:rounded-none md:max-w-full mx-auto ${
              props.mobilePadding ?? "md:px-4 md:py-4"
            }`}>
            <div className="hidden md:flex flex-row mb-4 cursor-pointer" onClick={() => props.showHideUploadImagePage()}>
              <div className="w-6 h-6">
                <LeftArrow className="rotate-90" />
              </div>
              <span className="ml-1 paragraphMediumMedium">Back to {props.backToWhichPageName}</span>
            </div>

            <div className="flex flex-row justify-between items-center mb-6 md:hidden">
              <div className="flex flex-row items-center">
                <div className={`${!props.isShowBackArrow && "hidden"}`}>
                  <LeftArrow className="rotate-90 mr-4" />
                </div>
                <div>
                  <span className="paragraphLargeMedium">Upload new image</span>
                  <p className="paragraphMediumItalic text-neutral-500">Allowed file types: .jpeg, .jpg, .png, .webp.</p>
                </div>
              </div>
              <div className="cursor-pointer" onClick={() => props.showHideUploadImagePage()}>
                <Close />
              </div>
            </div>

            <div className={`mb-6 ${!showImageEditor && "hidden"}`}>
              <div className="lg:hidden flex flex-row">
                <div className="mr-2">
                  <LargePrimaryButton name="Crop" leftIconDefault={<CropIcon stroke="#ffffff" />} leftIconClick={<CropIcon stroke="#C4BEED" />} />
                </div>
                <div className="mx-2">
                  <LargePrimaryButton
                    name="Flip horizontal"
                    leftIconDefault={<FlipIcon stroke="#ffffff" className="rotate-90" />}
                    leftIconClick={<FlipIcon stroke="#C4BEED" className="rotate-90" />}
                  />
                </div>
                <div className="mx-2">
                  <LargePrimaryButton name="Flip vertical" leftIconDefault={<FlipIcon stroke="#ffffff" />} leftIconClick={<FlipIcon stroke="#C4BEED" />} />
                </div>
                <div className="mx-2">
                  <LargePrimaryButton name="Rotate left" leftIconDefault={<RotateIcon stroke="#ffffff" />} leftIconClick={<RotateIcon stroke="#C4BEED" />} />
                </div>
                <div className="mx-2">
                  <LargePrimaryButton
                    name="Rotate right"
                    leftIconDefault={<RotateIcon stroke="#ffffff" className="-scale-x-100" />}
                    leftIconClick={<RotateIcon stroke="#C4BEED" className="-scale-x-100" />}
                  />
                </div>
              </div>

              <div className="hidden lg:flex flex-row">
                <div className="mr-2">
                  <LargePrimaryButton leftIconDefault={<CropIcon stroke="#ffffff" />} leftIconClick={<CropIcon stroke="#C4BEED" />} />
                </div>
                <div className="mx-2">
                  <LargePrimaryButton leftIconDefault={<FlipIcon stroke="#ffffff" className="rotate-90" />} leftIconClick={<FlipIcon stroke="#C4BEED" className="rotate-90" />} />
                </div>
                <div className="mx-2">
                  <LargePrimaryButton leftIconDefault={<FlipIcon stroke="#ffffff" />} leftIconClick={<FlipIcon stroke="#C4BEED" />} />
                </div>
                <div className="mx-2">
                  <LargePrimaryButton leftIconDefault={<RotateIcon stroke="#ffffff" />} leftIconClick={<RotateIcon stroke="#C4BEED" />} />
                </div>
                <div className="mx-2">
                  <LargePrimaryButton
                    leftIconDefault={<RotateIcon stroke="#ffffff" className="-scale-x-100" />}
                    leftIconClick={<RotateIcon stroke="#C4BEED" className="-scale-x-100" />}
                  />
                </div>
              </div>
            </div>

            <div className="flex flex-row justify-between lg:block">
              <div className={`max-w-[668px] w-full lg:max-w-full`}>
                <div
                  className={`h-[375px] lg:h-[339px] w-full border rounded-2xl border-neutral-500 border-dashed flex flex-col justify-center items-center mr-2 lg:max-w-full ${
                    showImage && "hidden"
                  } md:w-full`}>
                  <h3 className="paragraphLargeMedium">Drop files to upload</h3>
                  <div className="my-2">
                    <span className="paragraphSmallRegular">or</span>
                  </div>
                  <button className="paragraphMediumRegular px-4 border border-neutral-300 rounded-md py-3 cursor-pointer" onClick={handleClickSelectFile}>
                    Select files
                  </button>
                  <p className="paragraphMediumItalic text-neutral-500 mt-4">Maximum file size allowed: 50 MB</p>
                </div>
                <div className={`${!showImage && "hidden"}`}>
                  <img src={imageToEdit} alt="" className={`w-[591px] ${!showImageEditor && "mx-auto"}`} />
                  <div className={`flex flex-row mt-6 justify-center ${showImageEditor && "hidden"}`}>
                    <div className="max-w-[153px] cursor-pointer" onClick={handleClickEditBtn}>
                      <LargePrimaryButton
                        name={windowWidth > 500 ? "Edit image" : "Edit"}
                        leftIconDefault={<EditIcon stroke="#ffffff" />}
                        leftIconClick={<EditIcon stroke="#C4BEED" />}
                      />
                    </div>
                    <div className="mx-4 max-w-[184px] mobile:mx-1">
                      <LargePrimaryButton
                        name={windowWidth > 500 ? "Replace image" : "Replace"}
                        leftIconDefault={<ReplaceIcon stroke="#ffffff" />}
                        leftIconClick={<ReplaceIcon stroke="#C4BEED" />}
                      />
                    </div>
                    <div className="max-w-[173px]">
                      <LargeDestructiveButton
                        name={windowWidth > 500 ? "Delete image" : "Delete"}
                        leftIconDefault={<DeleteIcon stroke="#ffffff" />}
                        leftIconClick={<DeleteIcon stroke="#d7edeb" />}
                      />
                    </div>
                  </div>
                  <div className={`mt-6 flex flex-row w-full h-full ${!showImageEditor && "hidden"}`}>
                    <div className="w-[114px] mr-4">
                      <LargePrimaryButton name="Undo" leftIconDefault={<UndoIcon stroke="#ffffff" />} leftIconClick={<UndoIcon stroke="#C4BEED" />} />
                    </div>
                    <div className={`w-[112px]`}>
                      <LargePrimaryButton
                        name="Redo"
                        leftIconDefault={<UndoIcon stroke="#ffffff" className="-scale-x-100" />}
                        leftIconClick={<UndoIcon stroke="#C4BEED" className="-scale-x-100" />}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="ml-4 lg:ml-0">
                <div className={`relative lg:flex-row lg:justify-between ${showImageEditor ? "lg:hidden md:hidden hidden" : "md:block lg:flex"}`}>
                  <div className="pb-6 border-b border-neutral-300 lg:flex lg:flex-col lg:mt-6 lg:pr-[19px] lg:border-r lg:border-b-0 lg:pb-0 md:border-r-0 md:pr-0">
                    <div className="mb-[15px] w-[375px] lg:w-[311px] md:w-full">
                      <DefaultInputField label="Image title" placeholder="Enter image name" boxHeight="[44px]" />
                    </div>
                    <div className="mb-[15px]">
                      <DefaultInputField label="Alt title" placeholder="Enter image's alt name" boxHeight="[44px]" />
                    </div>
                    <div className="mb-[15px] w-full">
                      {props.selectType ?? <DropDown type="category" label="Select type" buttonTextColor="neutral-300" dropdownLabel="Select an image type" menuItems={selectType} />}
                    </div>
                    <div className="mb-[15px] w-full">
                      <DropDown type="subCategory" label="Select sub-category" buttonTextColor="neutral-300" dropdownLabel="Select image sub-category" menuItems={selectSubCategory} />
                    </div>
                    <div className="">
                      <DefaultInputField label="Image URL" Action="Copy URL" placeholder="Image URL" boxHeight="[44px]" />
                    </div>
                  </div>
                  <div className="border-neutral-300 mt-6 lg:flex lg:flex-col lg:mt-[49px] lg:ml-[18px] md:mt-4 md:mb-6 md:ml-0">
                    <div className="border-b pb-6 md:border-b-0">
                      <div className="mb-4 paragraphMediumRegular">
                        <span className="text-neutral-400">Uploaded on:</span>
                        <span className="ml-2">11 November, 2022</span>
                      </div>
                      <div className="mb-4 paragraphMediumRegular">
                        <span className="text-neutral-400">Uploaded by:</span>
                        <span className="ml-2 text-primary-500">Sarthak Kanchan</span>
                      </div>
                      <div className="mb-4 paragraphMediumRegular">
                        <span className="text-neutral-400">File type:</span>
                        <span className="ml-2">Image/jpeg</span>
                      </div>
                      <div className="mb-4 paragraphMediumRegular">
                        <span className="text-neutral-400">File size:</span>
                        <span className="ml-2">5 MB</span>
                      </div>
                      <div className="mb-4 paragraphMediumRegular">
                        <span className="text-neutral-400">Original dimensions:</span>
                        <span className="ml-2">1400px x 720px</span>
                      </div>
                      <div className="paragraphMediumRegular">
                        <span className="text-neutral-400">Cropped dimensions:</span>
                        <span className="ml-2">1280px x 720px</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="flex flex-row w-[375px] absolute bottom-6 right-8 lg:flex-none lg:hidden">
                  <button className="mr-[9px] max-w-[183px] h-12 w-full paragraphMediumMedium text-neutral-500 border border-neutral-300 rounded-md">Cancel</button>
                  <div className="max-w-[183px] w-full">
                    <LargePrimaryButton name="Save image" />
                  </div>
                </div>
                <div className="absolute bottom-6 right-8 hidden lg:block md:hidden">
                  <div className="lg:w-[289px] w-full mb-[19px]">
                    <LargePrimaryButton name="Save image" />
                  </div>
                  <button className="h-12 w-full paragraphMediumMedium text-neutral-500 border border-neutral-300 rounded-md lg:max-w-[289px]">Cancel</button>
                </div>
              </div>
            </div>
          </div>
          <div className="fixed bottom-0 flex-row hidden md:flex w-full px-4 pt-2 shadow-dropShadow bg-white pb-1 md:left-0 md:right-0">
            <button className="border-neutral-300 border rounded-md w-1/2 mr-[7.5px]">Cancel</button>
            <div className="ml-[7.5px] w-1/2">
              <LargePrimaryButton name="Save image" />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default UploadImage;
