import React from 'react';
import { ReactComponent as Avatarplaceholder } from '../../Assets/avatar-placeholder.svg';
import { ReactComponent as AvatarInitials } from '../../Assets/avatar-initials.svg';
import AvatarImage from '../../Assets/avatar-image.png';

export const XxLargeplaceholder = () => {
  return (
    <>
      <div className='w-16 h-16 relative'>
        <Avatarplaceholder />
        <div className='bg-[#22C55E] w-5 h-5 rounded-full border-2 border-white absolute -right-0.5 -bottom-0.5'></div>
      </div>
    </>
  )
}

export const XxLargeInitials = () => {
  return (
    <>
      <div className="w-16 h-16 relative">
        <AvatarInitials />
        <div className="bg-[#22C55E] w-5 h-5 rounded-full border-2 border-white absolute -right-0.5 -bottom-0.5"></div>
      </div>
    </>
  );
};

export const XxLargeImage = () => {
  return (
    <>
      <div className="w-16 h-16 relative">
        <img src={AvatarImage} alt="Avatar Image" />
        <div className="bg-[#22C55E] w-5 h-5 rounded-full border-2 border-white absolute -right-0.5 -bottom-0.5"></div>
      </div>
    </>
  );
};


export const XLargeplaceholder = () => {
  return (
    <>
      <div className='w-14 h-14 relative'>
        <Avatarplaceholder className='w-full h-auto' />
        <div className='bg-[#22C55E] w-[18px] h-[18px] rounded-full border-2 border-white absolute -right-0.5 -bottom-0.5'></div>
      </div>
    </>
  )
}

export const XLargeInitials = () => {
  return (
    <>
      <div className="w-14 h-14 relative">
        <AvatarInitials className="w-full h-auto" />
        <div className="bg-[#22C55E] w-[18px] h-[18px] rounded-full border-2 border-white absolute -right-0.5 -bottom-0.5"></div>
      </div>
    </>
  );
};

export const XLargeImage = () => {
  return (
    <>
      <div className="w-14 h-14 relative">
        <img src={AvatarImage} className="w-full h-auto" alt="Avatar Image" />
        <div className="bg-[#22C55E] w-[18px] h-[18px] rounded-full border-2 border-white absolute -right-0.5 -bottom-0.5"></div>
      </div>
    </>
  );
};


export const Largeplaceholder = () => {
  return (
    <>
      <div className='w-12 h-12 relative'>
        <Avatarplaceholder className='w-full h-auto' />
        <div className='bg-[#22C55E] w-4 h-4 rounded-full border-2 border-white absolute -right-0.5 -bottom-0.5'></div>
      </div>
    </>
  )
}

export const LargeInitials = () => {
  return (
    <>
      <div className="w-12 h-12 relative">
        <AvatarInitials className="w-full h-auto" />
        <div className="bg-[#22C55E] w-4 h-4 rounded-full border-2 border-white absolute -right-0.5 -bottom-0.5"></div>
      </div>
    </>
  );
};

export const LargeImage = () => {
  return (
    <>
      <div className="w-12 h-12 relative">
        <img src={AvatarImage} className="w-full h-auto" alt="Avatar Image" />
        <div className="bg-[#22C55E] w-4 h-4 rounded-full border-2 border-white absolute -right-0.5 -bottom-0.5"></div>
      </div>
    </>
  );
};

export const Mediumplaceholder = () => {
  return (
    <>
      <div className='w-10 h-10 relative'>
        <Avatarplaceholder className='w-full h-auto' />
        <div className='bg-[#22C55E] w-[14px] h-[14px] rounded-full border-2 border-white absolute -right-0.5 -bottom-0.5'></div>
      </div>
    </>
  )
}

export const MediumInitials = () => {
  return (
    <>
      <div className="w-10 h-10 relative">
        <AvatarInitials className="w-full h-auto" />
        <div className="bg-[#22C55E] w-[14px] h-[14px] rounded-full border-2 border-white absolute -right-0.5 -bottom-0.5"></div>
      </div>
    </>
  );
};

export const MediumImage = () => {
  return (
    <>
      <div className="w-10 h-10 relative">
        <img src={AvatarImage} className="w-full h-auto" alt="Avatar Image" />
        <div className="bg-[#22C55E] w-[14px] h-[14px] rounded-full border-2 border-white absolute -right-0.5 -bottom-0.5"></div>
      </div>
    </>
  );
};

export const Smallplaceholder = () => {
  return (
    <>
      <div className='w-8 h-8 relative'>
        <Avatarplaceholder className='w-full h-auto' />
        <div className='bg-[#22C55E] w-3 h-3 rounded-full border-2 border-white absolute -right-0.5 -bottom-0.5'></div>
      </div>
    </>
  )
}

export const SmallInitials = () => {
  return (
    <>
      <div className="w-8 h-8 relative">
        <AvatarInitials className="w-full h-auto" />
        <div className="bg-[#22C55E] w-3 h-3 rounded-full border-2 border-white absolute -right-0.5 -bottom-0.5"></div>
      </div>
    </>
  );
};

export const SmallImage = () => {
  return (
    <>
      <div className="w-8 h-8 relative">
        <img src={AvatarImage} className="w-full h-auto" alt="Avatar Image" />
        <div className="bg-[#22C55E] w-3 h-3 rounded-full border-2 border-white absolute -right-0.5 -bottom-0.5"></div>
      </div>
    </>
  );
};

export const XSmallplaceholder = () => {
  return (
    <>
      <div className='w-6 h-6 relative'>
        <Avatarplaceholder className='w-full h-auto' />
        <div className='bg-[#22C55E] w-2.5 h-2.5 rounded-full border-2 border-white absolute -right-0.5 -bottom-0.5'></div>
      </div>
    </>
  )
}

export const XSmallInitials = () => {
  return (
    <>
      <div className="w-6 h-6 relative">
        <AvatarInitials className="w-full h-auto" />
        <div className="bg-[#22C55E] w-2.5 h-2.5 rounded-full border-2 border-white absolute -right-0.5 -bottom-0.5"></div>
      </div>
    </>
  );
};

export const XSmallImage = () => {
  return (
    <>
      <div className="w-6 h-6 relative">
        <img src={AvatarImage} className="w-full h-auto" alt="Avatar Image" />
        <div className="bg-[#22C55E] w-2.5 h-2.5 rounded-full border-2 border-white absolute -right-0.5 -bottom-0.5"></div>
      </div>
    </>
  );
};
