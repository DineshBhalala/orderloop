import React from 'react'

const AvatarsIcon = (props) => {
    return (
        <>
            <div className={`w-${props.widthOuter} h-${props.heightOuter} relative mr-2`}>
                <img src={props.imageIcon} alt="Avatar" />
                <div className={`bg-${props.colorIcon} w-${props.widthIcon} h-${props.heightIcon} rounded-full border-2 border-white absolute -right-0.5 -bottom-0.5`}></div>
            </div>
        </>
    )
}

export default AvatarsIcon