import React, { useState } from "react";

export const Switch = () => {
    return (
        <>
            <div className="switchToggle">
                <label htmlFor="toggleSwitch" className="flex items-center cursor-pointer">
                    <div className="relative">
                        <input type="checkbox" id="toggleSwitch" className="sr-only" defaultChecked />
                        <div className="block bg-neutral-300 switchButton w-10 h-6 rounded-[20px]"></div>
                        <div className="dot absolute left-0.5 top-0.5 bg-white w-5 h-5 rounded-full transition"></div>
                    </div>
                </label>
            </div>
        </>
    );
};

export const RadioButton = (props) => {
    const handleChangeOption = () => {
        props.handleOptionChange && props.handleOptionChange(props.optionId);
    };
    return (
        <>
            <div className={`radioButton flex flex-row items-center relative ml-${props.marginL} mt-${props.marginT} mr-${props.marginR} mb-${props.marginB}`}>
                <input type="radio" id={props.optionId} name="radioGroup" onChange={handleChangeOption} defaultChecked={props.chacked} />
                <label htmlFor={props.optionId} className="radio-label">
                    <span className={`${props.labelStyle ?? "paragraphSmallRegular"} pl-${props.paddingL ?? "5"}`}>{props.label}</span>
                </label>
            </div>
        </>
    );
};

export const CheckBox = (props) => {
    const [isChecked, setIsChecked] = useState(false);

    const handleCheckboxChange = () => {
        setIsChecked(!isChecked);
        props.isChecked && props.isChecked(props.object, !isChecked);
    };
    return (
        <>
            <div className={`checkboxstyle flex flex-row items-center relative ml-${props.marginL} mt-${props.marginT} mr-${props.marginR} mb-${props.marginB}`}>
                <input type="checkbox" id={props.optionId} defaultChecked={props.checked} checked={isChecked} onChange={handleCheckboxChange} />
                {props.label && (
                    <label htmlFor={props.optionId} className="checkbox-label">
                        <span className={`${props.labelStyle ?? "paragraphSmallMedium"} pl-${props.paddingL ?? "2"}`}>{props.label}</span>
                    </label>
                )}
            </div>
        </>
    );
};

export const CheckBoxWithoutLabels = (props) => {
    const [isChecked, setIsChecked] = useState(false);

    const handleCheckboxChange = () => {
        setIsChecked(!isChecked);
        props.isChecked && props.isChecked(props.object, !isChecked);
    };

    return (
        <>
            <div className="checkboxWithoutLabel">
                <input type="checkbox" checked={isChecked} onChange={handleCheckboxChange} />
                <div className="checkboxWithoutLabelVisible"></div>
            </div>
        </>
    );
};
