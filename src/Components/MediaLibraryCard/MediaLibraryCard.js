import React, { useState } from "react";

const MediaLibraryCard = (props) => {
  const [isClicked, setIsClickeding] = useState(false);

  return (
    <>
      <div
        onClick={() => setIsClickeding(!isClicked)}
        className={`h-full w-full px-2 pt-2 pb-3 border rounded-lg ${
          isClicked && "bg-primary-50 border-primary-500"
        } mobile:text-center cursor-pointer`}
      >
        <img
          src={props.icon}
          alt=""
          className="h-[180px] w-[180px] mb-1 lg:h-[144px] lg:w-[144px] md:w-[152px] md:h-[152px] mobile:mx-auto"
        />
        <h3 className="paragraphMediumMedium mb-1">{props.title}</h3>
        <p className="text-neutral-500 paragraphMediumItalic">
          {props.category}
        </p>
      </div>
    </>
  );
};

export default MediaLibraryCard;
