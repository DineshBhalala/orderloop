import React from "react";

export default function Table(props) {
    
  return (
    <>
      <div className="w-full rounded-lg overflow-auto [&::-webkit-scrollbar]:hidden md:hidden border-neutral-300 border">
        <table className="w-full break-words tableMediaLibrary">
          <thead>
            <tr className="paragraphOverlineSmall text-neutral-700 bg-neutral-50 text-left justify-center h-11">
              {props.header.map((el, index) => {
                return (
                  <th className="px-6 lg:min-w-[199px] capitalize shadow-innerShadow" key={index}>
                    {el}
                  </th>
                );
              })}
            </tr>
          </thead>
          <tbody>
            {props.tableRowDetails.map((el, index) => {
              return (
                <tr className={`paragraphSmallRegular ${index !== 0 && "border-t"} border-neutral-300 justify-center h-[70px]`} key={index}>
                  <td className="px-6">{el}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}
