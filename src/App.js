import React, { useState } from "react";
import { BrowserRouter, Routes, Route, Router, Navigate } from "react-router-dom";
import SidebarMenu from "./Screen/SidebarMenu/SidebarMenu";
import DashBoard from "./Screen/DashBoard/DashBoard";
import MediaLibrary from "./Screen/MediaLibrary/MediaLibrary";
import NoPage from "./Screen/NoPage";
import Rider from "./Screen/Rider/Rider";
import Credits from "./Screen/Credits/Credits";
import RiderDetails from "./Screen/Rider/RiderDetails";
import LoyaltyCashback from "./Screen/LoyaltyCashback/LoyaltyCashback";
import AbandonedCarts from "./Screen/AbandonedCarts/AbandonedCarts";
import OrderRatings from "./Screen/OrderRating/OrderRating";
import CustomerInformation from "./Screen/CustomerInformation/CustomerInformation";
import CustomerGroupDetails from "./Screen/CustomerInformation/CustomerGroupDetails";
import SelectRestaurant from "./Screen/SelectRestaurant/SelectRestaurant";
import CustomerOrderDetails from "./Screen/CustomerInformation/CustomerOrderDetails";
import Notifications from "./Screen/Notifications/Notifications";
import ItemAvailability from "./Screen/ItemAvailability/ItemAvailability";
import Login from "./Screen/Login/Login";
import Banners from "./Screen/Banners/Banners";
import Offers from "./Screen/Offers/Offers";
import CreateOffer from "./Screen/Offers/CreateOffer";
import ScrollToTop from "./ScrollToTop";
import BillPayments from "./Screen/BillPayment/BillPayments";
import KitchenDisplaySystem from "./Screen/KitchenDisplaySystem/KitchenDisplaySystem";
import Setting from "./Screen/Setting/Setting";
import TableManagement from "./Screen/TableManagement/TableManagement";
import MenuManagement from "./Screen/MenuManagement/MenuManagement";
import MenuManagementAddCategory from "./Screen/MenuManagement/MenuManagementAddCategory";
import MenuManagementAddDish from "./Screen/MenuManagement/AddDish/MenuManagementAddDish";
import MenuManagementOutletMenu from "./Screen/MenuManagement/OutletMenu/MenuManagementOutletMenu";
import MenuManagementDishDetails from "./Screen/MenuManagement/MenuManagementDishDetails";
import PosTableView from "./Screen/POS/TableView/PosTableView";
import PosDelivery from "./Screen/POS/Delivery/PosDelivery";
import PosOrders from "./Screen/POS/LiveOrder/PosOrders";

function App() {
    const [enableFullScreenMode, setEnableFullScreenMode] = useState(false);

    const handleFullScreenMode = (value) => {
        setEnableFullScreenMode(value);
    };

    const [isHoldOrder, setIsHoldOrder] = useState(false);

    const handelHoldOrder = () => {
        setIsHoldOrder(!isHoldOrder);
    };

    return (
        <>
            <BrowserRouter>
                <ScrollToTop />
                <Routes>
                    <Route path="/" element={<Login />} />
                    <Route path="select-restaurant" element={<SelectRestaurant />} />
                    <Route
                        path="*"
                        element={
                            <Routes>
                                <Route path="/" element={<SidebarMenu enableFullScreenMode={enableFullScreenMode} isHoldOrder={isHoldOrder} />}>
                                    <Route index element={<DashBoard />} />
                                    <Route path="dashboard" element={<DashBoard />} />
                                    <Route path="pos" element={<PosOrders />} />
                                    <Route path="pos/orders" element={<PosOrders />} />
                                    <Route path="pos/table-view" element={<PosTableView />} />
                                    <Route path="pos/delivery" element={<PosDelivery handelHoldOrder={handelHoldOrder} />} />
                                    <Route path="abandoned-carts" element={<AbandonedCarts />} />
                                    <Route path="kitchen-display-system" element={<KitchenDisplaySystem handleFullScreenMode={handleFullScreenMode} />} />
                                    <Route path="menu" element={<MenuManagement />} />
                                    <Route path="menu/add-category" element={<MenuManagementAddCategory />} />
                                    <Route path="menu/add-dish" element={<MenuManagementAddDish />} />
                                    <Route path="menu/outlet-menu" element={<MenuManagementOutletMenu />} />
                                    <Route path="menu/dish-details" element={<MenuManagementDishDetails />} />
                                    <Route path="media-library" element={<MediaLibrary />} />
                                    <Route path="item-availability" element={<ItemAvailability />} />
                                    <Route path="order-ratings" element={<OrderRatings />} />
                                    <Route path="customer-information" element={<CustomerInformation />} />
                                    <Route path="customer-group-details" element={<CustomerGroupDetails />} />
                                    <Route path="customer-order-details" element={<CustomerOrderDetails />} />
                                    <Route path="notifications" element={<Notifications />} />
                                    <Route path="offers" element={<Offers />} />
                                    <Route path="create-offer" element={<CreateOffer />} />
                                    <Route path="banners" element={<Banners />} />
                                    <Route path="credits" element={<Credits />} />
                                    <Route path="settings" element={<Setting />} />
                                    <Route path="riders" element={<Rider />} />
                                    <Route path="rider-details" element={<RiderDetails />} />
                                    <Route path="bill-payments" element={<BillPayments />} />
                                    <Route path="table-management" element={<TableManagement />} />
                                    <Route path="loyalty-cashback" element={<LoyaltyCashback />} />
                                    <Route path="*" element={<NoPage />} />
                                </Route>
                            </Routes>
                        }
                    />
                </Routes>
            </BrowserRouter>
        </>
    );
}

export default App;